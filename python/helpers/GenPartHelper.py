# a helper for finding and matching gen particles based on:
# https://github.com/hqucms/NanoHRT-tools/blob/master/python/producers/hrtMCTreeProducer.py
import numpy as np
import ROOT
import logging
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaR, closest

class genparthelper:
    #gencollection is "GenPart" collection, jetcollection is for example "Jets"
    def __init__(self):
        self._maxDeltaRJetParton = 0.4
        pass

    def tagttbb(self, event):
    # tags whether there are 2 hadronic tops and 2 bs in an event
        genparts = Collection(event, "GenPart")
        Ngentops=0; Ngenbs=0; Ngenbs2=0
        isttbb1 = False; isttbb2=False
        bsfromtops = []

        def getFinal( gp): #finds final daugter of a gen particle
            for idx in gp.dauIdx:
                dau = genparts[idx]
                if dau.pdgId==gp.pdgId:
                    return getFinal(dau)
            return gp

        def isHadronic( gp): #returns true if hadronic
            if len(gp.dauIdx)==0:
                raise ValueError('Particle has no daughters!')
            for idx in gp.dauIdx:
                if abs(genparts[idx].pdgId)<6: #quarkies
                    return True
            return False

        #adding daughter idx attribute to genparts if it's not there 
        for idx, gp in enumerate(genparts):
        #adding general index
            if not hasattr(gp, 'Idx'):   
                gp.Idx = [idx]
            if not hasattr(gp, 'dauIdx'):   
                gp.dauIdx = []
            if gp.genPartIdxMother >= 0:
                mom = genparts[gp.genPartIdxMother]
                if not hasattr(mom, 'dauIdx'):
                    mom.dauIdx = [idx]
                else:
                    mom.dauIdx.append(idx)

        for gpidx, gp in enumerate(genparts): #loop over genparts
        # status flags for proper hadronic process
            if gp.statusFlags & (1 << 13) == 0:
                continue       
            if abs(gp.pdgId)==6: #top
                Bkid = False; Wkid = False
                for i, idx in enumerate(gp.dauIdx): #loop over daugters
                    dau = genparts[idx]
                    # print 'daughter', idx, dau, '#', i
                    if abs(dau.pdgId)==24: #W
                        genW = getFinal(dau) #get final product of W
                        if isHadronic(genW):
                            Wkid=True
                        #print 'W', genW, 'mom', gp.genPartIdxMother
                    elif abs(dau.pdgId)==5: #bjet
                        Bkid=True
                        bsfromtops.append(dau.Idx)
                        #print 'B', genB, 'mom', gp.genPartIdxMother
                #print 'Wkid', Wkid, 'Bkid', Bkid
                if (Wkid and Bkid):
                    #print 'GOOD TOP'
                    Ngentops+=1

            elif abs(gp.pdgId)==5: #bottom
                #check that the b doesnt have mothers that are also bs:
                if gp.genPartIdxMother >= 0 and abs(genparts[gp.genPartIdxMother].pdgId)!=5:
                        Ngenbs+=1  #total bs
                else: #no b mothers, can add it
                    Ngenbs+=1

                if gp.Idx not in bsfromtops:
                    #print 'not from top!'
                    # #check that the b doesnt have mothers that are also bs: #is this what I have to add? check again that these are the same. 
                    # if gp.genPartIdxMother >= 0 and abs(genparts[gp.genPartIdxMother].pdgId)!=5:
                    Ngenbs2+=1 #bs not from tops
                        
        # print 'Ngentops',Ngentops, 'Ngenbs', Ngenbs
        if (Ngentops==2 and Ngenbs>=4):
            isttbb1=True
        if (Ngentops==2 and Ngenbs2>=2):
            isttbb2=True
        #v1 is all bs, v2 is only bs not from tops
        return isttbb1, isttbb2



    def tagleps(self, event):
    # tags whether there are leptonic W decays in an event
        genparts = Collection(event, "GenPart")
        Ngentops=0; Ngenbs=0; Ngenbs2=0
        nmu = 0; nel = 0; ntau = 0

        def getFinal( gp): #finds final daugter of a gen particle
            for idx in gp.dauIdx:
                dau = genparts[idx]
                if dau.pdgId==gp.pdgId:
                    return getFinal(dau)
            return gp

        def isHadronic( gp): #returns true if hadronic
            if len(gp.dauIdx)==0:
                raise ValueError('Particle has no daughters!')
            for idx in gp.dauIdx:
                if abs(genparts[idx].pdgId)<6: #quarkies
                    return True
            return False

        #adding daughter idx attribute to genparts if it's not there 
        for idx, gp in enumerate(genparts):
        #adding general index
            if not hasattr(gp, 'Idx'):   
                gp.Idx = [idx]
            if not hasattr(gp, 'dauIdx'):   
                gp.dauIdx = []
            if gp.genPartIdxMother >= 0:
                mom = genparts[gp.genPartIdxMother]
                if not hasattr(mom, 'dauIdx'):
                    mom.dauIdx = [idx]
                else:
                    mom.dauIdx.append(idx)

        for gpidx, gp in enumerate(genparts): #loop over genparts
        # status flags for proper hadronic process
            # if gp.statusFlags & (1 << 13) == 0:
            #     continue       
            if abs(gp.pdgId)==6: #top
                Bkid = False; Wkid = False
                for i, idx in enumerate(gp.dauIdx): #loop over daugters
                    dau = genparts[idx]
                    # print 'daughter', idx, dau, '#', i
                    if abs(dau.pdgId)==24: #W
                        genW = getFinal(dau) #get final product of W
                        if not isHadronic(genW):
                            if abs(genW.pdgId)==11 and gp.genPartIdxMother >= 0 and abs(genparts[gp.genPartIdxMother].pdgId)!=11:
                                nel+=1
                            elif abs(genW.pdgId)==13 and gp.genPartIdxMother >= 0 and abs(genparts[gp.genPartIdxMother].pdgId)!=13:
                                nmu+=1
                            elif abs(genW.pdgId)==15 and gp.genPartIdxMother >= 0 and abs(genparts[gp.genPartIdxMother].pdgId)!=15:
                                ntau+=1
        return nmu, nel, ntau



    def IndexDaughters(self, genparts):
    #adding daughter idx attribute to genparts if it's not there 
        for idx, gp in enumerate(genparts):
            if not hasattr(gp, 'dauIdx'):   
                gp.dauIdx = []
            if gp.genPartIdxMother >= 0:
                mom = genparts[gp.genPartIdxMother]
                if not hasattr(mom, 'dauIdx'):
                    mom.dauIdx = [idx]
                else:
                    mom.dauIdx.append(idx)
        return genparts


    # gen match leptons, takes collection of genparticles
    def FindGenLeps(self, GenParts):
        gen_mus = []
        gen_els = []
        ngtaus=0; ngmus=0; ngels=0
        for gp in GenParts:
            #find electrons and muons: try this way
            momidx = gp.genPartIdxMother
            if momidx>=0:
                mom = GenParts[momidx]
                if (mom.pdgId==23 or abs(mom.pdgId)==24): #Ws
                    if abs(gp.pdgId)==11:
                        gen_els.append(gp)
                        ngels+=1
                    elif abs(gp.pdgId)==13:
                        gen_mus.append(gp)
                        ngmus+=1
                    elif abs(gp.pdgId)==15:
                        ngtaus+=1

                elif abs(mom.pdgId)==15: #taus
                    if abs(gp.pdgId)==11:
                        gen_els.append(gp)
                    elif abs(gp.pdgId)==13:
                        gen_mus.append(gp)

            ngenleps=ngels+ngmus+ngtaus #only count those coming from the W
            return np.array(gen_els), np.array(gen_mus), ngenleps


    # finds gen particles corresponding to hadronic tops
    # returns gentopcands, which are arrays of [parton, partonidx, genb, genWdau1, genWdau2]
    def GenTopFinder(self, event):
        genparts = Collection(event, "GenPart")
        gentopcands=[]

        def getFinal( gp): #finds final daugter of a gen particle
            for idx in gp.dauIdx:
                dau = genparts[idx]
                if dau.pdgId==gp.pdgId:
                    return getFinal(dau)
            return gp

        def isHadronic( gp): #returns true if hadronic
            if len(gp.dauIdx)==0:
                raise ValueError('Particle has no daughters!')
            for idx in gp.dauIdx:
                if abs(genparts[idx].pdgId)<6: #quarkies
                    return True
            return False

        #adding daughter idx attribute to genparts if it's not there 
        for idx, gp in enumerate(genparts):
            if not hasattr(gp, 'dauIdx'):   
                gp.dauIdx = []
            if gp.genPartIdxMother >= 0:
                mom = genparts[gp.genPartIdxMother]
                if not hasattr(mom, 'dauIdx'):
                    mom.dauIdx = [idx]
                else:
                    mom.dauIdx.append(idx)

        for idx, gp in enumerate(genparts): #loop over genparts
            gentop = []
        # status flags for proper hadronic process
            if gp.statusFlags & (1 << 13) == 0:
                continue       
            Bkid = False; Wkid = False
            if abs(gp.pdgId)==6: #top
                for i, idx in enumerate(gp.dauIdx): #loop over daugters
                    dau = genparts[idx]
                    # print 'daughter', idx, dau, '#', i
                    if abs(dau.pdgId)==24: #W
                        genW = getFinal(dau) #get final product of W
                        if isHadronic(genW):
                            Wkid=True
                        #print 'W', genW, 'mom', gp.genPartIdxMother
                    elif abs(dau.pdgId)in(1,3,5): #bjet
                        genB = dau
                        Bkid=True
                        #print 'B', genB, 'mom', gp.genPartIdxMother
            #print 'Wkid', Wkid, 'Bkid', Bkid
            if (Wkid and Bkid):
                #print 'GOOD TOP'
                gentop = [gp, idx, genB, genparts[genW.dauIdx[0]], genparts[genW.dauIdx[1]] ]
                gentopcands.append(gentop)
                
        return gentopcands

    #adds jet idx to a jet collection
    def AddJetIdx(self, JetCollection): #**use to give indices to topcands
        for idx, jet in enumerate(JetCollection):
            if not hasattr(jet, 'Idx'):
                jet.Idx = [idx]
            # print 'jet', jet, 'idx', idx
        return JetCollection
        
    #takes parton (gp) and finds matched jet
    def FindJetMatch(self, gentopparton, JetCollection):
        jet, dR = closest(gentopparton, JetCollection)
        return False if dR > self._maxDeltaRJetParton else jet

    #takes parton (gp) and checks if top jet is matched
    # def FindJetMatch(self, gentopparton, JetCollection):
    #     jet, dR = closest(gentopparton, JetCollection)
    #     return _NullObject() if dR > self._maxDeltaRJetParton else jet

    #takes (bdt-based) topcands and returns if they are genmatched or not. 
    def GenMatchTops(self, event, topcand, usemvatops=True):
        #topcand is your array [p4, bjet, j2, j3]
        #print topcand
        JetColl = Collection(event, "Jet")
        JetColl = self.AddJetIdx(JetColl)
        gentopcands = self.GenTopFinder(event)
        #match to topjets or ak4jets
        for gt in gentopcands:
            bjet = self.FindJetMatch(gt[2], JetColl)           
            jet2 = self.FindJetMatch(gt[3], JetColl)
            jet3 = self.FindJetMatch(gt[4], JetColl)
            if bjet==False or jet2==False or jet3==False:
                'cant find gen match'
                return False
            # print bjet.Idx[0], jet2.Idx[0], jet3.Idx[0]
            goodinds = {bjet.Idx[0]:'bjet', jet2.Idx[0]:'jet2', jet3.Idx[0]:'jet3'}
        #then check if those jets are in the topcand. 
            if usemvatops:
                triplejet = 0
                for tc in topcand[1:-1]:
                    # print tc, tc.Idx[0], 'vs', bjet, jet2, jet3, 'inds:', bjet.Idx, jet2.Idx, jet3.Idx
                    # if (bjet.Idx == tc.Idx or jet2.Idx == tc.Idx or jet3.Idx == tc.Idx:
                    if tc.Idx[0] in goodinds:
                        # print 'found', goodinds[tc.Idx[0]]
                        triplejet+=1
                # print 'number of matched jets:', triplejet
                if triplejet==3:
                    # print 'correctly tagged top!'
                    # print 'cand jets pts:', topcand[1].pt, topcand[2].pt, topcand[3].pt
                    # print 'gen jets pts:', gt[2].pt, gt[3].pt, gt[4].pt 
                    # print topcand[-1]
                    return True
            elif not usemvatops:
                deepcand = [topcand.j1Idx, topcand.j2Idx, topcand.j3Idx]
                dtriplejet = 0
                for dc in deepcand:
                    # print JetColl[dc], dc, 'vs', bjet, jet2, jet3, 'inds:', bjet.Idx, jet2.Idx, jet3.Idx
                    # if (bjet.Idx == dc or jet2.Idx == dc or jet3.Idx == dc):                        
                    if dc in goodinds:
                        # print 'found', goodinds[dc]
                        dtriplejet+=1
                # print 'number of matched jets:', dtriplejet
                if dtriplejet==3:
                    # print 'correctly tagged top!'
                    # print 'cand jets pts:', JetColl[topcand.j1Idx].pt, JetColl[topcand.j2Idx].pt, JetColl[topcand.j3Idx].pt
                    # print 'gen jets pts:', gt[2].pt, gt[3].pt, gt[4].pt 
                    return True
        return False

    # a version that DOES care if all the jets are matched exactly right (not done)
    # def GenMatchTops(self, event, topcand, usemvatops=True):
    #     #topcand is your array [p4, bjet, j2, j3]
    #     #print topcand
    #     JetColl = Collection(event, "Jet")
    #     JetColl = self.AddJetIdx(JetColl)
    #     gentopcands = self.GenTopFinder(event)
    #     #match to topjets or ak4jets
    #     for gt in gentopcands:
    #         bjet = self.FindJetMatch(gt[2], JetColl)           
    #         jet2 = self.FindJetMatch(gt[3], JetColl)
    #         jet3 = self.FindJetMatch(gt[4], JetColl)
    #         if bjet==False or jet2==False or jet3==False:
    #             'cant find gen match'
    #             return False
    #         print bjet.Idx[0], jet2.Idx[0], jet3.Idx[0]
    #         goodinds = {bjet.Idx[0]:'bjet', jet2.Idx[0]:'jet2', jet3.Idx[0]:'jet3'}
    #     #then check if those jets are in the topcand. 
    #         if usemvatops:
    #             triplejet = 0
    #             for tc in topcand[1:-1]:
    #                 print tc, tc.Idx[0], 'vs', bjet, jet2, jet3, 'inds:', bjet.Idx, jet2.Idx, jet3.Idx
    #                 # if (bjet.Idx == tc.Idx or jet2.Idx == tc.Idx or jet3.Idx == tc.Idx:
    #                 if tc.Idx[0] in goodinds:
    #                     print 'found', goodinds[tc.Idx[0]]
    #                     triplejet+=1
    #             print 'number of matched jets:', triplejet
    #             if triplejet==3:
    #                 print 'correctly tagged top!'
    #                 print 'cand jets pts:', topcand[1].pt, topcand[2].pt, topcand[3].pt
    #                 print 'gen jets pts:', gt[2].pt, gt[3].pt, gt[4].pt 
    #                 print topcand[-1]
    #                 return True
    #         elif not usemvatops:
    #             deepcand = [topcand.j1Idx, topcand.j2Idx, topcand.j3Idx]
    #             dtriplejet = 0
    #             for dc in deepcand:
    #                 print JetColl[dc], dc, 'vs', bjet, jet2, jet3, 'inds:', bjet.Idx, jet2.Idx, jet3.Idx
    #                 # if (bjet.Idx == dc or jet2.Idx == dc or jet3.Idx == dc):                        
    #                 if dc in goodinds:
    #                     print 'found', goodinds[dc]
    #                     dtriplejet+=1
    #             print 'number of matched jets:', dtriplejet
    #             if dtriplejet==3:
    #                 print 'correctly tagged top!'
    #                 print 'cand jets pts:', JetColl[topcand.j1Idx].pt, JetColl[topcand.j2Idx].pt, JetColl[topcand.j3Idx].pt
    #                 print 'gen jets pts:', gt[2].pt, gt[3].pt, gt[4].pt 
    #                 return True
    #     return False
