import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

class plotdefs:
    def __init__(self, year, base, cr ='D', modbins=False, usettbb=False):
        self.modbins = modbins
        self.cr = cr
        self.year = year
        self.base = base
        self.lumi = str(rs.lumis[year]['0lep'])
        self.tn = 'Events'
        self.mergehtbins=False
        self.usevarbin=True
        self.edges = np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0], dtype=np.double)
        self.shifts = [2, 10, 20, 30]
        self.usettbb = usettbb
        self.addunc = True
        # if base =='SR':
        #     self.addunc = True
        # print type(year), type(base), type(cr)

        self.mcprocs = ["tttt", "ttH+ttV", "minor"]
        if self.usettbb:
            self.mcprocs = ["tttt", "ttH+ttV", "minor", "ttbb"]

        self.colors = [r.TColor.GetColor("#a1794a"), r.TColor.GetColor("#163d4e"), r.TColor.GetColor("#d07e93"), r.TColor.GetColor("#c1caf3"), r.kBlack,  r.kCyan, r.kOrange, r.kMagenta, r.TColor.GetColor("#cf9ddb"), r.kRed-7, r.kBlack,r.kPink+2,  r.kMagenta-9, r.kViolet-4, r.kOrange+1, r.kYellow-7, r.kCyan-7, r.kBlue-9, r.kGreen-9]
        
        # self.shapeshift = {'2016':{'cut0bin0':0.01, #old
        #                            'cut0bin1':0.02,
        #                            'cut0bin2':0.03,
        #                            'cut0bin3':0.02,
        #                            'cut0bin4':0.03,
        #                            'cut0bin5':0.02,
        #                            'cut0bin6':0.02,
        #                            'cut0bin7':0.01,
        #                            'cut1bin0':0.01,
        #                            'cut1bin1':0.02,
        #                            'cut2bin0':0.01,
        #                            'cut2bin1':0.01},
        #                    '2017':{'cut0bin0':0.01,
        #                            'cut0bin1':0.01,
        #                            'cut0bin2':0.03,
        #                            'cut0bin3':0.01,
        #                            'cut0bin4':0.01,
        #                            'cut0bin5':0.01,
        #                            'cut0bin6':0.01,
        #                            'cut0bin7':0.01,
        #                            'cut1bin0':0.02,
        #                            'cut1bin1':0.02,
        #                            'cut2bin0':0.02,
        #                            'cut2bin1':0.01},
        #                    '2018':{'cut0bin0':0.01, #old
        #                            'cut0bin1':0.01,
        #                            'cut0bin2':0.01,
        #                            'cut0bin3':0.01,
        #                            'cut0bin4':0.01,
        #                            'cut0bin5':0.01,
        #                            'cut0bin6':0.01,
        #                            'cut0bin7':0.01,
        #                            'cut1bin0':0.02,
        #                            'cut1bin1':0.01,
        #                            'cut2bin0':0.01,
        #                            'cut2bin1':0.01}}

        #NEW - statcomm change
        # self.shapeshift = {'2016':{'cut0bin0':0.232,
        #                            'cut0bin1':0.566,
        #                            'cut0bin2':0.029,
        #                            'cut0bin3':0.305,
        #                            'cut0bin4':0.109,
        #                            'cut0bin5':0.093,
        #                            'cut0bin6':0.264,
        #                            'cut0bin7':0.132,
        #                            'cut1bin0':0.029,
        #                            'cut1bin1':0.627,
        #                            'cut2bin0':0.254,
        #                            'cut2bin1':0.033},
        #                    '2017':{'cut0bin0':0.388,
        #                            'cut0bin1':0.082,
        #                            'cut0bin2':0.218,
        #                            'cut0bin3':0.289,
        #                            'cut0bin4':0.216,
        #                            'cut0bin5':0.368,
        #                            'cut0bin6':0.071,
        #                            'cut0bin7':0.108,
        #                            'cut1bin0':0.041,
        #                            'cut1bin1':0.201,
        #                            'cut2bin0':0.823,
        #                            'cut2bin1':0.608},
        #                    '2018':{'cut0bin0':0.104,                           
        #                            'cut0bin1':0.229,
        #                            'cut0bin2':0.067,
        #                            'cut0bin3':0.088,
        #                            'cut0bin4':0.173,
        #                            'cut0bin5':0.100,
        #                            'cut0bin6':0.309,
        #                            'cut0bin7':0.026,
        #                            'cut1bin0':0.184,
        #                            'cut1bin1':0.191,
        #                            'cut2bin0':0.041,
        #                            'cut2bin1':0.035}}

        #NEW - statcomm change (full hist)
        # self.shapeshift = {'2016':{'cut0bin0':[0.07873332500457764, 0.173775315284729, 0.02992713451385498, 0.05856722593307495, 0.05955100059509277, 0.012038350105285645, 0.06966036558151245, 0.015949130058288574, 0.23236119747161865],
        #                            'cut0bin1':[0.0950155258178711, 0.01145172119140625, 0.09251368045806885, 0.07166421413421631, 0.01629263162612915, 0.11257219314575195, 0.06964421272277832, 0.03446882963180542, 0.5659515857696533],
        #                            'cut0bin2':[0.05013000965118408, 0.0016783475875854492, 0.0793907642364502, 0.027339518070220947, 0.011176705360412598, 0.017948627471923828, 0.11616873741149902, 0.06437742710113525, 0.028673827648162842],
        #                            'cut0bin3':[0.034958481788635254, 0.1472616195678711, 0.03725409507751465, 0.12915992736816406, 0.04318714141845703, 0.0913163423538208, 0.08566403388977051, 0.08255279064178467, 0.30498582124710083],
        #                            'cut0bin4':[0.1198502779006958, 0.177994966506958, 0.041634440422058105, 0.10013079643249512, 0.13960641622543335, 0.0022071003913879395, 0.19130253791809082, 0.001128852367401123, 0.10928815603256226],
        #                            'cut0bin5':[0.08795249462127686, 0.26602256298065186, 0.08341020345687866, 0.04310697317123413, 0.06709915399551392, 0.01866227388381958, 0.013856172561645508, 0.07430672645568848, 0.09334611892700195],
        #                            'cut0bin6':[0.11720764636993408, 0.031653404235839844, 0.05572474002838135, 0.04606151580810547, 0.07206499576568604, 0.037745535373687744, 0.25868135690689087, 0.06871700286865234, 0.2636009454727173],
        #                            'cut0bin7':[0.24269556999206543, 0.07828664779663086, 0.21057486534118652, 0.04149460792541504, 0.01970851421356201, 0.0393141508102417, 0.04063308238983154, 0.06573742628097534, 0.13219141960144043],
        #                            'cut1bin0':[1.148052453994751, 0.5451275408267975, 0.21531176567077637, 0.17825591564178467, 0.0323336124420166, 0.055235981941223145, 0.1007070541381836, 0.349560022354126, 0.028841853141784668],
        #                            'cut1bin1':[7.242439270019531, 0.40674853324890137, 0.07461118698120117, 0.3622620701789856, 0.0309755802154541, 0.25419044494628906, 0.2252628207206726, 0.07454335689544678, 0.6268599033355713],
        #                            'cut2bin0':[0.7332748174667358, 0.20886832475662231, 0.0068680644035339355, 0.09313219785690308, 0.1590181589126587, 0.04471755027770996, 0.07076907157897949, 0.42224764823913574, 0.25395673513412476],
        #                            'cut2bin1':[1.0, 0.31460410356521606, 0.39445579051971436, 0.04941028356552124, 0.05967479944229126, 0.09612441062927246, 0.0147627592086792, 0.11687731742858887, 0.03345692157745361]},
        #                    '2017':{'cut0bin0':[0.22591209411621094, 0.033391475677490234, 0.0443652868270874, 0.04926586151123047, 0.09153628349304199, 0.03418058156967163, 0.024461984634399414, 0.09270071983337402, 0.38805198669433594],
        #                            'cut0bin1':[0.14566516876220703, 0.042475342750549316, 0.04494750499725342, 0.02077937126159668, 0.04673194885253906, 0.017431020736694336, 0.026498079299926758, 0.06897532939910889, 0.08199405670166016],
        #                            'cut0bin2':[0.14629578590393066, 0.016165852546691895, 0.10241615772247314, 0.04417622089385986, 0.005196809768676758, 0.05918872356414795, 0.017646968364715576, 0.07683408260345459, 0.21797215938568115],
        #                            'cut0bin3':[0.2244831919670105, 0.019137263298034668, 0.12446814775466919, 0.02687293291091919, 0.13116669654846191, 0.09061968326568604, 0.0615389347076416, 0.1269296407699585, 0.28928685188293457],
        #                            'cut0bin4':[0.34148699045181274, 0.04441487789154053, 0.01965653896331787, 0.0427173376083374, 0.07986122369766235, 0.030756711959838867, 0.04124188423156738, 0.07363569736480713, 0.21594929695129395],
        #                            'cut0bin5':[0.20047438144683838, 0.06128489971160889, 0.20879769325256348, 0.0057599544525146484, 0.09324288368225098, 0.08075082302093506, 0.0993582010269165, 0.036638498306274414, 0.36797165870666504],
        #                            'cut0bin6':[0.17155218124389648, 0.24058949947357178, 0.13836205005645752, 0.02352309226989746, 0.09709173440933228, 0.03459513187408447, 0.16289371252059937, 0.18851518630981445, 0.07065999507904053],
        #                            'cut0bin7':[0.434312105178833, 0.4972972869873047, 0.05966365337371826, 0.1506568193435669, 0.07648777961730957, 0.06459510326385498, 0.0301896333694458, 0.09849953651428223, 0.1078101396560669],
        #                            'cut1bin0':[0.17585891485214233, 0.016112089157104492, 0.15254688262939453, 0.08021426200866699, 0.05556678771972656, 0.2797966003417969, 0.1186065673828125, 0.2631765604019165, 0.04073333740234375],
        #                            'cut1bin1':[1.0, 0.42138373851776123, 0.27192115783691406, 0.602188229560852, 0.28815436363220215, 0.20108354091644287, 0.017835497856140137, 0.12011343240737915, 0.23984134197235107],
        #                            'cut2bin0':[0.059799134731292725, 0.1888434886932373, 0.20541071891784668, 0.12738466262817383, 0.07298684120178223, 0.08845871686935425, 0.06519216299057007, 0.061752915382385254, 0.8234851360321045],
        #                            'cut2bin1':[1150.10205078125, 6.643919944763184, 0.16005796194076538, 0.0959126353263855, 0.21670985221862793, 0.17650121450424194, 0.09047287702560425, 0.024848997592926025, 0.6077666282653809]},
        #                    '2018':{'cut0bin0':[0.18651032447814941, 0.07104229927062988, 0.006566047668457031, 0.07982772588729858, 0.05548936128616333, 0.017359673976898193, 0.0039653778076171875, 0.13374412059783936, 0.10405266284942627],                           
        #                            'cut0bin1':[0.049058377742767334, 0.07642829418182373, 0.006828188896179199, 0.00873333215713501, 0.09829729795455933, 0.04753267765045166, 0.06106388568878174, 0.13466525077819824, 0.22925198078155518],
        #                            'cut0bin2':[0.0027837753295898438, 0.019880056381225586, 0.022519230842590332, 0.009724259376525879, 0.0858660340309143, 0.03144717216491699, 0.04767405986785889, 0.044554710388183594, 0.06652712821960449],
        #                            'cut0bin3':[0.22211432456970215, 0.006950855255126953, 0.018637895584106445, 0.02766132354736328, 0.045293211936950684, 0.010698795318603516, 0.050363898277282715, 0.04005396366119385, 0.0876990556716919],
        #                            'cut0bin4':[0.19681143760681152, 0.018901944160461426, 0.046617329120635986, 0.10966002941131592, 0.0011200904846191406, 0.13033849000930786, 0.17615115642547607, 0.15869760513305664, 0.17263329029083252],
        #                            'cut0bin5':[0.38880592584609985, 0.009483814239501953, 0.013504981994628906, 0.10545170307159424, 0.0500485897064209, 0.03129887580871582, 0.10800337791442871, 0.08731305599212646, 0.1000676155090332],
        #                            'cut0bin6':[0.3753606081008911, 0.10384476184844971, 0.05175900459289551, 0.049691200256347656, 0.017440438270568848, 0.10496068000793457, 0.037431955337524414, 0.016379594802856445, 0.30927640199661255],
        #                            'cut0bin7':[0.26959025859832764, 0.4663197994232178, 0.020956039428710938, 0.21315091848373413, 0.08701944351196289, 0.024099528789520264, 0.10668718814849854, 0.05350601673126221, 0.02636796236038208],
        #                            'cut1bin0':[0.33703017234802246, 0.04064297676086426, 0.14055728912353516, 0.13697409629821777, 0.23020005226135254, 0.001801133155822754, 0.08043384552001953, 0.07982444763183594, 0.18367862701416016],
        #                            'cut1bin1':[1.3832030296325684, 0.5674604177474976, 0.2575702667236328, 0.28086012601852417, 0.05956137180328369, 0.012285351753234863, 0.07203412055969238, 0.04196369647979736, 0.19148552417755127],
        #                            'cut2bin0':[0.6907228529453278, 0.2788299322128296, 0.015856623649597168, 0.08131027221679688, 0.07123041152954102, 0.005060076713562012, 0.1216859221458435, 0.13773465156555176, 0.04135143756866455],
        #                            'cut2bin1':[1.0, 0.1416594386100769, 0.554854542016983, 0.12733125686645508, 0.1645677089691162, 0.0848623514175415, 0.13359403610229492, 0.11033904552459717, 0.03469574451446533]}}

        # #NEW
        self.shapeshift = {'2016':{'cut0bin0':0.01,
                                   'cut0bin1':0.03,
                                   'cut0bin2':0.01,
                                   'cut0bin3':0.02,
                                   'cut0bin4':0.02,
                                   'cut0bin5':0.01,
                                   'cut0bin6':0.02,
                                   'cut0bin7':0.01,
                                   'cut1bin0':0.01,
                                   'cut1bin1':0.03,
                                   'cut2bin0':0.02,
                                   'cut2bin1':0.01},
                           '2017':{'cut0bin0':0.01,
                                   'cut0bin1':0.01,
                                   'cut0bin2':0.03,
                                   'cut0bin3':0.02,
                                   'cut0bin4':0.01,
                                   'cut0bin5':0.01,
                                   'cut0bin6':0.01,
                                   'cut0bin7':0.02,
                                   'cut1bin0':0.01,
                                   'cut1bin1':0.01,
                                   'cut2bin0':0.03,
                                   'cut2bin1':0.01},
                           '2018':{'cut0bin0':0.01,                                                                                            
                                   'cut0bin1':0.01,
                                   'cut0bin2':0.01,
                                   'cut0bin3':0.01,
                                   'cut0bin4':0.01,
                                   'cut0bin5':0.01,
                                   'cut0bin6':0.02,
                                   'cut0bin7':0.01,
                                   'cut1bin0':0.02,
                                   'cut1bin1':0.03,
                                   'cut2bin0':0.01,
                                   'cut2bin1':0.02}}

        # self.colors = {'tttt':r.TColor.GetColor("#163d4e"),
        #                'DDBKG':r.kRed-7,
        #                # 'TTX':r.TColor.GetColor("#d07e93"),
        #                'ttH':r.kCyan,
        #                'ttW':r.kOrange,
        #                'ttOther':r.kMagenta,
        #                'minor':r.TColor.GetColor("#cf9ddb")}


        # if not modbins:
        NAFfiles = {'SR': '../combine/naf_March_09/naf_SR_'+year+'_Tree2.root',
                    'VR': '../combine/naf_March_09/naf_VR_'+year+'_Tree2.root'}
        self.NAFfile = r.TFile(NAFfiles[base])
        self.NAFfilename = NAFfiles[base]

        datafiles = {'2016':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/JETHT_'+year+'_merged_0lep_tree.root',
                     '2017':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/JETHT_'+year+'_merged_0lep_tree.root',
                     '2018':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/JETHT_'+year+'_merged_0lep_tree.root',
                     'ALL':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/JETHT_'+year+'_merged_0lep_tree.root',}

        self.mcfiles = {'tttt': '/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TTTT_'+year+'_merged_0lep_tree.root',
                        'ttH+ttV':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TTX_'+year+'_merged_0lep_tree.root',
                        'ttX':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TTX_'+year+'_merged_0lep_tree.root',
                        'ttbb':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TT_'+year+'_merged_0lep_tree.root',
                        'minor':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/minorMC_'+year+'_merged_0lep_tree.root',
                        'other':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/minorMC_'+year+'_merged_0lep_tree.root',
                        'MC': '/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/MC_noTTbb_'+year+'_merged_0lep_tree.root'}
        #old:
        # datafiles = {'2016':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',
        #              '2017':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',
        #              '2018':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',
        #              'ALL':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',}

        # self.mcfiles = {'tttt': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/TTTT_'+year+'_merged_0lep_tree.root',
        #                 'ttH+ttV' : '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/newTTX_'+year+'_merged_0lep_tree.root',
        #                 'ttbb':'/eos/uscms/store/user/lpcstop/mequinna/ttbb_studies/ttbb_'+year+'_merged.root',
        #                 'other':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/minorMC_'+year+'_merged_0lep_tree.root',
        #                 'MC': '/eos/uscms/store/user/lpcstop/mequinna/pt50_studies/MC_'+year+'_merged_0lep_tree.root'}

        # self.datafiles=datafiles
        self.datafile = r.TFile(datafiles[year])
        self.datafilename = datafiles[year]

        # self.QCD_TT_file =  '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/QCD_TT_'+year+'_merged_0lep_tree.root'

        metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
        metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'
        
        self.dstr = {'2016' : '&& (trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || trigHLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
                     '2017' : '&& (trigHLT_PFHT430_SixJet40_BTagCSV_p080 || trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,
                     '2018' : '&& (trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,
                     'ALL' : '&& (passtrig)'+metfilters,}

        baseline = {'VR':'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==8',
                    'SR':'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9'}

        #no bSFcorr
        # self.mcstr = {'2016' : metfilters16+')*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
        #               '2017' : metfilters+')*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
        #               '2018' : metfilters+')*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
        #               'ALL' : '&& METFilters )*weight*btagSF_nosys*puWeight*TrigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,}
        
        #the real deal
        self.mcstr = {'2016' : metfilters16+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
                      '2017' : metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
                      '2018' : metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
                      'ALL' : '&& METFilters )*weight*btagSF_nosys*bSFcorr_nosys*puWeight*TrigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,}

        binsels = {'cut0bin0':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=700 && ht_nosys<800',
                   'cut0bin1':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=800 && ht_nosys<900', 
                   'cut0bin2':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=900 && ht_nosys<1000', 
                   'cut0bin3':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1000 && ht_nosys<1100',
                   'cut0bin4':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1100 && ht_nosys<1200',
                   'cut0bin5':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1200 && ht_nosys<1300',
                   'cut0bin6':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1300 && ht_nosys<1500', 
                   'cut0bin7':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1500',
                   'cut1bin0':'nrestops_nosys==1 && nbstops_nosys>=1 && ht_nosys<1400',
                   'cut1bin1':'nrestops_nosys==1 && nbstops_nosys>=1 && ht_nosys>=1400',
                   'cut2bin0':'nrestops_nosys>=2 && nbstops_nosys>=0 && ht_nosys<1100',
                   'cut2bin1':'nrestops_nosys>=2 && nbstops_nosys>=0 && ht_nosys>=1100'}
        if modbins:
            binsels['cut1bin0']='nrestops_nosys==1 && nbstops_nosys>=1'
            binsels['cut2bin0']='nrestops_nosys>=2 && nbstops_nosys>=0' 

        if self.mergehtbins:
            binsels = {'cut0':'nrestops_nosys==1 && nbstops_nosys==0',
                       'cut1':'nrestops_nosys==1 && nbstops_nosys>=1',
                       'cut2':'nrestops_nosys>=2 && nbstops_nosys>=0'}

        self.binsels = binsels
        self.basesel = baseline[base]

        basesel_nob = baseline[base].replace(' && nbjets_nosys>=3', '')
        self.basesel_nob = basesel_nob

        ttbb_basesel = baseline[base].replace('nfunky_leptons==0 &&', 'nfunky_leptons==0 && isttbb==1 &&')
        ttbb_basesel_nob = basesel_nob.replace('nfunky_leptons==0 &&', 'nfunky_leptons==0 && isttbb==1 &&')
        self.ttbb_basesel_nob = ttbb_basesel_nob
        self.ttbb_basesel = ttbb_basesel
        
        outdir = './plots/predhists/'+base+year+'_'+self.cr+'/'
        # outdir = './plots/randseed/'+base+year+'/'
        if self.usettbb:
            outdir = './plots/ttbbhists/'+base+year+'_'+self.cr+'/'
        
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        self.outdir = outdir


        if not usettbb: #NO TTBB - NEW
            extDDyields = {'2016' : {'VR' : {"cut0bin0": [ 3223.934 , 104.645 ],
                                             "cut0bin1": [ 2200.325 , 88.871 ],
                                             "cut0bin2": [ 1683.785 , 85.641 ],
                                             "cut0bin3": [ 1113.855 , 71.846 ],
                                             "cut0bin4": [ 960.947 , 77.273 ],
                                             "cut0bin5": [ 598.858 , 63.167 ],
                                             "cut0bin6": [ 503.337 , 51.46 ],
                                             "cut0bin7": [ 489.402 , 61.272 ],
                                             "cut1bin0": [ 369.221 , 37.026 ],
                                             "cut1bin1": [ 142.227 , 31.866 ],
                                             "cut2bin0": [ 494.501 , 44.052 ],
                                             "cut2bin1": [ 115.192 , 39.515 ]},
                                     'SR' : {"cut0bin0": [ 1541.432 , 64.492 ],
                                             "cut0bin1": [ 1502.071 , 71.544 ],
                                             "cut0bin2": [ 1149.019 , 64.472 ],
                                             "cut0bin3": [ 1041.726 , 69.389 ],
                                             "cut0bin4": [ 670.204 , 54.03 ],
                                             "cut0bin5": [ 480.002 , 48.759 ],
                                             "cut0bin6": [ 691.133 , 65.515 ],
                                             "cut0bin7": [ 758.273 , 80.068 ],
                                             "cut1bin0": [ 196.7 , 25.43 ],
                                             "cut1bin1": [ 282.765 , 54.089 ],
                                             "cut2bin0": [ 493.862 , 45.909 ],
                                             "cut2bin1": [ 276.909 , 55.954 ]} },
                           '2017' : {'VR' : {"cut0bin0": [ 3947.812 , 135.039 ],
                                             "cut0bin1": [ 2837.362 , 119.231 ],
                                             "cut0bin2": [ 2276.728 , 120.041 ],
                                             "cut0bin3": [ 1286.622 , 85.777 ],
                                             "cut0bin4": [ 990.588 , 84.656 ],
                                             "cut0bin5": [ 628.487 , 68.725 ],
                                             "cut0bin6": [ 700.244 , 79.058 ],
                                             "cut0bin7": [ 543.886 , 75.552 ],
                                             "cut1bin0": [ 455.902 , 46.289 ],
                                             "cut1bin1": [ 179.256 , 39.908 ],
                                             "cut2bin0": [ 690.001 , 63.038 ],
                                             "cut2bin1": [ 180.098 , 74.453 ]},
                                     'SR' : {"cut0bin0": [ 1997.86 , 81.83 ],
                                             "cut0bin1": [ 1864.015 , 85.777 ],
                                             "cut0bin2": [ 1262.894 , 68.668 ],
                                             "cut0bin3": [ 1178.831 , 76.508 ],
                                             "cut0bin4": [ 748.311 , 59.75 ],
                                             "cut0bin5": [ 530.642 , 52.263 ],
                                             "cut0bin6": [ 728.663 , 69.398 ],
                                             "cut0bin7": [ 628.353 , 68.64 ],
                                             "cut1bin0": [ 295.489 , 36.462 ],
                                             "cut1bin1": [ 440.826 , 77.783 ],
                                             "cut2bin0": [ 582.581 , 53.309 ],
                                             "cut2bin1": [ 339.314 , 65.745 ]} },
                           '2018' : {'VR' : {"cut0bin0": [ 5093.551 , 148.532 ],
                                             "cut0bin1": [ 3711.272 , 132.462 ],
                                             "cut0bin2": [ 2896.176 , 131.074 ],
                                             "cut0bin3": [ 1838.048 , 105.565 ],
                                             "cut0bin4": [ 1145.826 , 84.085 ],
                                             "cut0bin5": [ 789.461 , 74.749 ],
                                             "cut0bin6": [ 738.353 , 70.557 ],
                                             "cut0bin7": [ 676.385 , 82.891 ],
                                             "cut1bin0": [ 784.718 , 63.748 ],
                                             "cut1bin1": [ 196.48 , 38.178 ],
                                             "cut2bin0": [ 897.291 , 70.238 ],
                                             "cut2bin1": [ 324.989 , 107.75 ]},
                                     'SR' : {"cut0bin0": [ 2730.779 , 97.021 ],
                                             "cut0bin1": [ 2432.161 , 96.798 ],
                                             "cut0bin2": [ 2110.873 , 97.805 ],
                                             "cut0bin3": [ 1564.347 , 87.506 ],
                                             "cut0bin4": [ 1263.017 , 86.125 ],
                                             "cut0bin5": [ 1039.898 , 86.709 ],
                                             "cut0bin6": [ 1135.229 , 91.961 ],
                                             "cut0bin7": [ 967.105 , 91.201 ],
                                             "cut1bin0": [ 444.378 , 44.693 ],
                                             "cut1bin1": [ 527.357 , 81.372 ],
                                             "cut2bin0": [ 660.866 , 54.285 ],
                                             "cut2bin1": [ 366.779 , 59.632 ]} } }

        elif usettbb: #WITH TTBB
            extDDyields = {'2016' : {'VR' : {"cut0bin0": [ 2808.007 , 97.127 ],
                                             "cut0bin1": [ 1943.988 , 83.053 ],
                                             "cut0bin2": [ 1501.231 , 80.421 ],
                                             "cut0bin3": [ 1023.52 , 69.066 ],
                                             "cut0bin4": [ 885.479 , 74.153 ],
                                             "cut0bin5": [ 550.446 , 60.716 ],
                                             "cut0bin6": [ 457.54 , 48.842 ],
                                             "cut0bin7": [ 449.726 , 59.005 ],
                                             "cut1bin0": [ 332.045 , 35.239 ],
                                             "cut1bin1": [ 118.823 , 28.981 ],
                                             "cut2bin0": [ 404.335 , 38.404 ],
                                             "cut2bin1": [ 95.311 , 34.314 ]},
                                     'SR' : {"cut0bin0": [ 1285.315 , 58.978 ],
                                             "cut0bin1": [ 1267.75 , 65.896 ],
                                             "cut0bin2": [ 967.41 , 59.107 ],
                                             "cut0bin3": [ 906.027 , 64.763 ],
                                             "cut0bin4": [ 580.652 , 50.103 ],
                                             "cut0bin5": [ 418.766 , 45.62 ],
                                             "cut0bin6": [ 627.496 , 63.189 ],
                                             "cut0bin7": [ 685.535 , 77.165 ],
                                             "cut1bin0": [ 158.878 , 22.506 ],
                                             "cut1bin1": [ 231.863 , 49.82 ],
                                             "cut2bin0": [ 372.107 , 39.391 ],
                                             "cut2bin1": [ 220.861 , 49.844 ]} },
                           '2017' : {'VR' : {"cut0bin0": [ 3557.454 , 128.828 ],
                                             "cut0bin1": [ 2564.426 , 113.964 ],
                                             "cut0bin2": [ 2098.194 , 116.052 ],
                                             "cut0bin3": [ 1164.903 , 81.539 ],
                                             "cut0bin4": [ 936.459 , 83.946 ],
                                             "cut0bin5": [ 578.911 , 66.254 ],
                                             "cut0bin6": [ 637.243 , 75.851 ],
                                             "cut0bin7": [ 508.168 , 73.809 ],
                                             "cut1bin0": [ 399.574 , 43.366 ],
                                             "cut1bin1": [ 163.141 , 38.919 ],
                                             "cut2bin0": [ 600.293 , 57.887 ],
                                             "cut2bin1": [ 154.547 , 65.863 ]},
                                     'SR' : {"cut0bin0": [ 1788.351 , 78.258 ],
                                             "cut0bin1": [ 1624.404 , 80.543 ],
                                             "cut0bin2": [ 1119.329 , 65.107 ],
                                             "cut0bin3": [ 1062.758 , 73.8 ],
                                             "cut0bin4": [ 645.985 , 55.428 ],
                                             "cut0bin5": [ 458.032 , 48.671 ],
                                             "cut0bin6": [ 628.934 , 65.214 ],
                                             "cut0bin7": [ 559.465 , 65.264 ],
                                             "cut1bin0": [ 259.686 , 34.707 ],
                                             "cut1bin1": [ 336.297 , 66.697 ],
                                             "cut2bin0": [ 479.887 , 48.283 ],
                                             "cut2bin1": [ 288.485 , 61.565 ]}},
                           '2018' : {'VR' : {"cut0bin0": [ 4710.868 , 142.673 ], #no SF
                                             "cut0bin1": [ 3400.815 , 126.273 ],
                                             "cut0bin2": [ 2665.849 , 125.697 ],
                                             "cut0bin3": [ 1685.174 , 100.77 ],
                                             "cut0bin4": [ 1037.504 , 79.295 ],
                                             "cut0bin5": [ 736.565 , 72.538 ],
                                             "cut0bin6": [ 674.482 , 67.213 ],
                                             "cut0bin7": [ 616.77 , 79.721 ],
                                             "cut1bin0": [ 707.94 , 60.607 ],
                                             "cut1bin1": [ 158.656 , 33.84 ],
                                             "cut2bin0": [ 810.409 , 65.518 ],
                                             "cut2bin1": [ 294.322 , 101.006 ]},
                                     'SR' : {"cut0bin0": [ 2443.353 , 91.076 ], #no SF
                                             "cut0bin1": [ 2175.12 , 91.164 ],
                                             "cut0bin2": [ 1871.815 , 91.709 ],
                                             "cut0bin3": [ 1381.048 , 81.875 ],
                                             "cut0bin4": [ 1133.296 , 81.831 ],
                                             "cut0bin5": [ 935.062 , 82.161 ],
                                             "cut0bin6": [ 1011.82 , 86.973 ],
                                             "cut0bin7": [ 857.528 , 86.57 ],
                                             "cut1bin0": [ 379.788 , 40.829 ],
                                             "cut1bin1": [ 439.726 , 75.73 ],
                                             "cut2bin0": [ 553.756 , 48.588 ],
                                             "cut2bin1": [ 302.783 , 53.15 ]} } }
                           # '2018' : {'VR' : {"cut0bin0": [ 4570.17 , 140.544 ], #with SF
                           #                   "cut0bin1": [ 3287.039 , 124.019 ],
                           #                   "cut0bin2": [ 2581.228 , 123.744 ],
                           #                   "cut0bin3": [ 1629.193 , 99.028 ],
                           #                   "cut0bin4": [ 998.022 , 77.55 ],
                           #                   "cut0bin5": [ 717.083 , 71.747 ],
                           #                   "cut0bin6": [ 651.101 , 65.997 ],
                           #                   "cut0bin7": [ 594.814 , 78.586 ],
                           #                   "cut1bin0": [ 679.637 , 59.461 ],
                           #                   "cut1bin1": [ 145.074 , 32.225 ],
                           #                   "cut2bin0": [ 779.021 , 63.806 ],
                           #                   "cut2bin1": [ 283.141 , 98.518 ]}, #with SF
                           #           'SR' : {"cut0bin0": [ 2337.724 , 88.907 ],
                           #                   "cut0bin1": [ 2080.683 , 89.116 ],
                           #                   "cut0bin2": [ 1784.1 , 89.486 ],
                           #                   "cut0bin3": [ 1313.898 , 79.823 ],
                           #                   "cut0bin4": [ 1085.601 , 80.286 ],
                           #                   "cut0bin5": [ 896.564 , 80.507 ],
                           #                   "cut0bin6": [ 966.544 , 85.165 ],
                           #                   "cut0bin7": [ 817.268 , 84.914 ],
                           #                   "cut1bin0": [ 356.207 , 39.411 ],
                           #                   "cut1bin1": [ 407.412 , 73.73 ],
                           #                   "cut2bin0": [ 515.245 , 46.506 ],
                           #                   "cut2bin1": [ 279.847 , 50.772 ]} } }
                                             
        self.extDDyields = extDDyields[year][base]

        if not usettbb: #NO TTBB - OLD
            # VRerr = {'2016':{'cut0bin0':0.111,
            #                  'cut0bin1':0.085,
            #                  'cut0bin2':0.140,
            #                  'cut0bin3':0.132,
            #                  'cut0bin4':0.241,
            #                  'cut0bin5':0.266,
            #                  'cut0bin6':0.146,
            #                  'cut0bin7':0.147,
            #                  'cut1bin0':0.285,
            #                  'cut1bin1':0.268,
            #                  'cut2bin0':0.244,
            #                  'cut2bin1':0.371},
            #          '2017':{'cut0bin0':0.133,
            #                  'cut0bin1':0.089,
            #                  'cut0bin2':0.230,
            #                  'cut0bin3':0.105,
            #                  'cut0bin4':0.207,
            #                  'cut0bin5':0.188,
            #                  'cut0bin6':0.241,
            #                  'cut0bin7':0.254,
            #                  'cut1bin0':0.232,
            #                  'cut1bin1':0.252,
            #                  'cut2bin0':0.149,
            #                  'cut2bin1':0.178},
            #          '2018':{'cut0bin0':0.075,
            #                  'cut0bin1':0.064,
            #                  'cut0bin2':0.117,
            #                  'cut0bin3':0.068,
            #                  'cut0bin4':0.118,
            #                  'cut0bin5':0.075,
            #                  'cut0bin6':0.123,
            #                  'cut0bin7':0.184,
            #                  'cut1bin0':0.218,
            #                  'cut1bin1':0.387,
            #                  'cut2bin0':0.110,
            #                  'cut2bin1':0.355}}

        # if not usettbb: #NO TTBB - NEW
            VRerr = {'2016':{'cut0bin0': 0.124,
                             'cut0bin1': 0.101,
                             'cut0bin2': 0.112,
                             'cut0bin3': 0.105,
                             'cut0bin4': 0.249,
                             'cut0bin5': 0.266,
                             'cut0bin6': 0.145,
                             'cut0bin7': 0.135,
                             'cut1bin0': 0.246,
                             'cut1bin1': 0.264,
                             # 'cut2bin0': 0.55,
                             'cut2bin0': 0.254,
                             'cut2bin1': 0.273},
                     '2017':{'cut0bin0': 0.138,
                             'cut0bin1': 0.087,
                             'cut0bin2': 0.231,
                             'cut0bin3': 0.109,
                             'cut0bin4': 0.207,
                             'cut0bin5': 0.196,
                             'cut0bin6': 0.242,
                             'cut0bin7': 0.250,
                             'cut1bin0': 0.188,
                             'cut1bin1': 0.301,
                             'cut2bin0': 0.179,
                             'cut2bin1': 0.355},
                     '2018':{'cut0bin0': 0.089,
                             'cut0bin1': 0.079,
                             'cut0bin2': 0.121,
                             'cut0bin3': 0.069,
                             'cut0bin4': 0.118,
                             'cut0bin5': 0.071,
                             'cut0bin6': 0.130,
                             'cut0bin7': 0.180,
                             'cut1bin0': 0.203,
                             'cut1bin1': 0.365,
                             'cut2bin0': 0.111,
                             'cut2bin1': 0.351}}

        elif self.usettbb: ##TTBB
            VRerr = {'2016':{'cut0bin0': 0.126,
                             'cut0bin1': 0.162,
                             'cut0bin2': 0.118,
                             'cut0bin3': 0.101,
                             'cut0bin4': 0.241,
                             'cut0bin5': 0.266,
                             'cut0bin6': 0.256,
                             'cut0bin7': 0.192,
                             'cut1bin0': 0.196,
                             'cut1bin1': 0.273,
                             'cut2bin0': 0.180,
                             'cut2bin1': 0.273},
                     '2017':{'cut0bin0': 0.138,
                             'cut0bin1': 0.107,
                             'cut0bin2': 0.223,
                             'cut0bin3': 0.202,
                             'cut0bin4': 0.251,
                             'cut0bin5': 0.227,
                             'cut0bin6': 0.292,
                             'cut0bin7': 0.282,
                             'cut1bin0': 0.171,
                             'cut1bin1': 0.278,
                             'cut2bin0': 0.157,
                             'cut2bin1': 0.165},
                     '2018':{'cut0bin0': 0.113,
                             'cut0bin1': 0.130,
                             'cut0bin2': 0.174,
                             'cut0bin3': 0.126,
                             'cut0bin4': 0.203,
                             'cut0bin5': 0.152,
                             'cut0bin6': 0.117,
                             'cut0bin7': 0.246,
                             'cut1bin0': 0.192,
                             'cut1bin1': 0.396,
                             'cut2bin0': 0.112,
                             'cut2bin1': 0.347}}

        if self.mergehtbins:
            extDDyields = {'2016':{'SR':{'cut0':[7772.67, 171.14],
                                         'cut1':[445.41, 46.33],
                                         'cut2':[762.17, 62.97]},
                                   'VR':{'cut0':[10691.52, 208.88],
                                         'cut1':[521.73, 47.19],
                                         'cut2':[609.67, 51.85]}},
                           '2017':{'SR':{'cut0':[9096.4, 195.68],
                                         'cut1':[666.95, 65.56],
                                         'cut2':[925.37, 74.51]},
                                   'VR':{'cut0':[13220.4, 270.73],
                                         'cut1':[641.17, 58.34],
                                         'cut2':[857.7, 75.01]}},
                           '2018':{'SR':{'cut0':[13232.26, 244.66],
                                         'cut1':[891.81, 73.06],
                                         'cut2':[1044.58, 74.86]},
                                   'VR':{'cut0':[16959.58, 296.74],
                                         'cut1':[1026.77, 75.65],
                                         'cut2':[1154.5, 85.85]}}}

            VRerr = {'2016':{'cut0':0.096,
                             'cut1':0.228,
                             'cut2':0.22},
                     '2017':{'cut0':0.141,
                             'cut1':0.168,
                             'cut2':0.136},
                     '2018':{'cut0':0.067,
                             'cut1':0.150,
                             'cut2':0.123}}
        self.VRerr = VRerr
                                
    def getyield(self, hist, verbose=False):
        errorVal = r.Double(0)
        minbin=0
        maxbin=hist.GetNbinsX()+1
        hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
        if verbose:
            print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
        return hyield,  errorVal
        
    def getsels(self, chanid, CR='D'):
        crdict = {'VR':{'D':['nbjets_nosys>=3 && njets_nosys==8','nbjets_nosys>=3 && njets_nosys==8'],
                        'A':['nbjets_nosys>=3 && njets_nosys==8','nbjets_nosys==2 && njets_nosys==7'],
                        'B':['nbjets_nosys>=3 && njets_nosys==8','nbjets_nosys==2 && njets_nosys==8'],
                        'C':['nbjets_nosys>=3 && njets_nosys==8','nbjets_nosys>=3 && njets_nosys==7'],
                        'X':['nbjets_nosys>=3 && njets_nosys==8','nbjets_nosys==2 && njets_nosys==6'],
                        'Y':['nbjets_nosys>=3 && njets_nosys==8','nbjets_nosys>=3 && njets_nosys==6'],
                    },
                  'SR':{'D':['nbjets_nosys>=3 && njets_nosys>=9','nbjets_nosys>=3 && njets_nosys>=9'],
                        'A':['nbjets_nosys>=3 && njets_nosys>=9','nbjets_nosys==2 && njets_nosys==8'],
                        'B':['nbjets_nosys>=3 && njets_nosys>=9','nbjets_nosys==2 && njets_nosys>=9'],
                        'C':['nbjets_nosys>=3 && njets_nosys>=9','nbjets_nosys>=3 && njets_nosys==8'],
                        'X':['nbjets_nosys>=3 && njets_nosys>=9','nbjets_nosys==2 && njets_nosys==7'],
                        'Y':['nbjets_nosys>=3 && njets_nosys>=9','nbjets_nosys>=3 && njets_nosys==7'],
                    }}
        replstr = crdict[self.base][CR]
        basesel = self.basesel.replace(replstr[0], replstr[1])
        binsel = self.binsels[chanid]
        sel = '('+binsel+' && '
        mcsel = sel + basesel + self.mcstr[self.year]
        datasel = sel + basesel + self.dstr[self.year]+')'

        mysels = {'sel':sel, 'mcsel':mcsel, 'datasel':datasel}
        return mysels
            
    def gethist(self, filename, projvar, cut):
        varbin=self.usevarbin
        nbins, selrange = self.getHistSettings(projvar)
        rf, t = rs.makeroot(filename, self.tn)
        if not varbin:
            hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
        else:
            hist = r.TH1F('hist', 'hist', len(self.edges)-1, self.edges)
        t.Draw(projvar+">>hist", cut)
        rs.fixref(rf, hist)
        return hist

    def newhistfromtree(self, chanid, typ='nom',nonorm=False):
        #typ is nom, med, lo, or hi, min, max
        print 'typ',typ
        path = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/data/NNretrain/'
        bdtvar = 'BDT_disc'
        if typ=='nom':
            if self.usettbb:
                nafdir = self.year+'_ttpure_minorbkgttbb/' #USEttbb
            elif not self.usettbb:
                nafdir = self.year+'_ttinclusive_minorbkg-20220305/'
                # nafdir = self.year+'_ttinclusive_minorbkg/'
            if self.base =='SR':
                binalias = {'cut0bin0':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<800'],
                            'cut0bin1':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=800 && ht<900'],
                            'cut0bin2':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=900 && ht<1000'],
                            'cut0bin3':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1000 && ht<1100'],
                            'cut0bin4':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1100 && ht<1200'],
                            'cut0bin5':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1200 && ht<1300'],
                            'cut0bin6':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1300 && ht<1500'],
                            'cut0bin7':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1500'],
                            'cut1bin0':['240k_SR2.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1400'],
                            'cut1bin1':['240k_SR2.root', 'nJet>=9 && nbJet>=3 && ht>=1400'],
                            'cut2bin0':['240k_SR3.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1100'],
                            'cut2bin1':['240k_SR3.root', 'nJet>=9 && nbJet>=3 && ht>=1100']}# for reading trees
            else:
                binalias = {'cut0bin0':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=700 && ht<800'],
                            'cut0bin1':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=800 && ht<900'],
                            'cut0bin2':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=900 && ht<1000'],
                            'cut0bin3':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=1000 && ht<1100'],
                            'cut0bin4':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=1100 && ht<1200'],
                            'cut0bin5':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=1200 && ht<1300'],
                            'cut0bin6':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=1300 && ht<1500'],
                            'cut0bin7':['240k_VR1.root', 'nJet==8 && nbJet>=3 && ht>=1500'],
                            'cut1bin0':['240k_VR2.root', 'nJet==8 && nbJet>=3 && ht>=700 && ht<1400'],
                            'cut1bin1':['240k_VR2.root', 'nJet==8 && nbJet>=3 && ht>=1400'],
                            'cut2bin0':['240k_VR3.root', 'nJet==8 && nbJet>=3 && ht>=700 && ht<1100'],
                            'cut2bin1':['240k_VR3.root', 'nJet==8 && nbJet>=3 && ht>=1100']}# for reading trees
        elif typ=='med' or typ=='lo' or typ=='hi' or typ=='min' or typ=='max':
            if self.base!='SR':
                print 'WARNING INCOMPATIBLE TEST - VR IN RANDSEED TEST'
            else:
                nafdir = self.year+'SRs_minlomedhimax/'
                binalias = {'cut0bin0':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<800'],
                            'cut0bin1':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=800 && ht<900'],
                            'cut0bin2':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=900 && ht<1000'],
                            'cut0bin3':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1000 && ht<1100'],
                            'cut0bin4':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1100 && ht<1200'],
                            'cut0bin5':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1200 && ht<1300'],
                            'cut0bin6':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1300 && ht<1500'],
                            'cut0bin7':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1500'],
                            'cut1bin0':[self.year+'_240k_SR2_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1400'],
                            'cut1bin1':[self.year+'_240k_SR2_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1400'],
                            'cut2bin0':[self.year+'_240k_SR3_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1100'],
                            'cut2bin1':[self.year+'_240k_SR3_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1100']}# for reading trees  
        path = path+nafdir
        nafname = path+binalias[chanid][0]
        print nafname
        rf, t = rs.makeroot(nafname, 'mytree')
        if not self.usevarbin:
            hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
        else:
            hist = r.TH1F('hist', 'hist', len(self.edges)-1, self.edges)
        t.Draw(bdtvar+">>hist", binalias[chanid][1])
        rs.fixref(rf, hist)

        if not nonorm:
            yld, unc = self.getyield(hist)
            print 'yld',yld
            hist.Sumw2()
            ABCDyld = self.extDDyields[chanid][0]
            ABCDunc = self.extDDyields[chanid][1]
            ABCD_relunc = ABCDunc/ABCDyld
            VR_relunc   = self.VRerr[self.year][chanid]
            # VR_relunc   = self.VRerr[self.year+'_'+chanid]['mean'] #mean now, not total
            hist.Scale(ABCDyld/yld) #scale histogram
            
            #deal with errors properly
            # sqrt[sum_i(abs_error_VR_i^2 + abs_error_abcd_i^2 + stat_unc_bin^2) ]
            for ibin in range(hist.GetNbinsX()+2):
                stat_i = hist.GetBinError(ibin) #statistics of that bin
                histval_i = hist.GetBinContent(ibin) #value of that bin
                err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 )
                if self.addunc: #include VR_err
                    if not self.usettbb:
                        err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 + (VR_relunc*histval_i)**2 )
                    elif self.usettbb:
                        err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 + (VR_relunc*histval_i)**2 + (0.35*histval_i)**2)
                hist.SetBinError(ibin, err)
            
        return hist

    def predshape(self, chanid, uncval=0.01, uncarr=[], newtyp=True):
        path = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/data/NNretrain/'
        # nafdir = self.year+'_ttinclusive_minorbkg/'
        nafdir = self.year+'_ttinclusive_minorbkg-20220305/'
        bdtvar = 'BDT_disc'
        tn = 'mytree'
        binalias = {'cut0bin0':['240k_'+self.base+'1.root',700.0,800.0],
                    'cut0bin1':['240k_'+self.base+'1.root',800.0,900.0],
                    'cut0bin2':['240k_'+self.base+'1.root',900.0,1000.0],
                    'cut0bin3':['240k_'+self.base+'1.root',1000.0,1100.0],
                    'cut0bin4':['240k_'+self.base+'1.root',1100.0,1200.0],
                    'cut0bin5':['240k_'+self.base+'1.root',1200.0,1300.0],
                    'cut0bin6':['240k_'+self.base+'1.root',1300.0,1500.0],
                    'cut0bin7':['240k_'+self.base+'1.root',1500.0,1000000.0],
                    'cut1bin0':['240k_'+self.base+'2.root',700.0,1400.0],
                    'cut1bin1':['240k_'+self.base+'2.root',1400.0,1000000.0],
                    'cut2bin0':['240k_'+self.base+'3.root',700.0,1100.0],
                    'cut2bin1':['240k_'+self.base+'3.root',1100.0,1000000.0]}# for reading trees
        treefile = path+nafdir+binalias[chanid][0]

        if not newtyp:
            tn='Events'
            bdtvar = 'BDT_dsic'
            treefile = self.NAFfilename
            binalias = {'cut0bin0':'0',
                        'cut0bin1':'1',
                        'cut0bin2':'2',
                        'cut0bin3':'3',
                        'cut0bin4':'4',
                        'cut0bin5':'5',
                        'cut0bin6':'6',
                        'cut0bin7':'7',
                        'cut1bin0':'10',
                        'cut1bin1':'11',
                        'cut2bin0':'20',
                        'cut2bin1':'21'}# for reading trees
            mybin = binalias[chanid]
            

        rf, tree = rs.makeroot(treefile, tn)

        histup = r.TH1F('histup', 'histup', len(self.edges)-1, self.edges)
        histdown = r.TH1F('histdown', 'histdown', len(self.edges)-1, self.edges)
        systup = 1.0+uncval  #for set %
        systdown = 1.0-uncval

        for i,ent in enumerate(tree):
            #selection:
            if newtyp: 
                if ( ((ent.nJet>=9 and self.base=='SR') or (ent.nJet==8 and self.base=='VR')) and ent.nbJet>=3 and ent.ht>=binalias[chanid][1] and ent.ht<binalias[chanid][2] and ent.BDT_disc>=0.0 and ent.BDT_disc<=1.0 ):
                    #OLD VERSION#############
                    # #put in 0.8 cutoff
                    # if ent.BDT_disc>=0.6:
                    discup = systup*ent.BDT_disc  
                    discdown = systdown*ent.BDT_disc
                    # else:
                    #     discup = ent.BDT_disc
                    #     discdown = ent.BDT_disc
                    #############################
                    #statcomm version##########
                    # discup   = ent.BDT_disc
                    # discdown = ent.BDT_disc 
                    ###########################

                    if discup>=1.0:
                        discup=1.0
                    elif discup<0.0:
                        discup=0.0
                    if discdown>=1.0:
                        discdown=1.0
                    elif discdown<0.0:
                        discdown=0.0
                    histup.Fill(discup)
                    histdown.Fill(discdown)
                    histup.SetBinContent(10, 0.0) #overflow 
                    histdown.SetBinContent(10, 0.0) #overflow

            elif not newtyp:
                if (str(ent.bin)==mybin and ent.BDT_dsic>=0.0 and ent.BDT_dsic<=1.0) :
                    discup = systup*ent.BDT_dsic  
                    discdown = systdown*ent.BDT_dsic
                    if discup>=1.0:
                        discup=1.0
                    elif discup<0.0:
                        discup=0.0
                    if discdown>=1.0:
                        discdown=1.0
                    elif discdown<0.0:
                        discdown=0.0
                    histup.Fill(discup)
                    histdown.Fill(discdown)
                    histup.SetBinContent(10, 0.0) #overflow 
                    histdown.SetBinContent(10, 0.0) #overflow

        uyld, uunc = self.getyield(histup)
        dyld, dunc = self.getyield(histdown)
        print uyld, dyld
        histup.Sumw2(); histdown.Sumw2()
        ABCDyld = self.extDDyields[chanid][0]
        ABCDunc = self.extDDyields[chanid][1]
        ABCD_relunc = ABCDunc/ABCDyld
        histup.Scale(ABCDyld/uyld) #scale histogram
        histdown.Scale(ABCDyld/dyld) #scale histogram

        #statcomm version##########
        # if len(uncarr)>0:
        #     for b in range(9):
        #         histup.SetBinContent(b+1, histup.GetBinContent(1+b)*(1.0+uncarr[b]))
        #         histdown.SetBinContent(b+1, histdown.GetBinContent(1+b)*(1.00-uncarr[b]))
        #         print b, 1.0+uncarr[b], 1.00-uncarr[b]                
        # else:
        #     histup.SetBinContent(9, histup.GetBinContent(9)*systup)
        #     histdown.SetBinContent(9, histdown.GetBinContent(9)*systdown)
        
        #############################

        histup.SetDirectory(0); histdown.SetDirectory(0)
        tree.ResetBranchAddresses()
        rf.Close()
        print histup, histdown

        # histup.SetDirectory(0)
        # histdown.SetDirectory(0)
        return histup, histdown

    def histfromtree(self, chanid, varname='catBDTdisc_nosys', nonorm=False):
        varbin=self.usevarbin
        binalias = {'cut0bin0':'0',
                    'cut0bin1':'1',
                    'cut0bin2':'2',
                    'cut0bin3':'3',
                    'cut0bin4':'4',
                    'cut0bin5':'5',
                    'cut0bin6':'6',
                    'cut0bin7':'7',
                    'cut1bin0':'10',
                    'cut1bin1':'11',
                    'cut2bin0':'20',
                    'cut2bin1':'21'}# for reading trees

        if self.mergehtbins:
            binalias = {'cut0':'(bin==0 || bin==1 || bin==2 || bin==3 || bin==4 || bin==5 || bin==6 || bin==7)',
                        'cut1':'(bin==10 || bin==11)',
                        'cut2':'(bin==20 || bin==21)'}

        var = 'BDT_dsic'
        treefile = self.NAFfilename
        mybin = binalias[chanid]
        print treefile, 'bin',mybin
        nbins, selrange =  self.getHistSettings(varname)
        if not self.mergehtbins:
            if self.modbins and chanid=='cut2bin0':
                # #need to draw bin 21 into histogram as well if modbins and chan 20!
                cut = '((bin==20 || bin==21) && '+var+'>=0.0 && '+var+'<=1.0)'
            elif self.modbins and chanid=='cut1bin0':
                # #need to draw bin 11 into histogram as well if modbins and chan 10!
                cut = '((bin==10 || bin==11) && '+var+'>=0.0 && '+var+'<=1.0)'
            else:
                cut = '(bin=='+mybin+' && '+var+'>=0.0 && '+var+'<=1.0)'
        elif self.mergehtbins:
            cut = '('+mybin+' && '+var+'>=0.0 && '+var+'<=1.0)'
        print cut
        rf, t = rs.makeroot(treefile, self.tn)
        if not varbin:
            hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
        else:
            hist = r.TH1F('hist', 'hist', len(self.edges)-1, self.edges)
        t.Draw(var+">>hist", cut)
        rs.fixref(rf, hist)

        if not nonorm:
            yld, unc = self.getyield(hist)
            print 'yld:', yld
            hist.Sumw2()
            ABCDyld = self.extDDyields[chanid][0]
            ABCDunc = self.extDDyields[chanid][1]
            ABCD_relunc = ABCDunc/ABCDyld
            VR_relunc   = self.VRerr[self.year][chanid]
            # VR_relunc   = self.VRerr[self.year+'_'+chanid]['mean'] #mean now, not total
            hist.Scale(ABCDyld/yld) #scale histogram
            
            #deal with errors properly
            # sqrt[sum_i(abs_error_VR_i^2 + abs_error_abcd_i^2 + stat_unc_bin^2) ]
            for ibin in range(hist.GetNbinsX()+2):
                stat_i = hist.GetBinError(ibin) #statistics of that bin
                histval_i = hist.GetBinContent(ibin) #value of that bin
                err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 )
                if self.addunc: #include VR_err
                    err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 + (VR_relunc*histval_i)**2 )
                hist.SetBinError(ibin, err)
            
        return hist

        # cutovfl = '(bin=='+mybin+')'
        # histovfl = rs.gethist(t, var, cutovfl, nbins, selrange)
        # histovfl = r.TH1F('histovfl', 'histovfl', nbins, selrange[0], selrange[1])
        # t.Draw(var+">>histovfl", cutovfl)
        # rs.fixref(rf, histovfl)
        # yldovfl, uncovfl = self.getyield(histovfl)
        # print 'yield', yld#, 'overflow', yldovfl-yld

    def getHistSettings(self, var):
        vardict = {
            #[nbins, xmin, xmax]
            'catBDTdisc': [10, 0.0, 1.0],
            'catBDTdisc_manual_nosys': [10, 0.0, 1.0],
            'catBDTdisc_nosys': [10, 0.0, 1.0],
            'catBDTdisc_jerUp': [10, 0.0, 1.0],
            'catBDTdisc_jerDown': [10, 0.0, 1.0],
            'catBDTdisc_jesUp': [10, 0.0, 1.0],
            'catBDTdisc_jesDown': [10, 0.0, 1.0],
            'catBDTdisc_btagLFUp': [10, 0.0, 1.0],
            'catBDTdisc_btagHFUp': [10, 0.0, 1.0],
            'catBDTdisc_btagLFDown': [10, 0.0, 1.0],
            'catBDTdisc_btagHFDown': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats1Up': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats1Up': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats1Down': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats1Down': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats2Up': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats2Up': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats2Down': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats2Down': [10, 0.0, 1.0],
            'ht_nosys'     : [20, 0.0, 2500.0],
            'leadbpt_nosys'     : [20, 0.0, 3000.0],
            'htratio_nosys'   : [50, 0, 1500],
            'bjht_nosys'   : [50, 0, 1500],
            'met_nosys'    : [50, 0, 1000],
            'metovsqrtht_nosys': [50, 0.0, 200.0],
            'npv': [20, 0.0, 100.0],
            'sfjm_nosys'   : [100, 0, 1000],
            'njets_nosys'  : [20, 0, 20],
            'nbjets_nosys' : [10, 0, 10],
            'nbsws_nosys'  : [10, 0, 10],
            'nrestops': [6, 0, 6],
            'nbstops_nosys': [6, 0, 6],
            'nleptons': [20, 0, 20],
            'fwm0_nosys' : [50, 0, 1],
            'fwm1_nosys' : [50, 0, 1],
            'fwm2_nosys' : [50, 0, 1],
            'fwm3_nosys' : [50, 0, 1],
            'fwm4_nosys' : [50, 0, 1],
            'fwm5_nosys' : [50, 0, 1],
            'fwm6_nosys' : [50, 0, 1],
            'fwm7_nosys' : [50, 0, 1],
            'fwm8_nosys' : [50, 0, 1],
            'fwm9_nosys' : [50, 0, 1],
            'fwm10_nosys' : [50, 0, 1],
            'fwm20_nosys' : [50, 0, 1], 
            'fwm30_nosys' : [50, 0, 1],
            # 'topdiscs_nosys' : [50, -1, 1],
            'rescand_disc_nosys' : [50, 0.95, 1],
            'rescand_pt_nosys' : [20, 0.0, 1000],
            'jet1pt_nosys' : [50, 0, 1000],
            'jet2pt_nosys' : [50, 0, 1000],
            'jet7pt_nosys' : [50, 0, 1000],
            'bstop1_pt_nosys' : [50, 100, 1000],
            'bstop2_pt_nosys' : [50, 100, 1000],
            'restop1pt_nosys' : [50, 100, 1000],
            'restop2pt_nosys' : [50, 100, 1000],
            'qglsum_nosys' : [50, 0, 30],
            'qglprod_nosys' : [50, 0, 30],
            'jet8pt_nosys' : [50, 0, 200],
            'aplanarity_nosys' : [20, 0, 1],
            'sphericity_nosys' : [20, 0, 1],
            'centrality_nosys' : [20, 0, 1],
            'dphij1j2_nosys' : [10, -4, 4],
            'detaj1j2_nosys' : [10, -4, 4],
            'dphib1b2_nosys' : [10, -4, 4],
            'detab1b2_nosys' : [10, -4, 4],
            'leadbpt_nosys': [50, 0, 500],
            'npv_nosys': [50, 0, 100],
            'lep1pt_nosys' : [50, 0, 250],
            'lep2pt_nosys' : [50, 0, 250],
            'ModBDT2disc': [100, 0.0, 1.0],
            'evtbdtdisc': [100, 0.0, 1.0],
            'meanbdisc_nosys': [100, 0.0, 1.0],
            'NewBDT3disc': [100, 0.0, 1.0],
            'NewBDT4disc': [100, 0.0, 1.0],
            'Best12disc': [100, 0.0, 1.0],
            'passtrigel': [2, 0,1], 
            'passtrigmu': [2, 0,1]}
        params = vardict[var]
        nbins = params[0]
        selrange = (params[1], params[2])
        if self.usevarbin:
            nbins= len(self.edges)-1
        return nbins, selrange

