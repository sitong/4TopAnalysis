import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--fitfile", dest="fitfile", default='fitDiagnostics_orig_partcorr.root')
parser.add_argument("-v", "--var", dest="var", default='catBDTdisc_nosys')
parser.add_argument("-y", "--year", dest="year", default='2016')
parser.add_argument("-n", "--note", dest="note", default='')
args = parser.parse_args()


class postfit:
    def __init__(self, args):
        self.year = args.year
        self.var = args.var
        self.note = args.note
        self.fitfile = args.fitfile
        self.pd = plotdefs(self.year, 'SR')

        self.colors = {'TTTT':r.TColor.GetColor("#a1794a"),
                       'DDBKG':r.TColor.GetColor("#c1caf3"),
                       'TTX':r.TColor.GetColor("#163d4e"),
                       'ttbb':r.kCyan,
                       # 'TTW':r.kOrange,
                       # 'TTOther':r.kMagenta,
                       'other':r.TColor.GetColor("#d07e93")}

        outdir_Bonly = './plots/postfit/Bonly/'+self.note+self.year+'/'
        outdir_SB = './plots/postfit/SB/'+self.note+self.year+'/'
        if not os.path.exists(outdir_Bonly):
            os.makedirs(outdir_Bonly)
        self.outdir_Bonly = outdir_Bonly
        if not os.path.exists(outdir_SB):
            os.makedirs(outdir_SB)
        self.outdir_SB = outdir_SB

        self.sig = 'TTTT'
        self.data = 'data'
        self.procs = [self.data, self.sig, 'DDBKG', 'TTX', 'other']
        # self.procs = [self.data, self.sig, 'DDBKG', 'TTH','TTW', 'TTOther', 'other']
        self.rf = r.TFile('fitDiagnostics/'+self.fitfile, 'read')
        
    def cardlist(self, year):        
        cardlist = [
            'RT1BT0htbin0_'+year+'_datacard2',
            'RT1BT0htbin1_'+year+'_datacard3',
            'RT1BT0htbin2_'+year+'_datacard0',
            'RT1BT0htbin3_'+year+'_datacard1',
            'RT1BT0htbin4_'+year+'_datacard6',
            'RT1BT0htbin5_'+year+'_datacard7',
            'RT1BT0htbin6_'+year+'_datacard4',
            'RT1BT0htbin7_'+year+'_datacard5',
            'RT1BT1htbin0_'+year+'_datacard9',
            'RT1BT1htbin1_'+year+'_datacard8',
            'RT2BTALLhtbin0_'+year+'_datacard10',
            'RT2BTALLhtbin1_'+year+'_datacard11'
        ]
        return cardlist


    def getgraphs(self, indir, card):
        print '////////////////////////////'
        print indir, card
        print '////////////////////////////'
        graphs = {} 
        for proc in self.procs:
            print proc
            graph = self.rf.Get(indir+'/'+card+'/'+proc)
            if proc!=self.data:
                graph.Sumw2()
                graphs[proc] = graph
                #convert histogram to graph:
                # g = r.TGraphAsymmErrors(graph)
                # graphs[proc] = g
            elif proc ==self.data:
                datahist = r.TH1F('datahist', 'datahist',9, 0.000000, 9.000000)
                print datahist.GetEntries()
                # x = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5]
                N = graph.GetN(); print 'N', N
                X = np.full(N, 0.0, dtype=np.float32)
                Y = np.full(N, 0.0, dtype=np.float32)
                Xerrup = np.full(N, 0.0, dtype=np.float32)
                Yerrup = np.full(N, 0.0, dtype=np.float32)
                Xerrdown = np.full(N, 0.0, dtype=np.float32)
                Yerrdown = np.full(N, 0.0, dtype=np.float32)
                Xerr = np.full(N, 0.0, dtype=np.float32)
                Yerr = np.full(N, 0.0, dtype=np.float32)
                # xbins = [0,1,2,3,4,5,6,7,8,9]
                for i in range (0, N):                    
                    X[i]=graph.GetX()[i]
                    Y[i]=graph.GetY()[i]
                    Xerr[i] = graph.GetErrorX(i)
                    Xerrup[i] = graph.GetErrorXhigh(i)
                    Xerrdown[i] = graph.GetErrorXlow(i)
                    Yerr[i] = graph.GetErrorY(i)
                    Yerrup[i] = graph.GetErrorYhigh(i)
                    Yerrdown[i] = graph.GetErrorYlow(i)
                    print i, X[i], Y[i]
                    # Print 'xerr', Xerr[i], Xerrup[i], Xerrdown[i]
                    print 'yerr', Yerr[i], Yerrup[i], Yerrdown[i]
                    datahist.SetBinContent(i+1, Y[i])
                    datahist.SetBinError(i+1, Yerr[i])
                datahist.Sumw2()
                graphs[proc] = datahist 
                # graphs[proc] = graph 
        #     print 'self.rf.Get('+indir+'/'+card+'/'+proc+')'
        # print graphs
        return graphs

    def plot_settings(self, indir, histname, plots):
        #top plot
        c = r.TCanvas("c","c",1000,1000)
        p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        p1.SetBottomMargin(0.1)
        p1.Draw()
        p1.cd()

        stack = r.THStack("stack", "stack")
        legend = r.TLegend(0.1, 0.6, 0.3, 0.9)
        # stacktot = r.TH1F('stacktot', 'stacktot', len(self.pd.edges)-1, self.pd.edges)
        stacktot = r.TH1F('stacktot', 'stacktot',9, 0.000000, 9.000000)
        hcount = 0; hnames = []
        for proc, plot in plots.iteritems():
            if proc == self.data:
                datahist = plot
                datahist.SetLineWidth(2)
                datahist.SetMarkerStyle(20)
                datahist.SetLineColor(r.kGray+2)
                legend.AddEntry(datahist, "data", "L")           
                
            elif proc!=self.data:
                plot.SetLineWidth(1)
                plot.SetFillColor(self.colors[proc])
                plot.SetLineColor(self.colors[proc])
                if not (indir=='shapes_fit_b' and proc=='TTTT'):
                    stack.Add(plot)
                    stacktot.Add(plot)
                hnames.append(proc)
                legend.AddEntry(plot, hnames[hcount], "F")           
                hcount+=1
                
        #draw stack
        stack.SetTitle("")
        stack.Draw("hist")
        gPad.Modified()
        gPad.Update()
        ymax = p1.GetUymax()*1.6 #max+30%
        stack.SetMaximum(ymax)

        datahist.Draw("psame")

        # stacktot = r.TGraphAsymmErrors(stacktot)
        stacktot.SetLineWidth(0) #dont show line
        stacktot.SetFillColor(r.kGray+2)
        stacktot.SetFillStyle(3002)
        stacktot.Draw("e2same")

        stack.GetYaxis().SetTitleSize(20)
        stack.GetYaxis().SetTitleFont(43)
        stack.GetYaxis().SetTitleOffset(1.55)
        stack.GetYaxis().SetTitle('NEntries')
        stack.GetXaxis().SetTitle('BDT discriminant')
        stack.GetYaxis().SetLabelFont(43)
        stack.GetYaxis().SetLabelSize(20)

        gPad.Modified()
        gPad.Update()

        legend.Draw()
        gPad.Modified()
        gPad.Update()

        #bottom plot
        c.cd()
        p2 = r.TPad("p2", "p2", 0, 0.05, 1, 0.3)
        p2.SetGridy()
        p2.Draw()
        p2.cd()

        ratio = datahist.Clone("ratio")
        ratio.SetLineColor(r.kBlack)
        ratio.Divide(stacktot) 
        ratio.SetMarkerStyle(20)

        raterr = datahist.Clone("raterr")
        raterr.SetLineWidth(0)
        raterr.SetFillColor(r.kGray+2)
        raterr.SetFillStyle(3002)
        raterr.SetMarkerStyle(1)
        raterr.SetMinimum(0.4)
        raterr.SetMaximum(1.6)

        #error bars
        for ibin in range(0, ratio.GetNbinsX()+2):
            derr = datahist.GetBinError(ibin)
            d = datahist.GetBinContent(ibin)
            relderr=0.0
            if d>0.0: relderr=derr/d

            err = stacktot.GetBinError(ibin)
            s = stacktot.GetBinContent(ibin)
            relerr=0.0
            if s>0:
                relerr = err/s

            raterr.SetBinError(ibin, relerr)
            ratio.SetBinError(ibin, relderr)
            raterr.SetBinContent(ibin, 1)
        
        #draw
        raterr.Draw("e2")
        ratio.Draw("pe0same")
        l = r.TLine(p2.GetUxmin(), 1.0, p2.GetUxmax(), 1.0);
        l.SetLineColor(r.kBlack)
        l.SetLineWidth(1)
        l.Draw("same")

        raterr.SetTitle("")
        raterr.GetYaxis().SetTitle("ratio data/prediction")
        raterr.GetYaxis().SetNdivisions(505)
        raterr.GetYaxis().SetTitleSize(20)
        raterr.GetYaxis().SetTitleFont(43)
        raterr.GetYaxis().SetTitleOffset(1.55)
        raterr.GetYaxis().SetLabelFont(43)
        raterr.GetYaxis().SetLabelSize(20)
        raterr.GetXaxis().SetTitle("BDT discriminant")
        raterr.GetXaxis().SetTitleSize(20)
        raterr.GetXaxis().SetTitleFont(43)
        raterr.GetXaxis().SetTitleOffset(4.0)
        raterr.GetXaxis().SetLabelFont(43)
        raterr.GetXaxis().SetLabelSize(20)

        p2.SetTopMargin(0.1)
        p2.SetBottomMargin(0.2)
        gPad.Modified()
        gPad.Update()
        
        outdir = self.outdir_Bonly
        if indir == 'shapes_fit_s':
            outdir = self.outdir_SB
        
        print 'saved histogram: ', outdir+histname+'.png'
        c.SaveAs(outdir+histname+'.png')        
        c.SaveAs(outdir+histname+'.C')        

    def rungraphs(self):
        Sdir = 'shapes_fit_s'
        Bdir = 'shapes_fit_b'
        cardlist = self.cardlist(self.year)
        # for indir in [Sdir]:
        for indir in [Bdir, Sdir]:
            for card in cardlist:
                graphs = self.getgraphs(indir, card)
                self.plot_settings(indir, card, graphs)
            r.gROOT.Reset()
        #save
        self.rf.Close()

#run the thing
pf = postfit(args)
pf.rungraphs()
