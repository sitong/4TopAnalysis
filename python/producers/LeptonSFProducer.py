# translated from 4TopAnalysis/AnalysisBase/TreeAnalyzer/src/LeptonCorrectionSet.cc
import ROOT
import os
import math
import json
import logging
import numpy as np
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper
from PhysicsTools.NanoTrees.helpers.ConfigHelper import LepSetter
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import closest

#class definition
class LeptonSFProducer(Module):
    def __init__(self, channel, year):
        self.verbose = False
        self.extras  = False

        #get settings from ConfigHelper:
        self.LS=LepSetter(channel, year)
        print self.LS.mu_pt, self.LS.mu_eta, self.LS.mu_ID, self.LS.mu_isotype, self.LS.mu_isoval, self.LS.el_pt, self.LS.el_eta, self.LS.el_ID        

        self.nselleps = 0
        if channel=='1lep':
            self.nselleps = 1 
        self.year = year
        self.elidbin = self.LS.el_ID 
        self.muid = self.LS.mu_ID #Medium, Loose, or Tight
        self.muiso = 'Tight'
        if self.LS.mu_isoval>0.2: # for 1lep=0.15 and 0lep=0.25, if use mini alter this
            self.muiso = 'Loose' #Tight or Loose
        elif self.LS.mu_isoval<0.2:
            self.muiso = 'Tight' #Tight or Loose

        self.muisoid = self.LS.mu_ID  #id used in iso. Medim, Loose, or Tight. set to be the same as the ID
        if self.muisoid == 'Tight':
            self.muisoid += 'IDandIPCut'
        else:
            self.muisoid +='ID'

        self.elidname = 'Tight' #Loose, Tight, Medium, or Veto
        if self.LS.el_ID == 4:
            self.elidname = self.year+'LegacyReReco_ElectronTight'
        elif self.LS.el_ID == 1:
            self.elidname = self.year+'_ElectronWPVeto' #fill in the rest0:fail, 1:veto, 2:loose, 3:medium, 4:tight0

        #load the files:
        print "LOADING HISTOGRAMS>>>>>"
        self._fileElSf_id        = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/LeptonSFFiles/'+self.year+'/'+self.elidname+'_Fall17V2.root')
        self._fileMuSf_id_BCDEF  = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/LeptonSFFiles/2016/RunBCDEF_SF_ID.root')
        self._fileMuSf_id_GH     = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/LeptonSFFiles/2016/RunGH_SF_ID.root')
        self._fileMuSf_iso_BCDEF = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/LeptonSFFiles/2016/RunBCDEF_SF_ISO.root')
        self._fileMuSf_iso_GH    = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/LeptonSFFiles/2016/RunGH_SF_ISO.root')
        self._fileLepSf_effs      = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/LeptonSFFiles/2016/lepCorMCEff_HM.root')
        #open root files:
        self._fileElSf_id        = ROOT.TFile(self._fileElSf_id)
        self._fileMuSf_id_BCDEF  = ROOT.TFile(self._fileMuSf_id_BCDEF)
        self._fileMuSf_iso_BCDEF = ROOT.TFile(self._fileMuSf_iso_BCDEF)
        self._fileMuSf_id_GH     = ROOT.TFile(self._fileMuSf_id_GH)
        self._fileMuSf_iso_GH    = ROOT.TFile(self._fileMuSf_iso_GH)
        self._fileLepSf_effs     = ROOT.TFile(self._fileLepSf_effs)
        #get histograms
        self.muhist_id_BCDEF      = self._fileMuSf_id_BCDEF.Get("NUM_"+self.muid+"ID_DEN_genTracks_eta_pt")
        self.muhist_iso_BCDEF     = self._fileMuSf_iso_BCDEF.Get("NUM_"+self.muiso+"RelIso_DEN_"+self.muisoid+"_eta_pt")
        self.muhist_id_GH         = self._fileMuSf_id_GH.Get("NUM_"+self.muid+"ID_DEN_genTracks_eta_pt")
        self.muhist_iso_GH        = self._fileMuSf_iso_GH.Get("NUM_"+self.muiso+"RelIso_DEN_"+self.muisoid+"_eta_pt")
        self.muhist_effs_id       = self._fileLepSf_effs.Get("lepCorMCEff_Mu_Id_CR")
        self.muhist_effs_iso      = self._fileLepSf_effs.Get("lepCorMCEff_Mu_Iso_CR")
        self.elhist_id           = self._fileElSf_id.Get("EGamma_SF2D")
        self.elhist_effs_id      = self._fileLepSf_effs.Get("lepCorMCEff_El_Id_CR")
        self.elhist_effs_iso     = self._fileLepSf_effs.Get("lepCorMCEff_El_Iso_CR") 
        ##because root is being a little poopyhead about memory leaks in the histograms
        self.muhist_id_BCDEF.SetDirectory(0)
        self.muhist_iso_BCDEF.SetDirectory(0)
        self.muhist_id_GH.SetDirectory(0)
        self.muhist_iso_GH.SetDirectory(0)
        self.muhist_effs_id.SetDirectory(0)
        self.muhist_effs_iso.SetDirectory(0)
        self.elhist_id.SetDirectory(0)
        self.elhist_effs_id.SetDirectory(0)
        self.elhist_effs_iso.SetDirectory(0)
        self._fileElSf_id.Close()
        self._fileMuSf_id_BCDEF.Close()
        self._fileMuSf_iso_BCDEF.Close()
        self._fileMuSf_id_GH.Close()
        self._fileMuSf_iso_GH.Close()
        self._fileLepSf_effs.Close()

    def beginJob(self):
        self.genhelp = genparthelper()

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.isMC = bool(inputTree .GetBranch('genWeight'))
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree

        self.out.branch("singleelectron_SelSF",       "F")
        self.out.branch("singleelectron_SelSF_upper", "F")
        self.out.branch("singleelectron_SelSF_lower", "F")
        self.out.branch("singlemuon_SelSF",           "F")
        self.out.branch("singlemuon_SelSF_upper",     "F")
        self.out.branch("singlemuon_SelSF_lower",     "F")

        self.out.branch("muon_evtSelSF",        "F")
        self.out.branch("electron_evtSelSF",    "F")

        self.out.branch("lep_evtVetoSF",   "F")
        self.out.branch("lep_evtSelSF",    "F")

        self.out.branch("muon_VetoSF",           "F")
        self.out.branch("muon_VetoSF_upper",     "F")
        self.out.branch("muon_VetoSF_lower",     "F")
        self.out.branch("electron_VetoSF",       "F")
        self.out.branch("electron_VetoSF_upper", "F")
        self.out.branch("electron_VetoSF_lower", "F")

        self.out.branch("nleps",    "I")
        self.out.branch("nmus",    "I")
        self.out.branch("nels",    "I")

        self.out.branch("ngenmus",    "I")
        self.out.branch("ngenels",    "I")
        self.out.branch("ngenleps",    "I")
        self.out.branch("ngenmatchmus",    "I")
        self.out.branch("ngenmatchels",    "I")
        
        #extras
        if self.extras:
            self.out.branch("e_pt",    "F", lenVar="nElectron")
            self.out.branch("e_eta",   "F", lenVar="nElectron")
            self.out.branch("e_SF",    "F", lenVar="nElectron")
            self.out.branch("mu_pt",    "F", lenVar="nMuon")
            self.out.branch("mu_eta",   "F", lenVar="nMuon")
            self.out.branch("gmu_pt",    "F", lenVar="nMuon")
            self.out.branch("gmu_eta",   "F", lenVar="nMuon")
            self.out.branch("gmu_SF",    "F", lenVar="nMuon")
            self.out.branch("mu_SF",    "F", lenVar="nMuon")


    def get_avg_sf(self, hist_RunBtoF, hist_RunGH, var1, var2, syst=None):
        fracBCDEF, fracGH = 0.55, 0.45 #check these. normalized to lumi
        ret = fracBCDEF * self.get_sf(hist_RunBtoF, var1, var2, None) + fracGH * self.get_sf(hist_RunGH, var1, var2, None)
        if syst is not None:
            stat   = ret[1] - ret[0]
            unc    = np.sqrt(syst*syst+stat*stat)
            ret[1] = ret[0] + unc
            ret[2] = ret[0] - unc
        else:
            unc    = ret[1] - ret[0]
            ret[1] = ret[0] + unc
            ret[2] = ret[0] - unc
        return ret

    def get_sf(self, hist, var1, var2, syst=None):
        # `eta` refers to the first binning var, `pt` refers to the second binning var
        eta, pt = var1, var2
        SF = np.ones(3)
        SF = self.getBinContent2D(hist, pt, eta)
        if syst is not None:
            stat = SF[1] - SF[0]
            unc  = np.sqrt(syst*syst+stat*stat)
            SF[1] = SF[0] + unc
            SF[2] = SF[0] - unc
        return SF

    def getBinContent2D(self, hist, x, y):
        bin_x0 = hist.GetXaxis().FindFixBin(x)
        bin_y0 = hist.GetYaxis().FindFixBin(y)
        binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
        binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
        if binX>hist.GetNbinsX(): ##? set to last bin
            binX=hist.GetNbinsX()
        if binY>hist.GetNbinsY():
            binY=hist.GetNbinsY()
        val = hist.GetBinContent(binX, binY)
        # print 'val', val, 'binX', binX, 'binY', binY, 'last bin:', hist.GetNbinsX(), hist.GetNbinsY()
        err = hist.GetBinError(binX, binY)
        return np.array([val, val + err, val - err])
    
    # gen match leptons
    def FindGenLeps(self, GenParts):
        gen_mus = []
        gen_els = []
        ngtaus=0; ngmus=0; ngels=0
        for gp in GenParts:
            #find electrons and muons: try this way
            momidx = gp.genPartIdxMother
            if momidx>=0:
                mom = GenParts[momidx]
                if (mom.pdgId==23 or abs(mom.pdgId)==24): #Ws
                    if abs(gp.pdgId)==11:
                        gen_els.append(gp)
                        ngels+=1
                    elif abs(gp.pdgId)==13:
                        gen_mus.append(gp)
                        ngmus+=1
                    elif abs(gp.pdgId)==15:
                        ngtaus+=1

                elif abs(mom.pdgId)==15: #taus
                    if abs(gp.pdgId)==11:
                        gen_els.append(gp)
                    elif abs(gp.pdgId)==13:
                        gen_mus.append(gp)

        ngenleps=ngels+ngmus+ngtaus #only count those coming from the W
        return np.array(gen_els), np.array(gen_mus), ngenleps

    #get lepton collection, id genmus and genels
    def getSelectedLeptons(self, event):
        if not self.isMC:
            self.out.fillBranch("electron_evtSelSF",  1.0)
            self.out.fillBranch("muon_evtSelSF",      1.0)
            
            self.out.fillBranch("singlemuon_SelSF",       1.0)
            self.out.fillBranch("singlemuon_SelSF_upper", 1.0)
            self.out.fillBranch("singlemuon_SelSF_lower", 1.0)
            
            self.out.fillBranch("singleelectron_SelSF",       1.0)
            self.out.fillBranch("singleelectron_SelSF_upper", 1.0)
            self.out.fillBranch("singleelectron_SelSF_lower", 1.0)

            self.out.fillBranch("lep_evtSelSF",    1.0)
            self.out.fillBranch("lep_evtVetoSF",   1.0)

            self.out.fillBranch("muon_VetoSF",       1.0)
            self.out.fillBranch("muon_VetoSF_upper", 1.0)
            self.out.fillBranch("muon_VetoSF_lower", 1.0)
            self.out.fillBranch("electron_VetoSF",       1.0)
            self.out.fillBranch("electron_VetoSF_upper", 1.0)
            self.out.fillBranch("electron_VetoSF_lower", 1.0)
            return False

        Mus      = Collection(event, "Muon")
        Els      = Collection(event, "Electron")
        GenParts = Collection(event, "GenPart")
        # GenParts = self.genhelp.IndexDaughters(genparts) #add dauIdxes for gen matching
        genEls, genMus, Ngenleps = self.FindGenLeps(GenParts)
        
        #get collection of unmatched muons to get weights for failed muon ids:
        gentestmus = []
        if genMus.size > 0:
            for gmu in genMus:
                gen, dr = closest(gmu, Mus)
                if dr > 0.4 and abs(gmu.pdgId)==13: # so not close to any muons
                    gentestmus.append(gmu)
                # elif dr <= 0.4:
                #     if self.verbose: print 'gmu rejected due to radius of ', dr
                # elif abs(gmu.pdgid)!=13:
                #     if self.verbose: print 'gmu rejected due to pdgid of', gmu.pdgid
        genTestMus = np.array(gentestmus)

        #have array of scale factors for each lepton
        muon1lepSelSF     = 1.0; muon1lepSelSF_upper     = 1.0; muon1lepSelSF_lower     = 1.0 
        electron1lepSelSF = 1.0; electron1lepSelSF_upper = 1.0; electron1lepSelSF_lower = 1.0
        muonSelEvtSF      = 1.0; electronSelEvtSF        = 1.0 
        muonVetoSF        = 1.0; muonVetoSF_upper        = 1.0; muonVetoSF_lower        = 1.0
        electronVetoSF    = 1.0; electronVetoSF_upper    = 1.0; electronVetoSF_lower    = 1.0
        muonVetoERR=[]; electronVetoERR=[]
        if self.extras:
            self.elpts=np.zeros(event.nElectron); self.eletas=np.zeros(event.nElectron); self.elSF=np.zeros(event.nElectron)
            self.mupts=np.zeros(event.nMuon); self.muetas=np.zeros(event.nMuon); self.muSF=np.zeros(event.nMuon)
            self.gmupts=np.zeros(event.nGenPart); self.gmuetas=np.zeros(event.nGenPart); self.gmuSF=np.zeros(event.nGenPart)

        # print 'nmus:', len(Mus)
        # print 'nels:', len(Els)
        nselmus=0; nselels=0; nleps=0; nmatchmus=0; nmatchels=0
        for im, mu in enumerate(Mus):
            MuVetoSF=np.ones(2) #set to 1 if fails selection
            MuSelSF=np.ones(2)
            if (mu.pt>=self.LS.mu_pt and abs(mu.eta)<self.LS.mu_eta and abs(mu.pdgId)==13): #allow antimuons? #include num muons?
                passId = False; passIso=False
                if ((self.LS.mu_isotype=='pf4' and  mu.pfRelIso04_all<self.LS.mu_isoval) or (self.LS.mu_isotype=='mini' and  mu.miniPFRelIso_all<self.LS.mu_isoval)):
                    passIso=True
                if ((self.LS.mu_ID=='Loose'and mu.looseId) or (self.LS.mu_ID=='Medium' and mu.mediumId) or (self.LS.mu_ID=='Tight' and mu.tightId)):
                    passId=True
                # if self.verbose: print 'MU','mu#',im,'passId?', passId, 'passIso', passIso, 'tightid?', mu.tightId,'iso', mu.pfRelIso04_all,  'mu pt', mu.pt, 'mu eta', mu.eta
                if passIso and passId:
                    nselmus+=1; nleps+=1
                    if self.nselleps>0:
                        MuSelSF, MuVetoSF, nmms, nmes = self.calc_sfs(mu, im, genMus, passId, passIso, typ=13, sellep=True)
                        if nselmus<=self.nselleps: #only calculate for sel leptons, for one selected muon
                            # if self.verbose: print 'setting sf for muon', im
                            muonSelEvtSF   *=MuSelSF[0]
                            muon1lepSelSF       =MuSelSF[0]
                            muon1lepSelSF_upper =MuSelSF[0]+MuSelSF[1]
                            muon1lepSelSF_lower =MuSelSF[0]-MuSelSF[1]
                        nmatchmus+=nmms; nmatchels+=nmes
                elif (not passIso and not passId) or (not passIso and passId) or (passIso and not passId):
                    if self.nselleps==0:
                        MuSelSF, MuVetoSF, nmms, nmes = self.calc_sfs(mu, im, genMus, passId, passIso, typ=13, sellep=False)
                        if MuVetoSF[0]!=1 and self.verbose:
                            print 'muon', im, 'muvetosf', MuVetoSF[0], 'passid', passId, 'passiso',passIso, 'tightid?', mu.tightId,'iso', mu.pfRelIso04_all,  'mu pt', mu.pt, 'mu eta', mu.eta
                        muonVetoSF      *= MuVetoSF[0]
                        muonVetoERR.append(MuVetoSF[1])
                        # muonVetoSF_upper*= MuVetoSF[1]
                        # muonVetoSF_lower*= MuVetoSF[2]
                        # if MuVetoSF[1]<0:
                        #     print 'mu'. MuVetoSF, passIso, passId
                        nmatchmus+=nmms; nmatchels+=nmes            
                
        if self.nselleps==0: #only need this for veto selection
            for ig, gmu in enumerate(genTestMus):
                #needs to pass pt and eta cut
                passId = False
                if (gmu.pt>self.LS.mu_pt and abs(gmu.eta)<self.LS.mu_eta): 
                    # if self.verbose: print 'genmuon', ig
                    MuVetoSF=np.ones(2)
                    MuSelSF, MuVetoSF, nmms, nmes = self.calc_sfs(gmu, ig, genMus, False, False, typ=13, sellep=False, genmu=True)
                    if MuVetoSF[0]!=1 and self.verbose:
                        print 'GENmuon', ig, 'muvetosf', MuVetoSF[0], 'pt', gmu.pt, 'eta', gmu.eta
                    muonVetoSF      *= MuVetoSF[0]
                    muonVetoERR.append(MuVetoSF[1])
                    # muonVetoSF_upper*= MuVetoSF[1]
                    # muonVetoSF_lower*= MuVetoSF[2]

        for ie, el in enumerate(Els):
            ElVetoSF=np.ones(2) #set to 1 if fails selection
            ElSelSF=np.ones(2)
            if (el.pt>=self.LS.el_pt and abs(el.eta)<self.LS.el_eta and abs(el.pdgId)==11): #allows positrons
                passId = False; passIso=True #no iso for electrons, just has to pass presel
                if el.cutBased>=self.elidbin:
                    passId=True # id
                # if self.verbose: print 'EL', 'el#',ie, 'passId?', passId, 'id', el.cutBased, 'el pt', el.pt, 'el eta', el.eta
                if passId:
                    nselels+=1; nleps+=1
                    if self.nselleps>0:  #only care it passes if selecting leptons
                        ElSelSF, ElVetoSF, nmms, nmes = self.calc_sfs(el, ie, genEls, passId, passIso, typ=11, sellep=True)
                        if nselels<=self.nselleps: #only for one selected electron, cant be zero
                            # if self.verbose: print 'setting sf for electron', ie
                            electronSelEvtSF  *=ElSelSF[0]
                            electron1lepSelSF      =ElSelSF[0]
                            electron1lepSelSF_upper=ElSelSF[0]+ElSelSF[1]
                            electron1lepSelSF_lower=ElSelSF[0]-ElSelSF[1]
                        nmatchmus+=nmms; nmatchels+=nmes            
                elif not passId:            
                    if self.nselleps==0: #only care it doesnt pass if not selecting leptons
                        ElSelSF, ElVetoSF, nmms, nmes = self.calc_sfs(el, ie, genEls, passId, passIso, typ=11, sellep=False)
                        # if ElVetoSF[0]!=1 and self.verbose:
                        #     print 'electron', ie, 'elvetosf', ElVetoSF[0], 'passid', passId, 'passiso',passIso
                        electronVetoSF      *= ElVetoSF[0]
                        electronVetoERR.append(ElVetoSF[1])
                        # electronVetoSF_upper*= ElVetoSF[1]
                        # electronVetoSF_lower*= ElVetoSF[2]
                        # if ElVetoSF[1]<0:
                            # print 'el', ElVetoSF, passId
                        nmatchmus+=nmms; nmatchels+=nmes            
        #vetosf should be 1 if there is a selected lepton: make sure
        if nselels>0:
            # if self.verbose: print 'nselels>0: reset veto sf to 1'
            electronVetoSF=1.0; muonVetoSF=1.0
        #vetosf should be 1 if there is a selected lepton: make sure
        if nselmus>0:
            # if self.verbose: print 'nselmus>0: reset veto sf to 1'
            muonVetoSF=1.0; electronVetoSF=1.0

        muERR=0.0; elERR=0.0
        for err in muonVetoERR:
            muERR += err*err
        for err in electronVetoERR:
            elERR += err*err
        muERR = math.sqrt(muERR); elERR = math.sqrt(elERR)
        muonVetoSF_upper = muonVetoSF + muERR
        muonVetoSF_lower = muonVetoSF - muERR
        electronVetoSF_upper = electronVetoSF + muERR
        electronVetoSF_lower = electronVetoSF - muERR

        # if nleps!=self.nselleps: #set to 1 if doesn't match lepton selection 
        if self.nselleps==0 and nleps!=self.nselleps: #set to 1 if doesn't match lepton selection MODIFIED so that this only applies to 0lep vs NOT 0lep
            #so if nselleps==1 these will NOT be reset!
            # if self.verbose: print 'too many leptons! reset to 1'
            electronSelEvtSF = electron1lepSelSF = electron1lepSelSF_lower = electron1lepSelSF_upper = 1.0
            muonSelEvtSF = muon1lepSelSF = muon1lepSelSF_lower = muon1lepSelSF_upper = 1.0

        # if self.verbose: print 'el 1lep SF:', electron1lepSelSF, 'el event SF:', electronSelEvtSF
        # if self.verbose: print 'mu 1lep SF:', muon1lepSelSF, 'mu event SF:', muonSelEvtSF

        # cap at +/-2.0
        if electronSelEvtSF>2.0: electronSelEvtSF=2.0
        if muonSelEvtSF>2.0: muonSelEvtSF=2.0

        if electron1lepSelSF>2.0: electron1lepSelSF=2.0
        if electron1lepSelSF_upper>2.0: electron1lepSelSF_upper=2.0
        if electron1lepSelSF_lower>2.0: electron1lepSelSF_lower=2.0
        if muon1lepSelSF>2.0: muon1lepSelSF=2.0
        if muon1lepSelSF_upper>2.0: muon1lepSelSF_upper=2.0
        if muon1lepSelSF_lower>2.0: muon1lepSelSF_lower=2.0

        if electronVetoSF>2.0: electronVetoSF=2.0
        if electronVetoSF_upper>2.0: electronVetoSF_upper=2.0
        if electronVetoSF_lower>2.0: electronVetoSF_lower=2.0
        if muonVetoSF>2.0: muonVetoSF=2.0
        if muonVetoSF_upper>2.0: muonVetoSF_upper=2.0
        if muonVetoSF_lower>2.0: muonVetoSF_lower=2.0

        if self.verbose:
            if muonVetoSF!=1.0: #or electronVetoSF!=1.0:
                print '#######event', event.event                
        #         print 'EVTveto electron sf', electronVetoSF
                print 'EVTveto muon     sf', muonVetoSF
                print 'nsellep', nleps
        #         print 'EVTsel electron sf', electronSelEvtSF
        #         print 'EVTsel muon     sf', muonSelEvtSF
        #         print '\n'
        # print muonVetoSF, electronVetoSF
        self.out.fillBranch("electron_evtSelSF",    electronSelEvtSF) #for leptons matching selection only
        self.out.fillBranch("muon_evtSelSF",        muonSelEvtSF)

        self.out.fillBranch("singlemuon_SelSF",       muon1lepSelSF) #for 1lep only
        self.out.fillBranch("singlemuon_SelSF_upper", muon1lepSelSF_upper)
        self.out.fillBranch("singlemuon_SelSF_lower", muon1lepSelSF_lower)

        self.out.fillBranch("singleelectron_SelSF",       electron1lepSelSF) #for 1lep only
        self.out.fillBranch("singleelectron_SelSF_upper", electron1lepSelSF_upper)
        self.out.fillBranch("singleelectron_SelSF_lower", electron1lepSelSF_lower)

        self.out.fillBranch("lep_evtSelSF",    electronSelEvtSF*muonSelEvtSF)#for leptons matching selection only
        self.out.fillBranch("lep_evtVetoSF",   electronVetoSF*muonVetoSF)

        self.out.fillBranch("muon_VetoSF",       muonVetoSF)
        self.out.fillBranch("muon_VetoSF_upper", muonVetoSF_upper)
        self.out.fillBranch("muon_VetoSF_lower", muonVetoSF_lower)
        self.out.fillBranch("electron_VetoSF",       electronVetoSF)
        self.out.fillBranch("electron_VetoSF_upper", electronVetoSF_upper)
        self.out.fillBranch("electron_VetoSF_lower", electronVetoSF_lower)
 
        self.out.fillBranch("ngenleps", Ngenleps )
        self.out.fillBranch("ngenmus", len(genMus) )
        self.out.fillBranch("ngenels", len(genEls) )
         
        #extras:
        if self.extras: #by lepton
            self.out.fillBranch("nleps", nleps  )
            self.out.fillBranch("nmus", nselmus  )
            self.out.fillBranch("nels", nselels  )

            self.out.fillBranch("ngenmatchmus", nmatchmus  )
            self.out.fillBranch("ngenmatchels", nmatchels  )

            self.out.fillBranch("e_pt",  list(self.elpts) )
            self.out.fillBranch("e_eta", list(self.eletas) )
            self.out.fillBranch("e_SF",  list(self.elSF) )
            self.out.fillBranch("mu_pt",  list(self.mupts) )
            self.out.fillBranch("mu_eta", list(self.muetas) )
            self.out.fillBranch("gmu_pt",  list(self.gmupts) )
            self.out.fillBranch("gmu_eta", list(self.gmuetas) )
            self.out.fillBranch("gmu_SF",  list(self.gmuSF) )
            self.out.fillBranch("mu_SF",  list(self.muSF) )
        return True

    #get those weights!
    def calc_sfs(self, lep, ilep, genleps, passId, passIso, typ=13, sellep=False, genmu=False): #done per lepton, typ is 11 or 13 for el or muon
        pt, eta = lep.pt, lep.eta
        abseta = abs(lep.eta)
        supereta = lep.eta
        SelEventWgt = np.ones(2)
        VetoEventWgt = np.ones(2)
        sfID = np.ones(3); sfISO = np.ones(3)
        sfMCeffID = np.ones(3); sfMCeffISO = np.ones(3)

        nmatchedels=0; nmatchedmus=0
        # electrons
        if typ==11:
            supereta = lep.eta + lep.deltaEtaSC
            # print 'Electron!'
            gen, dr = closest(lep, genleps)             #gen matching
            if dr <= 0.4:
                # if self.verbose: print 'matched!'
                nmatchedels+=1
                sfID = self.get_sf(self.elhist_id, pt, supereta)
            # calc overall scale factor using iso and id:
                if passId:
                    if sellep:   #pass id and iso and sel SF
                        SelEventWgt[0] = sfID[0]
                        SelEventWgt[1] = sfID[1]-sfID[0]
                        # if self.verbose: print 'selel:', 'sfID', sfID, 'SF', SelEventWgt
                        if self.extras: self.elpts[ilep]=pt; self.eletas[ilep]=supereta; self.elSF[ilep]=sfID[0]
                else:    #fails id and calculating veto SF
                    if not sellep:
                        sfMCeffID  = self.get_sf(self.elhist_effs_id, abseta, pt)  #x:pt y:eta
                        VetoEventWgt[0] = (1-sfID[0]*sfMCeffID[0])/(1-sfMCeffID[0])
                        # if self.verbose: print 'veto el:', 'passID?', passId, 'passISO?', passIso, 'supereta', supereta, 'pt', pt, 'sfID', sfID[0], 'sfMCeffID', sfMCeffID[0], 'VetoSF', VetoEventWgt[0]
                        VetoEventWgt[1] = (1-(sfID[1]-sfID[0])*sfMCeffID[0])/(1-sfMCeffID[0])
                        # if self.verbose: print 'vetoel:', 'sfID', sfID, 'SF', VetoEventWgt[0]
                        # VetoEventWgt[2] = -(1-(sfID[1]-sfID[0])*sfMCeffID[0])/(1-sfMCeffID[0])
                        
        # muons:
        elif typ==13 and not genmu:
            # print 'Muon!'
            gen, dr = closest(lep, genleps)             #gen matching
            if dr <= 0.4:
                # if self.verbose: print 'matched!'
                nmatchedmus+=1
                sfID   = self.get_avg_sf(self.muhist_id_BCDEF, self.muhist_id_GH, pt, eta)
                sfISO  = self.get_avg_sf(self.muhist_iso_BCDEF, self.muhist_iso_GH, pt, eta)
            # calc overall scale factor using iso and id:
                IDerr = sfID[1]-sfID[0]; ISOerr = sfISO[1]-sfISO[0];
                if (passId and passIso):   #pass id and iso and sel SF
                    if sellep:
                        SelEventWgt[0] = sfID[0]*sfISO[0]
                        SelEventWgt[1] = math.sqrt(IDerr*IDerr+ISOerr*ISOerr)
                        # SelEventWgt[2] = math.sqrt(IDerr*IDerr+ISOerr*ISOerr)
                        # if self.verbose: print 'selmuon:', 'sfID', sfID, 'sfISO', sfISO, 'SF', SelEventWgt
                        if self.extras: self.mupts[ilep]=pt; self.muetas[ilep]=eta; self.muSF[ilep]=sfID[0]*sfISO[0]
                elif (passId and not passIso): #pass id not iso, veto SF
                    if not sellep:
                        sfMCeffISO  = self.get_sf(self.muhist_effs_iso, abseta, pt) #x:pt y:eta
                        VetoEventWgt[0] = sfID[0]*(1-sfISO[0]*sfMCeffISO[0])/(1-sfMCeffISO[0])
                        # if self.verbose: print 'veto mu:', 'passID?', passId, 'passISO?', passIso,  'eta', eta, 'pt', pt, 'sfID', sfID[0], 'sfISO', sfISO[0], 'sfMCeffISO', sfMCeffISO[0], 'VetoSF', VetoEventWgt[0]
                        #complicated uncertainties
                        VetoEventWgt[1] = (1/(1-sfMCeffISO[0]))*(IDerr-sfMCeffISO[0]*math.sqrt(IDerr*IDerr+ISOerr*ISOerr)) #just error now
                        # if self.verbose: print 'vetomuon:', 'sfID', sfID, 'sfISO', sfISO, 'SF', VetoEventWgt[0]
                        # VetoEventWgt[2] = -(1/(1-sfMCeffISO[0]))*(IDerr-sfMCeffISO[0]*math.sqrt(IDerr*IDerr+ISOerr*ISOerr)) #just error now
                        # VetoEventWgt[1] = sfID[1]*(1-sfISO[1]*sfMCeffISO[0])/(1-sfMCeffISO[0])
                        # VetoEventWgt[2] = sfID[2]*(1-sfISO[2]*sfMCeffISO[0])/(1-sfMCeffISO[0])
                elif not passId :    #fails id. handled by gen muons!
                    if not sellep:
                        VetoEventWgt = np.ones(3)

        #gen muon to replace fail id
        elif (typ==13 and genmu):
            if not sellep:
                # print 'GenMuon!'
                # if not passId:
                sfID   = self.get_avg_sf(self.muhist_id_BCDEF, self.muhist_id_GH, pt, eta)
                sfMCeffID  = self.get_sf(self.muhist_effs_id, abseta, pt) #x:pt y:eta
                VetoEventWgt[0] = (1-sfID[0]*sfMCeffID[0])/(1-sfMCeffID[0])
                VetoEventWgt[1] = (1-(sfID[1]-sfID[0])*sfMCeffID[0])/(1-sfMCeffID[0])
                # if self.verbose: print 'vetomuon:', 'sfID', sfID, 'SF', VetoEventWgt[0]        
                # if self.verbose: print 'veto genmu: sfID', sfID[0], 'sfMCeffID', sfMCeffID[0], 'VetoSF', VetoEventWgt[0]
                if self.extras: self.gmupts[ilep]=pt; self.gmuetas[ilep]=eta; self.gmuSF[ilep]=sfID[0]
                # VetoEventWgt[2] = -(1-(sfID[1]-sfID[0])*sfMCeffID[0])/(1-sfMCeffID[0])

        nmatchmus = nmatchedmus
        nmatchels = nmatchedels

        # print SelEventWgt, VetoEventWgt
        return SelEventWgt, VetoEventWgt, nmatchmus, nmatchels 
                
    # def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
    #     pass

    # fill scale factors
    def analyze(self, event):
        # if self.verbose: print '##########event', event.event
        calc = self.getSelectedLeptons(event)
        if self.verbose:
            print 'isMC and will calculate SFs?', calc
        return True

LepSF2016_1lep = lambda : LeptonSFProducer('1lep','2016')
LepSF2016_0lep = lambda : LeptonSFProducer('0lep','2016')

LepSF2017_1lep = lambda : LeptonSFProducer('1lep','2017')
LepSF2017_0lep = lambda : LeptonSFProducer('0lep','2017')

LepSF2018_1lep = lambda : LeptonSFProducer('1lep','2018')
LepSF2018_0lep = lambda : LeptonSFProducer('0lep','2018')

