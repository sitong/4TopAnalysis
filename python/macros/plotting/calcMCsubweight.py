#script for calculating data weights as a function of BDT and HT

import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
from addMCsubweight import addMCsubweight
# import RootStuff as rs
# import plottery as ply
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
gStyle.SetPalette(1)

parser = argparse.ArgumentParser(description='bla')
#parser.add_argument("-p", "--proc", dest="proc", default='TTTT')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

nj_nb = 'nj8nb3'
includettinc = False
fillfile     = True


jetmap = {'nj9nb3':'nbjets_nosys>=3 && njets_nosys>=9',
          'nj9nb2':'nbjets_nosys==2 && njets_nosys>=9',
          'nj7nb2':'nbjets_nosys==2 && njets_nosys==7',
          'nj7nb3':'nbjets_nosys>=3 && njets_nosys==7',
          'nj8nb2':'nbjets_nosys==2 && njets_nosys==8',
          'nj8nb3':'nbjets_nosys>=3 && njets_nosys==8'}


outdir = './plots/MCsubweight/'
year=args.year
util = addMCsubweight()
pd = plotdefs(year, 'SR', modbins=False)

mcfile  = pd.mcfiles['MC'] 
mcrf, mct = util.makeroot(mcfile, pd.tn)

if includettinc:
    TTfile  = pd.mcfiles['TT'] 
    TTrf, TTt = util.makeroot(TTfile, pd.tn)

datafile = pd.datafilename
drf, dt =  util.makeroot(datafile, pd.tn)

# print mcrf, TTrf, drf

topsels = ['RT1BT0', 'RT1BT1', 'RT2BTALL']
sels = {'RT1BT0':'nrestops_nosys==1 && nbstops_nosys==0 && ',
        'RT1BT1':'nrestops_nosys==1 && nbstops_nosys>=1 && ',
        'RT2BTALL':'nrestops_nosys>=2 && nbstops_nosys>=0 && ',}

def ttbbsel(insel):
    outsel = insel.replace("nfunky_leptons==0 &&","nfunky_leptons==0 && isttbb==1 &&")
    return outsel

def fixnjnb(insel):
    outsel = insel.replace("nbjets_nosys>=3 && njets_nosys>=9", jetmap[nj_nb])
    return outsel 

for ts in topsels:
    tsel = sels[ts]
    BDTbins =  np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0], dtype=np.double)
    HTbindict = {'RT1BT0'   : np.array([700.0, 800.0, 900.0, 1000.0, 1100.0, 1200.0, 1300.0, 1500.0], dtype=np.double),
                 'RT1BT1'   : np.array([700.0, 1400.0], dtype=np.double),
                 'RT2BTALL' : np.array([700.0, 1100.0], dtype=np.double) }
    HTbins = HTbindict[ts]
    HTbins_4plot = np.append(HTbins, 2000.0)

    c2 = r.TCanvas("c","c",800,800)
    num = r.TH2D("num"+year+'_'+ts, "num"+year+'_'+ts, len(HTbins), HTbins_4plot, len(BDTbins)-1, BDTbins)
    den = r.TH2D("den"+year+'_'+ts, "den"+year+'_'+ts, len(HTbins), HTbins_4plot, len(BDTbins)-1, BDTbins)
    corr = r.TH2D("corr"+year+'_'+ts, "corr"+year+'_'+ts, len(HTbins), HTbins_4plot, len(BDTbins)-1, BDTbins)
    for ibin in range(0, corr.GetNbinsX()):
        # print ibin
        if ibin<len(HTbins)-1:
            htcut='(ht_nosys>='+str(HTbins[ibin])+' && ht_nosys<'+str(HTbins[ibin+1])+') && '
        else:
            htcut='(ht_nosys>='+str(HTbins[ibin])+') && '

        mcsel = '('+htcut+tsel+pd.basesel+pd.mcstr[year]
        datasel = '('+htcut+tsel+pd.basesel+pd.dstr[year]+')'

        mcsel = fixnjnb(mcsel)
        datasel = fixnjnb(datasel)
        
        print mcsel, '\n'
        print datasel, '\n'

        print htcut, tsel, '\n'
        datahist = r.TH1D('datahist', 'datahist', len(BDTbins)-1, BDTbins)
        mchist = r.TH1D('mchist', 'mchist', len(BDTbins)-1, BDTbins)
        # datasubmchist = r.TH1D('subhist', 'subhist', len(BDTbins)-1, BDTbins)

        mct.Draw("catBDTdisc_nosys>>mchist", mcsel)
        dt.Draw("catBDTdisc_nosys>>datahist", datasel)

        if includettinc:
           tthist  = r.TH1D('tthist', 'tthist', len(BDTbins)-1, BDTbins)
           ttsel = ttbbsel(mcsel)
           TTt.Draw("catBDTdisc_nosys>>tthist", ttsel)
           mchist.Add(tthist) #add the ttbb histogram
        
        datasubmchist = datahist.Clone("datasubmchist")
        datasubmchist.Add(mchist, -1.0) #subtract minor mc backgrounds
        
        datahist.Sumw2(); datasubmchist.Sumw2()
        for jbin in range(0, corr.GetNbinsY()+1):
            #manually filling using 1d histograms that work...
            data=datahist.GetBinContent(jbin)
            sub=datasubmchist.GetBinContent(jbin)
            datae = datahist.GetBinError(jbin)
            sube = datasubmchist.GetBinError(jbin)
            fillval = 1.0; fillerr=0.0
            if data>0.0:
                fillval = sub/data
                if sub>0.0:
                    fillerr = np.sqrt((sube/sub)**2 + (datae/data)**2)*fillval
            print 'ht:', HTbins[ibin], 'BDT:', BDTbins[jbin], 'val:', sub, data, fillval
            corr.SetBinContent(ibin+1, jbin, fillval)
            num.SetBinContent(ibin+1, jbin, sub)
            den.SetBinContent(ibin+1, jbin, data)
        
            corr.SetBinError(ibin+1, jbin, fillerr)
            num.SetBinError(ibin+1, jbin, sube)
            den.SetBinError(ibin+1, jbin, datae)

    corr.GetYaxis().SetTitle("BDT")
    corr.GetXaxis().SetTitle("HT")
    corrhistname = 'corr2Dhist'+'_'+year+'_'+ts
    corr.Draw("COLZTEXTERROR")
    c2.SetRightMargin(0.18)
    c2.SetLeftMargin(0.18)
    gPad.Modified()
    gPad.Update()
    c2.Update()
    print 'saved histogram:', outdir+corrhistname+'.png'
    c2.SaveAs(outdir+corrhistname+'.png')

    cnum = r.TCanvas("c","c",800,800)
    cnum.cd()
    num.GetYaxis().SetTitle("BDT")
    num.GetXaxis().SetTitle("HT")
    numhistname = 'num2Dhist'+'_'+year+'_'+ts
    num.Draw("COLZTEXTERROR")
    cnum.SetRightMargin(0.18)
    cnum.SetLeftMargin(0.18)
    gPad.Modified()
    gPad.Update()
    cnum.Update()
    print 'saved histogram:', outdir+numhistname+'.png'
    cnum.SaveAs(outdir+numhistname+'.png')

    cden = r.TCanvas("c","c",800,800)
    cden.cd()
    den.GetYaxis().SetTitle("BDT")
    den.GetXaxis().SetTitle("HT")
    denhistname = 'den2Dhist'+'_'+year+'_'+ts
    den.Draw("COLZTEXTERROR")
    cden.SetRightMargin(0.18)
    cden.SetLeftMargin(0.18)
    gPad.Modified()
    gPad.Update()
    cden.Update()
    print 'saved histogram:', outdir+denhistname+'.png'
    cden.SaveAs(outdir+denhistname+'.png')

    if fillfile:
        if includettinc:
            outname = "./weighthists/MCsubweight_withttbb_"+nj_nb+".root"
        else:
            outname = "./weighthists/MCsubweight_NOttbb-redo_"+nj_nb+".root"
        print 'making', outname
        outrf = r.TFile(outname,"UPDATE")
        corr.Write()
        del outrf
