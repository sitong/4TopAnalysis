# revamped datacard dictionary
import numpy as np
from collections import OrderedDict
import ROOT as r

class datacardrefs:
    def __init__(self, year):
        self.year = year

# #all included
        systs = {'TTTT':['jes', 'jer', 'pileup','trigger', 'btagLF','btagHF','btagLFstats1', 'btagHFstats1', 'btagLFstats2','btagHFstats2', 'btagCFerr1','btagCFerr2', 'DeepAK8TopSF', 'DeepAK8WSF', 'isr', 'fsr', 'pdf','ME','ResTopEff', 'ResTopMiss'],
                 'TTX':['jes', 'jer', 'pileup', 'trigger', 'btagLF','btagHF','btagLFstats1', 'btagHFstats1', 'btagLFstats2','btagHFstats2', 'btagCFerr1','btagCFerr2', 'DeepAK8TopSF', 'DeepAK8WSF', 'isr', 'fsr', 'pdf','ME', 'ResTopEff', 'ResTopMiss'],
                 'other':['jes', 'jer', 'pileup', 'trigger', 'btagLF','btagHF','btagLFstats1', 'btagHFstats1', 'btagLFstats2','btagHFstats2', 'btagCFerr1','btagCFerr2', 'DeepAK8TopSF', 'DeepAK8WSF', 'ResTopEff', 'ResTopMiss']}

        self.ttbb =  False #ttbb as extra mc background
        self.randseed = False #add randseed shape uncertainty
        if self.randseed:
            self.ttbb=True

        if self.ttbb:
            systs['ttbb'] = ['jes', 'jer', 'pileup', 'trigger', 'btagLF','btagHF','btagLFstats1', 'btagHFstats1', 'btagLFstats2','btagHFstats2', 'btagCFerr1','btagCFerr2', 'DeepAK8TopSF', 'DeepAK8WSF', 'isr', 'fsr', 'pdf','ME', 'ResTopEff', 'ResTopMiss']

        self.qcdmc = False
        if self.qcdmc:
            systs['QCD-TT'] = ['pileup','trigger']
        self.systs=systs
        self.tn = 'Events'

        self.combVRbins = False #less conservative VR uncertainties
        self.mergehtbins = False #only top bins, not ht bins
        self.halfbdt=False #half blinded 
        self.usenafunc = True #add uncs at all
        self.addshapeunc = True #add shape, not just norm uncs
        self.corrnafunc = False #correlated vs. uncorrelated between bins
        self.nafunc = 0.2 #not used
        self.spursig = False
        self.perbin = False
        self.modspursig = False

        if self.spursig:
            systs['TTTT_spurious'] = ['jes', 'jer', 'pileup','trigger', 'btagLF','btagHF','btagLFstats1', 'btagHFstats1', 'btagLFstats2','btagHFstats2', 'btagCFerr1','btagCFerr2', 'DeepAK8TopSF', 'DeepAK8WSF', 'isr', 'fsr', 'pdf','ME','ResTopEff', 'ResTopMiss']

        self.usevarbin=True #use variable binning
        self.edges = np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0], dtype=np.double)
        if self.usevarbin:
            self.nbins= len(self.edges)-1
        else:
            self.nbins = 10

        self.selrange = [0.0, 1.0]

        self.prefire = {'RT1BT0htbin0_2016':{'TTTT':['0.973','1.00453'],'TTX':['0.981','1.00334']},
                        'RT1BT0htbin1_2016':{'TTTT':['0.973','1.00447'],'TTX':['0.979','1.00344']},
                        'RT1BT0htbin2_2016':{'TTTT':['0.972','1.00452'],'TTX':['0.977','1.00364']},
                        'RT1BT0htbin3_2016':{'TTTT':['0.972','1.00453'],'TTX':['0.977','1.0036']},
                        'RT1BT0htbin4_2016':{'TTTT':['0.971','1.00459'],'TTX':['0.975','1.00371']},
                        'RT1BT0htbin5_2016':{'TTTT':['0.970','1.00464'],'TTX':['0.974','1.00371']},
                        'RT1BT0htbin6_2016':{'TTTT':['0.969','1.00460'],'TTX':['0.974','1.00368']},
                        'RT1BT0htbin7_2016':{'TTTT':['0.971','1.00438'],'TTX':['0.976','1.0034']},
                        'RT1BT1htbin0_2016':{'TTTT':['0.974','1.00397'],'TTX':['0.979','1.00308']},
                        'RT1BT1htbin1_2016':{'TTTT':['0.975','1.00373'],'TTX':['0.977','1.00316']},
                        'RT2BTALLhtbin0_2016':{'TTTT':['0.975','1.00397'],'TTX':['0.981','1.00301']},
                        'RT2BTALLhtbin1_2016':{'TTTT':['0.975','1.00397'],'TTX':['0.981','1.00301']},
                        'RT1BT0htbin0_2017':{'TTTT':['0.963','1.00525'],'TTX':['0.970','1.00420']},
                        'RT1BT0htbin1_2017':{'TTTT':['0.961','1.00535'],'TTX':['0.968','1.00443']},
                        'RT1BT0htbin2_2017':{'TTTT':['0.960','1.00537'],'TTX':['0.965','1.00454']},
                        'RT1BT0htbin3_2017':{'TTTT':['0.957','1.00548'],'TTX':['0.964','1.00457']},
                        'RT1BT0htbin4_2017':{'TTTT':['0.957','1.00550'],'TTX':['0.961','1.00474']},
                        'RT1BT0htbin5_2017':{'TTTT':['0.955','1.00557'],'TTX':['0.960','1.00474']},
                        'RT1BT0htbin6_2017':{'TTTT':['0.953','1.00568'],'TTX':['0.959','1.00472']},
                        'RT1BT0htbin7_2017':{'TTTT':['0.953','1.00557'],'TTX':['0.961','1.00445']},
                        'RT1BT1htbin0_2017':{'TTTT':['0.961','1.00467'],'TTX':['0.969','1.00378']},
                        'RT1BT1htbin1_2017':{'TTTT':['0.960','1.00462'],'TTX':['0.968','1.00391']},
                        'RT2BTALLhtbin0_2017':{'TTTT':['0.962','1.00462'],'TTX':['0.970','1.00382']},
                        'RT2BTALLhtbin1_2017':{'TTTT':['0.962','1.00462'],'TTX':['0.970','1.00382']}}
        # #NEW
        VRerr = {'RT1BT0htbin0_2016': '1.124',
                 'RT1BT0htbin1_2016': '1.101',
                 'RT1BT0htbin2_2016': '1.112',
                 'RT1BT0htbin3_2016': '1.105',
                 'RT1BT0htbin4_2016': '1.249',
                 'RT1BT0htbin5_2016': '1.266',
                 'RT1BT0htbin6_2016': '1.145',
                 'RT1BT0htbin7_2016': '1.135',
                 'RT1BT1htbin0_2016': '1.246',
                 'RT1BT1htbin1_2016': '1.264',
                 'RT2BTALLhtbin0_2016': '1.254',
                 'RT2BTALLhtbin1_2016': '1.273',
                 'RT1BT0htbin0_2017': '1.138',
                 'RT1BT0htbin1_2017': '1.087',
                 'RT1BT0htbin2_2017': '1.231',
                 'RT1BT0htbin3_2017': '1.109',
                 'RT1BT0htbin4_2017': '1.207',
                 'RT1BT0htbin5_2017': '1.196',
                 'RT1BT0htbin6_2017': '1.242',
                 'RT1BT0htbin7_2017': '1.25',
                 'RT1BT1htbin0_2017': '1.188',
                 'RT1BT1htbin1_2017': '1.301',
                 'RT2BTALLhtbin0_2017': '1.179',
                 'RT2BTALLhtbin1_2017': '1.355',
                 'RT1BT0htbin0_2018': '1.089',
                 'RT1BT0htbin1_2018': '1.079',
                 'RT1BT0htbin2_2018': '1.121',
                 'RT1BT0htbin3_2018': '1.069',
                 'RT1BT0htbin4_2018': '1.118',
                 'RT1BT0htbin5_2018': '1.071',
                 'RT1BT0htbin6_2018': '1.130',
                 'RT1BT0htbin7_2018': '1.180',
                 'RT1BT1htbin0_2018': '1.203',
                 'RT1BT1htbin1_2018': '1.365',
                 'RT2BTALLhtbin0_2018': '1.111',
                 'RT2BTALLhtbin1_2018': '1.351'}

        # #OLD - recalc
        # VRerr = {'RT1BT0htbin0_2016':'1.111',
        #          'RT1BT0htbin1_2016':'1.085',
        #          'RT1BT0htbin2_2016':'1.140',
        #          'RT1BT0htbin3_2016':'1.132',
        #          'RT1BT0htbin4_2016':'1.241',
        #          'RT1BT0htbin5_2016':'1.266',
        #          'RT1BT0htbin6_2016':'1.146',
        #          'RT1BT0htbin7_2016':'1.147',
        #          'RT1BT1htbin0_2016':'1.285',
        #          'RT1BT1htbin1_2016':'1.268',
        #          'RT2BTALLhtbin0_2016':'1.244',
        #          'RT2BTALLhtbin1_2016':'1.371',
        #          'RT1BT0htbin0_2017':'1.133',
        #          'RT1BT0htbin1_2017':'1.089',
        #          'RT1BT0htbin2_2017':'1.230',
        #          'RT1BT0htbin3_2017':'1.105',
        #          'RT1BT0htbin4_2017':'1.207',
        #          'RT1BT0htbin5_2017':'1.188',
        #          'RT1BT0htbin6_2017':'1.241',
        #          'RT1BT0htbin7_2017':'1.254',
        #          'RT1BT1htbin0_2017':'1.232',
        #          'RT1BT1htbin1_2017':'1.252',
        #          'RT2BTALLhtbin0_2017':'1.149',
        #          'RT2BTALLhtbin1_2017':'1.178',
        #          'RT1BT0htbin0_2018':'1.075',
        #          'RT1BT0htbin1_2018':'1.064',
        #          'RT1BT0htbin2_2018':'1.117',
        #          'RT1BT0htbin3_2018':'1.068',
        #          'RT1BT0htbin4_2018':'1.118',
        #          'RT1BT0htbin5_2018':'1.075',
        #          'RT1BT0htbin6_2018':'1.123',
        #          'RT1BT0htbin7_2018':'1.184',
        #          'RT1BT1htbin0_2018':'1.218',
        #          'RT1BT1htbin1_2018':'1.387',
        #          'RT2BTALLhtbin0_2018':'1.110',
        #          'RT2BTALLhtbin1_2018':'1.355'}

        if self.ttbb:
                VRerr = {'RT1BT0htbin0_2016': '1.126',
                         'RT1BT0htbin1_2016': '1.162',
                         'RT1BT0htbin2_2016': '1.118',
                         'RT1BT0htbin3_2016': '1.101',
                         'RT1BT0htbin4_2016': '1.241',
                         'RT1BT0htbin5_2016': '1.266',
                         'RT1BT0htbin6_2016': '1.256',
                         'RT1BT0htbin7_2016': '1.192',
                         'RT1BT1htbin0_2016': '1.196',
                         'RT1BT1htbin1_2016': '1.273',
                         'RT2BTALLhtbin0_2016': '1.180',
                         'RT2BTALLhtbin1_2016': '1.273',
                         'RT1BT0htbin0_2017': '1.138',
                         'RT1BT0htbin1_2017': '1.107',
                         'RT1BT0htbin2_2017': '1.223',
                         'RT1BT0htbin3_2017': '1.202',
                         'RT1BT0htbin4_2017': '1.251',
                         'RT1BT0htbin5_2017': '1.227',
                         'RT1BT0htbin6_2017': '1.292',
                         'RT1BT0htbin7_2017': '1.282',
                         'RT1BT1htbin0_2017': '1.171',
                         'RT1BT1htbin1_2017': '1.278',
                         'RT2BTALLhtbin0_2017': '1.157',
                         'RT2BTALLhtbin1_2017': '1.165',
                         'RT1BT0htbin0_2018': '1.113',
                         'RT1BT0htbin1_2018': '1.130',
                         'RT1BT0htbin2_2018': '1.174',
                         'RT1BT0htbin3_2018': '1.126',
                         'RT1BT0htbin4_2018': '1.203',
                         'RT1BT0htbin5_2018': '1.152',
                         'RT1BT0htbin6_2018': '1.117',
                         'RT1BT0htbin7_2018': '1.246',
                         'RT1BT1htbin0_2018': '1.192',
                         'RT1BT1htbin1_2018': '1.396',
                         'RT2BTALLhtbin0_2018': '1.112',
                         'RT2BTALLhtbin1_2018': '1.347'}


        #OLD - orig
        # VRerr = {'RT1BT0htbin0_2016':'1.081',  #mean+rms  
        #          'RT1BT0htbin1_2016':'1.086',
        #          'RT1BT0htbin2_2016':'1.120',
        #          'RT1BT0htbin3_2016':'1.136',
        #          'RT1BT0htbin4_2016':'1.214',
        #          'RT1BT0htbin5_2016':'1.240',
        #          'RT1BT0htbin6_2016':'1.175',
        #          'RT1BT0htbin7_2016':'1.117',
        #          'RT1BT1htbin0_2016':'1.277',
        #          'RT1BT1htbin1_2016':'1.300',
        #          'RT2BTALLhtbin0_2016':'1.275',
        #          'RT2BTALLhtbin1_2016':'1.405',
        #          'RT1BT0htbin0_2017':'1.107',  
        #          'RT1BT0htbin1_2017':'1.066',
        #          'RT1BT0htbin2_2017':'1.214',
        #          'RT1BT0htbin3_2017':'1.120',
        #          'RT1BT0htbin4_2017':'1.195',
        #          'RT1BT0htbin5_2017':'1.153',
        #          'RT1BT0htbin6_2017':'1.206',
        #          'RT1BT0htbin7_2017':'1.209',
        #          'RT1BT1htbin0_2017':'1.227',
        #          'RT1BT1htbin1_2017':'1.293',
        #          'RT2BTALLhtbin0_2017':'1.146',
        #          'RT2BTALLhtbin1_2017':'1.140',
        #          'RT1BT0htbin0_2018':'1.051',  
        #          'RT1BT0htbin1_2018':'1.060',
        #          'RT1BT0htbin2_2018':'1.094',
        #          'RT1BT0htbin3_2018':'1.050',
        #          'RT1BT0htbin4_2018':'1.136',
        #          'RT1BT0htbin5_2018':'1.082',
        #          'RT1BT0htbin6_2018':'1.180',
        #          'RT1BT0htbin7_2018':'1.154',
        #          'RT1BT1htbin0_2018':'1.188',
        #          'RT1BT1htbin1_2018':'1.460',
        #          'RT2BTALLhtbin0_2018':'1.100',
        #          'RT2BTALLhtbin1_2018':'1.325'}

        if self.combVRbins:
            VRerr['RT1BT1htbin0_2016']='1.217'
            VRerr['RT1BT1htbin1_2016']='1.217'
            VRerr['RT2BTALLhtbin0_2016']='1.261'
            VRerr['RT2BTALLhtbin1_2016']='1.261'

            VRerr['RT1BT1htbin0_2017']='1.172'
            VRerr['RT1BT1htbin1_2017']='1.172'
            VRerr['RT2BTALLhtbin0_2017']='1.129'
            VRerr['RT2BTALLhtbin1_2017']='1.129'

            VRerr['RT1BT1htbin0_2018']='1.112'
            VRerr['RT1BT1htbin1_2018']='1.112'
            VRerr['RT2BTALLhtbin0_2018']='1.105'
            VRerr['RT2BTALLhtbin1_2018']='1.105'


        if self.mergehtbins:
            VRerr = {'RT1BT0_2016':'1.096',
                     'RT1BT1_2016':'1.228',
                     'RT2BTALL_2016':'1.22',
                     'RT1BT0_2017':'1.141',
                     'RT1BT1_2017':'1.168', 
                     'RT2BTALL_2017':'1.136',
                     'RT1BT0_2018':'1.067',
                     'RT1BT1_2018':'1.150',
                     'RT2BTALL_2018':'1.123'}

        self.VRerr = VRerr

        #signal
        #background
        spursigscale = {'RT1BT0htbin0_2016':17.72,
                        'RT1BT0htbin1_2016':38.81,
                        'RT1BT0htbin2_2016':2.516,
                        'RT1BT0htbin3_2016':20.32,
                        'RT1BT0htbin4_2016':7.825,
                        'RT1BT0htbin5_2016':7.072,
                        'RT1BT0htbin6_2016':23.60,
                        'RT1BT0htbin7_2016':17.54,
                        'RT1BT1htbin0_2016':2.151,
                        'RT1BT1htbin1_2016':18.13,
                        'RT2BTALLhtbin0_2016':6.792,
                        'RT2BTALLhtbin1_2016':1.902,
                        'RT1BT0htbin0_2017':48.57,
                        'RT1BT0htbin1_2017':9.760,
                        'RT1BT0htbin2_2017':17.11,
                        'RT1BT0htbin3_2017':21.92,
                        'RT1BT0htbin4_2017':16.04,
                        'RT1BT0htbin5_2017':28.10,
                        'RT1BT0htbin6_2017':7.012,
                        'RT1BT0htbin7_2017':10.28,
                        'RT1BT1htbin0_2017':3.387,
                        'RT1BT1htbin1_2017':15.59,
                        'RT2BTALLhtbin0_2017':31.72,
                        'RT2BTALLhtbin1_2017':18.48,
                        'RT1BT0htbin0_2018':12.27,                           
                        'RT1BT0htbin1_2018':21.80,
                        'RT1BT0htbin2_2018':6.479,
                        'RT1BT0htbin3_2018':7.193,
                        'RT1BT0htbin4_2018':15.35,
                        'RT1BT0htbin5_2018':10.16,
                        'RT1BT0htbin6_2018':26.03,
                        'RT1BT0htbin7_2018':3.640,
                        'RT1BT1htbin0_2018':8.782,
                        'RT1BT1htbin1_2018':10.06,
                        'RT2BTALLhtbin0_2018':2.329,
                        'RT2BTALLhtbin1_2018':1.891}
        self.spursigscale = spursigscale 

        #NEW - statcomm change
        # VRshapevals = {'2016':{'RT1BT0htbin0':0.232,
        #                        'RT1BT0htbin1':0.566,
        #                        'RT1BT0htbin2':0.029,
        #                        'RT1BT0htbin3':0.305,
        #                        'RT1BT0htbin4':0.109,
        #                        'RT1BT0htbin5':0.093,
        #                        'RT1BT0htbin6':0.264,
        #                        'RT1BT0htbin7':0.132,
        #                        'RT1BT1htbin0':0.029,
        #                        'RT1BT1htbin1':0.627,
        #                        'RT2BTALLhtbin0':0.254,
        #                        'RT2BTALLhtbin1':0.033},
        #                '2017':{'RT1BT0htbin0':0.388,
        #                        'RT1BT0htbin1':0.082,
        #                        'RT1BT0htbin2':0.218,
        #                        'RT1BT0htbin3':0.289,
        #                        'RT1BT0htbin4':0.216,
        #                        'RT1BT0htbin5':0.368,
        #                        'RT1BT0htbin6':0.071,
        #                        'RT1BT0htbin7':0.108,
        #                        'RT1BT1htbin0':0.041,
        #                        'RT1BT1htbin1':0.201,
        #                        'RT2BTALLhtbin0':0.823,
        #                        'RT2BTALLhtbin1':0.608},
        #                '2018':{'RT1BT0htbin0':0.104,                           
        #                        'RT1BT0htbin1':0.229,
        #                        'RT1BT0htbin2':0.067,
        #                        'RT1BT0htbin3':0.088,
        #                        'RT1BT0htbin4':0.173,
        #                        'RT1BT0htbin5':0.100,
        #                        'RT1BT0htbin6':0.309,
        #                        'RT1BT0htbin7':0.026,
        #                        'RT1BT1htbin0':0.184,
        #                        'RT1BT1htbin1':0.191,
        #                        'RT2BTALLhtbin0':0.041,
        #                        'RT2BTALLhtbin1':0.035}}

        #NEW RECALC
        VRshapevals = {'2016':{'RT1BT0htbin0':0.01,
                               'RT1BT0htbin1':0.03,
                               'RT1BT0htbin2':0.01,
                               'RT1BT0htbin3':0.02,
                               'RT1BT0htbin4':0.02,
                               'RT1BT0htbin5':0.01,
                               'RT1BT0htbin6':0.02,
                               'RT1BT0htbin7':0.01,
                               'RT1BT1htbin0':0.01,
                               'RT1BT1htbin1':0.03,
                               'RT2BTALLhtbin0':0.02,
                               'RT2BTALLhtbin1':0.01},
                       '2017':{'RT1BT0htbin0':0.01,
                               'RT1BT0htbin1':0.01,
                               'RT1BT0htbin2':0.03,
                               'RT1BT0htbin3':0.02,
                               'RT1BT0htbin4':0.01,
                               'RT1BT0htbin5':0.01,
                               'RT1BT0htbin6':0.01,
                               'RT1BT0htbin7':0.02,
                               'RT1BT1htbin0':0.01,
                               'RT1BT1htbin1':0.01,
                               'RT2BTALLhtbin0':0.03,
                               'RT2BTALLhtbin1':0.01},
                       '2018':{'RT1BT0htbin0':0.01,
                               'RT1BT0htbin1':0.01,
                               'RT1BT0htbin2':0.01,
                               'RT1BT0htbin3':0.01,
                               'RT1BT0htbin4':0.01,
                               'RT1BT0htbin5':0.01,
                               'RT1BT0htbin6':0.02,
                               'RT1BT0htbin7':0.01,
                               'RT1BT1htbin0':0.02,
                               'RT1BT1htbin1':0.03,
                               'RT2BTALLhtbin0':0.01,
                               'RT2BTALLhtbin1':0.02}}            
        #statcomm across all bins
        VRshapearr = {'2016':{'RT1BT0htbin0':[0.07873332500457764, 0.173775315284729, 0.02992713451385498, 0.05856722593307495, 0.05955100059509277, 0.012038350105285645, 0.06966036558151245, 0.015949130058288574, 0.23236119747161865],
                              'RT1BT0htbin1':[0.0950155258178711, 0.01145172119140625, 0.09251368045806885, 0.07166421413421631, 0.01629263162612915, 0.11257219314575195, 0.06964421272277832, 0.03446882963180542, 0.5659515857696533],
                              'RT1BT0htbin2':[0.05013000965118408, 0.0016783475875854492, 0.0793907642364502, 0.027339518070220947, 0.011176705360412598, 0.017948627471923828, 0.11616873741149902, 0.06437742710113525, 0.028673827648162842],
                              'RT1BT0htbin3':[0.034958481788635254, 0.1472616195678711, 0.03725409507751465, 0.12915992736816406, 0.04318714141845703, 0.0913163423538208, 0.08566403388977051, 0.08255279064178467, 0.30498582124710083],
                              'RT1BT0htbin4':[0.1198502779006958, 0.177994966506958, 0.041634440422058105, 0.10013079643249512, 0.13960641622543335, 0.0022071003913879395, 0.19130253791809082, 0.001128852367401123, 0.10928815603256226],
                              'RT1BT0htbin5':[0.08795249462127686, 0.26602256298065186, 0.08341020345687866, 0.04310697317123413, 0.06709915399551392, 0.01866227388381958, 0.013856172561645508, 0.07430672645568848, 0.09334611892700195],
                              'RT1BT0htbin6':[0.11720764636993408, 0.031653404235839844, 0.05572474002838135, 0.04606151580810547, 0.07206499576568604, 0.037745535373687744, 0.25868135690689087, 0.06871700286865234, 0.2636009454727173],
                              'RT1BT0htbin7':[0.24269556999206543, 0.07828664779663086, 0.21057486534118652, 0.04149460792541504, 0.01970851421356201, 0.0393141508102417, 0.04063308238983154, 0.06573742628097534, 0.13219141960144043],
                              'RT1BT1htbin0':[1.148052453994751, 0.5451275408267975, 0.21531176567077637, 0.17825591564178467, 0.0323336124420166, 0.055235981941223145, 0.1007070541381836, 0.349560022354126, 0.028841853141784668],
                              'RT1BT1htbin1':[7.242439270019531, 0.40674853324890137, 0.07461118698120117, 0.3622620701789856, 0.0309755802154541, 0.25419044494628906, 0.2252628207206726, 0.07454335689544678, 0.6268599033355713],
                              'RT2BTALLhtbin0':[0.7332748174667358, 0.20886832475662231, 0.0068680644035339355, 0.09313219785690308, 0.1590181589126587, 0.04471755027770996, 0.07076907157897949, 0.42224764823913574, 0.25395673513412476],
                              'RT2BTALLhtbin1':[1.0, 0.31460410356521606, 0.39445579051971436, 0.04941028356552124, 0.05967479944229126, 0.09612441062927246, 0.0147627592086792, 0.11687731742858887, 0.03345692157745361]},
                      '2017':{'RT1BT0htbin0':[0.22591209411621094, 0.033391475677490234, 0.0443652868270874, 0.04926586151123047, 0.09153628349304199, 0.03418058156967163, 0.024461984634399414, 0.09270071983337402, 0.38805198669433594],
                              'RT1BT0htbin1':[0.14566516876220703, 0.042475342750549316, 0.04494750499725342, 0.02077937126159668, 0.04673194885253906, 0.017431020736694336, 0.026498079299926758, 0.06897532939910889, 0.08199405670166016],
                              'RT1BT0htbin2':[0.14629578590393066, 0.016165852546691895, 0.10241615772247314, 0.04417622089385986, 0.005196809768676758, 0.05918872356414795, 0.017646968364715576, 0.07683408260345459, 0.21797215938568115],
                              'RT1BT0htbin3':[0.2244831919670105, 0.019137263298034668, 0.12446814775466919, 0.02687293291091919, 0.13116669654846191, 0.09061968326568604, 0.0615389347076416, 0.1269296407699585, 0.28928685188293457],
                              'RT1BT0htbin4':[0.34148699045181274, 0.04441487789154053, 0.01965653896331787, 0.0427173376083374, 0.07986122369766235, 0.030756711959838867, 0.04124188423156738, 0.07363569736480713, 0.21594929695129395],
                              'RT1BT0htbin5':[0.20047438144683838, 0.06128489971160889, 0.20879769325256348, 0.0057599544525146484, 0.09324288368225098, 0.08075082302093506, 0.0993582010269165, 0.036638498306274414, 0.36797165870666504],
                              'RT1BT0htbin6':[0.17155218124389648, 0.24058949947357178, 0.13836205005645752, 0.02352309226989746, 0.09709173440933228, 0.03459513187408447, 0.16289371252059937, 0.18851518630981445, 0.07065999507904053],
                              'RT1BT0htbin7':[0.434312105178833, 0.4972972869873047, 0.05966365337371826, 0.1506568193435669, 0.07648777961730957, 0.06459510326385498, 0.0301896333694458, 0.09849953651428223, 0.1078101396560669],
                              'RT1BT1htbin0':[0.17585891485214233, 0.016112089157104492, 0.15254688262939453, 0.08021426200866699, 0.05556678771972656, 0.2797966003417969, 0.1186065673828125, 0.2631765604019165, 0.04073333740234375],
                              'RT1BT1htbin1':[1.0, 0.42138373851776123, 0.27192115783691406, 0.602188229560852, 0.28815436363220215, 0.20108354091644287, 0.017835497856140137, 0.12011343240737915, 0.23984134197235107],
                              'RT2BTALLhtbin0':[0.059799134731292725, 0.1888434886932373, 0.20541071891784668, 0.12738466262817383, 0.07298684120178223, 0.08845871686935425, 0.06519216299057007, 0.061752915382385254, 0.8234851360321045],
                              'RT2BTALLhtbin1':[1150.10205078125, 6.643919944763184, 0.16005796194076538, 0.0959126353263855, 0.21670985221862793, 0.17650121450424194, 0.09047287702560425, 0.024848997592926025, 0.6077666282653809]},
                      '2018':{'RT1BT0htbin0':[0.18651032447814941, 0.07104229927062988, 0.006566047668457031, 0.07982772588729858, 0.05548936128616333, 0.017359673976898193, 0.0039653778076171875, 0.13374412059783936, 0.10405266284942627],                           
                              'RT1BT0htbin1':[0.049058377742767334, 0.07642829418182373, 0.006828188896179199, 0.00873333215713501, 0.09829729795455933, 0.04753267765045166, 0.06106388568878174, 0.13466525077819824, 0.22925198078155518],
                              'RT1BT0htbin2':[0.0027837753295898438, 0.019880056381225586, 0.022519230842590332, 0.009724259376525879, 0.0858660340309143, 0.03144717216491699, 0.04767405986785889, 0.044554710388183594, 0.06652712821960449],
                              'RT1BT0htbin3':[0.22211432456970215, 0.006950855255126953, 0.018637895584106445, 0.02766132354736328, 0.045293211936950684, 0.010698795318603516, 0.050363898277282715, 0.04005396366119385, 0.0876990556716919],
                              'RT1BT0htbin4':[0.19681143760681152, 0.018901944160461426, 0.046617329120635986, 0.10966002941131592, 0.0011200904846191406, 0.13033849000930786, 0.17615115642547607, 0.15869760513305664, 0.17263329029083252],
                              'RT1BT0htbin5':[0.38880592584609985, 0.009483814239501953, 0.013504981994628906, 0.10545170307159424, 0.0500485897064209, 0.03129887580871582, 0.10800337791442871, 0.08731305599212646, 0.1000676155090332],
                              'RT1BT0htbin6':[0.3753606081008911, 0.10384476184844971, 0.05175900459289551, 0.049691200256347656, 0.017440438270568848, 0.10496068000793457, 0.037431955337524414, 0.016379594802856445, 0.30927640199661255],
                              'RT1BT0htbin7':[0.26959025859832764, 0.4663197994232178, 0.020956039428710938, 0.21315091848373413, 0.08701944351196289, 0.024099528789520264, 0.10668718814849854, 0.05350601673126221, 0.02636796236038208],
                              'RT1BT1htbin0':[0.33703017234802246, 0.04064297676086426, 0.14055728912353516, 0.13697409629821777, 0.23020005226135254, 0.001801133155822754, 0.08043384552001953, 0.07982444763183594, 0.18367862701416016],
                              'RT1BT1htbin1':[1.3832030296325684, 0.5674604177474976, 0.2575702667236328, 0.28086012601852417, 0.05956137180328369, 0.012285351753234863, 0.07203412055969238, 0.04196369647979736, 0.19148552417755127],
                              'RT2BTALLhtbin0':[0.6907228529453278, 0.2788299322128296, 0.015856623649597168, 0.08131027221679688, 0.07123041152954102, 0.005060076713562012, 0.1216859221458435, 0.13773465156555176, 0.04135143756866455],
                              'RT2BTALLhtbin1':[1.0, 0.1416594386100769, 0.554854542016983, 0.12733125686645508, 0.1645677089691162, 0.0848623514175415, 0.13359403610229492, 0.11033904552459717, 0.03469574451446533]}}

        self.VRshapearr = VRshapearr

        #OLD RECALC
        # VRshapevals = {'2016':{'RT1BT0htbin0':0.01, #old
        #                        'RT1BT0htbin1':0.02,
        #                        'RT1BT0htbin2':0.03,
        #                        'RT1BT0htbin3':0.02,
        #                        'RT1BT0htbin4':0.03,
        #                        'RT1BT0htbin5':0.02,
        #                        'RT1BT0htbin6':0.02,
        #                        'RT1BT0htbin7':0.01,
        #                        'RT1BT1htbin0':0.01,
        #                        'RT1BT1htbin1':0.02,
        #                        'RT2BTALLhtbin0':0.01,
        #                        'RT2BTALLhtbin1':0.01},
        #                '2017':{'RT1BT0htbin0':0.01,
        #                        'RT1BT0htbin1':0.01,
        #                        'RT1BT0htbin2':0.03,
        #                        'RT1BT0htbin3':0.01,
        #                        'RT1BT0htbin4':0.01,
        #                        'RT1BT0htbin5':0.01,
        #                        'RT1BT0htbin6':0.01,
        #                        'RT1BT0htbin7':0.01,
        #                        'RT1BT1htbin0':0.02,
        #                        'RT1BT1htbin1':0.02,
        #                        'RT2BTALLhtbin0':0.02,
        #                        'RT2BTALLhtbin1':0.01},
        #                '2018':{'RT1BT0htbin0':0.01, #old
        #                        'RT1BT0htbin1':0.01,
        #                        'RT1BT0htbin2':0.01,
        #                        'RT1BT0htbin3':0.01,
        #                        'RT1BT0htbin4':0.01,
        #                        'RT1BT0htbin5':0.01,
        #                        'RT1BT0htbin6':0.01,
        #                        'RT1BT0htbin7':0.01,
        #                        'RT1BT1htbin0':0.02,
        #                        'RT1BT1htbin1':0.01,
        #                        'RT2BTALLhtbin0':0.01,
        #                        'RT2BTALLhtbin1':0.01}}


        self.VRshapevals = VRshapevals

        metfilters   = ' && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'
        if year=='2016':
            metfilters =  ' && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
        self.metfilters = metfilters

        datatrigs = {'2016' : '&& (trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || trigHLT_PFHT450_SixJet40_BTagCSV_p056==1)'+self.metfilters,
                     '2017' : '&& (trigHLT_PFHT430_SixJet40_BTagCSV_p080 || trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+self.metfilters,
                     '2018' : '&& (trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+self.metfilters}
        self.datatrigs = datatrigs[year]

        #[TTTT, TTX, minorMC]
        self.eLV = {'RT1BT0htbin0_2016':['1.02617', '1.00156', '1.00892'],
                    'RT1BT0htbin1_2016':['1.03397', '1.03092', '1.00459'],
                    'RT1BT0htbin2_2016':['1.04548', '1.01689', '1.01689'],
                    'RT1BT0htbin3_2016':['1.02698', '1.02723', '1.03946'],
                    'RT1BT0htbin4_2016':['1.03897', '1.02353', '1.00153'],
                    'RT1BT0htbin5_2016':['1.04506', '1.01239', '1.00653'],
                    'RT1BT0htbin6_2016':['1.05360', '1.01347', '1.00010'],
                    'RT1BT0htbin7_2016':['1.06050', '1.02796', '1.03231'],
                    'RT1BT1htbin0_2016':['1.03792', '1.02808', '1.00'],
                    'RT1BT1htbin1_2016':['1.04958', '1.04117', '1.00'],
                    'RT2BTALLhtbin0_2016':['1.03797', '1.01367', '1.01420'],
                    'RT2BTALLhtbin1_2016':['1.03797', '1.01367', '1.01420'],
                            
                    'RT1BT0htbin0_2017':['1.02657', '1.00250', '1.00589'],
                    'RT1BT0htbin1_2017':['1.02971', '1.00607', '1.00067'],
                    'RT1BT0htbin2_2017':['1.03168', '1.00152', '1.00'],
                    'RT1BT0htbin3_2017':['1.02625', '1.00620', '1.00451'],
                    'RT1BT0htbin4_2017':['1.03251', '1.00259', '1.00'],
                    'RT1BT0htbin5_2017':['1.03159', '1.02595', '1.00'],
                    'RT1BT0htbin6_2017':['1.03576', '1.00313', '1.0003'],
                    'RT1BT0htbin7_2017':['1.03761', '1.04195', '1.00597'],
                    'RT1BT1htbin0_2017':['1.02731', '1.00812', '1.00055'],
                    'RT1BT1htbin1_2017':['1.03300', '1.01688', '1.00'],
                    'RT2BTALLhtbin0_2017':['1.02324', '1.01278', '1.00'],
                    'RT2BTALLhtbin1_2017':['1.02324', '1.01278', '1.00'],
                            
                    'RT1BT0htbin0_2018':['1.03198', '1.01201', '1.00'],
                    'RT1BT0htbin1_2018':['1.03123', '1.01774', '1.00450'],
                    'RT1BT0htbin2_2018':['1.03222', '1.00608', '1.00763'],
                    'RT1BT0htbin3_2018':['1.02967', '1.01064', '1.01608'],
                    'RT1BT0htbin4_2018':['1.03073', '1.01798', '1.00'],
                    'RT1BT0htbin5_2018':['1.03302', '1.03157', '1.00982'],
                    'RT1BT0htbin6_2018':['1.03947', '1.02634', '1.00258'],
                    'RT1BT0htbin7_2018':['1.04149', '1.01075', '1.00031'],
                    'RT1BT1htbin0_2018':['1.03662', '1.00481', '1.00'],
                    'RT1BT1htbin1_2018':['1.02745', '1.00702', '1.00'],
                    'RT2BTALLhtbin0_2018':['1.02767', '1.01011', '1.00'],
                    'RT2BTALLhtbin1_2018':['1.02767', '1.01011', '1.00'] }

        self.mLV = {'RT1BT0htbin0_2016':['1.00456', '1.00112'],
                    'RT1BT0htbin1_2016':['1.01554', '1.00012'],
                    'RT1BT0htbin2_2016':['1.00924', '1.00019'],
                    'RT1BT0htbin3_2016':['1.00232', '1.00165'],
                    'RT1BT0htbin4_2016':['1.00421', '1.00128'],
                    'RT1BT0htbin5_2016':['1.00177', '1.00046'],
                    'RT1BT0htbin6_2016':['1.01173', '1.00088'],
                    'RT1BT0htbin7_2016':['1.00850', '1.01094'],
                    'RT1BT1htbin0_2016':['1.00788', '1.00103'],
                    'RT1BT1htbin1_2016':['1.00243', '1.01183'],
                    'RT2BTALLhtbin0_2016':['1.00788', '1.00178'],
                    'RT2BTALLhtbin1_2016':['1.00788', '1.00178'],
                    
                    'RT1BT0htbin0_2017':['1.00179', '1.00022'],
                    'RT1BT0htbin1_2017':['1.00156', '1.00020'],
                    'RT1BT0htbin2_2017':['1.00604', '1.00007'],
                    'RT1BT0htbin3_2017':['1.00256', '1.00139'],
                    'RT1BT0htbin4_2017':['1.00254', '1.00019'],
                    'RT1BT0htbin5_2017':['1.00659', '1.00106'],
                    'RT1BT0htbin6_2017':['1.00811', '1.00021'],
                    'RT1BT0htbin7_2017':['1.00555', '1.00014'],
                    'RT1BT1htbin0_2017':['1.00116', '1.00148'],
                    'RT1BT1htbin1_2017':['1.00050', '1.00118'],
                    'RT2BTALLhtbin0_2017':['1.00272', '1.00081'],
                    'RT2BTALLhtbin1_2017':['1.00272', '1.00081'],
                
                    'RT1BT0htbin0_2018':['1.00107', '1.00019'],
                    'RT1BT0htbin1_2018':['1.00318', '1.00528'],
                    'RT1BT0htbin2_2018':['1.00127', '1.00030'],
                    'RT1BT0htbin3_2018':['1.00147', '1.00014'],
                    'RT1BT0htbin4_2018':['1.00163', '1.00062'],
                    'RT1BT0htbin5_2018':['1.00049', '1.00037'],
                    'RT1BT0htbin6_2018':['1.00045', '1.00020'],
                    'RT1BT0htbin7_2018':['1.00066', '1.00108'],
                    'RT1BT1htbin0_2018':['1.00019', '1.00094'],
                    'RT1BT1htbin1_2018':['1.00112', '1.00048'],
                    'RT2BTALLhtbin0_2018':['1.00131', '1.00009'],
                    'RT2BTALLhtbin1_2018':['1.00131', '1.00009'] }
        
        lumi = {'2016':'36.33',
                '2017':'41.53',
                '2018':'59.74'}
        self.lumi = lumi[year]
        # self.lumi = lumi[year]

        uclumiunc={'2016':'1.010',
                   '2017':'1.020',
                   '2018':'1.015'}
        clumiunc={'2016':'1.006',
                  '2017':'1.009',
                  '2018':'1.020'}
        c2lumiunc={'2016':'1.000',
                   '2017':'1.006',
                   '2018':'1.002'}
        self.uclumiunc = uclumiunc[year]
        self.clumiunc = clumiunc[year]
        self.c2lumiunc = c2lumiunc[year]

        self.DDprocname = 'DDBKG' 
        if self.qcdmc:
            self.DDprocname = 'QCD-TT' 
        self.mcprocs = ['TTTT', 'TTX', 'other']
        self.processes=['TTTT', 'TTX', 'other', self.DDprocname]
        if self.ttbb:
            self.mcprocs = ['TTTT', 'TTX', 'other', 'ttbb']
            self.processes=['TTTT', 'TTX', 'other', 'ttbb', self.DDprocname]
        if self.spursig:
            self.mcprocs = ['TTTT', 'TTTT_spurious', 'TTX', 'other']
            self.processes=['TTTT', 'TTTT_spurious', 'TTX', 'other', self.DDprocname]
            

        #OLD
        # self.datafile = '/eos/uscms//store/user/lpcstop/mequinna/pt50_studies/DATA_'+year+'_merged_0lep_tree.root'
        # self.procfiles={
        #     'TTTT' :'/eos/uscms/store/user/lpcstop/mequinna/pt50_studies/TTTT_'+year+'_merged_0lep_tree.root',
        #     'TTX'  :'/eos/uscms/store/user/lpcstop/mequinna/pt50_studies/TTX_'+year+'_merged_0lep_tree.root',
        #     'other':'/eos/uscms/store/user/lpcstop/mequinna/pt50_studies/minorMC_'+year+'_merged_0lep_tree.root',
        #     'ttbb':'/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/python/macros/eventMVA/updatedrootfiles/ttbb_merged_notrigsf_'+year+'.root',
        #     'TTOther' : '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/TTOther_'+year+'_merged_0lep_tree.root',
        #     'QCD-TT' : '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/QCD-TT_'+year+'_merged_0lep_tree.root',
        # }

        #NEW
        self.datafile = '/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/JETHT_'+year+'_merged_0lep_tree.root'
        self.procfiles={
            'TTTT' :'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TTTT_'+year+'_merged_0lep_tree.root',
            'TTTT_spurious' :'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TTTT_'+year+'_merged_0lep_tree.root',
            'TTX'  :'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TTX_'+year+'_merged_0lep_tree.root',
            'other':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/minorMC_'+year+'_merged_0lep_tree.root',
            'ttbb':'/eos/uscms//store/user/lpcstop/mequinna/merged/'+year+'/feb2022_fullttbbtest/TT_'+year+'_merged_0lep_tree.root',
            'QCD-TT' : '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/QCD-TT_'+year+'_merged_0lep_tree.root',
        }

        self.binsels =self.getbinsels('nosys')
        self.mcsel = self.getsels('nosys',isMC=True)
        self.mcsel_ttbb = self.getsels('nosys',isMC=True,isttbb=True)
        self.datasel = self.getsels('nosys',isMC=False)

        #OLD
        # DDyields = {'2016' : { "RT1BT0htbin0": [ 1463.936 , 64.438 ],
        #                        "RT1BT0htbin1": [ 1441.295 , 72.106 ],
        #                        "RT1BT0htbin2": [ 1076.445 , 63.613 ],
        #                        "RT1BT0htbin3": [ 989.389 , 69.217 ],
        #                        "RT1BT0htbin4": [ 640.184 , 54.083 ],
        #                        "RT1BT0htbin5": [ 456.754 , 48.8 ],
        #                        "RT1BT0htbin6": [ 663.752 , 66.274 ],
        #                        "RT1BT0htbin7": [ 708.539 , 80.637 ],
        #                        "RT1BT1htbin0": [ 184.436 , 25.015 ],
        #                        "RT1BT1htbin1": [ 263.052 , 54.089 ],
        #                        "RT2BTALLhtbin0": [ 469.543 , 45.345 ],
        #                        "RT2BTALLhtbin1": [ 262.817 , 55.318 ]},
        #             '2017' : { "RT1BT0htbin0": [ 1939.244 , 82.263 ],
        #                        "RT1BT0htbin1": [ 1795.414 , 86.224 ],
        #                        "RT1BT0htbin2": [ 1193.359 , 67.963 ],
        #                        "RT1BT0htbin3": [ 1132.258 , 77.038 ],
        #                        "RT1BT0htbin4": [ 700.188 , 58.831 ],
        #                        "RT1BT0htbin5": [ 510.998 , 52.971 ],
        #                        "RT1BT0htbin6": [ 677.904 , 68.579 ],
        #                        "RT1BT0htbin7": [ 595.47 , 70.277 ],
        #                        "RT1BT1htbin0": [ 283.106 , 36.718 ],
        #                        "RT1BT1htbin1": [ 428.509 , 80.807 ],
        #                        "RT2BTALLhtbin0": [ 548.215 , 52.361 ],
        #                        "RT2BTALLhtbin1": [ 318.736 , 64.432 ]},                    
        #             '2018' : { "RT1BT0htbin0": [ 2648.844 , 97.648 ],
        #                        "RT1BT0htbin1": [ 2353.241 , 97.541 ],
        #                        "RT1BT0htbin2": [ 2024.165 , 97.935 ],
        #                        "RT1BT0htbin3": [ 1506.351 , 88.398 ],
        #                        "RT1BT0htbin4": [ 1223.197 , 87.637 ],
        #                        "RT1BT0htbin5": [ 991.664 , 87.078 ],
        #                        "RT1BT0htbin6": [ 1090.559 , 93.707 ],
        #                        "RT1BT0htbin7": [ 919.235 , 94.206 ],
        #                        "RT1BT1htbin0": [ 433.497 , 45.683 ],
        #                        "RT1BT1htbin1": [ 494.323 , 82.33 ],
        #                        "RT2BTALLhtbin0": [ 619.241 , 52.859 ],
        #                        "RT2BTALLhtbin1": [ 350.532 , 59.38 ]} }

        #NEW
        DDyields = {'2016' : {"RT1BT0htbin0": [ 1541.432 , 64.492 ],
                              "RT1BT0htbin1": [ 1502.071 , 71.544 ],
                              "RT1BT0htbin2": [ 1149.019 , 64.472 ],
                              "RT1BT0htbin3": [ 1041.726 , 69.389 ],
                              "RT1BT0htbin4": [ 670.204 , 54.03 ],
                              "RT1BT0htbin5": [ 480.002 , 48.759 ],
                              "RT1BT0htbin6": [ 691.133 , 65.515 ],
                              "RT1BT0htbin7": [ 758.273 , 80.068 ],
                              "RT1BT1htbin0": [ 196.7 , 25.43 ],
                              "RT1BT1htbin1": [ 282.765 , 54.089 ],
                              "RT2BTALLhtbin0": [ 493.862 , 45.909 ],
                              "RT2BTALLhtbin1": [ 276.909 , 55.954 ]},
                    '2017' : {"RT1BT0htbin0": [ 1997.86 , 81.83 ],
                              "RT1BT0htbin1": [ 1864.015 , 85.777 ],
                              "RT1BT0htbin2": [ 1262.894 , 68.668 ],
                              "RT1BT0htbin3": [ 1178.831 , 76.508 ],
                              "RT1BT0htbin4": [ 748.311 , 59.75 ],
                              "RT1BT0htbin5": [ 530.642 , 52.263 ],
                              "RT1BT0htbin6": [ 728.663 , 69.398 ],
                              "RT1BT0htbin7": [ 628.353 , 68.64 ],
                              "RT1BT1htbin0": [ 295.489 , 36.462 ],
                              "RT1BT1htbin1": [ 440.826 , 77.783 ],
                              "RT2BTALLhtbin0": [ 582.581 , 53.309 ],
                              "RT2BTALLhtbin1": [ 339.314 , 65.745 ]},
                    '2018' : {"RT1BT0htbin0": [ 2730.779 , 97.021 ],
                              "RT1BT0htbin1": [ 2432.161 , 96.798 ],
                              "RT1BT0htbin2": [ 2110.873 , 97.805 ],
                              "RT1BT0htbin3": [ 1564.347 , 87.506 ],
                              "RT1BT0htbin4": [ 1263.017 , 86.125 ],
                              "RT1BT0htbin5": [ 1039.898 , 86.709 ],
                              "RT1BT0htbin6": [ 1135.229 , 91.961 ],
                              "RT1BT0htbin7": [ 967.105 , 91.201 ],
                              "RT1BT1htbin0": [ 444.378 , 44.693 ],
                              "RT1BT1htbin1": [ 527.357 , 81.372 ],
                              "RT2BTALLhtbin0": [ 660.866 , 54.285 ],
                              "RT2BTALLhtbin1": [ 366.779 , 59.632 ]}}

        # if self.ttbb: #with SF
        #     DDyields = {'2016' : {"RT1BT0htbin0": [ 1192.159 , 57.145 ],
        #                           "RT1BT0htbin1": [ 1182.027 , 63.996 ],
        #                           "RT1BT0htbin2": [ 901.586 , 57.285 ],
        #                           "RT1BT0htbin3": [ 856.647 , 63.191 ],
        #                           "RT1BT0htbin4": [ 548.358 , 48.76 ],
        #                           "RT1BT0htbin5": [ 396.47 , 44.544 ],
        #                           "RT1BT0htbin6": [ 604.189 , 62.452 ],
        #                           "RT1BT0htbin7": [ 658.729 , 76.253 ],
        #                           "RT1BT1htbin0": [ 145.32 , 21.467 ],
        #                           "RT1BT1htbin1": [ 213.386 , 48.381 ],
        #                           "RT2BTALLhtbin0": [ 329.221 , 37.132 ],
        #                           "RT2BTALLhtbin1": [ 201.062 , 47.713 ]},
        #                 '2017' : {"RT1BT0htbin0": [ 1710.776 , 77.379 ],
        #                           "RT1BT0htbin1": [ 1536.443 , 79.086 ],
        #                           "RT1BT0htbin2": [ 1066.726 , 64.127 ],
        #                           "RT1BT0htbin3": [ 1019.808 , 73.175 ],
        #                           "RT1BT0htbin4": [ 609.019 , 54.148 ],
        #                           "RT1BT0htbin5": [ 431.577 , 47.6 ],
        #                           "RT1BT0htbin6": [ 592.468 , 64.103 ],
        #                           "RT1BT0htbin7": [ 534.446 , 64.36 ],
        #                           "RT1BT1htbin0": [ 246.586 , 34.271 ],
        #                           "RT1BT1htbin1": [ 299.736 , 63.165 ],
        #                           "RT2BTALLhtbin0": [ 442.719 , 46.726 ],
        #                           "RT2BTALLhtbin1": [ 269.65 , 60.332 ]},
        #                 '2018' : {"RT1BT0htbin0": [ 2337.724 , 88.907 ],
        #                           "RT1BT0htbin1": [ 2080.683 , 89.116 ],
        #                           "RT1BT0htbin2": [ 1784.1 , 89.486 ],
        #                           "RT1BT0htbin3": [ 1313.898 , 79.823 ],
        #                           "RT1BT0htbin4": [ 1085.601 , 80.286 ],
        #                           "RT1BT0htbin5": [ 896.564 , 80.507 ],
        #                           "RT1BT0htbin6": [ 966.544 , 85.165 ],
        #                           "RT1BT0htbin7": [ 817.268 , 84.914 ],
        #                           "RT1BT1htbin0": [ 356.207 , 39.411 ],
        #                           "RT1BT1htbin1": [ 407.412 , 73.73 ],
        #                           "RT2BTALLhtbin0": [ 515.245 , 46.506 ],
        #                           "RT2BTALLhtbin1": [ 279.847 , 50.772 ]}}

        if self.ttbb: #no SF
            DDyields = {'2016' : {"RT1BT0htbin0": [ 1285.315 , 58.978 ],
                                  "RT1BT0htbin1": [ 1267.75 , 65.896 ],
                                  "RT1BT0htbin2": [ 967.41 , 59.107 ],
                                  "RT1BT0htbin3": [ 906.027 , 64.763 ],
                                  "RT1BT0htbin4": [ 580.652 , 50.103 ],
                                  "RT1BT0htbin5": [ 418.766 , 45.62 ],
                                  "RT1BT0htbin6": [ 627.496 , 63.189 ],
                                  "RT1BT0htbin7": [ 685.535 , 77.165 ],
                                  "RT1BT1htbin0": [ 158.878 , 22.506 ],
                                  "RT1BT1htbin1": [ 231.863 , 49.82 ],
                                  "RT2BTALLhtbin0": [ 372.107 , 39.391 ],
                                  "RT2BTALLhtbin1": [ 220.861 , 49.844 ]},
                        '2017' : {"RT1BT0htbin0": [ 1788.351 , 78.258 ],
                                  "RT1BT0htbin1": [ 1624.404 , 80.543 ],
                                  "RT1BT0htbin2": [ 1119.329 , 65.107 ],
                                  "RT1BT0htbin3": [ 1062.758 , 73.8 ],
                                  "RT1BT0htbin4": [ 645.985 , 55.428 ],
                                  "RT1BT0htbin5": [ 458.032 , 48.671 ],
                                  "RT1BT0htbin6": [ 628.934 , 65.214 ],
                                  "RT1BT0htbin7": [ 559.465 , 65.264 ],
                                  "RT1BT1htbin0": [ 259.686 , 34.707 ],
                                  "RT1BT1htbin1": [ 336.297 , 66.697 ],
                                  "RT2BTALLhtbin0": [ 479.887 , 48.283 ],
                                  "RT2BTALLhtbin1": [ 288.485 , 61.565 ]},
                        '2018' : {"RT1BT0htbin0": [ 2443.353 , 91.076 ],
                                  "RT1BT0htbin1": [ 2175.12 , 91.164 ],
                                  "RT1BT0htbin2": [ 1871.815 , 91.709 ],
                                  "RT1BT0htbin3": [ 1381.048 , 81.875 ],
                                  "RT1BT0htbin4": [ 1133.296 , 81.831 ],
                                  "RT1BT0htbin5": [ 935.062 , 82.161 ],
                                  "RT1BT0htbin6": [ 1011.82 , 86.973 ],
                                  "RT1BT0htbin7": [ 857.528 , 86.57 ],
                                  "RT1BT1htbin0": [ 379.788 , 40.829 ],
                                  "RT1BT1htbin1": [ 439.726 , 75.73 ],
                                  "RT2BTALLhtbin0": [ 553.756 , 48.588 ],
                                  "RT2BTALLhtbin1": [ 302.783 , 53.15 ]}}


        if self.mergehtbins:
            DDyields = {'2016':{'RT1BT0':[7772.67, 171.14],
                                'RT1BT1':[445.41, 46.33],
                                'RT2BTALL':[762.17, 62.97]},
                        '2017':{'RT1BT0':[9096.4, 195.68],
                                'RT1BT1':[666.95, 65.56],
                                'RT2BTALL':[925.37, 74.51]},
                        '2018':{'RT1BT0':[13232.26, 244.66],
                                'RT1BT1':[891.81, 73.06],
                                'RT2BTALL':[1044.58, 74.86]}}
        
        self.DDyields = DDyields[year]

    def getsels(self, systyp='nosys', isMC=True, isttbb=False):

        wgtstr = '*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi

        basesel = ' nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9'
        if isttbb: 
            basesel = ' nfunky_leptons==0 && isttbb==1 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9'
        binsels = self.getbinsels('nosys')

        systsels = {'pileup'   : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*puWeight',
                    'trigger' : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*trigSF_',
                    'isr'      : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*isr_',
                    'fsr'      : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*fsr_',
                    'ME'      : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*MErn_',
                    'pdf'      : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*pdfrn_',
                    'btagHF'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagHF*btagSF_btagHF',
                    'btagLF'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagLF*btagSF_btagLF',
                    'btagHFstats1'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagHFstats1*btagSF_btagHFstats1',
                    'btagLFstats1'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagLFstats1*btagSF_btagLFstats1',
                    'btagHFstats2'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagHFstats2*btagSF_btagHFstats2',
                    'btagLFstats2'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagLFstats2*btagSF_btagLFstats2',
                    'btagCFerr1'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagCFerr1*btagSF_btagCFerr1',
                    'btagCFerr2'   : basesel+self.metfilters+')*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bSFcorr_btagCFerr2*btagSF_btagCFerr2',
                    'DeepAK8TopSF' : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bsTopSF_DeepAK8TopSF_',
                    'DeepAK8WSF' : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi+'*bsWSF_DeepAK8WSF_',
                    'ResTopEff' : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTmissSF_nosys*'+self.lumi+'*RTeffSF_ResTopEff_',
                    'ResTopMiss' : basesel+self.metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*'+self.lumi+'*RTmissSF_ResTopMiss_',
        } 
        
        # self.systlist = ['nosys', 'jerUp', 'jerDown', 'jesUp', 'jesDown', 'btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down', 'DeepAK8TopSF_Up', 'DeepAK8TopSF_Down', 'DeepAK8WSF_Up', 'DeepAK8WSF_Down']
        
        if systyp =='nosys':
            if not isMC:
                sel = basesel+self.datatrigs+')'#trigcorr
                return sel
            elif isMC:
                sel = basesel+self.metfilters+')'+wgtstr
                return sel

        elif systyp not in ['jer','jes']:
            systselup = systsels[systyp]+'Up'
            systseldown = systsels[systyp]+'Down'
            if systyp in ['btagLF','btagHF','btagLFstats1', 'btagHFstats1', 'btagLFstats2','btagHFstats2', 'btagCFerr1','btagCFerr2']:
                systselup = systselup.replace('*btagSF','Up*btagSF')
                systseldown = systseldown.replace('*btagSF','Down*btagSF')
            return [systselup, systseldown], [binsels, binsels]

        elif systyp in ['jer','jes']:
            sel= []; binsels=[]
            updown=['Up','Down']
            for ud in updown:
                syst=systyp+ud
                wgtstr = '*weight*btagSF_'+syst+'*puWeight*bSFcorr_nosys*trigSF_'+syst+'*bsWSF_'+syst+'*bsTopSF_'+syst+'*RTeffSF_'+syst+'*RTmissSF_'+syst+'*'+self.lumi
                basesel = ' nfunky_leptons==0 && ht_'+syst+'>=700 && nbjets_'+syst+'>=3 && njets_'+syst+'>=9'
                binsel = self.getbinsels(syst)
                systsel = basesel+self.metfilters+')'+wgtstr
                sel.append(systsel)
                binsels.append(binsel)
            return sel, binsels

    def getbinsels(self, syst):
        binsels = {'RT1BT0htbin0':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=700 && ht_'+syst+'<800',
                   'RT1BT0htbin1':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=800 && ht_'+syst+'<900', 
                   'RT1BT0htbin2':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=900 && ht_'+syst+'<1000', 
                   'RT1BT0htbin3':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=1000 && ht_'+syst+'<1100',
                   'RT1BT0htbin4':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=1100 && ht_'+syst+'<1200',
                   'RT1BT0htbin5':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=1200 && ht_'+syst+'<1300',
                   'RT1BT0htbin6':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=1300 && ht_'+syst+'<1500', 
                   'RT1BT0htbin7':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0 && ht_'+syst+'>=1500',
                   'RT1BT1htbin0':'nrestops_'+syst+'==1 && nbstops_'+syst+'>=1 && ht_'+syst+'<1400',
                   'RT1BT1htbin1':'nrestops_'+syst+'==1 && nbstops_'+syst+'>=1 && ht_'+syst+'>=1400',
                   'RT2BTALLhtbin0':'nrestops_'+syst+'>=2 && nbstops_'+syst+'>=0 && ht_'+syst+'<1100',
                   'RT2BTALLhtbin1':'nrestops_'+syst+'>=2 && nbstops_'+syst+'>=0 && ht_'+syst+'>=1100'}

        if self.mergehtbins:
            binsels = {'RT1BT0':'nrestops_'+syst+'==1 && nbstops_'+syst+'==0',
                       'RT1BT1':'nrestops_'+syst+'==1 && nbstops_'+syst+'>=1',
                       'RT2BTALL':'nrestops_'+syst+'>=2 && nbstops_'+syst+'>=0'}
        return binsels
    

    def getyield(self, hist, verbose=False):
        errorVal = r.Double(0)
        minbin=0
        maxbin=hist.GetNbinsX()+1
        hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
        if verbose:
            print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
        return hyield,  errorVal
    
    def fixref(self, rootfile, hist):
        hist.SetDirectory(0)
        rootfile.Close()

    def makeroot(self, infile, treename, options="read"):
        rfile = r.TFile(infile, options)
        tree = rfile.Get(treename)
        return rfile, tree
