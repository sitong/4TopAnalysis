#baseproducer to be inhrited by other modules 
#should call leptonSFproducer, maybe other sfproducers
import numpy as np
import math
import logging
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest
from PhysicsTools.NanoTrees.helpers.ConfigHelper import LepSetter, JetSetter

import PhysicsTools.NanoTrees.helpers.weighthelper as xw
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
import PhysicsTools.NanoTrees.helpers.LepTrigHelper as lth

class BaseProducer(Module):
    
    def __init__(self, channel, year):
        self.skim = True

        self.channel = channel
        self.year    = year
        self.BDTtrain = False
        self.eventonly = True
        self.addgenjet5 = False
        
    def beginJob(self):
        self.LS = LepSetter(self.channel, self.year)
        self.JS = JetSetter(self.channel, self.year)
        # self.BS = BoostSetter(self.year)
        pass 

    def endJob(self):
        pass

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        print "inputFile", inputFile
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        self.isMC = bool(inputTree.GetBranch('genWeight'))
        self.XWgt = xw.GetKnownWeight(self.infile, self.year, self.isMC) #xsec weight from file
        self.isQCD = False
        if self.addgenjet5:
            if 'QCD' in str(inputFile) and 'GenJets5' not in str(inputFile):
                self.isQCD = True
                print 'QCD file, will add genjet5 weight'
        print 'initializing trees...'
        #basic variables
        self.out.branch("ismc", "I")
        self.out.branch("event", "L")
        self.out.branch("run", "I")
        self.out.branch("lumi", "I")
        self.out.branch("weight", "F") 
        self.out.branch("npv", "I")
        self.out.branch("met", "F")
        # self.out.branch("puweighttest", "F")
        if self.isMC:
            self.out.branch("genweight", "F") 
            self.out.branch("gen_njets",   "I") 
            # self.out.branch("genjet5flag", "I") 

        #jet variables
        self.out.branch("njets", "I")
        # self.out.branch("passjetpresel", "F", lenVar="nJet")
        if not self.eventonly:
            self.out.branch("jetpt", "F",  lenVar="nJet")
            self.out.branch("jeteta", "F", lenVar="nJet")
        self.out.branch("nbjets", "I")
        self.out.branch("jet1pt", "F")
        self.out.branch("jet2pt", "F")
        self.out.branch("jet3pt", "F")
        self.out.branch("jet4pt", "F")
        self.out.branch("jet5pt", "F")
        self.out.branch("jet6pt", "F")
        self.out.branch("jet7pt", "F")
        self.out.branch("jet8pt", "F")
        self.out.branch("jet9pt", "F")
        self.out.branch("jet10pt", "F")
        self.out.branch("leadbpt", "F")
        self.out.branch("metovsqrtht", "F")
        self.out.branch("ht", "F")
        self.out.branch("centrality", "F")
        self.out.branch("htratio", "F")
        self.out.branch("meanbdisc", "F")
        self.out.branch("aplanarity", "F")
        self.out.branch("sphericity", "F")
        self.out.branch("bjht", "F")
        self.out.branch("dphij1j2", "F")
        self.out.branch("dphib1b2", "F")
        self.out.branch("detaj1j2", "F")
        self.out.branch("detab1b2", "F")
        self.out.branch("dphij1met", "F")
        self.out.branch("dphij2met", "F")
        self.out.branch("qglsum", "F")
        self.out.branch("qglprod", "F")
        self.out.branch("qglsum30", "F")
        # self.out.branch("fwm0", "F")
        # self.out.branch("fwm1", "F")
        # self.out.branch("fwm2", "F")
        # self.out.branch("fwm3", "F")
        # self.out.branch("fwm4", "F")
        # self.out.branch("fwm5", "F")
        # self.out.branch("fwm6", "F")
        # self.out.branch("fwm7", "F")
        # self.out.branch("fwm8", "F")
        # self.out.branch("fwm9", "F")
        self.out.branch("fwm10", "F")
        self.out.branch("fwm20", "F")
        self.out.branch("fwm30", "F")

        #leptons
        # self.out.branch("nleptons", "I")
        # self.out.branch("nmuons", "I")
        # self.out.branch("nelectrons", "I")
        # self.out.branch("lep1eta", "F")
        # self.out.branch("lep1phi", "F")
        # self.out.branch("lep1mass", "F")
        # self.out.branch("lep1q", "I")
        # self.out.branch("lep1pdgid", "I")
        # self.out.branch("lep1ptplusht", "F")
        # self.out.branch("lep1pt", "F")
        # self.out.branch("lep2pt", "F")
        # self.out.branch("lep3pt", "F")
        # self.out.branch("lep1id", "I")
        # self.out.branch("lep2id", "I")
        # self.out.branch("lep3id", "I")

        self.out.branch("goodverticesflag", "I")
        self.out.branch("haloflag", "I")
        self.out.branch("HBHEflag", "I")
        self.out.branch("HBHEisoflag", "I")
        self.out.branch("ecaldeadcellflag", "I")
        self.out.branch("badmuonflag", "I")
        if self.year=='2016':
            self.out.branch("eeBadScFilterflag","I")

        # if not self.isMC:
            #triggers and flags
            # self.out.branch("metfilterflag", "I")
        self.out.branch("passtrig", "I")

            # self.out.branch("passtrigmu", "I")
            # self.out.branch("passtrigel", "I")
            # self.out.branch("passtriglep", "I")
            # self.out.branch("passtriglepOR", "I")
            # self.out.branch("passtrigmuht", "I")
            # self.out.branch("passtrigelht", "I")
            # self.out.branch("passtrigmetmht", "I")

        if self.year=='2016':
            self.out.branch("trigHLT_PFHT450_SixJet40_BTagCSV_p056", "I")
            self.out.branch("trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056", "I")
            self.out.branch("trigHLT_PFHT900", "I")
            self.out.branch("trigHLT_Ele27_WPTight_Gsf", "I")
            self.out.branch("trigHLT_IsoMu24", "I")
            self.out.branch("trigHLT_IsoTkMu24", "I")
        elif self.year=='2017':
            self.out.branch("trigHLT_PFHT430_SixJet40_BTagCSV_p080", "I")
            self.out.branch("trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075", "I")
            self.out.branch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2", "I")
            self.out.branch("trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5", "I")
            self.out.branch("trigHLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0", "I")
            self.out.branch("trigHLT_PFHT1050", "I")
            self.out.branch("trigHLT_Ele27_WPTight_Gsf", "I")
            self.out.branch("trigHLT_Ele32_WPTight_Gsf", "I")
            self.out.branch("trigHLT_IsoMu27", "I")
            self.out.branch("trigHLT_IsoMu30", "I")
        elif self.year=='2018':
            self.out.branch("trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59", "I")
            self.out.branch("trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94", "I")
            self.out.branch("trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5", "I")
            self.out.branch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2", "I")
            self.out.branch("trigHLT_PFHT1050", "I")
            self.out.branch("trigHLT_Ele27_WPTight_Gsf", "I")
            self.out.branch("trigHLT_Ele32_WPTight_Gsf", "I")
            self.out.branch("trigHLT_Ele35_WPTight_Gsf", "I")
            self.out.branch("trigHLT_IsoMu27", "I")
            self.out.branch("trigHLT_IsoMu30", "I")

    def EventFiller(self, event):
        # ismc = sel#bool(event.genWeight)
        met       = Object(event,     "MET") 
        if self.isMC: 
            genjets   = Collection(event, "GenJet")
        flags     = Object(event,"Flag")

        #lepton selection+ jet cleaning if needed
        if self.LS.clean_jetsvleps:
            ak4jets, SelEls, SelMus, SelLeps =self.LS.getLeps(event)
        else:
            ak4jets   = Collection(event, "Jet")
            # SelEls, SelMus, SelLeps =self.LS.getLeps(event)

        # nels=0; nmus=0; nleps=0
        # l1pt=0.0; l1pdg=0; l1eta=0.0; l1phi=0.0; l1mass=0.0; l1q=0; l1ptplusht=0.0
        # l2pt=0.0; l2pdg=0; l3pt=0.0; l3pdg=0
        # nmus = len(SelMus)
        # nels = len(SelEls)
        # nleps = len(SelLeps)
        # if nleps>0:
        #     l1pt=SelLeps[0].pt; l1pdg=SelLeps[0].pdgId; l1eta=SelLeps[0].eta; l2phi=SelLeps[0].phi; l1mass=SelLeps[0].mass; l1q=SelLeps[0].charge
        # elif nleps>1:
        #     l2pt=SelLeps[1].pt; l2pdg=SelLeps[1].pdgId
        # elif nleps>2:
        #     l3pt=SelLeps[2].pt; l3pdg=SelLeps[2].pdgId

        # trigel, trigmu, triglep, passtrigmuht, passtrigelht, passtrigmetmht, passtriglepor = 0,0,0,0,0,0,0
        # if not self.isMC and nleps>0:
        #     trigel, trigmu, triglep, passtrigmuht, passtrigelht, passtrigmetmht, passtriglepor = lth.getleptriggers(event, self.infile)

        #get jets
        cleanjets, bjets, passjetpresel, njets, nbjets, ht, bjht, qglsum, qglsum30, qglprod, jetpts, jetetas, centrality, meanbdisc = self.JS.getJets(self.year, event, ak4jets, getvars=True)

        #other jet calculations
        j1pt=0.0; j2pt=0.0; j3pt=0.0; j4pt=0.0; j5pt=0.0; j6pt=0.0; j7pt=0.0; j8pt=0.0; j9pt=0.0; j10pt=0.0 

        htratio = 0.0
        # print 'njets', njets
        if njets>=1:
            j1pt = jetpts[0] #pt of 8th jet
        if njets>=2:
            j2pt = jetpts[1] #pt of 8th jet
        if njets>=3:
            j3pt = jetpts[2] #pt of 8th jet
        if njets>=4:
            j4pt = jetpts[3] #pt of 8th jet
        if njets>=5:
            j5pt = jetpts[4] #pt of 8th jet
        if njets>=6:
            j6pt = jetpts[5] #pt of 8th jet
            if ht>0.0:
                htratio = (j1pt+j2pt+j3pt+j4pt+j5pt+j6pt)/ht
        if njets>=7:
            j7pt = jetpts[6] #pt of 8th jet
        if njets>=8:
            j8pt = jetpts[7] #pt of 8th jet
        if njets>=9:
            j9pt = jetpts[8] #pt of 8th jet
        if njets>=10:
            j10pt = jetpts[9] #pt of 8th jet
        # print j1pt, j2pt, j3pt, j4pt, j5pt, j6pt, j7pt, j8pt, j9pt, j10pt 

        sph, apl = phys.EventShape(cleanjets)
        #fwmoments
        # print len(ak4jets)
        legendreP0 = 0.0; legendreP1 = 0.0; legendreP2 = 0.0; legendreP3 = 0.0; legendreP4 = 0.0; legendreP5 = 0.0; legendreP6 = 0.0
        legendreP7 = 0.0; legendreP8 = 0.0; legendreP9 = 0.0; legendreP10 = 0.0; legendreP20 = 0.0; legendreP30 = 0.0
        # if (njets>=self.JS.base_min_njets and nbjets>=self.JS.base_min_nbjets and ht>=self.JS.base_min_ht):
        if njets>6:
            # legendreP0 = phys.Calc_FoxWolframMoment(0, 6, cleanjets)
            # legendreP1 = phys.Calc_FoxWolframMoment(1, 6, cleanjets)
            # legendreP2 = phys.Calc_FoxWolframMoment(2, 6, cleanjets)
            # legendreP3 = phys.Calc_FoxWolframMoment(3, 6, cleanjets)
            # legendreP4 = phys.Calc_FoxWolframMoment(4, 6, cleanjets)
            # legendreP5 = phys.Calc_FoxWolframMoment(5, 6, cleanjets)
            # legendreP6 = phys.Calc_FoxWolframMoment(6, 6, cleanjets)
            # legendreP7 = phys.Calc_FoxWolframMoment(7, 6, cleanjets)
            # legendreP8 = phys.Calc_FoxWolframMoment(8, 6, cleanjets)
            # legendreP9 = phys.Calc_FoxWolframMoment(9, 6, cleanjets)
            legendreP10 = phys.Calc_FoxWolframMoment(10, 6, cleanjets)
            legendreP20 = phys.Calc_FoxWolframMoment(20, 6, cleanjets)
            legendreP30 = phys.Calc_FoxWolframMoment(30, 6, cleanjets)

        leadbpt=0.0
        if nbjets>0:
            leadbpt  = bjets[0].pt
        mosht    = 0.0
        if ht>0:
            mosht    =  (met.pt/math.sqrt(ht))
        dphib1b2 = 0.0; detab1b2 = 0.0; dphij1j2 = 0.0; detaj1j2 = 0.0; dphij1met = 0.0; dphij2met = 0.0
        if njets>0:
            dphij1met = deltaPhi(cleanjets[0], met.phi)
        if njets>1:
            dphij1j2 = deltaPhi(cleanjets[0].phi, cleanjets[1].phi)
            detaj1j2 = cleanjets[0].eta-cleanjets[1].eta
            dphij2met = deltaPhi(cleanjets[1], met.phi)
        if nbjets>1:
            # print nbjets
            dphib1b2 = deltaPhi(bjets[0].phi, bjets[1].phi)
            detab1b2 = bjets[0].eta-bjets[1].eta
        # if nleps>0: 
        #     l1ptplusht=(SelLeps[0].pt)+ht

            
        #cross sectional weights
        XWeight = 1.0
        if self.isMC:
            XWeight = self.XWgt
            if not self.BDTtrain: 
                if self.addgenjet5:
                    if event.nGenJet<5 and self.isQCD: #genjet5-influenced weights
                        XWeight = xw.GetLowNJetsWeight(self.infile, self.year)
            elif self.BDTtrain: #special weights
                XWeight = xw.GetBDTweight(self.infile)
            if event.genWeight < 0: # get the right sign
                XWeight = -XWeight
                
        # #variables for genjet5
        # gj5bool = False; ngoodgjs = 0
        # if self.isMC:
        #     if event.nGenJet >=5:
        #         for gj in genjets:
        #             if (gj.pt>=20 and gj.eta<2.4):
        #                 ngoodgjs+=1
        #     if ngoodgjs>=5:
        #         gj5bool = True

        #mc/data changes
        genweight = 1.0; ngenjets=0
        if self.isMC:
            genweight = event.genWeight
            ngenjets = event.nGenJet 
        
        # event=-1
        # if event.event<sys.maxint:
        #     event = event.event#issues with int size, maybe use python 3 or longs?
        self.out.fillBranch("event",            long(event.event))
        self.out.fillBranch("ismc",             int(self.isMC))  
        self.out.fillBranch("run",              int(event.run))
        self.out.fillBranch("lumi",             event.luminosityBlock)
        self.out.fillBranch("weight",           XWeight)
        self.out.fillBranch("met",              met.pt) 
        self.out.fillBranch("npv",              event.PV_npvs) 
        #test
        # self.out.fillBranch("puweighttest", event.puWeightUp)
        if self.isMC:
            self.out.fillBranch("genweight",        event.genWeight )
            self.out.fillBranch("gen_njets",        event.nGenJet )
            # self.out.fillBranch("genjet5flag",                               int(gj5bool))
            
        self.out.fillBranch("njets",            njets)
        self.out.fillBranch("nbjets",           nbjets)
        # self.out.fillBranch("passjetpresel",    list(passjetpresel) )        
        if not self.eventonly:
            self.out.fillBranch("jetpt",            list(jetpts) )
            self.out.fillBranch("jeteta",           list(jetetas) )
        self.out.fillBranch("jet1pt",           j1pt)
        self.out.fillBranch("jet2pt",           j2pt)
        self.out.fillBranch("jet3pt",           j3pt)
        self.out.fillBranch("jet4pt",           j4pt)
        self.out.fillBranch("jet5pt",           j5pt)
        self.out.fillBranch("jet6pt",           j6pt)
        self.out.fillBranch("jet7pt",           j7pt)
        self.out.fillBranch("jet8pt",           j8pt)
        self.out.fillBranch("jet9pt",           j9pt)
        self.out.fillBranch("jet10pt",          j10pt)
        self.out.fillBranch("leadbpt",          leadbpt)
        self.out.fillBranch("bjht",             bjht)
        self.out.fillBranch("dphij1j2",         dphij1j2)
        self.out.fillBranch("detaj1j2",         detaj1j2)
        self.out.fillBranch("dphib1b2",         dphib1b2)
        self.out.fillBranch("detab1b2",         detab1b2)
        self.out.fillBranch("dphij1met",        dphij1met)
        self.out.fillBranch("dphij2met",        dphij2met)
        self.out.fillBranch("qglsum",           qglsum)
        self.out.fillBranch("qglsum30",         qglsum30)
        self.out.fillBranch("qglprod",          qglprod)
        self.out.fillBranch("ht",               ht)
        self.out.fillBranch("htratio",          htratio)
        self.out.fillBranch("centrality",       centrality)
        self.out.fillBranch("meanbdisc",        meanbdisc)
        self.out.fillBranch("sphericity",       sph)
        self.out.fillBranch("aplanarity",       apl)
        self.out.fillBranch("metovsqrtht",      mosht)
        # self.out.fillBranch("fwm0",             legendreP0)
        # self.out.fillBranch("fwm1",             legendreP1)
        # self.out.fillBranch("fwm2",             legendreP2)
        # self.out.fillBranch("fwm3",             legendreP3)
        # self.out.fillBranch("fwm4",             legendreP4)
        # self.out.fillBranch("fwm5",             legendreP5)
        # self.out.fillBranch("fwm6",             legendreP6)
        # self.out.fillBranch("fwm7",             legendreP7)
        # self.out.fillBranch("fwm8",             legendreP8)
        # self.out.fillBranch("fwm9",             legendreP9)
        self.out.fillBranch("fwm10",            legendreP10)
        self.out.fillBranch("fwm20",            legendreP20)
        self.out.fillBranch("fwm30",            legendreP30)
 

        # self.out.fillBranch("nleptons",         nleps)
        # self.out.fillBranch("nelectrons",       nels)
        # self.out.fillBranch("nmuons",           nmus)
        # self.out.fillBranch("lep1pt",           l1pt)
        # self.out.fillBranch("lep2pt",           l2pt)
        # self.out.fillBranch("lep3pt",           l3pt)
        # self.out.fillBranch("lep1id",           l1pdg)
        # self.out.fillBranch("lep2id",           l2pdg)
        # self.out.fillBranch("lep3id",           l3pdg)
        # self.out.fillBranch("lep1eta",          l1eta)
        # self.out.fillBranch("lep1phi",          l1phi)
        # self.out.fillBranch("lep1mass",         l1mass)
        # self.out.fillBranch("lep1q",            l1q)
        # self.out.fillBranch("lep1ptplusht",     l1pptlusht)

        event.flags   = Object(event,"Flag")
        # self.out.fillBranch("metfilterflag",                             flags.METFilters)
        self.out.fillBranch("goodverticesflag",                          flags.goodVertices)
        self.out.fillBranch("haloflag",                                  flags.globalSuperTightHalo2016Filter)
        self.out.fillBranch("HBHEflag",                                  flags.HBHENoiseFilter)
        self.out.fillBranch("HBHEisoflag",                               flags.HBHENoiseIsoFilter)
        self.out.fillBranch("ecaldeadcellflag",                          flags.EcalDeadCellTriggerPrimitiveFilter)
        self.out.fillBranch("badmuonflag",                               flags.BadPFMuonFilter)
        if self.year=='2016':
            self.out.fillBranch("eeBadScFilterflag",                               flags.eeBadScFilter)

        #passtrig=1
        # if not self.isMC:            
        passtrig = 0
        if self.year=='2016':
            trig1=0; trig2=0
            if bool(self.intree.GetBranch('HLT_PFHT400_SixJet30_DoubleBTagCSV_p056')):
                trig1=event.HLT_PFHT400_SixJet30_DoubleBTagCSV_p056
            if bool(self.intree.GetBranch('HLT_PFHT450_SixJet40_BTagCSV_p056')):
                trig2=event.HLT_PFHT450_SixJet40_BTagCSV_p056
            if trig1 or trig2:
                passtrig=1
            self.out.fillBranch("passtrig",                                int(passtrig))
            self.out.fillBranch("trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056", int(trig1))
            self.out.fillBranch("trigHLT_PFHT450_SixJet40_BTagCSV_p056",       int(trig2))
            #extras
            puretrig = 0; el1=0; mu1=0; mu2=0
            if bool(self.intree.GetBranch('HLT_PFHT900')):
                puretrig = event.HLT_PFHT900
            if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                el1=event.HLT_Ele27_WPTight_Gsf
            if bool(self.intree.GetBranch('HLT_IsoMu24')):
                mu1=event.HLT_IsoMu24
            if bool(self.intree.GetBranch('HLT_IsoTkMu24')):
                mu2=event.HLT_IsoTkMu24
            self.out.fillBranch("trigHLT_PFHT900",             int(puretrig))
            self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
            self.out.fillBranch("trigHLT_IsoMu24",             int(mu1))
            self.out.fillBranch("trigHLT_IsoTkMu24",           int(mu2))

        elif self.year=='2017':
            trig1=0;trig2=0;trig3=0;trig4=0;trig5=0
            if  bool(self.intree.GetBranch('HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0')):
                trig1=event.HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0
            if bool(self.intree.GetBranch('HLT_PFHT430_SixJet40_BTagCSV_p080')):
                trig2=event.HLT_PFHT430_SixJet40_BTagCSV_p080
            if  bool(self.intree.GetBranch('HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5')):
                trig3=event.HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5
            if  bool(self.intree.GetBranch('HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2')):
                trig4=event.HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2
            if bool(self.intree.GetBranch('HLT_PFHT380_SixJet32_DoubleBTagCSV_p075')):
                trig5=event.HLT_PFHT380_SixJet32_DoubleBTagCSV_p075
            if any([trig1,trig2,trig3,trig4,trig5]):
                passtrig=1
            self.out.fillBranch("passtrig",                                                   int(passtrig))
            self.out.fillBranch("trigHLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0",  int(trig1))
            self.out.fillBranch("trigHLT_PFHT430_SixJet40_BTagCSV_p080",                          int(trig2))
            self.out.fillBranch("trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5",                       int(trig3))
            self.out.fillBranch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2",                 int(trig4))
            self.out.fillBranch("trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075",                    int(trig5))
                #extras
            puretrig = 0; el1=0; el2=0; mu1=0; mu2=0
            if bool(self.intree.GetBranch('HLT_PFHT1050')):
                puretrig = event.HLT_PFHT1050
            if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                el1=event.HLT_Ele27_WPTight_Gsf
            if bool(self.intree.GetBranch('HLT_Ele32_WPTight_Gsf')):
                el2=event.HLT_Ele32_WPTight_Gsf
            if bool(self.intree.GetBranch('HLT_IsoMu27')):
                mu1=event.HLT_IsoMu27
            if bool(self.intree.GetBranch('HLT_IsoMu30')):
                mu2=event.HLT_IsoMu30
            self.out.fillBranch("trigHLT_PFHT1050",         int(puretrig))
            self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
            self.out.fillBranch("trigHLT_Ele32_WPTight_Gsf",   int(el2))
            self.out.fillBranch("trigHLT_IsoMu27",             int(mu1))
            self.out.fillBranch("trigHLT_IsoMu30",             int(mu2))
        elif self.year=='2018':
            trig1=0;trig2=0;trig3=0;trig4=0;
            if bool(self.intree.GetBranch('HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59')):
                trig1=event.HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59
            if bool(self.intree.GetBranch('HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94')):
                trig2=event.HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94
            if bool(self.intree.GetBranch('HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5')):
                trig3=event.HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5
            if bool(self.intree.GetBranch('HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2')):
                trig4=event.HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2
            if any([trig1,trig2,trig3,trig4]):
                passtrig=1
            self.out.fillBranch("passtrig",                                          int(passtrig))
            self.out.fillBranch("trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59",         int(trig1))
            self.out.fillBranch("trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94",   int(trig2))
            self.out.fillBranch("trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5",          int(trig3))
            self.out.fillBranch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2",    int(trig4))
            #extras 
            puretrig = 0; el1=0; el2=0; el3=0; mu1=0; mu2=0
            if bool(self.intree.GetBranch('HLT_PFHT1050')):
                puretrig = event.HLT_PFHT1050
            if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                el1=event.HLT_Ele27_WPTight_Gsf
            if bool(self.intree.GetBranch('HLT_Ele32_WPTight_Gsf')):
                el2=event.HLT_Ele32_WPTight_Gsf
            if bool(self.intree.GetBranch('HLT_Ele35_WPTight_Gsf')):
                el3=event.HLT_Ele35_WPTight_Gsf
            if bool(self.intree.GetBranch('HLT_IsoMu27')):
                mu1=event.HLT_IsoMu27
            if bool(self.intree.GetBranch('HLT_IsoMu30')):
                mu2=event.HLT_IsoMu30
            self.out.fillBranch("trigHLT_PFHT1050",         int(puretrig))
            self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
            self.out.fillBranch("trigHLT_Ele32_WPTight_Gsf",   int(el2))
            self.out.fillBranch("trigHLT_Ele35_WPTight_Gsf",   int(el3))
            self.out.fillBranch("trigHLT_IsoMu27",             int(mu1))
            self.out.fillBranch("trigHLT_IsoMu30",             int(mu2))

        return njets, nbjets

    def analyze(self, event):
        # self.EventFiller(event)
        # return True
        if self.skim:
            njets,nbjets = self.EventFiller(event)
            # print nleps
            if njets>=4:
                return True
            else:
                return False
        else:
            njets,nbjets=self.EventFiller(event)
            return True
        # if keepevent:
        #     return True
        # return False

BaseTrees2016_0lep = lambda : BaseProducer('0lep','2016')
BaseTrees2016_1lep = lambda : BaseProducer('1lep','2016')

BaseTrees2017_0lep = lambda : BaseProducer('0lep','2017')
BaseTrees2017_1lep = lambda : BaseProducer('1lep','2017')

BaseTrees2018_0lep = lambda : BaseProducer('0lep','2018')
BaseTrees2018_1lep = lambda : BaseProducer('1lep','2018')
