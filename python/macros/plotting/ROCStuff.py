#effplottools + PlotROCCurves
# import root_numpy as rn
# import argparse
# import os
import numpy as np
# import pandas as pd
# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# import re
import ROOT as r

#creates one roc object
class ROCCurve:
    #files and trees are root files and trees
    def __init__(self, discvar, sfile, bfile, stree, btree, ssel, bsel, nbins=200, xmin=-1.0, xmax=1.0):
        sighist = r.TH1D("sig", "sig", nbins, xmin, xmax)
        bkghist = r.TH1D("bkg", "bkg", nbins, xmin, xmax)
        stree.Draw(discvar+">>sig", ssel)
        btree.Draw(discvar+">>bkg", bsel)
        self.xmin = xmin
        self.xmax = xmax
        self.sighist = sighist #hists with disc values for signal
        self.bgdhist = bkghist 
        self.discvar = discvar

    #creates roc curve from signal and background histograms
    def computeROCCurve(self, plotbkgrej=False):
        axis = self.sighist.GetXaxis()
        bmin = axis.FindBin(self.xmin)
        bmax = axis.FindBin(self.xmax)
        print 'bins', bmin-1, bmax+1
        nbins = self.sighist.GetNbinsX()
        if not (self.bgdhist.GetNbinsX()==nbins):
            raise AssertionError()
        xbins=np.empty(nbins+1, dtype='float64'); binw=np.empty(nbins, dtype='float64')
        for i in range(nbins):
            xbins[i]=(self.sighist.GetBinLowEdge(i+1))
            binw[i]=(self.sighist.GetBinWidth(i+1))
            # print i, nbins
        xbins[nbins] = (xbins[nbins-1]+binw[nbins-1])
        print 'xbins',xbins
        # npassSig = np.empty(nbins, dtype='int32'); npassBkg = np.empty(nbins, dtype='int32')
        npassSig = np.empty(nbins+1, dtype='float64'); npassBkg = np.empty(nbins+1, dtype='float64')
        effsig = np.empty(nbins+2, dtype='float64'); effbkg = np.empty(nbins+2, dtype='float64')

        # sigtotal0 = self.sighist.Integral(bmin-1, bmax+1)
        # bkgtotal0 = self.bgdhist.Integral(bmin-1, bmax+1)
        # print nbins+1
        sigtotal = self.sighist.Integral(0, nbins+1)
        bkgtotal = self.bgdhist.Integral(0, nbins+1)
        # print sigtotal, sigtotal0
        # print bkgtotal, bkgtotal0
        # print 0, nbins+1, bmin-1, bmax+1
        for ibin in range(nbins+1): #nbins or nbins+1
            npassSig[ibin]=(self.sighist.Integral(ibin, nbins+1))
            npassBkg[ibin]=(self.bgdhist.Integral(ibin, nbins+1))
            effsig[ibin]=((npassSig[ibin])/(sigtotal))
            # print ibin,'to', nbins+1, npassSig[ibin]/sigtotal, npassBkg[ibin]/bkgtotal 
            if plotbkgrej:
                effbkg[ibin]=(1.0 - ((npassBkg[ibin])/(bkgtotal)))
            elif not plotbkgrej:
                effbkg[ibin]=((npassBkg[ibin])/(bkgtotal))
        # print 'int to n+2:', self.sighist.Integral(0 , nbins+2)/sigtotal, self.bgdhist.Integral(0 , nbins+2)/bkgtotal
        # print 'int to n+1:', self.sighist.Integral(0 , nbins+1)/sigtotal, self.bgdhist.Integral(0 , nbins+1)/bkgtotal
        # print 'int to n:', self.sighist.Integral(0 , nbins)/sigtotal, self.bgdhist.Integral(0 , nbins)/bkgtotal
        # print 'LAST int to n+2:', self.sighist.Integral(nbins , nbins+2)/sigtotal, self.bgdhist.Integral(nbins , nbins+2)/bkgtotal
        # print 'LAST int to n+1:', self.sighist.Integral(nbins , nbins+1)/sigtotal, self.bgdhist.Integral(nbins , nbins+1)/bkgtotal
        # print 'LAST int to n:', self.sighist.Integral(nbins-1 , nbins)/sigtotal, self.bgdhist.Integral(nbins-1 , nbins)/bkgtotal

        print 'eff of last bin:', effsig[nbins], effbkg[nbins]
        # print len(effsig)
        # print len(effbkg)
        # print 'underflow',bmin-1, nbins+1,self.sighist.Integral(0, nbins+1)
        # print 'underflow',bmin-1, nbins+1,self.bgdhist.Integral(1, nbins+1)
        # # print 'underflow',bmin-1, nbins+2,self.sighist.Integral(0, 2)
        # # print 'underflow',bmin, nbins+2,self.bgdhist.Integral(1, 2)
        # print 'overflow',nbins+1, nbins+2,self.sighist.Integral(nbins+1, nbins+2)
        # print 'overflow',nbins+1, nbins+2,self.bgdhist.Integral(nbins+1, nbins+2)
        # effsig = np.array(effsig, dtype='float64')
        # effbkg = np.array(effbkg, dtype='float64')
        for i,disc in enumerate(xbins):
            # if i<nbins:
            print i, 'disc', disc, 'effsig', effsig[i], 'effbkg',effbkg[i], 'npassSig', npassSig[i], 'npassBkg', npassBkg[i], 'totsig', sigtotal, 'totbkg', bkgtotal
        # print 'discs', xbins, 'effsig', effsig, 'effbkg',effbkg
        # print roc
        effsig[-1]=0.0
        effbkg[-1]=0.0
        # print effsig, effbkg

        return int(nbins), np.array(effsig, dtype='float64'), np.array(effbkg, dtype='float64')

    def getCutValueForEfficiency(self, hist, targeteff):
        nbins = hist.GetNbinsX()
        xbins=np.empty(nbins+1, dtype='float64'); binw=np.empty(nbins, dtype='float64')
        for i in range(nbins):
            # print 'xbins[i]', self.sighist.GetBinLowEdge(i+1), 'binw', self.sighist.GetBinWidth(i+1)
            xbins[i]=(self.sighist.GetBinLowEdge(i+1))
            binw[i]=(self.sighist.GetBinWidth(i+1))
        xbins[nbins] = (xbins[nbins-1]+binw[nbins-1])
        # print 'last bin:', xbins[nbins-1]+binw[nbins-1]
        # print 'xbins', xbins
        npass = np.empty(nbins, dtype='int32'); eff = np.empty(nbins, dtype='float64')
        total = hist.Integral(0, nbins+1)
        effdiff = 1.0; nbin = -1

        for ibin in range(nbins):
            npass[ibin]=(hist.Integral(ibin, nbins+1))
            eff[ibin]=(npass[ibin]/total)
            tmpdiff = abs(eff[ibin]-targeteff)
            if tmpdiff < effdiff:
                effdiff = tmpdiff
                nbin = ibin
        
        return xbins[nbin], eff[nbin]

    def getEfficiencyForCutValue(self, hist, cut):
        nbins = hist.GetNbinsX()
        xbins=np.empty(nbins+1, dtype='float64'); binw=np.empty(nbins, dtype='float64')
        for i in range(nbins):
            xbins[i]=(self.sighist.GetBinLowEdge(i+1))
            binw[i]=(self.sighist.GetBinWidth(i+1))
        xbins[nbins] = (xbins[nbins-1]+binw[nbins-1])
        
        npass = np.empty(nbins, dtype='float64'); eff= np.empty(nbins, dtype='float64')
        total = hist.Integral(0, nbins+1)
        diff = 1.0; nbin = -1

        for ibin in range(nbins):
            npass[ibin]=(hist.Integral(ibin, nbins+1))
            eff[ibin]=(npass[ibin]/total)
            tmpdiff = abs(xbins[ibin]-cut)
            if tmpdiff < diff:
                diff = tmpdiff
                nbin = ibin
        
        return xbins[nbin], eff[nbin]
    
    def getInfoForSignalEff(self, eff):
        cut, sigeff  = self.getCutValueForEfficiency(self.sighist, eff)
        bcut, bkgeff = self.getEfficiencyForCutValue(self.bgdhist, cut)
        sigeff*=100.0; bkgeff*=100.0
        print 'A cut of', cut, 'corresponds to signal efficiency=', sigeff, 'background efficiency=', bkgeff

    def getInfoForBackgroundEff(self, eff):
        cut, bkgeff  = self.getCutValueForEfficiency(self.bgdhist, eff)
        bcut, sigeff = self.getEfficiencyForCutValue(self.sighist, cut)
        sigeff*=100.0; bkgeff*=100.0
        print 'A cut of', cut, 'corresponds to signal efficiency=', sigeff, 'background efficiency=', bkgeff

    def getInfoForCut(self, cut):
        scut, sigeff = self.getEfficiencyForCutValue(self.sighist, cut)
        bcut, bkgeff = self.getEfficiencyForCutValue(self.bgdhist, cut)
        sigeff*=100.0; bkgeff*=100.0
        print 'A cut of', cut, 'corresponds to signal efficiency=', sigeff, 'background efficiency=', bkgeff
        
    #plots sig and bkg disc distributions
    # def getDists(self, title='eventBDT_disc_dist', xtitle='BDT disc', ytitle='N Entries', normed=True):
    #     plots = [self.sighist, self.bkdhist]
    #     if normed:
            
        
