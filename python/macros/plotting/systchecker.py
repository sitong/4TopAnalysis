
# plots TTTT vs TTTT up and down systematic
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-b", "--base", dest="base", default='SR') #SR, VR, or TT
parser.add_argument("-v", "--var", dest="var", default='catBDTdisc')
parser.add_argument("-l", "--logy", dest="logy", default='True')
parser.add_argument("-m", "--modbins", dest="modbins", default='False')
parser.add_argument("-q", "--qcdtt", dest="qcdtt", default='True')
parser.add_argument("-s", "--shape", dest="shape", default='False')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

syst='ResTopMiss'
# wgtsyst=False
# if syst in ['ISR', 'FSR']:
#     wgtsyst=True

logy=True; modbins=True; useshape=True; useqcdtt=True
if args.logy=='False':
    logy=False
if args.modbins=='False':
    modbins=False
if args.shape=='False':
    useshape=False
if args.qcdtt=='False':
    useqcdtt=False
year = args.year
base = args.base
var = args.var 

def systchecker(args):
    pd = plotdefs(year, base, modbins)
    nbins, selrange = pd.getHistSettings(var+'_nosys')
    modhist = None
    for chanid, chansel in pd.binsels.iteritems():
        histname = year+'_'+syst+'_'+chanid+'_'+var
        sels = pd.getsels(chanid)
        mchist = pd.gethist(pd.mcfiles['TTTT'], var+'_nosys', sels['mcsel'])

        #syst plots
        if syst=='jer' or syst=='jes':
            upsel = sels['mcsel'].replace('nosys', syst+'Up')
            downsel = sels['mcsel'].replace('nosys', syst+'Down')
            upvar = var+'_'+syst+'Up'
            downvar = var+'_'+syst+'Down'
        else:
            upvar=var+'_nosys'
            downvar=var+'_nosys'
            if syst=='pileup':
                upsel=sels['mcsel'].replace('puWeight', 'puWeightUp')
                downsel=sels['mcsel'].replace('puWeight', 'puWeightDown')
            elif syst=='trigger':
                upsel=sels['mcsel'].replace('trigSF_nosys', 'trigSF_Up')
                downsel=sels['mcsel'].replace('trigSF_nosys', 'trigSF_Down')
            elif syst=='isr':
                upsel=sels['mcsel'].replace('puWeight', 'puWeight*isr_Up')
                downsel=sels['mcsel'].replace('puWeight', 'puWeight*isr_Down')
            elif syst=='fsr':
                upsel=sels['mcsel'].replace('puWeight', 'puWeight*fsr_Up')
                downsel=sels['mcsel'].replace('puWeight', 'puWeight*fsr_Down')
            elif syst=='ME':
                upsel=sels['mcsel'].replace('puWeight', 'puWeight*MErn_Up')
                downsel=sels['mcsel'].replace('puWeight', 'puWeight*MErn_Down')
            elif syst=='pdf':
                upsel=sels['mcsel'].replace('puWeight', 'puWeight*pdfrn_Up')
                downsel=sels['mcsel'].replace('puWeight', 'puWeight*pdfrn_Down')
            elif syst=='btagLF':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagLFUp')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagLFDown')
            elif syst=='btagHF':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagHFUp')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagHFDown')
            elif syst=='btagLFstats1':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagLFstats1Up')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagLFstats1Down')
            elif syst=='btagHFstats1':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagHFstats1Up')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagHFstats1Down')
            elif syst=='btagLFstats2':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagLFstats2Up')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagLFstats2Down')
            elif syst=='btagHFstats2':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagHFstats2Up')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagHFstats2Down')
            elif syst=='btagCFerr1':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagCFerr1Up')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagCFerr1Down')
            elif syst=='btagCFerr2':
                upsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagCFerr2Up')
                downsel=sels['mcsel'].replace('btagSF_nosys', 'btagSF_btagCFerr2Down')
            elif syst=='DeepAK8WSF':
                upsel=sels['mcsel'].replace('bsWSF_nosys', 'bsWSF_DeepAK8WSF_Up')
                downsel=sels['mcsel'].replace('bsWSF_nosys', 'bsWSF_DeepAK8WSF_Down')
            elif syst=='DeepAK8TopSF':
                upsel=sels['mcsel'].replace('bsTopSF_nosys', 'bsTopSF_DeepAK8TopSF_Up')
                downsel=sels['mcsel'].replace('bsTopSF_nosys', 'bsTopSF_DeepAK8TopSF_Down')
            elif syst=='ResTopEff':
                upsel=sels['mcsel'].replace('RTeffSF_nosys', 'RTeffSF_ResTopEff_Up')
                downsel=sels['mcsel'].replace('RTeffSF_nosys', 'RTeffSF_ResTopEff_Down')
            elif syst=='ResTopMiss':
                upsel=sels['mcsel'].replace('RTmissSF_nosys', 'RTmissSF_ResTopMiss_Up')
                downsel=sels['mcsel'].replace('RTmissSF_nosys', 'RTmissSF_ResTopMiss_Down')

        print var+'_nosys', sels['mcsel']
        print upvar, upsel
        print downvar, downsel
        
        upmchist = pd.gethist(pd.mcfiles['TTTT'], upvar, upsel)
        downmchist = pd.gethist(pd.mcfiles['TTTT'], downvar, downsel)
        # yld, unc = pd.getyield(mchist)
        # upyld, upunc = pd.getyield(upmchist)
        # downyld, downunc = pd.getyield(downmchist)
        # upmchist.Scale((yld/upyld)) 
        # downmchist.Scale((yld/downyld))
        # print yld, upyld, downyld

        #top plot
        c = r.TCanvas("c","c",800,800)
        p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        p1.SetBottomMargin(0.1)
        p1.SetGridx()
        p1.SetGridy()
        p1.Draw()
        p1.cd()
        legend = r.TLegend(0.1, 0.6, 0.3, 0.9)

        mchist.SetLineWidth(3)
        mchist.SetMarkerStyle(20)
        mchist.SetLineColor(r.kGray+2)
        mchist.SetMarkerColor(r.kGray+2)
        legend.AddEntry(mchist, "TTTT", "L")           

        upmchist.SetLineWidth(3)
        upmchist.SetMarkerStyle(20)
        upmchist.SetLineColor(r.kRed+2)
        upmchist.SetMarkerColor(r.kRed+2)
        legend.AddEntry(upmchist, "TTTT Up", "L")           

        downmchist.SetLineWidth(3)
        downmchist.SetMarkerStyle(20)
        downmchist.SetLineColor(r.kBlue+2)
        downmchist.SetMarkerColor(r.kBlue+2)
        legend.AddEntry(downmchist, "TTTT Down", "L")           

        mchist.SetTitle("")
        mchist.Draw("histe")
        upmchist.Draw("histesame")
        downmchist.Draw("histesame")
        gPad.Modified()
        gPad.Update()
        ymax = p1.GetUymax()*1.3 #max+30%
        # print 'ymax', ymax
        mchist.SetMaximum(ymax)

        mchist.GetYaxis().SetTitleSize(20)
        mchist.GetYaxis().SetTitleFont(43)
        mchist.GetYaxis().SetTitleOffset(1.55)
        mchist.GetYaxis().SetTitle('NEntries')
        mchist.GetXaxis().SetTitle('BDT discriminant')
        mchist.GetYaxis().SetLabelFont(43)
        mchist.GetYaxis().SetLabelSize(15)

        legend.Draw()
        gPad.Modified()
        gPad.Update()

        #bottom plot
        c.cd()
        p2 = r.TPad("p2", "p2", 0, 0.05, 1, 0.3)
        p2.SetGridx()
        p2.SetGridy()
        p2.Draw()
        p2.cd()

        ratioup = upmchist.Clone("ratioup")
        ratiodown = downmchist.Clone("ratiodown")
        ratioup.SetLineColor(r.kRed+2)
        ratiodown.SetLineColor(r.kBlue+2)
        ratioup.SetMinimum(0.4)
        ratioup.SetMaximum(1.6)
        ratioup.SetMarkerStyle(20)
        ratiodown.SetMarkerStyle(20)
        ratioup.SetMarkerColor(r.kRed+2)
        ratiodown.SetMarkerColor(r.kBlue+2)

        ratioup.Sumw2()
        ratiodown.Sumw2()

        ratioup.Divide(mchist)
        ratiodown.Divide(mchist)

        for ibin in range(0, ratioup.GetNbinsX()+2):
            uperr = upmchist.GetBinError(ibin)
            up = upmchist.GetBinContent(ibin)
            downerr = downmchist.GetBinError(ibin)
            down = downmchist.GetBinContent(ibin)
            reluperr=0.0
            reldownerr=0.0
            if up>0.0: reluperr=uperr/up
            if down>0.0: reldownerr=downerr/down
            ratioup.SetBinError(ibin, reluperr)
            ratiodown.SetBinError(ibin, reldownerr)
            print 'nom', mchist.GetBinContent(ibin), 'up', up, 'down', down
        ratioup.Draw("histep")
        ratiodown.Draw("histepsame")
        l = r.TLine(p2.GetUxmin(), 1.0, p2.GetUxmax(), 1.0);
        l.SetLineColor(r.kBlack)
        l.SetLineWidth(1)
        l.Draw("same")

        ratioup.SetTitle("")
        ratioup.GetYaxis().SetTitle("ratio syst/normal")
        ratioup.GetYaxis().SetNdivisions(505)
        ratioup.GetYaxis().SetTitleSize(20)
        ratioup.GetYaxis().SetTitleFont(43)
        ratioup.GetYaxis().SetTitleOffset(1.55)
        ratioup.GetYaxis().SetLabelFont(43)
        ratioup.GetYaxis().SetLabelSize(15)

        ratioup.GetXaxis().SetTitle("BDT discriminant")
        ratioup.GetXaxis().SetTitleSize(20)
        ratioup.GetXaxis().SetTitleFont(43)
        ratioup.GetXaxis().SetTitleOffset(4.0)
        ratioup.GetXaxis().SetLabelFont(43)
        ratioup.GetXaxis().SetLabelSize(15)

        p2.SetTopMargin(0.1)
        p2.SetBottomMargin(0.2)
        gPad.Modified()
        gPad.Update()
        
        print 'saved histogram: ', pd.outdir+histname+'.png'
        c.SaveAs(pd.outdir+histname+'.png')
systchecker(args)
