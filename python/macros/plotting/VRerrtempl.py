#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

class VRerrtempl:
    def __init__(self, year, region='SR', usettbb=False):
        self.fillhists=True
        if self.fillhists:
            self.outroot='./'+region+'-VRerrtempl_shape.root'
            print 'Will output histograms to', self.outroot
        self.var='catBDTdisc_nosys'
        self.year = args.year
        self.SR=region
        self.usettbb=usettbb
        self.pd = plotdefs(self.year, self.SR, usettbb=self.usettbb)
        self.treename = 'Events'
        self.colors = {'TTTT':r.TColor.GetColor("#163d4e"),
                       'tttt':r.TColor.GetColor("#163d4e"),
                       'DDBKG':r.kRed-7,
                       'ttH+ttV':r.TColor.GetColor("#d07e93"),
                       'TTX':r.TColor.GetColor("#d07e93"),
                       'other':r.TColor.GetColor("#cf9ddb"),
                       'minor':r.TColor.GetColor("#cf9ddb")}

        outdir = './plots/templhists/'+self.year+'/'
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        self.outdir = outdir

    def makeplots(self, plots, histname, varhists={} ):
        addupdown=False
        if 'up' in varhists.keys(): addupdown=True
        #top plot
        c = r.TCanvas("c","c",800,800)
        p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        p1.SetBottomMargin(0.1)
        p1.Draw()
        p1.cd()

        stack = r.THStack("stack", "stack")
        legend = r.TLegend(0.1, 0.6, 0.3, 0.9)
        stacktot = r.TH1F('stacktot', 'stacktot', len(self.pd.edges)-1, self.pd.edges)
        # stacktot = r.TH1F('stacktot', 'stacktot',9, 0.000000, 9.000000)
        hcount = 0; hnames = []
        for proc, plot in plots.iteritems():
            if proc == 'data':
                datahist = plot
                datahist.SetLineWidth(2)
                datahist.SetMarkerStyle(20)
                datahist.SetLineColor(r.kGray+2)
                legend.AddEntry(datahist, "data", "L")           
                
            elif proc!='data':
                plot.SetLineWidth(1)
                plot.SetFillColor(self.colors[proc])
                plot.SetLineColor(self.colors[proc])
                stack.Add(plot)
                stacktot.Add(plot)
                hnames.append(proc)
                legend.AddEntry(plot, hnames[hcount], "F")           
                hcount+=1
                
        #draw stack
        stack.SetTitle("")
        stack.Draw("hist")
        gPad.Modified()
        gPad.Update()
        ymax = p1.GetUymax()*1.6 #max+30%
        stack.SetMaximum(ymax)

        datahist.Draw("psame")

        stacktot.SetLineWidth(0) #dont show line
        stacktot.SetFillColor(r.kGray+2)
        stacktot.SetFillStyle(3002)
        stacktot.Draw("e2same")
        stackyld, unc = self.pd.getyield(stacktot)

        #up/down templ errors
        if addupdown:
            for key, plot in varhists.iteritems():
                # y,u = self.pd.getyield(plot)
                # plot.Scale(stackyld/y)
                plot.SetLineWidth(3)
                plot.SetLineColor(r.kAzure-2)
                if key=='down':
                    plot.SetLineColor(r.kSpring-1)
                plot.Draw("histsame")
                legend.AddEntry(plot, "DDBKG "+key, "L")           

        stack.GetYaxis().SetTitleSize(20)
        stack.GetYaxis().SetTitleFont(43)
        stack.GetYaxis().SetTitleOffset(1.55)
        stack.GetYaxis().SetTitle('NEntries')
        stack.GetXaxis().SetTitle('BDT discriminant')
        stack.GetYaxis().SetLabelFont(43)
        stack.GetYaxis().SetLabelSize(15)

        gPad.Modified()
        gPad.Update()

        legend.Draw()
        gPad.Modified()
        gPad.Update()

        #bottom plot
        c.cd()
        p2 = r.TPad("p2", "p2", 0, 0.05, 1, 0.3)
        p2.SetGridy()
        p2.Draw()
        p2.cd()

        ratio = datahist.Clone("ratio")
        ratio.SetLineColor(r.kBlack)
        ratio.Divide(stacktot) 
        ratio.SetMarkerStyle(20)

        raterr = datahist.Clone("raterr")
        raterr.SetLineWidth(0)
        raterr.SetFillColor(r.kGray+2)
        raterr.SetFillStyle(3002)
        raterr.SetMarkerStyle(1)
        raterr.SetMinimum(0.4)
        raterr.SetMaximum(1.6)

        # print 'nbins',datahist.GetNbinsX(), datahist.GetBinContent(0)
        if addupdown:
            ratup = datahist.Clone("ratup"); ratdown = datahist.Clone("ratdown")
            stackup = stacktot.Clone("stackup"); stackdown = stacktot.Clone("stackdown")
            #need to subtract predhist and add up and down hist to stacktot
            stackup.Add(plots['DDBKG'],-1); stackdown.Add(plots['DDBKG'],-1)
            stackup.Add(varhists['up']); stackdown.Add(varhists['down'])
            ratup.Divide(stackup); ratdown.Divide(stackdown)
            ratup.SetLineWidth(2);  ratdown.SetLineWidth(2)
            ratup.SetMarkerStyle(1);  ratdown.SetMarkerStyle(1)
            ratup.SetMarkerColor(self.colors['DDBKG']);  ratdown.SetMarkerColor(self.colors['DDBKG'])
            ratup.SetLineColor(r.kAzure-2); ratdown.SetLineColor(r.kSpring-1)

        #error bars
        for ibin in range(0, ratio.GetNbinsX()+2):
            derr = datahist.GetBinError(ibin)
            d = datahist.GetBinContent(ibin)
            relderr=0.0
            if d>0.0: relderr=derr/d

            err = stacktot.GetBinError(ibin)
            s = stacktot.GetBinContent(ibin)
            relerr=0.0
            if s>0:
                relerr = err/s

            raterr.SetBinError(ibin, relerr)
            ratio.SetBinError(ibin, relderr)
            raterr.SetBinContent(ibin, 1)

        #draw
        raterr.Draw("e2")
        ratio.Draw("pe0same")
        l = r.TLine(p2.GetUxmin(), 1.0, p2.GetUxmax(), 1.0);
        l.SetLineColor(r.kBlack)
        l.SetLineWidth(1)
        l.Draw("same")

        if addupdown:
            ratup.Draw("histsame"); ratdown.Draw("histsame")

        raterr.SetTitle("")
        raterr.GetYaxis().SetTitle("ratio data/prediction")
        raterr.GetYaxis().SetNdivisions(505)
        raterr.GetYaxis().SetTitleSize(20)
        raterr.GetYaxis().SetTitleFont(43)
        raterr.GetYaxis().SetTitleOffset(1.55)
        raterr.GetYaxis().SetLabelFont(43)
        raterr.GetYaxis().SetLabelSize(15)
        raterr.GetXaxis().SetTitle("BDT discriminant")
        raterr.GetXaxis().SetTitleSize(20)
        raterr.GetXaxis().SetTitleFont(43)
        raterr.GetXaxis().SetTitleOffset(4.0)
        raterr.GetXaxis().SetLabelFont(43)
        raterr.GetXaxis().SetLabelSize(15)

        p2.SetTopMargin(0.1)
        p2.SetBottomMargin(0.2)
        gPad.Modified()
        gPad.Update()
        
        print 'saved histogram: ', self.outdir+histname+'.png'
        c.SaveAs(self.outdir+histname+'.png')

    def slopeshape(self, year, chanid):
        getunc = {'2016':{'cut0bin0':0.06,
                          'cut0bin1':0.16,
                          'cut0bin2':0.21,
                          'cut0bin3':0.38,
                          'cut0bin4':0.10,
                          'cut0bin5':0.22,
                          'cut0bin6':0.04,
                          'cut0bin7':0.29,
                          'cut1bin0':0.95,
                          'cut1bin1':0.32,
                          'cut2bin0':0.51,
                          'cut2bin1':1.30},
                  '2017':{'cut0bin0':0.15,
                          'cut0bin1':0.15,
                          'cut0bin2':0.17,
                          'cut0bin3':0.37,
                          'cut0bin4':0.20,
                          'cut0bin5':0.40,
                          'cut0bin6':0.04,
                          'cut0bin7':0.39,
                          'cut1bin0':0.18,
                          'cut1bin1':1.28,
                          'cut2bin0':0.10,
                          'cut2bin1':0.14},
                  '2018':{'cut0bin0':0.22,
                          'cut0bin1':0.13,
                          'cut0bin2':0.13,
                          'cut0bin3':0.04,
                          'cut0bin4':0.44,
                          'cut0bin5':0.11,
                          'cut0bin6':0.42,
                          'cut0bin7':0.05,
                          'cut1bin0':0.32,
                          'cut1bin1':0.45,
                          'cut2bin0':0.25,
                          'cut2bin1':0.11}}
        uncval = getunc[year][chanid]
        print 'uncval', uncval

        nomhist = self.pd.histfromtree(chanid)
        histup = nomhist.Clone('histup')
        histdown = nomhist.Clone('histdown')
        for ibin in range(1,nomhist.GetNbinsX()+1):
        # for ibin in range(nomhist.GetNbinsX()):
            upbin = histup.GetBinContent(ibin)
            downbin = histdown.GetBinContent(ibin)
            binx = histup.GetBinCenter(ibin)
            upval = upbin+(uncval*binx)
            downval = downbin-(uncval*binx)
            histup.SetBinContent(ibin, upval)
            histdown.SetBinContent(ibin, downval)
            print upbin, upval, downval, binx

        n, nunc = self.pd.getyield(nomhist)
        u, uunc = self.pd.getyield(histup)
        d, dunc = self.pd.getyield(histdown)
        # histup.Scale(n/u)
        # histdown.Scale(n/d)

        hists={'up':histup, 'down':histdown}
        if self.fillhists:
            for typ, hist in hists.iteritems():
                histname = chanid+'_'+self.year+'_'+typ
                outhist = hist.Clone(histname)
                histfile = r.TFile(self.outroot, "UPDATE")
                outhist.Write()
                histfile.Close()
                del histfile
                print 'filled hist ', histname, 'to file', self.outroot                 
        return hists

    def scaleshape(self, year, chanid, uncval=None):
        #updated for new histogram types
        # binalias = {'cut0bin0':'0',
        #             'cut0bin1':'1',
        #             'cut0bin2':'2',
        #             'cut0bin3':'3',
        #             'cut0bin4':'4',
        #             'cut0bin5':'5',
        #             'cut0bin6':'6',
        #             'cut0bin7':'7',
        #             'cut1bin0':'10',
        #             'cut1bin1':'11',
        #             'cut2bin0':'20',
        #             'cut2bin1':'21'}# for reading trees
        binalias = {'cut0bin0':['240k_'+self.SR+'1.root',700.0,800.0],
                    'cut0bin1':['240k_'+self.SR+'1.root',800.0,900.0],
                    'cut0bin2':['240k_'+self.SR+'1.root',900.0,100.00],
                    'cut0bin3':['240k_'+self.SR+'1.root',100.00,1100.0],
                    'cut0bin4':['240k_'+self.SR+'1.root',1100.0,1200.0],
                    'cut0bin5':['240k_'+self.SR+'1.root',1200.0,1300.0],
                    'cut0bin6':['240k_'+self.SR+'1.root',1300.0,1500.0],
                    'cut0bin7':['240k_'+self.SR+'1.root',1500.0,1000000.0],
                    'cut1bin0':['240k_'+self.SR+'2.root',700.0,1400.0],
                    'cut1bin1':['240k_'+self.SR+'2.root',1400.0,1000000.0],
                    'cut2bin0':['240k_'+self.SR+'3.root',700.0,1100.0],
                    'cut2bin1':['240k_'+self.SR+'3.root',1100.0,1000000.0]}# for reading trees
            
        getunc = {'2016':{'cut0bin0':0.01,
                          'cut0bin1':0.02,
                          'cut0bin2':0.02,
                          'cut0bin3':0.02,
                          'cut0bin4':0.01,
                          'cut0bin5':0.01,
                          'cut0bin6':0.02,
                          'cut0bin7':0.01,
                          'cut1bin0':0.03,
                          'cut1bin1':0.02,
                          'cut2bin0':0.03,
                          'cut2bin1':0.02},
                  '2017':{'cut0bin0':0.02,
                          'cut0bin1':0.02,
                          'cut0bin2':0.02,
                          'cut0bin3':0.02,
                          'cut0bin4':0.01,
                          'cut0bin5':0.01,
                          'cut0bin6':0.02,
                          'cut0bin7':0.01,
                          'cut1bin0':0.01,
                          'cut1bin1':0.02,
                          'cut2bin0':0.02,
                          'cut2bin1':0.01},
                  '2018':{'cut0bin0':0.01,
                          'cut0bin1':0.01,
                          'cut0bin2':0.02,
                          'cut0bin3':0.01,
                          'cut0bin4':0.01,
                          'cut0bin5':0.01,
                          'cut0bin6':0.01,
                          'cut0bin7':0.01,
                          'cut1bin0':0.02,
                          'cut1bin1':0.03,
                          'cut2bin0':0.01,
                          'cut2bin1':0.01}}
        if uncval is None:
            # uncval = 0.01
            uncval = getunc[year][chanid]
        print 'uncval', uncval
        systup = 1.0+uncval  #for set %
        systdown = 1.0-uncval
        
        path = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/data/NNretrain/'
        if self.usettbb:
            nafdir = self.year+'_ttpure_minorbkgttbb/' #USEttbb
        elif not self.usettbb:
            nafdir = self.year+'_ttinclusive_minorbkg/'
        bdtvar = 'BDT_disc'
        path = path+nafdir
        nafname = path+binalias[chanid][0]
        print nafname
        rf, t = rs.makeroot(nafname, 'mytree')

        # treefile = self.pd.NAFfilename
        # mybin = binalias[chanid]
        # print treefile, 'bin', mybin
        histup = r.TH1F('histup', 'histup', len(self.pd.edges)-1, self.pd.edges)
        histdown = r.TH1F('histdown', 'histdown', len(self.pd.edges)-1, self.pd.edges)
        # rf, t = rs.makeroot(treefile, self.treename)
        for ent in t:
            if (((ent.nJet>=9 and self.SR=='SR') or (ent.nJet==8 and self.SR=='VR')) and ent.nbJet>=3 and ent.ht>=binalias[chanid][1] and ent.ht<binalias[chanid][2] and ent.BDT_disc>=0.0 and ent.BDT_disc<=1.0):
            # if (str(ent.bin)==mybin and ent.BDT_disc>=0.0 and ent.BDT_disc<=1.0):
                discup = systup*ent.BDT_disc       #for set %
                discdown = systdown*ent.BDT_disc 
                #limit to 0-1 range:
                if discup>=1.0: discup=1.0
                elif discup<0.0: discup=0.0
                if discdown>=1.0: discdown=1.0
                elif discdown<0.0: discdown=0.0
                histup.Fill(discup)
                histdown.Fill(discdown)
                histup.SetBinContent(10, 0.0) #overflow
                histdown.SetBinContent(10, 0.0) #overflow
                # print ent.BDT_disc, discup, discdown

        hists={'up':histup, 'down':histdown}
        for typ, hist in hists.iteritems():
            yld, unc = self.pd.getyield(hist)
            hist.Sumw2()
            ABCDyld = self.pd.extDDyields[chanid][0]
            ABCDunc = self.pd.extDDyields[chanid][1]
            ABCD_relunc = ABCDunc/ABCDyld
            hist.Scale(ABCDyld/yld) #scale histogram
            if self.fillhists:
                histname = chanid+'_'+self.year+'_'+typ
                outhist = hist.Clone(histname)
                histfile = r.TFile(self.outroot, "UPDATE")
                outhist.Write()
                histfile.Close()
                del histfile
                print 'filled hist ', histname, 'to file', self.outroot                 
        return hists

    def randseedhists(self, year, chanid):
        ABCDyld = self.pd.extDDyields[chanid][0]
        shapefile = r.TFile('histenvelopev2_'+year+'.root')
        # shapefile = r.TFile('histenvelope_'+year+'.root')
        uphist = shapefile.Get(chanid+'_'+year+'_high')
        downhist = shapefile.Get(chanid+'_'+year+'_low')
        up = uphist.Clone("up")
        down = downhist.Clone("down")
        # up = r.TH1F('up', 'up', len(self.pd.edges)-1, self.pd.edges)
        # down = r.TH1F('down', 'down', len(self.pd.edges)-1, self.pd.edges)
        # for ibin in range(1,uphist.GetNbinsX()+2):
        #     up.SetBinContent(ibin, uphist.GetBinContent(ibin))
        #     down.SetBinContent(ibin, downhist.GetBinContent(ibin))
        #     print ibin, uphist.GetBinContent(ibin)
        up.SetBinContent(10, 0.0) #overflow
        down.SetBinContent(10, 0.0) #overflow
        #renorm
        hyld, hunc = self.pd.getyield(up)
        dyld, dunc = self.pd.getyield(down)
        nomhist = self.pd.histfromtree(chanid, nonorm=True)
        nomyld, nomunc = self.pd.getyield(nomhist)
        # ABCDyld/hyld * hyld/nomyld) = ABCDyld/nomyld
        # up.Scale(ABCDyld/nomyld) #not fully normed
        # down.Scale(ABCDyld/nomyld)
        print 'up', ABCDyld*(hyld/nomyld), 'down', ABCDyld*(dyld/nomyld), 'nom', ABCDyld
        # print ABCDyld, 1/nomyld, ABCDyld/hyld, ABCDyld/dyld
        up.Scale(ABCDyld/hyld) #actually normed
        down.Scale(ABCDyld/dyld)
        print up.GetNbinsX()
        up.SetDirectory(0);down.SetDirectory(0)
        uphist.SetDirectory(0);downhist.SetDirectory(0)
        print up
        hists={'up':up, 'down':down}
        if self.fillhists:
            histnameup = chanid+'_'+self.year+'_up'
            histnamedown = chanid+'_'+self.year+'_down'
            outhistup = up.Clone(histnameup)
            outhistdown = down.Clone(histnamedown)
            histfile = r.TFile(self.outroot, "UPDATE")
            # up.Write()
            # down.Write()
            outhistup.Write()
            outhistdown.Write()
            outhistup.SetDirectory(0);outhistdown.SetDirectory(0)
            histfile.Close()
            del histfile
            print 'filled hists ', histnameup, histnamedown, 'to file', self.outroot
        del shapefile
        return hists

    def gaussmear(self, year, chanid):
        binalias = {'cut0bin0':'0',
                    'cut0bin1':'1',
                    'cut0bin2':'2',
                    'cut0bin3':'3',
                    'cut0bin4':'4',
                    'cut0bin5':'5',
                    'cut0bin6':'6',
                    'cut0bin7':'7',
                    'cut1bin0':'10',
                    'cut1bin1':'11',
                    'cut2bin0':'20',
                    'cut2bin1':'21'}# for reading trees
        # var = 'BDT_disc'
        treefile = self.pd.NAFfilename
        mybin = binalias[chanid]
        print treefile, 'bin', mybin
        histup = r.TH1F('histup', 'histup', len(self.pd.edges)-1, self.pd.edges)
        histdown = r.TH1F('histdown', 'histdown', len(self.pd.edges)-1, self.pd.edges)
        rf, t = rs.makeroot(treefile, self.treename)
        rms = self.pd.VRerr[year+'_'+chanid]['rms']
        for ent in t:
            if (str(ent.bin)==mybin and ent.BDT_dsic>=0.0 and ent.BDT_dsic<=1.0):
                random = r.TRandom3()
                random.SetSeed(0)
                rn = random.Gaus(1.0, rms)
                smear_disc = rn*ent.BDT_dsic 
                #limit to 0-1 range:
                if smear_disc>=1.0:
                    smear_disc=1.0
                elif smear_disc<0.0:
                    smear_disc=0.0

        ##get up and down hists: first method
                mirror_disc = (rn*ent.BDT_dsic) - smear_disc
                typ = 'none'
                if smear_disc>ent.BDT_dsic:
                    typ='up'
                    histup.Fill(smear_disc)
                    histdown.Fill(mirror_disc)
                elif smear_disc<ent.BDT_dsic:
                    typ='down'
                    histdown.Fill(smear_disc)
                    histup.Fill(mirror_disc)
                elif smear_disc==ent.BDT_dsic:
                    histup.Fill(smear_disc); histdown.Fill(smear_disc)
                print ent.bin, ent.BDT_dsic, typ, smear_disc, mirror_disc, rn
                histup.SetBinContent(10, 0.0) #overflow
                histdown.SetBinContent(10, 0.0) #overflow

        ##get up and down hists, alternative method
        #         histup.Fill(smear_disc)
        #         print 'random #',rn, 'smear disc', smear_disc, 'original disc:', ent.BDT_dsic
        # yldup, uncup = self.pd.getyield(histup)
        # histup.Sumw2()
        # ABCDyld = self.pd.extDDyields[chanid][0]
        # histup.Scale(ABCDyld/yldup) #scale histogram
        # histnom = self.pd.histfromtree(chanid); histnom.Sumw2()
        # histup.SetBinContent(10, 0.0) #overflow

        # histdown = self.mirrorhist(histnom, histup)
        # ylddown, uncdown = self.pd.getyield(histdown)
        # histdown.Sumw2()
        # histdown.Scale(ABCDyld/ylddown)
        # histdown.SetBinContent(10, 0.0) #overflow

        hists={'up':histup, 'down':histdown}
        for typ, hist in hists.iteritems():
            yld, unc = self.pd.getyield(hist)
            hist.Sumw2()
            ABCDyld = self.pd.extDDyields[chanid][0]
            ABCDunc = self.pd.extDDyields[chanid][1]
            ABCD_relunc = ABCDunc/ABCDyld
            VR_relunc   = self.pd.VRerr[year+'_'+chanid]['mean'] #mean now, not overall error
            hist.Scale(ABCDyld/yld) #scale histogram
            if self.fillhists:
                histname = chanid+'_'+self.year+'_'+typ
                outhist = hist.Clone(histname)
                histfile = r.TFile(self.outroot, "UPDATE")
                outhist.Write()
                histfile.Close()
                del histfile
                print 'filled hist ', histname, 'to file', self.outroot 
                
            #deal with errors properly - only matters if you show them
            # sqrt[sum_i(abs_error_VR_i^2 + abs_error_abcd_i^2 + stat_unc_bin^2) ]
            # for ibin in range(hist.GetNbinsX()+2):
            #     stat_i = hist.GetBinError(ibin) #statistics of that bin
            #     histval_i = hist.GetBinContent(ibin) #value of that bin
            #     #err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 )
            #     # if self.addunc: #include VR_err
            #     err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 + (VR_relunc*histval_i)**2 )
            #     hist.SetBinError(ibin, err)
        return hists

    def gethists(self, chanid):
        hists = {} 
        sels = self.pd.getsels(chanid)
        datahist = self.pd.gethist(self.pd.datafilename, self.var, sels['datasel'])
        predhist = self.pd.histfromtree(chanid)
        datahist.Sumw2(); predhist.Sumw2()
        hists['data']=datahist
        hists['DDBKG']=predhist 
        for proc in self.pd.mcprocs:
            if proc=='ttbb':
                ttbbsel=sels['mcsel']
                ttbbsel = sels['mcsel'].replace("nfunky_leptons==0 &&","nfunky_leptons==0 && isttbb==1 &&")
                print 'ttbbsel:', ttbbsel, '\n'
                mchist = pd.gethist(pd.mcfiles[proc], var, ttbbsel)
            else:
                mchist = self.pd.gethist(self.pd.mcfiles[proc], self.var, sels['mcsel'])
            mchist.Sumw2()
            hists[proc] = mchist
        return hists

    def mirrorhist(self, nomhist, uphist):
        #mirror uphist about nomhist
        #downhist = nomhist - (uphist-nomhist) = nomhist - uphist + nomhist = 2nomhist - uphist
        downhist = nomhist.Clone("downhist")
        # downhist.Add(nomhist) #2*nomhist
        # downhist.Add(uphist, -1) #2*nomhist-uphist
        for ibin in range(nomhist.GetNbinsX()+2):
            nombin = nomhist.GetBinContent(ibin)
            upbin = uphist.GetBinContent(ibin)
            uperr = uphist.GetBinError(ibin)
            diff = abs(nombin-upbin)
            downbin=nombin
            typ='none'
            if upbin>nombin:                
                downbin=nombin-diff
                typ='down'
            elif upbin<nombin:                
                downbin=nombin+diff
                typ='up'
            downhist.SetBinContent(ibin, downbin)
            downhist.SetBinError(ibin, uperr)
            print ibin, nomhist.GetBinCenter(ibin), 'nom:', nombin, 'up',upbin, 'down',downbin, 'diff',diff, typ
        return downhist
        
    def runall(self):
        for chanid, chansel in self.pd.binsels.iteritems():
            print chanid, chansel, '\n'
            histname = self.year+'_'+chanid
            hists = self.gethists(chanid)
            # updown = self.slopeshape(self.year, chanid) #get up and down scaling from slope from VRs
            # updown = self.randseedhists(self.year, chanid) #get up and down scaling from random seed distributions 
            updown = self.scaleshape(self.year, chanid) #scale up and down by some %
            # updown = self.gaussmear(self.year, chanid)
            # updown = self.mirrorhist(hists['DDBKG'], uphist)
            self.makeplots(hists, histname, updown)
            
VRet = VRerrtempl(args.year)
VRet.runall()
