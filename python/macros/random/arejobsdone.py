#check whether jobs are done
import subprocess
import numpy as np
import argparse
import ROOT as r
import pandas as pd
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
# import RootStuff as rs
import os
import re
import json

parser = argparse.ArgumentParser(description='')
parser.add_argument("-d", "--jobdir", dest="jobdir", default='jobs')
args = parser.parse_args()

pwd='/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/run'
jsonfile = pwd+'/'+args.jobdir+'/metadata.json'
#     {
#       "idx": 0,
#       "inputfiles": [
#         "/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2016/MC/ST_t-channel_top_4f_inclusiveDecays_13TeV-powhegV2-madspin-pythia8_TuneCUETP8M1/NANOAODSIM/PUMoriond17_Nano25Oct2019_102X_mcRun2_asymptotic_v7-v1/70000/0E776805-B9DF-7A44-8FAC-8AD15B9B9CFA.root"
#       ],
#       "samp": "ST_t-channel_top_4f_inclusiveDecays_13TeV-powhegV2-madspin-pythia8_TuneCUETP8M1"
#     },
  # "outputdir": "/eos/uscms//store/user/lpcstop/mequinna/NanoAODv6_trees/2017/jan22_includingttbar/mc", 

  # "samples": [
  #   "DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8", 
  #   "ST_s-channel_4f_hadronicDecays_TuneCP5_13TeV-amcatnlo-pythia8", 
  #   "ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8", 
  #   "ST_t-channel_antitop_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8", 
  #   "ST_t-channel_top_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8", 
  #   "ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8", 
  #   "ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8", 
  #   "TTGJets_SPLIT", 
  #   "ttHTobb_SPLIT", 
  #   "ttHToNonbb_SPLIT", 
  #   "TTTT_SPLIT", 
  #   "TTWJetsToLNu_SPLIT", 
  #   "TTWJetsToQQ_SPLIT", 
  #   "TTZToLLNuNu_SPLIT", 
  #   "TTZToQQ_SPLIT", 
  #   "tZq_ll_4f_ckm_NLO_TuneCP5_PSweights_13TeV-amcatnlo-pythia8", 
  #   "tZq_nunu_4f_ckm_NLO_TuneCP5_PSweights_13TeV-madgraph-pythia8", 
  #   "WJetsToLNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WJetsToLNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WJetsToLNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WJetsToLNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WJetsToLNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WJetsToQQ_HT400to600_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WJetsToQQ_HT600to800_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WJetsToQQ_HT-800toInf_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "WW_TuneCP5_13TeV-pythia8", 
  #   "WZ_TuneCP5_13TeV-pythia8", 
  #   "ZJetsToNuNu_HT-400To600_13TeV-madgraph", 
  #   "ZJetsToNuNu_HT-600To800_13TeV-madgraph", 
  #   "ZJetsToNuNu_HT-800To1200_13TeV-madgraph", 
  #   "ZJetsToNuNu_HT-1200To2500_13TeV-madgraph", 
  #   "ZJetsToNuNu_HT-2500ToInf_13TeV-madgraph", 
  #   "ZJetsToQQ_HT400to600_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "ZJetsToQQ_HT600to800_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "ZJetsToQQ_HT-800toInf_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8", 
  #   "ZZ_TuneCP5_13TeV-pythia8"
  # ], 


#load json file
#come up with file list
#see what files are missing
#TTTT_SPLIT_188_tree.root
badcount = 0
joblist = []
redo = []
samplist = []
print 'jsonfile:',jsonfile
with open(jsonfile) as jf:
    data = json.load(jf)
    outdir = data['outputdir']
    samps = data['samples']
    # outdir = outdir.replace("/eos/uscms/", "root://cmseos.fnal.gov/")
    jobs = data['jobs']
    print 'outdir:',outdir
    print '\n', list(samps), '\n'
    # print jobs[0:5]
    # for i,s in enumerate(samps):
    #     print 'sample:', s

    #these are files there should be
    print 'getting list of files...\n'
    for subdir, dirs, files in os.walk(outdir):
        for file in files:
            f=subdir+'/'+file
            # print 'file:',f
            joblist.append(f)

    print 'following files NOT found:'
    for i,j in enumerate(jobs):
        samp = j['samp']
        idx = str(j['idx'])
        infile = j['inputfiles'][0]
        search =  outdir+'/'+samp+'_'+idx+'_tree.root'
        if search not in joblist:
            print search
            badcount+=1
            redo.append(infile)
            if samp not in samplist:
                samplist.append(samp)
    print '# of missing files:', badcount, '\n'
    print 'files to redo in ' +args.jobdir+' ('+str(badcount)+' total):'
    for r in redo:
        fname = r.replace('/eos/uscms/','')
        print fname
    print '\n samps:', samplist
