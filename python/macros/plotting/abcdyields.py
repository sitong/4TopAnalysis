import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
from Defs import plotdefs
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='gets yields')
parser.add_argument("-v", "--base", dest="base", default='SR')
parser.add_argument("-c", "--cut", dest="cutname", default='BOTH')
parser.add_argument("-b", "--bin", dest="binname", default='ht_nosys')
parser.add_argument("-l", "--logy", dest="logy", default='True')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

typ='extABCD'
subtractMC = True
addttbb=True

print 'addttbb?', addttbb, '\n'

addunc= False
uncval = 0.2

trigcorrC = {'2016':'1.00623', '2017':'0.999813', '2018':'1.00085'} #(7,3)
trigcorrCp = {'2016':'0.995774', '2017':'0.967842', '2018':'0.971863'} #(6,3)

year = args.year
channame='BOTHht'
# modbins = False
# if args.base=='VR':
#     modbins=True

pd = plotdefs(year, args.base, modbins=False, usettbb=addttbb)

if args.base=='VR':
    CRs = {'B' : 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==8 ',
           'C' : 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==7 ',
           'A' : 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==7 ',
           'Ap': 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==6 ',
           'Cp': 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==6 '}
elif args.base=='SR':
    CRs = {'B' : 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys>=9 ',
           'C' : 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==8 ',
           'A' : 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==8 ',
           'Ap': 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==7 ',
           'Cp': 'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==7 '}


def gethist(filename, cut):
    projvar='ht_nosys'
    nbins=1; selrange =[0.0, 3000.0]
    rf, t = rs.makeroot(filename, 'Events')
    hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
    t.Draw(projvar+">>hist", cut)
    rs.fixref(rf, hist)
    return hist 
    
for binid, binsel in pd.binsels.iteritems(): #need to add triggers
    #can either correct 2bs up (A) or 3 or more bs down (C)
    #cant really scale #trigCorr_0lep
    Dsel = '('+pd.basesel+'&&'+binsel+pd.dstr[year]+')'
    Bsel = '('+CRs['B']+'&&'+binsel+pd.dstr[year]+')' #(8,2)
    Csel = '('+CRs['C']+'&&'+binsel+pd.dstr[year]+')'#'*'+trigcorrC[year]+')' #(7,3)
    Asel = '('+CRs['A']+'&&'+binsel+pd.dstr[year]+')' #(7,2)
    Apsel = '('+CRs['Ap']+'&&'+binsel+pd.dstr[year]+')' #(6,2)
    Cpsel = '('+CRs['Cp']+'&&'+binsel+pd.dstr[year]+')'#'*'+trigcorrCp[year]+')' #(6,3)
    CRsels ={'B':Bsel, 'C':Csel, 'A':Asel, 'Ap':Apsel, 'Cp':Cpsel}
    print 'selection:', Dsel

    # if subtractMC:
    # mc selectionsx
    mcDsel = '('+pd.basesel+'&&'+binsel+pd.mcstr[year]
    mcBsel = '('+CRs['B']+'&&'+binsel+pd.mcstr[year] #(8,2)
    mcCsel = '('+CRs['C']+'&&'+binsel+pd.mcstr[year] #(7,3)
    mcAsel = '('+CRs['A']+'&&'+binsel+pd.mcstr[year] #(7,2)
    mcApsel = '('+CRs['Ap']+'&&'+binsel+pd.mcstr[year] #(6,2)
    mcCpsel = '('+CRs['Cp']+'&&'+binsel+pd.mcstr[year] #(6,3)
    mcCRsels ={'B':mcBsel, 'C':mcCsel, 'A':mcAsel, 'Ap':mcApsel, 'Cp':mcCpsel}
    
    mcDhist = gethist(pd.mcfiles['MC'], mcDsel)
    mcDyld, mcDunc = pd.getyield(mcDhist)
    
    print 'MC selection:', mcDsel
    
    if args.base=='VR':
        #get actual data yield
        Dhist = gethist(pd.datafilename, Dsel)
        Dyld, Dunc = pd.getyield(Dhist)
        if subtractMC:
            Dyld = Dyld - mcDyld
            Dunc = np.sqrt((Dunc*Dunc)+(mcDunc*mcDunc))

    # tttthist = gethist(pd.mcfiles['tttt'], mcDsel)
    # tttty, ttttyu = pd.getyield(tttthist)            
    # print 'TTTT yield:', round(tttty,3), '+/-', round(ttttyu,3)
            
    #get MC yields to subtract
    if subtractMC:
        mcCRylds = {}
        for mcCR, mcCRsel in mcCRsels.iteritems():
            mccrhist = gethist(pd.mcfiles['MC'], mcCRsel)
            mccry, mccryu = pd.getyield(mccrhist)   
            print mccry
            if addttbb:
                ttbbsel = mcCRsel.replace('nfunky_leptons==0 &&', 'nfunky_leptons==0 && isttbb==1 &&') #just ttbb
                ttbbsel = ttbbsel.replace('*weight', '*weight*1.37') #just ttbb
                print 'ttbbsel', ttbbsel
                ttbbhist = gethist(pd.mcfiles['ttbb'], ttbbsel)
                ttbby, ttbbu = pd.getyield(ttbbhist)
                mccry+=ttbby
                mccryu=np.sqrt((ttbbu)**2+(mccryu)**2)
                print 'ttbb;',ttbby, 'mc tot',mccry
            print mcCR+' mc yield', mccry
            mcCRylds[mcCR] = mccry
            mcCRylds[mcCR+'unc']=mccryu

    CRylds = {}
    # CRhists={}
    for CR, CRsel in CRsels.iteritems():
        crhist = gethist(pd.datafilename, CRsel)
        cry, cryu = pd.getyield(crhist)            
        if subtractMC:
            # print CR, cry, '-', mcCRylds[CR]
            CRylds[CR] = cry-mcCRylds[CR]
            CRylds[CR+'unc']= np.sqrt((cryu*cryu)+(mcCRylds[CR+'unc']*mcCRylds[CR+'unc']))
        else:
            CRylds[CR] = cry
            CRylds[CR+'unc']=cryu
        if addunc: # add 20% or whatever uncertaninty to the yields 
            CRylds[CR+'unc']= np.sqrt( (uncval*CRylds[CR])**2 +(CRylds[CR+'unc']*CRylds[CR+'unc']) )
            
        print CR, CRylds[CR], '+/-', CRylds[CR+'unc']

    if typ=='extABCD':
        print typ
        calcyld = (CRylds['Ap']/(CRylds['B']*CRylds['Cp']))*(((CRylds['B']*CRylds['C'])/CRylds['A'])**2)
        calcunc=(2.0*((CRylds['Bunc']/CRylds['B'])**2+ (CRylds['Cunc']/CRylds['C'])**2+ (CRylds['Aunc']/CRylds['A'])**2) + (CRylds['Bunc']/CRylds['B'])**2+ (CRylds['Cpunc']/CRylds['Cp'])**2+ (CRylds['Apunc']/CRylds['Ap'])**2)
        calcunc = calcyld*np.sqrt(calcunc)

    elif typ=='ABCD':
        calcyld = ((CRylds['B']*CRylds['C'])/CRylds['A'])
        calcunc = calcyld*np.sqrt( (CRylds['Bunc']/CRylds['B'])**2 + (CRylds['Cunc']/CRylds['C'])**2 + (CRylds['Aunc']/CRylds['A'])**2)
    
    # print 'channel:', binid 
    # print 'predicted DD yield:'
    print '"',binid,'":', '[',round(calcyld,3), ',', round(calcunc,3),'],'
    # if args.base=='VR':
    #     print 'true data yield:',  round(Dyld,3), '+/-', round(Dunc,3)
        # ratio =round((Dyld/(calcyld)),2)
        # err = round(ratio*np.sqrt((Dunc/Dyld)**2+(calcunc/calcyld)**2), 2)
        # print 'data/pred:', ratio,'+/-',err
    # if subtractMC: #mcDyld, mcDunc
    #     print '% MC yield:', round(mcDyld/(calcyld+mcDyld),3)
    # if args.base=='SR':
    #     print 's/b:', round(tttty/calcyld,3),  '+/-', round((tttty/calcyld)*np.sqrt((calcunc/calcyld)**2+(ttttyu/tttty)**2),3)

    print '\n'
        
        
