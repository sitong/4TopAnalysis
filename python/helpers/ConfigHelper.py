# analysis constants and settings.
import os
import numpy as np
import math
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from  PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoTrees.helpers.tmvahelper import TMVAHelper

#sets all the lepton parameters based on channel, gets lepton collections
class LepSetter:
    #class variables
    mu_pt, mu_eta, mu_ID, mu_isotype, mu_isoval, el_pt, el_eta, el_ID = 0.0, 0.0, '', '', 0, 0.0, 0.0, 0
    def __init__(self, channel, year):
        lep_dict = {'0lep' : { '2016': {'mu_pt':      10.0,
                                        'mu_eta':     2.5,
                                        'mu_ID':      'Loose',
                                        'mu_isotype': 'pf4',
                                        'mu_isoval':  0.25,
                                        'el_pt':      15.0,
                                        'el_eta':     2.5, 
                                        'el_ID':      1 },

                               '2017': {'mu_pt':      10.0,
                                        'mu_eta':     2.5,
                                        'mu_ID':      'Loose',
                                        'mu_isotype': 'pf4',
                                        'mu_isoval':  0.25,
                                        'el_pt':      15.0,
                                        'el_eta':     2.5, 
                                        'el_ID':      1 },

                               '2018': {'mu_pt':      10.0,
                                        'mu_eta':     2.5,
                                        'mu_ID':      'Loose',
                                        'mu_isotype': 'pf4',
                                        'mu_isoval':  0.25,
                                        'el_pt':      15.0,
                                        'el_eta':     2.5, 
                                        'el_ID':      1 } },

                    '1lep' : { '2016': {'mu_pt':      26.0,
                                        'mu_eta':     2.1,
                                        'mu_ID':      'Tight',
                                        'mu_isotype': 'pf4',
                                        'mu_isoval':  0.15,
                                        'el_pt':      32.0,
                                        'el_eta':     2.1, 
                                        'el_ID':      4 },

                               '2017': {'mu_pt':      26.0,
                                        'mu_eta':     2.1,
                                        'mu_ID':      'Tight',
                                        'mu_isotype': 'pf4',
                                        'mu_isoval':  0.15,
                                        'el_pt':      32.0,
                                        'el_eta':     2.1, 
                                        'el_ID':      4 },

                               '2018': {'mu_pt':      26.0,
                                        'mu_eta':     2.1,
                                        'mu_ID':      'Tight',
                                        'mu_isotype': 'pf4',
                                        'mu_isoval':  0.15,
                                        'el_pt':      32.0,
                                        'el_eta':     2.1, 
                                        'el_ID':      4 } } }

        self.mu_pt      = lep_dict[channel][year]['mu_pt']
        self.mu_eta     = lep_dict[channel][year]['mu_eta']
        self.mu_ID      = lep_dict[channel][year]['mu_ID'] #Medium, Loose, Tight
        self.mu_isotype = lep_dict[channel][year]['mu_isotype'] #pf4 or mini
        self.mu_isoval  = lep_dict[channel][year]['mu_isoval']
        self.el_pt      = lep_dict[channel][year]['el_pt'] #was 35.0 #changed for cr cuts
        self.el_eta     = lep_dict[channel][year]['el_eta']
        self.el_ID      = lep_dict[channel][year]['el_ID'] # 0=fail, 1=veto, 2=loose, 3=medium, 4=tight cutbased ids 

        self.clean_jetsvleps = False
        if channel=='1lep':
            self.clean_jetsvleps = True

   #returns needed collections, rather than filling trees. always returns cleaned ak4jet collection, more than that is optional 
    def getLeps(self, event):
        ak4jets   = Collection(event, "Jet")
        muons     = Collection(event, "Muon")
        electrons = Collection(event, "Electron")
        SelMus = []; SelEls = []; SelLeps=[]
        for mu in muons:
            if (mu.pt>=self.mu_pt and abs(mu.eta)<=self.mu_eta): 
                if (mu.looseId >= self.mu_ID):
                    if (mu.miniIsoId >= self.mu_isoval):
                        SelMus.append(mu); SelLeps.append(mu)
                        if self.clean_jetsvleps: ak4jets = phys.ExcludeCloseNeighbors(mu, ak4jets, minDR=0.4) 
        for el in electrons:
            if (el.pt>=self.el_pt and abs(el.eta + el.deltaEtaSC)<=self.el_eta):
                if (abs(el.eta + el.deltaEtaSC)<1.442 or abs(el.eta + el.deltaEtaSC)>1.566): #isEBEEGap. why on veto?
                    if (el.mvaFall17V2noIso_WPL >= self.el_ID):
                        if (el.miniPFRelIso_all <= self.el_isoval):
                            SelEls.append(el); SelLeps.append(el)
                            if self.clean_jetsvleps: ak4jets = phys.ExcludeCloseNeighbors(el, ak4jets, minDR=0.4) 

        # for mu in muons:
        #     if (mu.pt>=self.mu_pt and abs(mu.eta)<self.mu_eta): 
        #         if ((self.mu_isotype=='pf4' and  mu.pfRelIso04_all<self.mu_isoval) or (self.mu_isotype=='mini' and  mu.miniPFRelIso_all<self.mu_isoval)):
        #             if ((self.mu_ID=='Loose'and mu.looseId) or (self.mu_ID=='Medium' and mu.mediumId) or (self.mu_ID=='Tight' and mu.tightId)):
        #                 SelMus.append(mu)
        #                 # nmus+=1
        #                 if self.clean_jetsvleps: ak4jets = phys.ExcludeCloseNeighbors(mu, ak4jets, minDR=0.4) #optimize? note this is before jet cleaning. removes jets overlapping with leptons
        # for el in electrons:
        #     if (el.pt>=self.el_pt and abs(el.eta)<self.el_eta and el.cutBased>=self.el_ID): 
        #         SelEls.append(el)
        #         # nels+=1
        #         if self.clean_jetsvleps: ak4jets = phys.ExcludeCloseNeighbors(el, ak4jets, minDR=0.4) 
        SelMus = np.array(SelMus)
        SelEls = np.array(SelEls)
        SelLeps = np.concatenate((SelMus,SelEls), axis=None)
        SelLeps = list(SelLeps)
        SelLeps.sort( key=lambda x : x.pt, reverse=True)        #sort highest to lowest pt
        SelLeps = np.array(SelLeps)
        if self.clean_jetsvleps:
            return ak4jets, SelEls, SelMus, SelLeps
        return SelEls, SelMus, SelLeps

# sets all jet parameters based on channel and btype
class JetSetter:
    jpt, jeta, usejid,  min_njets, bpt, beta, bWP, btype = 0.0, 0.0, False, 0, 0.0, 0.0, 0.0, ''

    def __init__(self, channel, year, BType='deepflav', bWPname='M'):
        bdict = {'2016' : { 'csvv2':    {'L':0.5426, 'M':0.8484, 'T':0.9535 },
                            'deepcsv':  {'L':0.2219, 'M':0.6324, 'T':0.8958 },
                            'deepflav': {'L':0.0614, 'M':0.3093, 'T':0.7221 } },

                 '2017' : { 'deepcsv':  {'L':0.1522, 'M':0.4941, 'T':0.8001 },
                            'deepflav': {'L':0.0521, 'M':0.3033, 'T':0.7489 } },

                 '2018' : { 'deepcsv':  {'L':0.1241, 'M':0.4184, 'T':0.7527 },
                            'deepflav': {'L':0.0494, 'M':0.2770, 'T':0.7264 } } }

        self.jpt       = 35.0
        self.jeta      = 2.4
        self.usejid    = True
        self.bpt       = 35.0
        self.beta      = 2.4
        self.btype     = BType
        self.bWP       = bdict[year][BType][bWPname]
        self.jh = JetHelper(self.btype, self.bWP)
        self.base_min_njets  = 9 #presel
        self.base_min_nbjets = 3 #presel
        self.base_min_ht     = 700 #presel
        self.year            = year
     
    #jet selection
    def getJets(self, year, event, ak4Jets, getvars=False):
        # if ak4jets==None:
        #     ak4jets   = Collection(event, "Jet")
        cleanjets  = []
        cleanbjets = []
        njets=0; nbjets=0; ht = 0.0; bjht=0.0; qglsum=0.0; qglprod=1.0; qglsum30=0.0
        jetpts = np.zeros(event.nJet); jetetas = np.zeros(event.nJet); passjetpresel = np.zeros(event.nJet) 
        sumE=0.0; centrality=0.0; meanbdisc=0.0; sumbdisc=0.0; ht6=0.0
        for i,j in enumerate(ak4Jets):
            if getvars: jetpts[i]=j.pt; jetetas[i]=j.eta
            if self.jh.cleanJets(j, self.jpt, self.jeta, self.usejid, year):
                if getvars:
                    passjetpresel[i]=1; njets+=1; ht+=j.pt; qglsum+=j.qgl; qglprod*=j.qgl
                    if j.pt>=30.0: qglsum30+=j.qgl
                    sumE += np.sqrt((j.pt**2)*np.cosh(j.eta)**2 + j.mass**2)
                cleanjets.append(j)
            if self.jh.cleanBJets(j, self.bpt, self.beta, self.usejid, year):
                cleanbjets.append(j)
                if getvars:
                    nbjets+=1; bjht+=j.pt
                    sumbdisc+=j.btagDeepFlavB
        if sumE>0.0:
            centrality = ht/sumE
        if nbjets>0:
            meanbdisc = sumbdisc/nbjets
        
        cleanjets  = np.array(cleanjets)
        cleanbjets = np.array(cleanbjets)
        if getvars:
            return cleanjets, cleanbjets, passjetpresel, njets, nbjets,ht, bjht, qglsum, qglsum30, qglprod, jetpts, jetetas, centrality, meanbdisc
        return cleanjets, cleanbjets

class BoostSetter:
    #1% mistag WPs (not MD) from https://indico.cern.ch/event/877167/contributions/3744193/attachments/1989744/3379280/DeepAK8_Top_W_SFs_V2.pdf
    boost_dict = {'2016' : {'Wwp':0.918, 'Twp':0.834},
                  '2017' : {'Wwp':0.925, 'Twp':0.725},
                  '2018' : {'Wwp':0.918, 'Twp':0.802} }
    DAK8W_PT   = 200.0
    DAK8TOP_PT = 400.0
    jpt = 200.0
    jeta = 2.4

    def __init__(self, year):
        self.DAK8W_WP   = self.boost_dict[year]['Wwp'] 
        self.DAK8TOP_WP = self.boost_dict[year]['Twp'] 
        self.DAK8W_PT = 200.0
        self.DAK8TOP_PT = 400.0
        self.etacut = 2.4

    def SelBoostedWs(self, fatj, wp, pt):
        if fatj.deepTag_WvsQCD > wp and fatj.pt >= pt:
            return 1
        return 0

    def SelBoostedTops(self, fatj, wp, pt):
        if fatj.deepTag_TvsQCD > wp and fatj.pt >= pt:
            return 1
        return 0

    #fatjet selection:            
    def getBoosts(self, event, FatJets=None ,getvars=False):
        # if getvars: vardict = { 'sfjm': 0.0 }
        fatjets=FatJets
        if fatjets==None:
            fatjets   = Collection(event, "FatJet")
        fjpt = self.jpt
        fjeta = self.jeta
        sfjm=0.0
        bstcands = []; bswcands = []
        for i,fj in enumerate(fatjets):
            if (fj.pt>=fjpt and abs(fj.eta)<fjeta):
                sfjm+=fj.mass
                seltop = self.SelBoostedTops(fj, self.DAK8TOP_WP, self.DAK8TOP_PT)
                selw   = self.SelBoostedWs(fj, self.DAK8W_WP, self.DAK8W_PT)
                if seltop: #and not selw:
                    bstcands.append(fj)
                if selw: #and not seltop:
                    bswcands.append(fj)
        bstcands.sort(key=lambda x : x.pt, reverse=True) #sort highest to lowest pt
        bswcands.sort(key=lambda x : x.pt, reverse=True)
    
        bstcands = np.array(bstcands)
        bswcands   = np.array(bswcands)
 
        if getvars:
            # vardict['sfjm']=sfjm
            return bstcands, bswcands, sfjm
        else:
            return bstcands, bswcands

#for setting bdt variables
class ResSetter:
    resthenboost    = True
    useTopWP        = True
    cleanWs         = False
    TopCand_minpt   = 100.0
    minjets         = 3 #minimum number of jets for a topcand
    Bimax           = 4 #number of top scoring csv jets to consider
    topmassrange    = 80
    wmassrange      = 40
    topmassparam    = 175.0 #really 172
    wmassparam      = 80.0
    cleanAK4overlap = 0.8 
    etaMax          = 2.4 #for top
    bdt_file = ''
    bdt_vars = []
    
    def __init__(self, tagtype='nano', year='2016'):

        # if not self.useTopWP:
        #     print '#############################################'
        #     print 'WARNING: no WP applied in resolved top tagger.'
        #     print '#############################################'

        if tagtype=='nano':
            topWP = { '2016':0.9988,
                      '2017':0.9992,
                      '2018':0.9994 }

            self.topWP = topWP[year]

            # self.bdt_file = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/ResMVAweights/nano_train_check_50k.xml')
            self.bdt_file = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/ResMVAweights/xgboost-NANO_deepbase.xml') #xgboostNANO.xml')
        # print bdt_file
            self.bdt_vars = [
                'var_b_mass', #static                       
                'var_topcand_mass',
                'var_topcand_ptDR',
                'var_wcand_mass',
                'var_wcand_ptDR',
                'var_b_j2_mass',
                'var_b_j3_mass',
                'var_sd_n2', 
                'var_b_btagDeepFlavB',#nanomod version               
                'var_j2_btagDeepFlavB',                              
                'var_j3_btagDeepFlavB',                              
                'var_j2_qgl',                                        
                'var_j2_btagDeepFlavC',                              
                'var_j3_qgl',                                        
                'var_j3_btagDeepFlavC',                              
                'var_j2_nConstituents', #nanomod version             
                'var_j3_nConstituents'                               
                ]
    
    
