import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import Defs as defs
import RootStuff as rs
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
gStyle.SetPalette(1)

parser = argparse.ArgumentParser(description='bla')
parser.add_argument("-p", "--proc", dest="proc", default='TTTT')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

infile = 'TTTT_'+args.year+'_merged_0lep_tree.root' 
extras = "nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1200 && "

def makeroot(infile, treename='Events', options="read"):
    rfile = r.TFile(infile, options)
    tree = rfile.Get(treename)
    return rfile, tree

#unweighted yield: NEnt. weighted yield: rate
def getyield(tree, cutstring, projvar='ht', lumi='36.0'):
    htmp = r.TH1D('htmp','htmp',1,0,100000)
    # cutstr = '('+str(lumi)+'*'+weight+')*('+cutstr+')'
    print 'CUTSTRING', "("+cutstring+")"
    NEnt = tree.Draw(projvar+">>htmp", cutstring)
    rate = htmp.Integral(0,2)
    # print 'file:', infile, 'cutstring:', cutstring, 'lumi', lumi
    print "raw event #:", NEnt, 'yield:', rate
    return NEnt, rate

rf,t = makeroot(infile)

if args.year == '2016':
    cut1 = "("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag))*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*35.91 "
    cut2 ="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag))*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*35.91"
    cut3 ="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag))*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*bSFcorr*35.91"
elif args.year == '2017':
    cut1="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*41.53 "
    cut2="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*41.53 "
    cut3="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*bSFcorr*41.53"
elif args.year == '2018':
    cut1="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*59.74 "
    cut2="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*59.74"
    cut3="("+extras+"nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*bSFcorr*59.74"

nent, yld = getyield(t, cut1, 'ht_nosys')
nent, yld = getyield(t, cut2, 'ht_nosys')
nent, yld = getyield(t, cut3, 'ht_nosys')
