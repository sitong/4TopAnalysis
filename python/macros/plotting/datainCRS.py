#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
from collections import OrderedDict
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
import yieldtable as yt

parser = argparse.ArgumentParser(description='yield table')
parser.add_argument("-y", "--year", dest="year", default='2016')
parser.add_argument("-r", "--region", dest="region", default='SR')
args = parser.parse_args()

class datainCRs:
    def __init__(self, args):
        if args.region =='SR':
            self.CRs = yt.yieldtable(args, yt.SRsels)
        elif args.region =='VR':
            self.CRs = yt.yieldtable(args, yt.VRsels)
        self.year = args.year
        self.pd = plotdefs(self.year, args.region, False)
        self.var='catBDTdisc_nosys'
        self.treename = 'Events'
        # self.procs = ['data', 'tttt', "QCD+ttbar" , 'ttX', 'other', 'Total B', 'data/pred']
        # self.procs = ['data', 'tttt', "QCD+ttbar" , 'ttH', 'ttW', 'ttZ', 'ttOther', 'other', 'Total B', 'data/pred']
    
        # c = r.TCanvas("c","c",800,800)
        # p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        # p1.Draw()
        # p1.cd()

    def print_plots(self):
        for label, binsel in self.CRs.seldict.iteritems():
            print '-----------------------'
            print 'making plots for', label
            plots = self.getplots(label, binsel)
            self.fourpads(label, plots)

    def getplots(self, label, binsel):
        mapbinsel = {'SR RT1BT0 HTbin0':'cut0bin0',
                     'SR RT1BT0 HTbin1':'cut0bin1',
                     'SR RT1BT0 HTbin2':'cut0bin2',
                     'SR RT1BT0 HTbin3':'cut0bin3',
                     'SR RT1BT0 HTbin4':'cut0bin4',
                     'SR RT1BT0 HTbin5':'cut0bin5',
                     'SR RT1BT0 HTbin6':'cut0bin6',
                     'SR RT1BT0 HTbin7':'cut0bin7',
                     'SR RT1BT1 HTbin0':'cut1bin0',
                     'SR RT1BT1 HTbin1':'cut1bin1',
                     'SR RT2BTALL HTbin0':'cut2bin0',
                     'SR RT2BTALL HTbin1':'cut2bin1'}
        
        crs = self.CRs.getCRs(binsel)
        # crylds = self.CRs.abcdyields(crs)
        crs['D']=binsel #add D
        plots={}

        for CR, sel in crs.iteritems(): #yields for A,B,C,D,X,Y
            print CR
            print sel

            datahist = self.pd.gethist(self.pd.datafilename, self.var, sel)
            datahist.SetLineWidth(3)
            datahist.SetMarkerStyle(20)
            datahist.SetLineColor(r.kGray+2)
            dyld, dunc = self.pd.getyield(datahist)
            datahist.Scale(100.0/dyld)
                
            if CR=='D':
                chanid = mapbinsel[label]
                predhist = self.pd.histfromtree(chanid)
                pyld, punc = self.pd.getyield(predhist)
                predhist.Scale(100.0/pyld)
                predunchist = predhist.Clone('D')

                predhist.SetLineWidth(3)
                predhist.SetMarkerStyle(20)
                predhist.SetLineColor(r.kBlue)
                
                # predunchist.SetLineWidth(2)
                predunchist.SetLineColor(r.kBlue-10)
                predunchist.SetFillColor(r.kBlue-10)
                # predhist.Scale(crylds['D']/pyld) #not MC subtracted
                plots['Dpred']=predhist
                plots['Dunc']=predunchist

                # sel = sel.replace('njets_nosys>=9', 'njets_nosys==9')
                # data9hist = self.pd.gethist(self.pd.datafilename, self.var, sel)
                # data9hist.SetLineWidth(2)
                # data9hist.SetLineStyle(9)
                # data9hist.SetLineColor(r.kRed+1)
                # d9yld, d9unc = self.pd.getyield(data9hist)
                # data9hist.Scale(100.0/dyld)
                # plots['Dmod']=data9hist

            plots[CR]= datahist
                
        return plots

    def fourpads(self, label, plots):

        c1 = r.TCanvas(); c1.DrawFrame(0.,0.,1.,30.); plots['Y'].Draw('histe') ; plots['Y'].SetTitle('Y') ; plots['Y'].SetMaximum(30.0)
        c2 = r.TCanvas(); c2.DrawFrame(0.,0.,1.,30.); plots['B'].Draw('histe') ; plots['B'].SetTitle('B') ; plots['B'].SetMaximum(30.0)
        c3 = r.TCanvas(); c3.DrawFrame(0.,0.,1.,30.); plots['Dunc'].Draw('e2') ; plots['Dpred'].Draw('histsame') ; plots['D'].Draw('histesame') ; plots['Dunc'].SetTitle('D') ; plots['Dunc'].SetMaximum(30.0); #plots['Dmod'].Draw('histsame')
        c4 = r.TCanvas(); c4.DrawFrame(0.,0.,1.,30.); plots['X'].Draw('histe') ; plots['X'].SetTitle('X') ; plots['X'].SetMaximum(30.0)
        c5 = r.TCanvas(); c5.DrawFrame(0.,0.,1.,30.); plots['A'].Draw('histe') ; plots['A'].SetTitle('A') ; plots['A'].SetMaximum(30.0)
        c6 = r.TCanvas(); c6.DrawFrame(0.,0.,1.,30.); plots['C'].Draw('histe') ; plots['C'].SetTitle('C'); plots['C'].SetMaximum(30.0)
        
        SixPads = r.TCanvas("SixPads","SixPads", 3000, 2100)
        SixPads.Divide(3,2)
        SixPads.cd(1) ; c1.DrawClonePad()
        SixPads.cd(2) ; c2.DrawClonePad()
        SixPads.cd(3) ; c3.DrawClonePad()
        SixPads.cd(4) ; c4.DrawClonePad()
        SixPads.cd(5) ; c5.DrawClonePad()
        SixPads.cd(6) ; c6.DrawClonePad()
            
        gPad.Modified()
        gPad.Update()

        print 'saved histogram: ', self.pd.outdir+label+'.png'
        SixPads.SaveAs(self.pd.outdir+label+'.png')

    
runplots = datainCRs(args)
runplots.print_plots()
