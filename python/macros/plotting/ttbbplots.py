#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
from collections import OrderedDict
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
import yieldtable as yt

parser = argparse.ArgumentParser(description='yield table')
parser.add_argument("-y", "--year", dest="year", default='2018')
parser.add_argument("-r", "--region", dest="region", default='SR')
parser.add_argument("-t", "--ttbb", dest="ttbb", default='True')
args = parser.parse_args()

class ttbbplots:
    def __init__(self, args):
        if args.region =='SR':
            self.CRs = yt.yieldtable(args, yt.SRsels)
        elif args.region =='VR':
            self.CRs = yt.yieldtable(args, yt.VRsels)
        self.year = args.year
        self.pd = plotdefs(self.year, args.region, False)
        self.var='catBDTdisc_nosys'
        self.treename = 'Events'
        # self.procs = ['data', 'tttt', "QCD+ttbar" , 'ttX', 'other', 'Total B', 'data/pred']
        # self.procs = ['data', 'tttt', "QCD+ttbar" , 'ttH', 'ttW', 'ttZ', 'ttOther', 'other', 'Total B', 'data/pred']
    
        # c = r.TCanvas("c","c",800,800)
        # p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        # p1.Draw()
        # p1.cd()

    def print_plots(self):
        for label, binsel in self.CRs.seldict.iteritems():
            print '-----------------------'
            print 'making plots for', label
            plots = self.getplots(label, binsel)
            self.fourpads(label, plots)

    def getplots(self, label, binsel):
        mapbinsel = {
            'SR RT1BT0 HTbin0':'cut0bin0',
            'SR RT1BT0 HTbin1':'cut0bin1',
            'SR RT1BT0 HTbin2':'cut0bin2',
            'SR RT1BT0 HTbin3':'cut0bin3',
            'SR RT1BT0 HTbin4':'cut0bin4',
            'SR RT1BT0 HTbin5':'cut0bin5',
            'SR RT1BT0 HTbin6':'cut0bin6',
            'SR RT1BT0 HTbin7':'cut0bin7',
            'SR RT1BT1 HTbin0':'cut1bin0',
            'SR RT1BT1 HTbin1':'cut1bin1',
            'SR RT2BTALL HTbin0':'cut2bin0',
            'SR RT2BTALL HTbin1':'cut2bin1'
        }
        
        crs = self.CRs.getCRs(binsel)
        # crylds = self.CRs.abcdyields(crs)
        crs['D']=binsel #add D
        plots={}

        for CR, sel in crs.iteritems(): #yields for A,B,C,D,X,Y
            print CR
            print '('+sel+self.pd.mcstr[self.year] #sel

            datahist = self.pd.gethist(self.pd.datafilename, self.var, '('+sel+')'+self.pd.dstr[self.year])
            datahist.SetLineWidth(3)
            datahist.SetMarkerStyle(20)
            datahist.SetLineColor(r.kGray+2)
            dyld, dunc = self.pd.getyield(datahist)
            datahist.Scale(100.0/dyld)

            TTincfile = self.pd.mcfiles['ttbb']
            # TTincfile = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/python/macros/eventMVA/TTupdatedrootfiles/ttinc_'+self.year+'_merged.root'
            mysel = '('+sel+self.pd.mcstr[self.year]
            TTinchist = self.pd.gethist(TTincfile, self.var, mysel)
            # TTinchist = self.pd.gethist(self.pd.mcfiles['TT'], self.var, '('+sel+self.pd.mcstr[self.year])
            TTinchist.SetLineWidth(3)
            TTinchist.SetMarkerStyle(20)
            TTinchist.SetLineColor(r.kMagenta)
            ttiyld, ttiunc = self.pd.getyield(TTinchist)
            TTinchist.Scale(100.0/dyld)

            TTtagfile = self.pd.mcfiles['ttbb']#'/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/python/macros/eventMVA/TTupdatedrootfiles/ttinc_'+self.year+'_merged.root'
            modsel = '('+sel+self.pd.mcstr[self.year]
            modsel=modsel.replace("nfunky_leptons==0 &&","nfunky_leptons==0 && isttbb==1 &&")
            print modsel
            TTtaghist = self.pd.gethist(TTtagfile, self.var, modsel)
            # TTtaghist = self.pd.gethist(self.pd.mcfiles['TT'], self.var, '('+sel+self.pd.mcstr[self.year])
            TTtaghist.SetLineWidth(3)
            TTtaghist.SetMarkerStyle(20)
            TTtaghist.SetLineColor(r.kRed)
            ttiyld, ttiunc = self.pd.getyield(TTtaghist)
            TTtaghist.Scale(100.0/dyld)

            # TTbbfile = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/python/macros/eventMVA/TTupdatedrootfiles/ttbb_'+self.year+'_merged.root'
            # TTbbhist = self.pd.gethist(TTbbfile, self.var, '('+sel+self.pd.mcstr[self.year])
            # TTbbhist.SetLineWidth(3)
            # TTbbhist.SetMarkerStyle(20)
            # TTbbhist.SetLineColor(r.kGreen)
            # ttbbyld, ttbbunc = self.pd.getyield(TTbbhist)
            # TTbbhist.Scale(100.0/dyld)

            # for ibin in range(0, TTtaghist.GetNbinsX()+2):
            #     print TTtaghist.GetBinContent(ibin), TTtaghist.GetBinContent(ibin)
            #     if TTtaghist.GetBinContent(ibin)<=0.0:
            #         TTtaghist.SetBinContent(ibin, 0.0001)

            
            if CR=='D':
                chanid = mapbinsel[label]
                predhist = self.pd.histfromtree(chanid)
                pyld, punc = self.pd.getyield(predhist)
                # predhist.Scale(100.0/pyld)
                predunchist = predhist.Clone('D')
                predhist.SetLineWidth(3)
                predhist.SetMarkerStyle(20)
                predhist.SetLineColor(r.kBlue)
                
                # predunchist.SetLineWidth(2)
                predunchist.SetLineColor(r.kBlue-10)
                predunchist.SetFillColor(r.kBlue-10)
                predhist.Scale(100.0/dyld) #not MC subtracted
                predunchist.Scale(100.0/dyld) #not MC subtracted
                plots['Dpred']=predhist
                plots['Dunc']=predunchist

                # sel = sel.replace('njets_nosys>=9', 'njets_nosys==9')
                # data9hist = self.pd.gethist(self.pd.datafilename, self.var, sel)
                # data9hist.SetLineWidth(2)
                # data9hist.SetLineStyle(9)
                # data9hist.SetLineColor(r.kRed+1)
                # d9yld, d9unc = self.pd.getyield(data9hist)
                # data9hist.Scale(100.0/dyld)
                # plots['Dmod']=data9hist


            ratiohist  = TTtaghist.Clone('ratio')
            ratiohist.Divide(TTtaghist)
            print 'dividing histograms'
            plots[CR]=datahist
            plots['TTinc_'+CR]=TTinchist
            plots['TTtag_'+CR]=TTtaghist
            # plots['TTbb_'+CR]=TTtaghist
            plots['ratio_'+CR]=ratiohist
            # plots['tttt_'+CR]=tttthist

        return plots

    def fourpads(self, label, plots):

        #need tt and ttbb ratios. and signal, and  log scale y

        c1 = r.TCanvas(); p1n1 = r.TPad("p1n1","p1n1", 0, 0.3, 1, 1.0); p1n1.SetBottomMargin(0.1); p1n1.Draw(); p1n1.cd(); p1n1.SetLogy() 
        plots['Y'].Draw('histe') ; plots['TTinc_Y'].Draw('histesame') ;plots['TTtag_Y'].Draw('histesame') ; plots['TTbb_Y'].Draw('histesame') ; plots['Y'].SetTitle('Y') ; #plots['Y'].SetMaximum(400.0)
        gPad.Modified(); gPad.Update(); c1.cd(); p1n2 = r.TPad("p1n2", "p1n2", 0, 0.05, 1, 0.3); p1n2.Draw(); p1n2.cd();
        plots['ratio_Y'].Draw('pe0'); plots['ratio_Y'].SetLineWidth(3); plots['ratio_Y'].SetMinimum(0.0); plots['ratio_Y'].SetMaximum(2.0)
        p1n2.SetTopMargin(0.1); p1n2.SetBottomMargin(0.2); #p1n2.SetGridy();
        l1 = r.TLine(0.0, 1.0, 1.0, 1.0); l1.SetLineColor(r.kBlack); l1.SetLineWidth(1); l1.Draw("same"); c1.cd()

        c2 = r.TCanvas(); p2n1 = r.TPad("p2n1","p2n1", 0, 0.3, 1, 1.0); p2n1.SetBottomMargin(0.1); p2n1.Draw(); p2n1.cd(); p2n1.SetLogy() 
        plots['B'].Draw('histe') ; plots['TTinc_B'].Draw('histesame') ; plots['TTtag_B'].Draw('histesame') ;plots['TTbb_Y'].Draw('histesame') ; plots['B'].SetTitle('B') ; #plots['B'].SetMaximum(400.0)
        gPad.Modified(); gPad.Update(); c2.cd(); p2n2 = r.TPad("p2n2", "p2n2", 0, 0.05, 1, 0.3); p2n2.Draw(); p2n2.cd();
        plots['ratio_B'].Draw('pe0'); plots['ratio_B'].SetLineWidth(3); plots['ratio_B'].SetMinimum(0.0); plots['ratio_B'].SetMaximum(2.0)
        p2n2.SetTopMargin(0.1); p2n2.SetBottomMargin(0.2); #p2n2.SetGridy();
        l2 = r.TLine(0.0, 1.0, 1.0, 1.0); l2.SetLineColor(r.kBlack); l2.SetLineWidth(1); l2.Draw("same"); c2.cd()

        c3 = r.TCanvas(); p3n1 = r.TPad("p3n1","p3n1", 0, 0.3, 1, 1.0); p3n1.SetBottomMargin(0.1); p3n1.Draw(); p3n1.cd(); p3n1.SetLogy() 
        plots['Dunc'].Draw('e2') ; plots['Dpred'].Draw('histesame') ; plots['TTinc_D'].Draw('histesame') ; plots['TTtag_D'].Draw('histesame') ; plots['TTbb_D'].Draw('histesame') ; plots['Dunc'].SetTitle('D') ; #plots['Dunc'].SetMaximum(400.0); #plots['Dmod'].Draw('histesame')
        gPad.Modified(); gPad.Update(); c3.cd(); p3n2 = r.TPad("p3n2", "p3n2", 0, 0.05, 1, 0.3); p3n2.Draw(); p3n2.cd();
        plots['ratio_D'].Draw('pe0'); plots['ratio_D'].SetLineWidth(3); plots['ratio_D'].SetMinimum(0.0); plots['ratio_D'].SetMaximum(2.0)
        p3n2.SetTopMargin(0.1); p3n2.SetBottomMargin(0.2); #p3n2.SetGridy();
        l3 = r.TLine(0.0, 1.0, 1.0, 1.0); l3.SetLineColor(r.kBlack); l3.SetLineWidth(1); l3.Draw("same"); c3.cd()

        c4 = r.TCanvas(); p4n1 = r.TPad("p4n1","p4n1", 0, 0.3, 1, 1.0); p4n1.SetBottomMargin(0.1); p4n1.Draw(); p4n1.cd(); p4n1.SetLogy() 
        plots['X'].Draw('histe') ; plots['TTinc_X'].Draw('histesame') ; plots['TTtag_X'].Draw('histesame') ;plots['TTbb_X'].Draw('histesame') ; plots['X'].SetTitle('X') ; #plots['X'].SetMaximum(400.0)
        gPad.Modified(); gPad.Update(); c4.cd(); p4n2 = r.TPad("p4n2", "p4n2", 0, 0.05, 1, 0.3); p4n2.Draw(); p4n2.cd();
        plots['ratio_X'].Draw('pe0'); plots['ratio_X'].SetLineWidth(3); plots['ratio_X'].SetMinimum(0.0); plots['ratio_X'].SetMaximum(2.0)
        p4n2.SetTopMargin(0.1); p4n2.SetBottomMargin(0.2); #p4n2.SetGridy();
        l4 = r.TLine(0.0, 1.0, 1.0, 1.0); l4.SetLineColor(r.kBlack); l4.SetLineWidth(1); l4.Draw("same"); c4.cd()

        c5 = r.TCanvas(); p5n1 = r.TPad("p5n1","p5n1", 0, 0.3, 1, 1.0); p5n1.SetBottomMargin(0.1); p5n1.Draw(); p5n1.cd(); p5n1.SetLogy() 
        plots['A'].Draw('histe') ; plots['TTinc_A'].Draw('histesame') ; plots['TTtag_A'].Draw('histesame') ;plots['TTbb_A'].Draw('histesame') ; plots['A'].SetTitle('A') ; #plots['A'].SetMaximum(400.0)
        gPad.Modified(); gPad.Update(); c5.cd(); p5n2 = r.TPad("p5n2", "p5n2", 0, 0.05, 1, 0.3); p5n2.Draw(); p5n2.cd();
        plots['ratio_A'].Draw('pe0'); plots['ratio_A'].SetLineWidth(3); plots['ratio_A'].SetMinimum(0.0); plots['ratio_A'].SetMaximum(2.0)
        p5n2.SetTopMargin(0.1); p5n2.SetBottomMargin(0.2); #p5n2.SetGridy(); 
        l5 = r.TLine(0.0, 1.0, 1.0, 1.0); l5.SetLineColor(r.kBlack); l5.SetLineWidth(1); l5.Draw("same"); c5.cd()

        c6 = r.TCanvas(); p6n1 = r.TPad("p6n1","p6n1", 0, 0.3, 1, 1.0); p6n1.SetBottomMargin(0.1); p6n1.Draw(); p6n1.cd(); p6n1.SetLogy() 
        plots['C'].Draw('histe') ; plots['TTinc_C'].Draw('histesame') ; plots['TTtag_C'].Draw('histesame') ;plots['TTbb_C'].Draw('histesame') ; plots['C'].SetTitle('C'); #plots['C'].SetMaximum(400.0)
        gPad.Modified(); gPad.Update(); c6.cd(); p6n2 = r.TPad("p6n2", "p6n2", 0, 0.05, 1, 0.3); p6n2.Draw(); p6n2.cd();
        plots['ratio_C'].Draw('pe0'); plots['ratio_C'].SetLineWidth(3); plots['ratio_C'].SetMinimum(0.0); plots['ratio_C'].SetMaximum(2.0)
        p6n2.SetTopMargin(0.1); p6n2.SetBottomMargin(0.2); #p6n2.SetGridy();
        l6 = r.TLine(0.0, 1.0, 1.0, 1.0); l6.SetLineColor(r.kBlack); l6.SetLineWidth(1); l6.Draw("same"); c6.cd()


        SixPads = r.TCanvas("SixPads","SixPads", 3000, 2100)
        SixPads.Divide(3,2)
        SixPads.cd(1) ; c1.DrawClonePad()
        SixPads.cd(2) ; c2.DrawClonePad()
        SixPads.cd(3) ; c3.DrawClonePad()
        SixPads.cd(4) ; c4.DrawClonePad()
        SixPads.cd(5) ; c5.DrawClonePad()
        SixPads.cd(6) ; c6.DrawClonePad()
            
        gPad.Modified()
        gPad.Update()

        print 'saved histogram: ', self.pd.outdir+label+'.png'
        SixPads.SaveAs(self.pd.outdir+label+'.png')

    
runplots = ttbbplots(args)
runplots.print_plots()
