#easier plotting
# import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
# import re
import ROOT as r
from ROCStuff import ROCCurve
import RootStuff as rs
import scipy.integrate as integrate
r.gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

year = '2018'

#plots multiple roc plots together
def ROCplots(distplot=True, printeffs=False):
    font = {'family': 'serif',
            'color':  'black',
            'weight': 'normal',
            'size': 16,
            }
    #PARAMS

    outputdir = './plots/ROCs/'
    outname = 'rocs.png'
    lumi = str(rs.lumis[year]['0lep'])

    outfile = outputdir+outname
    # baseline = '(nbjets>=0 '
    baseline = '(nfunky_leptons==0 && ht>=700 & njets>=9 && nbjets>=3'
    # baseline = '(nleptons==1 && ht>500 & njets>=4 && met>=50 && lep1pt>=30 && nbjets>=3'
    
    ssel =  baseline + ' && rescand_genmatch==1)*weight*'+lumi
    bsel =  baseline + ' && rescand_genmatch!=1)*weight*'+lumi
    print 'BASELINE', baseline
    treename = 'Friends'
    
    indir = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/SFtrees/'

    sig = 'TTTT'
    # bgd = 'QCD'
    suffix = '_'+year+'_merged_0lepSF_tree.root'
    discvars = ['rescand_disc', 'rescand_disc']
    names  = ['QCD', 'TT']
    # names  = ['QCD', 'hadTT', '1lepTT']
    sigfiles = [indir+sig+suffix, indir+sig+suffix]
    bkgfiles = [indir+'QCD'+suffix, indir+'TT'+suffix]
    # sigfiles = [indir+sig+suffix, indir+sig+suffix, indir+sig+suffix]

    # discvars = ['resMVAtop_disc', 'resMVAtop_disc','resMVAtop_disc','resMVAtop_disc']
    # names  = ['nano', 'redo', 'mod', 'NN']
    # sigfiles = [indir+sig+suffix+'nano_tree.root', indir+sig+suffix+'mod_tree.root', indir+sig+suffix+'redo_tree.root', indir+sig+suffix+'NN_tree.root']
    # # sigfiles = [indir+sig+suffix+'nano_tree0.root', indir+sig+suffix+'mod_tree0.root', indir+sig+suffix+'redo_tree0.root', indir+sig+suffix+'NN_tree0.root']
    # bkgfiles = [indir+bgd+suffix+'nano_tree.root', indir+bgd+suffix+'mod_tree.root', indir+bgd+suffix+'redo_tree.root', indir+bgd+suffix+'NN_tree.root']
    print sigfiles
    print bkgfiles

    nsig = len(sigfiles)
    nbgd = len(bkgfiles)
    strees = ['']*nsig; srfiles = ['']*nsig
    btrees = ['']*nbgd; brfiles = ['']*nbgd
    rocs = []
    names = np.array(names)
    for i in range(len(discvars)):
        srfiles[i], strees[i] = rs.makeroot(sigfiles[i], treename)
        brfiles[i], btrees[i] = rs.makeroot(bkgfiles[i], treename)
        print srfiles[i], strees[i], brfiles[i], btrees[i]
        # snevts, syield = rs.getyield(strees[i], baseline)
        # bnevts, byield = rs.getyield(btrees[i], baseline)
        # print 'baseline', baseline, 'signal yield:', syield, 'background yield:', byield
        xmn = -1.0; xmx=1.0; nbns=10000
        # xmn = 0.9; xmx=1.0; nbns=50
        # if names[i]=='NN':
        #     xmn=0.0
        #     nbns = nbns/2
        #     ssel =  baseline + ' && deeptop_genmatch==1'+')*weight*36' 
        #     bsel =  baseline + ' && deeptop_genmatch!=1'+')*weight*36' 
        # if names[i]=='hadTT':
        #     bsel = baseline + '&& ngenleps==0 && rescand_genmatch!=1)*weight*36'
        # elif names[i]=='1lepTT':
        #     bsel = baseline + '&& ngenleps==1 && rescand_genmatch!=1)*weight*36'
        RC = ROCCurve(discvars[i], srfiles[i], brfiles[i], strees[i], btrees[i], ssel, bsel, nbins=nbns, xmin=xmn, xmax=xmx) #class instance
        # RC.getInfoForSignalEff(0.0)

        nbins, seff, beff =RC.computeROCCurve()
        # for b,s in zip(beff, seff):
        #     print 'b',b,'s', s
        roc = r.TGraph(nbins, beff, seff)
        canvas = r.TCanvas("canvas", "canvas", 800, 800)
        roc.Draw('')
        # canvas.Update()
        # canvas.SaveAs(outputdir+'test.png')
        plt.plot(beff, seff, marker='.', label=names[i], linewidth=2)
        axes = plt.gca()
        axes.set_xlim([0.0,1.0])
        axes.set_ylim([0.0,1.0])
        #get area under curve:
        area = -1.0*(np.trapz(seff, beff)) #negative because array in decending order
        print 'area under', names[i], 'ROC curve  is', area
        RC.getInfoForBackgroundEff(0.1)
        RC.getInfoForCut(0.999)
        RC.getInfoForCut(0.998)
        RC.getInfoForCut(0.9992)
        RC.getInfoForCut(0.9993)
        RC.getInfoForCut(0.9994)
        print names[i]
        rocs.append(roc)
        if distplot:
            getDists(RC.sighist, RC.bgdhist, title=names[i]+'_dist')#discvars[i]
            # title='eventBDT_disc_dist'; xtitle='BDT disc'; ytitle='N Entries'

    #plot things
    # c = r.TCanvas("c", "canvas", 800, 800)
    plt.xlabel('Background Efficiency', fontdict=font)
    plt.ylabel('Signal Efficiency', fontdict=font)
    plt.legend(loc='lower right')
    plt.grid()
    plt.savefig(outputdir+year+'ROC.png')
    print 'creating ', outputdir+year+'ROC.png'
    #multiplot does crazy shit for some reason??
    rocs = np.array(rocs)
    	# A cut of 0.49 corresponds to
	# signal efficiency = 90.355%
	# background efficiency = 66.867%

# plots sig and bkg disc distributions
# takes sig and bkg hists with discdist and selection already drawn! 
def getDists(sig, bkd, title='disc_dist', xtitle='BDT disc', ytitle='N Entries', normed=True):
    outputdir = './plots/ROCs/'
    outfile = outputdir+title+year+".png"
    if normed:
        sig = rs.normhist(sig)
        bkg = rs.normhist(bkd)
    plots = [sig, bkd]
    names = ['signal', 'background']
    c = r.TCanvas("c", "canvas", 800, 800)
    rs.multiplot(outfile, plots, names, xtitle, ytitle, legend=True)
    # l.Draw('alsame')
    # # c.Show()
    # c.SaveAs()


ROCplots()


    # sigfiles = ['../eventMVA/updatedrootfiles/TTTT_TuneCUETP8M2T4_13TeV-amcatnlo-pythia8_2_tree_SimpleBDT.root', '../eventMVA/updatedrootfiles/TTTT_TuneCUETP8M2T4_13TeV-amcatnlo-pythia8_2_tree_SimpleBDT.root', '../eventMVA/updatedrootfiles/TTTT_TuneCUETP8M2T4_13TeV-amcatnlo-pythia8_2_tree_SimpleBDT.root'] 

    # bkgfiles = ['../eventMVA/updatedrootfiles/TT_simpleBDT.root', '../eventMVA/updatedrootfiles/QCD_SimpleBDT.root', '../eventMVA/updatedrootfiles/QCD_TT_SimpleBDT.root']
    # discvars = ['SimpleBDTdisc', 'SimpleBDTdisc', 'SimpleBDTdisc']
    # names  = ['ttttvsTT', 'ttttvsQCD', 'ttttvsTT+QCD']
    #deets for each plot
    # discvars = ['ModBDT1disc', 'ModBDT2disc', 'ModBDT1disc', 'ModBDT2disc', 'NewBDT3disc', 'NewBDT3disc']
    # discvars = ['ModBDT1disc', 'ModBDT2disc', 'NewBDT1disc', 'NewBDT2disc', 'NewBDT3disc', 'Best12disc', 'Best20disc' ]
    # names  = ['mod1 QCD+TT', 'mod2 QCD+TT', 'mod1 TT', 'mod2 TT', 'new3 QCD+TT', 'new3 TT']
    # names  = ['mod1', 'mod2', 'new1', 'new2', 'new3', 'best12', 'best20']

    # sigfiles = ['../eventMVA/updatedrootfiles/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TTTT_withBDT_2016MC_merged_tree.root']

    # bkgfiles = ['../eventMVA/updatedrootfiles/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TT_withBDT_2016MC_merged_tree.root']
# /eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/updatedrootfiles
    # sigfiles = ['/eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/updatedrootfiles_TT/TTTT_withBDT_2016MC_merged_tree.root']*len(discvars) 
    # bkgfiles = ['/eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/updatedrootfiles_TT/QCD_withBDT_2016MC_merged_tree.root']*len(discvars)
    # sigfiles = ['/eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/updatedrootfiles/TTTT_withBDT_2016MC_merged_tree.root']*len(discvars) 
    # bkgfiles = ['/eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/updatedrootfiles/QCD_withBDT_2016MC_merged_tree.root']*len(discvars)
