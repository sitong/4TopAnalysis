void RT1BT0htbin5_2017_datacard7()
{
//=========Macro generated from canvas: c/c
//=========  (Wed Aug 24 23:03:20 2022) by ROOT version 6.12/07
   TCanvas *c = new TCanvas("c", "c",0,0,1000,1000);
   gStyle->SetOptStat(0);
   c->SetHighLightColor(2);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "p1",0,0.3,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-1.125,-22.97165,10.125,206.7448);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("");
   stack->SetMaximum(175.0221);
   
   TH1F *stack_stack_18 = new TH1F("stack_stack_18","",9,0,9);
   stack_stack_18->SetMinimum(0);
   stack_stack_18->SetMaximum(183.7732);
   stack_stack_18->SetDirectory(0);
   stack_stack_18->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_18->SetLineColor(ci);
   stack_stack_18->GetXaxis()->SetTitle("BDT discriminant");
   stack_stack_18->GetXaxis()->SetLabelFont(42);
   stack_stack_18->GetXaxis()->SetLabelSize(0.035);
   stack_stack_18->GetXaxis()->SetTitleSize(0.035);
   stack_stack_18->GetXaxis()->SetTitleFont(42);
   stack_stack_18->GetYaxis()->SetTitle("NEntries");
   stack_stack_18->GetYaxis()->SetLabelFont(43);
   stack_stack_18->GetYaxis()->SetLabelSize(20);
   stack_stack_18->GetYaxis()->SetTitleSize(20);
   stack_stack_18->GetYaxis()->SetTitleOffset(1.55);
   stack_stack_18->GetYaxis()->SetTitleFont(43);
   stack_stack_18->GetZaxis()->SetLabelFont(42);
   stack_stack_18->GetZaxis()->SetLabelSize(0.035);
   stack_stack_18->GetZaxis()->SetTitleSize(0.035);
   stack_stack_18->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_18);
   
   
   TH1F *other_stack_1 = new TH1F("other_stack_1","other in RT1BT0htbin5_2017_datacard7",9,0,9);
   other_stack_1->SetBinContent(1,2.978112);
   other_stack_1->SetBinContent(2,2.956162);
   other_stack_1->SetBinContent(3,0.7878236);
   other_stack_1->SetBinContent(4,2.113573);
   other_stack_1->SetBinContent(5,1.58418);
   other_stack_1->SetBinContent(6,2.490827);
   other_stack_1->SetBinContent(7,2.590097);
   other_stack_1->SetBinContent(8,1.257186);
   other_stack_1->SetBinContent(9,1.113973);
   other_stack_1->SetBinError(1,1.614965);
   other_stack_1->SetBinError(2,2.073562);
   other_stack_1->SetBinError(3,0.4320711);
   other_stack_1->SetBinError(4,1.625011);
   other_stack_1->SetBinError(5,1.685912);
   other_stack_1->SetBinError(6,1.612272);
   other_stack_1->SetBinError(7,2.265333);
   other_stack_1->SetBinError(8,0.9540945);
   other_stack_1->SetBinError(9,1.405348);
   other_stack_1->SetEntries(9);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetLineColor(ci);
   other_stack_1->GetXaxis()->SetTitle("CMS_th1x");
   other_stack_1->GetXaxis()->SetLabelFont(42);
   other_stack_1->GetXaxis()->SetLabelSize(0.035);
   other_stack_1->GetXaxis()->SetTitleSize(0.035);
   other_stack_1->GetXaxis()->SetTitleFont(42);
   other_stack_1->GetYaxis()->SetTitle("Events / ( 1 )");
   other_stack_1->GetYaxis()->SetLabelFont(42);
   other_stack_1->GetYaxis()->SetLabelSize(0.035);
   other_stack_1->GetYaxis()->SetTitleSize(0.035);
   other_stack_1->GetYaxis()->SetTitleOffset(0);
   other_stack_1->GetYaxis()->SetTitleFont(42);
   other_stack_1->GetZaxis()->SetLabelFont(42);
   other_stack_1->GetZaxis()->SetLabelSize(0.035);
   other_stack_1->GetZaxis()->SetTitleSize(0.035);
   other_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(other_stack_1,"");
   
   TH1F *TTTT_stack_2 = new TH1F("TTTT_stack_2","TTTT in RT1BT0htbin5_2017_datacard7",9,0,9);
   TTTT_stack_2->SetBinContent(1,0.09676602);
   TTTT_stack_2->SetBinContent(2,0.1869866);
   TTTT_stack_2->SetBinContent(3,0.3203081);
   TTTT_stack_2->SetBinContent(4,0.4282729);
   TTTT_stack_2->SetBinContent(5,0.4333273);
   TTTT_stack_2->SetBinContent(6,0.657733);
   TTTT_stack_2->SetBinContent(7,0.9724784);
   TTTT_stack_2->SetBinContent(8,1.931985);
   TTTT_stack_2->SetBinContent(9,3.625732);
   TTTT_stack_2->SetBinError(1,0.05376776);
   TTTT_stack_2->SetBinError(2,0.07701114);
   TTTT_stack_2->SetBinError(3,0.138452);
   TTTT_stack_2->SetBinError(4,0.1804691);
   TTTT_stack_2->SetBinError(5,0.1822008);
   TTTT_stack_2->SetBinError(6,0.2717031);
   TTTT_stack_2->SetBinError(7,0.3882154);
   TTTT_stack_2->SetBinError(8,0.7666512);
   TTTT_stack_2->SetBinError(9,1.437438);
   TTTT_stack_2->SetEntries(9);

   ci = TColor::GetColor("#a1794a");
   TTTT_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#a1794a");
   TTTT_stack_2->SetLineColor(ci);
   TTTT_stack_2->GetXaxis()->SetTitle("CMS_th1x");
   TTTT_stack_2->GetXaxis()->SetLabelFont(42);
   TTTT_stack_2->GetXaxis()->SetLabelSize(0.035);
   TTTT_stack_2->GetXaxis()->SetTitleSize(0.035);
   TTTT_stack_2->GetXaxis()->SetTitleFont(42);
   TTTT_stack_2->GetYaxis()->SetTitle("Events / ( 1 )");
   TTTT_stack_2->GetYaxis()->SetLabelFont(42);
   TTTT_stack_2->GetYaxis()->SetLabelSize(0.035);
   TTTT_stack_2->GetYaxis()->SetTitleSize(0.035);
   TTTT_stack_2->GetYaxis()->SetTitleOffset(0);
   TTTT_stack_2->GetYaxis()->SetTitleFont(42);
   TTTT_stack_2->GetZaxis()->SetLabelFont(42);
   TTTT_stack_2->GetZaxis()->SetLabelSize(0.035);
   TTTT_stack_2->GetZaxis()->SetTitleSize(0.035);
   TTTT_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(TTTT_stack_2,"");
   
   TH1F *TTX_stack_3 = new TH1F("TTX_stack_3","TTX in RT1BT0htbin5_2017_datacard7",9,0,9);
   TTX_stack_3->SetBinContent(1,1.813021);
   TTX_stack_3->SetBinContent(2,1.767453);
   TTX_stack_3->SetBinContent(3,1.238503);
   TTX_stack_3->SetBinContent(4,1.191552);
   TTX_stack_3->SetBinContent(5,0.6985008);
   TTX_stack_3->SetBinContent(6,1.528759);
   TTX_stack_3->SetBinContent(7,1.113055);
   TTX_stack_3->SetBinContent(8,2.801944);
   TTX_stack_3->SetBinContent(9,1.505114);
   TTX_stack_3->SetBinError(1,1.049389);
   TTX_stack_3->SetBinError(2,0.8925505);
   TTX_stack_3->SetBinError(3,0.7543806);
   TTX_stack_3->SetBinError(4,0.7791519);
   TTX_stack_3->SetBinError(5,0.4149536);
   TTX_stack_3->SetBinError(6,0.7377997);
   TTX_stack_3->SetBinError(7,0.7562917);
   TTX_stack_3->SetBinError(8,1.447035);
   TTX_stack_3->SetBinError(9,0.6302342);
   TTX_stack_3->SetEntries(9);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_3->SetLineColor(ci);
   TTX_stack_3->GetXaxis()->SetTitle("CMS_th1x");
   TTX_stack_3->GetXaxis()->SetLabelFont(42);
   TTX_stack_3->GetXaxis()->SetLabelSize(0.035);
   TTX_stack_3->GetXaxis()->SetTitleSize(0.035);
   TTX_stack_3->GetXaxis()->SetTitleFont(42);
   TTX_stack_3->GetYaxis()->SetTitle("Events / ( 1 )");
   TTX_stack_3->GetYaxis()->SetLabelFont(42);
   TTX_stack_3->GetYaxis()->SetLabelSize(0.035);
   TTX_stack_3->GetYaxis()->SetTitleSize(0.035);
   TTX_stack_3->GetYaxis()->SetTitleOffset(0);
   TTX_stack_3->GetYaxis()->SetTitleFont(42);
   TTX_stack_3->GetZaxis()->SetLabelFont(42);
   TTX_stack_3->GetZaxis()->SetLabelSize(0.035);
   TTX_stack_3->GetZaxis()->SetTitleSize(0.035);
   TTX_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(TTX_stack_3,"");
   
   TH1F *DDBKG_stack_4 = new TH1F("DDBKG_stack_4","DDBKG in RT1BT0htbin5_2017_datacard7",9,0,9);
   DDBKG_stack_4->SetBinContent(1,99.2919);
   DDBKG_stack_4->SetBinContent(2,76.63794);
   DDBKG_stack_4->SetBinContent(3,76.78716);
   DDBKG_stack_4->SetBinContent(4,67.83224);
   DDBKG_stack_4->SetBinContent(5,66.71557);
   DDBKG_stack_4->SetBinContent(6,67.84956);
   DDBKG_stack_4->SetBinContent(7,66.88936);
   DDBKG_stack_4->SetBinContent(8,70.84606);
   DDBKG_stack_4->SetBinContent(9,53.8842);
   DDBKG_stack_4->SetBinError(1,4.501022);
   DDBKG_stack_4->SetBinError(2,3.597214);
   DDBKG_stack_4->SetBinError(3,4.11379);
   DDBKG_stack_4->SetBinError(4,3.529452);
   DDBKG_stack_4->SetBinError(5,3.200406);
   DDBKG_stack_4->SetBinError(6,3.567216);
   DDBKG_stack_4->SetBinError(7,3.354802);
   DDBKG_stack_4->SetBinError(8,3.573705);
   DDBKG_stack_4->SetBinError(9,3.255367);
   DDBKG_stack_4->SetEntries(9);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_4->SetFillColor(ci);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_4->SetLineColor(ci);
   DDBKG_stack_4->GetXaxis()->SetTitle("CMS_th1x");
   DDBKG_stack_4->GetXaxis()->SetLabelFont(42);
   DDBKG_stack_4->GetXaxis()->SetLabelSize(0.035);
   DDBKG_stack_4->GetXaxis()->SetTitleSize(0.035);
   DDBKG_stack_4->GetXaxis()->SetTitleFont(42);
   DDBKG_stack_4->GetYaxis()->SetTitle("Events / ( 1 )");
   DDBKG_stack_4->GetYaxis()->SetLabelFont(42);
   DDBKG_stack_4->GetYaxis()->SetLabelSize(0.035);
   DDBKG_stack_4->GetYaxis()->SetTitleSize(0.035);
   DDBKG_stack_4->GetYaxis()->SetTitleOffset(0);
   DDBKG_stack_4->GetYaxis()->SetTitleFont(42);
   DDBKG_stack_4->GetZaxis()->SetLabelFont(42);
   DDBKG_stack_4->GetZaxis()->SetLabelSize(0.035);
   DDBKG_stack_4->GetZaxis()->SetTitleSize(0.035);
   DDBKG_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(DDBKG_stack_4,"");
   stack->Draw("hist");
   
   TH1F *datahist__69 = new TH1F("datahist__69","datahist",9,0,9);
   datahist__69->SetBinContent(1,109);
   datahist__69->SetBinContent(2,86);
   datahist__69->SetBinContent(3,79);
   datahist__69->SetBinContent(4,70);
   datahist__69->SetBinContent(5,64);
   datahist__69->SetBinContent(6,69);
   datahist__69->SetBinContent(7,75);
   datahist__69->SetBinContent(8,86);
   datahist__69->SetBinContent(9,63);
   datahist__69->SetBinError(1,10.9608);
   datahist__69->SetBinError(2,9.796759);
   datahist__69->SetBinError(3,9.412366);
   datahist__69->SetBinError(4,8.892323);
   datahist__69->SetBinError(5,8.526937);
   datahist__69->SetBinError(6,8.832538);
   datahist__69->SetBinError(7,9.18508);
   datahist__69->SetBinError(8,9.796759);
   datahist__69->SetBinError(9,8.46441);
   datahist__69->SetEntries(9);
   datahist__69->SetStats(0);

   ci = TColor::GetColor("#666666");
   datahist__69->SetLineColor(ci);
   datahist__69->SetLineWidth(2);
   datahist__69->SetMarkerStyle(20);
   datahist__69->GetXaxis()->SetLabelFont(42);
   datahist__69->GetXaxis()->SetLabelSize(0.035);
   datahist__69->GetXaxis()->SetTitleSize(0.035);
   datahist__69->GetXaxis()->SetTitleFont(42);
   datahist__69->GetYaxis()->SetLabelFont(42);
   datahist__69->GetYaxis()->SetLabelSize(0.035);
   datahist__69->GetYaxis()->SetTitleSize(0.035);
   datahist__69->GetYaxis()->SetTitleOffset(0);
   datahist__69->GetYaxis()->SetTitleFont(42);
   datahist__69->GetZaxis()->SetLabelFont(42);
   datahist__69->GetZaxis()->SetLabelSize(0.035);
   datahist__69->GetZaxis()->SetTitleSize(0.035);
   datahist__69->GetZaxis()->SetTitleFont(42);
   datahist__69->Draw("psame");
   
   TH1F *stacktot__70 = new TH1F("stacktot__70","stacktot",9,0,9);
   stacktot__70->SetBinContent(1,104.1798);
   stacktot__70->SetBinContent(2,81.54854);
   stacktot__70->SetBinContent(3,79.13379);
   stacktot__70->SetBinContent(4,71.56564);
   stacktot__70->SetBinContent(5,69.43158);
   stacktot__70->SetBinContent(6,72.52689);
   stacktot__70->SetBinContent(7,71.56499);
   stacktot__70->SetBinContent(8,76.83717);
   stacktot__70->SetBinContent(9,60.12902);
   stacktot__70->SetBinError(1,4.896062);
   stacktot__70->SetBinError(2,4.247609);
   stacktot__70->SetBinError(3,4.206924);
   stacktot__70->SetBinError(4,3.967032);
   stacktot__70->SetBinError(5,3.645584);
   stacktot__70->SetBinError(6,3.992821);
   stacktot__70->SetBinError(7,4.136317);
   stacktot__70->SetBinError(8,4.045161);
   stacktot__70->SetBinError(9,3.877608);
   stacktot__70->SetEntries(36);
   stacktot__70->SetStats(0);

   ci = TColor::GetColor("#666666");
   stacktot__70->SetFillColor(ci);
   stacktot__70->SetFillStyle(3002);

   ci = TColor::GetColor("#000099");
   stacktot__70->SetLineColor(ci);
   stacktot__70->SetLineWidth(0);
   stacktot__70->GetXaxis()->SetLabelFont(42);
   stacktot__70->GetXaxis()->SetLabelSize(0.035);
   stacktot__70->GetXaxis()->SetTitleSize(0.035);
   stacktot__70->GetXaxis()->SetTitleFont(42);
   stacktot__70->GetYaxis()->SetLabelFont(42);
   stacktot__70->GetYaxis()->SetLabelSize(0.035);
   stacktot__70->GetYaxis()->SetTitleSize(0.035);
   stacktot__70->GetYaxis()->SetTitleOffset(0);
   stacktot__70->GetYaxis()->SetTitleFont(42);
   stacktot__70->GetZaxis()->SetLabelFont(42);
   stacktot__70->GetZaxis()->SetLabelSize(0.035);
   stacktot__70->GetZaxis()->SetTitleSize(0.035);
   stacktot__70->GetZaxis()->SetTitleFont(42);
   stacktot__70->Draw("e2same");
   
   TLegend *leg = new TLegend(0.1,0.6,0.3,0.9,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("other_stack_1","other","F");

   ci = TColor::GetColor("#d07e93");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#d07e93");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTTT_stack_2","TTTT","F");

   ci = TColor::GetColor("#a1794a");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#a1794a");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTX_stack_3","TTX","F");

   ci = TColor::GetColor("#163d4e");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#163d4e");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("datahist","data","L");

   ci = TColor::GetColor("#666666");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("DDBKG_stack_4","DDBKG","F");

   ci = TColor::GetColor("#c1caf3");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#c1caf3");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "p2",0,0.05,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-1.125,0.05714285,10.125,1.771429);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetGridy();
   p2->SetBottomMargin(0.2);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   
   TH1F *raterr__71 = new TH1F("raterr__71","",9,0,9);
   raterr__71->SetBinContent(0,1);
   raterr__71->SetBinContent(1,1);
   raterr__71->SetBinContent(2,1);
   raterr__71->SetBinContent(3,1);
   raterr__71->SetBinContent(4,1);
   raterr__71->SetBinContent(5,1);
   raterr__71->SetBinContent(6,1);
   raterr__71->SetBinContent(7,1);
   raterr__71->SetBinContent(8,1);
   raterr__71->SetBinContent(9,1);
   raterr__71->SetBinContent(10,1);
   raterr__71->SetBinError(1,0.04699627);
   raterr__71->SetBinError(2,0.05208688);
   raterr__71->SetBinError(3,0.05316217);
   raterr__71->SetBinError(4,0.05543208);
   raterr__71->SetBinError(5,0.05250613);
   raterr__71->SetBinError(6,0.05505298);
   raterr__71->SetBinError(7,0.05779805);
   raterr__71->SetBinError(8,0.05264589);
   raterr__71->SetBinError(9,0.06448812);
   raterr__71->SetMinimum(0.4);
   raterr__71->SetMaximum(1.6);
   raterr__71->SetEntries(20);
   raterr__71->SetStats(0);

   ci = TColor::GetColor("#666666");
   raterr__71->SetFillColor(ci);
   raterr__71->SetFillStyle(3002);

   ci = TColor::GetColor("#666666");
   raterr__71->SetLineColor(ci);
   raterr__71->SetLineWidth(0);
   raterr__71->GetXaxis()->SetTitle("BDT discriminant");
   raterr__71->GetXaxis()->SetLabelFont(43);
   raterr__71->GetXaxis()->SetLabelSize(20);
   raterr__71->GetXaxis()->SetTitleSize(20);
   raterr__71->GetXaxis()->SetTitleOffset(4);
   raterr__71->GetXaxis()->SetTitleFont(43);
   raterr__71->GetYaxis()->SetTitle("ratio data/prediction");
   raterr__71->GetYaxis()->SetNdivisions(505);
   raterr__71->GetYaxis()->SetLabelFont(43);
   raterr__71->GetYaxis()->SetLabelSize(20);
   raterr__71->GetYaxis()->SetTitleSize(20);
   raterr__71->GetYaxis()->SetTitleOffset(1.55);
   raterr__71->GetYaxis()->SetTitleFont(43);
   raterr__71->GetZaxis()->SetLabelFont(42);
   raterr__71->GetZaxis()->SetLabelSize(0.035);
   raterr__71->GetZaxis()->SetTitleSize(0.035);
   raterr__71->GetZaxis()->SetTitleFont(42);
   raterr__71->Draw("e2");
   
   TH1F *ratio__72 = new TH1F("ratio__72","datahist",9,0,9);
   ratio__72->SetBinContent(1,1.046268);
   ratio__72->SetBinContent(2,1.054587);
   ratio__72->SetBinContent(3,0.9983093);
   ratio__72->SetBinContent(4,0.9781231);
   ratio__72->SetBinContent(5,0.9217708);
   ratio__72->SetBinContent(6,0.9513713);
   ratio__72->SetBinContent(7,1.047999);
   ratio__72->SetBinContent(8,1.11925);
   ratio__72->SetBinContent(9,1.047747);
   ratio__72->SetBinError(1,0.1005578);
   ratio__72->SetBinError(2,0.1139158);
   ratio__72->SetBinError(3,0.1191439);
   ratio__72->SetBinError(4,0.1270332);
   ratio__72->SetBinError(5,0.1332334);
   ratio__72->SetBinError(6,0.1280078);
   ratio__72->SetBinError(7,0.1224677);
   ratio__72->SetBinError(8,0.1139158);
   ratio__72->SetBinError(9,0.1343557);
   ratio__72->SetEntries(507.4903);
   ratio__72->SetStats(0);
   ratio__72->SetLineWidth(2);
   ratio__72->SetMarkerStyle(20);
   ratio__72->GetXaxis()->SetLabelFont(42);
   ratio__72->GetXaxis()->SetLabelSize(0.035);
   ratio__72->GetXaxis()->SetTitleSize(0.035);
   ratio__72->GetXaxis()->SetTitleFont(42);
   ratio__72->GetYaxis()->SetLabelFont(42);
   ratio__72->GetYaxis()->SetLabelSize(0.035);
   ratio__72->GetYaxis()->SetTitleSize(0.035);
   ratio__72->GetYaxis()->SetTitleOffset(0);
   ratio__72->GetYaxis()->SetTitleFont(42);
   ratio__72->GetZaxis()->SetLabelFont(42);
   ratio__72->GetZaxis()->SetLabelSize(0.035);
   ratio__72->GetZaxis()->SetTitleSize(0.035);
   ratio__72->GetZaxis()->SetTitleFont(42);
   ratio__72->Draw("pe0same");
   TLine *line = new TLine(0,1,1,1);
   line->Draw();
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
