#calculates genjet5 weight given trees
import ROOT as r
import numpy as np

def get_sum(tree, wgtvar):
    htmpH = r.TH1D('htmpH', 'htmpH', 1, 0, 10)
    htmpL = r.TH1D('htmpL', 'htmpL', 1, 0, 10)
    tree.Project('htmpL', wgtvar, "gen_njets<5")
    low = htmpL.Integral()
    tree.Project('htmpH', wgtvar, "gen_njets>=5")
    high = htmpH.Integral()
    print 'sumhigh', high, 'sumlow', low, 'total', high+low
    return float(high), float(low)

def get_sum_gj5(gj5tree, wgtvar):
    htmp = r.TH1D('htmp', 'htmp', 1, 0, 10000)
    gj5tree.Project('htmp', wgtvar)
    sumwgt = htmp.Integral()
    print 'sumgj5', sumwgt
    return float(sumwgt)

def calcQCDweight(sample, xsec, gj5xsec):
    indir = '/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/run/output/forweights/parts/'
    suffix = '_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_tree.root'
    lumi=1000.0
    treename = 'Events'
    qcdfile = indir+sample+suffix
    qcdfile_gj5 = indir+sample+'_GenJets5'+suffix

    normfile = r.TFile(qcdfile)
    gj5file  = r.TFile(qcdfile_gj5)
    tree     = normfile.Get(treename)
    tree_gj5 = gj5file.Get(treename)

    sumhigh, sumlow = get_sum(tree, 'ismc')
    sumgj5 = get_sum_gj5(tree_gj5, 'ismc')

    highweight = (gj5xsec * (lumi / sumgj5)) + (xsec * (lumi / sumhigh))
    lowweight = xsec * (lumi / sumlow)

    print "sample", sample, "xsec", xsec, "njets>=5 weight", highweight, "njets<5 weight", lowweight

calcQCDweight('QCD_HT300to500',   347700, 70450)
calcQCDweight('QCD_HT500to700',   32100,  11300)
calcQCDweight('QCD_HT700to1000',  6831,   3030)
calcQCDweight('QCD_HT1000to1500', 1207,   623.4)
calcQCDweight('QCD_HT1500to2000', 199.9,  67.43)
calcQCDweight('QCD_HT2000toInf',   25.24,  14.47)
