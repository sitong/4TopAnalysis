#lets see if sequential hadding works
import subprocess
import os
import argparse
import sys
import math
import numpy as np

parser = argparse.ArgumentParser(description='mass merging!') 
# parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default='./testfiles/bla.root') #input files
parser.add_argument("-f", "--filenames", dest="filenames", default='./testfiles/bla.root') #input files
parser.add_argument("-s", "--samp", dest="samp", default='SAMP') #sample name
# parser.add_argument("-n", "--nleps", dest="nleps", default='1lep') #nleps
parser.add_argument("-o", "--outdir", dest="outdir", default='./output/merged/') #where you initially merge to
parser.add_argument("-c", "--copydir", dest="copydir", default='/eos/uscms/store/user/mquinnan/NanoAODv5/restop_training/forWP') #where you copy to
# parser.add_argument("-c", "--copydir", dest="copydir", default='/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv5_trees/2016/MC/0lep') #where you copy to
parser.add_argument("-d", "--delfile", dest="delfile", default=True)
args = parser.parse_args()

# indir = '/eos/uscms/store/user/mquinnan/NanoAODv5/2016data/fullset/'
# samp = 'TT_TuneCUETP8M2T4_13TeV-powheg-pythia8' 
# samp = 'JETHT'
# note = '_1lep2016'
# outdir = './output/merged/'
samp=args.samp
filenames = args.filenames+'*.root'
# tag='MC'
# if 'JETHT' in samp or 'SINGLEEL' in samp or 'SINGLEMU' in samp:
#     tag='DATA'
# nleps='1lep'
# if '0lep' in filenames:
#     nleps='0lep'
# copydir = args.copydir+tag+'/'+nleps+'/'
# haddname = args.samp+'_2016_merged_'+nleps+'_tree.root'

# tag='0lep'
tag='nano'
copydir = args.copydir+'/'#+tag+'/'
haddname = args.samp+'_2016_merged_'+tag+'_tree.root'
cmd1  = 'hadd -f '+args.outdir+haddname+' '+filenames
cmd2 = 'xrdcp '+args.outdir+haddname+' '+copydir
cmd3 = 'rm '+args.outdir+haddname 
print 'will copy to', copydir, '?', args.delfile
print 'merging', samp, 'files to', haddname  
print 'filnames', filenames
print cmd1
print cmd2
print cmd3
subprocess.call(cmd1, shell=True)
if args.delfile:
    subprocess.call(cmd2, shell=True)
    subprocess.call(cmd3, shell=True)

# for i,f in enumerate(args.filenames):
#     filename = f[f.rfind('/')+1 : f.rfind('Tune')-1]
#     print filename
#     for file in files:
#         if samp in file:
#             print 'file #', i
#             if first:
#                 print 'scp '+indir+file+' '+temp
#                 subprocess.call('scp '+indir+file+' '+temp, shell=True)
#                 first=False
#             else:
#                 print cmd+temp+' '+indir+file
#                 print 'mv '+haddname+' '+temp
#                 subprocess.call(cmd+temp+' '+indir+file, shell=True)
#                 subprocess.call('mv '+haddname+' '+temp, shell=True)
#     subprocess.call('mv '+temp+' '+haddname, shell=True)                
#     # print 'rm '+temp
#     # subprocess.call('rm '+temp, shell=True)
#     break
