import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
import ROOT
import numpy as np
import logging


#a helper for jet cleaning and such
class JetHelper:
    #'deepflav', 0.3039
    def __init__(self, btype, bWP):
        self.btype   = btype
        self.bWP     = bWP
        pass

    #applies jet preselections, returns True if passes
    def cleanJets(self, jet, minJet_pt=20.0, maxJet_eta=2.4, usejetid=False, year='2016'):
        if (jet.pt<minJet_pt or abs(jet.eta)>=maxJet_eta):
            return False
        if usejetid:
            if (year=='2016' and jet.jetId<1): # loose
                return False
            elif (year=='2017' and jet.jetId<2): # tight
                return False
            elif (year=='2018' and jet.jetId<2): # tight
                return False
        return True
    
    #returns True if jet is a clean bjet
    def cleanBJets(self, jet, minBJet_pt = 20.0, maxBJet_eta=2.4, usejetid=False, year='2016'):
        # print 'bjet cut: (jet.pt>=',minBJet_pt,' and abs(jet.eta)<',maxBJet_eta,') btype', self.btype,'jet.btagDeepFlavB >= ',self.bWP
        if self.btype=='csvv2':
            if jet.btagCSVV2 <= self.bWP:
                return False
        elif self.btype=='deepflav':
            if jet.btagDeepFlavB <= self.bWP:
                return False
        elif self.btype=='deepcsv':
            if jet.btagDeepB <= self.bWP:
                return False
        if (jet.pt<minBJet_pt or abs(jet.eta)>=maxBJet_eta):
            return False
        if usejetid:
            if (year=='2016' and jet.jetId<1): # loose
                return False
            elif (year=='2017' and jet.jetId<2): # tight
                return False
            elif (year=='2018' and jet.jetId<2): # tight
                return False
        return True

    #returns True if jet is a clean bjet
    def customBJets(self, jet, btype, bWP, minBJet_pt = 20.0, maxBJet_eta=2.4):
        if self.btype=='csvv2':
            if jet.btagCSVV2 < self.bWP:
                return False
        elif self.btype=='deepflav':
            if jet.btagDeepFlavB < self.bWP:
                return False
        elif self.btype=='deepcsv':
            if jet.btagDeepB < self.bWP:
                return False
        if (jet.pt<minBJet_pt or abs(jet.eta)>=maxBJet_eta):
            return False
        return True


    ##Clean jets vs lepton objects
    def cleanJetsvLeps(self, Jets, Leps, jetrad=0.4):
        JetCol=Jets #initially all jets
        for lep in Leps:
            newJets=phys.ExcludeCloseNeighbors(lep, JetCol, minDR=jetrad)
            JetCol=newJets#replace Jet collection with cleaned collection afer each lepton
        return JetCol 


    # function for removing too-close jets from one collection from another collection
    # can maybe speed up by vectorizing constjets or the deltaRs
    def CleanCollection(self, modjets, constjets, overlap, vecbool=2):
        # vecbool is false if restops change/fillboostfirst, and true if restops don't change/fillresfirst
        #print '..................CleanCollection..................' 
        changedjets = []
        for mj in modjets:
            isOverlap = False
            for cj in constjets:
                # have to set to right kind of vectors according to resthenboost  #<class 'ROOT.TLorentzVector'>
                # if self.verbose: print 'vecbool:', vecbool 
                if vecbool==1: #constjets var is rescands 
                    mjeta = mj.eta;      mjphi = mj.phi
                    cjeta = cj[0].Eta(); cjphi = cj[0].Phi()
                elif vecbool==0: #modjets var is rescands          
                    mjeta = mj[0].Eta(); mjphi = mj[0].Phi()
                    cjeta = cj.eta;      cjphi = cj.phi
                elif vecbool==2: #no change
                    mjeta = mj.eta;      mjphi = mj.phi
                    cjeta = cj.eta;      cjphi = cj.phi
                # if self.verbose: print 'cj', cj, 'mj', mj
                DelR = phys.deltaR(mjeta, mjphi, cjeta, cjphi)
                if DelR < overlap:
                    isOverlap=True
                    #print 'REJECTED CANDIDATE'
                    break #throw out that mj
            if not isOverlap:
                #print '>>> no overlap <<<'
                changedjets.append(mj)
        return np.array(changedjets)
