# easier plotting
# import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
#creates stacked histograms with ratio plot below
#references controlhists.py and uses plottery

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])

channel = '0lep' #0lep, 1lep, 1el, 1mu 
cr = '0lbase'

channame = channel
if channel=='1el' or channel=='1mu':
    channame='1lep'
varstoplot = ['ht', 'bjht', 'met', 'sfjm', 'njets', 'nbjets', 'nbstops', 'nrestops', 'jetpt', 'jeteta', 'bjetpt', 'bjeteta', 'nbsws']
lepvars = ['leptonpt', 'leptoneta', 'lep1pt']  
if channel != '0lep':
    varstoplot.extend(lepvars)

getyields = True
histdir = '/eos/uscms/store/user/mequinna/NanoAODv5/2016mc/fullset/' #directory of background files
datadir = '/eos/uscms/store/user/mequinna/NanoAODv5/2016data/fullset/' #directory of data files
samplist = ["QCD", "ttbar", "ttX", "singletop", "dibosons", "W+Jets","DY"]

lumi = str(rs.lumis[channel])

datadir= '/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv5_trees/2016/DATA/'+channame+'/pieces/'
mcdir= '/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv5_trees/2016/MC/'+channame+'/pieces/'

datafiles = {'1lep' : None, #['SINGLEEL_', 'SINGLEMU_'],
             '1el'  : ['SINGLEEL_'],
             '1mu'  : ['SINGLEMU_'],
             '0lep' : ['JETHT_'] } #directory of data files

mcfiles   = {'QCD'       : ['QCD_'],
             'ttbar'     : ['TT_TuneCUETP8M2T4_13TeV-powheg-pythia8_'],
             'ttX'       : ['ttHTo', 'TTWJetsTo', 'TTZTo', 'tZq_', 'ttGJets'],
             'W+Jets'    : ['WJetsTo'],
             'singletop' : ['ST_'],
             'znunu'     : ['ZJetsToNuNu_'],
             'dibosons'  : ['ZZTo', 'WWTo', 'WZTo'],
             'DY'        : ['DYJets']}

def getMChists(projvar, cr, mcfiles, samplist, treename='Events', lumi=lumi):
    cut = sels.getcut(cr, format='mc')     #get correct cutstring
    nbins, selrange = sels.getHistSettings(projvar)
    hists = []
    for i,f in enumerate(samplist):
        print i,f, mcfiles[f]
        # for fd in mcfiles[f]:
        #     print 'filedir',fd
        print cut
        hist = rs.getMultiFileHist(mcdir, mcfiles[f], treename, projvar, cut, nbins, selrange)            
        hist.SetDirectory(0)
        hists.append(hist)
    return hists

def getDatahists(projvar, cr, channel, treename='Events'):
    nbins, selrange = sels.getHistSettings(projvar)
    # if channel != '1lep':
    print datafiles[channel], channel
    # rf, t = rs.makeroot(f, treename)
    if channel == '0lep':
        cut = sels.getcut(cr, format='data', chan='0lep')    #get correct cutstring
    elif channel=='1el':
        cut = sels.getcut(cr, format='data1e', chan='1lep')
    elif channel=='1mu':
        cut = sels.getcut(cr, format='data1mu', chan='1lep')
    elif channel=='1lep': #strange case: different cuts. use rootnumpy
        fe = datafiles['1el']
        fm = datafiles['1mu']
        rfe, te = rs.makeroot(fe[0], treename)
        rfm, tm = rs.makeroot(fm[0], treename)
        cut_e = sels.getcut(cr, format='data1e',  chan='1lep')
        cut_m = sels.getcut(cr, format='data1mu', chan='1lep')
        histe = rs.gethist(te, projvar, cut_e, nbins, selrange)
        histm = rs.gethist(tm, projvar, cut_m, nbins, selrange)
        #combine hists with rootnumpy
        he = hist2array(histe, include_overflow=True)
        hm = hist2array(histm, include_overflow=True)
        data_arr = np.concatenate((he, hm), axis=0) 
        datahist = r.TH1D('datahist', 'datahist', nbins, selrange[0], selrange[1])
        rn.fill_hist(datahist, data_arr)
        rs.fixref(rf, datahist)
        return datahist

    #if not 1lep:
    datahist = rs.getMultiFileHist(datadir, datafiles[channel], treename, projvar, cut, nbins, selrange)            
    datahist.SetDirectory(0)
    return datahist

def makeControlHist(crname, plotvar, data, hists, labels, lumi=36):
    allcolors = [r.kRed-2,r.kOrange+2,r.kYellow,r.kGreen,r.kBlue,r.kViolet, r.kSpring, r.kBlack, r.kGray, r.kTeal-1, r.kPink, r.kAzure-4, r.kCyan, r.kMagenta]
    colors = allcolors[:len(hists)]# create list of colors sized as you need
    outname = 'plots/controlhists/'+crname+'_'+plotvar+'.png'
    print type(data)
    print hists
    print colors
    print labels
    ply.plot_hist( 
        data = data,
        bgs  = hists, #[h1,h2,h3,h4,h5,h9,h10],
        # sigs = mydata,
        # syst = hsyst,
        # sig_labels = labels,#["el", "mu"],
        # colors = colors, 
        legend_labels = labels,
        options = {
            "do_stack": True,
            "legend_scalex": 0.7,
            "legend_scaley": 1.5,
            "extra_text": [plotvar],
            # "yaxis_log": True,
            "ratio_range":[0.5,1.5],
            # "ratio_pull": True,
            "hist_disable_xerrors": True,
            "ratio_chi2prob": False,
            "output_name": outname,
            "legend_percentageinbox": False,
            "cms_label": "Preliminary",
            "lumi_value": lumi,
            # "output_ic": True,
            # "us_flag": False,
            # "output_jsroot": True,
            # "output_diff_previous": True,
            }
       )

for v in varstoplot:
    print v, cr, channel
    mchists  = getMChists(v, cr, mcfiles, samplist)
    datahist = getDatahists(v, cr, channel)
    makeControlHist(cr, v, datahist, mchists, samplist, lumi=rs.lumis[channel])
