#a python script for properly generating datacards according to cut category
# https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part2/settinguptheanalysis/#binned-shape-analysis
# https://github.com/scikit-hep/uproot#writing-histograms
#modify cuts and baselines in datacard_dictionary.py
import os
import sys
import math
import numpy as np
import argparse
from ROOT import gROOT,TFile,TTree,TH1D
import ROOT as r
from datacard_dictionary import datacard_dict
gROOT.SetBatch(True)
# import uproot
# import types
# import uproot_methods.classes.TH1
import numpy as np
from array import array
from NAFdict import getNAFerrors

class histmaker:
# class that creates shape histograms for combine
    def __init__(self, processes, selections, year, outroot='./shapehists.root', uncval=2.0, uncname='shapesyst', treename='Events', data='none'):

        self.processes  = processes  #dictionary of process names and roottrees
        self.selections = selections #dictionary of cut+bin ids and selections
        self.uncval     = uncval     #value of shape uncertainty
        self.outroot    = outroot    #name of shape rootfile
        self.treename   = treename   #name of trees in process files
        self.systname   = uncname    #'shapesyst' name of shape systematic
        self.year       = year

        self.datafile = data
        self.blind = False
        if data=='none' or '.root' not in data:
            print 'blind run. will add empty data_obs histograms'
            self.blind = True

        self.uncbool  = True       #whether or not to output shape uncertainty histograms
        self.shapevar = 'catBDTdisc' #name of bdt dicriminant variable used in shape
        self.selrange = [0,1]      #range of bdt disc for histograms
        self.nbins    = 10         #number of bins for histograms
        
        #temporary until i fix the bdt
        TTTThists = {'2016': './TTTTMC_SR/TTTTMC_SR_2016.root',
                     '2017': './TTTTMC_SR/TTTTMC_SR_2017.root',
                     '2018': './TTTTMC_SR/TTTTMC_SR_2018.root'}
        # TTTThists = {'2016': './TTTTMC_SR/TTTTMC_SR_2016_combined.root',
        #              '2017': './TTTTMC_SR/TTTTMC_SR_2017_combined.root',
        #              '2018': './TTTTMC_SR/TTTTMC_SR_2018_combined.root'}
        self.TTTThist = TTTThists[year]

    def datahist(self, channel, outfilename):

        histname = channel+'_'+self.year+'_data_obs'
        datahist = r.TH1F(histname, histname, self.nbins, self.selrange[0], self.selrange[1])

        #actually fill the histogram if it isn't a blind analysis
        if not self.blind:
            binsel = self.selections[channel]
            pfile = r.TFile(self.datafile)
            ptree = pfile.Get(self.treename)
            ptree.Draw(self.shapevar+'>>'+histname, binsel)#apply bin selection
            print 'filled data hist ', histname, 'with selection', binsel, 'for process data_obs and bin', channel
        else:
            print 'created empty data_obs hist', histname

        #open rootfile for writing histograms
        histfile = r.TFile(outfilename, "UPDATE")
        datahist.Write()
        #close rootfile
        histfile.Close()
        del histfile

    #makes a histogram for each process and bin. channel is the binname
    def gethists(self, processname, channel, outfilename):

        print outfilename

        # get selection and histogram names, get file for that process (open file in uproot)
        binsel = self.selections[channel]
        histname = channel+'_'+self.year+'_'+processname
        
        # get, modify and return histogram:
        pfile = r.TFile(self.processes[processname])
        ptree = pfile.Get(self.treename)
        dischist = r.TH1F(histname, histname, self.nbins, self.selrange[0], self.selrange[1])
        ptree.Draw(self.shapevar+'>>'+histname, binsel)#apply bin selection
        print 'filled hist ', histname, 'with selection', binsel, 'for process', processname, 'and bin', channel

        if self.uncbool:
        #returns histogram for a process shifted up and down by a certain uncertainty parameter
        #this can be for example the uncertainty on background shape and normalization or signal resolution

            #get the histograms set up
            histname_up = histname+'_'+histname+'_'+self.systname+'Up'
            histname_down = histname+'_'+histname+'_'+self.systname+'Down'
            # histname_up = histname+'_'+histname+'_'+self.year+'_'+self.systname+'Up'
            # histname_down = histname+'_'+histname+'_'+self.year+'_'+self.systname+'Down'
            uphist   = r.TH1F(histname_up  , histname_up  , self.nbins, self.selrange[0], self.selrange[1])
            downhist = r.TH1F(histname_down, histname_down, self.nbins, self.selrange[0], self.selrange[1])
            
            syst = float(self.uncval)/100.0
            systup=1.0+syst
            systdown = 1.0-syst
            print 'systup', systup, 'systdown', systdown
            #shift the value of the bdt discriminant up and down, not the value of the selection
            ptree.Draw('('+self.shapevar+'*('+str(systup)+'))>>'+histname_up, binsel)#apply bin selection
            ptree.Draw('('+self.shapevar+'*('+str(systdown)+'))>>'+histname_down, binsel)#apply bin selection
            print 'filled syst hists:', histname_up, histname_down,'with shapesyst', syst

        #open rootfile for writing histograms
        histfile = r.TFile(outfilename, "UPDATE")
        dischist.Write()
        if self.uncbool:
            #write to files
            uphist.Write()
            downhist.Write()
        #close rootfile
        histfile.Close()
        del histfile
                                        
    def makehists(self):        
        #loop over channels and processes and get histograms
        for chan in self.selections.keys():
            self.datahist(chan, self.outroot) #data histogram
            for proc in self.processes.keys():
                self.gethists(proc, chan, self.outroot) #MC histograms

    def hybridhists(self, NAFfilename, procname='BKG', getRates=True):

        NAFfile = r.TFile(NAFfilename)
        errdict = getNAFerrors(NAFfilename)

        NAFRates = self.addfromNAF(NAFfile, self.selections.keys(), procname, self.outroot, errdict, getRates)
        TTTTfile = r.TFile(self.TTTThist)
        TTTTRates = self.addfromNAF(TTTTfile, self.selections.keys(), 'TTTT', self.outroot, errdict=None, getRate=True)

        for chan in self.selections.keys():
            # add stuff from NAFfile
            self.datahist(chan, self.outroot) #data histogram
            for proc in self.processes.keys():
                # #temporary until i fix the bdt
                #if proc=='TTTT':
                if proc != 'TTTT':
                    self.gethists(proc, chan, self.outroot) #MC histograms
        # return NAFRates
        return NAFRates, TTTTRates #temporary until bdt fix

    def addfromNAF(self, NAFfile, chans, procname, outfilename, errdict=None, getRate=True):
        #adds all needed histograms from NAF file

        histfile = r.TFile(outfilename, "UPDATE")
        NAFrates = {} 

        for chan in chans:

            histname = chan+'_'+self.year+'_'+procname
            histname_up = histname+'_'+histname+'_'+self.systname+'Up'
            histname_down = histname+'_'+histname+'_'+self.systname+'Down'

            print 'adding hist:', histname 
            NAFhist      = NAFfile.Get(histname)
            NAFhist_up   = NAFfile.Get(histname_up)
            NAFhist_down = NAFfile.Get(histname_down)
            
            if getRate:
                rate = 0.0; err=0.0
                # if errdict==None:
                errorVal = r.Double(0)
                #get Nbins
                minbin=0
                maxbin=12#(NAFhist.GetMaximumBin())+2
                # print 'minbin',minbin, 'maxbin', maxbin
                rate = NAFhist.IntegralAndError(minbin,maxbin,errorVal)
                if rate < 0:#treatment of negative yields 
                    print 'WARNING: NEGATIVE YIELD:', rate, processname, cutstr #will throw error
                    rate = 0.0
                err=errorVal
                if errdict!=None:
                    # rate = errdict[chan][0]
                    err = errdict[chan][1]
                rateunc = 1.0 
                if rate>0.0: #nonzero
                    rateunc += (err/rate)
                NAFrates[chan] = [round(rate,3), round(rateunc,3)]
                print chan, [round(rate,3), round(rateunc,3)]
                # print NAFrates.keys()
                
            NAFhist.Write()
            NAFhist_up.Write()
            NAFhist_down.Write()

        #close rootfile
        histfile.Close()
        del histfile
        if getRate:
            return NAFrates
        else:
            return None


    #return info for datacards
    # what you need in the datacard for shape histograms:
    # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideCMSDataAnalysisSchool2014HiggsCombPropertiesExercise#A_shape_analysis_using_templates
    #format: shapes * * rootfilename.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
        # shapeline = 'shapes * * '+self.outroot+' $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC'
        # rfname =  self.outroot[self.outroot.find('//'):] #dont include directory
        # print rfname
        # shapeline = 'shapes * * '+self.outroot+' $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC'
        # print shapeline
        # return shapeline
    
    # def getshapeline(self):
    #     shapeline = 'shapes * * '+rfname+' $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC'
    #     return shapeline
