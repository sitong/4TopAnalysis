# easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
#creates stacked histograms with ratio plot below
#references controlhists.py and uses plottery

def getclosurecuts(cr, form, channel, year, lumi, rtbool, extracut):

    cuts = {}
    if cr=='closure' or cr=='dataclosure': #D=BC/A
        B = sels.getcut('CR3', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)
        C = sels.getcut('CR2', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)
        A = sels.getcut('CR1', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)

        Bcr = B.replace('(',extracut)
        Ccr = C.replace('(',extracut)
        Acr = A.replace('(',extracut)
    
        cuts = {'B':Bcr, 'C':Ccr, 'A':Acr}
        print cuts

    elif cr=='extclosure' or cr=='dataextclosure': #D=(BC/A)^2(A'/BC')
        B = sels.getcut('CR3', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)
        C = sels.getcut('CR2', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)
        A = sels.getcut('CR1', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)
        Ap = sels.getcut('CR1p', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)
        Cp = sels.getcut('CR2p', format=form, chan=channel, year=year, lumi=lumi, rtcut=rtbool)

        Bcr = B.replace('(',extracut)
        Ccr = C.replace('(',extracut)
        Acr = A.replace('(',extracut)
        Cpcr = Cp.replace('(',extracut)
        Apcr = Ap.replace('(',extracut)
        cuts = {'B':Bcr, 'C':Ccr, 'A':Acr, 'Ap':Apcr, 'Cp':Cpcr}
        print cuts
    else: print 'ERROR: not a valid closure region'

    return cuts

#need something diff for data vs. mc
def getClosure(projvar, cr, channel, sampfile, treename='Events', lumi='35.9', year='2016', cutdict=None, setnbins=None, setselrange=None, returnhists=False):
    # print 'channel:', channel
    rtbool = False
    if (projvar=='restop1pt'): rtbool=True

    form = 'mc'
    if 'data' in cr:
        form = 'data'

    #if aren't given cut dict as input, get them yourself:
    if cutdict is None:
        print 'getting cuts...'
        cuts = getclosurecuts(cr, form, channel, year, lumi, rtbool, sels.seldict[cr])
    else:
        print 'given cut dictionary as input!'
        cuts = cutdict

    # if projvar=='restop1pt': #no underflow
    #     cut = sels.getcut(cr, format=form, chan=channel, year=year, lumi=lumi, rtcut=True) 

    nbins, selrange = sels.getHistSettings(projvar)
    if setnbins is not None:
        nbins = setnbins
    if setselrange is not None:
        selrange=setselrange
    Dhist = r.TH1F('Dhist', 'Dhist', nbins, selrange[0], selrange[1])
    # for i,f in enumerate(samplist): #loop through samplist
    print sampfile
    rf, t = rs.makeroot(sampfile, treename)
    closurehists = {}
    closurestats = {}
    closureroothists = {}
    for key, cut in cuts.iteritems(): #loop over cut dictionaries, get histograms
        statsarr=[]# for statistical uncertainties
        hist = rs.gethist(t, projvar, cut, nbins, selrange)
        histarr = rn.hist2array(hist, include_overflow=True)
        #get bin errors
        for ibin in range(0, hist.GetNbinsX()+2): 
           statsarr.append(hist.GetBinError(ibin)) 
        closurehists[key]=histarr
        closurestats[key]=statsarr#these are the statistical errors per bin in each CR hist
        if returnhists:
            closureroothists[key] = hist
        hist.SetDirectory(0)

    # divide bins. bin number should be the same
    if cr=='closure' or cr=='dataclosure': #D=BC/A
        print 'A', closurehists['A']
        print 'B', closurehists['B']
        print 'C', closurehists['C']
        D = np.divide(np.multiply(closurehists['B'], closurehists['C']), closurehists['A'])
        D[np.isnan(D)] = 0.0
        D[np.isinf(D)] = 0.0
        print D
        Dhist = rn.array2hist(D, Dhist)

    elif cr=='extclosure' or cr=='dataextclosure': #D=(BC/A)*(BC/A)*(A'/BC')
        D1 = np.divide(np.multiply(closurehists['B'], closurehists['C']), closurehists['A'])
        D2 = np.divide(closurehists['Ap'], np.multiply(closurehists['B'],closurehists['Cp']))
        D1sq = np.multiply(D1, D1)
        D = np.multiply(D1sq, D2)
        # D = np.multiply( np.exp2(np.divide(np.multiply(closurehists['B'],closurehists['C']), closurehists['A'])), np.divide(closurehists['Ap'], np.multiply(closurehists['B'],closurehists['Cp'])))
        D[np.isnan(D)] = 0.0
        D[np.isinf(D)] = 0.0
        print D

        #deal with statistical uncertainties
        print 'Dshape', D.shape, len(D)

        #staterrorprop by bin...
        #(dA/A)^2 = (dA)^2/A^2
        Asqerr = np.divide(np.square(closurestats['A']),np.square(closurehists['A'])) 
        Bsqerr = np.divide(np.square(closurestats['B']),np.square(closurehists['B']))
        Csqerr = np.divide(np.square(closurestats['C']),np.square(closurehists['C'])) 
        Apsqerr = np.divide(np.square(closurestats['Ap']),np.square(closurehists['Ap']))
        Cpsqerr = np.divide(np.square(closurestats['Cp']),np.square(closurehists['Cp']))
        #if X= BC/A and Y=Ap/BCp then dD = D* sqrt( 2(dX/X)^2 + (dY/Y)^2) )
        
        #(dX/X)^2 = Dstats1 = (dB/B)^2 + (dC/C)^2 + (dA/A)^2
        #(dY/Y)^2 = Dstats2 = (dAp/Ap)^2 + (dB/B)^2 + (dCp/Cp)^2
        Dstats1 = np.add(np.add(Bsqerr,Csqerr),Asqerr) #sqrt( (dB/B)^2 + (dC/C)^2 + (dA/A)^2)^2 
        Dstats2 = np.add(Apsqerr, np.add(Bsqerr,Cpsqerr)) #sqrt( (dAp/Ap)^2 + (dB/B)^2 + (dCp/Cp)^2)^2 
        Dstats = np.multiply( D, np.sqrt( np.add( (2.0*Dstats1) ,Dstats2) ) ) #D* sqrt( 2(dX/X)^2 + (dY/Y)^2) )
        Dstats[np.isnan(Dstats)] = 0.0
        Dstats[np.isinf(Dstats)] = 0.0
        print Dstats

        Dhist = rn.array2hist(D, Dhist, errors=Dstats)
        

    rf.Close()
    if not returnhists:
        return Dhist
    elif returnhists:
        return Dhist, closureroothists
