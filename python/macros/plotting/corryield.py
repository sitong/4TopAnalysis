import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import Defs as defs
import RootStuff as rs
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
gStyle.SetPalette(1)

parser = argparse.ArgumentParser(description='bla')
parser.add_argument("-p", "--proc", dest="proc", default='TTbb')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

pd = plotdefs(args.year, 'SR', modbins=False)
if args.proc=='TTbb':
    mcfile = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/python/macros/eventMVA/updatedrootfiles/TT_'+args.year+'_merged_0lep_tree.root'
else:
    mcfile  = pd.mcfiles[args.proc] #TTTT, TTX or minor

# mcfile  = pd.mcfiles[args.proc] #TTTT, TTX or minor
# mcfile=mcfile.replace('maintrees_bSFcorradded','allsysts')
print mcfile
mcrf, mct = rs.makeroot(mcfile, pd.tn)

systlist = ['nosys', 'jesUp', 'jesDown', 'jerUp', 'jerDown', 'btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down']
# systlist = ['nosys']

#if proc = TTH, TTZ, TTW, or TTG, use TTX as the proc for the histogram
proc = args.proc
if args.proc in ['ttH', 'ttW', 'ttZ', 'ttOther']:
    proc = 'TTX'

corrhistfile = r.TFile("bSFcorrections.root","READ")
corrhists = {}
if proc not in ['QCD']:
    for syst in systlist:
        # print "corr"+proc+args.year+'_'+syst
        hist = corrhistfile.Get("corr"+proc+args.year+'_'+syst)
        # print hist
        corrhists["corr"+proc+args.year+'_'+syst] = hist
        hist.SetDirectory(0)
    corrhistfile.Close()

# trigfile      = r.TFile(os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/TriggerSFFiles/effSFs_2017fix.root'))
# trighist      = trigfile.Get("h2_eff")
# trighist.SetDirectory(0)
# trigfile.Close()

def getBinContent2D( hist, x, y):
    bin_x0 = hist.GetXaxis().FindFixBin(x)
    bin_y0 = hist.GetYaxis().FindFixBin(y)
    binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
    binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
    if binX>hist.GetNbinsX(): ##? set to last bin
        binX=hist.GetNbinsX()
    if binY>hist.GetNbinsY():
        binY=hist.GetNbinsY()
    val = hist.GetBinContent(binX, binY)
    # print 'val', val, 'binX', binX, 'binY', binY, 'last bin:', hist.GetNbinsX(), hist.GetNbinsY()
    err = hist.GetBinError(binX, binY)
    return np.array([val, val + err, val - err])

def get_sf( hist, var1, var2, syst=None):
    # `eta` refers to the first binning var, `pt` refers to the second binning var
    x, y = var1, var2
    SF = np.ones(3)
    SF = getBinContent2D(hist, x, y)
    if syst is not None:
        stat = SF[1] - SF[0]
        unc  = np.sqrt(syst*syst+stat*stat)
        SF[1] = SF[0] + unc
        SF[2] = SF[0] - unc
    return SF

    
outname = mcfile[mcfile.rfind('/')+1:]
outrf = r.TFile.Open(outname, "update")    
outrf.cd()
outtree=mct.CloneTree(0)

bSFcorr_nosys = np.full(1, 1.0, dtype=np.float32)
outtree.Branch('bSFcorr_nosys',   bSFcorr_nosys,   'bSFcorr_nosys/F')

if proc not in ['TT', 'QCD']:

    bSFcorr_nosys = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_jesUp = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_jesDown = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_jerUp = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_jerDown = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagLFUp = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagLFDown = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagHFUp = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagHFDown = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagLFstats1Up = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagLFstats1Down = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagHFstats1Up = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagHFstats1Down = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagLFstats2Up = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagLFstats2Down = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagHFstats2Up = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagHFstats2Down = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagCFerr1Up = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagCFerr1Down = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagCFerr2Up = np.full(1, 1.0, dtype=np.float32)
    bSFcorr_btagCFerr2Down = np.full(1, 1.0, dtype=np.float32)

    outtree.Branch('bSFcorr_nosys',   bSFcorr_nosys,   'bSFcorr_nosys/F')
    outtree.Branch('bSFcorr_jesUp',   bSFcorr_jesUp,   'bSFcorr_jesUp/F')
    outtree.Branch('bSFcorr_jesDown', bSFcorr_jesDown, 'bSFcorr_jesDown/F')
    outtree.Branch('bSFcorr_jerUp',   bSFcorr_jerUp,   'bSFcorr_jerUp/F')
    outtree.Branch('bSFcorr_jerDown', bSFcorr_jerDown, 'bSFcorr_jerDown/F')
    outtree.Branch('bSFcorr_btagLFUp',   bSFcorr_btagLFUp,   'bSFcorr_btagLFUp/F')
    outtree.Branch('bSFcorr_btagLFDown',   bSFcorr_btagLFDown,   'bSFcorr_btagLFDown/F')
    outtree.Branch('bSFcorr_btagHFUp',   bSFcorr_btagHFUp,   'bSFcorr_btagHFUp/F')
    outtree.Branch('bSFcorr_btagHFDown',   bSFcorr_btagHFDown,   'bSFcorr_btagHFDown/F')
    outtree.Branch('bSFcorr_btagLFstats1Up',   bSFcorr_btagLFstats1Up,   'bSFcorr_btagLFstats1Up/F')
    outtree.Branch('bSFcorr_btagLFstats1Down',   bSFcorr_btagLFstats1Down,   'bSFcorr_btagLFstats1Down/F')
    outtree.Branch('bSFcorr_btagHFstats1Up',   bSFcorr_btagHFstats1Up,   'bSFcorr_btagHFstats1Up/F')
    outtree.Branch('bSFcorr_btagHFstats1Down',   bSFcorr_btagHFstats1Down,   'bSFcorr_btagHFstats1Down/F')
    outtree.Branch('bSFcorr_btagLFstats2Up',   bSFcorr_btagLFstats2Up,   'bSFcorr_btagLFstats2Up/F')
    outtree.Branch('bSFcorr_btagLFstats2Down',   bSFcorr_btagLFstats2Down,   'bSFcorr_btagLFstats2Down/F')
    outtree.Branch('bSFcorr_btagHFstats2Up',   bSFcorr_btagHFstats2Up,   'bSFcorr_btagHFstats2Up/F')
    outtree.Branch('bSFcorr_btagHFstats2Down',   bSFcorr_btagHFstats2Down,   'bSFcorr_btagHFstats2Down/F')
    outtree.Branch('bSFcorr_btagCFerr1Up',   bSFcorr_btagCFerr1Up,   'bSFcorr_btagCFerr1Up/F')
    outtree.Branch('bSFcorr_btagCFerr1Down',   bSFcorr_btagCFerr1Down,   'bSFcorr_btagCFerr1Down/F')
    outtree.Branch('bSFcorr_btagCFerr2Up',   bSFcorr_btagCFerr2Up,   'bSFcorr_btagCFerr2Up/F')
    outtree.Branch('bSFcorr_btagCFerr2Down',   bSFcorr_btagCFerr2Down,   'bSFcorr_btagCFerr2Down/F')

# if args.year=='2017':
#     fixtrig_nosys = np.full(1, 1.0, dtype=np.float32)
#     fixtrigUp = np.full(1, 1.0, dtype=np.float32);     fixtrigDown = np.full(1, 1.0, dtype=np.float32)
#     outtree.Branch('fixtrigSF_nosys',      fixtrig_nosys,     'fixtrigSF_nosys/F')
#     outtree.Branch('fixtrigSF_Up',   fixtrigUp,   'fixtrigSF_Up/F')
#     outtree.Branch('fixtrigSF_Down', fixtrigDown, 'fixtrigSF_Down/F')
#     if proc not in ['TT', 'QCD']:
#         fixtrig_jerUp = np.full(1, 1.0, dtype=np.float32); fixtrig_jerDown = np.full(1, 1.0, dtype=np.float32)
#         fixtrig_jesUp = np.full(1, 1.0, dtype=np.float32); fixtrig_jesDown = np.full(1, 1.0, dtype=np.float32)
#         outtree.Branch('fixtrigSF_jerUp',      fixtrig_jerUp,     'fixtrigSF_jerUp/F')
#         outtree.Branch('fixtrigSF_jerDown',    fixtrig_jerDown,   'fixtrigSF_jerDown/F')
#         outtree.Branch('fixtrigSF_jesUp',      fixtrig_jesUp,     'fixtrigSF_jesUp/F')
#         outtree.Branch('fixtrigSF_jesDown',    fixtrig_jesDown,   'fixtrigSF_jesDown/F')

if proc not in ['QCD']:
    for i, evt in enumerate(mct):
        SFs = {}
        htnj = {}
        htnj = {"corr"+proc+args.year+'_nosys':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_jesUp':[evt.njets_jesUp, evt.ht_jesUp],
                "corr"+proc+args.year+'_jesDown':[evt.njets_jesDown, evt.ht_jesDown],
                "corr"+proc+args.year+'_jerUp':[evt.njets_jerUp, evt.ht_jerUp],
                "corr"+proc+args.year+'_jerDown':[evt.njets_jerDown, evt.ht_jerDown],
                "corr"+proc+args.year+'_btagLFUp':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagLFDown':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagHFUp':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagHFDown':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagLFstats1Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagLFstats1Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagHFstats1Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagHFstats1Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagLFstats2Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagLFstats2Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagHFstats2Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagHFstats2Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagCFerr1Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagCFerr1Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagCFerr2Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+args.year+'_btagCFerr2Down':[evt.njets_nosys, evt.ht_nosys]}

        for name, hist in corrhists.iteritems():
            nj = htnj[name][0]
            ht = htnj[name][1]
            if nj>=11:
                nj=12 #put in that bin
            if ht>=1450.0:
                ht=1500.0 #put in that bin
            sf = get_sf(hist, nj, ht) # x is NJ y is HT
            SFs[name] = sf[0]
        #print 'njets', nj, 'ht', ht, 'CF', SF[0]

        bSFcorr_nosys[0] = SFs["corr"+proc+args.year+'_nosys']
        bSFcorr_jesUp[0] = SFs["corr"+proc+args.year+'_jesUp']
        bSFcorr_jesDown[0] = SFs["corr"+proc+args.year+'_jesDown']
        bSFcorr_jerUp[0] = SFs["corr"+proc+args.year+'_jerUp']
        bSFcorr_jerDown[0] = SFs["corr"+proc+args.year+'_jerDown']
        bSFcorr_btagLFUp[0] = SFs["corr"+proc+args.year+'_btagLFUp']
        bSFcorr_btagLFDown[0] = SFs["corr"+proc+args.year+'_btagLFDown']
        bSFcorr_btagHFUp[0] = SFs["corr"+proc+args.year+'_btagHFUp']
        bSFcorr_btagHFDown[0] = SFs["corr"+proc+args.year+'_btagHFDown']
        bSFcorr_btagLFstats1Up[0] = SFs["corr"+proc+args.year+'_btagLFstats1Up']
        bSFcorr_btagLFstats1Down[0] = SFs["corr"+proc+args.year+'_btagLFstats1Down']
        bSFcorr_btagHFstats1Up[0] = SFs["corr"+proc+args.year+'_btagHFstats1Up']
        bSFcorr_btagHFstats1Down[0] = SFs["corr"+proc+args.year+'_btagHFstats1Down']
        bSFcorr_btagLFstats2Up[0] = SFs["corr"+proc+args.year+'_btagLFstats2Up']
        bSFcorr_btagLFstats2Down[0] = SFs["corr"+proc+args.year+'_btagLFstats2Down']
        bSFcorr_btagHFstats2Up[0] = SFs["corr"+proc+args.year+'_btagHFstats2Up']
        bSFcorr_btagHFstats2Down[0] = SFs["corr"+proc+args.year+'_btagHFstats2Down']
        bSFcorr_btagCFerr1Up[0] = SFs["corr"+proc+args.year+'_btagCFerr1Up']
        bSFcorr_btagCFerr1Down[0] = SFs["corr"+proc+args.year+'_btagCFerr1Down']
        bSFcorr_btagCFerr2Up[0] = SFs["corr"+proc+args.year+'_btagCFerr2Up']
        bSFcorr_btagCFerr2Down[0] = SFs["corr"+proc+args.year+'_btagCFerr2Down']
        # if args.year=='2017':
        #     trig_nosys = get_sf(trighist, evt.nbjets_nosys, evt.njets_nosys)
        #     trig_jesUp = get_sf(trighist, evt.nbjets_jesUp, evt.njets_jesUp)
        #     trig_jesDown = get_sf(trighist, evt.nbjets_jesDown, evt.njets_jesDown)
        #     trig_jerUp = get_sf(trighist, evt.nbjets_jerUp, evt.njets_jerUp)
        #     trig_jerDown = get_sf(trighist, evt.nbjets_jerDown, evt.njets_jerDown)

        #     fixtrig_nosys[0] = trig_nosys[0]
        #     fixtrigUp[0]    = trig_nosys[1]
        #     fixtrigDown[0]  = trig_nosys[2]
        #     fixtrig_jesUp[0]   = trig_jesUp[0]
        #     fixtrig_jesDown[0] = trig_jesDown[0]
        #     fixtrig_jerUp[0]   = trig_jerUp[0]
        #     fixtrig_jerDown[0] = trig_jerDown[0]
        outtree.Fill()

elif proc in ['TT', 'QCD']:

    for i, evt in enumerate(mct):
        bSFcorr_nosys[0] = 1.0
        if args.year=='2017':
            trig_nosys = get_sf(trighist, evt.nbjets_nosys, evt.njets_nosys)
            fixtrig_nosys[0] = trig_nosys[0]
            fixtrigUp[0]    = trig_nosys[1]
            fixtrigDown[0]  = trig_nosys[2]
        outtree.Fill()


outtree.Write("", r.TFile.kOverwrite)
outtree.ResetBranchAddresses()

outrf.Close()
mcrf.Close()
print 'wrote', outrf

