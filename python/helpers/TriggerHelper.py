import os
import numpy as np
import math
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
import ROOT
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
# from rootpy.tree import Tree, TreeModel, FloatCol
# from rootpy.io import root_open

class TriggerHelper:

    def __init__(self, trighist, ismc=True):
        self.ismc = ismc
        self.trighist = trighist

    def getBinContent2D(self, hist, x, y):
        bin_x0 = hist.GetXaxis().FindFixBin(x)
        bin_y0 = hist.GetYaxis().FindFixBin(y)
        binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
        binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
        if binX>hist.GetNbinsX(): ##? set to last bin
            binX=hist.GetNbinsX()
        if binY>hist.GetNbinsY():
            binY=hist.GetNbinsY()
        val = hist.GetBinContent(binX, binY)
        err = hist.GetBinError(binX, binY)
        return np.array([val, val + err, val - err])

    def get_trig_sf(self, var1, var2, syst=None):
        if not self.ismc:
            return [1.0, 1.0, 1.0]
        # `eta` refers to the first binning var, `pt` refers to the second binning var
        x, y = var1, var2
        SF = np.ones(3)
        SF = self.getBinContent2D(self.trighist, x, y)
        if syst is not None:
            stat = SF[1] - SF[0]
            unc  = np.sqrt(syst*syst+stat*stat)
            SF[1] = SF[0] + unc
            SF[2] = SF[0] - unc
        return SF

