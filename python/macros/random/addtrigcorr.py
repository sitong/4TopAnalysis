import subprocess
import os.path
from root_numpy import root2array, array2root, fill_hist, array2tree
from rootpy.tree import Tree, TreeModel, FloatCol
from rootpy.io import root_open
from rootpy.tree import Tree
import argparse
from array import array
import numpy as np
import pandas as pd
from root_pandas import to_root
import ROOT as r

parser = argparse.ArgumentParser(description='rootfile skimmer and more!')
#general:
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

jethtSFs           = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/TriggerSFFiles/cvalue.root')
#open root files:
file0lepSF        = r.TFile(jethtSFs)
#get histograms
chist      = file0lepSF.Get("nbnj"+args.year)


def getBinContent2D(hist, x, y):
    bin_x0 = hist.GetXaxis().FindFixBin(x)
    bin_y0 = hist.GetYaxis().FindFixBin(y)
    binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
    binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
    if binX>hist.GetNbinsX(): ##? set to last bin
        binX=hist.GetNbinsX()
    if binY>hist.GetNbinsY():
        binY=hist.GetNbinsY()
    val = hist.GetBinContent(binX, binY)
    # print 'val', val, 'binX', binX, 'binY', binY, 'last bin:', hist.GetNbinsX(), hist.GetNbinsY()
    err = hist.GetBinError(binX, binY)
    return np.array([val, val + err, val - err])

def get_sf(hist, var1, var2, syst=None):
    # `eta` refers to the first binning var, `pt` refers to the second binning var
    x, y = var1, var2
    SF = np.ones(3)
    SF = getBinContent2D(hist, x, y)
    if syst is not None:
        stat = SF[1] - SF[0]
        unc  = np.sqrt(syst*syst+stat*stat)
        SF[1] = SF[0] + unc
        SF[2] = SF[0] - unc
    return SF
    
skim = 'nleptons==0 && njets>=6 && nbjets>=2 && ht>=700'

for f in args.filenames:
    outf=f
    f=f.replace("/eos/uscms/","root://cmseos.fnal.gov/")
    outname = outf[outf.rfind('/')+1:]
    print 'prepping input file', f, '...'

    data = pd.DataFrame(root2array(f, 'Events', selection=skim))
    nbjets = data['nbjets']
    njets = data['njets']
    SFs = []
    for (nj, nb) in zip(njets, nbjets):
        SF = get_sf(chist, nb, nj)
        # print SF
        SFs.append(SF[0])
    data['trigcorr']=SFs

    array2root(data.to_records(index=False), filename=outname, treename='Events', mode='RECREATE')

#run command:
#python addtrigcorr.py -f /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/JETHT_2016_merged_0lep_tree.root -y 2016
