# for realsies
#calcs weight using nanoaod rootfiles (ntuples). not fucking around.
import subprocess
import os
import argparse
import sys
import math
import numpy as np
import ROOT as r
r.gStyle.SetOptStat(0)
from xsecdict import xsecdict16, xsecdict17, xsecdict18

parser = argparse.ArgumentParser(description='xsec weights') 
parser.add_argument("-d", "--dataset", dest="dataset", default='../datasets/nanoaodv5_MC2018.txt') 
parser.add_argument("-i", "--indir", dest="indir", default='/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC/') 
parser.add_argument("-l", "--lumi", dest="lumi", default=1000.0) 
parser.add_argument("-y", "--year", dest="year", default='2018') 
args = parser.parse_args()

#for adding training samples as well
traindir = '/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv5/BDTTRAIN/'
trainsamps = ['TT_TuneCUETP8M2T4_13TeV-powheg-pythia8',
            'QCD_HT1500to2000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8',
            'QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8',
            'QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8',
            'QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8',
            'QCD_HT500to700_TuneCUETP8M1_13TeV-madgraphMLM-pythia8']

ignore = ['/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2016/MC/ttHTobb_SPLIT/93765152-270F-D246-A0A5-C739BB5CC549_ttHTobb2016_1.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2016/MC/ttHToNonbb_SPLIT/94BD357E-51D4-1242-A18E-909A9DBC3D14_ttHToNonbb2016_1.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2016/MC/ttHToNonbb_SPLIT/54388644-5FE6-BF48-981D-5119B97C2404_ttHToNonbb2016_2.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHTobb_SPLIT/9C9984DA-94C3-BD48-8FA9-1A9F05EEC11F_ttHTobb2017_0.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHTobb_SPLIT/9C9984DA-94C3-BD48-8FA9-1A9F05EEC11F_ttHTobb2017_1.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHTobb_SPLIT/5597FE70-1429-9644-BB43-CBB8EB5F3EBD_ttHTobb2017_1_1.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHTobb_SPLIT/AD4CEA54-7358-6341-9C9B-10F65A923CD5_ttHTobb2017_0.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHTobb_SPLIT/AD4CEA54-7358-6341-9C9B-10F65A923CD5_ttHTobb2017_3.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHToNonbb_SPLIT/6D9AE19D-64AB-E443-A164-CD36642A6F61_ttHToNonbb2017_0.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHToNonbb_SPLIT/9393182E-09E3-4D44-B4D9-A2C7A8988EA4_ttHToNonbb2017_1.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/ttHToNonbb_SPLIT/9393182E-09E3-4D44-B4D9-A2C7A8988EA4_ttHToNonbb2017_2.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/TTZToLLNuNu_SPLIT/7CC488B3-0A87-8E46-B7B0-3789C394CE8C_TTZToLLNuNu2017_3.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC/TTZToLLNuNu_SPLIT/BB7180D5-720D-574C-B3DB-F8F77777CDF7_TTZToLLNuNu2017_1.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC/TTGJets_SPLIT/3504816A-7439-E147-BBDC-4EA3DCEEBE0C_TTGJets2018_1.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC/ttHTobb_SPLIT/3CB6282A-C983-0B44-AC96-EC6290CB32BA_ttHTobb2018_0.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC/ttHTobb_SPLIT/3CB6282A-C983-0B44-AC96-EC6290CB32BA_ttHTobb2018_2.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC/ttHTobb_SPLIT/3CB6282A-C983-0B44-AC96-EC6290CB32BA_ttHTobb2018_3.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC/ttHTobb_SPLIT/A5A6B45C-72A6-D748-9D93-E9BC91ABFDE5_ttHTobb2018_0.root',
          '/store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC/ttHTobb_SPLIT/E04467B5-8DCE-7445-BCF0-50C7A01496DA_ttHTobb2018_1.root']

#HAVE TO FIX GENJET5
addtrainfiles = False
addgjQCD = False

year = str(args.year)
if year =='2016':
    xsecdict = xsecdict16
elif year =='2017':
    xsecdict = xsecdict17
elif year =='2018':
    xsecdict = xsecdict18

def makeroot(infile, treename='Events', options="read"):
    rfile = r.TFile(infile, options)
    tree = rfile.Get(treename)
    return rfile, tree

def getweight(samp, nPos, nNeg, lumi=1.0):
# xsecweight = lumi_ * xsec_ * 1000 / (nPos_ > 0 ? (nPos_ + nNeg_) : float(nEvents));
# if nPos>0 then /nPos+nNeg else /nEvents 
# weight = xsecweight*genwgtsign / (1.0 - (2.0*(nNeg_/(nPos_+nNeg_))))

    totevts = nPos+nNeg #nentries
    xsec = xsecdict[samp]
    xsecweight = 0.0; weight = 0.0
    if totevts >0.0:
        xsecweight = (lumi*xsec*1000.0)/totevts
        weight = xsecweight/(1.0 - (2.0*(nNeg/(nPos+nNeg))))
    print 'sample',samp, 'nentries', totevts,'xsecweight',xsecweight, 'weight', weight 
    # return weight

with open(args.dataset, "r") as samps:
    bins = {'200to300'  :{'nqcdGJg5':0, 'nqcdGJl5':0}, #for qcd
            '300to500'  :{'nqcdGJg5':0, 'nqcdGJl5':0}, 
            '500to700'  :{'nqcdGJg5':0, 'nqcdGJl5':0},
            '700to1000' :{'nqcdGJg5':0, 'nqcdGJl5':0},
            '1000to1500':{'nqcdGJg5':0, 'nqcdGJl5':0},
            '1500to2000':{'nqcdGJg5':0, 'nqcdGJl5':0},
            '2000toInf' :{'nqcdGJg5':0, 'nqcdGJl5':0}
            }

    for line in samps:
        if '#' not in line:
            samp=line.strip()
            print 'loading', samp, '...'
            indir = args.indir+samp
            # print 'indir', indir+'/'
             
            npos_tot=0; nneg_tot=0; nents_tot=0

            if samp in trainsamps and addtrainfiles:
                # print 'adding BDTTRAIN sample for '+samp+'...'
                # traindir=traindir+samp
                # print traindir
                for subdir, dirs, files in os.walk(traindir+samp):
                    # print 'train files', files
                    for file in files:
                        # print file
                        f=subdir+'/'+file
                        # print f
                        rf, tree = makeroot(f)
                        #should project not loop over events
                        hpos = r.TH1D('hpos','hpos',1, -1000, 1000)
                        hneg = r.TH1D('hneg','hneg',1, -1000, 1000)
                        tree.Draw("Events.genWeight>>hpos", "Events.genWeight>=0.0")
                        tree.Draw("Events.genWeight>>hneg", "Events.genWeight<0.0")
                        nents = tree.GetEntries()
                        npos = hpos.Integral(0,2)
                        nneg = hneg.Integral(0,2)
                        npos_tot+=npos
                        nneg_tot+=nneg
                        nents_tot+=nents 
                        # print 'trainfile update:', 'npos', npos_tot, 'nneg', nneg_tot, 'total', npos_tot+nneg_tot
                print 'train sample', samp, 'npos', npos_tot, 'nneg', nneg_tot, 'total', npos_tot+nneg_tot, nents_tot
                # traindir=traindir
            
            if ('QCD' in samp) and addgjQCD:
                print 'QCD sample'
                tag = ''
                for key in bins:
                    if key in samp:
                        tag = key
                print 'tag is', tag
                if 'GenJets5' in samp or tag=='200to300':                    
                    print 'calculating qcd genjet5 weight...'
                    print 'start',bins[tag]
                    for subdir, dirs, files in os.walk(indir):
                        for file in files:
                            # print 'gj5 tot', bins[tag]['nqcdGJg5']
                            # print file
                            f=subdir+'/'+file
                            rf, tree = makeroot(f)
                            nents = tree.GetEntries()
                            bins[tag]['nqcdGJg5']+=nents
                            # print 'gj5 tot', bins[tag]['nqcdGJg5']
                    print 'end',bins[tag]
                elif not('GenJets5' in samp) and tag!='200to300':
                    print 'start',bins[tag]
                    print 'calculating qcd weight using genjet5...'
                    for subdir, dirs, files in os.walk(indir):
                        for file in files:
                            # print 'gj5 tot gr5 start', bins[tag]['nqcdGJg5']
                            # print 'gj5 tot ls5 start', bins[tag]['nqcdGJl5']
                            # print file
                            f=subdir+'/'+file
                            rf, tree = makeroot(f)
                            hpos = r.TH1F('hpos','hpos',1, -1000, 1000)
                            hneg = r.TH1F('hneg','hneg',1, -1000, 1000)
                            tree.Draw("Events.genWeight>>hpos", "Events.nGenJet>=5")
                            tree.Draw("Events.genWeight>>hneg", "Events.nGenJet<5")
                            nents  = tree.GetEntries()
                            ngreat = hpos.Integral(0,2)
                            nless  = hneg.Integral(0,2)
                            bins[tag]['nqcdGJg5']+=ngreat
                            bins[tag]['nqcdGJl5']+=nless
                            nents_tot+=nents
                            # print 'ngreat', ngreat, 'nless', nless, 'nents_tot', nents
                    print 'end',bins[tag]
                print 'sample', samp, 'nqcdGJg5 for',tag, bins[tag]['nqcdGJg5'], 'nqcdGJl5 for', tag,  bins[tag]['nqcdGJl5'], 'total', npos_tot+nneg_tot, nents_tot
                if not ('GenJets5' in samp): #NOTE: genjets5 has to be FIRST
                    print 'QCD weight nGenJets>=5:'
                    getweight(samp, bins[tag]['nqcdGJg5'], 0)
                    if not tag=='200to300':
                        print 'QCD weight nGenJets<5:'
                        getweight(samp, bins[tag]['nqcdGJl5'], 0)

            elif not addgjQCD and not('GenJets5' in samp):
                print 'normal sample'
                for subdir, dirs, files in os.walk(indir):
                    # if 'ext' in subdir:
                    #     continue
                    for file in files:
                        # print file
                        f=subdir+'/'+file
                        skip = False
                        for ig in ignore:
                            if ig in f:
                                print 'ignoring', ig
                                skip = True
                        if skip:
                            continue
                        rf, tree = makeroot(f)
                        # print 'file:',f
                        hpos = r.TH1F('hpos','hpos',1, -1000, 1000)
                        hneg = r.TH1F('hneg','hneg',1, -1000, 1000)
                        tree.Draw("Events.genWeight>>hpos", "Events.genWeight>=0.0")
                        tree.Draw("Events.genWeight>>hneg", "Events.genWeight<0.0")
                        nents = tree.GetEntries()
                        npos = hpos.Integral(0,2)
                        nneg = hneg.Integral(0,2)
                        npos_tot+=npos
                        nneg_tot+=nneg
                        nents_tot+=nents
                        # print 'file update:', 'npos', npos_tot, 'nneg', nneg_tot, 'total', npos_tot+nneg_tot
                    print 'subdir', subdir, 'sample', samp, 'npos', npos_tot, 'nneg', nneg_tot, 'total', npos_tot+nneg_tot, nents_tot
                print 'sample', samp, 'npos', npos_tot, 'nneg', nneg_tot, 'total', npos_tot+nneg_tot, nents_tot
                getweight(samp, npos_tot, nneg_tot)
                print '\n'

            elif not addgjQCD and ('GenJets5' in samp):
                print 'ignoring genjet5 sample...'
