import numpy as np
from ROOT import TMVA, TFile, TTree, TCut, TString
import logging
import array
from collections import OrderedDict

class TMVAHelper:
    
    def __init__(self, model_file, var_list):
        TMVA.Tools.Instance()
        self.firstcall = True
        self.modelfile = model_file
        self.varlist = var_list #list of variable names
        self.varDict = OrderedDict()
        for v in self.varlist:
            self.varDict[v] = array.array('f', [0.])
        self.reader = TMVA.Reader("!Color:!Silent") #need to delete?
        print 'model file:', model_file
        logging.info('Load TMVA model %s, input varaibles:\n  %s' % (model_file, str(var_list)))
        
    def addVariables(self): #needs to know what are ints, else floats
        for name, value in self.varDict.items():
            name = TString(name) #needs to be tstring
            self.reader.AddVariable(name, value)
        
    def setValue(self, bdtdict): #takes ORDERED dictionary!
        for name, value in bdtdict.items():
            if name not in self.varDict:
                print "ERROR: DICTIONARY DOES NOT MATCH BDT VARLIST! (var is ", name, " )"
                break
            if not isinstance(value,float):
                value = float(value) #only wants floats. fine
            self.varDict[name][0] = value
        
    def eval(self):
        if self.firstcall:
            self.reader.BookMVA("BDT", self.modelfile)
            self.firstcall = False
        bdtdisc = self.reader.EvaluateMVA("BDT")
        return bdtdisc

        

