#changes the weight in existing rootfile
from root_numpy import root2array, array2root
# import varlists as vl
import numpy as np
import pandas as pd
# import xgboost as xgb
import subprocess
import os
import sys
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score
import re
# import xml.etree.cElementTree as ET
import sys
sys.path.append("../plotting/")
import RootStuff as rs
import argparse

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-y", "-year", dest="year", default='2016')
args = parser.parse_args()


infiles = args.filenames#'/eos/uscms//store/user/mequinna/NanoAODv5/restop_training/modtrain/TTJets_DiLept_trainsamp_Friend.root'
outfile = './TTG_rewght_' #'./ttbar2l_trainsamp_10kmerged_fixwgt_Friend.root'
treename = 'Events'
wgtvar = "weight"

TTGweights = {'2016':0.0007743, '2017':0.0012953, '2018':0.0020066}
wgtval = TTGweights[args.year]

#2016 ST_tW_antitop 0.00517085157074
#2017 ST_tW_top 0.00463517009329

# base=''
# base = 'nleptons==0 && ht>700 & njets>=9 && nbjets>=3'
# base = 'nleptons==0 && ht>700 & njets>=9 && nbjets>=3'

for n,infile in enumerate(infiles):
    print "prepping input file", infile
    data = pd.DataFrame(root2array(infile, treename))
# data = pd.DataFrame(root2array(infile, treename, selection=base))
    print '# input events:', len(data)

    print 'writing file'
    inrf, intree = rs.makeroot(infile, treename)
    lenarr = intree.GetEntries()
    warr = np.full(lenarr, wgtval, dtype=np.float32) 
    # print data[wgtvar][:20]
    # for i,w in enumerate(data[wgtvar]):
    #     newW=w
    #     if data['ngenleps'][i]==0:
    #         newW = 0.01229547748
    #     elif data['ngenleps'][i]==1:
    #         newW = 0.01511563085
    #     elif data['ngenleps'][i]==2:
    #         newW = 0.03785496783
    #     # print w, newW
    #     data[wgtvar][i]=newW
    # print data[wgtvar][:20]

    for i,w in enumerate(warr):
        oldval = data[wgtvar][i]
        # # print 'old', oldval
        # if abs(float(oldval)) == np.float32(2.72619794209):
        #     # print float(oldval)
        #     w=5.43394042215
        # elif abs(float(oldval)) == np.float32(0.06763506716):
        #     # print oldval
        #     w=0.136409486613
        # elif abs(float(oldval))== np.float32(0.0330165409262):
        #     # print'conv', oldval
        #     w=0.0654710859292
        # else:
        #     print 'no change',float(oldval)
        #     w= oldval
        w=warr[i]

        if oldval<0:
            # print 'neg'
        # print data[wgtvar][i]
            warr[i]=-w #get right sign
        else:
            warr[i]=w
        # print 'new', warr[i]
    print 'old:', data[wgtvar][0:20]
    print 'new:', warr[0:20]
    data[wgtvar] = warr
    outfilename = outfile + str(n) + '.root'
    array2root(data.to_records(index=False), filename=outfilename, treename=treename, mode='RECREATE')

    # if oldval == 0.0005373: #TTTT
    #     print 'TTTT'
    #     w = 1.17282333762e-05
    # elif oldval == 0.00276349997147917747 : #ttbar 1lept
    #     print 'ttbar 1lept'
    #     w = 0.0029564491893 
    # elif oldval == 0.00301899993792176247 : #ttbar 1leptbar
    #     print 'ttbar 1leptbar'
    #     w = 0.00301903666845
    # elif oldval ==  0.00283139990642666817 or oldval==1.0: #ttbar 2lep
    #     print 'ttbar 2lep'
    #     w = 0.00287834598655
    # else:
    #     print 'unchanged', oldval
    #     w=oldval

 
    # if oldval==0.00003660:
    #     w=0.000774330514089
    # elif oldval==0.0001143:
    #     w=7.52140175661e-05
    # elif oldval==0.0007133:
    #     w= 0.000943970625828
    # elif oldval==0.00007278:
    #     w=3.93874618916e-0
    # elif oldval==0.00007208:
    #     w=2.09450240094e-05
    # elif oldval==0.00004034:
    #     w=3.0046759886e-05
    # elif oldval==0.00005584:
    #     w=2.17033056853e-05

    # elif oldval==-0.00003660:
    #     w=-0.000774330514089
    # elif oldval==-0.0001143:
    #     w=-7.52140175661e-05
    # elif oldval==-0.0007133:
    #     w=- 0.000943970625828
    # elif oldval==-0.00007278:
    #     w=-3.93874618916e-0
    # elif oldval==-0.00007208:
    #     w=-2.09450240094e-05
    # elif oldval==-0.00004034:
    #     w=-3.0046759886e-05
    # elif oldval==-0.00005584:
    #     w=-2.17033056853e-05
