#just gets best s/sqrt(b) for event level bdt cut
import os
import math
import numpy as np
import argparse
import uproot
import types
from collections import OrderedDict
import uproot_methods.classes.TH1
from ROOT import gROOT,TFile,TTree,TH1D
import ROOT as r
from datacard_dictionary import getprocesses
from cardtools import cardtools
gROOT.SetBatch(True)

def getRate(processname, cutstr, year='2016'):
            #calculates rates and statistical uncertainties
    # cutstr = '('+str(args.lumi)+'*'+weight+')*('+cutstr+')'
    pfile = r.TFile(processes[processname])
    tree = pfile.Get('Events')
    htmp = r.TH1F('htmp','htmp',1,0,10000)
    tree.Project('htmp','catBDTdisc',cutstr)
    errorVal = r.Double(0)
    rate = htmp.IntegralAndError(0,3,errorVal)
            # the error is computed using error propagation from the bin errors assuming bins uncorrelated
    if rate < 0:#treatment of negative yields 
        print 'WARNING: NEGATIVE YIELD:', rate, processname, cutstr #will throw error
        rate = 0.0
    return round(rate,3), round(errorVal,3)
    
lumi = 36
year = '2018'
if year=='2016':
    lumi=36
elif year=='2017':
    lumi=42
elif year=='2018':
    lumi=60

# base = 'njets>=9 && nbjets>=3 && nleptons==0 && ht>=700'
base = 'njets>=9 && nbjets>=3 && nleptons==0 && ht>=700 && nrestops>=1'
bdtcuts = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.83, 0.85, 0.87, 0.9, 0.95, 0.98]
print year
print base
processes = getprocesses(year)
for thr in bdtcuts:
    s = 0.0
    b = 0.0
    cutstr = '('+base+' && catBDTdisc>='+str(thr)+')*weight*'+str(lumi)
    for proc in processes:
        rate, unc = getRate(proc, cutstr, year)
        if proc == 'TTTT':
            s=rate
        else:
            b+=rate
    print thr, s/math.sqrt(b) 
            
        
        
