# looks through job directory and finds if jobs failed
from root_numpy import root2array, array2root
import numpy as np
import argparse
import pandas as pd
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
# import RootStuff as rs
import os
import mmap

parser = argparse.ArgumentParser(description='')
parser.add_argument("-d", "--dirname", dest="dirname", default='./jobs/')
parser.add_argument("-t", "--treename", dest="treename", default='Events')
args = parser.parse_args()

ntot = 0
nfail = 0
nout = 0
nerr = 0
nlog = 0
nunfin = 0

for filename in os.listdir(args.dirname):
    # print filename
    if '.err' in filename:
        nerr+=1
    elif '.log' in filename:
        nlog+=1
        check = filename.replace("log", "out")
        if check not in os.listdir(args.dirname):
            print filename, 'has no matching err/out file!'
            nunfin+=1

            # line_number=0
      # check = check.strip(".out")
      #       with open(args.dirname+'/metadata.json', 'r') as read_obj:
      #           # Read all lines in the file one by one
      #           for line in read_obj:
      #               # For each line, check if line contains the string
      #               line_number += 1
      # # "idx": 27,
      #               print '"idx": '+check+','
      #               if '"idx": '+check in line:
      #                   print line
    elif '.out' in filename:
        nout+=1
        ntot+=1
        with open(args.dirname+'/'+filename) as f:
            s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
            if not s.find('Normal termination'):
                print filename, 'does not teminate sucessfully'
                nfail+=1
            # else:
            #     print filename, 'does teminate sucessfully!'
              
print 'N failed jobs:', nfail
print 'total N jobs:', ntot
print 'N unfinished jobs:', nunfin
print 'nlog', nlog, 'nout', nout, 'nerr', nerr
