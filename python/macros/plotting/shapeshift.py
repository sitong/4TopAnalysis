import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

year = '2016'
basename = 'VR'
NAFfile = '../combine/NAF_with_mixed_yield/NAF_8jet_'+year+'_new_yield.root'
datafile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/JETHT_'+year+'_merged_0lep_tree.root'
# mcfile = ''

lumi = str(rs.lumis[year]['0lep'])
tn = 'Events'

shifts = [2, 10, 20, 30]
mcprocs = ["TTTT", "TTX", "minor"]


colors = [r.kRed-7, r.kOrange+1, r.kYellow-7, r.kGreen-9, r.kCyan-7, r.kAzure+1, r.kBlue-9, r.kViolet-4, r.kMagenta-9]

metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'

mcfiles = {'TTTT': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/TTTT_'+year+'_merged_0lep_tree.root',
           'TTX': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/TTX_'+year+'_merged_0lep_tree.root',
           'minor': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/minorMC_'+year+'_merged_0lep_tree.root',
       }


dstr = {'2016' : '&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
        '2017' : '&& (HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || HLT_PFHT430_SixJet40_BTagCSV_p080 || HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || HLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,
        '2018' : '&& (HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,}

base = {'VR':'nleptons==0 && ht>=700 && nbjets>=3 && njets==8',
        'SR':'nleptons==0 && ht>=700 && nbjets>=3 && njets>=9',
    }

mcstr = {'2016' : '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi,
         '2017' : '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi,
         '2018' : '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi,}

binsels = {'cut0bin0': 'ht>=700 && ht<800',
           'cut0bin1':'ht>=800 && ht<900', 
           'cut0bin2':'ht>=900 && ht<1000', 
           'cut0bin3':'ht>=1000 && ht<1100',
           'cut0bin4':'ht>=1100 && ht<1200',
           'cut0bin5':'ht>=1200 && ht<1300',
           'cut0bin6':'ht>=1300 && ht<1500', 
           'cut0bin7':'ht>=1500',
           'cut1bin0':'ht<1200',
           'cut1bin1':'ht>=1200',
           'cut2bin0':'ht<1500', 
           'cut2bin1':'ht>=1500',
       }
outdir = './plots/shapeshifthists/'+basename+year+'/'
if not os.path.exists(outdir):
    os.makedirs(outdir)


def gethist(name, filename, projvar, cut, nbins=10, selrange=[0,1], treename='Events'):
    rf, t = rs.makeroot(filename, treename)
    hist = rs.gethist(t, projvar, cut, nbins, selrange)
    rs.fixref(rf, hist)
    return hist

NAFfile = r.TFile(NAFfile)
for chanid, chansel in binsels.iteritems():
    datasel = '('+base[basename]+'&&'+chansel+')'+dstr[year]
    mcsel = '('+base[basename]+'&&'+chansel+')'+mcstr[year]
    print chanid
    print mcsel
    print datasel
    print shifts
    NAFname = chanid+'_'+year+'_BKG'

    for i,unc in enumerate(shifts):        

        predhist = NAFfile.Get(NAFname)
        predarr = rn.hist2array(predhist, include_overflow=True)
        datahist = gethist('data', datafile, 'catBDTdisc', datasel)
        #shift pred hist by the errors
        c1 = r.TCanvas()
        c1.cd()
        legend = r.TLegend(0.1, 0.6, 0.3, 0.9)
        stack = r.THStack("stack", "stack")
        stackup = r.THStack("stacku", "stacku")
        stackdown = r.THStack("stackd", "stackd")
        predhist.SetFillColor(r.kAzure+1)
        predhist.SetLineWidth(1)
        datahist.SetLineWidth(2)
        predhist.SetLineColor(r.kAzure)
        datahist.SetLineColor(r.kGray+2)
        legend.AddEntry(predhist, "DD prediction", "F")           
        legend.AddEntry(datahist, "data", "L")           
        # stack.GetYaxis().SetTitle('NEntries')
        # stack.GetXaxis().SetTitle('BDT discriminant')
        norm = 0.0
        for i, proc in enumerate(mcprocs):
            mchist =  gethist(proc, mcfiles[proc], 'catBDTdisc', mcsel, 10, [0,1])
            mchist.SetFillColor(r.TColor.GetColorDark(colors[i]))
            mchist.SetLineWidth(1)
            mchist.SetLineColor(r.TColor.GetColorDark(colors[i]))
            legend.AddEntry(mchist, proc, "F")           
            stack.Add(mchist)
            stackup.Add(mchist)
            stackdown.Add(mchist)
    
        datahist.Draw("hists")
        c1.Update()

        systup = 1.0+float(unc)/100
        systdown = 1.0-float(unc)/100
        histname_up = str(unc)+'%_up'
        histname_down = str(unc)+'%_down'
        uphist   = r.TH1F(histname_up  , histname_up  , 10, 0, 1)
        downhist = r.TH1F(histname_down, histname_down, 10, 0, 1)
        uparr = predarr; downarr=predarr
        for (valup, valdown) in zip(uparr,downarr):
            valup *= systup
            valdown *= systdown
        uphist = rn.array2hist(uparr,uphist)
        downhist = rn.array2hist(downarr,downhist)
        uphist.SetLineWidth(2)
        downhist.SetLineWidth(2)
        uphist.SetLineColor(colors[i])
        downhist.SetLineColor(colors[i])
        legend.AddEntry(uphist, str(unc), "L")           

        stack.Add(predhist)
        stackup.Add(uphist)
        stackdown.Add(downhist)

        stack.Draw("histsame")
        stackup.Draw("histsame")
        stackdown.Draw("histsame")
        
        
        legend.Draw()

        outname = outdir+"/"+chanid+"_"+year+"_shapeshift"+str(unc)+".png"
        r.gPad.Modified()
        # r.gPad.SetGrid()
        c1.Modified()
        #c1.Update()
        print 'created histogram', outname

        c1.SaveAs(outname)
    
