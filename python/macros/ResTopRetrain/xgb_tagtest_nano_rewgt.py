from root_numpy import root2array, array2root
import os
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# samples

#april 2021 testing
sig_file = 'root://cmseos.fnal.gov//store/user/lpcstop/noreplica/mequinna/oldtraining/smallntuples_trainsamps_spring2021//TTJets_SingleLept_2016_nanorestop_trainsample_tree.root'
bkg_file = 'root://cmseos.fnal.gov//store/user/lpcstop/noreplica/mequinna/oldtraining/smallntuples_trainsamps_spring2021//TTJets_DiLept_2016_nanorestop_trainsample_tree.root'

#used for old training
#these are here: /eos/uscms/store/user/mquinnan/NanoAODv5/restop_training/
# sig_file = '/eos/uscms/store/user/mquinnan/NanoAODv5/restop_training/TTJets_SingleLept_nano_4mil_trainfile.root' #100k each
# bkg_file = '/eos/uscms/store/user/mquinnan/NanoAODv5/restop_training/TTJets_DiLept_nano_700k_trainfile.root'
# bkg_file = '/eos/uscms/store/user/mquinnan/NanoAODv5/restop_training/TTJets_DiLept_nano_1mil_trainfile.root'

# sig_file = '/uscms_data/d3/mquinnan/Nano4top2/output/nanotrain/TTJets_SingleLeptFromT_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_merged_Friends.root'
# bkg_file = '/uscms_data/d3/mquinnan/Nano4top2/output/nanotrain/TTJets_DiLept_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_merged_Friends.root'
# sig_file = '/eos/uscms//store/user/mequinna/NanoAODv5/restop_training/toptrain/ttbar1l_nanotrainsamp_10kmerged_fixwgt_Friend.root'
# bkg_file = '/eos/uscms//store/user/mequinna/NanoAODv5/restop_training/toptrain/ttbar2l_nanotrainsamp_10kmerged_fixwgt_Friend.root'
# sig_file =  '/eos/uscms/store/user/lpcstop/noreplica/mequinna/training/TTJets_SingleLept_trainsample_Friend.root'
# bkg_file =  '/eos/uscms/store/user/lpcstop/noreplica/mequinna/training/TTJets_DiLept_trainsample_Friend.root'

# output file with MVA scores (for both training and testing events)
output_file = './outputfilesNANO/resTop-result-ucsb2016-flatpt-deepflavour-tt.root'
train_file  = './outputfilesNANO/resTop-training-ucsb2016-flatpt-deepflavour-tt.h5'
model_file  = './outputfilesNANO/resTop-xgb-ucsb2016-flatpt-deepflavour-tt.model'

# baseline selection
basesel = 'met>100 & njets>=5 & ndeepbjets>=1' #new baseline
# basesel = 'met>100 & njets>=5 & nbjets>=1 & nlbjets>=2' #original

# basesel = 'met>100 & njets>=5 & ndeepbjets>=1 & nldeepbjets>=2'
sig_sel = basesel + ' & flag_signal'
#bkg_sel = basesel + ' & (~ flag_signal) & (event%4==0)'
bkg_sel = basesel + ' & (~ flag_signal)'
wgtvar = 'weight'

# observer vars: not used in training but will be saved in the hdf5 file
obs_vars = [
 'weight',
 'njets',
 'nbjets',
 'ndeepbjets',
 'npv',
 'topcand_pt',
 'flag_signal',
]

# training vars: float
varsF = [
    'var_b_mass', #static
    'var_topcand_mass',
    'var_topcand_ptDR',
    'var_wcand_mass',
    'var_wcand_ptDR',
    'var_b_j2_mass',
    'var_b_j3_mass',
    'var_sd_n2',
    'var_b_btagDeepFlavB',#nanomod version
    'var_j2_btagDeepFlavB',
    'var_j3_btagDeepFlavB',
    'var_j2_qgl',
    'var_j2_btagDeepFlavC',
    'var_j3_qgl',
    'var_j3_btagDeepFlavC'
    ]

# training vars: int
varsI = [
    'var_j2_nConstituents', #nanomod version
    'var_j3_nConstituents'
    ]
input_vars = varsF + varsI
#input_vars = varsF

# preprocessing of the samples

reweight_by = 'topcand_pt'
reweight_bins = [0, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 
                 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850, 875, 900, 950, 1000, 2000, 3000, 5000]
#reweight_bins = [200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 5000]
reweighted_var = 'reweight'
do_reweighting = True

def prepSamp(filepath, is_signal):
    if is_signal:
        selection, target = sig_sel, 1
    else:
        selection, target = bkg_sel, 0
    df = pd.DataFrame(root2array(filepath, treename='Friends'))
    print selection
    print 'df', df
    df.query(selection, inplace=True) #applies selection. have problem applying selection
    df = df.loc[:, input_vars + obs_vars].reset_index(drop=True)
    df['target'] = target
    if do_reweighting:
        print 'Making reweighting weights, is_signal: ', is_signal
        hist_, bin_edges = np.histogram(df.loc[:, reweight_by], bins=reweight_bins, range=(min(reweight_bins), max(reweight_bins)))
        print 'Bin edges', bin_edges
        print 'Hist', hist_
        ref_val = np.min([x for x in hist_ if x > 0])
        print 'ref_val', ref_val
        hist = np.zeros(len(hist_))
        for i in range(len(hist_)):
            #print 'hist[i]', hist[i]
            if hist_[i] != 0:
                #print 'hist[i]-in', hist[i]
                hist[i] = float(ref_val) / hist_[i]
        print 'Weights', hist
        indices = np.clip(np.digitize(df.loc[:, reweight_by], bin_edges) - 1, a_min=0, a_max=len(bin_edges) - 2)
        df[reweighted_var] = hist[indices] * df.loc[:, wgtvar]
    return df

def convertToHD5(output):
    wvar = reweighted_var if do_reweighting else wgtvar
    sig_df = prepSamp(sig_file, True)
    bkg_df = prepSamp(bkg_file, False)
    sig_sum_wgt = sig_df[wvar].sum()
    bkg_sum_wgt = bkg_df[wvar].sum()
    print 'Signal events: %d, Background events: %d' % (len(sig_df), len(bkg_df))
    print 'Signal weight: %f, Background weight: %f' % (sig_sum_wgt, bkg_sum_wgt)
    sig_df['train_weight'] = sig_df[wvar] * (1000. / sig_sum_wgt)
    bkg_df['train_weight'] = bkg_df[wvar] * (1000. / bkg_sum_wgt)
    print 'After normalization:'
    print 'Signal weight: %f, Background weight: %f' % (sig_df['train_weight'].sum(), bkg_df['train_weight'].sum())
    full_df = pd.concat([sig_df, bkg_df], ignore_index=True)
    full_df.to_hdf(output, 'Friends', format='table')
    return full_df


def loadSample():
    try:
        return pd.read_hdf(train_file)
    except IOError:
        print 'Sample %s does not exist! Creating from root files...' % train_file
        return convertToHD5(train_file)


# Load samples
train_samp = loadSample()
train_samp = train_samp.sample(frac=1).reset_index(drop=True)  # shuffle the dataset

# Load variables
X = train_samp[input_vars]
y = train_samp['target']
W = train_samp['train_weight']
wgt0 = train_samp['weight']

# split samples for training and testing
test_size = 0.1  # size of the validtion sample
isTrain = np.random.uniform(size=len(train_samp)) > test_size
isTest = np.logical_not(isTrain)
train_samp['isTraining'] = isTrain

X_train, X_test = X[isTrain], X[isTest]
y_train, y_test = y[isTrain], y[isTest]
W_train, W_test = W[isTrain], W[isTest]
wgt0_train, wgt0_test = wgt0[isTrain], wgt0[isTest]

# from sklearn.cross_validation import train_test_split
# index = np.arange(len(y))
# idx_train, idx_test, X_train, X_test, W_train, W_test, wgt0_train, wgt0_test, y_train, y_test = train_test_split(index, X, W, wgt0, y, test_size=0.3)
# X_train, X_val, W_train, W_val, y_train, y_val = train_test_split(X_dev, W_dev, y_dev, test_size=0.2)

# load into xgboost DMatrix
import xgboost as xgb
d_train = xgb.DMatrix(X_train, label=y_train, weight=W_train)
d_test = xgb.DMatrix(X_test, label=y_test, weight=W_test)

# setup parameters for xgboost
param = {}
param['objective'] = 'binary:logistic'
param['eval_metric'] = ['error', 'auc', 'logloss']
param['min_child_weight'] = 0
# param['gamma']=0.01
param['eta'] = 0.05 ##keep 0.05
param['max_depth'] = 6
# param['colsample_bytree'] = 0.8
# param['subsample'] = 0.8

print 'Starting training...'
print 'Using %d vars:' % len(input_vars)
print input_vars

watchlist = [ (d_train, 'train'), (d_test, 'eval') ]
num_round = 2000  # max allowed no. of trees

# start training
bst = xgb.train(param, d_train, num_round, watchlist, early_stopping_rounds=20)

# save the model
bst.save_model(model_file)

# print out variable ranking
scores = bst.get_score()
ivar = 1
for k in sorted(scores, key=scores.get, reverse=True):
    print "%2d. %24s: %s" % (ivar, k, str(scores[k]))
    ivar = ivar + 1

# fill MVA scores and make root file
dmat = xgb.DMatrix(X, label=y)
preds = bst.predict(dmat)
train_samp['score'] = preds

print('Write prediction file to %s' % output_file)
array2root(train_samp.to_records(index=False), filename=output_file, treename='Friends', mode='RECREATE')


####### convert to TMVA compatible XML file #######
model = bst.get_dump()

import re
import xml.etree.cElementTree as ET
regex_float_pattern = r'[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?'

def build_tree(xgtree, base_xml_element, var_indices):
    parent_element_dict = {'0':base_xml_element}
    pos_dict = {'0':'s'}
    for line in xgtree.split('\n'):
        if not line: continue
#         print line
#         print parent_element_dict
#         print pos_dict
        if ':leaf=' in line:
            # leaf node
            result = re.match(r'(\t*)(\d+):leaf=({0})$'.format(regex_float_pattern), line)
            if not result:
                print line
            depth = result.group(1).count('\t')
            inode = result.group(2)
            res = result.group(3)
            node_elementTree = ET.SubElement(parent_element_dict[inode], "Node", pos=str(pos_dict[inode]),
                                             depth=str(depth), NCoef="0", IVar="-1", Cut="0.0e+00", cType="1", res=str(res), rms="0.0e+00", purity="0.0e+00", nType="-99")
        else:
            # \t\t3:[var_topcand_mass<138.19] yes=7,no=8,missing=7
            result = re.match(r'(\t*)([0-9]+):\[(?P<var>.+)<(?P<cut>{0})\]\syes=(?P<yes>\d+),no=(?P<no>\d+)'.format(regex_float_pattern), line)
            if not result:
                print line
            depth = result.group(1).count('\t')
            inode = result.group(2)
            var = result.group('var')
            cut = result.group('cut')
            lnode = result.group('yes')
            rnode = result.group('no')
            pos_dict[lnode] = 'l'
            pos_dict[rnode] = 'r'
            node_elementTree = ET.SubElement(parent_element_dict[inode], "Node", pos=str(pos_dict[inode]),
                                             depth=str(depth), NCoef="0", IVar=str(var_indices[var]), Cut=str(cut),
                                             cType="1", res="0.0e+00", rms="0.0e+00", purity="0.0e+00", nType="0")
            parent_element_dict[lnode] = node_elementTree
            parent_element_dict[rnode] = node_elementTree

def convert_model(model, input_variables, output_xml):
    NTrees = len(model)
    var_list = input_variables
    var_indices = {}

    # <MethodSetup>
    MethodSetup = ET.Element("MethodSetup", Method="BDT::BDT")

    # <Variables>
    Variables = ET.SubElement(MethodSetup, "Variables", NVar=str(len(var_list)))
    for ind, val in enumerate(var_list):
        name = val[0]
        var_type = val[1]
        var_indices[name] = ind
        Variable = ET.SubElement(Variables, "Variable", VarIndex=str(ind), Type=val[1],
            Expression=name, Label=name, Title=name, Unit="", Internal=name,
            Min="0.0e+00", Max="0.0e+00")

    # <GeneralInfo>
    GeneralInfo = ET.SubElement(MethodSetup, "GeneralInfo")
    Info_Creator = ET.SubElement(GeneralInfo, "Info", name="Creator", value="xgboost2TMVA")
    Info_AnalysisType = ET.SubElement(GeneralInfo, "Info", name="AnalysisType", value="Classification")

    # <Options>
    Options = ET.SubElement(MethodSetup, "Options")
    Option_NodePurityLimit = ET.SubElement(Options, "Option", name="NodePurityLimit", modified="No").text = "5.00e-01"
    Option_BoostType = ET.SubElement(Options, "Option", name="BoostType", modified="Yes").text = "Grad"

    # <Weights>
    Weights = ET.SubElement(MethodSetup, "Weights", NTrees=str(NTrees), AnalysisType="1")

    for itree in range(NTrees):
        BinaryTree = ET.SubElement(Weights, "BinaryTree", type="DecisionTree", boostWeight="1.0e+00", itree=str(itree))
        build_tree(model[itree], BinaryTree, var_indices)

    tree = ET.ElementTree(MethodSetup)
    tree.write(output_xml)


use_vars = [(k, 'F') for k in varsF] + [(k, 'I') for k in varsI]
#use_vars = [(k, 'F') for k in varsF]

convert_model(model, input_variables=use_vars, output_xml='xgboost-NANO.xml')

####### convert to TMVA compatible XML file #######


# plot roc curve

def plotROC(y_score, X_input, y_true, sample_weight=None):
    from sklearn.metrics import auc, roc_curve
    fpr, tpr, _ = roc_curve(y_true, y_score, sample_weight=sample_weight)
    roc_auc = auc(fpr, tpr, reorder=True)

#     fpr_tau21, tpr_tau21, _ = roc_curve(y_true, 1-X_input['fj_tau21'], sample_weight=sample_weight)
#     tau21_auc = auc(fpr_tau21, tpr_tau21, sample_weight=sample_weight)

    plt.figure()
    plt.plot(tpr, 1 - fpr, label='MVA (area = %0.3f)' % roc_auc)
#     plt.plot(tpr_tau21, fpr_tau21, label=r'$\tau_{21}$ (area = %0.3f)' % tau21_auc)
    plt.plot([0, 1], [1, 0], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Tagging eff.')
    plt.ylabel('Mistag rate')
#     plt.title('Receiver operating characteristic example')
    plt.legend(loc='best')
    plt.grid()


plotROC(bst.predict(d_test), X_test, y_test, wgt0_test)  # here we use the natural weight as in the sample
plt.ylim(0.8, 1)
plt.savefig('roc_xgb_nano_nvar%d.pdf' % len(input_vars))

