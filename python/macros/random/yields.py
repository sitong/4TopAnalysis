#gets event yield
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
# import re
import ROOT as r
# from ROCStuff import ROCCurve
# import RootStuff as rs


def makeroot(infile, treename, options="read"):
    rfile = r.TFile(infile, options)
    tree = rfile.Get(treename)
    return rfile, tree

#unweighted yield: NEnt. weighted yield: rate
def getyield(tree, cutstring, projvar='ht', lumi='36.0'):
    htmp = r.TH1D('htmp','htmp',1,0,100000)
    # cutstr = '('+str(lumi)+'*'+weight+')*('+cutstr+')'
    print 'CUTSTRING', "("+cutstring+")"
    NEnt = tree.Draw(projvar+">>htmp", cutstring)
    rate = htmp.Integral(0,2)
    # print 'file:', infile, 'cutstring:', cutstring, 'lumi', lumi
    print "raw event #:", NEnt, 'yield:', rate
    return NEnt, rate

# infiles = [

    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/JETHT_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/TTTT_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/TTX_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/TT_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/dibosons_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/singletop_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/QCD_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/DY_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/wjets_2016_merged_0lepSF_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/zjets_2016_merged_0lepSF_tree.root',

infiles16 = [

    '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintreeswithsysts/JETHT_2016_merged_0lep_noskim_tree.root',
 #    '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/JETHT_2016_merged_0lep_tree.root',

#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/TTTT_2016_merged_0lep_tree.root',
#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/TT_2016_merged_0lep_tree.root',
#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/QCD_2016_merged_0lep_tree.root',
#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/TTX_2016_merged_0lep_tree.root',
#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/wjets_2016_merged_0lep_tree.root',
#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/zjets_2016_merged_0lep_tree.root',
#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/singletop_2016_merged_0lep_tree.root',
#     '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/dibosons_2016_merged_0lep_tree.root',]
    ]
infiles17 = [

    '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintreeswithsysts/JETHT_2017_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/JETHT_2017_merged_0lep_tree.root',

    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/TTTT_2017_merged_0lep_tree.root', #w/ psw
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/TT_2017_merged_0lep_tree.root', #corrected
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/QCD_2017_merged_0lep_tree.root', #corrected
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/TTX_2017_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/wjets_2017_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/zjets_2017_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/singletop_2017_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/dibosons_2017_merged_0lep_tree.root',]
    ]
infiles18 = [

    '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintreeswithsysts/JETHT_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/JETHT_2018_merged_0lep_tree.root',

    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/TTTT_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/TT_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/QCD_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/TTX_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/wjets_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/zjets_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/singletop_2018_merged_0lep_tree.root',
    # '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/dibosons_2018_merged_0lep_tree.root',
    ]

treename = 'Events'
projvar = 'ht'
year = 2016

lumi = 0
infiles = []
if year==2016:
    lumi = '35.92'
    infiles=infiles16
elif year==2017:
    lumi = '41.53'
    infiles=infiles17
elif year==2018:
    lumi = '59.74'
    infiles=infiles18

# cutstr= [
metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'

cutstr = [
    '(njets>=9 && ht>=700 && nbjets>=3 && (nleptons==0))*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi,
    '(njets>=9 && ht>=700 && nbjets>=3 && nrestops>=1 && (nleptons==0) )*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi, ]
    # '(njets>=9 && ht>=700 && nbjets>=3 && nrestops==1 && nbstops==0 && (nVRleps>=1 && nVRleps<4) )*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi, 
    # '(njets>=9 && ht>=700 && nbjets>=3 && nrestops==1 && nbstops>=1 && (nVRleps>=1 && nVRleps<4) )*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi, 
    # '(njets>=9 && ht>=700 && nbjets>=3 && nrestops>=2 && nbstops>=0 && (nVRleps>=1 && nVRleps<4) )*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi, ]
    #data 16
cutstr16 = [
    '(njets_nosys>=9 && ht_nosys>=700 && nbjets_nosys>=3 && (nfunky_leptons==0) ) && (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
    '(njets_nosys>=9 && ht_nosys>=700 && nbjets_nosys>=3 && nrestops_nosys>=1 && (nfunky_leptons==0) )&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,]
    # '(njets>=9 && ht>=700 && nbjets>=3 && nrestops>=1 && nbstops==0 && (nVRleps>=1 && nVRleps<4) )&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
    # '(njets>=9 && ht>=700 && nbjets>=3 && nrestops>=1 && nbstops>=1 && (nVRleps>=1 && nVRleps<4) )&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
    # '(njets>=9 && ht>=700 && nbjets>=3 && nrestops>=1 && nbstops>=0 && (nVRleps>=1 && nVRleps<4) )&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,]
    #data 17
cutstr17 = [
    '(njets_nosys>=9 && ht_nosys>=700 && nbjets_nosys>=3 && (nfunky_leptons==0))&& (HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || HLT_PFHT430_SixJet40_BTagCSV_p080 || HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || HLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,
    '(njets_nosys>=9 && ht_nosys>=700 && nbjets_nosys>=3 && (nfunky_leptons==0) && nrestops_nosys>=1) && (HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || HLT_PFHT430_SixJet40_BTagCSV_p080 || HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || HLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,]
    # #data 18
cutstr18 = [
    '(njets_nosys>=9 && ht_nosys>=700 && nbjets_nosys>=3 && (nfunky_leptons==0))&& (HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,
    '(njets_nosys>=9 && ht_nosys>=700 && nbjets_nosys>=3 && (nfunky_leptons==0) && nrestops_nosys>=1)&& (HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,]

    #1l tt
    # '(njets>=9 && ht>=700 && nbjets>=3 && nleptons==0)'
    # '(nleptons==1 && nbjets>=1 && njets>=4 && rescand_disc>-1.0) && HLT_Ele27_WPTight_Gsf',
    # '(nleptons==1 && nbjets>=1 && njets>=4 && rescand_disc>-1.0) && (HLT_IsoMu24 || HLT_IsoTkMu24)',
    # '(nleptons==1 && nbjets>=1 && njets>=4 && rescand_disc>-1.0)*weight*evtbtagSFshape_preselJets*puWeight*35.917482079'
    #wjets
    # '(nleptons==1 && nbjets==0 && njets>=4 && ht>=700 && rescand_disc>-1.0) && HLT_Ele27_WPTight_Gsf',
    # '(nleptons==1 && nbjets==0 && njets>=4 && ht>=700 && rescand_disc>-1.0) && (HLT_IsoMu24 || HLT_IsoTkMu24)',
    # '(nleptons==1 && nbjets==0 && njets>=4 && ht>=800 && rescand_disc>-1.0)*weight*evtbtagSFshape_preselJets*puWeight*35.917482079'
    #0lep QCD 
    # '(nleptons==0 && nbjets==2 && njets>=5 && ht>=1200 && rescand_disc>-1.0) && HLT_PFHT900==1',
    # '(nleptons==0 && nbjets==2 && njets>=5 && ht>=1200 && rescand_disc>-1.0)*weight*evtbtagSFshape_preselJets*puWeight*35.917482079',
    # '(nleptons==0 && nbjets==2 && njets>=6 && ht>=1000) && HLT_PFHT900==1',
    # '(nleptons==0 && nbjets==2 && njets>=6 && ht>=1000)*weight*evtbtagSFshape_preselJets*puWeight*35.917482079',

    # ]


tot = 0
for i,cut in enumerate(cutstr):
    print '\n'
    print 'cut', cut, i
    for f in infiles:
        rf, t = makeroot(f, treename)
        print 'file:', f
        if 'JETHT' not in f:
            nent, yld = getyield(t, cut, projvar)
        elif 'JETHT' in f:
            if year==2016:
                dcut = cutstr16[i]
                nent, yld = getyield(t, dcut, projvar)
            elif year==2017:
                dcut = cutstr17[i]
                nent, yld = getyield(t, dcut, projvar)
            elif year==2018:
                dcut = cutstr18[i]
                nent, yld = getyield(t, dcut, projvar)
        if 'TTTT' not in f and 'JETHT' not in f:
            tot+=yld
        print 'updating tot', tot
    print 'tot', tot
    tot = 0
