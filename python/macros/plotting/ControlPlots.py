# easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
#creates stacked histograms with ratio plot below
#references controlhists.py and uses plottery

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-c", "--cr", dest="cr", default='CR1')
# parser.add_argument("-y", "--logy", dest="logy", default='False')
parser.add_argument("-l", "--logy", dest="logy", default='False')
parser.add_argument("-y", "--year", dest="year", default='2016')
parser.add_argument("-s", "--samp", dest="samp", default='TT') #QCD or TT
args = parser.parse_args()

logy=True
if args.logy=='True':
    logy=True
elif args.logy=='False':
    logy=False

channel = '0lep' #0lep, 1lep, 1el, 1mu 
cr = args.cr
samp = args.samp

channame = channel
if channel=='1el' or channel=='1mu' or channel=='mergey':
    channame='1lep'
# varstoplot = ['nrestops', 'ht', 'nbstops'] #
# varstoplot = ['rescand_disc']
# varstoplot = ['catBDTdisc']
varstoplot = ['rescand_pt']
# varstoplot = ['rescand_disc','rescand_pt', 'nrestops']
# varstoplot = ['lep1pt', 'lep2pt', 'nleptons', 'ht', 'bjht', 'met', 'njets', 'nbjets', 'nbstops', 'jet1pt', 'nrestops', 'restop1pt', 'leadbpt', 'sphericity', 'aplanarity', 'fwm30', 'catBDTdisc']#, 'di# scs']
# varstoplot = ['ht', 'bjht', 'met', 'njets', 'nbjets', 'nbstops', 'jet1pt', 'nrestops', 'restop1pt', 'leadbpt', 'sphericity', 'aplanarity'] 

getyields = False
# samplist = ["singletop", "dibosons", "DY",  "W+Jets",  "Z+Jets",  "ttX", "TT", "QCD", "TTTT"]
# samplist = ["TT", "QCD"]
samplist = ["MC"]
#samplist = ["TTTT", "DY",  "dibosons", "singletop",  "Z+Jets", "W+Jets", "ttX", "TT_1L", "TT_2L", "TT_0L", "QCD"]
# samplist = ["TTTT", "dibosons", "singletop",  "Z+Jets","W+Jets", "ttX", "TT", "QCD"]
# samplist = ["BOTH"]

year = args.year
lumi = str(rs.lumis[year][channel])
tn = 'Friends'


# datafiles = {'1lep': None,
#              '0lep':'/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/eventMVA/updatedrootfiles_TT/JETHT_onlytt_BDTtest_merged_tree.root' } #directory of data files

# mcfiles   = {
#     'QCD':'/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/eventMVA//updatedrootfiles/QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_0_tree_1lep_withBDT.root_1lep_withBDTplus.root'
#     }

datafiles = {'1lep': None, 
             '1el':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/1lep/SFtrees/SINGLEEL_'+year+'_merged_'+channame+'SF_tree.root',
             'mergey':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/1lep/SFtrees/SINGLELEP_'+year+'_merged_'+channame+'_tree.root',
             '1mu':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/1lep/SFtrees/SINGLEMU_'+year+'_merged_'+channame+'SF_tree.root',
             '0lep':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/SFtrees/JETHT_'+year+'_merged_'+channame+'SF_tree.root' } #directory of data files

mcfiles   = { #'+channame+'
    'MC':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/MC_'+year+'_merged_'+channame+'SF_tree.root',
    'TTTT':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/TTTT_'+year+'_merged_'+channame+'_tree.root',#_'+year+'_merged_0lep_tree.root
    'Z+Jets':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/zjets_'+year+'_merged_'+channame+'_tree.root',
    'TT':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/TT_'+year+'_merged_'+channame+'_tree.root',
    'TT_2L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/TT_'+year+'_merged_'+channame+'_tree.root',
    'TT_1L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/TT_'+year+'_merged_'+channame+'_tree.root',
    'TT_0L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/TT_'+year+'_merged_'+channame+'_tree.root',
    'QCD':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/QCD_'+year+'_merged_'+channame+'_tree.root',
    'ttX':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/TTX_'+year+'_merged_'+channame+'_tree.root',
    'singletop':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/singletop_'+year+'_merged_'+channame+'_tree.root',
    'dibosons':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/dibosons_'+year+'_merged_'+channame+'_tree.root',
    'W+Jets':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/wjets_'+year+'_merged_'+channame+'_tree.root',
    'DY':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/maintrees_newWP2/DY_'+year+'_merged_'+channame+'_tree.root'}

# datafiles = {'1lep': None, 
#              '1el':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/1lep/topSFfriends/2b/SINGLEEL_2016_merged_'+channame+'SF_tree.root',
#              'mergey':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/1lep/topSFfriends/2b/SINGLELEP_2016_merged_'+channame+'SF_tree.root',
#              '1mu':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/1lep/topSFfriends/2b/SINGLEMU_2016_merged_'+channame+'SF_tree.root',
#              '0lep':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/topSFfriends/2b/JETHT_2016_merged_'+channame+'SF_tree.root' } #directory of data files

# mcfiles   = { #'+channame+'
#     'MC':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/MC_2016_merged_'+channame+'SF_tree.root',
#     'TTTT':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/TTTT_2016_merged_'+channame+'SF_tree.root',#_2016_merged_0lep_tree.root
#     'Z+Jets':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/zjets_2016_merged_'+channame+'SF_tree.root',
#     'TT':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/TT_2016_merged_'+channame+'SF_tree.root',
#     'TT_2L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/TT_2016_merged_'+channame+'SF_tree.root',
#     'TT_1L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/TT_2016_merged_'+channame+'SF_tree.root',
#     'TT_0L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/TT_2016_merged_'+channame+'SF_tree.root',
#     'QCD':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/QCD_2016_merged_'+channame+'SF_tree.root',
#     'ttX':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/TTX_2016_merged_'+channame+'SF_tree.root',
#     'singletop':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/singletop_2016_merged_'+channame+'SF_tree.root',
#     'dibosons':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/dibosons_2016_merged_'+channame+'SF_tree.root',
#     'W+Jets':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/wjets_2016_merged_'+channame+'SF_tree.root',
#     'DY':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/'+channame+'/topSFfriends/2b/DY_2016_merged_'+channame+'SF_tree.root'}
def getyield(hist):
    errorVal = r.Double(0)
    minbin=0
    maxbin=hist.GetNbinsX()+1
    hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
    print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
    return hyield,  errorVal


def getMChists(projvar, cr, mcfiles, samplist, treename='Events', lumi=lumi, year='2016'):
    # print 'channel:', channel
    cut = sels.getcut(cr, format='mc', chan=channel,year=year, lumi=lumi)     #get correct cutstring
    if projvar=='restop1pt': #no underflow
        cut = sels.getcut(cr, format='mc', chan=channel, year=year, lumi=lumi, rtcut=True) 

    nbins, selrange = sels.getHistSettings(projvar)
    hists = []
    for i,f in enumerate(samplist):
        print i,f, mcfiles[f]
        rf, t = rs.makeroot(mcfiles[f], treename)
        
        if cr=='restopEffSF' or cr=='restopEffSFwp' or cr=='restopMissSF' or cr=='restopMissSFwp' or cr=='restopMissSFtest':
            newcut=cut
            ttstr = '('
            if f=='TT_2L':
                ttstr += '(ngenleps==2) && '
            elif f=='TT_1L':
                ttstr += '(ngenleps==1) && '
            elif f=='TT_0L':
                ttstr += '(ngenleps==0) && '
            # if f=='TT_2L':
            #     ttstr += '((ngenels==1 &&ngenmus==1) || (ngenels==2 &&ngenmus==0) || (ngenels==0 &&ngenmus==2)) && '
            # elif f=='TT_1L':
            #     ttstr += '((ngenels==1 && ngenmus==0) || (ngenels==0 && ngenmus==1)) && '
            # elif f=='TT_0L':
            #     ttstr += '(ngenels==0  && ngenmus==0) && '
            newcut = cut.replace("(", ttstr)
            print 'new cut:', newcut
            hist = rs.gethist(t, projvar, newcut, nbins, selrange)
        else:
            hist = rs.gethist(t, projvar, cut, nbins, selrange)
        
        if getyields:
            nevts, rate  = rs.getyield(t, cut, addweight=False) 
            print 'yield for ', samplist[i], 'is', rate, 'unweighted nevts:', nevts 
        rs.fixref(rf, hist)
        hists.append(hist)
            
    return hists

def getDatahists(projvar, cr, channel, treename='Events'):
    # print 'channel:', channel
    nbins, selrange = sels.getHistSettings(projvar)
    if channel != '1lep':
        f = datafiles[channel]
        print f, channel
        rf, t = rs.makeroot(f, treename)
        if channel == '0lep':
            cut = sels.getcut(cr, format='data', year=year, chan='0lep')    #get correct cutstring
        elif channel == 'mergey':
            cut = sels.getcut(cr, format='data', year=year,chan='1lep')    #get correct cutstring
        elif channel=='1el':
            cut = sels.getcut(cr, format='data1e', year=year,chan='1lep')
        elif channel=='1mu':
            cut = sels.getcut(cr, format='data1mu',year=year, chan='1lep')
        datahist = rs.gethist(t, projvar, cut, nbins, selrange)
        if getyields:
            nevts, rate  = rs.getyield(t, cut, addweight=False) 
            print 'yield for ', channel, 'is', rate 
        rs.fixref(rf, datahist)
        datahist.SetDirectory(0)    
        return datahist

    elif channel=='1lep':
        fe = datafiles['1el']
        fm = datafiles['1mu']
        rfe, te = rs.makeroot(fe, treename)
        rfm, tm = rs.makeroot(fm, treename)
        cut_e = sels.getcut(cr, format='data1e',  chan='1lep',year=year)
        cut_m = sels.getcut(cr, format='data1mu', chan='1lep', year=year)
        histe = rs.gethist(te, projvar, cut_e, nbins, selrange)
        histm = rs.gethist(tm, projvar, cut_m, nbins, selrange)
        rlist = r.TList()
        rlist.Add(histe)
        rlist.Add(histm)
        h = histe.Clone("hist")
        h.Reset()
        h.Merge(rlist)
        histe.SetDirectory(0)
        histm.SetDirectory(0)
        h.SetDirectory(0)
        rfe.Close()
        rfm.Close()
        return h

def makeControlHist(crname, plotvar, data, hists, labels, lumi=36, cperr_b=[], cperr_d=[]):
    
    varnames = {
    'rescand_disc': 'resolved top BDT discriminant',
    'rescand_pt': 'resolved top candidate pT',
    'ht': 'HT',
    'bjht': 'HT(bJets)', 
    'met': 'MET',
    'njets': 'N(Jets)',
    'nbjets': 'N(bJets)', 
    'nbstops': 'N(boosted tops)',
    'nleptons': 'N(leptons)',
    'jet1pt': 'PT(Lead Jet)',
    'nrestops': 'N(resolved tops)',
    'restop1pt': 'PT(Lead resolved top)',
    'leadbpt': 'PT(Lead bJet)',
    'sphericity': 'sphericity', 
    'aplanarity': 'aplanarity', 
    'fwm30': 'Fox Wolfram Moment (30)',
    'catBDTdisc' : 'eventBDT discriminant',
    'evtbdtdisc' : 'eventBDT discriminant',
    'passtrigmu': 'passtrigmu',
    'passtrigel': 'passtrigel',
    'lep1pt':'lep1pt',
    'lep2pt':'lep2pt',
    }
    colors = [r.kRed-7, r.kOrange+1, r.kYellow-7, r.kGreen-9, r.kCyan-7, r.kAzure+1, r.kBlue-9, r.kViolet-4, r.kMagenta-9]
    # colors = [r.kRed-2,r.kBlue,r.kViolet, r.kSpring, r.kTeal-1, r.kPink, r.kAzure-4, r.kCyan, r.kMagenta]# r.kYellow,r.kGreen,r.kOrange+2,
    if not os.path.exists('plots/controlhists/'+crname+'/'):
        os.makedirs('plots/controlhists/'+crname+'/')
        
    outname = 'plots/controlhists/'+crname+year+'/'+crname+'_'+year+'_'+plotvar+'.png'
    if 'closure' in crname and 'data' not in crname:
        outname = 'plots/controlhists/'+crname+year+'/'+crname+'_'+year+'_'+plotvar+'_'+args.samp+'.png'
        

    print outname
    print 'log?',logy
    print 'data',data
    print 'hists',hists
    print 'num bgs', len(hists)
    print 'colors',colors
    print 'labels',labels
    cp = []
    if cperr_b and cperr_d:
        cp = [cperr_b, cperr_d]
    ply.plot_hist( 
        data = data,
        bgs  = hists,
        # sigs = mydata,
        # syst = hsyst,
        # sig_labels = labels,#["el", "mu"],
        colors = colors, 
        legend_labels = labels,
        options = {
            #special error bars?
            # if cperr_b and cperr_d:
            "clopper_pearson":cp,
            "do_stack": True,
            "xaxis_label": varnames[plotvar],
            "yaxis_label": "Events",
            "legend_scalex": 0.7,
            "legend_scaley": 1.2,
            "bkg_sort_method":'ascending',
            # "extra_text": [plotvar],
            "yaxis_log": logy,
            "ratio_range":[0.5,1.5],
            # "ratio_pull": True,
            "hist_disable_xerrors": False,
            "show_bkg_errors": True,
            "bkg_err_fill_style":3003,
            "bkg_err_fill_color":r.kGray,
            # "palette_name": "pastel",
            "yaxis_moreloglabels": logy, 
            "legend_percentageinbox": False,
            "legend_alignment": "top right",
            "cms_label": "Preliminary",
            # "lumi_value": 36,
            "legend_smart": False,
            # "yaxis_range":[0,10],
            # "output_ic": True,
            # "us_flag": False,
            # "output_jsroot": True,
            # "output_diff_previous": True,
            "output_name": outname,
            # "output_name": 'discmiss',
            }
       )

for v in varstoplot:
    print v, cr, channel

    if cr=='closure' or cr=='extclosure':
        D='D'
        sampfile = mcfiles[samp]
        lumi=str(rs.lumis[year][channel])
        mchists  = getMChists(v, D, mcfiles, [samp], treename=tn, lumi=lumi, year=year)
        mcpred = cp.getClosure(v, cr, channel, sampfile, treename=tn, lumi=lumi, year=year)
        print 'mchist yield'
        y,e = getyield(mchists[0])
        print 'mcpred yield'
        y,e = getyield(mcpred)

        makeControlHist(cr, v, mchists[0], [mcpred], ['pred'], lumi)
        # datahist = getDatahists(v, cr, channel)
    elif cr=='dataclosure' or cr=='dataextclosure':
        D='D'
        sampfile = datafiles[channel]
        datahist = getDatahists(v, D, channel, treename=tn)
        datapred = cp.getClosure(v, cr, channel, sampfile, treename=tn,lumi=str(rs.lumis[year][channel]), year=year)
        # datahist=datapred
        makeControlHist(cr, v, datahist, [datapred], ['pred'], lumi=rs.lumis[year][channel])

    elif cr=='restopEffSF' or cr=='restopMissSF':
        if cr=='restopEffSF':
            datahist_num = getDatahists(v, 'restopEffSFwp', channel, treename=tn)
            mchists_num  = getMChists(v, 'restopEffSFwp', mcfiles, samplist, treename=tn)
            datahist_den = getDatahists(v, 'restopEffSF', channel, treename=tn)
            mchists_den  = getMChists(v, 'restopEffSF', mcfiles, samplist, treename=tn)
        elif cr=='restopMissSF':
            datahist_num = getDatahists(v, 'restopMissSFwp', channel, treename=tn)
            mchists_num  = getMChists(v, 'restopMissSFwp', mcfiles, samplist, treename=tn)
            datahist_den = getDatahists(v, 'restopMissSF', channel, treename=tn)
            mchists_den  = getMChists(v, 'restopMissSF', mcfiles, samplist, treename=tn)

        #divide histograms:
        datahist = datahist_num.Clone("datahist")
        datahist.Divide(datahist_num, datahist_den)
        # mchists = []
        mchist = mchists_num[0].Clone("mchist")
        if len(mchists_num)>1:
            print 'ERROR should only have one MC histogram'
        # for mchist_num, mchist_den in zip(mchists_num, mchists_den):
        mchist.Divide(mchists_num[0], mchists_den[0])
            # mchists.append(mchist)
        #clopper pearson
        CPvals_b = []; CPvals_d = []
        print 'clopper pearson errors:'
        for ibin in range(0,mchist.GetNbinsX()+2):            
            # print 'ibin', ibin
            #background
            npass_b = mchists_num[0].GetBinContent(ibin) #should be int not float
            ntotal_b = mchists_den[0].GetBinContent(ibin)
            # print 'MC',ntotal_b, npass_b, mchist[ibin]
            CPval_b = r.TEfficiency.ClopperPearson(abs(ntotal_b), abs(npass_b), 0.683, True) - abs(mchist[ibin]) #sometimes has negative bin vals
            CPvals_b.append(CPval_b)
            #data
            npass_d = datahist_num.GetBinContent(ibin)
            ntotal_d = datahist_den.GetBinContent(ibin)
            # print 'data',ntotal_d, npass_d, datahist[ibin]
            CPval_d = r.TEfficiency.ClopperPearson(ntotal_d, npass_d, 0.683, True) - datahist[ibin]
            CPvals_d.append(CPval_d)
    
        # print mchist, len(mchist)
        makeControlHist(cr, v, datahist, [mchist], samplist, lumi=rs.lumis[year][channel], cperr_b=CPvals_b, cperr_d=CPvals_d)

    # if cr!='extclosure' and cr!='dataextclosure' and cr!='closure' and cr!='dataclosure' and cr!='restopEffSF' :#and cr!='restopMissSF':
    else: #default
        datahist = getDatahists(v, cr, channel, treename=tn)
        mchists  = getMChists(v, cr, mcfiles, samplist, treename=tn)
        makeControlHist(cr, v, datahist, mchists, samplist, lumi=rs.lumis[year][channel])
