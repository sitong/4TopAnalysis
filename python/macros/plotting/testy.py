import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

Hfile = r.TFile('../combine/TTMC_SR/TTMC_SR_2016.root')
# Hfile = r.TFile('../combine/VRNAFwithdata/NAF_2016_error_base.root')
# Mfile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/JETHT_2016_merged_0lep_tree.root'
Mfile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/TT_2016_merged_0lep_tree.root'

metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
dstr = '&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16
mcstr = '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*35.91'

cutstr = '(nrestops==1 && nbstops==0 && ht>=700 && ht<800 && nleptons==0 && njets==8 && nbjets>=3)'+mcstr
Hhist = Hfile.Get('cut0bin0_2016_TT')
# Hhist = Hfile.Get('cut0bin0_2016_data')

def gethist(filename, cut):
    nbins, selrange = 10, [0,1]
    rf, t = rs.makeroot(filename, 'Events')
    hist = rs.gethist(t, 'catBDTdisc', cut, nbins, selrange)
    rs.fixref(rf, hist)
    return hist

def getyield(hist,  verbose=True):
    errorVal = r.Double(0)
    minbin=0
    maxbin=hist.GetNbinsX()+1
    hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
    if verbose:
        print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
    return hyield,  errorVal

Mhist = gethist(Mfile,cutstr)

c = r.TCanvas("c","c",800,800)
Mhist.SetLineColor(r.kBlue+2)
Hhist.SetLineColor(r.kRed+2)

print 'h'
yh,uh = getyield(Hhist)
print 'm'
ym,um = getyield(Mhist)

Hhist.Scale(1./yh)
Mhist.Scale(1./ym)


Hhist.Draw("hist")
Mhist.Draw("histsame")

c.SaveAs('testy.png')


