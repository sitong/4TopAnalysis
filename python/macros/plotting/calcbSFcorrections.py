#script for calculating btag SF correction factors as a function of HT and njets

import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import Defs as defs
import RootStuff as rs
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
gStyle.SetPalette(1)

parser = argparse.ArgumentParser(description='bla')
parser.add_argument("-p", "--proc", dest="proc", default='TTbb')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

#################
#WARNING: changed pd.basesel to ttbb_basesel
##################

fillfile = True
# syst = 'nosys'

systlist = ['nosys', 'jesUp', 'jesDown', 'jerUp', 'jerDown','btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down']
# systlist = ['jesUp', 'jesDown', 'jerUp', 'jerDown','btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down']
# systlist=['nosys']

for syst in systlist:
    oldsyst=syst 

    replsys = False
    if syst in ['nosys', 'jesUp',  'jesDown', 'jerUp', 'jerDown']:
        replsys=True

    outdir = './plots/bSFcorrhists/'
    year=args.year
    pd = plotdefs(year, 'SR', modbins=False)
    if args.proc=='TTbb':
        mcfile = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/python/macros/eventMVA/updatedrootfiles/TT_'+year+'_merged_0lep_tree.root'
    else:
        mcfile  = pd.mcfiles[args.proc] #TTTT, TTX or minor
    mcrf, mct = rs.makeroot(mcfile, pd.tn)
    print mcrf
    if not fillfile:
        rt,t = rs.makeroot(args.proc+'_'+args.year+'_merged_0lep_tree.root' , pd.tn)

    if replsys:
        #RTcut = ''
        # RTcut = 'nrestops_'+syst+'>=1 && '
        sel_withSF = '(nrestops_'+syst+'>=1 && '+pd.ttbb_basesel+pd.mcstr[year]
        # sel_withSF = '('+pd.ttbb_basesel_nob+pd.mcstr[year]
        sel_withSF = sel_withSF.replace('nosys', syst) #correct systematic please
        sel_noSF = sel_withSF.replace('btagSF_'+syst+'*','')
    else:
        #RTcut = ''
        # RTcut = 'nrestops_nosys>=1 && '
        sel_withSF = '(nrestops_nosys>=1 && '+pd.ttbb_basesel+pd.mcstr[year]
        # sel_withSF = '('+pd.basesel_nob+pd.mcstr[year]
        sel_withSF = sel_withSF.replace('btagSF_nosys', 'btagSF_'+syst) #correct systematic please
        sel_noSF = sel_withSF.replace('btagSF_'+syst+'*','')
    
    sel_withSFandcorr = '(nrestops_'+syst+'>=1 && '+pd.ttbb_basesel+pd.mcstr[year]+'*bSFcorr_nosys'
    # sel_withSFandcorr = '('+pd.basesel_nob+pd.mcstr[year]+'*bSFcorr_nosys'
    # sel_withSFandcorr = sel_withSFandcorr.replace('btagSF_'+syst+'*','')

    if not replsys:
        syst='nosys'

    if args.proc=='minor':
        NJbins_forplot = np.array([9, 10, 20], dtype=np.double) #minor
        NJbins = np.array([8, 9, 10,  20], dtype=np.double) #minor
        #HTbins = np.array([700.0, 800.0, 1000.0, 1200.0, 1400.0, 2000.0], dtype=np.double) #minor
        HTbins = np.array([700.0, 1000.0, 2000.0], dtype=np.double) #TTTT

    elif args.proc=='TTX' or args.proc=='TT' or args.proc=='TTbb':
        NJbins_forplot = np.array([9, 10, 20], dtype=np.double) #TTX
        NJbins = np.array([8, 9, 10,  20], dtype=np.double) #TTX
        #    HTbins = np.array([700.0, 800.0, 900.0, 1000.0, 1100.0, 1200.0, 1300.0, 1500.0, 2000.0], dtype=np.double) #TTX
        HTbins = np.array([700.0, 900.0, 1200.0, 2000.0], dtype=np.double) #TTTT

    elif args.proc=='TTTT':
        NJbins_forplot = np.array([9, 10, 11, 20], dtype=np.double) #TTTT
        NJbins = np.array([8, 9, 10, 11, 20], dtype=np.double) #TTTT 
        HTbins = np.array([700.0, 900.0, 1000.0, 1200.0, 1450.0, 2000.0], dtype=np.double) #TTTT
        

    print replsys, syst

    # NJbins_forplot = np.array([9, 10, 11, 12], dtype=np.double)
    # NJbins = np.array([8, 9, 10, 11, 12], dtype=np.double)
    # HTbins = np.array([700.0, 1000.0, 1200.0, 1400.0, 1800.0], dtype=np.double)
    
    if not fillfile:
        #for plots
        NJbins_forplot = np.array([9, 10, 11, 12], dtype=np.double)
        NJbins = np.array([9, 10, 11, 14, 20], dtype=np.double)
        HTbins = np.array([700.0, 900.00, 1100.0, 1300.0, 1500.0, 1700.0, 1900.0, 2100.0, 2300.0, 2500.0], dtype=np.double)

    NJ_before = r.TH1F('NJ_before', 'NJ_before', len(NJbins)-1, NJbins)
    HT_before = r.TH1F('HT_before', 'HT_before', len(HTbins)-1, HTbins)
    NJ_after = r.TH1F('NJ_after', 'NJ_after', len(NJbins)-1, NJbins)
    HT_after = r.TH1F('HT_after', 'HT_after', len(HTbins)-1, HTbins)
    
    #get SF
    # print mct[0], mct[0].btagSF_'+syst+'
    
    mct.Draw("njets_"+syst+">>NJ_before", sel_noSF)
    mct.Draw("njets_"+syst+">>NJ_after", sel_withSF)
    mct.Draw("ht_"+syst+">>HT_before", sel_noSF)
    mct.Draw("ht_"+syst+">>HT_after", sel_withSF)
    if not fillfile:
        NJ_corr = r.TH1F('NJ_corr', 'NJ_corr', len(NJbins)-1, NJbins)
        HT_corr = r.TH1F('HT_corr', 'HT_corr', len(HTbins)-1, HTbins)
        t.Draw("ht_"+syst+">>HT_corr", sel_withSFandcorr)
        t.Draw("njets_"+syst+">>NJ_corr", sel_withSFandcorr)

    def getratioplot(varname, beforehist, afterhist, corrhist):
        #varname is HT or NJ
        histname = varname+'_'+args.proc+'_'+year+'_'+'btagcorrhist'

        #top plot
        c = r.TCanvas("c","c",800,800)
        p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        p1.SetBottomMargin(0.1)
        p1.SetGridx()
        p1.SetGridy()
        p1.Draw()
        p1.cd()
        legend = r.TLegend(0.5, 0.7, 0.9, 0.9)
        
        beforehist.SetLineWidth(2)
        beforehist.SetMarkerStyle(20)
        beforehist.SetLineColor(r.kBlue+2)
        afterhist.SetLineWidth(2)
        afterhist.SetMarkerStyle(20)
        afterhist.SetLineColor(r.kRed+2)
        corrhist.SetLineWidth(2)
        corrhist.SetMarkerStyle(20)
        corrhist.SetLineColor(r.kGreen+1)
        
        beforehist.Draw("hist")
        afterhist.Draw("histsame")
        corrhist.Draw("histsame")
        legend.AddEntry(beforehist, "no bSFs", "L")           
        legend.AddEntry(afterhist, "with bSFs", "L")           
        legend.AddEntry(corrhist, "with bSFs + correction", "L")           
        gPad.Modified()
        gPad.Update()
        ymax = p1.GetUymax()*1.3 #max+30%
        beforehist.SetMaximum(ymax)
    
        beforehist.GetYaxis().SetTitleSize(20)
        beforehist.GetYaxis().SetTitleFont(43)
        beforehist.GetYaxis().SetTitleOffset(1.55)
        beforehist.GetYaxis().SetTitle('NEntries')
        beforehist.GetXaxis().SetTitle(varname)
        beforehist.GetYaxis().SetLabelFont(43)
        beforehist.GetYaxis().SetLabelSize(15)

        ratio = afterhist.Clone("ratio")
        ratio.Divide(beforehist)
        ratio2 = corrhist.Clone("ratio2")
        ratio2.Divide(beforehist)

        #multiply sfhist by ratio
        # corrhist = afterhist.Clone("corrhist")
        # corrhist.Multiply(ratio)
        # corrhist.SetLineWidth(2)
        # corrhist.SetMarkerStyle(20)
        # corrhist.SetLineColor(r.kGreen+2)
        # legend.AddEntry(corrhist, "corrected bSFs", "L")           

        legend.Draw()
        gPad.Modified()
        gPad.Update()
    
        #bottom plot
        c.cd()
        p2 = r.TPad("p2", "p2", 0, 0.05, 1, 0.3)
        p2.SetGridx()
        p2.SetGridy()
        p2.Draw()
        p2.cd()

        ratio.SetLineColor(r.kRed+2)
        ratio.SetMarkerSize(5)
        ratio.SetLineWidth(2)
        ratio.SetMarkerStyle(1)
        
        ratio2.SetLineColor(r.kGreen+1)
        ratio2.SetMarkerSize(5)
        ratio2.SetLineWidth(2)
        ratio2.SetMarkerStyle(1)
        
        ratio.SetMinimum(0.4)
        ratio.SetMaximum(1.6)
        ratio.Draw("pe1")
        ratio2.Draw("pe1same")

        ratio.SetTitle("")
        ratio.GetYaxis().SetTitle("with SF/no SF")
        ratio.GetYaxis().SetNdivisions(505)
        ratio.GetYaxis().SetTitleSize(20)
        ratio.GetYaxis().SetTitleFont(43)
        ratio.GetYaxis().SetTitleOffset(1.55)
        ratio.GetYaxis().SetLabelFont(43)
        ratio.GetYaxis().SetLabelSize(15)

        ratio.GetXaxis().SetTitle(varname)
        ratio.GetXaxis().SetTitleSize(20)
        ratio.GetXaxis().SetTitleFont(43)
        ratio.GetXaxis().SetTitleOffset(4.0)
        ratio.GetXaxis().SetLabelFont(43)
        ratio.GetXaxis().SetLabelSize(15)
        
        l = r.TLine(p2.GetUxmin(), 1.0, p2.GetUxmax(), 1.0);
        l.SetLineColor(r.kBlack)
        l.SetLineWidth(1)
        l.Draw("same")

        p2.SetTopMargin(0.1)
        p2.SetBottomMargin(0.2)
        gPad.Modified()
        gPad.Update()
    
        print 'saved histogram: ', outdir+histname+'.png'
        c.SaveAs(outdir+histname+'.png')

    if not fillfile:
        getratioplot('HT', HT_before, HT_after, HT_corr)
        getratioplot('NJ', NJ_before, NJ_after, NJ_corr)

    # now get the 2D histograms
    # NJratio = NJ_before.Clone("NJratio")
    # NJratio.Divide(NJ_after)
    # HTratio = HT_before.Clone("HTratio")
    # HTratio.Divide(HT_after)
    
    # sel2D_noSF = sel_noSF+':'+sel_noSF
    # sel2D_withSF = sel_withSF+':'+sel_withSF
    print 'nosf', sel_noSF,'\n'
    print 'withsf', sel_withSF,'\n'
    # print 'withcorr', sel_withSFandcorr

    c2 = r.TCanvas("c","c",800,800)
    num = r.TH2D("num"+args.proc+year+'_'+oldsyst, "num"+args.proc+year+'_'+oldsyst, len(NJbins_forplot)-1, NJbins_forplot, len(HTbins)-1, HTbins)
    den = r.TH2D("den"+args.proc+year+'_'+oldsyst, "den"+args.proc+year+'_'+oldsyst, len(NJbins_forplot)-1, NJbins_forplot, len(HTbins)-1, HTbins)
    corr = r.TH2D("corr"+args.proc+year+'_'+oldsyst, "corr"+args.proc+year+'_'+oldsyst, len(NJbins_forplot)-1, NJbins_forplot, len(HTbins)-1, HTbins)
    # noSF2D = r.TH3D("noSF2D", "noSF2D", len(NJbins)-1, NJbins, len(HTbins)-1, HTbins)
    # mct.Project("noSF2D", "njets_nosys:ht_nosys")
    # mct.Project("noSF2D", "njets_nosys:ht_nosys",  sel_noSF)
    # mct.Draw("njets_nosys:ht_nosys >> noSF2D", sel_noSF, 'goff')
    # withSF2D = r.TH3D("withSF2D", "withSF2D", len(NJbins)-1, NJbins, len(HTbins)-1, HTbins)
    # # mct.Project("withSF2D", "njets_nosys:ht_nosys")
    # # mct.Project("withSF2D", "njets_nosys:ht_nosys",  sel_withSF)
    # mct.Draw("njets_nosys:ht_nosys >> withSF2D", sel_withSF, 'goff')
    # corr = noSF2D.Clone("corr")
    # corr.Divide(withSF2D)
    for ibin in range(0, corr.GetNbinsX()+1):
        # print ibin
        if ibin<len(NJbins)-1:
            njcut='(njets_'+syst+'>='+str(NJbins[ibin])+' && njets_'+syst+'<'+str(NJbins[ibin+1])+') && '
        else:
            njcut='(njets_'+syst+'>='+str(NJbins[ibin])+') && '

        sel_withSF = '('+njcut+pd.ttbb_basesel_nob+pd.mcstr[year]
        sel_noSF = sel_withSF.replace('btagSF_'+syst+'*','')

        if replsys:
            sel_withSF = sel_withSF.replace('nosys', syst) #correct systematic please
            sel_noSF = sel_withSF.replace('btagSF_'+syst+'*','')
        else:
            sel_withSF = sel_withSF.replace('btagSF_nosys', 'btagSF_'+oldsyst) #correct systematic please
            sel_noSF = sel_withSF.replace('btagSF_'+oldsyst+'*','')
        # print sel_withSF
        # print sel_noSF

        print njcut, '\n'
        binhistB = r.TH1D('binhistB', 'binhistB', len(HTbins)-1, HTbins)
        binhistA = r.TH1D('binhistA', 'binhistA', len(HTbins)-1, HTbins)
        mct.Draw("ht_"+syst+">>binhistB", sel_noSF)
        mct.Draw("ht_"+syst+">>binhistA", sel_withSF)

        binhistB.Sumw2(); binhistA.Sumw2()
        for jbin in range(0, corr.GetNbinsY()+1):
            # njbin = NJratio.GetBinContent(ibin)
            # htbin = HTratio.GetBinContent(jbin)
            # print 'nj:', NJbins[ibin], 'ht:', HTbins[jbin], 'val:', noSF2D.GetBinContent(ibin, jbin), withSF2D.GetBinContent(ibin, jbin)
            # print njbin, htbin, njbin*htbin
            # corr.SetBinContent(ibin, jbin, njbin*htbin)
            # print noSF2D.GetBinContent(ibin, jbin)
            # print withSF2D.GetBinContent(ibin, jbin)c
            
            #manually filling using 1d histograms that work...
            b4=binhistB.GetBinContent(jbin)
            af=binhistA.GetBinContent(jbin)
            b4e = binhistB.GetBinError(jbin)
            afe = binhistA.GetBinError(jbin)
            fillval = 1.0; fillerr=0.0
            if af>0.0:
                fillval = b4/af
                if b4>0.0:
                    fillerr = np.sqrt((b4e/b4)**2 + (afe/af)**2)*fillval
            print 'nj:', NJbins[ibin], 'ht:', HTbins[jbin], 'val:', b4, af, fillval
            corr.SetBinContent(ibin, jbin+1, fillval)
            num.SetBinContent(ibin, jbin, b4)
            den.SetBinContent(ibin, jbin, af)

            corr.SetBinError(ibin, jbin, fillerr)
            num.SetBinError(ibin, jbin, b4e)
            den.SetBinError(ibin, jbin, afe)

    corr.GetYaxis().SetTitle("HT")
    corr.GetXaxis().SetTitle("NJ")
    corrhistname = 'corr2Dhist'+'_'+args.proc+'_'+year
    corr.Draw("COLZTEXTERROR")
    # corr.SetAxisRange(0.0, 1.0,"Z")

    c2.SetRightMargin(0.18)
    c2.SetLeftMargin(0.18)
    gPad.Modified()
    gPad.Update()
    c2.Update()
    print 'saved histogram:', outdir+corrhistname+'.png'
    c2.SaveAs(outdir+corrhistname+'.png')

    ###########################

    cnum = r.TCanvas("c","c",800,800)
    cnum.cd()
    num.GetYaxis().SetTitle("HT")
    num.GetXaxis().SetTitle("NJ")
    numhistname = 'num2Dhist'+'_'+args.proc+'_'+year
    num.Draw("COLZTEXTERROR")
    cnum.SetRightMargin(0.18)
    cnum.SetLeftMargin(0.18)
    gPad.Modified()
    gPad.Update()
    cnum.Update()
    print 'saved histogram:', outdir+numhistname+'.png'
    cnum.SaveAs(outdir+numhistname+'.png')

    cden = r.TCanvas("c","c",800,800)
    cden.cd()
    den.GetYaxis().SetTitle("HT")
    den.GetXaxis().SetTitle("NJ")
    denhistname = 'den2Dhist'+'_'+args.proc+'_'+year
    den.Draw("COLZTEXTERROR")
    cden.SetRightMargin(0.18)
    cden.SetLeftMargin(0.18)
    gPad.Modified()
    gPad.Update()
    cden.Update()
    print 'saved histogram:', outdir+denhistname+'.png'
    cden.SaveAs(outdir+denhistname+'.png')
    
    if fillfile:
        outrf = r.TFile("bSFcorrections.root","UPDATE")
        # num.Write()
        # den.Write()
        corr.Write()
        del outrf
