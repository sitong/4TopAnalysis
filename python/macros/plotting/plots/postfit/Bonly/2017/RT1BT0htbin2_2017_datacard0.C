void RT1BT0htbin2_2017_datacard0()
{
//=========Macro generated from canvas: c/c
//=========  (Wed Aug 24 23:03:18 2022) by ROOT version 6.12/07
   TCanvas *c = new TCanvas("c", "c",0,0,1000,1000);
   gStyle->SetOptStat(0);
   c->SetHighLightColor(2);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "p1",0,0.3,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-1.125,-51.61992,10.125,464.5792);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("");
   stack->SetMaximum(393.2946);
   
   TH1F *stack_stack_3 = new TH1F("stack_stack_3","",9,0,9);
   stack_stack_3->SetMinimum(0);
   stack_stack_3->SetMaximum(412.9593);
   stack_stack_3->SetDirectory(0);
   stack_stack_3->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_3->SetLineColor(ci);
   stack_stack_3->GetXaxis()->SetTitle("BDT discriminant");
   stack_stack_3->GetXaxis()->SetLabelFont(42);
   stack_stack_3->GetXaxis()->SetLabelSize(0.035);
   stack_stack_3->GetXaxis()->SetTitleSize(0.035);
   stack_stack_3->GetXaxis()->SetTitleFont(42);
   stack_stack_3->GetYaxis()->SetTitle("NEntries");
   stack_stack_3->GetYaxis()->SetLabelFont(43);
   stack_stack_3->GetYaxis()->SetLabelSize(20);
   stack_stack_3->GetYaxis()->SetTitleSize(20);
   stack_stack_3->GetYaxis()->SetTitleOffset(1.55);
   stack_stack_3->GetYaxis()->SetTitleFont(43);
   stack_stack_3->GetZaxis()->SetLabelFont(42);
   stack_stack_3->GetZaxis()->SetLabelSize(0.035);
   stack_stack_3->GetZaxis()->SetTitleSize(0.035);
   stack_stack_3->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_3);
   
   
   TH1F *other_stack_1 = new TH1F("other_stack_1","other in RT1BT0htbin2_2017_datacard0",9,0,9);
   other_stack_1->SetBinContent(1,5.635899);
   other_stack_1->SetBinContent(2,2.994689);
   other_stack_1->SetBinContent(3,4.594686);
   other_stack_1->SetBinContent(4,2.616843);
   other_stack_1->SetBinContent(5,4.973231);
   other_stack_1->SetBinContent(6,10.93152);
   other_stack_1->SetBinContent(7,1.549428);
   other_stack_1->SetBinContent(8,2.300577);
   other_stack_1->SetBinContent(9,1.154173);
   other_stack_1->SetEntries(9);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetLineColor(ci);
   other_stack_1->GetXaxis()->SetTitle("CMS_th1x");
   other_stack_1->GetXaxis()->SetLabelFont(42);
   other_stack_1->GetXaxis()->SetLabelSize(0.035);
   other_stack_1->GetXaxis()->SetTitleSize(0.035);
   other_stack_1->GetXaxis()->SetTitleFont(42);
   other_stack_1->GetYaxis()->SetTitle("Events / ( 1 )");
   other_stack_1->GetYaxis()->SetLabelFont(42);
   other_stack_1->GetYaxis()->SetLabelSize(0.035);
   other_stack_1->GetYaxis()->SetTitleSize(0.035);
   other_stack_1->GetYaxis()->SetTitleOffset(0);
   other_stack_1->GetYaxis()->SetTitleFont(42);
   other_stack_1->GetZaxis()->SetLabelFont(42);
   other_stack_1->GetZaxis()->SetLabelSize(0.035);
   other_stack_1->GetZaxis()->SetTitleSize(0.035);
   other_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(other_stack_1,"");
   
   TH1F *TTX_stack_2 = new TH1F("TTX_stack_2","TTX in RT1BT0htbin2_2017_datacard0",9,0,9);
   TTX_stack_2->SetBinContent(1,4.22995);
   TTX_stack_2->SetBinContent(2,3.408214);
   TTX_stack_2->SetBinContent(3,3.694956);
   TTX_stack_2->SetBinContent(4,5.074917);
   TTX_stack_2->SetBinContent(5,4.390148);
   TTX_stack_2->SetBinContent(6,3.916173);
   TTX_stack_2->SetBinContent(7,3.679701);
   TTX_stack_2->SetBinContent(8,4.852468);
   TTX_stack_2->SetBinContent(9,4.167185);
   TTX_stack_2->SetEntries(9);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_2->SetLineColor(ci);
   TTX_stack_2->GetXaxis()->SetTitle("CMS_th1x");
   TTX_stack_2->GetXaxis()->SetLabelFont(42);
   TTX_stack_2->GetXaxis()->SetLabelSize(0.035);
   TTX_stack_2->GetXaxis()->SetTitleSize(0.035);
   TTX_stack_2->GetXaxis()->SetTitleFont(42);
   TTX_stack_2->GetYaxis()->SetTitle("Events / ( 1 )");
   TTX_stack_2->GetYaxis()->SetLabelFont(42);
   TTX_stack_2->GetYaxis()->SetLabelSize(0.035);
   TTX_stack_2->GetYaxis()->SetTitleSize(0.035);
   TTX_stack_2->GetYaxis()->SetTitleOffset(0);
   TTX_stack_2->GetYaxis()->SetTitleFont(42);
   TTX_stack_2->GetZaxis()->SetLabelFont(42);
   TTX_stack_2->GetZaxis()->SetLabelSize(0.035);
   TTX_stack_2->GetZaxis()->SetTitleSize(0.035);
   TTX_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(TTX_stack_2,"");
   
   TH1F *DDBKG_stack_3 = new TH1F("DDBKG_stack_3","DDBKG in RT1BT0htbin2_2017_datacard0",9,0,9);
   DDBKG_stack_3->SetBinContent(1,224.2381);
   DDBKG_stack_3->SetBinContent(2,201.3001);
   DDBKG_stack_3->SetBinContent(3,202.0517);
   DDBKG_stack_3->SetBinContent(4,182.4269);
   DDBKG_stack_3->SetBinContent(5,167.1697);
   DDBKG_stack_3->SetBinContent(6,152.4857);
   DDBKG_stack_3->SetBinContent(7,137.5411);
   DDBKG_stack_3->SetBinContent(8,120.9057);
   DDBKG_stack_3->SetBinContent(9,84.93596);
   DDBKG_stack_3->SetEntries(9);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_3->SetLineColor(ci);
   DDBKG_stack_3->GetXaxis()->SetTitle("CMS_th1x");
   DDBKG_stack_3->GetXaxis()->SetLabelFont(42);
   DDBKG_stack_3->GetXaxis()->SetLabelSize(0.035);
   DDBKG_stack_3->GetXaxis()->SetTitleSize(0.035);
   DDBKG_stack_3->GetXaxis()->SetTitleFont(42);
   DDBKG_stack_3->GetYaxis()->SetTitle("Events / ( 1 )");
   DDBKG_stack_3->GetYaxis()->SetLabelFont(42);
   DDBKG_stack_3->GetYaxis()->SetLabelSize(0.035);
   DDBKG_stack_3->GetYaxis()->SetTitleSize(0.035);
   DDBKG_stack_3->GetYaxis()->SetTitleOffset(0);
   DDBKG_stack_3->GetYaxis()->SetTitleFont(42);
   DDBKG_stack_3->GetZaxis()->SetLabelFont(42);
   DDBKG_stack_3->GetZaxis()->SetLabelSize(0.035);
   DDBKG_stack_3->GetZaxis()->SetTitleSize(0.035);
   DDBKG_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(DDBKG_stack_3,"");
   stack->Draw("hist");
   
   TH1F *datahist__9 = new TH1F("datahist__9","datahist",9,0,9);
   datahist__9->SetBinContent(1,257);
   datahist__9->SetBinContent(2,192);
   datahist__9->SetBinContent(3,197);
   datahist__9->SetBinContent(4,203);
   datahist__9->SetBinContent(5,190);
   datahist__9->SetBinContent(6,165);
   datahist__9->SetBinContent(7,147);
   datahist__9->SetBinContent(8,120);
   datahist__9->SetBinContent(9,94);
   datahist__9->SetBinError(1,16.54445);
   datahist__9->SetBinError(2,14.37175);
   datahist__9->SetBinError(3,14.55081);
   datahist__9->SetBinError(4,14.76272);
   datahist__9->SetBinError(5,14.29948);
   datahist__9->SetBinError(6,13.36181);
   datahist__9->SetBinError(7,12.64194);
   datahist__9->SetBinError(8,11.47396);
   datahist__9->SetBinError(9,10.21747);
   datahist__9->SetEntries(9);
   datahist__9->SetStats(0);

   ci = TColor::GetColor("#666666");
   datahist__9->SetLineColor(ci);
   datahist__9->SetLineWidth(2);
   datahist__9->SetMarkerStyle(20);
   datahist__9->GetXaxis()->SetLabelFont(42);
   datahist__9->GetXaxis()->SetLabelSize(0.035);
   datahist__9->GetXaxis()->SetTitleSize(0.035);
   datahist__9->GetXaxis()->SetTitleFont(42);
   datahist__9->GetYaxis()->SetLabelFont(42);
   datahist__9->GetYaxis()->SetLabelSize(0.035);
   datahist__9->GetYaxis()->SetTitleSize(0.035);
   datahist__9->GetYaxis()->SetTitleOffset(0);
   datahist__9->GetYaxis()->SetTitleFont(42);
   datahist__9->GetZaxis()->SetLabelFont(42);
   datahist__9->GetZaxis()->SetLabelSize(0.035);
   datahist__9->GetZaxis()->SetTitleSize(0.035);
   datahist__9->GetZaxis()->SetTitleFont(42);
   datahist__9->Draw("psame");
   
   TH1F *stacktot__10 = new TH1F("stacktot__10","stacktot",9,0,9);
   stacktot__10->SetBinContent(1,234.1039);
   stacktot__10->SetBinContent(2,207.703);
   stacktot__10->SetBinContent(3,210.3414);
   stacktot__10->SetBinContent(4,190.1186);
   stacktot__10->SetBinContent(5,176.5331);
   stacktot__10->SetBinContent(6,167.3333);
   stacktot__10->SetBinContent(7,142.7702);
   stacktot__10->SetBinContent(8,128.0587);
   stacktot__10->SetBinContent(9,90.25732);
   stacktot__10->SetEntries(27);
   stacktot__10->SetStats(0);

   ci = TColor::GetColor("#666666");
   stacktot__10->SetFillColor(ci);
   stacktot__10->SetFillStyle(3002);

   ci = TColor::GetColor("#000099");
   stacktot__10->SetLineColor(ci);
   stacktot__10->SetLineWidth(0);
   stacktot__10->GetXaxis()->SetLabelFont(42);
   stacktot__10->GetXaxis()->SetLabelSize(0.035);
   stacktot__10->GetXaxis()->SetTitleSize(0.035);
   stacktot__10->GetXaxis()->SetTitleFont(42);
   stacktot__10->GetYaxis()->SetLabelFont(42);
   stacktot__10->GetYaxis()->SetLabelSize(0.035);
   stacktot__10->GetYaxis()->SetTitleSize(0.035);
   stacktot__10->GetYaxis()->SetTitleOffset(0);
   stacktot__10->GetYaxis()->SetTitleFont(42);
   stacktot__10->GetZaxis()->SetLabelFont(42);
   stacktot__10->GetZaxis()->SetLabelSize(0.035);
   stacktot__10->GetZaxis()->SetTitleSize(0.035);
   stacktot__10->GetZaxis()->SetTitleFont(42);
   stacktot__10->Draw("e2same");
   
   TLegend *leg = new TLegend(0.1,0.6,0.3,0.9,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("other_stack_1","other","F");

   ci = TColor::GetColor("#d07e93");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#d07e93");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTTT","TTTT","F");

   ci = TColor::GetColor("#a1794a");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#a1794a");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTX_stack_2","TTX","F");

   ci = TColor::GetColor("#163d4e");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#163d4e");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("datahist","data","L");

   ci = TColor::GetColor("#666666");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("DDBKG_stack_3","DDBKG","F");

   ci = TColor::GetColor("#c1caf3");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#c1caf3");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "p2",0,0.05,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-1.125,0.05714285,10.125,1.771429);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetGridy();
   p2->SetBottomMargin(0.2);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   
   TH1F *raterr__11 = new TH1F("raterr__11","",9,0,9);
   raterr__11->SetBinContent(0,1);
   raterr__11->SetBinContent(1,1);
   raterr__11->SetBinContent(2,1);
   raterr__11->SetBinContent(3,1);
   raterr__11->SetBinContent(4,1);
   raterr__11->SetBinContent(5,1);
   raterr__11->SetBinContent(6,1);
   raterr__11->SetBinContent(7,1);
   raterr__11->SetBinContent(8,1);
   raterr__11->SetBinContent(9,1);
   raterr__11->SetBinContent(10,1);
   raterr__11->SetMinimum(0.4);
   raterr__11->SetMaximum(1.6);
   raterr__11->SetEntries(20);
   raterr__11->SetStats(0);

   ci = TColor::GetColor("#666666");
   raterr__11->SetFillColor(ci);
   raterr__11->SetFillStyle(3002);

   ci = TColor::GetColor("#666666");
   raterr__11->SetLineColor(ci);
   raterr__11->SetLineWidth(0);
   raterr__11->GetXaxis()->SetTitle("BDT discriminant");
   raterr__11->GetXaxis()->SetLabelFont(43);
   raterr__11->GetXaxis()->SetLabelSize(20);
   raterr__11->GetXaxis()->SetTitleSize(20);
   raterr__11->GetXaxis()->SetTitleOffset(4);
   raterr__11->GetXaxis()->SetTitleFont(43);
   raterr__11->GetYaxis()->SetTitle("ratio data/prediction");
   raterr__11->GetYaxis()->SetNdivisions(505);
   raterr__11->GetYaxis()->SetLabelFont(43);
   raterr__11->GetYaxis()->SetLabelSize(20);
   raterr__11->GetYaxis()->SetTitleSize(20);
   raterr__11->GetYaxis()->SetTitleOffset(1.55);
   raterr__11->GetYaxis()->SetTitleFont(43);
   raterr__11->GetZaxis()->SetLabelFont(42);
   raterr__11->GetZaxis()->SetLabelSize(0.035);
   raterr__11->GetZaxis()->SetTitleSize(0.035);
   raterr__11->GetZaxis()->SetTitleFont(42);
   raterr__11->Draw("e2");
   
   TH1F *ratio__12 = new TH1F("ratio__12","datahist",9,0,9);
   ratio__12->SetBinContent(1,1.097803);
   ratio__12->SetBinContent(2,0.9243968);
   ratio__12->SetBinContent(3,0.9365727);
   ratio__12->SetBinContent(4,1.067754);
   ratio__12->SetBinContent(5,1.076286);
   ratio__12->SetBinContent(6,0.9860557);
   ratio__12->SetBinContent(7,1.029627);
   ratio__12->SetBinContent(8,0.9370703);
   ratio__12->SetBinContent(9,1.041467);
   ratio__12->SetBinError(1,0.06437529);
   ratio__12->SetBinError(2,0.07485287);
   ratio__12->SetBinError(3,0.073862);
   ratio__12->SetBinError(4,0.07272277);
   ratio__12->SetBinError(5,0.0752604);
   ratio__12->SetBinError(6,0.08098067);
   ratio__12->SetBinError(7,0.0859996);
   ratio__12->SetBinError(8,0.09561632);
   ratio__12->SetBinError(9,0.1086965);
   ratio__12->SetEntries(1330.086);
   ratio__12->SetStats(0);
   ratio__12->SetLineWidth(2);
   ratio__12->SetMarkerStyle(20);
   ratio__12->GetXaxis()->SetLabelFont(42);
   ratio__12->GetXaxis()->SetLabelSize(0.035);
   ratio__12->GetXaxis()->SetTitleSize(0.035);
   ratio__12->GetXaxis()->SetTitleFont(42);
   ratio__12->GetYaxis()->SetLabelFont(42);
   ratio__12->GetYaxis()->SetLabelSize(0.035);
   ratio__12->GetYaxis()->SetTitleSize(0.035);
   ratio__12->GetYaxis()->SetTitleOffset(0);
   ratio__12->GetYaxis()->SetTitleFont(42);
   ratio__12->GetZaxis()->SetLabelFont(42);
   ratio__12->GetZaxis()->SetLabelSize(0.035);
   ratio__12->GetZaxis()->SetTitleSize(0.035);
   ratio__12->GetZaxis()->SetTitleFont(42);
   ratio__12->Draw("pe0same");
   TLine *line = new TLine(0,1,1,1);
   line->Draw();
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
