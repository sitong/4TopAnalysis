#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
from collections import OrderedDict
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
import yieldtable as yt

parser = argparse.ArgumentParser(description='yield table')
parser.add_argument("-y", "--year", dest="year", default='2018')
parser.add_argument("-r", "--region", dest="region", default='VR')
args = parser.parse_args()

region = args.region
year = args.year
pd = plotdefs(year, args.region, False)
var='ht'
treename = 'mytree'

drf, dt = rs.makeroot(pd.datafilename, pd.tn)

if region =='SR':
    rf1, t1 = rs.makeroot('240k_SR1.root', treename)
    rf2, t2 = rs.makeroot('240k_SR2.root', treename)
    rf3, t3 = rs.makeroot('240k_SR3.root', treename)
elif region =='VR':
    rf1, t1 = rs.makeroot('240k_VR1.root', treename)
    rf2, t2 = rs.makeroot('240k_VR2.root', treename)
    rf3, t3 = rs.makeroot('240k_VR3.root', treename)


# rs.fixref(rf1, t1)
# rs.fixref(rf2, t2)
# rs.fixref(rf3, t3)

trees = [t1, t2, t3]
names = ['RT1BT0', 'RT1BT1', 'RT2']
outdir = './plots/htdists/'

dtopsels = ['&& nrestops_nosys==1 && nbstops_nosys==0', '&& nrestops_nosys==1 && nbstops_nosys>=1', '&& nrestops_nosys>=2 && nbstops_nosys>=0']

for i,t in enumerate(trees):

    trigsels = ' && (trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2) && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'
    trigsels = ''
    
    if region=='SR':
        selb2j8 = 'nbJet==2 && nJet==8 && ht>=700'
        selb2j9 = 'nbJet==2 && nJet==9 && ht>=700'
        selb3j8 = 'nbJet==3 && nJet==8 && ht>=700'
        selb3j9 = 'nbJet==3 && nJet==9 && ht>=700'

        dselb2j8 = '(nbjets_nosys==2 && njets_nosys==8 && ht_nosys>=700'+dtopsels[i]+')'+trigsels
        dselb2j9 = '(nbjets_nosys==2 && njets_nosys>=9 && ht_nosys>=700'+dtopsels[i]+')'+trigsels
        dselb3j8 = '(nbjets_nosys>=3 && njets_nosys==8 && ht_nosys>=700'+dtopsels[i]+')'+trigsels
        dselb3j9 = '(nbjets_nosys>=3 && njets_nosys>=9 && ht_nosys>=700'+dtopsels[i]+')'+trigsels

    elif region=='VR':
        selb2j8 = 'nbJet==2 && nJet==7 && ht>=700'
        selb2j9 = 'nbJet==2 && nJet==8 && ht>=700'
        selb3j8 = 'nbJet==3 && nJet==7 && ht>=700'
        selb3j9 = 'nbJet==3 && nJet==8 && ht>=700'

        dselb2j8 = '(nbjets_nosys==2 && njets_nosys==7 && ht_nosys>=700'+dtopsels[i]+')'+trigsels
        dselb2j9 = '(nbjets_nosys==2 && njets_nosys==8 && ht_nosys>=700'+dtopsels[i]+')'+trigsels
        dselb3j8 = '(nbjets_nosys>=3 && njets_nosys==7 && ht_nosys>=700'+dtopsels[i]+')'+trigsels
        dselb3j9 = '(nbjets_nosys>=3 && njets_nosys==8 && ht_nosys>=700'+dtopsels[i]+')'+trigsels

    b2j8 = rs.gethist(t, var, selb2j8, nbins=20, selrange=(0,2500))
    b2j9 = rs.gethist(t, var, selb2j9, nbins=20, selrange=(0,2500))
    b3j8 = rs.gethist(t, var, selb3j8, nbins=20, selrange=(0,2500))
    b3j9 = rs.gethist(t, var, selb3j9, nbins=20, selrange=(0,2500))
    
    b2j8 = rs.normhist(b2j8)
    b2j9 = rs.normhist(b2j9)
    b3j8 = rs.normhist(b3j8)
    b3j9 = rs.normhist(b3j9)

    data_b2j8 = rs.gethist(dt, 'ht_nosys', dselb2j8, nbins=20, selrange=(0,2500))
    data_b2j9 = rs.gethist(dt, 'ht_nosys', dselb2j9, nbins=20, selrange=(0,2500))
    data_b3j8 = rs.gethist(dt, 'ht_nosys', dselb3j8, nbins=20, selrange=(0,2500))
    data_b3j9 = rs.gethist(dt, 'ht_nosys', dselb3j9, nbins=20, selrange=(0,2500))

    data_b2j8 = rs.normhist(data_b2j8)
    data_b2j9 = rs.normhist(data_b2j9)
    data_b3j8 = rs.normhist(data_b3j8)
    data_b3j9 = rs.normhist(data_b3j9)

    ratio_b2j8 = data_b2j8.Clone("ratio_b2j8"); ratio_b2j8.Divide(b2j8)
    ratio_b2j9 = data_b2j9.Clone("ratio_b2j9"); ratio_b2j9.Divide(b2j9)
    ratio_b3j8 = data_b3j8.Clone("ratio_b3j8"); ratio_b3j8.Divide(b3j8)
    ratio_b3j9 = data_b3j9.Clone("ratio_b3j9"); ratio_b3j9.Divide(b3j9)

    #c2.DrawFrame(0.,0.,1.,30.); 
    c1 = r.TCanvas(); p1n1 = r.TPad("p1n1","p1n1", 0, 0.3, 1, 1.0); p1n1.SetBottomMargin(0.1); p1n1.Draw(); p1n1.cd(); 
    b2j8.Draw('histe') ; b2j8.SetTitle(names[i]+selb2j8); b2j8.SetLineWidth(2); b2j8.SetLineColor(r.kRed);  data_b2j8.Draw('histesame'); data_b2j8.SetLineWidth(2); b2j8.SetMaximum(0.4)
    gPad.Modified(); gPad.Update(); c1.cd(); p1n2 = r.TPad("p1n2", "p1n2", 0, 0.05, 1, 0.3); p1n2.Draw(); p1n2.cd(); ratio_b2j8.Draw('pe0'); ratio_b2j8.SetLineWidth(3); ratio_b2j8.SetMinimum(0.0); ratio_b2j8.SetMaximum(2.0); p1n2.SetTopMargin(0.1); p1n2.SetBottomMargin(0.2); p1n2.SetGridy()
    l1 = r.TLine(0.0, 1.0, 2500.0, 1.0); l1.SetLineColor(r.kBlack); l1.SetLineWidth(1); l1.Draw("same"); c1.cd()

    c2 = r.TCanvas(); p2n1 = r.TPad("p2n1","p2n1", 0, 0.3, 1, 1.0); p2n1.SetBottomMargin(0.1); p2n1.Draw(); p2n1.cd(); 
    b2j9.Draw('histe') ; b2j9.SetTitle(names[i]+selb2j9); b2j9.SetLineWidth(2);  b2j9.SetLineColor(r.kRed);  data_b2j9.Draw('histesame'); data_b2j9.SetLineWidth(2); b2j9.SetMaximum(0.4)
    gPad.Modified(); gPad.Update(); c2.cd(); p2n2 = r.TPad("p2n2", "p2n2", 0, 0.05, 1, 0.3);  p2n2.Draw(); p2n2.cd(); ratio_b2j9.Draw('pe0'); ratio_b2j9.SetLineWidth(3); ratio_b2j9.SetMinimum(0.0); ratio_b2j9.SetMaximum(2.0); p2n2.SetTopMargin(0.1); p2n2.SetBottomMargin(0.2); p2n2.SetGridy(); 
    l2 = r.TLine(0.0, 1.0, 2500.0, 1.0); l2.SetLineColor(r.kBlack); l2.SetLineWidth(1); l2.Draw("same"); c2.cd()

    c3 = r.TCanvas(); p3n1 = r.TPad("p3n1","p3n1", 0, 0.3, 1, 1.0); p3n1.SetBottomMargin(0.1); p3n1.Draw(); p3n1.cd(); 
    b3j8.Draw('histe') ; b3j8.SetTitle(names[i]+selb3j8); b3j8.SetLineWidth(2);  b3j8.SetLineColor(r.kRed);  data_b3j8.Draw('histesame'); data_b3j8.SetLineWidth(2); b3j8.SetMaximum(0.4)
    gPad.Modified(); gPad.Update(); c3.cd(); p3n2 = r.TPad("p3n2", "p3n2", 0, 0.05, 1, 0.3);  p3n2.Draw(); p3n2.cd(); ratio_b3j8.Draw('pe0'); ratio_b3j8.SetLineWidth(3); ratio_b3j8.SetMinimum(0.0); ratio_b3j8.SetMaximum(2.0); p3n2.SetTopMargin(0.1); p3n2.SetBottomMargin(0.2); p3n2.SetGridy();
    l3 = r.TLine(0.0, 1.0, 2500.0, 1.0); l3.SetLineColor(r.kBlack); l3.SetLineWidth(1); l3.Draw("same"); c3.cd()

    c4 = r.TCanvas(); p4n1 = r.TPad("p4n1","p4n1", 0, 0.3, 1, 1.0); p4n1.SetBottomMargin(0.1); p4n1.Draw(); p4n1.cd(); 
    b3j9.Draw('histe') ; b3j9.SetTitle(names[i]+selb3j9); b3j9.SetLineWidth(2);  b3j9.SetLineColor(r.kRed);  data_b3j9.Draw('histesame'); data_b3j9.SetLineWidth(2); b3j9.SetMaximum(0.4)
    gPad.Modified(); gPad.Update(); c4.cd(); p4n2 = r.TPad("p4n2", "p4n2", 0, 0.05, 1, 0.3);  p4n2.Draw(); p4n2.cd(); ratio_b3j9.Draw('pe0'); ratio_b3j9.SetLineWidth(3); ratio_b3j9.SetMinimum(0.0); ratio_b3j9.SetMaximum(2.0); p4n2.SetTopMargin(0.1); p4n2.SetBottomMargin(0.2); p4n2.SetGridy(); 
    l4 = r.TLine(0.0, 1.0, 2500.0, 1.0); l4.SetLineColor(r.kBlack); l4.SetLineWidth(2); l4.Draw("same"); c4.cd()

    Pads = r.TCanvas("Pads","Pads", 3000, 2100)
    Pads.Divide(2,2)
    Pads.cd(1) ; c1.DrawClonePad()
    Pads.cd(2) ; c2.DrawClonePad()
    Pads.cd(3) ; c3.DrawClonePad()
    Pads.cd(4) ; c4.DrawClonePad()

    gPad.Modified()
    gPad.Update()

    print 'saved histogram: ', outdir+names[i]+'.png'
    Pads.SaveAs(outdir+names[i]+'.png')
