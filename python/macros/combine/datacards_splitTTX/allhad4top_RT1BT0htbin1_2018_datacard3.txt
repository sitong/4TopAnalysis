## card: datacards_splitTTX/allhad4top_RT1BT0htbin1_2018_datacard3.txt number in set: 3
##selection: (nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=800 && ht_nosys<900 &&  nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*59.74*prefirewgt*

#----------------------------------------------#
#generating datacards. name: allhad4top, year2018

#number of cards in set: 12

## number of signals (i), backgrounds (j), and uncertainties (k)
---
imax: 1
jmax: 5
kmax: *
---

## input the bdt shape histograms
---
shapes * * allhad4top_2018_shapehists.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
---

## list the bin label and the number of (data) events observed
---
bin  RT1BT0htbin1_2018
observation 2516.0
---

## expected events for signal and all backgrounds in the bins
---
bin        RT1BT0htbin1_2018    RT1BT0htbin1_2018    RT1BT0htbin1_2018    RT1BT0htbin1_2018    RT1BT0htbin1_2018    RT1BT0htbin1_2018
process    TTTT    TTH    TTW    TTOther    other    DDBKG
process        0        1        2        3        4        5
rate       3.58543813974    21.3212094307    4.79813636839    9.95731323957    32.8580643535    2353.241
---

## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin
---
## statistical uncertainties

RT1BT0htbin1_2018_TTTT_stat      lnN    1.01301529912    -    -    -    -    -
RT1BT0htbin1_2018_TTH_stat      lnN    -    1.01169396692    -    -    -    -
RT1BT0htbin1_2018_TTW_stat      lnN    -    -    1.16679630676    -    -    -
RT1BT0htbin1_2018_TTOther_stat      lnN    -    -    -    1.26804571564    -    -
RT1BT0htbin1_2018_other_stat      lnN    -    -    -    -    1.09102348017    -
RT1BT0htbin1_2018_DDBKG_stat      lnN    -    -    -    -    -    1.04144964328

---
## systematic uncertainties

lumi_uncorr_2018   lnN    1.015    1.015    1.015    1.015    1.015    -    
lumi_corr16_17_18   lnN    1.020    1.020    1.020    1.020    1.020    -    
lumi_corr17_18   lnN    1.002    1.002    1.002    1.002    1.002    -    
elveto_2018   lnN    1.03123     1.01774     1.01774     1.01774    -    -    
muveto_2018   lnN    1.00318     1.00528     1.00528     1.00528    -    -    
cross_section   lnU   -   1.50   1.23   1.23     -    -    
data_pred_norm_disagreement_2018    lnN        -      -         -         -         -      1.060
data_pred_shape_disagreement_2018  shape   -    -    -    -    -    1.0  

---
## shape uncertainties

jes_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
jer_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
pileup_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
trigger_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
btagLF  shape     1.0    1.0    1.0    1.0    1.0    - 
btagHF  shape     1.0    1.0    1.0    1.0    1.0    - 
btagLFstats1_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
btagHFstats1_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
btagLFstats2_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
btagHFstats2_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
btagCFerr1  shape     1.0    1.0    1.0    1.0    1.0    - 
btagCFerr2  shape     1.0    1.0    1.0    1.0    1.0    - 
DeepAK8TopSF_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
DeepAK8WSF_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
isr_TTTT  shape     1.0    -    -   -    -    -  
isr_TTH  shape     -    1.0    -    -    -    -  
isr_TTW  shape     -    -    1.0     -    -    -  
fsr_TTTT  shape     1.0    -    -   -    -    -  
fsr_TTH  shape     -    1.0    -    -    -    -  
fsr_TTW  shape     -    -    1.0     -    -    -  
pdf  shape     1.0    1.0    1.0      -     -     - 
ME_TTTT  shape     1.0    -    -   -    -    -  
ME_TTH  shape     -    1.0    -    -    -    -  
ME_TTW  shape     -    -    1.0     -    -    -  
ResTopEff_2018   shape     1.0    1.0   1.0   1.0   1.0    - 
ResTopMiss_2018   shape     1.0    1.0   1.0   1.0   1.0    - 

---

RT1BT0htbin1_2018 autoMCStats 0 1
---

