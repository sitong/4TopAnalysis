#processing of random seed histograms
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
from collections import OrderedDict
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)


inhists = [('cut0bin0' , './2018SRs/2018SR1_700_800.root'),
           ('cut0bin1' , './2018SRs/2018SR1_800_900.root'),
           ('cut0bin2' , './2018SRs/2018SR1_900_1000.root'),
           ('cut0bin3' , './2018SRs/2018SR1_1000_1100.root'),
           ('cut0bin4' , './2018SRs/2018SR1_1100_1200.root'),
           ('cut0bin5' , './2018SRs/2018SR1_1200_1300.root'),
           ('cut0bin6' , './2018SRs/2018SR1_1300_1500.root'),
           ('cut0bin7' , './2018SRs/2018SR1_1500_20000.root'),
           ('cut1bin0' , './2018SRs/2018SR2_700_1400.root'),
           ('cut1bin1' , './2018SRs/2018SR2_1400_20000.root'),
           ('cut2bin0' , './2018SRs/2018SR3_700_1100.root'),
           ('cut2bin1' , './2018SRs/2018SR3_1100_20000.root')]

class randseedenvelope:
    def __init__(self):
        self.pd = plotdefs('2018','SR',False)
        self.outroot = './histenvelopev2_2018.root'
        self.inhists = OrderedDict(inhists)

    def gethists(self):
        shitlist = []
        for cat, fname in self.inhists.iteritems():
            rf = r.TFile(fname)
            c = r.TCanvas("c","c",800,800)
            bins_low = np.full(9, 100000).tolist()
            bins_high = np.zeros(9).tolist()
            # hist_low = r.TH1F('hist_low', 'hist_low', len(self.pd.edges)-1, self.pd.edges)
            # hist_high = r.TH1F('hist_high', 'hist_high', len(self.pd.edges)-1, self.pd.edges)
            orig = self.pd.histfromtree(cat, varname='catBDTdisc_nosys', nonorm=True)
            hist_high = orig
            hist_low = orig
            origyld, unc = self.pd.getyield(orig)
            orig.SetLineColor(r.kRed)
            orig.SetLineWidth(5)
            orig.SetTitle(cat)
            orig.Draw("hist")
            # orig.SetMaximum(1200)
            bins_high[8] = orig.GetBinContent(9) #initialize to original
            bins_low[8] = orig.GetBinContent(9)
            for key in rf.GetListOfKeys():
                # newbins = np.zeros(9).tolist()
                # print cat, h
                keyname = key.GetName()
                if keyname in shitlist:
                    print 'skipping', keyname

                elif keyname not in shitlist:
                    hist = rf.Get(keyname)
                    hist.Sumw2() #Norm
                    hist.SetLineColor(r.kGreen)
                    hist.Draw("histsame")
                    # histset[name] = h

            #get the highest and lowest last bins
                    y, u = self.pd.getyield(hist)
                    hist.Scale(origyld/y)
                    val = hist.GetBinContent(9)
                    if val<bins_low[8]:
                        bins_low[8]=val
                        hist_low = hist
                    if val>bins_high[8]:
                        bins_high[8]=val
                        hist_high = hist
                    shitlist.append(keyname)
                    # print key.GetName()
                    # print 'newbins',newbins
            print 'bins_low',bins_low
            print 'bins_high',bins_high


            #get the envelope
            #         for ibin in range(1, hist.GetNbinsX()+1):
            #             # newbins[ibin-1] = hist.GetBinContent(ibin)
            #             val = hist.GetBinContent(ibin)
            #             if val<bins_low[ibin-1]:
            #                 bins_low[ibin-1]=val
            #             if val>bins_high[ibin-1]:
            #                 bins_high[ibin-1]=val
            #         shitlist.append(keyname)
            #         # print key.GetName()
            #         # print 'newbins',newbins
            # print 'bins_low',bins_low
            # print 'bins_high',bins_high
            # for ibin in range(1, hist_low.GetNbinsX()+1):
            #     hist_low.SetBinContent(ibin, bins_low[ibin-1])
            #     hist_high.SetBinContent(ibin, bins_high[ibin-1])
            #     print ibin, bins_low[ibin-1], bins_high[ibin-1] 

            hist_low.SetLineColor(r.kBlue)
            hist_high.SetLineColor(r.kBlue)
            hist_low.SetLineWidth(3)
            hist_high.SetLineWidth(3)
            hist_low.Draw("histsame")
            hist_high.Draw("histsame")
            print 'saved histogram: ', self.pd.outdir+cat+'.png'
            c.SaveAs(self.pd.outdir+cat+'.png')

            print 'outputting histogram', self.outroot
            low = hist_low.Clone(cat+'_2018_low')
            high = hist_high.Clone(cat+'_2018_high')
            print low.GetNbinsX()

            histfile = r.TFile(self.outroot, "UPDATE")
            low.Write()
            high.Write()
            histfile.Close()
            del histfile

rse = randseedenvelope()
rse.gethists()
