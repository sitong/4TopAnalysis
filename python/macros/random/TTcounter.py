#script for counting and reweighting different TT types
from root_numpy import root2array, array2root
import numpy as np
import argparse
import pandas as pd
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
import RootStuff as rs

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['/eos/uscms//store/user/lpcstop/mquinnan_backup/NanoAODv6_trees/2016/MC/1lep/TTpieces/*.root'])
parser.add_argument("-t", "--treename", dest="treename", default='Friends')
parser.add_argument("-b", "--baseline", dest="base", default='nleptons==0 & ht>700 & njets>=7 & nbjets>=2')
args = parser.parse_args()

totevts=0
n1lep=0
n2lep=0
n0lep=0
for f in args.filenames:
    inrf, intree = rs.makeroot(f, args.treename)
    newevts =  intree.GetEntries()
    totevts+=newevts

    print "prepping input file", f
    data = pd.DataFrame(root2array(f, args.treename))
    genleps = data["ngenleps"]
    for l in genleps:
        if l==0:
            n0lep+=1
        elif l==1:
            n1lep+=1
        elif l==2:
            n2lep+=1
    
print 'nevts', totevts
print 'n1lep', n1lep
print 'n2lep', n2lep
print 'n0lep', n0lep
    
