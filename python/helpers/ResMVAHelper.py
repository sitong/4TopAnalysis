# Helper to get original ResMVA weights
# takes a model file and variable list
# Melissa Quinnan 3/15/19
import ROOT
import numpy as np
import xgboost as xgb
import logging
# import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys



    # Useful TopCand definitions and utilities
class ResMVAHelper:
    # def __init__(self):
        
    def p4(self, obj):
        fourvec=ROOT.TLorentzVector()
        fourvec.SetPtEtaPhiM(obj.pt, obj.eta, obj.phi, obj.mass)
        # fourvec.SetptEtaPhiM(self.pt, self.eta, self.phi, self.mass)
        return fourvec

    # takes 3 jets and creates a top cand
    def getTopCand(self, inB, inJ2, inJ3):
        bjet = inB
        if (inJ2.pt > inJ3.pt): 
            jet2 = inJ2
            jet3 = inJ3
        else:
            jet2 = inJ3
            jet3 = inJ2
        tcandp4 = self.p4(bjet) + self.p4(jet2) + self.p4(jet3)
        topcand = [tcandp4, bjet, jet2, jet3, -1.0] #-1 placeholder for disc value
        return np.array(topcand) # maybe these should be classes rather than np arrays. OR they have a zero in place of disc?

    def getWCand(self, inJ2, inJ3): 
        if (inJ2.pt > inJ3.pt): 
            jet2 = inJ2
            jet3 = inJ3
        else:
            jet2 = inJ3
            jet3 = inJ2
        wcandp4 = self.p4(jet2) + self.p4(jet3)
        wcand = [wcandp4, jet2, jet3]
        return np.array(wcand)
        #better to make it depend on topcand?

    def WfromTop(topcand):
        wcand = np.array([ self.p4(topcand[2]) + self.p4(topcand[3]), topcand[2], topcand[3] ])
        return wcand
    
    #check if candiate is close enough to mass of top or w
    def PassMass(self, topcandp4, wcandp4, trange=80.0, wrange=40.0, tparam=175.0, wparam=80.0):
        pmW = False; pmT= False
        if ( abs(wcandp4.M()-wparam) <= wrange ):
            pmW = True
        if ( abs(topcandp4.M()-tparam) <= trange ):
            pmT = True
        # self.PassMassW = pmW
        # self.PassMassTop =pmT
        PassMass = (pmW and pmT)
        return PassMass #, self.PassMassTop, self.PassMassW

