# sets up distributions for combine using cardtools
# uses same dictionary and files that go into datacards
import os
import math
import numpy as np
import argparse
import uproot
import types
from collections import OrderedDict
import uproot_methods.classes.TH1
from ROOT import gROOT,TFile,TTree,TH1D
import ROOT as r
from datacard_dictionary import datacard_dict, getprocesses, datafile
from cardtools import cardtools
gROOT.SetBatch(True)

parser = argparse.ArgumentParser(description='Datacard distribution maker')
parser.add_argument("-b", "--baseline", dest="base",     default="VR",                                 help="baseline")
parser.add_argument("-c", "--cutsets",  dest="cutsets",  default=['BOTH'],                      help="array of cutsets to consider")
# parser.add_argument("-c", "--cutsets",  dest="cutsets",  default=['BASE','BOTH'],                      help="array of cutsets to consider")
parser.add_argument("-o", "--outdir",   dest="outdir",   default='./datacards/',                       help="directory to put datacards in")
parser.add_argument("-y", "--year",     dest="year",     default='2016',                               help="year")
args = parser.parse_args()

binset = ['ht']
varlist = ['catBDTdisc']
# varlist = ['nrestops', 'nbstops', 'njets', 'nbjets']
# varlist = ['ht', 'nrestops', 'nbstops', 'njets', 'nbjets', 'catBDTdisc']

#set up output directory
outdir = 'carddists_'+args.year+'/'
if not os.path.exists(outdir):
    os.makedirs(outdir); print 'Making output directory ', outdir

ct = cardtools(args.cutsets, binset, args.base, args.year)

ct.runNAFdists(outdir)
# ct.runshortprocs(varlist, outdir)
# ct.runnormdists(varlist, outdir)
# ct.rungetdists(varlist, outdir)
