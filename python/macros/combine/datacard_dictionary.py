#python script storing dictionaries for datacards
#takes desired baseline and bool for printing information and returns cuts and bins needed for that baseline
#first define bin dictionaries
#then define and cat dictionaries, which shouldnt change
#finally baseline-specific dictionaries with any modifications
#np arrays are faster than lists!
import numpy as np
from collections import OrderedDict

#dictionary of rootfiles
#MUST be ordered or TTTT signal will be in the wrong place, so make an ordered dict from a list instead 

def getprocesses(year='2016'):
    if year=='2016':
        processes = [('TTTT'     ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/TTTT_2016_merged_0lep_tree.root'),
                     ('TTX'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/TTX_2016_merged_0lep_tree.root'),
                     ('other'    ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/minorMC_2016_merged_0lep_tree.root')]
                     # ('TT'       ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/TT_2016_merged_0lep_tree.root'),
                     # ('QCD'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/QCD_2016_merged_0lep_tree.root'),
                     # ('W+Jets'   ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/wjets_2016_merged_0lep_tree.root'),
                     # ('Z+Jets'   ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/zjets_2016_merged_0lep_tree.root'),
                     # ('Singletop','/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/singletop_2016_merged_0lep_tree.root'),
                     # ('Dibosons' ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees_newWP2/dibosons_2016_merged_0lep_tree.root' )]
    elif year=='2017':
        processes = [('TTTT'     ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/TTTT_2017_merged_0lep_tree.root'),
                     ('TTX'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/TTX_2017_merged_0lep_tree.root'),
                     ('other'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/minorMC_2017_merged_0lep_tree.root')]
                     # ('QCD'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/QCD_2017_merged_0lep_tree.root'), 
                     # ('TT'       ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/TT_2017_merged_0lep_tree.root'),
                     # ('W+Jets'   ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/wjets_2017_merged_0lep_tree.root'),
                     # ('Z+Jets'   ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/zjets_2017_merged_0lep_tree.root'),
                     # ('Singletop','/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/singletop_2017_merged_0lep_tree.root'),
                     # ('Dibosons' ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/MC/0lep/maintrees_newWP2/dibosons_2017_merged_0lep_tree.root' )]
    elif year=='2018':
        processes = [('TTTT'     ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/TTTT_2018_merged_0lep_tree.root'),  
                     ('TTX'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/TTX_2018_merged_0lep_tree.root'),
                     ('other'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/minorMC_2018_merged_0lep_tree.root')]
                     # ('TT'       ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/TT_2018_merged_0lep_tree.root'), 
                     # ('QCD'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/QCD_2018_merged_0lep_tree.root'),
                     # ('W+Jets'   ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/wjets_2018_merged_0lep_tree.root'),
                     # ('Z+Jets'   ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/zjets_2018_merged_0lep_tree.root'),
                     # ('Singletop','/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/singletop_2018_merged_0lep_tree.root'),
                     # ('Dibosons' ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/MC/0lep/maintrees_newWP2/dibosons_2018_merged_0lep_tree.root' )]
    processes = OrderedDict(processes)
    return processes

datafile = 'none'

def getNAFfile(year, cutname, binname):
    #get's correct NAF histogram and process name
    NAFdict = { '2016' : {'BOTHht'  :'./NAF_VR_SR_before_combine/NAF_SR_2016_before_combine.root',
                          # 'BOTHht'  :'./NAF_ht_2016_shapehists_12bins_oct22.root',
                          # 'BOTHht'  :'./NAF_ht_2016_shapehists.root',
                          'BOTHtops':'./NAF_tops_2016_shapehists.root '},
                '2017' : {'BOTHht'  :'./NAF_VR_SR_before_combine/NAF_SR_2017_before_combine.root'},
                '2018' : {'BOTHht'  :'./NAF_VR_SR_before_combine/NAF_SR_2018_before_combine.root'}}
    chanid = cutname+binname
    print 'finding NAF histogram file for ', chanid, 'in year', year, '...'
    NAFfile = NAFdict[year][chanid]
    print 'NAFfile:', NAFfile
    return NAFfile
    
def datacard_dict(basename='SR', cutname='RT', binname='ht', year='2016', weightvar='weight', topsplit=False, basic=False):
    verbose = True
    lumi=36
    if year=='2016':
        lumi=35.92
    elif year=='2017':
        lumi=41.53
    elif year=='2018':
        lumi=59.74

    #baseline dictionaries:
    baselines={'SR'   :'njets_nosys>=9 && nbjets_nosys>=3 && nfunky_leptons==0 && ht_nosys>=700',
               'VR'   :'njets_nosys==8 && nbjets_nosys>=3 && nfunky_leptons==0 && ht_nosys>=700',
               'cutSR':'njets_nosys>=9 && nbjets_nosys>=3 && nfunky_leptons==0 && ht_nosys>=700 && catBDTdisc>=0.85'}

    if basename in baselines.keys():
        if verbose:
            print "Selected baseline is ", basename, baselines[basename]
    else:
        print 'Baseline not recognized!'
    baseline = baselines[basename]

    # you have a dictionary that maps the cut selections to the required bin selections
    #binvar = 'ht' or whatever
    bindict = { 'tops' :  {'all'   : ['ht_nosys>=700'],
                           '2bins'   : ['ht_nosys>=700'],
                           '3bins'   : ['ht_nosys>=700'],
                           },
                'ht' :  {'all'   : ['ht_nosys>=700'],
                         'test'   : ['ht_nosys<1200'],
                         '2bins' : ['ht_nosys<1000', 'ht_nosys>=1000'],
                         '2bins12' : ['ht_nosys<1200', 'ht_nosys>=1200'],
                         '2bins13' : ['ht_nosys<1300', 'ht_nosys>=1300'],
                         '2bins15' : ['ht_nosys<1500', 'ht_nosys>=1500'],
                         '2bins14' : ['ht_nosys<1400', 'ht_nosys>=1400'],
                         '3bins15GT' : ['ht_nosys<1500', 'ht_nosys>=1500 && ht_nosys<1800', 'ht_nosys>=1800'],
                         'cutbins' : ['ht_nosys<900', 'ht_nosys>=900'],
                         '3binsGT' : ['ht_nosys<1000', 'ht_nosys>=1000 && ht_nosys<1200', 'ht_nosys>=1200'], #was 1300
                         'htstep' : ['ht_nosys>=700 && ht_nosys<800', 'ht_nosys>=800 && ht_nosys<850', 'ht_nosys>=850 && ht_nosys<900', 'ht_nosys>=900 && ht_nosys<950', 'ht_nosys>=950 && ht_nosys<1000', 'ht_nosys>=1000 && ht_nosys<1050', 'ht_nosys>=1050 && ht_nosys<1100', 'ht_nosys>=1100 && ht_nosys<1200', 'ht_nosys>=1200 && ht_nosys<1300', 'ht_nosys>=1300 && ht_nosys<1500', 'ht_nosys>=1500'],
                         'htstep2' : ['ht_nosys>=700 && ht_nosys<900', 'ht_nosys>=900 && ht_nosys<1000', 'ht_nosys>=1000 && ht_nosys<1100', 'ht_nosys>=1100 && ht_nosys<1200', 'ht_nosys>=1200 && ht_nosys<1300', 'ht_nosys>=1300 && ht_nosys<1500', 'ht_nosys>=1500'],
                         'htstep3' : ['ht_nosys>=700 && ht_nosys<800', 'ht_nosys>=800 && ht_nosys<900', 'ht_nosys>=900 && ht_nosys<1000', 'ht_nosys>=1000 && ht_nosys<1100', 'ht_nosys>=1100 && ht_nosys<1200', 'ht_nosys>=1200 && ht_nosys<1300', 'ht_nosys>=1300 && ht_nosys<1500', 'ht_nosys>=1500'],
                         '4bins' : ['ht_nosys<800', 'ht_nosys>=800 && ht_nosys<1000', 'ht_nosys>=1000 && ht_nosys<1300', 'ht_nosys>=1300']},
                'met' : {'all'   : ['met>=0'],#no met cut
                         '2bins' : ['met<100', 'met>=100'],
                         '3bins' : ['met<100', 'met>=100 && met<200' , 'met>=200']},
                'nj' :  {'all'   : ['njets>=9'],
                         '2bins' : ['njets<11', 'njets>=11'],
                         'cutbins' : ['njets<11', 'njets>=11'],
                         '3bins' : ['njets<10', 'njets>=10 && njets<12', 'njets>=12']},
                'htnj' :  {'all'   : ['ht_nosys>=700 && njets>=9'],
                           '2binsHT' : ['ht_nosys<1000', 'ht_nosys>=1000'],
                           '2binsHT' : ['ht_nosys<1300', 'ht_nosys>=1300'],
                           '2binsHH15' : ['ht_nosys<1500', 'ht_nosys>=1500'],
                           '2binsNJ' : ['njets<10', 'njets>=10'],
                           '3binsA' : ['ht_nosys<1000 && njets>=9', 'ht_nosys>=1000 && njets<11', 'ht_nosys>=1000 && njets>=11'],
                           '3binsB' : ['ht_nosys<1000 && njets<11', 'ht_nosys<1000 && njets>=11', 'ht_nosys>=1000 && njets>=9'],
                           '4bins' : ['ht_nosys<1000 && njets<11', 'ht_nosys<1000 && njets>=11', 'ht_nosys>=1000 && njets<11', 'ht_nosys>=1000 && njets>=11'],
                           '3binsA2' : ['ht_nosys<1300 && njets>=9', 'ht_nosys>=1300 && njets<11', 'ht_nosys>=1300 && njets>=11'],
                           '5bins' : ['ht_nosys<1300 && njets==9', 'ht_nosys<1300 && njets>9', 'ht_nosys>=1300 && njets<10', 'ht_nosys>=1300 && njets==10', 'ht_nosys>=1300 && njets>=11'],
                           '3binsB2' : ['ht_nosys<1300 && njets<11', 'ht_nosys<1300 && njets>=11', 'ht_nosys>=1300 && njets>=9'],
                           '3binsB3' : ['ht_nosys<1000 && njets<10', 'ht_nosys<1000 && njets>=10', 'ht_nosys>=1000 && njets>=9'],
                           '3binsB4' : ['ht_nosys<1300 && njets<=10', 'ht_nosys<1300 && njets>10', 'ht_nosys>=1300 && njets>=9'],
                           '3binsB5' : ['ht_nosys<1400 && njets>=9', 'ht_nosys>=1400 && njets<11', 'ht_nosys>=1400 && njets>=11'],
                           '4bins2' : ['ht_nosys<1500 && njets<11', 'ht_nosys<1500 && njets>=11', 'ht_nosys>=1500 && njets<11', 'ht_nosys>=1500 && njets>=11'],
                           '4bins5' : ['ht_nosys<1500 && njets<10', 'ht_nosys<1500 && njets>=10', 'ht_nosys>=1500 && njets<10', 'ht_nosys>=1500 && njets>=10'],
                           '4bins3'  : ['ht_nosys<1300 && njets<11', 'ht_nosys<1300 && njets>=11', 'ht_nosys>=1300 && njets<11', 'ht_nosys>=1300 && njets>=11'],
                           '4bins4' : ['ht_nosys<1000 && njets<10', 'ht_nosys<1000 && njets>=10', 'ht_nosys>=1000 && njets<10', 'ht_nosys>=1000 && njets>=10'],
                           '4bins4b' : ['ht_nosys<1000 && njets<10', 'ht_nosys<1000 && njets>=10', 'ht_nosys>=1000 && njets<11', 'ht_nosys>=1000 && njets>=11'],
                           '7bins' : ['ht_nosys<1000 && njets==9', ' ht_nosys<1000 && njets==10','ht_nosys<1000 && njets>10', 'ht_nosys>=1000 && njets<10', 'ht_nosys>=1000 && njets==10', 'ht_nosys>=1000 && njets==11','ht_nosys>=1000 && njets>11'],
                           '8bins' : ['ht_nosys<850 && njets==9','(ht_nosys>=850 && ht_nosys<1000) && njets==9', ' ht_nosys<1000 && njets==10','ht_nosys<1000 && njets>10', 'ht_nosys>=1000 && njets<10', 'ht_nosys>=1000 && njets==10', 'ht_nosys>=1000 && njets==11','ht_nosys>=1000 && njets>11'],
                           'cutbins' : ['ht_nosys<1100 && njets>=9', 'ht_nosys>=1100 && njets<11', 'ht_nosys>=1100 && njets>=11'],
                           '3bins' : ['ht_nosys<1500 && njets<12', 'ht_nosys<1500 && njets>=12 && njets<16', 'ht_nosys<1500 && njets>=16', 'ht_nosys>=1500 && njets<12', 'ht_nosys>=1500 && njets>=12 && njets<16', 'ht_nosys>=1500 && njets>=16']},
                }

    cutdict = { 'BASE' : { 'topcuts' : ['nfunky_leptons==0'],
                           'bins'    : ['all'] },
                'BASE2': { 'topcuts' : ['nrestops_nosys>=0',
                                       'nrestops_nosys>=1'],
                        'bins'    : ['all', 'all'] },
                'RT': { 'topcuts'  : ['nrestops_nosys==1', 
                                      'nrestops_nosys>=2'],
                        'bins'     : ['all', 'all'] },
                'RTv2': { 'topcuts'  : ['nrestops_nosys==1', 
                                        'nrestops_nosys==2', 
                                        'nrestops_nosys>=3'], #added
                        'bins'     : ['all', 'all', 'all'] },
                        # 'bins'     : ['3bins', '2bins'] },
                'BT': { 'topcuts'  : ['nbstops_nosys==1',
                                      'nbstops_nosys>=2'],
                        'bins'     : ['all', 'all'] },
                        # 'bins'     : ['2bins', 'all'] },
                'BOTH': { 'topcuts': [
                    #'nrestops_nosys==0 && nbstops_nosys==1', #added
                    # 'nrestops_nosys==0 && nbstops_nosys>=2', #added
                                      'nrestops_nosys==1 && nbstops_nosys==0', 
                                      'nrestops_nosys==1 && nbstops_nosys>=1',
                                      'nrestops_nosys>=2 && nbstops_nosys>=0'],
                          # 'bins'   : ['ht_nosysstep3', '3bins15GT', '3binsGT'] },
                          'bins'   : ['htstep3', '2bins15', '2bins12'] }, #not combined
                          # 'bins'   : ['ht_nosysstep3', '2bins15', 'test'] }, #combined
                'BOTHv2': { 'topcuts': ['nrestops_nosys==0 && nbstops_nosys>=1', #added
                                        'nrestops_nosys==1 && nbstops_nosys==0', 
                                        'nrestops_nosys==1 && nbstops_nosys>=1',
                                        'nrestops_nosys>=2 && nbstops_nosys==0',
                                        'nrestops_nosys>=2 && nbstops_nosys>=1'],
                            'bins'   : ['all', 'all', 'all', 'all', 'all'] },
                'BOTHv3': { 'topcuts': ['nrestops_nosys==1 && nbstops_nosys==0', 
                                        'nrestops_nosys==1 && nbstops_nosys>=1',
                                        'nrestops_nosys>=2 && nbstops_nosys>=0'],
                                        # 'nrestops_nosys>=2 && nbstops_nosys==0',
                                        # 'nrestops_nosys>=2 && nbstops_nosys>=1'],
                            # 'bins'   : ['all', 'all', 'all', 'all', 'all'] },
                            # 'bins'   : ['4bins', '2bins15', '2bins', 'all'] }, #ht_nosys
                            # 'bins'   : ['4bins4b', '3binsA2', '2binsHT_NOSYS', 'all'] }, #ht_nosysnj
                            'bins'   : ['8bins', '3binsB5', '3binsB4'] }, #ht_nosysnj
                'BOTHbdtcut': { 'topcuts': ['nrestops_nosys==1 && nbstops_nosys==0 && catBDTdisc>=0.85', 
                                            'nrestops_nosys==1 && nbstops_nosys>=1 && catBDTdisc>=0.85',
                                            'nrestops_nosys>=2 && nbstops_nosys>=0 && catBDTdisc>=0.85'],
                                # 'bins'   : ['7bins', '3binsB5', '3binsB4'] },
                                'bins'   : ['ht_nosysstep3', '2bins15', '2bins12'] },
                                # 'bins'   : ['ht_nosysstep2', '2bins15', '3binsGT'] },
                'BOTHv4': { 'topcuts': ['nrestops_nosys==1 && nbstops_nosys==0', 
                                        'nrestops_nosys==2 && nbstops_nosys==0',
                                        'nrestops_nosys>=3 && nbstops_nosys==0',
                                        'nrestops_nosys>=1 && nbstops_nosys>=1'],
                            'bins'   : ['all', 'all', 'all', 'all'] },
                          # 'bins'   : ['all', '2bins', 'all', '2bins', 'all'] },
                # 'BOTH': { 'topcuts': ['nrestops_nosys==1 && nbstops_nosys==0', #old version
                #                       'nrestops_nosys==1 && nbstops_nosys==1', 
                #                       'nrestops_nosys==1 && nbstops_nosys>=2',
                #                       'nrestops_nosys>=2 && nbstops_nosys==0',
                #                       'nrestops_nosys>=2 && nbstops_nosys>=1'],
                #           'bins'   : ['3bins', '2bins', 'all', '2bins', 'all'] },
                'TEST': { 'topcuts'  : ['nrestops_nosys==1', # same as RT just has fewer bins
                                        'nrestops_nosys>=2'],
                          'bins'     : ['2bins', 'all'] }
                }

    cutdict = OrderedDict(cutdict)
    bindict = OrderedDict(bindict)

    #then you can return the full list of selections (cutstrings) for the given cutset and bin variable
    print 'getting bins from dictionary...'
    cutstr_list = {}
    cuts = cutdict[cutname]['topcuts']
    print cutname, binname
    for i, cut in enumerate(cuts):
        bintype = cutdict[cutname]['bins'][i]
        if binname=='tops':
            bintype='all' #no bin splitting in tops only
        bins = bindict[binname][bintype]
        for j, bin in enumerate(bins):
            cutid = 'cut'+str(i)+'bin'+str(j)
            if not basic:
                if not topsplit:
                    cutstr_list[cutid]='('+baseline+' && '+cut+' && '+bin+')*('+str(lumi)+'*'+weight_nosysvar+'*evtbtagSFshape_preselJets*puWeight_nosys*trigSF_0lep)' #selection cutstring

                elif topsplit:
                    cutstr_list[cutid]=['('+baseline+' && '+cut+' && '+bin+' && nrestops_nosys==0)*('+str(lumi)+'*'+weight_nosysvar+'*evtbtagSFshape_preselJets*puWeight_nosys*trigSF_0lep)', 
                                    '('+baseline+' && '+cut+' && '+bin+' && nrestops_nosys>=1)*('+str(lumi)+'*'+weight_nosysvar+'*evtbtagSFshape_preselJets*puWeight_nosys*trigSF_0lep)',
                                    '('+baseline+' && '+cut+' && '+bin+')*('+str(lumi)+'*'+weight_nosysvar+'*evtbtagSFshape_preselJets*puWeight_nosys*trigSF_0lep)']
                    print 'topsplit!!!'
            elif basic:
                    cutstr_list[cutid]=cut+' && '+bin  #selection cutstring
                
            print bintype, cutid, cutstr_list[cutid]

    return cutstr_list


