from root_numpy import root2array, array2root
import varlists as vl
import argparse
import numpy as np
import pandas as pd
import xgboost as xgb
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score
import re
import xml.etree.cElementTree as ET
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
# import PhysicsTools.NanoTrees.python.macros.plotting.RootStuff as rs
import RootStuff as rs

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
# parser.add_argument("-i", "--indir", dest="indir", default='/eos/uscms/store/user/mequinna/NanoAODv5/2016MC/fullset/')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-o", "--outdir", dest="outdir", default='./updatedrootfiles/')
parser.add_argument("-t", "--treename", dest="treename", default='Events')
parser.add_argument("-b", "--baseline", dest="base", default='nleptons==0 & ht>700 & njets>=7 & nbjets>=2')
parser.add_argument("-v", "--version", dest="vers", default='bdt1')
parser.add_argument("-c", "--filecount", dest="filecount", default=False)
# parser.add_argument("-m", "--merge", dest="merge", default=False)
# parser.add_argument("-k", "--keepall", dest="keepall", default=False)
# parser.add_argument("-s", "--skim", dest="skim", default=True)
args = parser.parse_args()

outdir = args.outdir #'./updatedrootfiles/'
filenames = args.filenames #['TTTT_TuneCUETP8M2T4_13TeV-amcatnlo-pythia8_1_tree.root']
treename = args.treename #'Events'
baseline = args.base #'nleptons==0 & ht>700 & njets>=9 & nbjets>=3' 

weights = {}

nevents_new = {
'TTTT': 897060,
    '':6743434
}

if args.filecount:
    tot = 0.0
    inrf, intree = rs.makeroot(infile, args.treename)
    print 'getting #input events from files. they should be the same type!'
    for f in filenames:
        infile = f
        new_nevts = intree.GetEntries()
        tot+=new_nevts

print "Will reweight", len(filenames), "files..."
for f in filenames:
    filename = f[f.rfind('/')+1:]
    filename = filename.strip('.root')
    filename += '_'+bdtname+'.root'
    infile = f
    outfile = outdir+filename

    mykey = ''
    for key in rs.nevt_dict:
        if key in infile:
            print "sample type:",key
            mykey = key

    reweight(mykey, infile, outfile)

#add new weight if not using full sample
def reweight(key, infile, outfile):
    #load tree, create new branch
    print 'reweighting file...', infile
    inrf, intree = rs.makeroot(infile, args.treename)
    outrf, outtree = rs.makeroot(outfile, args.treename, options="update")
    lenarr = outtree.GetEntries()
    # old_nevts = 0
    # for key in rs.nevt_dict:
    #     if key in infile:
    #         print "sample type:",key
    old_nevts = rs.nevt_dict[key]
    weight = weights[key]
    new_nevts = 0
    if not args.filecount:
        new_nevts = nevents_new[key]
    elif args.filecount:
        new_nevts = tot

    reweight = weight*(old_nevts/new_nevts)
    warr = np.full(lenarr, reweight,  dtype=np.float32) 
    branchname = 'new_weight'
    #declare branch
    wbranch = outtree.Branch(branchname, warr, branchname+"/F")
    #fill branch
    for w in warr:
        wbranch.Fill()
    outrf.Write()
    outrf.Close()
    # delete outrf
    print 'new weight:', reweight

reweight(args)
