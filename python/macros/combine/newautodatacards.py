#new and improved datacard generator, with shape included!
#a python script for properly generating datacards according to cut category
#modify cuts and baselines in datacard_dictionary.py
import os
import sys
import math
import numpy as np
import argparse
import uproot
import types
from collections import OrderedDict
from ROOT import gROOT,TFile,TTree,TH1D
import ROOT as r
from datacardrefs import datacardrefs
# from datacard_dictionary import datacard_dict, getprocesses, datafile, getNAFfile
from newhistmaker import histmaker
gROOT.SetBatch(True)

parser = argparse.ArgumentParser(description='Datacard information')
parser.add_argument("-b", "--baseline", dest="base",     default="SR",                                 help="baseline")
parser.add_argument("-o", "--outdir",   dest="outdir",   default='./datacards/',                       help="directory to put datacards in")
parser.add_argument("-y", "--year",     dest="year",     default='2016',                               help="year")
parser.add_argument("-f", "--shapefile",    dest="shapefile",    default='shapehists.root',            help="directory where shape histograms are located")
parser.add_argument("-r", "--remakehists",    dest="remakehists",    default='True',                   help="regenerate shape histograms")
args = parser.parse_args()

projvar = 'catBDTdisc_nosys'

fullR2=False
if args.year=='RunII':
    fullR2=True

usemcbinuncs = True

remakehists=False
if args.remakehists=='True':
    remakehists=True

if not fullR2:
    years = [args.year]
elif fullR2:
    years = ['2016', '2017', '2018']

#organizes set of bins for datacards and sets up card names and output directory
def cardsetup(args, year, startbinnum):

    refs = datacardrefs(year)
    name = 'allhad4top'

    #setup directories
    outdir=args.outdir
    if not os.path.exists(outdir):
        os.makedirs(outdir); print 'Making output directory ', outdir
        
    #get bins for datacard sets from datacard dictionary
   
    # get name of output shape rootfile
    shaperoot = args.outdir+'/'+name+'_'+year+'_'+args.shapefile
    cardname = args.outdir+'/'+name

    #print info on the datacards you are making
    #this is also text to be put in each datacard in the set
    header='#----------------------------------------------#\n'+'#generating datacards. name: '+name+', year'+year+'\n'
    header+='\n'+'#number of cards in set: '+str(len(refs.binsels.keys()))+'\n'
    print header

    #generate shape histograms:
    hm = histmaker(year, shaperoot)
    if (not os.path.exists(shaperoot)) or remakehists:
        print 'generating histograms...'
        histfile = r.TFile(shaperoot, "RECREATE")
        histfile.Close()
        del histfile
        yields = hm.makehists()

    shapeline = 'shapes * * '+name+'_'+year+'_'+args.shapefile+' $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC'
    print shapeline

    #make cards:
    binnum = startbinnum
    for chanid, binsel in refs.binsels.iteritems():
        label=chanid+'_'+year
        sel = '('+binsel+' && '+refs.mcsel
        makecard(args, cardname, label, year, sel, binnum, header, shapeline, refs, yields)
        binnum+=1
    return binnum

#calculates rates and statistical uncertainties
def getRate(args, procfile, cutstr, year, treename='Events'):
    print procfile, treename
    tree = r.TFile(procfile).Get(treename)
    htmp = r.TH1F('htmp','htmp',1,0.0,1.0)
    tree.Project('htmp','catBDTdisc_nosys',cutstr)
    errorVal = r.Double(0)
    rate = htmp.IntegralAndError(0,3,errorVal)
    rate = 0.0;rateunc = 1.0 
    if rate>0.0: #nonzero
        rateunc += (errorVal/rate)
    return round(rate,3), round(rateunc,3)
        
def uncline(proc, uncval, uncname, unctype, binlabel, index, nprocs):
    rowname = binlabel+'_'+proc+'_'+uncname
    line = rowname+'      '+unctype
    row = ['-']*nprocs
    row[index] = str(uncval)
    for u in row:
        line+=('    '+u)
    line += '\n'
    return line

def unctable(processes, unclist, uncname, unctype, binlabel):
        #unctype is lnN or shape...
        #unclist is the list of uncertainties for each process
    rownames = []
    for proc in processes:
        rowname = binlabel+'_'+proc+'_'+uncname
        rownames.append(rowname)
        # loop and create the table
    table = ''
    for i in range(len(processes)):
        row = ['-']*len(processes)
        row[i] = str(unclist[i])
        line = rownames[i]+'      '+unctype
        for u in row:
            line+=('    '+u)
        line += '\n'
        table+=line
        # print table
    return table

#creates a single datacard
def makecard(args, cardname, binlabel, year, cutstr, nbin, header, shapeline, refs, yields):
    #binlabel is cut#_bin#_year
    #cardfile is for example datacards/RTht5_cut1bin5_datacard.txt
    cardfile = cardname+'_'+binlabel+'_datacard'+str(nbin)+'.txt'
    procs = refs.processes
    # procs.append(refs.DDprocname) #[TTTT, TTX, other, DDBKG]
    nprocs = len(procs)

    imax = 1#number of final states analyzed (signals)
    jmax = nprocs-1 #number of processes(incl signal)-1
    kmax = '*' #* makes the code figure it out. number of independent systematical uncertainties 
    unctype  = 'lnN'

    print '\n'+binlabel
    print 'writing', cardfile,'selection:',cutstr

    # get yields and statistical uncertainties for each process in this bin:
    rates=[]; stats=[]
    for proc in procs:
        r=0.0;u=0.0
        # if proc == refs.DDprocname: #NAF
        #     r=refs.DDyields[][0]
        #     if refs.usenafunc:
        #         u = np.sqrt((refs.DDyields[1])**2+(refs.nafunc*refs.DDyields[0])**2) #extra norm uncertainty
        #     else:
        #         u = refs.DDyields[1]
        # else:
        # procfile = refs.procfiles[proc]
        # r,u =getRate(args, procfile, cutstr, year)
        yld = yields[binlabel+'_'+proc]
        r=yld[0]; u=yld[1]
        # if proc == refs.DDprocname and refs.usenafunc: #NAF
        #     u = np.sqrt((u)**2+(refs.nafunc*r)**2) #extra norm uncertainty
        rates.append(r); stats.append(1.0+(u/r))
        print proc, r, u
    
    if '.root' not in refs.datafile:
        obs  = int(round(sum(rates)))
        print 'TOTAL B: ', round(sum(rates[1:]))
    else:
        obs = yields[binlabel+'_data_obs'][0] #actual data rate
        print 'DATA:', obs

    # put together uncertanties for each process
    shapeuncs = [str(1.0)]*nprocs
    stat_table  = unctable(procs, stats, 'stat', 'lnN', binlabel)

    shape_table=''
    syst_table=''
    uclumi_line = 'lumi_uncorr_'+year+'   lnN    '+refs.uclumiunc+'    '+refs.uclumiunc+'    '+refs.uclumiunc+'    -    \n'
    clumi_line  = 'lumi_corr16_17_18'+'   lnN    '+refs.clumiunc+'    '+refs.clumiunc+'    '+refs.clumiunc+'    -    \n'
    if refs.spursig:
        uclumi_line = 'lumi_uncorr_'+year+'   lnN    '+refs.uclumiunc+'    '+refs.uclumiunc+'    '+refs.uclumiunc+'    '+refs.uclumiunc+'    -    \n'
        clumi_line  = 'lumi_corr16_17_18'+'   lnN    '+refs.clumiunc+'    '+refs.clumiunc+'    '+refs.clumiunc+'    '+refs.uclumiunc+'    -    \n'
    # uclumi_line = 'lumi_uncorr_'+year+'   lnN    '+refs.uclumiunc+'    '+refs.uclumiunc+'    '+refs.uclumiunc+'    '+refs.uclumiunc+'    '+refs.uclumiunc+'    -    \n'
    # clumi_line  = 'lumi_corr16_17_18'+'   lnN    '+refs.clumiunc+'    '+refs.clumiunc+'    '+refs.clumiunc+'    '+refs.clumiunc+'    '+refs.clumiunc+'    -    \n'
    syst_table+=uclumi_line
    syst_table+=clumi_line
    if year!='2016':
        c2lumi_line  = 'lumi_corr17_18'+'   lnN    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    -    \n'
        if refs.spursig:
            c2lumi_line  = 'lumi_corr17_18'+'   lnN    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'      -   \n'
        # c2lumi_line  = 'lumi_corr17_18'+'   lnN    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    '+refs.c2lumiunc+'    -    \n'
        syst_table+=c2lumi_line
    if not refs.mergehtbins:
        lepline_el = 'elveto_'+year+'   lnN    '+refs.eLV[binlabel][0]+'     '+refs.eLV[binlabel][1]+'    -    -    \n'
        lepline_mu = 'muveto_'+year+'   lnN    '+refs.mLV[binlabel][0]+'     '+refs.mLV[binlabel][1]+'    -    -    \n'
        if refs.spursig:
            lepline_el = 'elveto_'+year+'   lnN    '+refs.eLV[binlabel][0]+'     '+refs.eLV[binlabel][0]+'     '+refs.eLV[binlabel][1]+'    -   -  \n'
            lepline_mu = 'muveto_'+year+'   lnN    '+refs.mLV[binlabel][0]+'     '+refs.mLV[binlabel][0]+'     '+refs.mLV[binlabel][1]+'    -   -  \n'
        # lepline_el = 'elveto_'+year+'   lnN    '+refs.eLV[binlabel][0]+'     '+refs.eLV[binlabel][1]+'     '+refs.eLV[binlabel][1]+'     '+refs.eLV[binlabel][1]+'    -    -    \n'
        # lepline_mu = 'muveto_'+year+'   lnN    '+refs.mLV[binlabel][0]+'     '+refs.mLV[binlabel][1]+'     '+refs.mLV[binlabel][1]+'     '+refs.mLV[binlabel][1]+'    -    -    \n'
        syst_table+=lepline_el
        syst_table+=lepline_mu
    if not refs.mergehtbins:
        if year!='2018':
            prefire_line = 'prefire_'+year+'   lnN    '+refs.prefire[binlabel]['TTTT'][1]+'     '+refs.prefire[binlabel]['TTX'][1]+'    -    -    \n'
            if refs.spursig:
                prefire_line = 'prefire_'+year+'   lnN    '+refs.prefire[binlabel]['TTTT'][1]+'     '+refs.prefire[binlabel]['TTTT'][1]+'     '+refs.prefire[binlabel]['TTX'][1]+'    -    -  \n' 

            # prefire_line = 'prefire_'+year+'   lnN    '+refs.prefire[binlabel]['TTTT'][1]+'     '+refs.prefire[binlabel]['TTX'][1]+'     '+refs.prefire[binlabel]['TTX'][1]+'     '+refs.prefire[binlabel]['TTX'][1]+'    -    -    \n'
            syst_table+=prefire_line

    xsecline = 'cross_section   lnN   -   1.26    -    -    \n' #orig
    if refs.spursig:
        xsecline = 'cross_section   lnN   -    -    1.26  -   -  \n' #spursig
        # xsecline = 'cross_section   lnN   -   1.26    -   1.35   -  \n' #spursig #ttbb
        # xsecline = 'cross_section   lnN   -   1.26    -   1.15   -  \n' #orig #with spursig sf
    # xsecline = 'cross_section   lnN   -   1.26    -    -    \n' #orig
    # xsecline = 'cross_section   lnN   -   1.25    -    1.50    \n'
    # xsecline = 'cross_section   lnN   -   1.50    -    -    \n'
    # xsecline = 'cross_section   lnN   -   1.26   1.23   1.17   -    -    -    \n'
    # xsecline = 'cross_section   lnN   -   1.26   1.23   1.23     -    -    \n'
    syst_table+=xsecline

    if refs.usenafunc:
        # nafunc=1.0+refs.nafunc
        #adding shape version too:
        topcats = {'RT1BT0htbin0_2016':'RT1BT0_2016',  #for semi-correlation  
                   'RT1BT0htbin1_2016':'RT1BT0_2016',
                   'RT1BT0htbin2_2016':'RT1BT0_2016',
                   'RT1BT0htbin3_2016':'RT1BT0_2016',
                   'RT1BT0htbin4_2016':'RT1BT0_2016',
                   'RT1BT0htbin5_2016':'RT1BT0_2016',
                   'RT1BT0htbin6_2016':'RT1BT0_2016',
                   'RT1BT0htbin7_2016':'RT1BT0_2016',
                   'RT1BT1htbin0_2016':'RT1BT1_2016',
                   'RT1BT1htbin1_2016':'RT1BT1_2016',
                   'RT2BTALLhtbin0_2016':'RT2BTALL_2016',
                   'RT2BTALLhtbin1_2016':'RT2BTALL_2016',
                   'RT1BT0htbin0_2017':'RT1BT0_2017',  #for semi-correlation  
                   'RT1BT0htbin1_2017':'RT1BT0_2017',
                   'RT1BT0htbin2_2017':'RT1BT0_2017',
                   'RT1BT0htbin3_2017':'RT1BT0_2017',
                   'RT1BT0htbin4_2017':'RT1BT0_2017',
                   'RT1BT0htbin5_2017':'RT1BT0_2017',
                   'RT1BT0htbin6_2017':'RT1BT0_2017',
                   'RT1BT0htbin7_2017':'RT1BT0_2017',
                   'RT1BT1htbin0_2017':'RT1BT1_2017',
                   'RT1BT1htbin1_2017':'RT1BT1_2017',
                   'RT2BTALLhtbin0_2017':'RT2BTALL_2017',
                   'RT2BTALLhtbin1_2017':'RT2BTALL_2017',
                   'RT1BT0htbin0_2018':'RT1BT0_2018',  #for semi-correlation 
                   'RT1BT0htbin1_2018':'RT1BT0_2018',
                   'RT1BT0htbin2_2018':'RT1BT0_2018',
                   'RT1BT0htbin3_2018':'RT1BT0_2018',
                   'RT1BT0htbin4_2018':'RT1BT0_2018',
                   'RT1BT0htbin5_2018':'RT1BT0_2018',
                   'RT1BT0htbin6_2018':'RT1BT0_2018',
                   'RT1BT0htbin7_2018':'RT1BT0_2018',
                   'RT1BT1htbin0_2018':'RT1BT1_2018',
                   'RT1BT1htbin1_2018':'RT1BT1_2018',
                   'RT2BTALLhtbin0_2018':'RT2BTALL_2018',
                   'RT2BTALLhtbin1_2018':'RT2BTALL_2018'}
        # #uncorr:
        #uncorr:
        if not refs.corrnafunc:
            extranorm='data-pred_norm_disagreement_'+topcats[binlabel]+'    lnN        -         -         -      '+refs.VRerr[binlabel]+'\n' #partcorr
            # extranorm='data-pred_norm_disagreement_'+binlabel+'    lnN        -         -         -      '+refs.VRerr[binlabel]+'\n' #uncorr
            if refs.spursig:
                extranorm='data-pred_norm_disagreement_'+topcats[binlabel]+'    lnN        -         -         -      -    '+refs.VRerr[binlabel]+'\n' #partcorr
                # extranorm='data-pred_norm_disagreement_'+binlabel+'    lnN        -         -         -      -    '+refs.VRerr[binlabel]+'\n' #uncorr
            syst_table+=extranorm
            if refs.addshapeunc:
                extranormshape = 'data_pred_shape_disagreement_'+topcats[binlabel]+'  shape?     -    -    -    1.0  \n'   #partcorr
                # extranormshape = 'data_pred_shape_disagreement_'+binlabel+'  shape?     -    -    -    1.0  \n'   #uncorr
                if refs.spursig:
                    extranormshape = 'data_pred_shape_disagreement_'+binlabel+'  shape?     -    -    -   -   1.0  \n'  
                syst_table+=extranormshape
            if refs.randseed:
                # extranormshape = 'NNseed_shape_'+binlabel+'  shape     -    -    -    1.0  \n'  
                extranormshape = 'NNseed_shape_'+binlabel+'  shape     -    -    -  -  1.0  \n'  
                syst_table+=extranormshape
        #corr
        elif refs.corrnafunc:
            extranorm='data_pred_norm_disagreement_'+year+'    lnN        -         -         -      '+refs.VRerr[binlabel]+'\n'
            if refs.spursig:
                extranorm='data_pred_norm_disagreement_'+year+'    lnN        -         -         -   -   '+refs.VRerr[binlabel]+'\n'
            # extranorm='data_pred_norm_disagreement_'+year+'    lnN        -      -         -         -         -      '+refs.VRerr[binlabel]+'\n'
            syst_table+=extranorm
            if refs.addshapeunc:
                extranormshape = 'data_pred_shape_disagreement_'+year+'  shape?     -    -    -    1.0  \n'  
                if refs.spursig:
                    extranormshape = 'data_pred_shape_disagreement_'+year+'  shape?     -    -    -   -   1.0  \n'  
                # extranormshape = 'data_pred_shape_disagreement_'+year+'  shape   -    -    -    -    -    1.0  \n'  
                syst_table+=extranormshape
            if refs.randseed:
                # extranormshape = 'NNseed_shape_'+year+'  shape     -    -    -    1.0  \n'  
                extranormshape = 'NNseed_shape_'+year+'  shape     -    -    -  -  1.0  \n'  
                syst_table+=extranormshape

    #syst_table  = unctable(procnames, systs, ratesystname, 'lnN', binlabel)
    corr =[]
    # symbs = []
    for proc, systlist in refs.systs.iteritems():
        for sys in systlist: 
            if sys not in corr:
                corr.append(sys)
    for sys in corr:
        if sys=='isr' or sys=='fsr' or sys=='ME':
            if sys in refs.systs['TTTT']:
                if not refs.spursig:
                    shape_table += sys+'_'+'TTTT'+'  shape     1.0    -    -    -  \n'  
                if refs.spursig:
                    shape_table += sys+'_'+'TTTT'+'  shape     1.0    1.0    -    -  -  \n'  
                # shape_table += sys+'_'+'TTTT'+'  shape     1.0    -    -   -    -    -  \n'  
            if sys in refs.systs['TTX']:
                if not refs.spursig:
                    shape_table += sys+'_'+'TTX'+'  shape     -    1.0    -    -  \n'  
                if refs.spursig:
                    shape_table += sys+'_'+'TTX'+'  shape     -    -  1.0    -   -  \n'  
        ##procs+years
        elif sys=='btagLF' or sys=='btagHF' or sys=='btagCFerr1' or sys=='btagCFerr2':
            if not refs.spursig:
                shape_table += sys+'  shape     1.0    1.0    1.0    - \n'
            if refs.spursig:
                shape_table += sys+'  shape     1.0    1.0    1.0    1.0  - \n'
        elif sys=='pdf':
            if not refs.spursig:
                shape_table += sys+'  shape     1.0    1.0    -    - \n'
            if refs.spursig:
                shape_table += sys+'  shape     1.0    1.0    1.0    -   -   \n'
        else: 
            #just procs
            if not refs.spursig:
                shape_table += sys+'_'+year+'   shape     1.0   1.0   1.0    - \n'
            if refs.spursig:
                shape_table += sys+'_'+year+'   shape     1.0   1.0   1.0  1.0   - \n'


    rateline = ''
    procline = ''
    numline = ''
    binuncline = ''
    if usemcbinuncs:
        thr = 0
        includesig = 1
        binuncline = binlabel+' autoMCStats '+str(thr)+' '+str(includesig)
        print binuncline

    #write datacard to file

    f = open(cardfile, "w")
    print>>f, '## card: '+cardfile+' number in set: '+str(nbin)
    print>>f, '##selection: '+cutstr+'*prefirewgt*\n'
    print>>f, header
    print>>f, '## number of signals (i), backgrounds (j), and uncertainties (k)'
    print>>f, "---"
    print>>f, "imax: "+str(imax)
    print>>f, "jmax: "+str(jmax)
    print>>f, "kmax: "+str(kmax)
    print>>f, "---\n"
    # if useshape:
    print>>f, '## input the bdt shape histograms'
    print>>f, "---"
    print>>f, shapeline
    print>>f, "---\n"
    print>>f, '## list the bin label and the number of (data) events observed'
    print>>f, "---"
    print>>f, "bin  "+binlabel
    print>>f, "observation "+str(obs)
    print>>f, "---\n"
    print>>f, '## expected events for signal and all backgrounds in the bins'
    print>>f, "---"
    print>>f, "bin    "+(("    "+binlabel)*nprocs)
    for i,p in enumerate(procs):
        procline+=("    "+p)
        # if refs.spursig:
        #     numline+=("        "+str(i-1))
        # elif not refs.spursig:
        numline+=("        "+str(i))
            
    print>>f, "process"+procline
    print>>f, "process"+numline
    for r in rates:
        rateline+=("    "+str(r))
    print>>f, "rate   "+rateline        
    print>>f, "---\n"
    # if not nounc:
    print>>f, '## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin'
    print>>f, "---"
    print>>f, '## statistical uncertainties\n'
    print>>f, stat_table
    # if not nounc:
    print>>f, "---"
    print>>f, '## systematic uncertainties\n'
    print>>f, syst_table
    # if useshape:
    print>>f, "---"
    print>>f, '## shape uncertainties\n'
    print>>f, shape_table
    print>>f, "---\n"
    print>>f, binuncline
    print>>f, "---\n"
    if refs.spursig:
            print>>f, "---"
            print>>f, '## spurious signal \n'
            print>>f, 'sig_constraint_'+binlabel+'  param  0.0 '+str(refs.spursigscale[binlabel]-1.0)
            print>>f, 'sig_constraint_'+binlabel+'  rateParam  '+binlabel+' TTTT_spurious 0.0 [-100,100]'
    f.close()

####################################################################
# reinitialize roothists

startbinnum=0
print 'how many years?', len(years)
for year in years:
    print 'year', year, 'startbinnum', startbinnum
    print 'making cards with baseline:', args.base, '...'
    lastbinnum = cardsetup(args, year, startbinnum)
    print 'lastbinnum', lastbinnum
    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n'
    startbinnum=lastbinnum

