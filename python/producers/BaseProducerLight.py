#baseproducer to be inhritxed by other modules 
#should call leptonSFproducer, maybe other sfproducers
import numpy as np
import math
import logging
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest
from PhysicsTools.NanoTrees.helpers.ConfigHelper import LepSetter, JetSetter
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper

import PhysicsTools.NanoTrees.helpers.weighthelper as xw
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
import PhysicsTools.NanoTrees.helpers.LepTrigHelper as lth

class BaseProducerLight(Module):
    
    def __init__(self, channel, year):
        self.skim = True #skims on trigger
        self.gentt = True #includes ttbb flag
        self.genleps = True
        #self.skim = False #skims on trigger
        self.channel = channel
        self.year    = year
        
    def beginJob(self):
        if self.gentt:
            self.GEN = genparthelper()
        # self.LS = LepSetter(self.channel, self.year)
        # self.JS = JetSetter(self.channel, self.year)
        # self.BS = BoostSetter(self.year)
        pass 

    def endJob(self):
        pass

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        print "inputFile", inputFile
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        self.isMC = bool(inputTree.GetBranch('genWeight'))
        self.XWgt = xw.GetKnownWeight(self.infile, self.year, self.isMC) #xsec weight from file
        self.isQCD = False
        print 'initializing trees...'
        #basic variables
        self.out.branch("ismc", "I")
        # ttfiles = ['/TT_Tune', 'TTbb', 'TTToHadronic_', 'TTToSemiLeptonic_', 'TTTo2L2Nu_']
        # if any(name in self.infile for name in ttfiles):
        #     self.gentt = True
        if self.gentt:
            #check whether file needs it
            self.out.branch("isttbb", "I")
            self.out.branch("isttbbv2", "I")
        if self.genleps:
            self.out.branch("ngen_muons", "I")
            self.out.branch("ngen_electrons", "I")
            self.out.branch("ngen_taus", "I")
        self.out.branch("event", "L")
        self.out.branch("run", "I")
        self.out.branch("lumi", "I")
        self.out.branch("weight", "F") 
        self.out.branch("npv", "I")
        self.out.branch("met", "F")
        # self.out.branch("puweighttest", "F")
        if self.isMC:
            self.out.branch("genweight", "F") 
            self.out.branch("gen_njets",   "I") 
            # self.out.branch("genjet5flag", "I") 

        #leptons
        # self.out.branch("nleptons", "I")
        # self.out.branch("nmuons", "I")
        # self.out.branch("nelectrons", "I")
        # self.out.branch("lep1eta", "F")
        # self.out.branch("lep1phi", "F")
        # self.out.branch("lep1mass", "F")
        # self.out.branch("lep1q", "I")
        # self.out.branch("lep1pdgid", "I")
        # self.out.branch("lep1ptplusht", "F")
        # self.out.branch("lep1pt", "F")
        # self.out.branch("lep2pt", "F")
        # self.out.branch("lep3pt", "F")
        # self.out.branch("lep1id", "I")
        # self.out.branch("lep2id", "I")
        # self.out.branch("lep3id", "I")

        self.out.branch("goodverticesflag", "I")
        self.out.branch("haloflag", "I")
        self.out.branch("HBHEflag", "I")
        self.out.branch("HBHEisoflag", "I")
        self.out.branch("ecaldeadcellflag", "I")
        self.out.branch("badmuonflag", "I")

        if self.year=='2016':
            self.out.branch("eeBadScFilterflag","I")
            self.out.branch("trigHLT_PFHT900", "I")
        elif self.year=='2017' or self.year=='2018':
            self.out.branch("trigHLT_PFHT1050", "I")
                
        if not self.isMC:
            #triggers and flags
            self.out.branch("passtrig", "I")
            # self.out.branch("metfilterflag", "I")

            # self.out.branch("passtrigmu", "I")
            # self.out.branch("passtrigel", "I")
            # self.out.branch("passtriglep", "I")
            # self.out.branch("passtriglepOR", "I")
            # self.out.branch("passtrigmuht", "I")
            # self.out.branch("passtrigelht", "I")
            # self.out.branch("passtrigmetmht", "I")

            if self.year=='2016':
                self.out.branch("trigHLT_PFHT450_SixJet40_BTagCSV_p056", "I")
                self.out.branch("trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056", "I")
                # self.out.branch("trigHLT_Ele27_WPTight_Gsf", "I")
                # self.out.branch("trigHLT_IsoMu24", "I")
                # self.out.branch("trigHLT_IsoTkMu24", "I")
            elif self.year=='2017':
                self.out.branch("trigHLT_PFHT430_SixJet40_BTagCSV_p080", "I")
                self.out.branch("trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075", "I")
                self.out.branch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2", "I")
                self.out.branch("trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5", "I")
                self.out.branch("trigHLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0", "I")
            elif self.year=='2018':
                self.out.branch("trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59", "I")
                self.out.branch("trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94", "I")
                self.out.branch("trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5", "I")
                self.out.branch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2", "I")

    def EventFiller(self, event):
        # ismc = sel#bool(event.genWeight)
        met       = Object(event,     "MET") 
        if self.isMC: 
            genjets   = Collection(event, "GenJet")
        flags     = Object(event,"Flag")

        # #lepton selection+ jet cleaning if needed
        # if self.LS.clean_jetsvleps:
        #     ak4jets, SelEls, SelMus, SelLeps =self.LS.getLeps(event)
        # else:
        #     ak4jets   = Collection(event, "Jet")
        #     SelEls, SelMus, SelLeps =self.LS.getLeps(event)

        # nels=0; nmus=0; nleps=0
        # l1pt=0.0; l1pdg=0; l1eta=0.0; l1phi=0.0; l1mass=0.0; l1q=0; l1ptplusht=0.0
        # l2pt=0.0; l2pdg=0; l3pt=0.0; l3pdg=0
        # nmus = len(SelMus)
        # nels = len(SelEls)
        # nleps = len(SelLeps)
        # if nleps>0:
        #     l1pt=SelLeps[0].pt; l1pdg=SelLeps[0].pdgId; l1eta=SelLeps[0].eta; l2phi=SelLeps[0].phi; l1mass=SelLeps[0].mass; l1q=SelLeps[0].charge
        # elif nleps>1:
        #     l2pt=SelLeps[1].pt; l2pdg=SelLeps[1].pdgId
        # elif nleps>2:
        #     l3pt=SelLeps[2].pt; l3pdg=SelLeps[2].pdgId

        # trigel, trigmu, triglep, passtrigmuht, passtrigelht, passtrigmetmht, passtriglepor = 0,0,0,0,0,0,0
        # if not self.isMC and nleps>0:
        #     trigel, trigmu, triglep, passtrigmuht, passtrigelht, passtrigmetmht, passtriglepor = lth.getleptriggers(event, self.infile)
            
        #cross sectional weights
        XWeight = 1.0
        if self.isMC:
            XWeight = self.XWgt
            if event.genWeight < 0: # get the right sign
                XWeight = -XWeight

        #mc/data changes
        genweight = 1.0; ngenjets=0
        if self.isMC:
            genweight = event.genWeight
            ngenjets = event.nGenJet 
        
        # event=-1
        # if event.event<sys.maxint:
        #     event = event.event#issues with int size, maybe use python 3 or longs?
        self.out.fillBranch("event",            long(event.event))
        self.out.fillBranch("ismc",             int(self.isMC))  
        self.out.fillBranch("run",              int(event.run))
        self.out.fillBranch("lumi",             event.luminosityBlock)
        self.out.fillBranch("weight",           XWeight)
        self.out.fillBranch("met",              met.pt) 
        self.out.fillBranch("npv",              event.PV_npvs) 
        #test
        # self.out.fillBranch("puweighttest", event.puWeightUp)
        if self.isMC:
            self.out.fillBranch("genweight",        event.genWeight )
            self.out.fillBranch("gen_njets",        event.nGenJet )
            if self.gentt:
                isttbbv1, isttbbv2 = self.GEN.tagttbb(event)
                self.out.fillBranch("isttbb",       bool(isttbbv1))
                self.out.fillBranch("isttbbv2",       bool(isttbbv2))
            if self.genleps:
                nmu, nel, ntau = self.GEN.tagleps(event)
                self.out.fillBranch("ngen_muons",      nmu)
                self.out.fillBranch("ngen_electrons",  nel)
                self.out.fillBranch("ngen_taus",      ntau)
                
        # self.out.fillBranch("nleptons",         nleps)
        # self.out.fillBranch("nelectrons",       nels)
        # self.out.fillBranch("nmuons",           nmus)
        # self.out.fillBranch("lep1pt",           l1pt)
        # self.out.fillBranch("lep2pt",           l2pt)
        # self.out.fillBranch("lep3pt",           l3pt)
        # self.out.fillBranch("lep1id",           l1pdg)
        # self.out.fillBranch("lep2id",           l2pdg)
        # self.out.fillBranch("lep3id",           l3pdg)
        # self.out.fillBranch("lep1eta",          l1eta)
        # self.out.fillBranch("lep1phi",          l1phi)
        # self.out.fillBranch("lep1mass",         l1mass)
        # self.out.fillBranch("lep1q",            l1q)
        # self.out.fillBranch("lep1ptplusht",     l1ptplusht)

        event.flags   = Object(event,"Flag")
        # self.out.fillBranch("metfilterflag",                             flags.METFilters)
        self.out.fillBranch("goodverticesflag",                          flags.goodVertices)
        self.out.fillBranch("haloflag",                                  flags.globalSuperTightHalo2016Filter)
        self.out.fillBranch("HBHEflag",                                  flags.HBHENoiseFilter)
        self.out.fillBranch("HBHEisoflag",                               flags.HBHENoiseIsoFilter)
        self.out.fillBranch("ecaldeadcellflag",                          flags.EcalDeadCellTriggerPrimitiveFilter)
        self.out.fillBranch("badmuonflag",                               flags.BadPFMuonFilter)
        if self.year=='2016':
            self.out.fillBranch("eeBadScFilterflag",                               flags.eeBadScFilter)

        # if not self.isMC:
        #     passtrig=False
            # self.out.fillBranch("passtrigmu",       trigmu)
            # self.out.fillBranch("passtriglep",      triglep) 
            # self.out.fillBranch("passtrigel",       trigel)
            # self.out.fillBranch("passtriglepOR",    passtriglepor)
            # self.out.fillBranch("passtrigmuht",     passtrigmuht)
            # self.out.fillBranch("passtrigelht",     passtrigelht)
            # self.out.fillBranch("passtrigmetmht",   passtrigmetmht)

        puretrig=False
        if self.year=='2016':
            if bool(self.intree.GetBranch('HLT_PFHT900')):
                puretrig=event.HLT_PFHT900
            self.out.fillBranch("trigHLT_PFHT900",       int(puretrig))
        elif self.year=='2017' or self.year=='2018':
            if bool(self.intree.GetBranch('HLT_PFHT1050')):
                puretrig=event.HLT_PFHT1050
            self.out.fillBranch("trigHLT_PFHT1050",       int(puretrig))

        passtrig=True
        if not self.isMC:
            passtrig=False
            if self.year=='2016':
                trig1=False;trig2=False
                if bool(self.intree.GetBranch('HLT_PFHT400_SixJet30_DoubleBTagCSV_p056')):
                    trig1=event.HLT_PFHT400_SixJet30_DoubleBTagCSV_p056
                if bool(self.intree.GetBranch('HLT_PFHT450_SixJet40_BTagCSV_p056')):
                    trig2=event.HLT_PFHT450_SixJet40_BTagCSV_p056
                if trig1 or trig2:
                    passtrig=True
                self.out.fillBranch("passtrig",                                int(passtrig))
                self.out.fillBranch("trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056", int(trig1))
                self.out.fillBranch("trigHLT_PFHT450_SixJet40_BTagCSV_p056",       int(trig2))
                
                #extras
                # puretrig = False; el1=False, mu1=False, mu2=False
                # if bool(self.intree.GetBranch('HLT_PFHT900')):
                #     puretrig = event.HLT_PFHT900
                # if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                #     el1=event.HLT_Ele27_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_IsoMu24')):
                #     mu1=event.HLT_IsoMu24
                # if bool(self.intree.GetBranch('HLT_IsoTkMu24')):
                #     mu2=event.HLT_IsoTkMu24
                # self.out.fillBranch("trigHLT_PFHT900",             int(puretrig))
                # self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
                # self.out.fillBranch("trigHLT_IsoMu24",             int(mu1))
                # self.out.fillBranch("trigHLT_IsoTkMu24",           int(mu2))

                # puretrig = False; el1=False, mu1=False, mu2=False
                # if bool(self.intree.GetBranch('HLT_PFHT900')):
                #     puretrig = event.HLT_PFHT900
                # if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                #     el1=event.HLT_Ele27_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_IsoMu24')):
                #     mu1=event.HLT_IsoMu24
                # if bool(self.intree.GetBranch('HLT_IsoTkMu24')):
                #     mu2=event.HLT_IsoTkMu24
                # self.out.fillBranch("trigHLT_PFHT900",             int(puretrig))
                # self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
                # self.out.fillBranch("trigHLT_IsoMu24",             int(mu1))
                # self.out.fillBranch("trigHLT_IsoTkMu24",           int(mu2))


            elif self.year=='2017':
                trig1=False;trig2=False;trig3=False;trig4=False;trig5=False
                if  bool(self.intree.GetBranch('HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0')):
                    trig1=event.HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0
                if bool(self.intree.GetBranch('HLT_PFHT430_SixJet40_BTagCSV_p080')):
                    trig2=event.HLT_PFHT430_SixJet40_BTagCSV_p080
                if  bool(self.intree.GetBranch('HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5')):
                    trig3=event.HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5
                if  bool(self.intree.GetBranch('HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2')):
                    trig4=event.HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2
                if bool(self.intree.GetBranch('HLT_PFHT380_SixJet32_DoubleBTagCSV_p075')):
                    trig5=event.HLT_PFHT380_SixJet32_DoubleBTagCSV_p075
                if any([trig1,trig2,trig3,trig4,trig5]):
                    passtrig=True
                self.out.fillBranch("passtrig",                                                   int(passtrig))
                self.out.fillBranch("trigHLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0",  int(trig1))
                self.out.fillBranch("trigHLT_PFHT430_SixJet40_BTagCSV_p080",                          int(trig2))
                self.out.fillBranch("trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5",                       int(trig3))
                self.out.fillBranch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2",                 int(trig4))
                self.out.fillBranch("trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075",                    int(trig5))

                #extras

                # puretrig = False; el1=False; el2=False; mu1=False; mu2=False
                # if bool(self.intree.GetBranch('HLT_PFHT1050')):
                #     puretrig = event.HLT_PFHT1050
                # if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                #     el1=event.HLT_Ele27_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_Ele32_WPTight_Gsf')):
                #     el2=event.HLT_Ele32_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_IsoMu27')):
                #     mu1=event.HLT_IsoMu27
                # if bool(self.intree.GetBranch('HLT_IsoMu30')):
                #     mu2=event.HLT_IsoMu30
                # self.out.fillBranch("trigHLT_PFHT1050",         int(puretrig))
                # self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
                # self.out.fillBranch("trigHLT_Ele32_WPTight_Gsf",   int(el2))
                # self.out.fillBranch("trigHLT_IsoMu27",             int(mu1))
                # self.out.fillBranch("trigHLT_IsoMu30",             int(mu2))

                # puretrig = False; el1=False; el2=False; mu1=False; mu2=False
                # if bool(self.intree.GetBranch('HLT_PFHT1050')):
                #     puretrig = event.HLT_PFHT1050
                # if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                #     el1=event.HLT_Ele27_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_Ele32_WPTight_Gsf')):
                #     el2=event.HLT_Ele32_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_IsoMu27')):
                #     mu1=event.HLT_IsoMu27
                # if bool(self.intree.GetBranch('HLT_IsoMu30')):
                #     mu2=event.HLT_IsoMu30
                # self.out.fillBranch("trigHLT_PFHT1050",         int(puretrig))
                # self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
                # self.out.fillBranch("trigHLT_Ele32_WPTight_Gsf",   int(el2))
                # self.out.fillBranch("trigHLT_IsoMu27",             int(mu1))
                # self.out.fillBranch("trigHLT_IsoMu30",             int(mu2))
            elif self.year=='2018':
                trig1=False;trig2=False;trig3=False;trig4=False
                if bool(self.intree.GetBranch('HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59')):
                    trig1=event.HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59
                if bool(self.intree.GetBranch('HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94')):
                    trig2=event.HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94
                if bool(self.intree.GetBranch('HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5')):
                    trig3=event.HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5
                if bool(self.intree.GetBranch('HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2')):
                    trig4=event.HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2
                if any([trig1,trig2,trig3,trig4]):
                    passtrig=True
                self.out.fillBranch("passtrig",                                          int(passtrig))
                self.out.fillBranch("trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59",         int(trig1))
                self.out.fillBranch("trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94",   int(trig2))
                self.out.fillBranch("trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5",          int(trig3))
                self.out.fillBranch("trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2",    int(trig4))
                #extras 
                # puretrig = False; el1=False; el2=False; el3=False; mu1=False; mu2=False
                # if bool(self.intree.GetBranch('HLT_PFHT1050')):
                #     puretrig = event.HLT_PFHT1050
                # if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                #     el1=event.HLT_Ele27_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_Ele32_WPTight_Gsf')):
                #     el2=event.HLT_Ele32_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_Ele35_WPTight_Gsf')):
                #     el3=event.HLT_Ele35_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_IsoMu27')):
                #     mu1=event.HLT_IsoMu27
                # if bool(self.intree.GetBranch('HLT_IsoMu30')):
                #     mu2=event.HLT_IsoMu30
                # self.out.fillBranch("trigHLT_PFHT1050",         int(puretrig))
                # self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
                # self.out.fillBranch("trigHLT_Ele32_WPTight_Gsf",   int(el2))
                # self.out.fillBranch("trigHLT_Ele35_WPTight_Gsf",   int(el3))
                # self.out.fillBranch("trigHLT_IsoMu27",             int(mu1))
                # self.out.fillBranch("trigHLT_IsoMu30",             int(mu2))
                # puretrig = False; el1=False; el2=False; el3=False; mu1=False; mu2=False
                # if bool(self.intree.GetBranch('HLT_PFHT1050')):
                #     puretrig = event.HLT_PFHT1050
                # if bool(self.intree.GetBranch('HLT_Ele27_WPTight_Gsf')):
                #     el1=event.HLT_Ele27_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_Ele32_WPTight_Gsf')):
                #     el2=event.HLT_Ele32_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_Ele35_WPTight_Gsf')):
                #     el3=event.HLT_Ele35_WPTight_Gsf
                # if bool(self.intree.GetBranch('HLT_IsoMu27')):
                #     mu1=event.HLT_IsoMu27
                # if bool(self.intree.GetBranch('HLT_IsoMu30')):
                #     mu2=event.HLT_IsoMu30
                # self.out.fillBranch("trigHLT_PFHT1050",         int(puretrig))
                # self.out.fillBranch("trigHLT_Ele27_WPTight_Gsf",   int(el1))
                # self.out.fillBranch("trigHLT_Ele32_WPTight_Gsf",   int(el2))
                # self.out.fillBranch("trigHLT_Ele35_WPTight_Gsf",   int(el3))
                # self.out.fillBranch("trigHLT_IsoMu27",             int(mu1))
                # self.out.fillBranch("trigHLT_IsoMu30",             int(mu2))

        # if self.skim:
        #     # self.basesel = 'nleptons==0 && njets>=6 && nbjets>=2'
        #     # self.basesel = 'nleptons>=1 && njets>=3 && nbjets>=1'
        #     if self.channel=='0lep' and (nleps==0 and njets>=6 and nbjets>=2):
        #         return True
        #     elif self.channel=='1lep' and (nleps>=1 and njets>=3 and nbjets>=1):
        #         return True
        #     return False
        # return True
        return passtrig

    def analyze(self, event):
        # self.EventFiller(event)
        # return True
        passtrigger = self.EventFiller(event)
        if self.skim:
            # print nleps
            # if self.channel=='1lep':
            # if self.channel=='0lep' and nleps>0:
            if self.isMC or passtrigger:
                return True
            else:
                return False
        return True
        # if keepevent:
        #     return True
        # return False

BaseTrees2016_0lep = lambda : BaseProducerLight('0lep','2016')
BaseTrees2016_1lep = lambda : BaseProducerLight('1lep','2016')

BaseTrees2017_0lep = lambda : BaseProducerLight('0lep','2017')
BaseTrees2017_1lep = lambda : BaseProducerLight('1lep','2017')

BaseTrees2018_0lep = lambda : BaseProducerLight('0lep','2018')
BaseTrees2018_1lep = lambda : BaseProducerLight('1lep','2018')
