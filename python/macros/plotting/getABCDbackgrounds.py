#code to get data driven backgrounds for sfplots
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

class ABCDbackgrounds:
    def __init__(self, projvar, typ, year, mcfile, datafile, applySFs=False):

        self.projvar = projvar
        self.year=year
        self.applySFs=applySFs

        if projvar=='nrestops':
            treename='Events'
        else:
            treename='Friends'
        self.nbins, self.selrange=sels.getHistSettings(projvar)
        self.mcrf, self.mct = rs.makeroot(mcfile, treename)
        self.drf, self.dt = rs.makeroot(datafile, treename)

        lumi = str(rs.lumis[year]['0lep'])
        # if projvar !='nrestops':
        trigs ={'2016':'&& trigHLT_PFHT900',
                '2017':'&& trigHLT_PFHT1050',
                '2018':'&& trigHLT_PFHT1050'}
        # else:
        #     trigs={'2016':'&& (trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || trigHLT_PFHT450_SixJet40_BTagCSV_p056==1)', 
        #            '2017':'&& (trigHLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || trigHLT_PFHT430_SixJet40_BTagCSV_p080 || trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075)',
        #            '2018':'&& (trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'}

        self.trigsel = trigs[year]

        weights = '*weight*'+lumi+'*puWeight'
        if projvar=='nrestops':
            weights='*weight*btagSF_nosys*puWeight*bsWSF_nosys*bsTopSF_nosys*'+lumi
            if applySFs:
                weights+='*RTeffSF_nosys*RTmissSF_nosys'
        elif projvar!='nrestops' and applySFs:
            weights+='*(rescand_genmatch==1 ? evteffSF : evtmistagSF)'
        self.weights = weights

        wp = {'2016':'0.9988',
              '2017':'0.9992',
              '2018':'0.9994'}
        topwp=wp[year]
        self.topwp=topwp

        metfilters   = ' && goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag'
        if year=='2016':
            metfilters = ' && goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag'
        possiblesels = { 'effval':{'D' : '( nfunky_leptons==0 && nbjets>=3 && njets==8 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'B' : '( nfunky_leptons==0 && nbjets==2 && njets==8 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'C' : '( nfunky_leptons==0 && nbjets>=3 && njets==7 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'A' : '( nfunky_leptons==0 && nbjets==2 && njets==7 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'Ap': '( nfunky_leptons==0 && nbjets==2 && njets==6 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'Cp': '( nfunky_leptons==0 && nbjets>=3 && njets==6 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')'},
                         'misval':{'D' : '( nfunky_leptons==0 && nbjets==1 && (njets==4 || njets==5) && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'B' : '( nfunky_leptons==0 && nbjets==0 && (njets==4 || njets==5) && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'C' : '( nfunky_leptons==0 && nbjets==1 && njets==3 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'A' : '( nfunky_leptons==0 && nbjets==0 && njets==3 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'Ap': '( nfunky_leptons==0 && nbjets==0 && njets==2 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')',
                                   'Cp': '( nfunky_leptons==0 && nbjets==1 && njets==2 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[year]+')'},
        }
        self.sels = possiblesels[typ]
        print mcfile, datafile
        print 'set up ABCD histograms for type', typ, 'year', year, 'and variable', projvar

    def getyield(self, hist, verbose=False):
        errorVal = r.Double(0)
        minbin=0
        maxbin=hist.GetNbinsX()+1
        hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
        if verbose:
            print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
        return hyield,  errorVal

    def getCRhist(self, CR, verbose=True):
        #returns mc-subtracted hist of that type with appropriate error bars
        sel = self.sels[CR]
        var = self.projvar
        if self.projvar == 'nrestops': #other type of file
            sel = sel.replace('&& rescand_disc>'+self.topwp, '')
            sel = sel.replace('nbjets','nbjets_nosys')
            sel = sel.replace('njets','njets_nosys')
            sel = sel.replace('ht','ht_nosys')
            var = 'nrestops_nosys'
        if verbose:
            print 'CR', CR, 'var:', var, 'sel', sel

        mcsel=sel+self.weights
        if self.projvar == 'nrestops':
            mcsel = mcsel.replace(self.trigsel,'')
        # mchist = rs.gethist(self.mct, 'ht_nosys', mcsel, self.nbins, self.selrange)
        # dhist = rs.gethist(self.dt, 'ht_nosys', sel, self.nbins, self.selrange)
        mchist = rs.gethist(self.mct, var, mcsel, self.nbins, self.selrange)
        dhist = rs.gethist(self.dt, var, sel, self.nbins, self.selrange)

        #get yields
        mcy, mcu = self.getyield(mchist)
        dy, du = self.getyield(dhist)
        cry=dy-mcy
        cru=np.sqrt((du*du)+(mcu*mcu))        
        yld=cry; unc=cru
        yld=dy; unc=du #temp
        if CR=='D':
            yld=dy; unc=du
        print 'dy', dy, 'mcy', mcy

        #subtract histograms
        if CR!='D':
            dhist.Add(mchist,-1)

        mchist.SetDirectory(0)
        dhist.SetDirectory(0)

        # rs.fixref(self.mcrf, mchist)
        # rs.fixref(self.drf, dhist)

        return dhist, yld, unc

    def getABCDhists(self, returnyld=False):
        CRylds = {}; CRhists = {}
        for cr, crsel in self.sels.iteritems():
            print 'CR', cr, 'selection:', crsel
            crhist, cry, cru = self.getCRhist(cr)
            print cry, cru
            CRylds[cr]=cry
            CRylds[cr+'unc']=cru
            CRhists[cr]=crhist

        #yields
        calcyld = (CRylds['Ap']/(CRylds['B']*CRylds['Cp']))*(((CRylds['B']*CRylds['C'])/CRylds['A'])**2)
        calcunc=(2.0*((CRylds['Bunc']/CRylds['B'])**2+ (CRylds['Cunc']/CRylds['C'])**2+ (CRylds['Aunc']/CRylds['A'])**2) + (CRylds['Bunc']/CRylds['B'])**2+ (CRylds['Cpunc']/CRylds['Cp'])**2+ (CRylds['Apunc']/CRylds['Ap'])**2)
        calcunc = calcyld*np.sqrt(calcunc)
        
        #divide hists (ApB/Cp)(C/A)^2
        ABCDhist = CRhists['B'].Clone('ABCDhist') #B
        ABCDhist.Sumw2()
        ABCDhist.Multiply(CRhists['Ap']) #BAp
        ABCDhist.Divide(CRhists['Cp']) #BAp/Cp
        sqpart = CRhists['C'].Clone('sqpart') #C
        sqpart.Sumw2()
        sqpart.Divide(CRhists['A']) #(C/A)
        sqpart.Multiply(sqpart) #(C/A)^2
        ABCDhist.Multiply(sqpart)#(ApB/Cp)(C/A)^2

        #print yield information
        print 'calculated yield:', calcyld, '+/-', calcunc
        print 'true data yield:', CRylds['D'], '+/-', CRylds['Dunc']
        print '\n' 

        if returnyld:
            return ABCDhist, CRhists['D'], calcyld, calcunc
        else:
            return ABCDhist, CRhists['D']
