#plots discriminant distribution
import root_numpy as rn
import argparse
import os
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import ROOT as r


title = 'new2'
sigfile = '/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/eventMVA/rootfiles/NewBDT2_eventBDT-result.root'
bgdfile = sigfile
treename = 'Events'
sigsel = '(target==1 &&eventBDTdisc>0.0)'
bgdsel = '(target==0 &&eventBDTdisc>0.0)'
# sigsel = '(target==1 && nleptons==0 && nbjets>=3 && njets>=6 && ht>=700 && eventBDTdisc!=0)*weight'
# bgdsel = '(target==0 && nleptons==0 && nbjets>=3 && njets>=6 && ht>=700  && eventBDTdisc!=0)*weight'
varname = 'eventBDTdisc'
norm = True
outfile = './plots/'+title+'.png'

nbins = 100
selrange = (0,1)

print "normed?", norm
print 'sigsel', sigsel
print 'bgdsel', bgdsel

def makeroot(infile, treename):
    rfile = r.TFile(infile)
    tree = rfile.Get(treename)
    return rfile, tree

f1, t1 = makeroot(sigfile, treename)
f2, t2 = makeroot(bgdfile, treename)

c = r.TCanvas("c", "canvas", 800, 800)
h1 = r.TH1D('h1', 'h1', nbins, selrange[0], selrange[1])
h2 = r.TH1D('h2', 'h2', nbins, selrange[0], selrange[1])
t1.Draw(varname+">>h1",sigsel)
t2.Draw(varname+">>h2", bgdsel)

if norm:
    N = 1.0
    scale1 = N/h1.Integral()
    scale2 = N/h2.Integral()
    h1.Scale(scale1)
    h2.Scale(scale2)
    
h1.SetStats(0)
h1.Draw()
h2.Draw("SAME")

#prettiness
h1.SetLineColor(r.kBlue)
h1.SetLineWidth(2)
h2.SetLineColor(r.kRed)
h2.SetLineWidth(2)

c.SetGridx()
c.SetGridy()

h1.GetYaxis().SetTitle("NEntries")
h1.GetYaxis().SetTitleSize(20)
h1.GetYaxis().SetTitleFont(43)
h1.GetYaxis().SetTitleOffset(1.55)

h1.GetXaxis().SetTitle(varname)
h1.GetXaxis().SetTitleSize(20)
h1.GetXaxis().SetTitleFont(43)
h1.GetXaxis().SetTitleOffset(1.55)

leg1= r.TLegend(0.5,0.8,0.9,0.9)
leg1.AddEntry(h1,          "signal", "l")
leg1.AddEntry(h2,          "background", "l")
leg1.Draw("same")
c.Update()
c.SaveAs(outfile)
