# takes list of files and splits them 
import subprocess
import numpy as np
import argparse
import ROOT as r
import pandas as pd
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
# import RootStuff as rs
import os
import re

parser = argparse.ArgumentParser(description='')
parser.add_argument("-f", "--filenames", dest="txtfile", default='./TTredo_splitfiles.txt')
# parser.add_argument("-o", "--outdir", dest="outdir", default='root://cmseos.fnal.gov///store/user/lpcstop/noreplica/mequinna/NanoAODv6/')
parser.add_argument("-o", "--outdir", dest="outdir", default='root://cmseos.fnal.gov///store/user/lpcstop/mequinna/NanoAODv6/')
args = parser.parse_args()

#loop over lines in text file to get files
#get number of entries in files
#calculate number of events per file
#split files and output into outdir

procs = ['ST_tW_antitop', 'ZJetsToQQ_HT400to600', 'ZJetsToQQ_HT-800toInf', 'ttHTobb_SPLIT', 'WJetsToQQ_HT-800toInf', 'ZJetsToQQ_HT600to800', 'WJetsToQQ_HT600to800', 'WJetsToQQ_HT-800toInf', 'TT_SPLIT', 'TTToHadronic_SPLIT']
# procs = ['TT', 'TTToHadronic', 'TTToSemiLeptonic', 'TTTo2L2Nu']
# procs = ['TTTT', 'TTGJets', 'TTZToLLNuNu', 'TTZToQQ','TTWJetsToLNu', 'TTWJetsToQQ', 'ttHTobb', 'ttHToNonbb']
# procs = ['DYJetsToLL_M-10to50', 'DYJetsToLL_M-50', 'ST_s-channel_4f_InclusiveDecays','ST_s-channel_4f_hadronicDecays', 'ST_s-channel_4f_leptonDecays', 'ST_t-channel_antitop', 'ST_t-channel_top', 'ST_tW_antitop', 'ST_tW_top', 'TTTT', 'TTX_SPLIT', 'tZq_ll', 'tZq_nunu', 'WJetsToLNu_HT-400To600','WJetsToLNu_HT-600To800', 'WJetsToLNu_HT-800To1200', 'WJetsToLNu_HT-1200To2500', 'WJetsToLNu_HT-2500ToInf', 'WJetsToQQ_HT400to600', 'WJetsToQQ_HT600to800', 'WJetsToQQ_HT800ToInf', 'ZZ_', 'WZ_', 'WW_', 'ZJetsToNuNu_HT-400To600','ZJetsToNuNu_HT-600To800', 'ZJetsToNuNu_HT-800To1200', 'ZJetsToNuNu_HT-1200To2500', 'ZJetsToNuNu_HT-2500ToInf', 'ZJetsToQQ_HT400to600', 'ZJetsToQQ_HT600to800', 'ZJetsToQQ_HT800ToInf', 'TTZToLLNuNu', 'TTZToQQ', 'TTWJetsToQQ', 'TTWJetsToLNu', 'TTGJets']

years = {'/2016/':'2016', '/2017/':'2017', '/2018/':'2018'}

with open(args.txtfile, "r") as filelist:
    for f in filelist:
        if '#' not in f:
            f = re.sub(r"[\n\t\s]*", "", f)
            # f=f.strip()
            print 'loading', f, '...'
            fname=f.replace("/store/", "/eos/uscms/store/")
            # fname=f.replace("root://cmseos.fnal.gov///", "/eos/uscms//")
            # fname = f
            # print fname
            outname = fname[fname.rfind('/')+1:]
            # outname = './'+outname
            # outname = args.outdir+outname
            outname = outname.strip('.root')
            proc = ''; year = ''
            for p in procs:
                if p in fname:
                    proc = p
            for yname, y in years.iteritems():
                if yname in fname:
                    year = y
            rf = r.TFile(str(fname))
            tree = rf.Get("Events")
            nevts = tree.GetEntries()
            # N0 = int(float(nevts/3.0))
            # N1 = N0+1
            # N2 = (N0*2)+1
            # N3 = (N0*3)+1

            #split into 4 ################
            # out0 = outname+'_'+proc+year+'_0.root'            
            # out1 = outname+'_'+proc+year+'_1.root'
            # out2 = outname+'_'+proc+year+'_2.root'            
            # out3 = outname+'_'+proc+year+'_3.root'

            # print nevts, int(float(nevts/4.0))
            # N0 = int(float(nevts/4.0))

            # cmd0='rooteventselector -l '+str(N0)+' '+fname+':Events '+out0
            # cmd1='rooteventselector -f '+str(N0+1)+' -l '+str(N0*2)+' '+fname+':Events '+out1
            # cmd2='rooteventselector -f '+str((N0*2)+1)+' -l '+str(N0*3)+' '+fname+':Events '+out2
            # cmd3='rooteventselector -f '+str((N0*3)+1)+' '+fname+':Events '+out3
            # # cmd2='rooteventselector -f '+str((N0*2)+1)+' '+fname+':Events '+out2
            # print cmd0
            # print cmd1
            # print cmd2
            # print cmd3

            # copydir = args.outdir+year+'/MC/'+proc+'_SPLIT'
            # cp0 = 'xrdcp '+out0+' '+copydir+'; rm '+out0
            # cp1 = 'xrdcp '+out1+' '+copydir+'; rm '+out1
            # cp2 = 'xrdcp '+out2+' '+copydir+'; rm '+out2
            # cp3 = 'xrdcp '+out3+' '+copydir+'; rm '+out3
            # print cp0
            # print cp1
            # print cp2
            # print cp3
            ################

            #split into 2 ################
            out0 = outname+'_'+proc+year+'_0.root'            
            out1 = outname+'_'+proc+year+'_1.root'

            print nevts, int(float(nevts/2.0))
            N0 = int(float(nevts/2.0))

            cmd0='rooteventselector -l '+str(N0)+' '+fname+':Events '+out0
            cmd1='rooteventselector -f '+str(N0+1)+' '+fname+':Events '+out1
            print cmd0
            print cmd1

            copydir = args.outdir+year+'/MC/'+proc+'_SPLIT'
            cp0 = 'xrdcp '+out0+' '+copydir+'; rm '+out0
            cp1 = 'xrdcp '+out1+' '+copydir+'; rm '+out1
            print cp0
            print cp1
            ################

            print 'running command...'
            subprocess.call(cmd0, shell=True)
            subprocess.call(cp0, shell=True)

            subprocess.call(cmd1, shell=True)
            subprocess.call(cp1, shell=True)

            # subprocess.call(cmd2, shell=True)
            # subprocess.call(cp2, shell=True)

            # subprocess.call(cmd3, shell=True)
            # subprocess.call(cp3, shell=True)

            print '\n'
