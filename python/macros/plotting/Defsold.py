import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

class plotdefs:
    def __init__(self, year, base, modbins=True):
        self.year = year
        self.base = base
        self.lumi = str(rs.lumis[year]['0lep'])
        self.tn = 'Events'
        self.edges = np.array([0.0, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95,  1.0], dtype=np.double)
        
        self.shifts = [2, 10, 20, 30]
        self.mcprocs = ["TTTT", "TTX", "minor"]
        
        self.colors = [r.kRed-7, r.kPink+2, r.kAzure+1, r.kViolet-4, r.kMagenta-9, r.kOrange+1, r.kYellow-7, r.kCyan-7, r.kBlue-9, r.kGreen-9]
        # self.colors = [r.kRed-7, r.kOrange+1, r.kYellow-7, r.kGreen-9, r.kCyan-7, r.kAzure+1, r.kBlue-9, r.kViolet-4, r.kMagenta-9]
        
        # if not modbins:
        #     NAFfiles = {'SR': '../combine/NAF_VR_SR_before_combine/NAF_SR_'+year+'_before_combine.root',
        #                 'VR': '../combine/NAF_VR_SR_before_combine/NAF_8jet_'+year+'_before_combine.root'}
        # elif modbins:
        #     NAFfiles = {'SR': '../combine/NAF_VR_SR_after_combine/NAF_SR_'+year+'_after_combine.root',
        #                 'VR': '../combine/NAF_VR_SR_after_combine/NAF_8jet_'+year+'_after_combine.root'}

        # self.NAFfile = r.TFile(NAFfiles[base])
        self.datafile = 'JETHT_'+year+'_merged_0lep_tree.root'
        # self.datafile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep//JETHT_'+year+'_merged_0lep_tree.root'
        # self.datafile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintreeswithsysts/JETHT_'+year+'_merged_0lep_tree.root'
        # datafiles = {'2016':'../combine/VRNAFwithdata/NAF_2016_error_base.root',
        #              '2017':'../combine/VRNAFwithdata/NAF_2017_error_base.root',
        #              '2018':'../combine/VRNAFwithdata/NAF_2018_error_base.root'}
        # self.datafile = r.TFile(datafiles[year])

        self.QCD_TT_file =  '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/QCD_TT_'+year+'_merged_0lep_tree.root'

        metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
        metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'

        self.metfilters = metfilters
        if year=='2016':
            self.metfilters = metfilters16

        self.mcfiles = {'TTTT': '../eventMVA/updatedrootfiles/TTTT_'+year+'_merged_0lep_tree.root',
                        # 'TTTT': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/TTTT_'+year+'_merged_0lep_tree.root',
                        'TTX': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/TTX_'+year+'_merged_0lep_tree.root',
                        'minor': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/minorMC_'+year+'_merged_0lep_tree.root'}

        # self.dstr = {'2016' : '&& passtrig'+metfilters16,
        #              '2017' : '&& passtrig'+metfilters,
        #              '2018' : '&& passtrig'+metfilters,}
        self.dstr = {'2016' : '&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
                     '2017' : '&& (HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || HLT_PFHT430_SixJet40_BTagCSV_p080 || HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || HLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,
                     '2018' : '&& (HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,}

        baseline = {'VR':'nleptons==0 && ht>=700 && nbjets>=3 && njets==8',
                    'SR':'nleptons==0 && ht>=700 && nbjets>=3 && njets>=9'}
        # baseline = {'VR':'nfunky_leptons==0 && ht>=700 && nbjets>=3 && njets==8',
        #             'SR':'nfunky_leptons==0 && ht>=700 && nbjets>=3 && njets>=9'}

        self.mcstr = {'2016' : metfilters16+'*weight*btagSF*puWeight*trigSF_0lep*bsWSF*bsTopSF*'+self.lumi,
                      '2017' : metfilters+'*weight*btagSF*puWeight*trigSF_0lep*bsWSF*bsTopSF*'+self.lumi,
                      '2018' : metfilters+'*weight*btagSF*puWeight*trigSF_0lep*bsWSF*bsTopSF*'+self.lumi,}

        binsels = {'cut0bin0':'nrestops==1 && nbstops==0 && ht>=700 && ht<800',
                   'cut0bin1':'nrestops==1 && nbstops==0 && ht>=800 && ht<900', 
                   'cut0bin2':'nrestops==1 && nbstops==0 && ht>=900 && ht<1000', 
                   'cut0bin3':'nrestops==1 && nbstops==0 && ht>=1000 && ht<1100',
                   'cut0bin4':'nrestops==1 && nbstops==0 && ht>=1100 && ht<1200',
                   'cut0bin5':'nrestops==1 && nbstops==0 && ht>=1200 && ht<1300',
                   'cut0bin6':'nrestops==1 && nbstops==0 && ht>=1300 && ht<1500', 
                   'cut0bin7':'nrestops==1 && nbstops==0 && ht>=1500',
                   'cut1bin0':'nrestops==1 && nbstops>=1 && ht<1500',
                   'cut1bin1':'nrestops==1 && nbstops>=1 && ht>=1500'}
        if modbins:
            binsels['cut2bin0']='nrestops>=2 && nbstops>=0'
        else:
            binsels['cut2bin0']='nrestops>=2 && nbstops>=0 && ht<1200'
            binsels['cut2bin1']='nrestops>=2 && nbstops>=0 && ht>=1200'

        extras = {'base_ls':'ht<1200',
                  'base_gr':'ht>=1200'}

        self.binsels = extras#binsels
        self.basesel = baseline[base]
        
        outdir = './plots/systhists/'+base+year+'/'
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        self.outdir = outdir

        extDDyields = {'2016' : {'VR' : {'cut0bin0':[3258.78, 106.939],
                                         'cut0bin1':[2263.517, 92.798],
                                         'cut0bin2':[1704.779, 88.583],
                                         'cut0bin3':[1122.769, 74.383],
                                         'cut0bin4':[969.98, 80.894],
                                         'cut0bin5':[586.735, 64.566],
                                         'cut0bin6':[506.997, 54.076],
                                         'cut0bin7':[467.993, 62.387],
                                         'cut1bin0':[404.703, 39.43],
                                         'cut1bin1':[114.166, 31.098],
                                         'cut2bin0':[623.174, 52.79]},

                                 'SR' : {'cut0bin0':[1615.131, 67.799],
                                         'cut0bin1':[1562.481, 74.815],
                                         'cut0bin2':[1181.7, 67.007],
                                         'cut0bin3':[1051.773, 71.268],
                                         'cut0bin4':[700.215, 57.475],
                                         'cut0bin5':[495.65, 51.552],
                                         'cut0bin6':[686.641, 67.088],
                                         'cut0bin7':[753.188, 83.088],
                                         'cut1bin0':[249.856, 30.501],
                                         'cut1bin1':[224.2, 51.392],
                                         'cut2bin0':[807.459, 66.191]} },

                                 # 'TT' : {'cut0bin0':[, 67.799],
                                 #         'cut0bin1':[1562.481, 74.815],
                                 #         'cut0bin2':[1181.7, 67.007],
                                 #         'cut0bin3':[1051.773, 71.268],
                                 #         'cut0bin4':[700.215, 57.475],
                                 #         'cut0bin5':[495.65, 51.552],
                                 #         'cut0bin6':[686.641, 67.088],
                                 #         'cut0bin7':[753.188, 83.088],
                                 #         'cut1bin0':[249.856, 30.501],
                                 #         'cut1bin1':[224.2, 51.392],
                                 #         'cut2bin0':[224.2, 51.392],
                                 #         'cut2bin1':[807.459, 66.191]} },

                       '2017' : {'VR' : {'cut0bin0':[4292.906, 143.438],
                                         'cut0bin1':[3071.298, 122.306],
                                         'cut0bin2':[2570.54, 121.891],
                                         'cut0bin3':[1549.458, 89.825],
                                         'cut0bin4':[1029.803, 75.423],
                                         'cut0bin5':[699.804, 64.7],
                                         'cut0bin6':[796.572, 75.945],
                                         'cut0bin7':[529.705, 62.889],
                                         'cut1bin0':[590.31, 50.809],
                                         'cut1bin1':[175.951, 43.127],
                                         'cut2bin0':[936.7, 75.969]},

                                 'SR' : {'cut0bin0':[2113.445, 84.614],
                                         'cut0bin1':[1976.126, 89.029],
                                         'cut0bin2':[1391.696, 73.165],
                                         'cut0bin3':[1239.76, 76.521],
                                         'cut0bin4':[895.121, 66.868],
                                         'cut0bin5':[605.95, 55.211],
                                         'cut0bin6':[818.924, 72.591],
                                         'cut0bin7':[758.176, 78.274],
                                         'cut1bin0':[430.671, 45.841],
                                         'cut1bin1':[427.129, 82.361],
                                         'cut2bin0':[995.527, 77.13]} },
                                 # 'TT' : {'cut0bin0':[, 67.799],
                                 #         'cut0bin1':[1562.481, 74.815],
                                 #         'cut0bin2':[1181.7, 67.007],
                                 #         'cut0bin3':[1051.773, 71.268],
                                 #         'cut0bin4':[700.215, 57.475],
                                 #         'cut0bin5':[495.65, 51.552],
                                 #         'cut0bin6':[686.641, 67.088],
                                 #         'cut0bin7':[753.188, 83.088],
                                 #         'cut1bin0':[249.856, 30.501],
                                 #         'cut1bin1':[224.2, 51.392],
                                 #         'cut2bin0':[224.2, 51.392],
                                 #         'cut2bin1':[807.459, 66.191]} },

                       '2018' : {'VR' : {'cut0bin0':[5219.384, 140.429],
                                         'cut0bin1':[3911.998, 126.634],
                                         'cut0bin2':[3072.169, 122.631],
                                         'cut0bin3':[1965.625, 99.079],
                                         'cut0bin4':[1216.993, 78.497],
                                         'cut0bin5':[903.24, 75.29],
                                         'cut0bin6':[850.298, 71.678],
                                         'cut0bin7':[721.681, 77.044],
                                         'cut1bin0':[906.981, 64.165],
                                         'cut1bin1':[241.367, 52.488],
                                         'cut2bin0':[1197.528, 82.308]},

                                 'SR' : {'cut0bin0':[2844.721, 98.315],
                                         'cut0bin1':[2562.637, 99.223],
                                         'cut0bin2':[2155.301, 96.404],
                                         'cut0bin3':[1701.409, 91.311],
                                         'cut0bin4':[1351.45, 88.385],
                                         'cut0bin5':[1063.247, 85.036],
                                         'cut0bin6':[1245.621, 96.656],
                                         'cut0bin7':[1036.99, 94.63],
                                         'cut1bin0':[566.176, 51.083],
                                         'cut1bin1':[432.965, 75.162],
                                         'cut2bin0':[1126.695, 78.027]} } }
                                 # 'TT' : {'cut0bin0':[, 67.799],
                                 #         'cut0bin1':[1562.481, 74.815],
                                 #         'cut0bin2':[1181.7, 67.007],
                                 #         'cut0bin3':[1051.773, 71.268],
                                 #         'cut0bin4':[700.215, 57.475],
                                 #         'cut0bin5':[495.65, 51.552],
                                 #         'cut0bin6':[686.641, 67.088],
                                 #         'cut0bin7':[753.188, 83.088],
                                 #         'cut1bin0':[249.856, 30.501],
                                 #         'cut1bin1':[224.2, 51.392],
                                 #         'cut2bin0':[224.2, 51.392],
                                 #         'cut2bin1':[807.459, 66.191]} }

        if not modbins:
            extDDyields['2016']['VR']['cut2bin0']=[564.892, 48.834]
            extDDyields['2017']['VR']['cut2bin0']=[822.098, 68.374]
            extDDyields['2018']['VR']['cut2bin0']=[1072.673, 75.568]

            extDDyields['2016']['SR']['cut2bin0']=[605.567, 53.035]
            extDDyields['2017']['SR']['cut2bin0']=[745.827, 62.514]
            extDDyields['2018']['SR']['cut2bin0']=[828.831, 62.232]

            extDDyields['2016']['VR']['cut2bin1']=[39.745, 22.162]
            extDDyields['2017']['VR']['cut2bin1']=[81.977, 43.719]
            extDDyields['2018']['VR']['cut2bin1']=[118.299, 63.961]

            extDDyields['2016']['SR']['cut2bin1']=[242.962, 68.323]
            extDDyields['2017']['SR']['cut2bin1']=[216.806, 52.457]
            extDDyields['2018']['SR']['cut2bin1']=[317.946, 69.081]

        self.extDDyields = extDDyields[year][base]

    def getyield(self, hist, verbose=False):
        errorVal = r.Double(0)
        minbin=0
        maxbin=hist.GetNbinsX()+1
        hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
        if verbose:
            print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
        return hyield,  errorVal
        
    def getsels(self, chanid):
        binsel = self.binsels[chanid]
        sel = '('+binsel+' && '
        mcsel = sel + self.basesel +')'+ self.mcstr[self.year]
        datasel = sel + self.basesel +')'+ self.dstr[self.year]
        mysels = {'sel':sel, 'mcsel':mcsel, 'datasel':datasel}
        return mysels
            
    def gethist(self, filename, projvar, cut, varbin=True):
        nbins, selrange = self.getHistSettings(projvar)
        rf, t = rs.makeroot(filename, self.tn)
        if not varbin:
            hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
        else:
            hist = r.TH1F('hist', 'hist', nbins, self.edges)
        t.Draw(projvar+">>hist", cut)
        rs.fixref(rf, hist)
        return hist

    def histfromtree(self, chanid, proc='DDBKG', varbin=True):
        binalias = {'cut0bin0':'0',
                    'cut0bin1':'1',
                    'cut0bin2':'2',
                    'cut0bin3':'3',
                    'cut0bin4':'4',
                    'cut0bin5':'5',
                    'cut0bin6':'6',
                    'cut0bin7':'7',
                    'cut1bin0':'10',
                    'cut1bin1':'11',
                    'cut2bin0':'20',
                    'cut2bin1':'21'}# for reading trees
        doscale = False 
        treefile = '../combine/NAFtrees/'
        var = 'BDT_dsic'
        if proc == 'DDBKG':
            doscale=True
            treefile = treefile+'naf_'+self.base+'/'+'naf_'+self.base+'_'+self.year+'_Tree.root'
        elif proc == 'TTTT':
            treefile = treefile+'TTTT/TTTT_SR_'+self.year+'_Tree.root'
            var = 'BDT_disc'
        elif proc == 'data':
            treefile = treefile+'data/ori_VR_'+self.year+'_Tree.root'
        else:
            print 'proc must be data, TTTT or DDBKG'
        mybin = binalias[chanid]
        print treefile, 'bin',mybin
        nbins, selrange =  self.getHistSettings('catBDTdisc')
        cut = '(bin=='+mybin+' && '+var+'>=0.0 && '+var+'<=1.0)'
        print cut
        cutovfl = '(bin=='+mybin+')'
        rf, t = rs.makeroot(treefile, self.tn)
        histovfl = rs.gethist(t, var, cutovfl, nbins, selrange)
        if not varbin:
            hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
        else:
            hist = r.TH1F('hist', 'hist', nbins, self.edges)
        # histovfl = r.TH1F('histovfl', 'histovfl', nbins, selrange[0], selrange[1])
        t.Draw(var+">>hist", cut)
        # t.Draw(var+">>histovfl", cutovfl)
        rs.fixref(rf, hist)
        rs.fixref(rf, histovfl)
        if doscale:
            yld, unc = self.getyield(hist)
            yldovfl, uncovfl = self.getyield(histovfl)
            print 'yield', yld, 'overflow', yldovfl-yld
            hist.Scale(self.extDDyields[chanid][0]/yld)
        return hist

    def getHistSettings(self, var):
        vardict = {
            #[nbins, xmin, xmax]
            'catBDTdisc': [10, 0.0, 1.0],
            'catBDTdisc_manual': [10, 0.0, 1.0],
            'catBDTdisc_manual_jerUp': [10, 0.0, 1.0],
            'catBDTdisc_manual_jerDown': [10, 0.0, 1.0],
            'catBDTdisc_manual_jesUp': [10, 0.0, 1.0],
            'catBDTdisc_manual_jesDown': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagLFUp': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagHFUp': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagLFDown': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagHFDown': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagLFstats1Up': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagHFstats1Up': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagLFstats1Down': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagHFstats1Down': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagLFstats2Up': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagHFstats2Up': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagLFstats2Down': [10, 0.0, 1.0],
            'catBDTdisc_manual_btagHFstats2Down': [10, 0.0, 1.0],
            'catBDTdisc_manual_DeepAK8TopSF_Up': [10, 0.0, 1.0],
            'catBDTdisc_manual_DeepAK8TopSF_Down': [10, 0.0, 1.0],
            'catBDTdisc_manual_DeepAK8WSF_Up': [10, 0.0, 1.0],
            'catBDTdisc_manual_DeepAK8WSF_Down': [10, 0.0, 1.0],
            'ht'      : [20, 0.0, 3000.0],
            'bjht'    : [50, 0, 1500],
            'met'     : [50, 0, 1000],
            'metovsqrtht' : [50, 0.0, 200.0],
            'sfjm'    : [100, 0, 1000],
            'njets'   : [20, 0, 20],
            'nbjets'  : [10, 0, 10],
            'nbsws'   : [10, 0, 10],
            'nrestops': [6, 0, 6],
            'nbstops' : [6, 0, 6],
            'nfunky_leptons': [20, 0, 20],
            'fwm0'  : [50, 0, 1],
            'fwm1'  : [50, 0, 1],
            'fwm2'  : [50, 0, 1],
            'fwm3'  : [50, 0, 1],
            'fwm4'  : [50, 0, 1],
            'fwm5'  : [50, 0, 1],
            'fwm6'  : [50, 0, 1],
            'fwm7'  : [50, 0, 1],
            'fwm8'  : [50, 0, 1],
            'fwm9'  : [50, 0, 1],
            'fwm10'  : [50, 0, 1],
            'fwm20'  : [50, 0, 1], 
            'fwm30'  : [50, 0, 1],
            # 'topdiscs'  : [50, -1, 1],
            'rescand_disc'  : [50, 0.95, 1],
            'rescand_pt'  : [20, 0.0, 1000],
            'jet1pt'  : [50, 0, 1000],
            'jet2pt'  : [50, 0, 1000],
            'bstop1_pt'  : [50, 100, 1000],
            'bstop2_pt'  : [50, 100, 1000],
            'restop1pt'  : [50, 100, 1000],
            'restop2pt'  : [50, 100, 1000],
            'qglsum'  : [50, 0, 30],
            'qglprod'  : [50, 0, 30],
            'jet8pt'  : [50, 0, 200],
            'aplanarity'  : [20, 0, 1],
            'sphericity'  : [20, 0, 1],
            'dphij1j2'  : [10, -4, 4],
            'detaj1j2'  : [10, -4, 4],
            'dphib1b2'  : [10, -4, 4],
            'detab1b2'  : [10, -4, 4],
            'leadbpt' : [50, 0, 500],
            'npv' : [50, 0, 100],
            'lep1pt'  : [50, 0, 250],
            'lep2pt'  : [50, 0, 250],
            'ModBDT2disc': [100, 0.0, 1.0],
            'evtbdtdisc': [100, 0.0, 1.0],
            'NewBDT3disc': [100, 0.0, 1.0],
            'NewBDT4disc': [100, 0.0, 1.0],
            'Best12disc': [100, 0.0, 1.0],
            'passtrigel': [2, 0,1], 
            'passtrigmu': [2, 0,1]}
        params = vardict[var]
        nbins = params[0]
        selrange = (params[1], params[2])
        return nbins, selrange

