import subprocess
import os
import argparse
import sys
import math
import numpy as np
import ROOT as r
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
r.gROOT.SetBatch(True)


r.gROOT.SetBatch(1)
parser = argparse.ArgumentParser(description='xsec weights') 
parser.add_argument("-p", "--proc", dest="proc", default='TT') 
parser.add_argument("-t", "--typ", dest="typ", default='pdf') #ME or pdf or nom
parser.add_argument("-y", "--year", dest="year", default='2018') 
args = parser.parse_args()

year = args.year
eos = '/eos/uscms/'
indir = '/store/user/lpcstop/mequinna/NanoAODv6/'+year+'/MC/'
prefix = 'root://cmseos.fnal.gov//'

lumis= {'2016': 35.91,
        '2017': 41.53,
        '2018': 59.74 }
lumi = lumis[year]

procs = {# 'TTTT':{'2016':['TTTT_TuneCUETP8M2T4_PSweights_13TeV-amcatnlo-pythia8'],
         #         '2017':['TTTT_TuneCP5_PSweights_13TeV-amcatnlo-pythia8_correctnPartonsInBorn'],
         #         '2018':['TTTT_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTT_TuneCP5_13TeV-amcatnlo-pythia8_split']},
         # 'TTX':{'2016':['TTX_SPLIT'],
         #        '2017':['TTX_SPLIT'],
         #        '2018':['TTX_SPLIT'] } }
         # 'TTX':{'2016':['TTX_SPLIT','ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8', 'ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8',
         #                'TTZToQQ_TuneCUETP8M1_13TeV-amcatnlo-pythia8', 'TTZToLLNuNu_M-10_TuneCUETP8M1_13TeV-amcatnlo-pythia8',
         #                'TTWJetsToQQ_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8', 'TTWJetsToLNu_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8',
         #                'TTGJets_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8', 'TTWJetsToLNu_SPLIT', 'TTZToLLNuNu_SPLIT', 'TTWJetsToQQ_SPLIT',
         #                'TTZToQQ_SPLIT', 'ttHTobb_SPLIT', 'ttHToNonbb_SPLIT'],
         #        '2017':['TTX_SPLIT','ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8', 'ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8',
         #                'TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8', 'TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8',
         #                'TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8', 'TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
         #                'TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8', 'TTWJetsToLNu_SPLIT', 'TTZToLLNuNu_SPLIT', 'TTWJetsToQQ_SPLIT',
         #                'TTZToQQ_SPLIT', 'ttHTobb_SPLIT', 'ttHToNonbb_SPLIT'],
         #        '2018':['TTX_SPLIT','ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8', 'ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8',
         #                'TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8', 'TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8', 
         #                'TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8', 'TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
         #                'TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8', 'TTWJetsToLNu_SPLIT', 'TTZToLLNuNu_SPLIT', 'TTWJetsToQQ_SPLIT',
         #                'TTZToQQ_SPLIT', 'ttHTobb_SPLIT', 'ttHToNonbb_SPLIT'] } }
    'TT':{'2016':['TT_SPLIT'],
          '2017':['TTTo2L2Nu_SPLIT', 'TTToSemiLeptonic_SPLIT', 'TTToHadronic_SPLIT'],
          '2018':['TTTo2L2Nu_SPLIT', 'TTToSemiLeptonic_SPLIT', 'TTToHadronic_SPLIT']}}#from datasets
    
#loop over rootfiles

keys = {'TTHTobb':'ttHTobb',
        'TTHToNonbb':'ttHToNonbb',
        'TTG':'TTG',
        'TTZToLLNuNu':'TTZToLLNuNu',
        'TTZToQQ':'TTZToQQ',
        'TTWJetsToLNu':'TTWJetsToLNu',
        'TTWJetsToQQ':'TTWJetsToQQ',
        'TT': 'TT_SPLIT',
        'TTTo2L2Nu':'TTTo2L2Nu_SPLIT',
        'TTToSemiLeptonic':'TTToSemiLeptonic_SPLIT',
        'TTToHadronic':'TTToHadronic_SPLIT'}


proclist = procs['TT'][year]
key= keys[args.proc] 
chain = r.TChain('Events') 
nidx = 9
for process in proclist:
    # print 'process:', process
    procdir = eos+indir+process+'/'
    if args.typ == 'nom':
        nidx=1
    # weight = xw.GetKnownWeight(process, year)
    # print 'weight', weight

    for subdir, dirs, files in os.walk(procdir):
        # print subdir, dirs, files
        for i,f in enumerate(files):
            fname = subdir+'/'+f
            fname=fname.replace("/eos/uscms/","root://cmseos.fnal.gov/")
            if key not in fname:
                print key,'not in file...'
                continue
            print i, fname
            if nidx==9 and i==0 and args.typ=='pdf': #get # of indices for pdf
                rf = r.TFile.Open(fname)
                tree = rf.Get('Events')
                for i,evt in enumerate(tree):
                    if i==0:
                        newnidx = len(evt.LHEPdfWeight)
                        if newnidx>nidx:
                            nidx=newnidx
                    else:
                        break
                rf.Close()
            #add indexed variable to chains
            chain.Add(fname)
    print 'proc nidx', process, nidx

if args.typ=='pdf':
    if key=='TTG' and year=='2016':
        nidx=102
    if key=='ttHTobb' and (year=='2017' or year=='2016'):
        nidx=33
    if key=='TTZToQQ' and year=='2018':
        nidx=33
    if key=='ttHToNonbb' and year=='2017':
        nidx=100
    if key=='ttHToNonbb' and year=='2018':
        nidx=33
print 'nidx', nidx
norms= []
for i in range(nidx):
        # c = r.TCanvas("c","c",800,800)
    if args.typ=='ME':
        var ='LHEScaleWeight'
    elif args.typ=='pdf':
        var ='LHEPdfWeight'
    elif args.typ=='nom':
        var = 'genWeight'
    nomhist = r.TH1F('nomhist', 'nomhist', 1, -100, 100)
    nomhist.Sumw2()
    hist = r.TH1F('hist', 'hist', 1, -100, 100)
    hist.Sumw2()
    chain.Draw("1.0>>nomhist", "genWeight") 
    chain.Draw("1.0>>hist", var+"["+str(i)+"]*genWeight") 

    hyield = hist.Integral(0, hist.GetNbinsX()+1)
    nomyield = nomhist.Integral(0, nomhist.GetNbinsX()+1)
    print var+str(i)+"]", hyield, nomyield, 'NORM FACTOR:', nomyield/hyield            
    norms.append(nomyield/hyield)
        # c.SaveAs('./normhists/'+process+'_'+var+str(i)+'.png')
        # print 'saved', './normhists/'+process+'_'+var+str(i)+'.png'
    hist.SetDirectory(0)
    nomhist.SetDirectory(0)
    print args.typ, year, key
    print norms
    print '\n'



        # if args.typ!='nom':
            # chain.Draw(var+"["+str(i)+"]>>hist", "") #genweight for sign
            # chain.Draw(var+"["+str(i)+"]>>hist", "(nJet>=0)*LHEWeight_originalXWGTUP") #genweight for sign
            # chain.Draw(var+"["+str(i)+"]>>hist", var+"["+str(i)+"]*genWeight") #genweight for sign
            # chain.Draw(var+"["+str(i)+"]>>hist", "(nJet>=0)*(genWeight/abs(genWeight))*"+str(weight)+"*"+str(lumi)) #genweight for sign
        # else:
        
            # chain.Draw("genWeight>>hist", "genWeight") 
            # chain.Draw("genWeight>>hist", "(nJet>=0)*(1.0/abs(genWeight))*"+str(weight)+"*"+str(lumi)) #genweight for sign
            # chain.Draw(var+">>hist", "") #nominal weight

