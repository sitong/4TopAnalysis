 
xsecdict16 = { 
    #TTTT
    "TTTT_SPLIT":0.012,
    "TTTT_TuneCUETP8M2T4_13TeV-amcatnlo-pythia8":0.012,
    "TTTT_TuneCUETP8M2T4_PSweights_13TeV-amcatnlo-pythia8":0.012,
    "TTTT_TuneCUETP8M1_13TeV-amcatnlo-pythia8":0.012, #9.103e-03
    "TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8":5.5,
    # # TTJets_DiLept
    "TTJets_DiLept_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":5.669e+01,
    # #TTJets_SingleLeptFromT
    "TTJets_SingleLeptFromT_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.141e+02 ,
    # #TTJets_SingleLeptFromTbar
    "TTJets_SingleLeptFromTbar_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.138e+02,
    #### TTJets_HT
    "TTJets_HT-600to800_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.649e+00,
    "TTJets_HT-800to1200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":6.729e-01,
    "TTJets_HT-1200to2500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.196e-01,
    "TTJets_HT-2500toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.447e-03,
    # TTJets inclusive
    "TT_TuneCUETP8M2T4_13TeV-powheg-pythia8":831.76,
    # QCD
    "QCD_HT200to300_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1710000.0,
    "QCD_HT300to500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":348000.0,
    "QCD_HT500to700_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":32100.0,
    "QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":6830.0,
    "QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1210.0,
    "QCD_HT1500to2000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":120.9,
    "QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":25.3,
    # W+Jets
    "WJetsToQQ_HT400to600_qc19_3j_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":2.70e+02,
    "WJetsToQQ_HT600to800_qc19_3j_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":5.91e+01,
    "WJetsToQQ_HT-800toInf_qc19_3j_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":3.05e+01,
    "WJetsToLNu_HT-400To600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":4.91e+01,
    "WJetsToLNu_HT-600To800_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.21e+01,
    "WJetsToLNu_HT-800To1200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":5.47e+00,
    "WJetsToLNu_HT-1200To2500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.33e+00,
    "WJetsToLNu_HT-2500ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":3.21e-02,
    #Z+Jets
    "ZJetsToQQ_HT400to600_qc19_4j_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.15e+02,
    "ZJetsToQQ_HT600to800_qc19_4j_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":2.75e+01,
    "ZJetsToQQ_HT-800toInf_qc19_4j_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":1.48e+01,
    "ZJetsToNuNu_HT-400To600_13TeV-madgraph":3.590e+00,
    "ZJetsToNuNu_HT-600To800_13TeV-madgraph":8.57e-01, 
    "ZJetsToNuNu_HT-800To1200_13TeV-madgraph":3.94e-01,
    "ZJetsToNuNu_HT-1200To2500_13TeV-madgraph":9.55e-02,
    "ZJetsToNuNu_HT-2500ToInf_13TeV-madgraph":2.36e-03,
    # ttX
    "TTWJetsToLNu_TuneCP5_PSweights_13TeV-amcatnloFXFX-madspin-pythia8": 0.2043,
    "TTWJetsToQQ_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8": 0.4062,
    "TTZToLLNuNu_M-10_TuneCP5_PSweights_13TeV-amcatnlo-pythia8": 0.2529,
    "TTZToQQ_TuneCUETP8M1_13TeV-amcatnlo-pythia8":0.5297,
    "ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8":0.2934,
    "ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8":0.2934,
    "TTGJets_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8":3.697,
    "tZq_ll_4f_13TeV-amcatnlo-pythia8": 0.0758,
    "tZq_nunu_4f_13TeV-amcatnlo-pythia8_TuneCUETP8M1": 0.1379,
    "TTWJetsToQQ_SPLIT": 0.4062,
    "ttHTobb_SPLIT":0.2934,
    "ttHToNonbb_SPLIT":0.2934,
    "TTGJets_SPLIT":3.697,
    "TTZToLLNuNu_SPLIT": 0.2529,
    "TTZToQQ_SPLIT":0.5297,
    "TTWJetsToLNu_SPLIT": 0.2043,
    # singletop
    "ST_s-channel_4f_InclusiveDecays_13TeV-amcatnlo-pythia8":10.32,
    "ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1":35.85, 
    "ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1":35.85,
    "ST_t-channel_top_4f_inclusiveDecays_13TeV-powhegV2-madspin-pythia8_TuneCUETP8M1":136.02, 
    "ST_t-channel_antitop_4f_inclusiveDecays_13TeV-powhegV2-madspin-pythia8_TuneCUETP8M1":80.95, 
    # diboson
    "WW_TuneCUETP8M1_13TeV-pythia8":110.8,
    "WZ_TuneCUETP8M1_13TeV-pythia8":47.13,
    "ZZ_TuneCUETP8M1_13TeV-pythia8":16.523,
    #DY
    "DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8":6077.22,
    "DYJetsToLL_M-10to50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8":18610.0,
    # QCD genjets5
    # "QCD_HT300to500_GenJets5_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":70450,
    # "QCD_HT500to700_GenJets5_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":11300,
    # "QCD_HT700to1000_GenJets5_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":3030,
    # "QCD_HT1000to1500_GenJets5_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":623.4,
    # "QCD_HT1500to2000_GenJets5_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":67.43,
    # "QCD_HT2000toInf_GenJets5_TuneCUETP8M1_13TeV-madgraphMLM-pythia8":14.47,
    }

xsecdict17 = { 
    # "TTTT_TuneCP5_13TeV-amcatnlo-pythia8":0.012, #8.213e-03
    "TTTT_SPLIT":0.012, #8.213e-03
    "TTTT_TuneCP5_PSweights_13TeV-amcatnlo-pythia8_correctnPartonsInBorn":0.012, #8.213e-03
    "TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8":5.5,
    "TTJets_DiLept_TuneCP5_13TeV-madgraphMLM-pythia8":5.383e+01,
    # #TTJets_SingleLeptFromT
    "TTJets_SingleLeptFromT_TuneCP5_13TeV-madgraphMLM-pythia8":1.090e+02,
    # #TTJets_SingleLeptFromTbar
    "TTJets_SingleLeptFromTbar_TuneCP5_13TeV-madgraphMLM-pythia8":1.089e+02,
    #### TTJets_HT
    "TTJets_HT-600to800_TuneCP5_13TeV-madgraphMLM-pythia8":1.815e+00,
    "TTJets_HT-800to1200_TuneCP5_13TeV-madgraphMLM-pythia8":7.601e-01,
    "TTJets_HT-1200to2500_TuneCP5_13TeV-madgraphMLM-pythia8":1.302e-01,
    "TTJets_HT-2500toInf_TuneCP5_13TeV-madgraphMLM-pythia8":1.402e-03,
    # TTJets 831.76,
    "TTToHadronic_TuneCP5_13TeV-powheg-pythia8":     380.11,
    "TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8": 364.31,
    "TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8":        87.33,
    ##QCD
    "QCD_HT200to300_TuneCP5_13TeV-madgraph-pythia8":1712000,
    "QCD_HT300to500_TuneCP5_13TeV-madgraph-pythia8":347700,
    "QCD_HT500to700_TuneCP5_13TeV-madgraph-pythia8":32100,
    "QCD_HT700to1000_TuneCP5_13TeV-madgraph-pythia8":6831,
    "QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8":1207,
    "QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8":119.9,
    "QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8":25.24,
    ## no QCD genjets5
    # W+Jets 
    "WJetsToQQ_HT400to600_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8":3.16e+02,
    "WJetsToQQ_HT600to800_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8":6.87e+01,
    "WJetsToQQ_HT-800toInf_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8":3.47e+01,
    "WJetsToLNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8":59.181,
    "WJetsToLNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8":14.58,
    "WJetsToLNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8":6.656,
    "WJetsToLNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8":1.608,
    "WJetsToLNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8":0.0389,
    #Z+Jets
    "ZJetsToQQ_HT400to600_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8":1.45e+02,
    "ZJetsToQQ_HT600to800_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8":3.44e+01,
    "ZJetsToQQ_HT-800toInf_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8":1.85e+01,
    "ZJetsToNuNu_HT-400To600_13TeV-madgraph":1.31e+01,
    "ZJetsToNuNu_HT-600To800_13TeV-madgraph":3.26e+00,
    "ZJetsToNuNu_HT-800To1200_13TeV-madgraph":1.50e+00,
    "ZJetsToNuNu_HT-1200To2500_13TeV-madgraph":3.42e-01,
    "ZJetsToNuNu_HT-2500ToInf_13TeV-madgraph":5.15e-03,
    # ttX
    "TTWJetsToLNu_TuneCP5_PSweights_13TeV-amcatnloFXFX-madspin-pythia8":0.179245,
    "TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8":0.4062,
    "TTZToLLNuNu_M-10_TuneCP5_PSweights_correctnPartonsInBorn_13TeV-amcatnlo-pythia8":0.259032,
    "TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8":0.5297,
    "ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8":0.2934,
    "ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8":0.2151,
    "TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8":3.697,
    "tZq_ll_4f_ckm_NLO_TuneCP5_PSweights_13TeV-amcatnlo-pythia8":0.0758,
    "tZq_nunu_4f_ckm_NLO_TuneCP5_PSweights_13TeV-madgraph-pythia8":0.1379,
    "TTWJetsToQQ_SPLIT":0.4062,
    "ttHTobb_SPLIT":0.2934,
    "ttHToNonbb_SPLIT":0.2151,
    "TTGJets_SPLIT":3.697,
    "TTWJetsToLNu_SPLIT":0.179245,
    "TTZToLLNuNu_SPLIT":0.259032,
    "TTZToQQ_SPLIT":0.5297,
    # singletop
    "ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8":3.36,
    "ST_s-channel_4f_hadronicDecays_TuneCP5_13TeV-amcatnlo-pythia8":6.96,
    "ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8":35.85,
    "ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8":35.85,
    "ST_t-channel_top_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8":136.02,
    "ST_t-channel_antitop_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8":80.95,
    # diboson
    "WW_TuneCP5_13TeV-pythia8":118.7,
    "WZ_TuneCP5_13TeV-pythia8":47.13,
    "ZZ_TuneCP5_13TeV-pythia8":16.523,
   #DY
    "DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8":6077.22,
    "DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8":18610.0,
    }


xsecdict18 = { 
    "TTTT_TuneCP5_13TeV-amcatnlo-pythia8":0.012, #
    "TTTT_SPLIT":0.012, #
    "TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8":5.5,
    "TTJets_DiLept_TuneCP5_13TeV-madgraphMLM-pythia8":5.448e+01,
    # #TTJets_SingleLeptFromT
    "TTJets_SingleLeptFromT_TuneCP5_13TeV-madgraphMLM-pythia8":1.088e+02,
    # #TTJets_SingleLeptFromTbar 
    "TTJets_SingleLeptFromTbar_TuneCP5_13TeV-madgraphMLM-pythia8":1.089e+02,
    #### TTJets_HT
    "TTJets_HT-600to800_TuneCP5_13TeV-madgraphMLM-pythia8":1.815e+00,
    "TTJets_HT-800to1200_TuneCP5_13TeV-madgraphMLM-pythia8":7.538e-01,
    "TTJets_HT-1200to2500_TuneCP5_13TeV-madgraphMLM-pythia8":1.316e-01,
    "TTJets_HT-2500toInf_TuneCP5_13TeV-madgraphMLM-pythia8":1.418e-03,
    # TTJets 831.76,
    "TTToHadronic_TuneCP5_13TeV-powheg-pythia8":     380.11,
    "TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8": 364.31,
    "TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8":        87.33,
    ##QCD
    "QCD_HT200to300_TuneCP5_13TeV-madgraphMLM-pythia8":1712000,
    "QCD_HT300to500_TuneCP5_13TeV-madgraphMLM-pythia8":347700,
    "QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8":32100,
    "QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8":6831,
    "QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8":1207,
    "QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8":119.9,
    "QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8":25.24,
    ## no QCD genjets5
    # W+Jets
    "WJetsToQQ_HT400to600_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8":3.16e+02,
    "WJetsToQQ_HT600to800_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8":6.88e+01,
    "WJetsToQQ_HT-800toInf_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8":3.44e+01,
    "WJetsToLNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8":59.181,
    "WJetsToLNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8":14.58,
    "WJetsToLNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8":6.656,
    "WJetsToLNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8":1.608,
    "WJetsToLNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8":0.0389,
    #Z+Jets
    "ZJetsToQQ_HT400to600_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8":1.450e+02,
    "ZJetsToQQ_HT600to800_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8":3.380e+01,
    "ZJetsToQQ_HT-800toInf_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8":1.87e+01,
    "ZJetsToNuNu_HT-400To600_13TeV-madgraph":1.31e+01, 
    "ZJetsToNuNu_HT-600To800_13TeV-madgraph":3.26,
    "ZJetsToNuNu_HT-800To1200_13TeV-madgraph":1.50e+00,  
    "ZJetsToNuNu_HT-1200To2500_13TeV-madgraph":0.342,
    "ZJetsToNuNu_HT-2500ToInf_13TeV-madgraph":5.323e-03, 
    # ttX
    "TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8":0.179245, 
    "TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8":0.4062, 
    "TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8":0.259032, 
    "TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8":0.5297,
    "ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8":0.2934,
    "ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8":0.2151,
    "TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8":3.697,
    "tZq_ll_4f_ckm_NLO_TuneCP5_13TeV-madgraph-pythia8":0.0758,
    "tZq_nunu_4f_ckm_NLO_TuneCP5_PSweights_13TeV-mcatnlo-pythia8":0.1379,
    "TTWJetsToQQ_SPLIT":0.4062, 
    "ttHTobb_SPLIT":0.2934,
    "ttHToNonbb_SPLIT":0.2151,
    "TTGJets_SPLIT":3.697,
    "TTWJetsToLNu_SPLIT":0.179245, 
    "TTZToLLNuNu_SPLIT":0.259032, 
    "TTZToQQ_SPLIT":0.5297,
    # singletop
    "ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-madgraph-pythia8":3.36,     
    "ST_s-channel_4f_hadronicDecays_TuneCP5_13TeV-madgraph-pythia8":6.96,
    "ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8":35.85, 
    "ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8":35.85,
    "ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8":136.02,
    "ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8":80.95,
    # diboson
    "WW_TuneCP5_13TeV-pythia8":118.7,
    "WZ_TuneCP5_13TeV-pythia8":47.13,
    "ZZ_TuneCP5_13TeV-pythia8":16.523,
   #DY
    "DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8":6077.22,
    "DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8":18610.0,
    }
