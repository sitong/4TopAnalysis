#easier plotting
# import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
# import re
import ROOT as r
from ROCStuff import ROCCurve
import RootStuff as rs
import scipy.integrate as integrate

#plots multiple roc plots together
def ROCplots(distplot=True, printeffs=False):
    font = {'family': 'serif',
            'color':  'black',
            'weight': 'normal',
            'size': 16,
            }
    #PARAMS
    outputdir = './plots/evtROCs/'
    outname = 'rocs.png'
    # training = 'qcdandtt'#'onlytt'
    # rootdir = 'updatedrootfiles'
    # if training =='onlytt':
    #     rootdir = 'updatedrootfiles_TT'
    note = 'crosscheck'
    outfile = outputdir+outname
    # baseline = '(nbjets>=1)*weight*100.0'
    # baseline = '(nleptons==0 && ht>700 & njets>=9 && nbjets>=3 && nrestops>=1)*weight*36'
    baseline = '(nleptons==0 && ht>700 & njets>=9 && nbjets>=3)*weight*36'
    ssel = bsel =baseline
    treename = 'Events'

    # names  = ['ttttvsTT', 'ttttvsQCD', 'ttttvsTT+QCD']
    # deets for each plot
    # discvars = ['ModBDT1disc', 'ModBDT2disc', 'ModBDT1disc', 'ModBDT2disc', 'NewBDT3disc', 'NewBDT3disc']
    discvars = ['catBDTdisc', 'catBDTdisc']#, 'NewBDT3disc', 'NewBDT4disc']
    # names  = ['mod1 QCD+TT', 'mod2 QCD+TT', 'mod1 TT', 'mod2 TT', 'new3 QCD+TT', 'new3 TT']
    names  = ['QCD',  'TT']#, 'new_qgl', 'new_noqgl']

    # sigfiles = ['../eventMVA/updatedrootfiles/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TTTT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TTTT_withBDT_2016MC_merged_tree.root']

    # bkgfiles = ['../eventMVA/updatedrootfiles/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles/TT_withBDT_2016MC_merged_tree.root', '../eventMVA/updatedrootfiles_TT/TT_withBDT_2016MC_merged_tree.root']
    # /eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/updatedrootfiles
    sigfiles =['/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees/TTTT_2016_merged_0lep_tree.root', '/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees/TTTT_2016_merged_0lep_tree.root']
    bkgfiles =['/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees/QCD_2016_merged_0lep_tree.root','/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/MC/0lep/maintrees/TT_2016_merged_0lep_tree.root']
    # sigfiles = ['../eventMVA/updatedrootfiles_TT/TTTT_2016_merged_0lep_tree_withBDT.root']*len(discvars) 
    # bkgfiles = ['../eventMVA/updatedrootfiles_TT/TT_onlytt_BDTtest_merged_tree.root']*len(discvars)
    # sigfiles = ['../eventMVA/updatedrootfiles/TT_TuneCUETP8M2T4_13TeV-powheg-pythia8_0_tree_1lep_withBDT.root']*len(discvars) 
    # bkgfiles = ['../eventMVA/updatedrootfiles/QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_0_tree_1lep_withBDT.root']*len(discvars)

    # sigfiles = ['/eos/uscms/store/user/mquinnan/NanoAODv6_trees/0lep/skim_pieces/TTTT_2016_merged_0lep_tree_withBDT.root', '/eos/uscms/store/user/mquinnan/NanoAODv6_trees/0lep/skim_pieces/TTTT_2016_merged_0lep_tree_withBDT.root']
    # bkgfiles = ['/eos/uscms/store/user/mquinnan/NanoAODv6_trees/0lep/skim_pieces/QCD_2016_merged_0lep_tree_withBDT.root', '/eos/uscms/store/user/mquinnan/NanoAODv6_trees/0lep/skim_pieces/TT_2016_merged_0lep_tree_withBDT.root']
    

    indir =  ''
    #'/uscms_data/d3/mquinnan/Nano4top2/output/toptesting/'
    #'/eos/uscms/store/user/mquinnan/NanoAODv5/restop_training/toptesting/'
    print sigfiles
    print bkgfiles

    nsig = len(sigfiles)
    nbgd = len(bkgfiles)
    strees = ['']*nsig; srfiles = ['']*nsig
    btrees = ['']*nbgd; brfiles = ['']*nbgd
    rocs = []
    names = np.array(names)
    for i in range(len(discvars)):
        srfiles[i], strees[i] = rs.makeroot(sigfiles[i], treename)
        brfiles[i], btrees[i] = rs.makeroot(bkgfiles[i], treename)
        print srfiles[i], strees[i], brfiles[i], btrees[i]
        # snevts, syield = rs.getyield(strees[i], baseline)
        # bnevts, byield = rs.getyield(btrees[i], baseline)
        # print 'baseline', baseline, 'signal yield:', syield, 'background yield:', byield
        xmn = 0.0; xmx=1.0; nbns=50
        RC = ROCCurve(discvars[i], srfiles[i], brfiles[i], strees[i], btrees[i], ssel, bsel, nbins=nbns, xmin=xmn, xmax=xmx) #class instance
        RC.getInfoForSignalEff(0.5)
        RC.getInfoForSignalEff(0.0)

        nbins, seff, beff =RC.computeROCCurve()
        # for b,s in zip(beff, seff):
        #     print 'b',b,'s', s
        roc = r.TGraph(nbins, beff, seff)
        canvas = r.TCanvas("canvas", "canvas", 800, 800)
        roc.Draw('')
        # canvas.Update()
        # canvas.SaveAs(outputdir+'test.png')
        plt.plot(beff, seff, marker='.', label=names[i], linewidth=2)
        #get area under curve:
        area = -1.0*(np.trapz(seff, beff)) #negative because array in decending order
        print 'area under', names[i], 'ROC curve  is', area
        print names[i]
        rocs.append(roc)
        if distplot:
            getDists(RC.sighist, RC.bgdhist, title=names[i]+'_dist')#discvars[i]
            # title='eventBDT_disc_dist'; xtitle='BDT disc'; ytitle='N Entries'

    #plot things
    # c = r.TCanvas("c", "canvas", 800, 800)
    plt.xlabel('Background Efficiency', fontdict=font)
    plt.ylabel('Signal Efficiency', fontdict=font)
    plt.legend(loc='lower right')
    plt.grid()
    plt.savefig(outputdir+note+'ROC.png')
    print 'creating ', outputdir+note+'ROC.png'
    #multiplot does crazy shit for some reason??
    rocs = np.array(rocs)
    	# A cut of 0.49 corresponds to
	# signal efficiency = 90.355%
	# background efficiency = 66.867%

# plots sig and bkg disc distributions
# takes sig and bkg hists with discdist and selection already drawn! 
def getDists(sig, bkd, title='disc_dist', xtitle='BDT disc', ytitle='N Entries', normed=True):
    outputdir = './plots/evtROCs/'
    outfile = outputdir+title+".png"
    if normed:
        sig = rs.normhist(sig)
        bkg = rs.normhist(bkd)
    plots = [sig, bkd]
    names = ['signal', 'background']
    c = r.TCanvas("c", "canvas", 800, 800)
    rs.multiplot(outfile, plots, names, xtitle, ytitle, legend=True)
    # l.Draw('alsame')
    # # c.Show()
    # c.SaveAs()


ROCplots()

