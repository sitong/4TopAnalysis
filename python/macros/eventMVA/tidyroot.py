#skim tree pieces, flatten arrays, merge if desired. add bdts if desired. proper triggers
import subprocess
# import catboost as cat
from catboost import CatBoostClassifier
import varlists as vl
import os.path
from root_numpy import root2array, array2root, fill_hist, array2tree
from rootpy.tree import Tree, TreeModel, FloatCol
from rootpy.io import root_open
from rootpy.tree import Tree
import argparse
from array import array
import numpy as np
import pandas as pd
from root_pandas import to_root
import xgboost as xgb
import ROOT as r
import inspect
pd.options.mode.chained_assignment = None  # default='warn'


# print inspect.getmodule(xgb)
# print inspect.getmodule(cat)

parser = argparse.ArgumentParser(description='rootfile skimmer and more!')
#general:
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-t", "--treename", dest="treename", default='Events')
parser.add_argument("-o", "--outdir", dest="outdir", default='./updatedrootfiles/')
parser.add_argument("-v", "--verbose", dest="verbose", default='False')
#change weights
parser.add_argument("-w", "--changeweights", dest="changeweights", default='False')
#skimming:
parser.add_argument("-s", "--skim", dest="skim", default='True')
parser.add_argument("-c", "--cut", dest="cut", default='default') #if not specified will be default for 0 or 1 lep
#bdt:
parser.add_argument("-a", "--addbdt", dest="addbdt", default='False')
parser.add_argument("-C", "--fillcat", dest="fillcat", default='True')
parser.add_argument("-b", "--bdts", dest="bdts", default=['evtbdt']) #list of bdt discs to be added
parser.add_argument("-d", "--data", dest="isdata", default='False') #list of bdt discs to be added
#keep array branches:
parser.add_argument("-k", "--keeparr", dest="keeparr", default='False')
#merge:
parser.add_argument("-m", "--merge", dest="merge", default='False')
parser.add_argument("-y", "-year", dest="year", default='2016')
parser.add_argument("-r", "--removepieces", dest="rempieces", default='False') #remove smaller files aftter merge

args = parser.parse_args()

systs = ['nosys', 'jerUp', 'jerDown', 'jesUp', 'jesDown']

if args.isdata =='True':
    print 'WARNING: running nosys only'
    systs = ['nosys']

rename = True
        # self.systlist = ['nosys', 'jerUp', 'jerDown', 'jesUp', 'jesDown', 'btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down', 'DeepAK8TopSF_Up', 'DeepAK8TopSF_Down', 'DeepAK8WSF_Up', 'DeepAK8WSF_Down']


def Skim(args, syst='nosys'):
    # sel0lep = 'nleptons==0 && njets>=9 && nbjets>=3 && ht>=700'
    # sel0lep = 'nfunky_leptons==0 && njets>=9 && nbjets>=3 && ht>=700'

    sel0lep = 'nfunky_leptons==0 && njets_'+syst+'>=5 && nbjets_'+syst+'>=1 && ht_'+syst+'>=500'  #proper one, looser
    # sel0lep = 'nfunky_leptons==0 && njets_'+syst+'>=6 && nbjets_'+syst+'>=2 && ht_'+syst+'>=700'  #proper one
    # sel0lep = 'nfunky_leptons==0 && njets_'+syst+'>=2 && ht_'+syst+'>=1000' #very loose

    # sel1lep = 'nleptons>=1 && njets>=3 && nbjets==0'
    sel1lep = 'nleptons>=1 && njets>=3 && nbjets>=1'
    sel = args.cut
    if sel=='default':
        if '1lep' in f:
            sel = sel1lep
        elif '0lep' in f or 'train' in f or 'output/test' in f:
            sel = sel0lep
        else: 
            sel = sel0lep
    return sel

def getBDT(args, data, bdtdir = '/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/eventMVA/updated_training/'):
    bdtdict = { "evtbdt": vl.evtbdt,
                "bdt1": vl.bdt1,
                "bdt2": vl.bdt2,
                "bdt3": vl.bdt3,
                "bdt4": vl.bdt4,
                "new1": vl.new1,
                "new2": vl.new2,
                "new3": vl.new3,
                "new4": vl.new4,
                "best12":vl.best12 }
    for i,bdt in enumerate(args.bdts):
        print 'adding bdt', bdt
        vars = bdtdict[bdt]
        bdtname = vars['name']
        modelfile = bdtdir+bdtname+'_qcdandtt_eventBDT-xgb.model'
        print modelfile
        branchname = bdtname+'disc'
        #load training
        model = xgb.Booster() 
        model.load_model(modelfile)
        varsF = vars['floats']
        varsI = vars['ints']
        input_vars = varsF+varsI
        testset = data[input_vars] #input vars to bdt
        preds = model.predict(xgb.DMatrix(testset))
        print 'writing discriminant to tree in branch:', branchname
        #add bdt to tree
        data[branchname] = preds
    return data


def getCatBDT(args, data, syst, bdtfile='./BDT_ANdraft3'): #./new_BDT #./BDT_TT+QCD #newBDTwith16MC
    #loads a catboost, not xgboost, bdt file
    #data is a pandas dataframe
    from_file = CatBoostClassifier()
    model = from_file.load_model(bdtfile)
    # yout_q7   = model.predict_proba(xq7_val)[yq7_val==0,1]
    #inputs
    # "nJet", "nbJet", "met", "nsdw","metovsqrtht", "ht", "sumfatjetmass", "leadTpt1","sphericity", "aplanarity", "leadbpt", "bjht", "dphij1j2", "dphib1b2", "detaj1j2", "detab1b2", "jpt7th", "meanbdisc","htratio", "centrality"
    modi = ['njets_'+syst,'nbjets_'+syst,'met_'+syst,'nbsws_'+syst,'metovsqrtht_'+syst,'ht_'+syst,'sfjm_'+syst,'restop1pt_'+syst,'sphericity_'+syst,'aplanarity_'+syst,'leadbpt_'+syst,'bjht_'+syst,'dphij1j2_'+syst,'dphib1b2_'+syst,'detaj1j2_'+syst,'detab1b2_'+syst,'jet7pt_'+syst,'meanbdisc_'+syst,'htratio_'+syst,'centrality_'+syst]
    # modi = ['njets','nbjets','sphericity','met','aplanarity','nbsws','metovsqrtht','ht','restop1pt','leadbpt', 'bjht','dphij1j2','dphib1b2','detaj1j2','detab1b2','sfjm', 'centrality', 'htratio','meanbdisc', 'jet7pt']
    inputs = data[modi] #keep inputs needed for the bdt
    #have to rename variables appropriately
    inputs = inputs.rename(columns={"njets_"+syst: "nJet", 
                                    "nbjets_"+syst: "nbJet", 
                                    "met_"+syst: "met",                                     
                                    "nbsws_"+syst: "nsdw", 
                                    "metovsqrtht_"+syst: "metovsqrtht", 
                                    "ht_"+syst: "ht", 
                                    "sfjm_"+syst: "sumfatjetmass",
                                    "restop1pt_"+syst: "leadTpt1", 
                                    "sphericity_"+syst: "sphericity", 
                                    "aplanarity_"+syst: "aplanarity", 
                                    "leadbpt_"+syst: "leadbpt", 
                                    "bjht_"+syst: "bjht", 
                                    "dphij1j2_"+syst: "dphij1j2", 
                                    "dphib1b2_"+syst: "dphib1b2", 
                                    "detaj1j2_"+syst: "detaj1j2", 
                                    "detab1b2_"+syst: "detab1b2", 
                                    "jet7pt_"+syst: "jpt7th", 
                                    "meanbdisc_"+syst: "meanbdisc", 
                                    "htratio_"+syst: "htratio", 
                                    "centrality_"+syst: "centrality"})
    #get probablility the discriminant for those inputs is 1
    # print inputs["aplanarity"]
    # print '# inputs',len(modi)
    preds = model.predict_proba(inputs)[:,1]
    data['catBDTdisc_'+syst]=preds
    print 'filling catboost bdt...'
    return data


def changeweights(args, data, fstr):

    oldweights = data["weight"]
    # print data["weight"]
    newweights = data["weight"]
    #print newweights
    years = ['2016', '2017', '2018']
    weightmap = { '2016':{'TT_SPLIT':0.00593728510615,
                          'ttHTobb':3.05992190629e-05,
                          'ttHToNonbb':3.12300720687e-05,
                          'TTG':0.00234308700604,
                          'TTWJetsToLNu':.48402638422e-05,
                          'TTWJetsToQQ':0.000943970625828,
                          'TTZToLLNuNu':3.95776629867e-05,
                          'TTZToQQ':0.00150841202401},
                  '2017':{'ttHTobb':4.53727524714e-05,
                          'ttHToNonbb':3.08611376497e-05,
                          'TTG':0.00129531046458,
                          'TTWJetsToLNu':6.67295573836e-05,
                          'TTWJetsToQQ':0.000919920282634,
                          'TTZToLLNuNu':4.08326755986e-05,
                          'TTZToQQ':0.00148672695531},
                  '2018':{'ttHTobb':3.72255991843e-05,
                          'ttHToNonbb':2.91924917074e-05,
                          'TTG':0.00211280202582,
                          'TTWJetsToLNu':6.67307001428e-05,
                          'TTWJetsToQQ':0.00088646213877,
                          'TTZToLLNuNu':4.128627683e-05,
                          'TTZToQQ':0.00149116337205}}
    neww = 1.0
    found= False
    print 'FINDING WEIGHT'
    for year in years:
        if year in fstr:
            wm = weightmap[year]
            for proc, wgt in wm.iteritems():
                if proc in fstr:
                    found = True
                    print year, proc, 'new weight:',wgt
                    neww=wgt
                    break
            break
    if not found:
        print "PROC NOT FOUND WEIGHT UNCHANGED"
        return data

    for i,w in enumerate(oldweights):
        # print str(abs(w))
        # if str(abs(w)) in weightmap:
        #     neww = weightmap[str(abs(w))]
            # if w < 0.0:
        nw = (w/abs(w))*neww #to maintain sign
        # print 'changing', w, 'to ', nw
        newweights[i] = nw
    data["weight"] = newweights
    # print data["weight"]
    return data
    

# def flatbranch(branch, data):
# # alternative: use just root, draw to new tree
# # basically did this before
#     allvals = []
#     for b in data[branch]:
#         # print len(b)
#         vec = r.std.vector("float")(len(b))
#         for i, val in enumerate(b):
#             vec[i]=val
#         # print 'vec', vec
#         # allvals.append(np.asarray(b, dtype=np.float32))
#         allvals.append(vec)
#         # allvals+=(b[:].tolist())
#     fillvals = allvals
#     # fillvals = np.asarray(allvals, dtype=np.float32)
#     flatdata = pd.DataFrame()
#     flatdata[branch]=fillvals #create new dataframe for the branch
#     # flatdata[branch]=data[branch] #create new dataframe for the branch
#     data.drop(columns=branch) #delete old branch
#     # print fillvals
#     print flatdata.columns
#     return flatdata

# def Merge(args, filedir, files, channel, killbits):
#     #NOTE: this may cause problems if there are other files in the filedir not in the list
#     samples = {"TTTT": ,
#                "QCD":  ,
              
#                    }
#     cmd = "hadd "+samp+
#     # subprocess.call(cmd, shell=True)

#set everything up
outfiles = []

fillbdts=False; mergey=False; killbits=False; skimmy=False; changeweight=False
print "Will process", len(args.filenames), "files..."
if args.skim=='True':
    print 'Will skim files.'
    skimmy=True
if args.addbdt=='True':
    print 'Filling bdts:', args.bdts
    fillbdts=True
if args.changeweights=='True':
    print "will update weights in file if needed"
    changeweight=True
if args.merge=='True':
    print 'Will merge files. Deleting pieces?', args.rempieces
    mergey=True
    if args.rempieces=='True':
        killbits=True

#load files:
for f in args.filenames: #get proper outname:
    outf=f
    f=f.replace("/eos/uscms/","root://cmseos.fnal.gov/")
    print 'prepping input file', f, '...'
    outname = outf[outf.rfind('/')+1:]
    # if outname in mlist:
    #     print 'MINOR REDO:', outname
    # else:
    #     print 'NOT FOUND'
    #     continue
    outname = outname.strip('.root')

    if os.path.exists(args.outdir+outname):
        print 'File ',args.outdir+outname,'exists, skipping...'
        continue

    if args.keeparr=='True':
        rf = r.TFile.Open(f)
        t = rf.Get(args.treename)
        passdiscs = []
        for entry in t:
            # print t.discs
            # should save it in same format as before.  
            # nleptons==0 && njets>=6 && nbjets>=1' nleptons>=1 && njets>=3 && nbjets>=1'
            discs = []
            if ('1lep' in f and t.njets>=4 and t.nleptons==1 and t.nbjets>=1) or ('0lep' in f and t.njets>=6 and t.nleptons==0 and t.nbjets>=1):
                # passdiscs.append(t.discs)
                # print t.discs
                for d in t.discs:
                    # print d, t.njets, t.nleptons
                    discs.append(d)
            passdiscs.append(discs)

    sel = ''; note = ''
    if '1lep' in f:
        note='1lep'
    elif '0lep' in f:
        note='0lep'
    if '2016' in f:
        note+='_2016'
    elif '2017' in f:
        note+='_2017'
    elif '2018' in f:
        note+='_2018'
    if args.keeparr=='True':
        outname += '_'+note+'_withBDTplus.root' 
    elif args.keeparr=='False':
        if rename:
            outname += '_'+note+'_withBDT.root' 
        else:
            outname += '.root' 
    
    rf = r.TFile.Open(f)
    if rf.Get(args.treename).GetEntries()==0:
        print 'NO EVENTS. continuing'
        rf.Close()
        continue
    rf.Close()

    #get skim selection if needed:
    if skimmy:
        skim=Skim(args)
        print 'selection:', skim

    rf = r.TFile.Open(f)
    if rf.Get(args.treename).GetEntries()==0:
        print 'NO EVENTS. continuing'
        rf.Close()
        continue
    rf.Close()

    data = pd.DataFrame(root2array(f, args.treename, selection=skim))
    print '# input events:', len(data)
    if len(data)==0:
        print 'no skimmed events. skipping'
        continue

    badbranches = ["HLT_PFHT450_SixJet40_BTagCSV_p056", "HLT_PFHT400_SixJet30_DoubleBTagCSV_p056", "HLT_PFHT900", "HLT_Ele27_WPTight_Gsf", "HLT_IsoMu24", "HLT_IsoTkMu24", "HLT_PFHT430_SixJet40_BTagCSV_p080", "HLT_PFHT380_SixJet32_DoubleBTagCSV_p075", "HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2", "HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5","HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0", "HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59", "HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94", "HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5", "HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2"]
    if 'TTTT' not in f:
        badbranches+=['nLHEPart','detab1b2_DeepAK8WSF_Down','detab1b2_DeepAK8WSF_Up','LHE_NpLO']
    #delete fucked up branches
    for branch in badbranches:
        if branch in data:
            del data[branch]

    genlepbranches = ['ngen-taus','ngen-electrons', 'ngen-muons']
    new_genlepbranches = ['ngen_taus','ngen_electrons', 'ngen_muons']
    for i, branch in enumerate(genlepbranches):
        if branch in data:
            newname = new_genlepbranches[i]
            data[newname]=data[branch]
        
    #add BDT(s) if needed:
    if fillbdts:
        data = getBDT(args, data)
    
    if args.fillcat=='True':
        for syst in systs:
            data = getCatBDT(args, data, syst)

    if changeweight:
        data = changeweights(args, data, f)

    # flatdfs = []
    # for c in data.columns:
    #     if data[c].dtype=='object':
    #         print 'flattening array in rootfile for branch:', c
    #         flatdfs.append(flatbranch(c, data))


    array2root(data.to_records(index=False), filename=args.outdir+outname, treename=args.treename, mode='RECREATE')

    if args.keeparr=='True':
        #add the disc array to the tree
        print 'filling discs'

        outrf = r.TFile.Open(args.outdir+outname, "update")
        tree = outrf.Get(args.treename)
        
        #create branch
        nmax = 10
        nevts = tree.GetEntries()


        discs = np.full( nmax, -1.0, dtype=np.float32)
        tree.Branch('topdiscs', discs, 'topdiscs['+str(nmax)+']/F')
        # disclist = []
        for i,d in enumerate(passdiscs):
            ## for j,val in enumerate(d):
            # discs[i]=d
            # print len(d)
            # dval = array( 'f', [ len(d) ] )
            # evdiscs = np.full( nmax, -1.0, dtype=np.float32) #reinitialize
            count=0
            for disc in d:
                # print disc
                if disc>-1.0:
                    discs[count]=disc
                    # print disc, discs
                    count+=1
            tree.Fill()
            discs = np.full( nmax, -1.0, dtype=np.float32) #reinitialize
        tree.Write()
        outrf.Close()
                    
    print 'Wrote', args.outdir+outname
    # outfiles.append(args.outdir+outname)

# if mergey:
#     print 'Merging files...'
#     Merge(args, args.outdir, outfiles, channel='0lep', killbits)
#     Merge(args, args.outdir, outfiles, channel='1lep', killbits)
