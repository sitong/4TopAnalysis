import os
import numpy as np
import math
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoTrees.helpers.ConfigHelper import BoostSetter
from  PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper

#from https://twiki.cern.ch/twiki/bin/view/CMS/DeepAK8Tagging2018WPsSFs
#1% WP, nominal
#format [sf, sfup, sfdown]

#adds scale factors to fatjets
def GetBoostSFs(year, fatjets):

    SFs = {'2016': {'T' :{'pt300to400':[0.98, 1.10, 0.86],
                          'pt400to480':[0.90, 0.94, 0.86],
                          'pt480to600':[0.97, 1.01, 0.93],
                          'pt600to1200':[1.03, 1.10, 0.97] },
                    'W' :{'pt200to300':[1.13, 1.25, 1.03],
                          'pt300to400':[0.90, 1.00, 0.81],
                          'pt400to800':[0.73, 0.82, 0.64]  } },
           '2017': {'T' :{'pt300to400':[1.02, 1.13, 0.92],
                          'pt400to480':[1.02, 1.06, 0.98],
                          'pt480to600':[0.97, 1.01, 0.92 ],
                          'pt600to1200':[1.01, 1.08, 0.95] },
                    'W' :{'pt200to300':[1.03, 1.12, 0.93],
                          'pt300to400':[0.91, 0.99, 0.84],
                          'pt400to800':[0.88, 0.97, 0.80]  } },
           '2018': {'T' :{'pt300to400':[0.97, 1.04, 0.90],
                          'pt400to480':[1.06, 1.08, 1.02],
                          'pt480to600':[0.99, 1.02, 0.96],
                          'pt600to1200':[1.00, 1.05, 0.95] },
                    'W' :{'pt200to300':[0.97, 1.04, 0.90],
                          'pt300to400':[0.90, 0.96, 0.84],
                          'pt400to800':[0.91, 0.98, 0.84]  } }
    }

    sfdict = SFs[year]
    #boosted top settings
    BS = BoostSetter(year)
    boostWSF = np.array([1.0, 1.0, 1.0])
    boostTSF = np.array([1.0, 1.0, 1.0])
    
    for fj in fatjets:
        Wsfs=[1.0, 1.0, 1.0]
        Tsfs=[1.0, 1.0, 1.0]
        #w scale factors
        if (fj.pt>=BS.DAK8W_PT and abs(fj.eta)<BS.etacut):
            if fj.pt>=200.0 and fj.pt<300.0:
                Wsfs = sfdict['W']['pt200to300']
            elif fj.pt>=300.0 and fj.pt<400.0:
                Wsfs = sfdict['W']['pt300to400']
            elif fj.pt>=400.0 and fj.pt<800.0:
                Wsfs = sfdict['W']['pt400to800']

        if (fj.pt>=BS.DAK8TOP_PT and abs(fj.eta)<BS.etacut):
            if fj.pt>=300.0 and fj.pt<400.0:
                Tsfs = sfdict['T']['pt300to400']
            elif fj.pt>=400.0 and fj.pt<480.0:
                Tsfs = sfdict['T']['pt400to480']
            elif fj.pt>=480.0 and fj.pt<600.0:
                Tsfs = sfdict['T']['pt480to600']
            elif fj.pt>=600.0 and fj.pt<1200.0:
                Tsfs = sfdict['T']['pt600to1200']

        Wsfs = np.array(Wsfs); Tsfs = np.array(Tsfs)
        #element wise muliplication
        boostWSF*=Wsfs
        boostTSF*=Tsfs
        
        return boostWSF, boostTSF

        # fj.wsf=Wsfs[0]; fj.wsf_Up=Wsfs[1]; fj.wsf_down=Wsfs[2] 
        # fj.tsf=Tsfs[0]; fj.tsf_Up=Tsfs[1]; fj.tsf_down=Tsfs[2] 
    # return fatjets
