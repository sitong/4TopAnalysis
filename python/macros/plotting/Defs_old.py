import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

class plotdefs:
    def __init__(self, year, base, modbins=True):
        self.modbins = modbins
        self.year = year
        self.base = base
        self.lumi = str(rs.lumis[year]['0lep'])
        self.tn = 'Events'
        self.usevarbin=True
        self.edges = np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0], dtype=np.double)
        # self.edges = np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95, 1.0], dtype=np.double)
        # self.edges = np.array([0.0, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95, 1.0], dtype=np.double)
        
        self.shifts = [2, 10, 20, 30]
    
        self.mcprocs = ["TTTT", "TTX", "minor"]
        self.colors = [ r.kRed-7, r.kPink+2, r.kAzure+1, r.kMagenta-9, r.kViolet-4, r.kOrange+1, r.kYellow-7, r.kCyan-7, r.kBlue-9, r.kGreen-9]
        
        # if not modbins:
        NAFfiles = {'SR': '../combine/naf_March_09/naf_SR_'+year+'_Tree2.root',
                    'VR': '../combine/naf_March_09/naf_VR_'+year+'_Tree2.root'}
        # NAFfiles = {'SR': '../combine/naf_Feb_15/naf_SR_'+year+'_Tree.root',
        #             'VR': '../combine/naf_Feb_15/naf_VR_'+year+'_Tree.root'}
        # NAFfiles = {'SR': '../combine/NAF_jan8/naf_SR_'+year+'_Tree.root',
        #             'VR': '../combine/NAF_jan8/naf_VR_'+year+'_Tree.root'}
        self.NAFfile = r.TFile(NAFfiles[base])
        self.NAFfilename = NAFfiles[base]

        datafiles = {'2016':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',
                     '2017':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',
                     '2018':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',
                     'ALL':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root',}
        self.datafile = r.TFile(datafiles[year])
        self.datafilename = datafiles[year]

        self.QCD_TT_file =  '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/QCD_TT_'+year+'_merged_0lep_tree.root'

        metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
        metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'

        self.mcfiles = {'TTTT': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/TTTT_'+year+'_merged_0lep_tree.root',
                        'TTX' : '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/TTX_'+year+'_merged_0lep_tree.root',
                        'minor':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/minorMC_'+year+'_merged_0lep_tree.root',
                        'MC': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_bSFcorradded/MC_'+year+'_merged_0lep_tree.root'}

        self.dstr = {'2016' : '&& (trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || trigHLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
                     '2017' : '&& (trigHLT_PFHT430_SixJet40_BTagCSV_p080 || trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || trigHLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,
                     '2018' : '&& (trigHLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || trigHLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||trigHLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,
                     'ALL' : '&& (passtrig)'+metfilters,}

        baseline = {'VR':'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==8',
                    'SR':'nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9'}

        # self.mcstr = {'2016' : metfilters16+')*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
        #               '2017' : metfilters+')*weight*btagSF_nosys*puWeight*fixtrigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
        #               '2018' : metfilters+')*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
        #               'ALL' : '&& METFilters )*weight*btagSF_nosys*puWeight*TrigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,}
        self.mcstr = {'2016' : metfilters16+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
                      '2017' : metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*fixtrigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
                      '2018' : metfilters+')*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,
                      'ALL' : '&& METFilters )*weight*btagSF_nosys*bSFcorr_nosys*puWeight*TrigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+self.lumi,}

        binsels = {'cut0bin0':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=700 && ht_nosys<800',
                   'cut0bin1':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=800 && ht_nosys<900', 
                   'cut0bin2':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=900 && ht_nosys<1000', 
                   'cut0bin3':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1000 && ht_nosys<1100',
                   'cut0bin4':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1100 && ht_nosys<1200',
                   'cut0bin5':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1200 && ht_nosys<1300',
                   'cut0bin6':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1300 && ht_nosys<1500', 
                   'cut0bin7':'nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1500',
                   'cut1bin0':'nrestops_nosys==1 && nbstops_nosys>=1 && ht_nosys<1400',
                   'cut1bin1':'nrestops_nosys==1 && nbstops_nosys>=1 && ht_nosys>=1400'}
        if modbins:
            binsels['cut2bin0']='nrestops_nosys>=2 && nbstops_nosys>=0' 
        else:
            binsels['cut2bin0']='nrestops_nosys>=2 && nbstops_nosys>=0 && ht_nosys<1100'
            binsels['cut2bin1']='nrestops_nosys>=2 && nbstops_nosys>=0 && ht_nosys>=1100'

        self.binsels = binsels
        self.basesel = baseline[base]
        basesel_nob = baseline[base].replace(' && nbjets_nosys>=3', '')
        self.basesel_nob = basesel_nob

        outdir = './plots/predhists/'+base+year+'/'
        # outdir = './plots/systhists/'+base+year+'/'
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        self.outdir = outdir

        extDDyields = {'2016' : {'VR' : { "cut0bin0": [ 3079.213 , 104.844 ],
                                          "cut0bin1": [ 2092.384 , 88.782 ],
                                          "cut0bin2": [ 1607.809 , 85.759 ],
                                          "cut0bin3": [ 1064.694 , 72.095 ],
                                          "cut0bin4": [ 923.237 , 77.973 ],
                                          "cut0bin5": [ 574.885 , 64.223 ],
                                          "cut0bin6": [ 477.697 , 51.802 ],
                                          "cut0bin7": [ 458.426 , 62.906 ],
                                          "cut1bin0": [ 350.862 , 36.514 ],
                                          "cut1bin1": [ 129.763 , 31.142 ],
                                          "cut2bin0": [ 584.268 , 50.8 ]},
                                 'SR' : { "cut0bin0": [ 1463.936 , 64.438 ],
                                          "cut0bin1": [ 1441.295 , 72.106 ],
                                          "cut0bin2": [ 1076.445 , 63.613 ],
                                          "cut0bin3": [ 989.389 , 69.217 ],
                                          "cut0bin4": [ 640.184 , 54.083 ],
                                          "cut0bin5": [ 456.754 , 48.8 ],
                                          "cut0bin6": [ 663.752 , 66.274 ],
                                          "cut0bin7": [ 708.539 , 80.637 ],
                                          "cut1bin0": [ 184.436 , 25.015 ],
                                          "cut1bin1": [ 263.052 , 54.089 ],
                                          "cut2bin0": [ 469.543 , 45.345 ]} },
                       '2017' : {'VR' : { "cut0bin0": [ 3797.352 , 136.758 ],
                                          "cut0bin1": [ 2736.093 , 121.747 ],
                                          "cut0bin2": [ 2219.858 , 124.492 ],
                                          "cut0bin3": [ 1213.493 , 85.639 ],
                                          "cut0bin4": [ 972.348 , 88.965 ],
                                          "cut0bin5": [ 593.31 , 69.858 ],
                                          "cut0bin6": [ 662.541 , 80.443 ],
                                          "cut0bin7": [ 506.099 , 78.422 ],
                                          "cut1bin0": [ 433.791 , 46.29 ],
                                          "cut1bin1": [ 170.154 , 41.706 ],
                                          "cut2bin0": [ 821.76 , 74.195 ]},
                                 'SR' : { "cut0bin0": [ 1939.244 , 82.263 ],
                                          "cut0bin1": [ 1795.414 , 86.224 ],
                                          "cut0bin2": [ 1193.359 , 67.963 ],
                                          "cut0bin3": [ 1132.258 , 77.038 ],
                                          "cut0bin4": [ 700.188 , 58.831 ],
                                          "cut0bin5": [ 510.998 , 52.971 ],
                                          "cut0bin6": [ 677.904 , 68.579 ],
                                          "cut0bin7": [ 595.47 , 70.277 ],
                                          "cut1bin0": [ 283.106 , 36.718 ],
                                          "cut1bin1": [ 428.509 , 80.807 ],
                                          "cut2bin0": [ 548.215 , 52.361 ]} },
                       '2018' : {'VR' : { "cut0bin0": [ 4878.239 , 149.606 ],
                                          "cut0bin1": [ 3561.494 , 134.111 ],
                                          "cut0bin2": [ 2799.008 , 134.558 ],
                                          "cut0bin3": [ 1772.491 , 108.939 ],
                                          "cut0bin4": [ 1080.053 , 84.665 ],
                                          "cut0bin5": [ 766.237 , 78.469 ],
                                          "cut0bin6": [ 688.026 , 71.482 ],
                                          "cut0bin7": [ 633.101 , 88.524 ],
                                          "cut1bin0": [ 733.211 , 62.743 ],
                                          "cut1bin1": [ 181.385 , 38.724 ],
                                          "cut2bin0": [ 1120.11 , 85.528 ]},
                                 'SR' : { "cut0bin0": [ 2648.844 , 97.648 ],
                                          "cut0bin1": [ 2353.241 , 97.541 ],
                                          "cut0bin2": [ 2024.165 , 97.935 ],
                                          "cut0bin3": [ 1506.351 , 88.398 ],
                                          "cut0bin4": [ 1223.197 , 87.637 ],
                                          "cut0bin5": [ 991.664 , 87.078 ],
                                          "cut0bin6": [ 1090.559 , 93.707 ],
                                          "cut0bin7": [ 919.235 , 94.206 ],
                                          "cut1bin0": [ 433.497 , 45.683 ],
                                          "cut1bin1": [ 494.323 , 82.33 ],
                                          "cut2bin0": [ 619.241 , 52.859 ]} },
                       'ALL':{'SR':{}}}

        if not modbins:

            extDDyields['2016']['VR']['cut2bin0']=[580.887, 50.548]#ALL MOD
            extDDyields['2017']['VR']['cut2bin0']=[815.609, 73.386]
            extDDyields['2018']['VR']['cut2bin0']=[1118.487, 85.47]

            extDDyields['2016']['VR']['cut2bin1']=[580.887, 50.548]#ALL MOD
            extDDyields['2017']['VR']['cut2bin1']=[815.609, 73.386]
            extDDyields['2018']['VR']['cut2bin1']=[1118.487, 85.47]

            extDDyields['2016']['SR']['cut2bin0']=[ 469.543 , 45.345 ]
            extDDyields['2017']['SR']['cut2bin0']=[ 548.215 , 52.361 ]
            extDDyields['2018']['SR']['cut2bin0']=[ 619.241 , 52.859 ]

            extDDyields['2016']['SR']['cut2bin1']=[ 262.817 , 55.318 ]
            extDDyields['2017']['SR']['cut2bin1']=[ 318.736 , 64.432 ]
            extDDyields['2018']['SR']['cut2bin1']=[ 350.532 , 59.38 ]

        
        extDDyields['ALL']['SR']=extDDyields['2016']['SR']
        for key, val in extDDyields['ALL']['SR'].iteritems():
            val += extDDyields['2017']['SR'][key] + extDDyields['2018']['SR'][key]

        self.extDDyields = extDDyields[year][base]

        VRerr = {'2016':{'cut0bin0':0.081,                                                     
                         'cut0bin1':0.086,
                         'cut0bin2':0.120,
                         'cut0bin3':0.136,
                         'cut0bin4':0.214,
                         'cut0bin5':0.240,
                         'cut0bin6':0.175,
                         'cut0bin7':0.117,
                         'cut1bin0':0.277,
                         'cut1bin1':0.300,
                         'cut2bin0':0.261,
                         'cut2bin1':0.261},
                 '2017':{'cut0bin0':0.107,                                                                                                     
                         'cut0bin1':0.066,
                         'cut0bin2':0.214,
                         'cut0bin3':0.120,
                         'cut0bin4':0.195,
                         'cut0bin5':0.153,
                         'cut0bin6':0.206,
                         'cut0bin7':0.209,
                         'cut1bin0':0.227,
                         'cut1bin1':0.293,
                         'cut2bin0':0.129,
                         'cut2bin1':0.129},
                 '2018':{'cut0bin0':0.051,                                                                                                      
                         'cut0bin1':0.060,
                         'cut0bin2':0.094,
                         'cut0bin3':0.050,
                         'cut0bin4':0.136,
                         'cut0bin5':0.082,
                         'cut0bin6':0.180,
                         'cut0bin7':0.154,
                         'cut1bin0':0.188,
                         'cut1bin1':0.460,
                         'cut2bin0':0.105,
                         'cut2bin1':0.105},
                 'ALL':{}}

        VRerr['ALL']=VRerr['2016']
        for key, val in VRerr['ALL'].iteritems():
            val = np.sqrt(val**2 + VRerr['2017'][key]**2+VRerr['2018'][key]**2)

        self.VRerr = VRerr

    def getyield(self, hist, verbose=False):
        errorVal = r.Double(0)
        minbin=0
        maxbin=hist.GetNbinsX()+1
        hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
        if verbose:
            print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
        return hyield,  errorVal
        
    def getsels(self, chanid):
        binsel = self.binsels[chanid]
        sel = '('+binsel+' && '
        mcsel = sel + self.basesel + self.mcstr[self.year]
        datasel = sel + self.basesel + self.dstr[self.year]+')'
        mysels = {'sel':sel, 'mcsel':mcsel, 'datasel':datasel}
        return mysels
            
    def gethist(self, filename, projvar, cut):
        varbin=self.usevarbin
        nbins, selrange = self.getHistSettings(projvar)
        rf, t = rs.makeroot(filename, self.tn)
        if not varbin:
            hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
        else:
            hist = r.TH1F('hist', 'hist', nbins, self.edges)
        t.Draw(projvar+">>hist", cut)
        rs.fixref(rf, hist)
        return hist

    def histfromtree(self, chanid, proc='DDBKG', varname='catBDTdisc_nosys'):
        varbin=self.usevarbin
        binalias = {'cut0bin0':'0',
                    'cut0bin1':'1',
                    'cut0bin2':'2',
                    'cut0bin3':'3',
                    'cut0bin4':'4',
                    'cut0bin5':'5',
                    'cut0bin6':'6',
                    'cut0bin7':'7',
                    'cut1bin0':'10',
                    'cut1bin1':'11',
                    'cut2bin0':'20',
                    'cut2bin1':'21'}# for reading trees
        doscale = False 
        var = 'BDT_dsic'
        if proc == 'DDBKG':
            doscale=True
        else:
            print 'proc must be data, TTTT or DDBKG'
        treefile = self.NAFfilename
        mybin = binalias[chanid]
        print treefile, 'bin',mybin
        nbins, selrange =  self.getHistSettings(varname)
        if self.modbins and chanid=='cut2bin0':
            # #need to draw bin 21 into histogram as well if modbins and chan 20!
            cut = '((bin==20 || bin==21) && '+var+'>=0.0 && '+var+'<=1.0)'
        else:
            cut = '(bin=='+mybin+' && '+var+'>=0.0 && '+var+'<=1.0)'
        print cut
        # cutovfl = '(bin=='+mybin+')'
        rf, t = rs.makeroot(treefile, self.tn)
        # histovfl = rs.gethist(t, var, cutovfl, nbins, selrange)
        if not varbin:
            hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
        else:
            hist = r.TH1F('hist', 'hist', nbins, self.edges)
        # histovfl = r.TH1F('histovfl', 'histovfl', nbins, selrange[0], selrange[1])
        t.Draw(var+">>hist", cut)
        rs.fixref(rf, hist)
        # t.Draw(var+">>histovfl", cutovfl)
        # rs.fixref(rf, histovfl)
        if doscale:
            yld, unc = self.getyield(hist)
            # yldovfl, uncovfl = self.getyield(histovfl)
            print 'yield', yld#, 'overflow', yldovfl-yld
            hist.Scale(self.extDDyields[chanid][0]/yld)
        return hist

    def getHistSettings(self, var):
        vardict = {
            #[nbins, xmin, xmax]
            'catBDTdisc': [10, 0.0, 1.0],
            'catBDTdisc_manual_nosys': [10, 0.0, 1.0],
            'catBDTdisc_nosys': [10, 0.0, 1.0],
            'catBDTdisc_jerUp': [10, 0.0, 1.0],
            'catBDTdisc_jerDown': [10, 0.0, 1.0],
            'catBDTdisc_jesUp': [10, 0.0, 1.0],
            'catBDTdisc_jesDown': [10, 0.0, 1.0],
            'catBDTdisc_btagLFUp': [10, 0.0, 1.0],
            'catBDTdisc_btagHFUp': [10, 0.0, 1.0],
            'catBDTdisc_btagLFDown': [10, 0.0, 1.0],
            'catBDTdisc_btagHFDown': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats1Up': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats1Up': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats1Down': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats1Down': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats2Up': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats2Up': [10, 0.0, 1.0],
            'catBDTdisc_btagLFstats2Down': [10, 0.0, 1.0],
            'catBDTdisc_btagHFstats2Down': [10, 0.0, 1.0],
            'ht'      : [20, 0.0, 3000.0],
            'bjht'    : [50, 0, 1500],
            'met'     : [50, 0, 1000],
            'metovsqrtht' : [50, 0.0, 200.0],
            'sfjm'    : [100, 0, 1000],
            'njets'   : [20, 0, 20],
            'nbjets'  : [10, 0, 10],
            'nbsws'   : [10, 0, 10],
            'nrestops': [6, 0, 6],
            'nbstops' : [6, 0, 6],
            'nleptons': [20, 0, 20],
            'fwm0'  : [50, 0, 1],
            'fwm1'  : [50, 0, 1],
            'fwm2'  : [50, 0, 1],
            'fwm3'  : [50, 0, 1],
            'fwm4'  : [50, 0, 1],
            'fwm5'  : [50, 0, 1],
            'fwm6'  : [50, 0, 1],
            'fwm7'  : [50, 0, 1],
            'fwm8'  : [50, 0, 1],
            'fwm9'  : [50, 0, 1],
            'fwm10'  : [50, 0, 1],
            'fwm20'  : [50, 0, 1], 
            'fwm30'  : [50, 0, 1],
            # 'topdiscs'  : [50, -1, 1],
            'rescand_disc'  : [50, 0.95, 1],
            'rescand_pt'  : [20, 0.0, 1000],
            'jet1pt'  : [50, 0, 1000],
            'jet2pt'  : [50, 0, 1000],
            'bstop1_pt'  : [50, 100, 1000],
            'bstop2_pt'  : [50, 100, 1000],
            'restop1pt'  : [50, 100, 1000],
            'restop2pt'  : [50, 100, 1000],
            'qglsum'  : [50, 0, 30],
            'qglprod'  : [50, 0, 30],
            'jet8pt'  : [50, 0, 200],
            'aplanarity'  : [20, 0, 1],
            'sphericity'  : [20, 0, 1],
            'dphij1j2'  : [10, -4, 4],
            'detaj1j2'  : [10, -4, 4],
            'dphib1b2'  : [10, -4, 4],
            'detab1b2'  : [10, -4, 4],
            'leadbpt' : [50, 0, 500],
            'npv' : [50, 0, 100],
            'lep1pt'  : [50, 0, 250],
            'lep2pt'  : [50, 0, 250],
            'ModBDT2disc': [100, 0.0, 1.0],
            'evtbdtdisc': [100, 0.0, 1.0],
            'NewBDT3disc': [100, 0.0, 1.0],
            'NewBDT4disc': [100, 0.0, 1.0],
            'Best12disc': [100, 0.0, 1.0],
            'passtrigel': [2, 0,1], 
            'passtrigmu': [2, 0,1]}
        params = vardict[var]
        nbins = params[0]
        selrange = (params[1], params[2])
        if self.usevarbin:
            nbins= len(self.edges)-1
        return nbins, selrange

