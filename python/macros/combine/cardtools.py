# sets up some useful things for sensitivity study, like distributions and cutflow tables. 
# uses same disctionary and files that go into datacards
import os
import math
import numpy as np
import argparse
import uproot
import types
from collections import OrderedDict
import uproot_methods.classes.TH1
from ROOT import gROOT,TFile,TTree,TH1D, gStyle
import ROOT as r
from datacard_dictionary import datacard_dict, getprocesses, datafile
from histmaker import histmaker
gROOT.SetBatch(True)
gStyle.SetOptStat(0)

class cardtools:
    def __init__(self, cuts, bins, baseline, year='2016'):
        lumi=36
        if year=='2016':
            lumi=36
        elif year=='2017':
            lumi=42
        elif year=='2018':
            lumi=60

        self.year = year
        self.lumi = lumi
        self.cuts  = cuts
        self.bins  = bins
        self.base = baseline
        #get bins for datacard sets from datacard dictionary
        
        self.processes = getprocesses(year)        
        
        self.treename = 'Events'
        self.datafile = datafile
        self.projvar = 'ht'
        self.blind = False
        if datafile=='none' or '.root' not in datafile:
            print 'no datafile input, so no datafile will be used'
            self.blind = True
        print 'INFO: cuts', cuts, 'bins', bins, 'lumi', lumi, 'baseline', baseline, 'use data?', self.blind


    def runshortprocs(self, varlist, outdir='./'):
        for cut in self.cuts:
            for bin in self.bins:
                print 'producing dists for', cut, bin
                self.short_procs(cut, bin, varlist, outdir)

    def runNAFdists(self, outdir='./'):
        for cut in self.cuts:
            for bin in self.bins:
                print 'producing dists for', cut, bin
                self.NAF_dists(cut, bin, outdir)


    def rungetdists(self, varlist, outdir='./'):
        for cut in self.cuts:
            for bin in self.bins:
                print 'producing dists for', cut, bin
                self.getdists(cut, bin, varlist, outdir)

    def runnormdists(self, varlist, outdir='./'):
        for cut in self.cuts:
            for bin in self.bins:
                print 'producing dists for', cut, bin
                self.getdists(cut, bin, varlist, outdir, False, True)

    def rungetcutflow(self):
        for cut in self.cuts:
            for bin in self.bins:
                print 'producing cutflow tables for', cut, bin
                self.getcutflow(cut, bin)
        
    def getdists(self, cut, bin, varlist, outdir='./', logscale=True, usenorm=False):
        #norm is TTTT vs background and normalization
            #plots distributions in datacard bins
            # varlist=['nrestops', 'nbstops', 'ht', 'njets', 'nbjets', 'catBDTdisc']
        # colors = [r.kRed-7, r.kOrange+1, r.kYellow-7, r.kGreen-9, r.kCyan-7, r.kAzure+1, r.kBlue-9, r.kViolet-4, r.kMagenta-9] #rainbow
        colors = [r.kCyan-10, r.kAzure-9, r.kBlue-9, r.kViolet-9, r.kMagenta-10, r.kPink+1, r.kRed-9, r.kOrange-9, r.kYellow-9] #pastel pinky
        nbins = {'ht':25,
                 'nrestops':5,
                 'nbstops':5,
                 'njets':20,
                 'nbjets':20,
                 'catBDTdisc':10,
                 }
        selrange = {'ht':[500,2500],
                    'nrestops':[0,5],
                    'nbstops':[0,5],
                    'njets':[0,20],
                    'nbjets':[0,20],
                    'catBDTdisc':[0,1],
                    }
        
            #set up output directory
        outdir = outdir+cut+bin+'dists'
        if not os.path.exists(outdir):
            os.makedirs(outdir); print 'Making output directory ', outdir
            
            #get selections for this top and kinbin category from the datacard dictionary
        cutlist = datacard_dict(self.base, cut, bin, self.lumi, 'weight')

            #make histogram for each variable in each bin
        for chanid, chansel in cutlist.iteritems():
            print 'channel:', chanid, 'selection:', chansel
            for var in varlist:
                print 'plotting var', var
                stack = r.THStack("stack", "stack")
                c1 = r.TCanvas()
                c1.cd()
                legend = r.TLegend(0.1, 0.6, 0.3, 0.9)
                
                hists = []
                bnorm = 0.0; snorm = 0.0
                for proc, procfile in self.processes.iteritems():
                    histname = var+'_'+proc+'_'+chanid+'_'+cut+bin+'hist'
                    print 'adding hist', histname, 'to stack with selection', chansel
                    pfile = r.TFile(procfile)
                    ptree = pfile.Get(self.treename)
                    phist = r.TH1F(histname, histname, nbins[var], selrange[var][0], selrange[var][1])
                    ptree.Draw(var+'>>'+histname, chansel)#apply bin selection
                    phist.SetDirectory(0)
                    if usenorm:
                        if proc=='TTTT':
                            snorm += phist.Integral()
                        else: 
                            bnorm+= phist.Integral()
                        print 'calc norm', snorm, bnorm
                    hists.append(phist)

                #h->Scale(factor/h->Integral());
                procnames = self.processes.keys()
                #reorder so TTTT is last
                
                if not logscale and not usenorm: 
                    hists.append(hists[0])
                    del hists[0]
                    procnames.append(procnames[0])
                    del procnames[0]
                elif usenorm:
                    shist = hists[0]
                    shist.Scale(1.0/shist.Integral())
                    del hists[0]
                    del procnames[0]
                print procnames

                for i,(proc, phist) in enumerate(zip(procnames, hists)):
                    phist.SetFillColor(colors[i])
                    phist.SetLineWidth(1)
                    phist.SetLineColor(r.TColor.GetColorDark(colors[i]))
                    legend.AddEntry(phist, proc, "F")           
                    if usenorm:
                        phist.Scale(1.0/bnorm)
                    stack.Add(phist)

                c1.cd()
                stack.Draw("histe1")
                if usenorm:
                    shist.Draw("histe1same")
                    shist.SetLineWidth(2)
                    shist.SetLineColor(r.kGray+2)
                    legend.AddEntry(shist, "TTTT", "L")           
                r.gPad.Modified()
                r.gPad.SetGrid()
                # c1.Update()
                stack.GetXaxis().SetTitle(var)
                stack.GetYaxis().SetTitle('NEntries')
                if not usenorm:
                    stack.SetMaximum(100)
                else:
                    stack.SetMaximum(0.5)

                stack.SetTitle(var+" "+chanid+" "+cut+bin+" histograms")
                legend.Draw()

                outname = outdir+"/"+var+"_"+chanid+"_"+cut+bin+"_histograms"+".png"
                # c.Draw()
                # c1.Modified()
                # c.Update()
                if logscale:
                    c1.SetLogy()
                c1.SaveAs(outname)
                print 'created histogram', outname
                        

    def NAF_dists(self, cut, bin, outdir='./', VR=True, logscale=True):
        TTTTfile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+self.year+'/MC/0lep/maintrees_newWP2/TTTT_'+self.year+'_merged_0lep_tree.root'
        BGDfile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+self.year+'/MC/0lep/maintrees_newWP2/BKGD_'+self.year+'_merged_0lep_tree.root'
        datafile = '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+self.year+'/MC/0lep/maintrees_newWP2/JETHT_'+self.year+'_merged_0lep_tree.root'
        nbins = 10
        selrange = [0,1]
        var = 'catBDTdisc'

        outdir = outdir+cut+bin+'NAFdists'
        if not os.path.exists(outdir):
            os.makedirs(outdir); print 'Making output directory ', outdir

        histfile=''
        if cut=='BOTH':
            if bin=='tops':
                histfile = 'NAF_tops_2016_shapehists.root'
            elif bin=='ht':
                histfile = 'NAF_ht_2016_shapehists_12bins_oct22.root'
            if VR: 
                histfile = 'NAF_8jetVR_2016_shapehists_12binv2.root'
        else: 
            print 'no NAF hist!'

        cutlist = datacard_dict(self.base, cut, bin, self.lumi, 'weight')        
        for chanid, chansel in cutlist.iteritems():
            print 'channel:', chanid, 'selection:', chansel
            outname = var+'_'+chanid+'_'+cut+bin+'hist'
            c1 = r.TCanvas("c1","multipads",700,700)
            c1.cd()
            legend = r.TLegend(0.1, 0.6, 0.3, 0.9)
            histname = 'TTTT_'+outname
            print 'adding hist', histname, ' with selection', chansel
            pfile = r.TFile(TTTTfile)
            ptree = pfile.Get(self.treename)
            phist = r.TH1F(histname, histname, nbins, selrange[0], selrange[1])
            ptree.Draw(var+'>>'+histname, chansel)#apply bin selection

            bhistname = 'MC_BGD_'+outname
            print 'adding hist', bhistname, ' with selection', chansel
            bfile = r.TFile(BGDfile)
            btree = bfile.Get(self.treename)
            bhist = r.TH1F(bhistname, bhistname, nbins, selrange[0], selrange[1])
            
            btree.Draw(var+'>>'+bhistname, chansel)#apply bin selection

            if VR:
                trigs = '&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1) && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
                dhistname = 'DATA_'+outname
                dfile = r.TFile(datafile)
                dtree = dfile.Get(self.treename)
                dhist = r.TH1F(dhistname, dhistname, nbins, selrange[0], selrange[1])
                dsel = chansel.split("*")[0]
                dsel = dsel.replace('*', '')
                dsel = dsel+trigs
                # print 'dsel:', dsel
                print 'adding hist', dhistname, ' with selection', dsel
                dtree.Draw(var+'>>'+dhistname, dsel)#apply bin selection
                dhist.SetDirectory(0)
                dhist.SetLineWidth(2)
                dhist.SetLineColor(r.kBlack)
                legend.AddEntry(dhist, 'DATA', "F")           

            phist.SetDirectory(0)
            bhist.SetDirectory(0)

            phist.SetLineWidth(2)
            phist.SetLineColor(r.kRed-3)
            legend.AddEntry(phist, 'TTTT', "F")           
            bhist.SetLineWidth(2)
            bhist.SetLineColor(r.kBlue+2)
            legend.AddEntry(bhist, 'MC BGD', "F")           

            #get correct histogram from file:
            mycolor = r.kViolet-9
            NAFname = chanid+'_'+self.year+'_BKG'
            NAFfile = r.TFile(histfile)
            # NAFhist = r.TH1F(NAFname, NAFname, nbins, selrange[var][0], selrange[var][1])
            NAFhist = NAFfile.Get(NAFname)
            NAFhist.SetLineColor(r.TColor.GetColorDark(mycolor))
            NAFhist.SetFillColor(mycolor)
            NAFhist.SetLineWidth(2)
            legend.AddEntry(NAFhist, 'NAF BGD', "F")           

            NAFhist.GetXaxis().SetTitle(var)
            NAFhist.GetYaxis().SetTitle('NEntries')

            phist.Scale(100)
            NAFhist.Draw("histe1")
            phist.Draw("histe1same")
            bhist.Draw("histe1same")
            if VR:
                dhist.Draw("histe1same")
            r.gPad.SetGrid()
            if logscale:
                r.gPad.SetLogy()
            legend.Draw("same")
            outname = outdir+"/"+outname+'.png'
            r.gPad.Modified()
            c1.Update()
            c1.SaveAs(outname)
            print 'created histogram', outname


    def short_procs(self, cut, bin, varlist, outdir='./', logscale=True):
        short_processes = [('BKGD'     ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+self.year+'/MC/0lep/maintrees_newWP/BKGD_'+self.year+'_merged_0lep_tree.root'),
                           ('TT'       ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+self.year+'/MC/0lep/maintrees_newWP/TT_'+self.year+'_merged_0lep_tree.root'),
                           ('QCD'      ,'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+self.year+'/MC/0lep/maintrees_newWP/QCD_'+self.year+'_merged_0lep_tree.root'),
                           ('TTTT'     , '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+self.year+'/MC/0lep/maintrees_newWP/TTTT_'+self.year+'_merged_0lep_tree.root')]
        self.processes = OrderedDict(short_processes)

        colors = [r.kCyan-10, r.kAzure-9, r.kBlue-9, r.kViolet-9, r.kMagenta-10, r.kPink+1, r.kRed-9, r.kOrange-9, r.kYellow-9] #pastel pinky
        nbins = {'ht':25,
                 'nrestops':5,
                 'nbstops':5,
                 'njets':20,
                 'nbjets':20,
                 'catBDTdisc':10}
        selrange = {'ht':[500,2500],
                    'nrestops':[0,5],
                    'nbstops':[0,5],
                    'njets':[0,20],
                    'nbjets':[0,20],
                    'catBDTdisc':[0,1]}
        
            #set up output directory
        outdir = outdir+cut+bin+'dists'
        if not os.path.exists(outdir):
            os.makedirs(outdir); print 'Making output directory ', outdir
            
        splittops=False
        if cut=='BASE':
            splittops =True

        cutlist = datacard_dict(self.base, cut, bin, self.lumi, 'weight', topsplit=splittops)        

            #make histogram for each variable in each bin
        for chanid, chansel in cutlist.iteritems():
            print 'channel:', chanid, 'selection:', chansel
            if not splittops: 
                chansel = [chansel] #to avoid silly errors
            for var in varlist:
                print 'plotting var', var
                # if splittops:
                #     c1 = r.TCanvas("c1","multipads",500,900)
                #     c1.Divide(1,3, 0, 0)
              
                for c, sel in enumerate(chansel): #(usually just one)
                    outname = var+'_'+chanid+'_'+cut+bin+'hist'
                    if splittops:
                        outname = var+'_'+chanid+'_'+cut+bin+'hist'+str(c)

                    c1 = r.TCanvas("c1","multipads",700,700)
                    c1.cd()

                    hists = [] # CHANGED needs to be within the selection loop
                    norm=0.0
                    # if splittops:
                    #     pad = c1.cd(c+1)
                    #     print 'canvas', c+1
                        # subhists=[]
                    # stack = r.THStack("stack", "stack")
                    legend = r.TLegend(0.1, 0.6, 0.3, 0.9)
                    for proc, procfile in self.processes.iteritems():
                        histname = proc+'_'+outname
                        print 'adding hist', histname, 'to stack with selection', sel
                        pfile = r.TFile(procfile)
                        ptree = pfile.Get(self.treename)
                        nb=20
                        if not splittops:
                            nb = nbins[var]
                        phist = r.TH1F(histname, histname, nb, selrange[var][0], selrange[var][1])
                        ptree.Draw(var+'>>'+histname, sel)#apply bin selection
                        phist.SetDirectory(0)
                        hists.append(phist)

                        # if not splittops:
                        #     hists.append(phist)
                        # elif splittops:
                        #     subhists.append(phist)
                       # if splittops:
                       #     hists.append(subhists)
                    print 'hists', hists

                    # if splittops:
                    #     c1.cd(c+1)

                    procnames = self.processes.keys()
                    for i,(proc, phist) in enumerate(zip(procnames, hists)):

                        print i, proc, phist
                        # phist.SetFillStyle(3325)
                        phist.SetLineWidth(2)
                        # phist.SetLineColor(colors[i])
                        legend.AddEntry(phist, proc, "F")           

                        if proc == 'TT' or proc=='QCD':
                            phist.SetLineColor(colors[i])
                            if not splittops:
                                phist.SetLineColor(r.TColor.GetColorDark(colors[i]))
                                phist.SetFillColor(colors[i])
                            # stack.Add(phist)
                            phist.Draw("histe1same")

                        elif proc=='BKGD':
                            phist.SetLineColor(r.kGray+2)
                            # phist.SetLineWidth(2)
                            phist.GetXaxis().SetTitle(var)
                            phist.GetYaxis().SetTitle('NEntries')
                            # phist.SetMaximum(2000)
                            if not splittops:

                                phist.SetTitle(var+" "+chanid+" "+cut+bin+" histograms")                       
                            elif splittops:
                                phist.SetFillColor(r.kGray)
                            #     c1.cd(c+1)
                            phist.Draw("histe1")
                            norm = phist.Integral()
                            print 'bkgd norm',norm

                        elif proc=='TTTT':
                            phist.SetLineColor(r.kRed-3)
                            # if norm>0.0:
                            #     phist.Scale(1.0*norm)
                            phist.Scale(100)
                            phist.Draw("histe1same")
                            
                        # c1.Update()

                    # if splittops:
                            # c1.cd(c+1)
                            # pad.Modified()
                            # pad.Update()
                        # r.gPad.Modified()
                        # r.gPad.Update()
                        # c1.cd(0)
                        # # c1.Modified()
                        # c1.Update()
                        # c1.cd(c+1)

                            # phist.SetDirectory(0)
                            # phist.Reset()

                        # else: 
                        #     phist.SetLineColor(r.TColor.GetColorDark(colors[i]))
                        #     phist.SetFillColor(colors[i])
                        # r.gPad.Modified()
                    r.gPad.SetGrid()
                
                    # stack.Draw("histe1same")
                    
                    # if splittops:
                    #     c1.cd(c+1)
                    #     pad.Update()

                    if logscale:
                        r.gPad.SetLogy()

                    # if not splittops:
                    legend.Draw("same")
                    # else: 
                        # pad.BuildLegend(0.1, 0.6, 0.3, 0.9)
                    # outname = outdir+"/"+var+"_"+chanid+"_"+cut+bin+"_histograms"+str(c)".png"
                    outname = outdir+"/"+outname+'.png'
                    # c1.cd(0)
                    r.gPad.Modified()
                    c1.Update()
                    c1.SaveAs(outname)
                    print 'created histogram', outname


    def getcutflow(self, cut, bin):
            #makes cutflow tables
            #get selections for this top and kinbin category from the datacard dictionary
        cutlist = datacard_dict(self.base, cut, bin, self.lumi, 'weight')

            #some latex defs
        hline  = '\\hline\n'
        endrow = ' \\\\\n'            

            #header
        print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
        print 'CUTFLOW TABLE FOR '+cut+' '+bin
        print 'baseline: '+self.base+' lumi: '+str(self.lumi)
        print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

            # get number of columns and appropriate latex line
            # nrows = len(cutlist.keys())+1+header
        ncols = len(self.processes.keys())+4
        begintable = '\n\\begin{tabular}{|c|'+('c c |'*ncols)+'}\n' + hline
        
            #print the table line by line
        print begintable
            #table header
        header = 'top selection && binid & '
        for proc in self.processes.keys():
            header+= ' & '+proc+' & '
        header += ' & total bkgd && sig / sqrt(bkgd) & '
        header += endrow + hline
        print header
        for chanid, chansel in cutlist.iteritems():
                # get a nicer selection label for tops. will have to add this for binning too
            rt_loc = chansel.partition('nrestops')[2] 
            bt_loc = chansel.partition('nrestops')[2] 
            nrt = rt_loc[:3]; nbt = bt_loc[:3]
            nrt = self.replacesigns(nrt)
            nbt = self.replacesigns(nbt)
            topsel = 'N(resolved tops)'+nrt+', N(boosted tops)'+nbt
                
                #get the rates and uncertainties
            rates=[]; uncs=[]
            for proc, procfile in self.processes.iteritems():
                r,u = self.getRate(proc, cutstr)
                rates.append(r); uncs.append(u)
               # print proc, r, u

                #other calculations:
            totbg = sum(rates[1:])
            npuncs = np.array(uncs[1:])
            totbgunc = math.sqrt(np.dot(npuncs))
            sovsqrtb = rates[1]/math.sqrt(totbg)
            
            line = topsel+ ' & '
            line += ' & '+chanid+' & '
        for rate, unc in zip(rates, uncs):
            line+= ' & '+str(rate)+'$\pm$'+str(unc)+' & '
            line+= ' & '+str(totbg)+'$\pm$'+str(totbgunc)+' & ' #total background
            line+= ' & '+str(sovsqrtb)+' & ' #s/sqrt(b)
            line+= endrow + hline
            print line

            #finish the table
        print '\\end{tabular}\n'

    def getRate(self, processname, cutstr):
            #calculates rates and statistical uncertainties
            # cutstr = '('+str(args.lumi)+'*'+weight+')*('+cutstr+')'
        pfile = r.TFile(self.processes[processname])
        tree = pfile.Get(self.treename)
        htmp = r.TH1F('htmp','htmp',1,0,10000)
        tree.Project('htmp',self.projvar,cutstr)
        errorVal = r.Double(0)
        rate = htmp.IntegralAndError(0,3,errorVal)
            # the error is computed using error propagation from the bin errors assuming bins uncorrelated
        if rate < 0:#treatment of negative yields 
            print 'WARNING: NEGATIVE YIELD:', rate, processname, cutstr #will throw error
            rate = 0.0
        return round(rate,3), round(errorVal,3)

    def replacesigns(self, mystr):
        if '>=' in mystr:
            mystr.replace('>=', '$\geq$')
        elif '<=' in mystr:
            mystr.replace('<=', '$\leq$')
        elif '==' in mystr:
            mystr.replace('==', '$=$')
        elif '>' in mystr:
            mystr.replace('>', '$>$')
        elif '<' in mystr:
            mystr.replace('<', '$<$')
        return mystr
