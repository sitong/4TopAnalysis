# Helper to get original ResMVA weights
# takes a model file and variable list
# Melissa Quinnan 3/15/19
import ROOT
import math
import numpy as np
import xgboost as xgb
import logging
from PhysicsTools.NanoTrees.helpers.tmvahelper import TMVAHelper
from collections import OrderedDict
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest
from PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoTrees.helpers.ConfigHelper import ResSetter

    # Useful TopCand definitions and utilities
class ResTopHelper:
    def __init__(self, btype, tagtype='nano', year='2016', useTMVA=True):
        #helpers
        self.verbose       = False
        self.RS            = ResSetter(tagtype, year)
        self.tagtype       = tagtype
        self.tmva = TMVAHelper(self.RS.bdt_file, self.RS.bdt_vars)
        if useTMVA:
            self.tmva.addVariables()
        self.jh            = JetHelper(btype, 1.0)         
        self.btype         = btype


    #ROOT 4vector
    def p4(self, obj):
        fourvec=ROOT.TLorentzVector()
        fourvec.SetPtEtaPhiM(obj.pt, obj.eta, obj.phi, obj.mass)
        return fourvec


    # takes 3 jets and creates a top cand
    # format: [4vec, bjet, jet2, jet3, disc]
    def getTopCand(self, inB, inJ2, inJ3):
        bjet = inB
        if (inJ2.pt > inJ3.pt): 
            jet2 = inJ2
            jet3 = inJ3
        else:
            jet2 = inJ3
            jet3 = inJ2
        tcandp4 = self.p4(bjet) + self.p4(jet2) + self.p4(jet3)
        topcand = [tcandp4, bjet, jet2, jet3, -1.0] #-1 placeholder for disc value
        return np.array(topcand) # maybe these should be classes rather than np arrays later

    
    #get W candidate
    def getWCand(self, inJ2, inJ3): 
        if (inJ2.pt > inJ3.pt): 
            jet2 = inJ2
            jet3 = inJ3
        else:
            jet2 = inJ3
            jet3 = inJ2
        wcandp4 = self.p4(jet2) + self.p4(jet3)
        wcand = [wcandp4, jet2, jet3]
        return np.array(wcand)


    def WfromTop(topcand):
        wcand = np.array([ self.p4(topcand[2]) + self.p4(topcand[3]), topcand[2], topcand[3] ])
        return wcand
    
    #check if candiate is close enough to mass of top or w
    def PassMass(self, topcandp4, wcandp4, trange=80.0, wrange=40.0, tparam=175.0, wparam=80.0):
        pmW = False; pmT= False
        if ( abs(wcandp4.M()-wparam) <= wrange ):
            pmW = True
        if ( abs(topcandp4.M()-tparam) <= trange ):
            pmT = True
        PassMass = (pmW and pmT)
        return PassMass #, self.PassMassTop, self.PassMassW


    # discriminant threshold for resolved mva
    def SelMVAResTops(self, top4vec, disc=0.0):
        if self.RS.useTopWP:
            if (disc <= self.RS.topWP):
                return False #fail
        if(top4vec.Eta()>self.RS.etaMax):
            return False
        if(top4vec.Pt()<self.RS.TopCand_minpt):
            return False
        return True #pass
    
    #just so other module knows the working point
    def restopWP(self):
        return self.RS.topWP

   # takes collection of ak4jets and return a collection of resolved top candidates
    def getrescands(self, cleanjets):
        havecands = False; topcands = []; bestcsv = []
        if len(cleanjets)<self.RS.minjets: #require 3 jets for topcand
            return havecands, None
        else:
            for jet in cleanjets:
                if   self.btype=='csvv2':    csv = jet.btagCSVV2
                elif self.btype=='deepcsv':  csv = jet.btagDeepB
                elif self.btype=='deepflav': csv = jet.btagDeepFlavB
                bestcsv.append(csv)
        bestcsv = np.array(bestcsv)
        csvinds = np.argsort(bestcsv)[::-1]
        orderjets = [cleanjets[i] for i in csvinds] #order jets in descending csv score order

        nbcanjets=self.RS.Bimax
        if len(orderjets)<self.RS.Bimax: #make sure not considering more bcands than there are jets
            nbcandjets=len(orderjets)

        if self.verbose: print 'numjets', len(orderjets), 'n bjet cands', len(orderjets[:nbcanjets])
        #get the cands. NO CSV CUT
        for Bi, bcand in enumerate(orderjets[:nbcanjets]): #iterate over first 3 (or whatever) elements in list
            for j2i, j2 in enumerate(orderjets[1:]):
                if j2i!=Bi:
                    for j3i, j3 in enumerate(orderjets[2:]):
                        if (j3i!=Bi and j3i!=j2i): #no overlapping jets
                            tcand = self.getTopCand(bcand, j2, j3)
                            wcand = self.getWCand(j2, j3)
                            passmass = self.PassMass(tcand[0], wcand[0], self.RS.topmassrange, self.RS.wmassrange, self.RS.topmassparam, self.RS.wmassparam)
                            if passmass:
                                havecands = True  #true if there are ANY tops in the event
                                topcands.append(tcand)
        if havecands:
            return havecands, np.array(topcands)
        elif not havecands:
            return havecands, None #empty


    # fills variables needed to calculate bdt discriminant for restop mva
    def _fillBDTvars(self, havecands, topcand): #should only calculate bdt if >1 topcand
        if havecands: wcand = self.getWCand(topcand[2], topcand[3])
        bcsv = 0.; j2csv = 0.; j3csv = 0.
        if   self.btype=='csvv2' and havecands: 
            bcsv = topcand[1].btagCSVV2;     j2csv = topcand[2].btagCSVV2;     j3csv = topcand[3].btagCSVV2
        elif self.btype=='deepcsv' and havecands: 
            bcsv = topcand[1].btagDeepB;     j2csv = topcand[2].btagDeepB;     j3csv = topcand[3].btagDeepB
        elif self.btype=='deepflav' and havecands:
            bcsv = topcand[1].btagDeepFlavB; j2csv = topcand[2].btagDeepFlavB; j3csv = topcand[3].btagDeepFlavB

        #calc BDT vars
        wcand_deltaR=0; b_wcand_deltatR = 0; sd_0 = 0; var_sd_n2 =0
        if havecands:
            wcand_deltaR = phys.deltaR(wcand[1].eta, wcand[1].phi, wcand[2].eta, wcand[2].phi) #deltaR(j2, j3)
            b_wcand_deltaR = phys.deltaR(topcand[1].eta, topcand[1].phi, wcand[0].Eta(), wcand[0].Phi()) #deltaR(b, wcand)
            sd_0 = topcand[3].pt/(topcand[2].pt+topcand[3].pt) #j3pt/j2pt+j3pt
            if wcand_deltaR != 0:
                var_sd_n2 = sd_0/((wcand_deltaR)**-2)

        if self.tagtype=='nano':
        # create dictionary
            empty_tcdict = OrderedDict( [("var_b_mass",  0),
                                         ("var_topcand_mass",0),
                                         ("var_topcand_ptDR",0),
                                         ("var_wcand_mass",  0), 
                                         ("var_wcand_ptDR",  0),
                                         ("var_b_j2_mass",   0),
                                         ("var_b_j3_mass",   0),
                                         ("var_sd_n2",       0),
                                         ("var_b_btagDeepFlavB",  -1), 
                                         ("var_j2_btagDeepFlavB", -1), 
                                         ("var_j3_btagDeepFlavB",  -1),
                                         ("var_j2_qgl",-1), 
                                         ("var_j2_btagDeepFlavC",-1),
                                         ("var_j3_qgl", -1),
                                         ("var_j3_btagDeepFlavC", -1),
                                         ("var_j2_nConstituents",     0),
                                         ("var_j3_nConstituents",     0) ])

            if not havecands: full_tcdict = empty_tcdict
            elif havecands:
                #try to avoid NaNs
                bbtag=-1.0; j2btag=-1.0; j3btag=-1.0
                j2ctag=-1.0; j3ctag=-1.0
                j2qgl=-1.0; j3qgl=-1.0
                if not math.isnan( topcand[1].btagDeepFlavB):
                    bbtag = topcand[1].btagDeepFlavB
                if not math.isnan( topcand[2].btagDeepFlavB):
                    j2btag = topcand[2].btagDeepFlavB
                if not math.isnan( topcand[3].btagDeepFlavB):
                    j3btag = topcand[3].btagDeepFlavB
                if not math.isnan( topcand[2].btagDeepFlavC):
                    j2ctag = topcand[2].btagDeepFlavC
                if not math.isnan( topcand[3].btagDeepFlavC):
                    j3ctag = topcand[3].btagDeepFlavC
                if not math.isnan( topcand[2].qgl):
                    j2qgl = topcand[2].qgl
                if not math.isnan( topcand[3].qgl):
                    j3qgl = topcand[3].qgl

                full_tcdict = OrderedDict([
                        ("var_b_mass",  topcand[1].mass),
                        ("var_topcand_mass", topcand[0].M()),
                        ("var_topcand_ptDR", topcand[0].Pt()*b_wcand_deltaR ),#fixed, *b_wcand_deltaR
                        ("var_wcand_mass",   wcand[0].M()),
                        ("var_wcand_ptDR",   wcand[0].Pt()*wcand_deltaR),#fixed, *wcandpt
                        ("var_b_j2_mass",    (self.p4(topcand[1]) + self.p4(topcand[2]) ).M() ),
                        ("var_b_j3_mass",    (self.p4(topcand[1]) + self.p4(topcand[3]) ).M() ),
                        ("var_sd_n2",        var_sd_n2),
                        ("var_b_btagDeepFlavB",  bbtag),
                        ("var_j2_btagDeepFlavB", j2btag),
                        ("var_j3_btagDeepFlavB", j3btag),
                        ("var_j2_qgl"          , j2qgl),
                        ("var_j2_btagDeepFlavC", j2ctag),
                        ("var_j3_qgl"          , j3qgl),
                        ("var_j3_btagDeepFlavC", j3ctag),
                        ("var_j2_nConstituents",   int(topcand[2].nConstituents)),#corrected
                        ("var_j3_nConstituents",   int(topcand[3].nConstituents)) ])

        elif self.tagtype=='redo':
        # create dictionary
            empty_tcdict = OrderedDict( [("var_b_mass",  0),
                                         ("var_topcand_mass",0),
                                         ("var_topcand_ptDR",0),
                                         ("var_wcand_mass",  0), 
                                         ("var_wcand_ptDR",  0),
                                         ("var_b_j2_mass",   0),
                                         ("var_b_j3_mass",   0),
                                         ("var_sd_n2",       0),
                                         ("var_b_csv",       -1),
                                         ("var_j2_csv",       -1),
                                         ("var_j3_csv",       -1),
                                         ("var_j2_cvsl",-1),
                                         ("var_j2_ptD",0),
                                         ("var_j2_axis1",-1),
                                         ("var_j3_cvsl", -1),
                                         ("var_j3_ptD",0),
                                         ("var_j3_axis1",-1),
                                         ("var_j2_mult",     0),
                                         ("var_j3_mult",     0) ])

            if not havecands: full_tcdict = empty_tcdict
            elif havecands:
                full_tcdict = OrderedDict([
                        ("var_b_mass",  topcand[1].mass),
                        ("var_topcand_mass", topcand[0].M()),
                        ("var_topcand_ptDR", topcand[0].Pt()*b_wcand_deltaR ),#fixed, *b_wcand_deltaR                                                                  
                        ("var_wcand_mass",   wcand[0].M()),
                        ("var_wcand_ptDR",   wcand[0].Pt()*wcand_deltaR),#fixed, *wcandpt                                                                                                  
                        ("var_b_j2_mass",    (self.p4(topcand[1]) + self.p4(topcand[2]) ).M() ),
                        ("var_b_j3_mass",    (self.p4(topcand[1]) + self.p4(topcand[3]) ).M() ),
                        ("var_sd_n2",        var_sd_n2),
                        ("var_b_csv",   topcand[1].btagCSVV2),
                        ("var_j2_csv",  topcand[2].btagCSVV2),
                        ("var_j2_cvsl", topcand[2].CvsL),
                        ("var_j2_ptD",  topcand[2].qgptD),
                        ("var_j2_axis1",topcand[2].qgAxis1),
                        ("var_j3_csv",  topcand[3].btagCSVV2),
                        ("var_j3_cvsl", topcand[3].CvsL),
                        ("var_j3_ptD",  topcand[3].qgptD),
                        ("var_j3_axis1",topcand[3].qgAxis1),
                        ("var_j2_mult",      topcand[2].qgMult),#corrected                                                                                                          
                        ("var_j3_mult",      topcand[3].qgMult) ])

        elif self.tagtype=='mod':
        # create dictionary
            empty_tcdict = OrderedDict( [("var_b_mass",  0),
                                         ("var_topcand_mass",0),
                                         ("var_topcand_ptDR",0),
                                         ("var_wcand_mass",  0),
                                         ("var_wcand_ptDR",  0),
                                         ("var_b_j2_mass",   0),
                                         ("var_b_j3_mass",   0),
                                         ("var_sd_n2",       0),
                                         ("var_b_deepFlavourb", 0),
                                         ("var_j2_deepFlavourb",0),
                                         ("var_j3_deepFlavourb",0),
                                         ("var_b_deepFlavourbb",0),
                                         ("var_j2_deepFlavourbb",0),
                                         ("var_j3_deepFlavourbb",0),
                                         ("var_b_deepFlavourc",0),
                                         ("var_j2_deepFlavourc",0),
                                         ("var_j3_deepFlavourc",0),
                                         ("var_b_deepFlavourg",0),
                                         ("var_j2_deepFlavourg",0),
                                         ("var_j3_deepFlavourg",0),
                                         ("var_b_deepFlavourlepb",0),
                                         ("var_j2_deepFlavourlepb",0),
                                         ("var_j3_deepFlavourlepb",0),
                                         ("var_b_deepFlavouruds",0),
                                         ("var_j2_deepFlavouruds",0),
                                         ("var_j3_deepFlavouruds",0),
                                         ("var_j2_mult",     0),
                                         ("var_j3_mult",     0) ])

            if not havecands: full_tcdict = empty_tcdict
            elif havecands:
                full_tcdict = OrderedDict([("var_b_mass",  topcand[1].mass),
                                           ("var_topcand_mass", topcand[0].M()),
                                           ("var_topcand_ptDR", topcand[0].Pt()*b_wcand_deltaR ),
                                           ("var_wcand_mass",   wcand[0].M()),
                                           ("var_wcand_ptDR",   wcand[0].Pt()*wcand_deltaR),
                                           ("var_b_j2_mass",    (self.p4(topcand[1]) + self.p4(topcand[2]) ).M() ),
                                           ("var_b_j3_mass",    (self.p4(topcand[1]) + self.p4(topcand[3]) ).M() ),
                                           ("var_sd_n2",        var_sd_n2),
                                           ("var_b_deepFlavourb", topcand[1].deepFlavourb),
                                           ("var_j2_deepFlavourb",topcand[2].deepFlavourb),
                                           ("var_j3_deepFlavourb",topcand[3].deepFlavourb),
                                           ("var_b_deepFlavourbb",topcand[1].deepFlavourbb),
                                           ("var_j2_deepFlavourbb",topcand[2].deepFlavourbb),
                                           ("var_j3_deepFlavourbb",topcand[3].deepFlavourbb),
                                           ("var_b_deepFlavourc",topcand[1].deepFlavourc),
                                           ("var_j2_deepFlavourc",topcand[2].deepFlavourc),
                                           ("var_j3_deepFlavourc",topcand[3].deepFlavourc),
                                           ("var_b_deepFlavourg",topcand[1].deepFlavourg),
                                           ("var_j2_deepFlavourg",topcand[2].deepFlavourg),
                                           ("var_j3_deepFlavourg",topcand[3].deepFlavourg),
                                           ("var_b_deepFlavourlepb",topcand[1].deepFlavourlepb),
                                           ("var_j2_deepFlavourlepb",topcand[2].deepFlavourlepb),
                                           ("var_j3_deepFlavourlepb",topcand[3].deepFlavourlepb),
                                           ("var_b_deepFlavouruds",topcand[1].deepFlavouruds),
                                           ("var_j2_deepFlavouruds",topcand[2].deepFlavouruds),
                                           ("var_j3_deepFlavouruds",topcand[3].deepFlavouruds),
                                           ("var_j2_mult",      topcand[2].qgMult),
                                           ("var_j3_mult",      topcand[3].qgMult) ])

        #fill and evalutate mva:
        if havecands:
            self.tmva.setValue(full_tcdict)
            disc =self.tmva.eval()
        elif not havecands: disc =-1

        #Find good top candidates        
        goodcand = False
        if havecands:
            iscand = self.SelMVAResTops(topcand[0], disc)
            isoverlap = self.checkResJetOverlap(topcand)
            if (iscand and isoverlap): #if passes selection and no overlap
                goodcand = True
        #be sure will fill vars if goodcand, else fill zero:
        if not goodcand:
            full_tcdict = empty_tcdict 
        return goodcand, topcand, disc, full_tcdict


    # check that jets within resolved top cands don't overlap with each other
    def checkResJetOverlap(self, topcand):
        bj=topcand[1]; j2=topcand[2]; j3=topcand[3]
        if (bj==j2 or bj==j3 or j2==bj or j2==j3 or j3==bj or j3==j2):
            return False #IS overlapping
        else:
            return True #not overlapping


    # cleans boosted from resolved (getcleanbstops=True) or resolved from boosted (getcleanbstops=False) 
    def CleanResvsBoost(self, bstcands, bswcands, rescands):                
        if self.RS.cleanWs:
        #first clean Ws from rescands
            if (np.size(rescands)>0 and np.size(bswcands)>0):
                rescands = self.jh.CleanCollection(rescands, bswcands, self.RS.cleanAK4overlap, 0) #always change rescands
        #clean resolved tops from boosted tops. boosted tops change
        if self.RS.resthenboost: 
            if(np.size(bstcands)>0 and np.size(rescands)>0):
                bstcands = self.jh.CleanCollection(bstcands, rescands, self.RS.cleanAK4overlap, self.RS.resthenboost)
        #clean boosted tops from resolved tops. resolved tops change
        elif not self.RS.resthenboost: 
            if (np.size(bstcands)>0 and np.size(rescands)>0):
                rescands = self.jh.CleanCollection(rescands, bstcands, self.RS.cleanAK4overlap, self.RS.resthenboost)

        return np.array(bstcands), np.array(bswcands), np.array(rescands)


    #make sure resolved candidates are unique
    def checkResCandOverlap(self, Selcands, discs):
        cleancands = []; cleanjets = []
        #order selcands from best (highest) to worst disc values:
        discis  = np.argsort(discs)[::-1]
        selcands = [Selcands[i] for i in discis]
        orderdiscs = np.sort(discs)[::-1]

        for i1, c1 in enumerate(selcands):
            #use topcands [tvec, j1, j2, j3] if mva top
            c1j1=c1[1]; c1j2=c1[2]; c1j3=c1[3]
            C1 = c1
            if i1==0:
                c1[4]=orderdiscs[i1] #add disc to array if considered a candidate
                cleancands.append(c1) #keep first/best candidate
                cleanjets.extend((c1j1,c1j2,c1j3))

            for i2, c2  in enumerate(selcands):
                c2j1=c2[1]; c2j2=c2[2]; c2j3=c2[3]

                if i2>i1:
                    #check for full overlap:
                    if (c2j1 in C1 and c2j2 in C1 and c2j3 in C1):
                        if self.verbose: print 'same jets!'
                    else:
                        if ((c2j1 not in C1) and (c2j2 not in C1) and (c2j3 not in C1)):
                            if ((c2j1 not in cleanjets) and (c2j2 not in cleanjets) and (c2j3 not in cleanjets)):
                                c2[4]=orderdiscs[i2]
                                cleancands.append(c2) #c2 not overlapping!
                                cleanjets.extend((c2j1,c2j2,c2j3))
                            else: 
                                if self.verbose: print 'overlaps with cleanjets!'
                        else:
                            if self.verbose: print 'overlapping jets!'
        return np.array(cleancands)

