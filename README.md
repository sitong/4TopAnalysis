# All-Hadronic Four-Top Analysis Tree-Making (post-processing) NanoAOD Framework

CADI Line (TOP-21-005): https://cms.cern.ch/iCMS/analysisadmin/cadilines?id=2430&ancode=TOP-21-005&tp=an&line=TOP-21-005


## Installation

**Set up CMSSW**

```
bash
cmsrel CMSSW_10_2_20_UL
cd CMSSW_10_2_20_UL/src
cmsenv
```

**Set up official NanoAOD-tools**

```
bash
git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools
```

Enable `LZ4` compression algo by adding the following line after L47 of `$CMSSW_BASE/src/PhysicsTools/NanoAODTools/python/postprocessing/framework/postprocessor.py`:

```
python
elif algo == "LZ4":  compressionAlgo  = ROOT.ROOT.kLZ4
```

**Get customized tools for making analysis trees**

```
bash
git clone https://gitlab.cern.ch/mequinna/4TopAnalysis.git PhysicsTools/NanoTrees
```

**Get some updated NanoAODTools files/modules for btagSFs**

```
cd ./PhysicsTools/NanoTrees
scp ./data/4NanoAODTools/modules/btagSFProducer.py ../NanoAODTools/python/postprocessing/modules/btv/
scp ./data/4NanoAODTools/btagSF/*.csv ../NanoAODTools/data/btagSF/
cd -
```

**Compile**

```
bash
scram b -j8
```

You can then enter the work directory:

```
cd ./PhysicsTools/NanoTrees
```

## Code Structure

**The python subdirectory**

The main scripts for postprocessing ntuples as well as many other useful scripts are located here:

```
/PhysicsTools/NanoTrees/python/
```
This python directory has 3 subdirectories: helpers, macros, and producers. Producers are the modules that actually add variables to the root trees. Helpers are functions and classes for configuring these modules. And macros includes various useful scripts for running on postprocessed trees.

"macros" includes 5 subdirectories: 

```
ResTopRetrain  combine  eventMVA  plotting  random
```

ResTopRetrain was used for training and testing the resolved top tagger. 

"combine" contains resources for creating datacards to use with Higgs combine (http://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part2/settinguptheanalysis/). The most important scripts here are newautodatacards.py, newhistmaker.py, and datacardrefs.py. The code for example to run datacards for all three years (I usually use screens) and put them in the directory /datacards_dir/ is for example:

```
python newautodatacards.py -o ./datacards_dir/ -y 2016
python newautodatacards.py -o ./datacards_dir/ -y 2017
python newautodatacards.py -o ./datacards_dir/ -y 2018
```

Note settings for the datacards need to be specified (mainly in datacardrefs.py).

"eventMVA" has scripts for training and testing the event-level BDT and also some important skimming scripts for postprocessed trees. These are described in the "Submitting jobs for postprocessed trees" section.

"plotting" contains scripts for making plots and histograms. The most useful of these are Defs.py for settings, predplots.py for prefit distributions, abcdyields.py for yields calculations, calcbSFcorrections.py for calculating bSFcorrection factors, and postfit.py for postfit distribtions. 

For example, if you want to run VR prefit distributions in 2018, the command is:

```
python predplots.py -y 2018 -b VR
```

"random" contains various other scripts, for example a python script for splitting ntuples into smaller pieces so they aren't too large condor can finish jobs in time (rootsplitter.py). It also has scripts for checking if all trees have finished successfully (arejobsdone.py, findfailjobs.py).


Note: The TreeTesting producer is standalone for ROC curves, and fills extra resolved candidate variables

Directory for making/submitting trees: 
```
/PhysicsTools/NanoTrees/run/
```
The grid submission script is in this directory (runPostProcessing.py) and the local submission script is here:
```
/PhysicsTools/NanoAODTools/scripts/nano_postproc.py
```
**The data subdirectory**

The main files needed for postprocessing ntuples etc are located here:

```
/PhysicsTools/NanoTrees/data/
```

Some notes on the subsubdirectories located here:

- 4NanoAODTools - for copying to NanoAODTools things that are not currently located on the main repo for some reason
- GoldenJSON - golden json lumi files for each year
- LeptonSFFiles - lepton veto scale factor files
- NNretrain - ABCDnn trainings for data-driven backgrounds
- ResMVAweights - resolved top tagger trainings
- TriggerSFFiles  - trigger scale factor histograms
- bSFcorrFiles  - btag SF correction factor histograms
- eventBDTweights - trainings for event-level BDT 

**The run subdirectory**

This is the subdirectory for submitting postprocessing jobs. It also contains code for xsec weight calculations.

Some notes on what is located here:

- ./datasets/ contains txt files (for example nanoaodv5_MC2016.txt nanoaodv5_MC2017.txt nanoaodv5_MC2018.txt) with the lists of MC samples you want to run when submitting jobs
- some job submission scripts, like runPostProcessing.py and run_postproc_condor.sh 
- scripts for calculating xsec weights, located within ./samples/ (for example calcweight.py)

```
/PhysicsTools/NanoTrees/run/
```

## Generating Trees

**General Info**

The main NanoAOD twiki can be found here: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD
The idea is that you run postprocessing on central ntuples in order to store useful variables in flat root trees. 
You can list centrally produced datasets for example using (need grid certificate):

```
dasgoclient -query="dataset=/TTWJetsTo*/RunIISummer16NanoAODv6*/NANOAODSIM"
```

And can copy them into your own eos space for use by doing for example:

```
xrdcp root://cmsxrootd.fnal.gov//store/PATH/file.root root://cmseos.fnal.gov//store/user/lpcstop/mequinna/NanoAODv6/DIR
```

Note that for copying central samples xrdcp needs a different command per individual file - this doesn't work with directories. A simple python script that takes a list of filenames and runs a command on them is the best way to do this, for example:

```
import subprocess
import numpy as np
import ROOT as r
import pandas as pd
import sys                                                                                     
import os
import re

destdir = './'
#for eos prefix is root://cmseos.fnal.gov//DIR-TO

with open('filelist.txt', "r") as filelist:
    for f in filelist:
        if '#' not in f:
            f = re.sub(r"[\n\t\s]*", "", f)                                                                               
            print 'loading', f, '...'
            fname=f.replace("/store/", "root://cmsxrootd.fnal.gov//store/")

	    cmd = 'xrdcp '+fname+' '+destdir+'/'

	    print cmd
            print 'running command...'
	    #subprocess.call(cmd, shell=True)
```
More information on eos/xrdcp and getting a grid certificate can be found on the lpc website: https://uscms.org/uscms_at_work/computing/LPC/usingEOSAtLPC.shtml

**Local Testing**

- Making trees locally:

General formula for running single-modules (recommended for testing):

```
nano_postproc.py -I PhysicsTools.NanoTrees.producers.ProducerName NameTrees /OUTPUT_DIR/ /INPUT_DIR/*.root
```

**Submitting jobs for postprocessed trees**

1. You need to be in the run subdirectory:

```
cd ./PhysicsTools/NanoTrees/run
```

2. It is recommended before job submission to remove previous job directories and the root\:/ directory created in previous job submissions. Also make sure that the \eos\ directory you are targeting exists and does not contain the same files already, otherwise you will get an error that the files already exist.

```
#for example, in /PhysicsTools/NanoTrees/run/
rm -rf *jobs*
rm -rf root\:/
```

3. Make sure the MC samples you want to submit are indicated in the appropriate ./datasets/ textfile. Use # to comment out samples you do not want to include

```
#for example, if the following was in a datasets/example.txt file, only the jobs for TTTT_SPLIT would be submitted:
# DYJetsToLL
TTTT_SPLIT
```

4. Set up your grid certificate with voms:

```
voms-proxy-init --voms cms --valid 192:00
```

5. Submit the jobs using runPostProcessing.py. 

```
# 2016 DATA
python runPostProcessing.py /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2016/DATA/JETHT /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/0lep_maintrees/DATA  --bi keep_and_drop_input.txt --bo keep_and_drop_output.txt -y 2016 -I PhysicsTools.NanoTrees.producers.SystProducer SystProducer2016 -I PhysicsTools.NanoTrees.producers.BaseProducerLight BaseTrees2016_0lep -I PhysicsTools.NanoTrees.producers.FunkyLeptons FunkyLeps2016_0lep  --json Cert_271036-284044_13TeV_23Sep2016ReReco_Collisions16_JSON.txt --isData -j data16jobs 
 
# 2017 DATA
python runPostProcessing.py /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/DATA/JETHT /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/0lep_maintrees/DATA  --bi keep_and_drop_input.txt --bo keep_and_drop_output.txt -y 2017  -I PhysicsTools.NanoTrees.producers.SystProducer SystProducer2017 -I PhysicsTools.NanoTrees.producers.BaseProducerLight BaseTrees2017_0lep  -I PhysicsTools.NanoTrees.producers.FunkyLeptons FunkyLeps2017_0lep  --json Cert_294927-306462_13TeV_EOY2017ReReco_Collisions17_JSON.txt --isData -j data17jobs 

# 2018 DATA
python runPostProcessing.py /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/DATA/JETHT /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/0lep_maintrees/DATA  --bi keep_and_drop_input.txt --bo keep_and_drop_output.txt -y 2018  -I PhysicsTools.NanoTrees.producers.SystProducer SystProducer2018 -I PhysicsTools.NanoTrees.producers.BaseProducerLight BaseTrees2018_0lep -I PhysicsTools.NanoTrees.producers.FunkyLeptons FunkyLeps2018_0lep  --json Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt --isData -j data18jobs 

# 2016 MC
python runPostProcessing.py /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2016/MC /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/0lep_maintrees/MC  --bi keep_and_drop_input.txt --bo keep_and_drop_output.txt -y 2016 -I PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer puWeight_2016 -I PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer btagSF2016  -I PhysicsTools.NanoTrees.producers.FunkyLeptons FunkyLeps2016_0lep -I PhysicsTools.NanoTrees.producers.SystProducer SystProducer2016 -I PhysicsTools.NanoTrees.producers.BaseProducerLight BaseTrees2016_0lep --datasets ./datasets/nanoaodv5_MC2016.txt -j mc16jobs 

# 2017 MC
python runPostProcessing.py /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2017/MC /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2017/0lep_maintrees/MC  --bi keep_and_drop_input.txt --bo keep_and_drop_output.txt -y 2017 -I PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer puWeight_2017 -I PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer btagSF2017 -I PhysicsTools.NanoTrees.producers.FunkyLeptons FunkyLeps2017_0lep -I PhysicsTools.NanoTrees.producers.SystProducer SystProducer2017 -I PhysicsTools.NanoTrees.producers.BaseProducerLight BaseTrees2017_0lep --datasets ./datasets/nanoaodv5_MC2017.txt -j mc17jobs 

# 2018 MC
python runPostProcessing.py /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2018/MC /eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2018/0lep_maintrees/MC  --bi keep_and_drop_input.txt --bo keep_and_drop_output.txt -y 2018 -I PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer puWeight_2018 -I PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer btagSF2018 -I PhysicsTools.NanoTrees.producers.FunkyLeptons FunkyLeps2018_0lep -I PhysicsTools.NanoTrees.producers.SystProducer SystProducer2018 -I PhysicsTools.NanoTrees.producers.BaseProducerLight BaseTrees2018_0lep  --datasets ./datasets/nanoaodv5_MC2018.txt -j mc18jobs 
```

When you do this you will be prompted with up to 3 questions, for example

```
jobdir data0lep16jobs already exists, remove? [yn] y
outputdir root://cmseos:1094//eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/2016/0lep_maintrees/DATA/pieces already exists, continue? [yn] y
CMSSW tarball /uscms_data/d3/mquinnan/nano4top/CMSSW.tar.gz already exists, remove? [yn] n
```

- The first question about the jobs directory - you must enter yes or rename the jobs directory.
- The second about the root\:/ directory - if you get it enter y to continue.
- The third is regarding the tarbell and is the most important! if you made changes to your code (the modules being submitted) you should redo the tarbell (y). Otherwise enter n for no. If you already have jobs running and you remake the tarbell, this will likely cause the already running jobs to fail. So usually you only have to remake the tarbell for the first set of jobs you submit!

For example, these are the commands for running full Run II data+MC jobs. The first directory is the input source of the ntuples to be processed. The second directory is the output directory where finished files will be copied. -j NAME names the job directory:


You will then get a submit command to enter, for example

```
 condor_submit data0lep16jobs/submit.cmd
```

The jobs directory created will contain `metadata.json` which lists the job submission info (indir, outdir, files, etc) and the job err out and log files. 

- log files contain processing/timing info from the cluster (not very interesting)
- err files contain any error messages or reasons for failing
- out files contain printouts from the job itself and info on the file and modules run


6. Monitor jobs

The following condor commands are useful:

- condor_q - current status of jobs
- condor_rm mquinnan -name lpcschedd1 - remove ALL jobs of your username (mquinnan) on a given scheduler (lpcschedd1, lpcschedd2 or lpcschedd3)
- condor_rm 123456 -name lpcschedd1 - remove all jobs with a certain ID (found using condor_q) on a given scheduler (lpcschedd1, lpcschedd2 or lpcschedd3)
- condor_release mquinnan -name lpcschedd1 - release held jobs of your username on a given scheduler
- condor_q -hold -af HoldReason -  tells you why your jobs are held. usually either an error in the code (look in err files) or space issues

7. Skim and merge completed postprocessed files 

Once the jobs are done, some final systematics need to be added and the filesizes can be further reduced. Here you need to be in the eventMVA directory:

```
/PhysicsTools/NanoTrees/python/macros/eventMVA
```

You will then need to run two python scripts, one to add extra systematics (because numpy dataframes do not like arrays that store list variables, needed for some systematic uncertainties) and one for skimming the files and adding the eventMVA (because catboost is not installed on some condor clusters). I usually run these on 3 screens, one for each year, for example, first run addtheorysysts:

addtheorysysts ONLY needs to be run on MC. so the process for MC files is: 

```
python addtheorysysts.py -f  /eos/uscms//store/user/lpcstop/mequinna/DIRECTORY-OF-FINISHED-FILES/*.root -y 2016
python addtheorysysts.py -f  /eos/uscms//store/user/lpcstop/mequinna/DIRECTORY-OF-FINISHED-FILES/*.root -y 2017
python addtheorysysts.py -f  /eos/uscms//store/user/lpcstop/mequinna/DIRECTORY-OF-FINISHED-FILES/*.root -y 2018
```

This outputs to /PhysicsTools/NanoTrees/python/macros/eventMVA/rootfiles. You can then run tidyroot. For MC files (all systematics included) this is:

```
python tidyroot.py -f ./rootfiles/2016/*.root 
python tidyroot.py -f ./rootfiles/2017/*.root 
python tidyroot.py -f ./rootfiles/2018/*.root 
```

For data files, you can run tidyroot directly without including systematic variations of the event-level bdt. This is for example:

```
python tidyroot.py -f /eos/uscms//store/user/lpcstop/mequinna/DIRECTORY-OF-FINISHED-FILES/*.root -d True 
```

This outputs to /PhysicsTools/NanoTrees/python/macros/eventMVA/updatedrootfiles. You can then merge the files, for example:

```
#hadd MERGED_FILENAME INPUT_FILENAMES
hadd TTTT_2016_merged.root ./updatedrootfiles/TTTT*2016*.root
```

And then finally move the merged files to where you want to access them for plotting/combine. If you are copying to \eos\ use xrdcp, for example:

```
# xrdcp -r FILESTOCOPY*.root root://cmseos.fnal.gov//store/user/USERNAME/EOS-DESTINATION-DIR
xrdcp -r ./*2016*merged*.root root://cmseos.fnal.gov//store/user/lpcstop/mequinna/2016/merged/
```

Don't forget to periodically delete the files in updatedrootfiles and rootfiles.


## Useful combine commands 

Here are various useful commands for use in your combine directory. The main reference is here: http://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/

Script called "run4topcombine.py" for naming parameters in the datacards, located inside the combine directory (for example mine is `/uscms_data/d3/mquinnan/combine/CMSSW_10_2_13/src/HiggsAnalysis/CombinedLimit`)

```
#combines cards with little lablels                                                                                                                                                                    
import subprocess
import numpy as np
import argparse
import ROOT as r
import pandas as pd
import sys
import os
import re

parser = argparse.ArgumentParser(description='')
parser.add_argument("-i", "--indir", dest="indir", nargs="*", default=[])
parser.add_argument("-o", "--outname", dest="outname", default='allhad4top_RunII_datacard.txt')
# parser.add_argument("-o", "--outdir", dest="outdir", default='')                                                                                                                                     
args = parser.parse_args()

cmd='combineCards.py '

for cardpath in args.indir:
    cardname =  cardpath[cardpath.rfind('allhad4top_')+11:]
    cardname = cardname.strip('.txt')
    cmd+=''+cardname+'='+cardpath+' '

# cmd+=' combineCards.py > '+args.outname                                                                                                                                                              
cmd+=' > '+args.outname
print cmd
subprocess.call(cmd, shell=True)
```

For blinded (expected) results:

```
#for limits/significance

combineCards.py /DATACARD_DIR//*.txt > NAME_datacard.txt
combine -M AsymptoticLimits --run expected -d allhad4top_datacard.txt -t -1  -v 1 --expectSignal 1
combine -M Significance -d allhad4top_datacard.txt -t -1 --expectSignal 1

#for impacts: (m doesnt matter, but if running many impacts at once use different numbers)

python run4topcombine.py -i /DATACARD_DIR/*.txt
text2workspace.py allhad4top_RunII_datacard.txt -o NAME_workspace.root
combineTool.py -M Impacts -d NAME_workspace.root --rMin -1 --rMax 2 --robustFit 1 --doInitialFit --expectSignal 1 -m 2 -t -1
combineTool.py -M Impacts -d NAME_workspace.root --rMin -1 --rMax 2 --robustFit 1 --doFits -m 2 --expectSignal 1 -t -1 --parallel 50
combineTool.py -M Impacts -d NAME_workspace.root --rMin -1 --rMax 2 --robustFit 1 --output impacts.json -m 2 --expectSignal 1 -t -1
plotImpacts.py -i impacts.json -o impacts

```

For unblinded (observed) results:

```
#for limits/significance

combineCards.py /DATACARD_DIR//*.txt > NAME_datacard.txt
combine -M AsymptoticLimits -d allhad4top_datacard.txt
combine -M Significance -d allhad4top_datacard.txt

#for impacts: (m doesnt matter, but if running many impacts at once use different numbers)

python run4topcombine.py -i /DATACARD_DIR/*.txt
text2workspace.py allhad4top_RunII_datacard.txt -o NAME_workspace.root
combineTool.py -M Impacts -d NAME_workspace.root  --robustFit 1 --doInitialFit -m 2 
combineTool.py -M Impacts -d NAME_workspace.root  --robustFit 1 --doFits -m 2 --parallel 50
combineTool.py -M Impacts -d NAME_workspace.root--robustFit 1 --output impacts.json -m 2
plotImpacts.py -i impacts.json -o impacts

#for postfit distributions and fit to signal

python run4topcombine.py -i /DATACARD_DIR/*.txt
text2workspace.py allhad4top_RunII_datacard.txt -o NAME_workspace.root
combine -M FitDiagnostics NAME_workspace.root -m 2 --saveShapes --saveWithUncertainties  --robustFit 1  -v 3 
mv fitDiagnostics.root fitDiagnostics_NAME.root

#for goodness of fit test statistic distributions
#for example for 100 toys, random seed of 1, output rootfile higgsCombineTest.GoodnessOfFit.mH120.1.root for toys

combine -M GoodnessOfFit NAME_datacard.txt --algo=saturated
combine -M GoodnessOfFit NAME_datacard.txt --algo=saturated -t 100 -s 1

```

