import ROOT
import os
import math
import json
import logging
import numpy as np
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper
from PhysicsTools.NanoTrees.helpers.ConfigHelper import JetSetter, LepSetter
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import closest

class TriggerCorrections(Module):
    def __init__(self, channel, year):
        self.verbose = False
        self.year    = year
        self.channel = channel


        print "LOADING HISTOGRAMS>>>>>"
        self.jethtSFs           = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/TriggerSFFiles/cvalue.root')

        #open root files:
        self.file0lepSF        = ROOT.TFile(self.jethtSFs)

        #get histograms
        self.hist0lepSF      = self.file0lepSF.Get("nbnj"+year)


    def beginJob(self):
        self.LS = LepSetter(self.channel, self.year)
        self.JS = JetSetter(self.channel, self.year)

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.isMC = bool(inputTree .GetBranch('genWeight'))
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree

        # self.out.branch("trigSF_1lep",          "F")
        # self.out.branch("trigSF_upper_1lep",    "F")
        # self.out.branch("trigSF_lower_1lep",    "F")
        self.out.branch("trigCorr_0lep",          "F")
        self.out.branch("trigCorr_upper_0lep",    "F")
        self.out.branch("trigCorr_lower_0lep",    "F")

    def get_sf(self, hist, var1, var2, syst=None):
        # `eta` refers to the first binning var, `pt` refers to the second binning var
        x, y = var1, var2
        SF = np.ones(3)
        SF = self.getBinContent2D(hist, x, y)
        if syst is not None:
            stat = SF[1] - SF[0]
            unc  = np.sqrt(syst*syst+stat*stat)
            SF[1] = SF[0] + unc
            SF[2] = SF[0] - unc
        return SF

    def getBinContent2D(self, hist, x, y):
        bin_x0 = hist.GetXaxis().FindFixBin(x)
        bin_y0 = hist.GetYaxis().FindFixBin(y)
        binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
        binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
        if binX>hist.GetNbinsX(): ##? set to last bin
            binX=hist.GetNbinsX()
        if binY>hist.GetNbinsY():
            binY=hist.GetNbinsY()
        val = hist.GetBinContent(binX, binY)
        # print 'val', val, 'binX', binX, 'binY', binY, 'last bin:', hist.GetNbinsX(), hist.GetNbinsY()
        err = hist.GetBinError(binX, binY)
        return np.array([val, val + err, val - err])

    def getTriggerSFs(self, event):
        if self.isMC:
            # self.out.fillBranch("trigSF_1lep",  1.0)
            # self.out.fillBranch("trigSF_upper_1lep",  1.0)
            # self.out.fillBranch("trigSF_lower_1lep",  1.0)
            self.out.fillBranch("trigCorr_0lep",  1.0)
            self.out.fillBranch("trigCorr_upper_0lep",  1.0)
            self.out.fillBranch("trigCorr_lower_0lep",  1.0)
            # return False

        else:
            #jettyjets
            ak4jets    = Collection(event,"Jet")
            if self.LS.clean_jetsvleps:
                ak4jets, SelEls, SelMus, SelLeps = self.LS.getLeps(event)
            cleanjets, bjets  = self.JS.getJets(self.year, event, ak4jets, getvars=False)

            njets  = len(cleanjets)
            nbjets = len(bjets)
            # if self.verbose:
            #     print 'njets', njets, 'nbjets', nbjets
            
            if self.channel == '0lep':
                sf=1.0; sfu=1.0; sfl=1.0
                SF = self.get_sf(self.hist0lepSF, nbjets, njets)
                # if SF[0]==0.0: #no zero SFs
                #     SF = [1,1,1]
                sf=SF[0]; sfu=SF[1]; sfl=SF[2]
                self.out.fillBranch("trigCorr_0lep",        sf)
                self.out.fillBranch("trigCorr_upper_0lep",  sfu)
                self.out.fillBranch("trigCorr_lower_0lep",  sfl)


    # fill scale factors
    def analyze(self, event):
        self.getTriggerSFs(event)
        # if self.verbose:
        #     print 'isMC and will calculate SFs?', calc
        return True

# TrigSF2016_1lep = lambda : LeptonSFProducer('1lep','2016')
TrigC2016_0lep = lambda : TriggerCorrections('0lep','2016')
TrigC2017_0lep = lambda : TriggerCorrections('0lep','2017')
TrigC2018_0lep = lambda : TriggerCorrections('0lep','2018')

