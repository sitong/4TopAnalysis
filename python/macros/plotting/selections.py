#defines control regions and cutstrings
import numpy as np
import ROOT as r

#first lep
elstr   = " && lep1pt>=32 && lep1id==11"#changed ept to 40 from 32
mustr   = " && lep1pt>=27 && lep1id==13" #changed mupt to 27 from 24 to 30
    #second lep
eestr   = " && lep1pt>=32. && lep1id==11 && lep2id==11 && lep2pt>=20.0" #change prim e pt from 32 to 40
mumustr = " && lep1pt>=27. && lep1id==13 && lep2id==13 && lep2pt>=15.0"
emustr  = " && lep1pt>=32. && lep1id==11 && lep2id==13 && lep2pt>=15.0"
muestr  = " && lep1pt>=27. && lep1id==13 && lep2id==11 && lep2pt>=20.0"
    #third lep
lep3str = "&& lep3pt>=10."

seldict = {
        'closure'   : '(nrestops_nosys>=1 && ',
        # 'extclosure': '(nrestops>=2 && nbstops>=0 && ht>=700 && ht<1200 &&',
        'extclosure': '(nrestops_nosys==1 && nbstops_nosys>=1 &&',

        'dataclosure'   : '(nrestops_nosys>=1 && ',
        # 'dataclosure'   : '(nrestops==1 && nbstops==0 && ', #r1b0
        # 'dataclosure'   : '(nrestops==1 && nbstops>=1 && ', #r1b1
        # 'dataclosure'   : '(nrestops>=2 && nbstops>=0 && ', #r2b0

        'dataextclosure': '(nrestops_nosys>=1 &&',
        # 'dataextclosure': '(nrestops==1 && nbstops==0 && ', #r1b0
        # 'dataextclosure': '(nrestops==1 && nbstops>=1 && ', #r1b1
        # 'dataextclosure': '(nrestops>=2 && nbstops>=0 && ', #r2b0

        # 'dataextclosure': '(nrestops>=1 && (nVRleps==1 && nVRleps<4 ) &&',
        'restopEffSF'  : '(nfunky_leptons==1 && nbjets==1 && njets>=4 && rescand_disc>-1.0',
        'restopEffSFwp'  : '(nfunky_leptons==1 && nbjets==1 && njets>=4 && rescand_disc>0.9992',

        'restopMissSF'  : '(nfunky_leptons==0 && nbjets==1 && ht>=1000 && njets>=6 && rescand_disc>-1.0',
        'restopMissSFwp'  : '(nfunky_leptons==0 && nbjets==1 && ht>=1000 && njets>=6 && rescand_disc>0.9992',
        # 'restopMissSFtest'  : '(nleptons==0 && ht>=1000 && nbjets==2 && njets>=5 && rescand_disc>-1.0',
        # 'restopMissSFtest'  : '(nleptons==1 && ht>=700 && nbjets==0 && njets>=4 && rescand_disc>-1.0',

        'D'             : '(nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==8',   #for VR
        # 'D'         : '(nrestops==1 && nbstops==0 && nleptons==0 && ht>=700 && nbjets>=3 && njets==8',   #for VR #r1b0
        # 'D'         : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets>=3 && njets==8',   #for VR #r1b1
        # 'D'         : '(nrestops>=2 && nbstops>=0 && nleptons==0 && ht>=700 && nbjets>=3 && njets==8',   #for VR r2b0

        # 'D'             : '(nrestops_nosys>=1 && nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9', #for SR
        # 'D'         : '(nrestops==1 && nbstops==0  && nleptons==0 && ht>=700 && nbjets>=3 && njets>=9', #for SR
        # 'D'         : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets>=3 && njets>=9', #for SR
        # 'D'         : '(nrestops>=2 && nbstops>=0 && nleptons==0 && ht>=700 && ht<1200 && nbjets>=3 && njets>=9', #for SR
        # 'D'         : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets>=3 && njets>=9', #for SR


        'none0l'    : '(nleptons==0 && njets>=7)',
        'none1l'    : '(nleptons>=1 && njets>=3',
        '0lbase'    : '(nleptons==0 && ht>=700 && nbjets>=3 && njets>=9',
        '0lbasert'  : '(nleptons==0 && ht>=700 && nbjets>=3 && njets>=9 && nrestops>=1',

        'CR1'       : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==7', #for VR
        'CR2'       : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==7',
        'CR3'       : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==8',
        'CR1p'      : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==6',
        'CR2p'      : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==6',

        # 'CR1'       : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==8', #for SR
        # 'CR2'       : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==8',
        # 'CR3'       : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys>=9',
        # 'CR1p'      : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys==2 && njets_nosys==7',
        # 'CR2p'      : '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==7',

        # 'CR1'       : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets==2 && njets==7', #for VR/SR tests
        # 'CR2'       : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets>=3 && njets==7',
        # 'CR3'       : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets==2 && njets==8',
        # 'CR1p'      : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets==2 && njets==6',
        # 'CR2p'      : '(nrestops==1 && nbstops>=1 && nleptons==0 && ht>=700 && nbjets>=3 && njets==6',


        # 'CR1'       : '(ht>=700 && nbjets==2 && njets==8', #test
        # 'CR2'       : '(ht>=700 && nbjets>=3 && njets==8',
        # 'CR3'       : '(ht>=700 && nbjets==2 && njets>=9',
        # 'CR1p'      : '(ht>=700 && nbjets==2 && njets==7',
        # 'CR2p'      : '(ht>=700 && nbjets>=3 && njets==7',

        'CR4'       : '(nleptons==0 && ht>=700 && nbjets>=2 && njets==8 && nrestops>=1',
        'CR5'       : '(nleptons==0 && ht>=700 && nbjets==2 && njets>=9 && nrestops>=1',
        'CR4p'      : '(nleptons==0 && ht>=700 && nbjets>=2 && njets==7 && nrestops>=1',
        'CR1rt1'    : '(nleptons==0 && ht>=700 && nbjets==2 && njets==8 && nrestops>=1',
        'CR2rt1'    : '(nleptons==0 && ht>=700 && nbjets>=3 && njets==8 && nrestops>=1',

        'VR8j'      : '(nleptons==0 && ht>=700 && nbjets>=3 && njets==8',
        'VR0rt'     : '(nleptons==0 ht>=700 && nbjets>=3 && njets>=9 && nrestops==0',
        'VRbt'      : '(nleptons==0 ht>=700 && nbjets>=3 && njets>=9 && nrestops==0 && nbstops>=0',
        'VR_badlep' : '(ht>=700 && nbjets>=3 && njets>=9 && nrestops>=1 && (nVRleps==1 || nVRleps<4 )',
        'VR8j_badlep'      : '(ht>=700 && nbjets>=3 && njets>=7 && njets<9 && nVRleps==1',
        'VR0rt_badlep'     : '(ht>=700 && nbjets>=3 && njets>=9 && nrestops==0 && nVRleps==1',
        'VRbt_badlep'      : '(ht>=700 && nbjets>=3 && njets>=9 && nrestops==0 && nbstops>=0 && nVRleps==1',

        'qcdrt0b0'  : '(nleptons==0 && nbjets==0 && njets>=9 && met<=100 && ht>=700 && nrestops==0',
        'qcdrt0b1'  : '(nleptons==0 && nbjets==1 && njets>=9 && met<=100 && ht>=700 && nrestops==0',
        'qcdrt0b2'  : '(nleptons==0 && nbjets==2 && njets>=9 && met<=100 && ht>=700 && nrestops==0',
        'qcdrt0b3'  : '(nleptons==0 && nbjets>=3 && njets>=9 && met<=100 && ht>=700 && nrestops==0',
        'qcdrt1b1'  : '(nleptons==0 && nbjets==1 && njets>=9 && met<=100 && ht>=700 && nrestops>=1',
        'qcdrt1b2'  : '(nleptons==0 && nbjets==2 && njets>=9 && met<=100 && ht>=700 && nrestops>=1',
        'qcdrt1b0'  : '(nleptons==0 && nbjets==0 && njets>=9 && met<=100 && ht>=700 && nrestops>=1',
        'ttbarcheck': '(nleptons==0 && nbjets==2 && njets>=9 && met>100 && ht>=700',
        '1lbase'    : '(nleptons==1 && met>50 && lep1pt>=30 && nbjets>=3 && njets>=8',
        '1lloose2'    : '(nleptons==1 && nbjets>=1 && njets>=4 && topdiscs>-1.0',
        '1lbasert'  : '(nleptons==1 && met>50 && lep1pt>=30 && nbjets>=3 && njets>=8 && nrestops>=1',
        '1ebase'    : '(nleptons==1 && ht>500 && njets>=3 && met>100 && nbjets>=1'+elstr+'',
        '1mubase'   : '(nleptons==1 && ht>500 && njets>=3 && met>100 && nbjets>=1'+mustr+'',
        # '1ebase'    : '(nleptons==1 && ht<1000 && njets>=3 && met>100 && nbjets>=2'+elstr+'',
        # '1mubase'   : '(nleptons==1 && ht<1000 && njets>=3 && met>100 && nbjets>=2'+mustr+'',
        # '2lbase'    : '(nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1 && lep1pt>=30.0 && lep2pt>=20.0 ',
        '2lbase'    : '(nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1 && lep1pt>=30.0 && lep2pt>0.0 ',
        'eebase'    : '(nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1'+eestr+'',
        'mumubase'  : '(nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1'+mumustr+'',
        'emubase'   : '(nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1'+emustr+'',
        'muebase'   : '(nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1'+muestr+'',
        '3lbase'    : '(nleptons>=3 && ht>500 && njets>=3 && met>100 && nbjets>=1 && lep1pt>=30.0 && lep2pt>=20.0 && lep3pt>=10.0 ',
        'eexbase'   : '(nleptons>=3 && ht>500 && njets>=3 && met>100 && nbjets>=1'+eestr+lep3str+'',
        'mumuxbase' : '(nleptons>=3 && ht>500 && njets>=3 && met>100 && nbjets>=1'+mumustr+lep3str+'',
        'emuxbase'  : '(nleptons>=3 && ht>500 && njets>=3 && met>100 && nbjets>=1'+emustr+lep3str+'',
        'muexbase'  : '(nleptons>=3 && ht>500 && njets>=3 && met>100 && nbjets>=1'+muestr+lep3str+'',
        '1lcombobase'  : '(nleptons==1 &&ht>500 && njets>=3 && met>100 && nbjets>=1',
        '2lcombobase'  : 'nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1',
        '3lcombobase'  : 'nleptons==3 && ht>500 && njets>=3 && met>100 && nbjets>=1'+lep3str,
        'emucombobase' : 'nleptons==2 && ht>500 && njets>=3 && met>100 && nbjets>=1',
        'emuxcombobase': 'nleptons==3 && ht>500 && njets>=3 && met>100 && nbjets>=1'+lep3str 
        }

#given control region name return proper cutstring
def getcut(crname, format='mc', chan='0lep', year='2016', lumi='36.0',rtcut=False):
    metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
    metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'

    if 'data' in crname:
        format = 'data'
    #trigger selections
    trigs=''
    if not format=='mc': 
        #2016 only!
        if chan=='1lep':
            # pass
            if format=='data1e':
                # trigs+=' && HLT_Ele27_WPTight_Gsf' #'&& passtriglep==1'#'&&(passtrigel && !passtrigmu)'#
                trigs+=' && (trigHLT_Ele27_WPTight_Gsf || trigHLT_Ele32_WPTight_Gsf)' #'&& passtriglep==1'#'&&(passtrigel && !passtrigmu)'#
            elif format=='data1mu':
                # trigs+='&& (HLT_IsoMu24 || HLT_IsoTkMu24)' #'&& passtriglep==1'#'&&(passtrigmu &&  !passtrigel)'
                trigs+='&& (trigHLT_IsoMu27 || trigHLT_IsoMu30)' #'&& passtriglep==1'#'&&(passtrigmu &&  !passtrigel)'
        elif chan=='0lep':
            # Trigs+='&& ( HLT_PFHT900==1)'
            if year=='2016':
                trigs+='&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16
            elif year=='2017':
                # trigs+=' && (HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || HLT_PFHT430_SixJet40_BTagCSV_p080 || HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || HLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters
                trigs+=' && ( trigHLT_PFHT1050 )'+metfilters
            elif year=='2018':
                trigs+='&& (HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters

    #no underflow
    undercut = ''
    if rtcut:
        undercut+= ' && restop1pt>0'
    
    cutstr = seldict[crname]
    if 'closure' not in crname:
        cutstr = cutstr+undercut+trigs
        if 'combo' not in crname:
            cutstr+=')' 
    elif 'closure' in crname:
        cutstr = cutstr+undercut

    if format == 'mc':
        # if crname == '1lcombobase':
        #     newstr = '( ('+cutstr+elstr+') || ('+cutstr+mustr+') )'
        #     cutstr = newstr            
        # elif crname=='2lcombobase' or crname=='3lcombobase':            
        #     newstr = '( ('+cutstr+eestr+') || ('+cutstr+mumustr+') || ('+cutstr+emustr+') || ('+cutstr+muestr+') )'
        #     cutstr = newstr
        # elif crname=='emucombobase' or crname=='emuxcombobase': 
        #     newstr = '( ('+cutstr+emustr+') || ('+cutstr+muestr+') )'
        #     cutstr = newstr
        # cutstr+='*weight*'+lumi
        # cutstr+='*weight*evtbtagSFshape_preselJets*'+lumi
        cutstr+='*weight*'+lumi#'*weight*evtbtagSFshape_preselJets*puWeight*'+lumi
        # if chan=='0lep':
        #     cutstr+='*trigSF_0lep'
            # cutstr+='*lep_evtVetoSF*trigSF_0lep'
        # elif chan=='1lep' or chan=='1el' or chan=='1mu':
        #     cutstr+='*lep_evtSelSF'

        #normalization for sfs
        # if crname=='restopEffSF' or crname=='restopEffSFwp':
        #     cutstr+='*1.06362730977' #2016
        #     # cutstr+='*1.4' #2016
        # elif crname=='restopMissSF' or crname=='restopMissSFtest' or crname=='restopMissSFwp':
        # #     # cutstr+='*0.79867150748' #wjets sf
        #     cutstr+='*0.93439152974' #ht 1000
            # cutstr+='*0.89934861553' #ht 1200
            # cutstr+='*0.85788742811' #ht 1500
            # cutstr+='*0.84434440956' #ht 1500 nj 7
            # cutstr+='*0.87688309524' #ht 1200 nj 8
            # cutstr+='*0.83585014787' #ht 1500 nj 8
            # cutstr+='*0.86913892016' #ht 1500 nj 5
            # cutstr+= '*0.95051935145' #cand ht 1000 bj 2 nj5
            # cutstr+= '*0.92946027724' #cand ht 1200 bj 2 nj5
        #'*0.89473369717'#'*0.89619110335'#'*0.90166901043'

    elif format == 'data1e' or format == 'data1mu': #needs to be organized by single el or single mu
        if format=='data1e' and (crname=='1lcombobase'):
            newstr = '( ('+cutstr+elstr+') )'
            cutstr = newstr            
        elif format=='data1e' and (crname=='2lcombobase' or crname=='3lcombobase'):
            newstr = '( ('+cutstr+eestr+') || ('+cutstr+emustr+') )'
            cutstr = newstr            
        elif format=='data1e' and (crname=='emucombobase' or crname=='emuxcombobase'):
            newstr = '('+cutstr+emustr+')'
            cutstr = newstr
        elif format=='data1mu' and (crname=='1lcombobase'):
            newstr = '( ('+cutstr+mustr+') )'
            cutstr = newstr
        elif format=='data1mu' and (crname=='2lcombobase' or crname=='3lcombobase'):
            newstr = '( ('+cutstr+mumustr+') || ('+cutstr+muestr+') )'
            cutstr = newstr
        elif format=='data1mu' and (crname=='emucombobase' or crname=='emuxcombobase'):
            newstr = '('+cutstr+muestr+')'
            cutstr = newstr

    # if crname=='restopSF': #temp fix
    #     cutstr=''
    print 'cut:', cutstr
    return cutstr

def getHistSettings(var):
    vardict = {
        #[nbins, xmin, xmax]
        # 'ht_nosys'      : [10, 0.0, 3000.0],
        'ht_nosys'      : [1, 0.0, 3000.0],
        'ht'      : [20, 0.0, 2000.0],
        'bjht'    : [50, 0, 1500],
        'met'     : [20, 0.0, 300.0],
        'metovsqrtht' : [50, 0.0, 200.0],
        'sfjm'    : [100, 0, 1000],
        'njets_nosys'   : [20, 0, 20],
        'nbjets_nosys'  : [10, 0, 10],
        'njets'   : [20, 0, 20],
        'nbjets'  : [10, 0, 10],
        'nbsws'   : [10, 0, 10],
        'nrestops_nosys': [2, 0, 2],
        'nrestops': [2, 0, 2],
        'nbstops_nosys' : [6, 0, 6],
        'nleptons': [20, 0, 20],
        'fwm0'  : [50, 0, 1],
        'fwm1'  : [50, 0, 1],
        'fwm2'  : [50, 0, 1],
        'fwm3'  : [50, 0, 1],
        'fwm4'  : [50, 0, 1],
        'fwm5'  : [50, 0, 1],
        'fwm6'  : [50, 0, 1],
        'fwm7'  : [50, 0, 1],
        'fwm8'  : [50, 0, 1],
        'fwm9'  : [50, 0, 1],
        'fwm10'  : [50, 0, 1],
        'fwm20'  : [50, 0, 1], 
        'fwm30'  : [50, 0, 1],
        # 'topdiscs'  : [50, -1, 1],
        'rescand_disc'  : [20, 0.999, 1],
        'rescand_pt'  : [15, 0.0, 800],
        'rescand_ptDR'  : [15, 0.0, 1000],
        'rescand_mass'  : [20, 0.0, 500],
        'funkylep1_pt'  : [20, 0.0, 1000],
        'jet1pt'  : [50, 0, 1000],
        'jet2pt'  : [50, 0, 1000],
        'bstop1_pt'  : [50, 100, 1000],
        'bstop2_pt'  : [50, 100, 1000],
        'restop1pt'  : [50, 100, 1000],
        'restop2pt'  : [50, 100, 1000],
        'qglsum'  : [50, 0, 30],
        'qglprod'  : [50, 0, 30],
        'jet8pt'  : [50, 0, 200],
        'aplanarity'  : [20, 0, 1],
        'sphericity'  : [20, 0, 1],
        'dphij1j2'  : [10, -4, 4],
        'detaj1j2'  : [10, -4, 4],
        'dphib1b2'  : [10, -4, 4],
        'detab1b2'  : [10, -4, 4],
        'leadbpt' : [50, 0, 500],
        'npv' : [50, 0, 100],
        'lep1pt'  : [50, 0, 250],
        'lep2pt'  : [50, 0, 250],
        'ModBDT2disc': [100, 0.0, 1.0],
        'evtbdtdisc': [100, 0.0, 1.0],
        'catBDTdisc': [50, 0.0, 1.0],
        'NewBDT3disc': [100, 0.0, 1.0],
        'NewBDT4disc': [100, 0.0, 1.0],
        'Best12disc': [100, 0.0, 1.0],
        'passtrigel': [2, 0,1], 
        'passtrigmu': [2, 0,1],
        }
    params = vardict[var]
    nbins = params[0]
    selrange = (params[1], params[2])
    return nbins, selrange
