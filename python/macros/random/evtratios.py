import subprocess
import os
import argparse
import sys
import math
import numpy as np
import ROOT as r
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
r.gROOT.SetBatch(True)

r.gROOT.SetBatch(1)
parser = argparse.ArgumentParser(description='xsec weights')
parser.add_argument("-p", "--proc", dest="proc", default='TT')
args = parser.parse_args()

myproc = args.proc
eos = '/eos/uscms/'
# indir = '/store/user/cmantill/bbVV/input/AK8_May30/2017/' 
indir = '/store/user/cmantill/bbVV/input/AK8_May30_Validation/2017/'
#/eos/uscms/store/user/cmantill/bbVV/input/AK8_May30_Validation/2017/GluGluHToWWToLNuQQ
prefix = 'root://cmseos.fnal.gov//'

#cut definitions
base = "(fj_pt>300 && fj_pt<2500 && abs(fj_eta)<2.4 && fj_msoftdrop>=30 && fj_msoftdrop<320) &&"

elsig = "(fj_H_VV_elenuqq==1) "
musig = "(fj_H_VV_munuqq==1) "
tausig= "((fj_H_VV_leptauelvqq==1) || (fj_H_VV_leptaumuvqq==1) || (fj_H_VV_hadtauvqq==1)) "

ttbar = "((fj_Top_2q==1) || (fj_Top_elenu==1) || (fj_Top_munu==1) || (fj_Top_taunu==1)) "
qcd = "((fj_isQCDb==1) || (fj_isQCDbb==1) || (fj_isQCDc==1) || (fj_isQCDcc==1) || (fj_isQCDothers==1)) "
wjets = "((fj_V_2q==1) || (fj_V_elenu==1) || (fj_V_munu==1) || (fj_V_taunu==1)) "

lepmerged = " && fj_lepinprongs==1"
lepsplit = "&& fj_lepinprongs!=1"

procdirs = {'bulkg':['BulkGravitonToHHTo4W_JHUGen_MX-600to6000_MH-15to250_v2', 'BulkGravitonToHHTo4W_JHUGen_MX-600to6000_MH-15to250_v2_ext1'],
            'sm':['GluGluHToWWToLNuQQ'],
            'jhu':['JHUVariableWMass_part1', 'JHUVariableWMass_part2','JHUVariableWMass_part3'],
            'ttbar':['TTToSemiLeptonic'],
            'wjets':['WJetsToLNu_HT-1200To2500'],
            'qcd':['QCD_Pt_300to470','QCD_Pt_470to600','QCD_Pt_600to800','QCD_Pt_800to1000','QCD_Pt_1000to1400']}

#signal version
cuts = [base+elsig+lepmerged, base+elsig+lepsplit, base+elsig,
        base+musig+lepmerged, base+musig+lepsplit, base+musig,
        base+tausig+lepmerged, base+tausig+lepsplit, base+tausig]
if myproc in ['ttbar', 'wjets', 'qcd']:
    #background version
    cuts = [base+ttbar+lepmerged, base+ttbar+lepsplit, base+ttbar,
            base+wjets+lepmerged, base+wjets+lepsplit, base+wjets,
            base+qcd+lepmerged, base+qcd+lepsplit, base+qcd]


ylds = [0.0]*len(cuts)

print procdirs[myproc]

for proc in procdirs[myproc]:
    procdir = eos+indir+proc+'/root/'
    for subdir, dirs, files in os.walk(procdir):
        print subdir
        for i,f in enumerate(files):
            fname = subdir+'/'+f
            fname=fname.replace("/eos/uscms/","root://cmseos.fnal.gov/")
            print i, fname
            rf = r.TFile.Open(fname)
            tree = rf.Get('Events')
            for j,cut in enumerate(cuts):
                N = tree.GetEntries(cut)
                ylds[j] += N
                print j, cut, ylds[j]
            rf.Close()
    
print 'proc', myproc, procdirs[myproc]
print 'cuts', cuts
print 'yields:', ylds
