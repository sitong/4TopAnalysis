import subprocess
import os.path
# import catboost as cat
from catboost import CatBoostClassifier
import varlists as vl
import os.path
from rootpy.tree import Tree, TreeModel, FloatCol
from rootpy.io import root_open
from rootpy.tree import Tree
import argparse
from array import array
import numpy as np
import pandas as pd
import ROOT as r
from thysystnormrecord import pdfnorms, MEnorms#totInt, MEnorms1, pdfnorms1, MEnorms2, pdfnorms2
# #adds theoretical uncertainties
parser = argparse.ArgumentParser(description='rootfile skimmer and more!')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
args = parser.parse_args()

SFs={'miss':{'2016':[1.0, 1.3486641645431519, 1.148268222808838, 1.1212979555130005, 1.1519887447357178, 1.1012041568756104, 1.0909525156021118, 1.157222867012024, 1.0504330396652222, 0.9640796184539795, 1.0218476057052612, 1.0670303106307983],
             '2017':[1.0, 1.1454821825027466, 1.1085493564605713, 1.0633724927902222, 1.1575764417648315, 1.0952008962631226, 1.0599652528762817, 1.0429214239120483, 0.9864946007728577, 1.0190281867980957, 1.0380607843399048, 0.9544106721878052],
             '2018':[1.0, 1.1755003929138184, 1.1824177503585815, 1.0982625484466553, 1.041006088256836, 1.01741361618042, 0.9709364771842957, 0.9495097398757935, 0.9101343750953674, 0.9283883571624756, 0.8783420920372009, 0.9578810334205627]},
     'eff':{'2016':[1.044, 0.955, 0.978],
            '2017':[0.916, 0.99, 0.866],
            '2018':[0.845, 0.858, 0.954]}
}

# [1.0(0-100), 1.248809814453125(100-150), 1.2329858541488647(150-200), 1.1429007053375244(200-250), 1.2091445922851562(250-300), 1.0850862264633179(300-350), 1.013912320137024(350-400), 1.0221302509307861(400-450), 0.9723251461982727(450-500), 0.9767608046531677(500-550), 0.9037830233573914(550-600), 0.9347902536392212(600-650), 0.8901468515396118(650-700), 0.9871330261230469(>700)]
#bins
# [ 0.  100.  150.  200.  250.  300.  350.  400.  450.  500.  550.  600.  650.  700. 1000.]
#   0     1    2     3    4      5     6    7      8     9     10    11    12    13   14

addevtsf= True

def addSF(candpt, year, typ='eff'):
    sf=1.0
    sflist = SFs[typ][year]
    if typ =='miss':
        if candpt >=100.0 and candpt<200.0:
            sf=sflist[1]
        elif candpt >=200.0 and candpt<300.0:
            sf=sflist[2]
        elif candpt >=300.0 and candpt<350.0:
            sf=sflist[3]
        elif candpt >=350.0 and candpt<400.0:
            sf=sflist[4]
        elif candpt >=400.0 and candpt<450.0:
            sf=sflist[5]
        elif candpt >=450.0 and candpt<500.0:
            sf=sflist[6]
        elif candpt >=500.0 and candpt<550.0:
            sf=sflist[7]
        elif candpt >=550.0 and candpt<600.0:
            sf=sflist[8]
        elif candpt >=600.0 and candpt<650.0:
            sf=sflist[9]
        elif candpt >=650.0 and candpt<700.0:
            sf=sflist[10]
        elif candpt >=700.0:
            sf=sflist[11]
    elif typ=='eff':
        if candpt >=100.0 and candpt<300.0:
            sf=sflist[0]
        elif candpt >=300.0 and candpt<500.0:
            sf=sflist[1]
        elif candpt >=500.0:
            sf=sflist[2]
    return sf

#load files:
for f in args.filenames: #get proper outname:
    f=f.replace("/eos/uscms/","root://cmseos.fnal.gov/")
    print 'prepping input file', f, '...'
    year = '2016'
    WP = 0.9988
    if '2017' in f:
        year='2017'
        WP=0.9992
    if '2018' in f:
        year = '2018'
        WP=0.9994
    print 'year', year
    outdir = './'
    outname = f[f.rfind('/')+1:]
    rf = r.TFile.Open(f)
    tree = rf.Get('Friends')
    nevts = tree.GetEntries()
    print 'nentries: ', nevts
    print outdir+outname
    outrf = r.TFile.Open(outdir+outname, "update")    
    outrf.cd()
    outtree=tree.CloneTree(0)#0 #clone events    
    
    effSF = np.full(1, 1.0, dtype=np.float32)
    outtree.Branch('effSF',   effSF,   'effSF/F')
    missSF = np.full(1, 1.0, dtype=np.float32)
    outtree.Branch('mistagSF',   missSF,   'mistagSF/F')
    if addevtsf:
        evtmissSF = np.full(1, 1.0, dtype=np.float32)
        outtree.Branch('evtmistagSF',   evtmissSF,   'evtmistagSF/F')
        evteffSF = np.full(1, 1.0, dtype=np.float32)
        outtree.Branch('evteffSF',   evteffSF,   'evteffSF/F')
        evtdict={}
        effdict={}
        missdict={}
        msf_evt=1.0; esf_evt=1.0
    for i, evt in enumerate(tree):
        if evt.candnum==0:
            di = i
            msf_evt=1.0; esf_evt=1.0
        # print i, evt.candnum
        if evt.rescand_disc>WP and evt.rescand_genmatch==0:
            msf = addSF(evt.rescand_pt, year, typ='miss')
            esf = 1.0
        elif evt.rescand_disc>WP and evt.rescand_genmatch==1:
            msf = 1.0
            esf = addSF(evt.rescand_pt, year, typ='eff')
        else:
            esf=1.0; msf=1.0
        if addevtsf:
            msf_evt*=msf; esf_evt*=esf
            evtdict[di]=[msf_evt, esf_evt]
            # print i, di, evt.candnum, msf, esf, msf_evt, esf_evt
        # print evt.rescand_pt, sf
        if not addevtsf:
            effSF[0]=esf
            missSF[0]=msf
            outtree.Fill()
        else:
            effdict[i]=esf
            missdict[i]=msf

    # for key, val in evtdict.iteritems():
    #     print key, val
    if addevtsf:
        for i, evt in enumerate(tree):
            if evt.candnum==0:
                di = i
            evteffSF[0]=evtdict[di][1]
            evtmissSF[0]=evtdict[di][0]
            effSF[0]=effdict[i]
            missSF[0]=missdict[i]
            # print i, evt.candnum,effSF[0], missSF[0], evteffSF[0], evtmissSF[0]

            outtree.Fill()

    outtree.Write("", r.TFile.kOverwrite)
    outtree.ResetBranchAddresses()

    outrf.Close()
    rf.Close()
    print 'wrote', outrf

