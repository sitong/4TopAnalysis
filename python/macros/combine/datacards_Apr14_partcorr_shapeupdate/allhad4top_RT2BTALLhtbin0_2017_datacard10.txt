## card: ./datacards_Apr14_partcorr_shapeupdate//allhad4top_RT2BTALLhtbin0_2017_datacard10.txt number in set: 10
##selection: (nrestops_nosys>=2 && nbstops_nosys>=0 && ht_nosys<1100 &&  nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*41.53*prefirewgt*

#----------------------------------------------#
#generating datacards. name: allhad4top, year2017

#number of cards in set: 12

## number of signals (i), backgrounds (j), and uncertainties (k)
---
imax: 1
jmax: 3
kmax: *
---

## input the bdt shape histograms
---
shapes * * allhad4top_2017_shapehists.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
---

## list the bin label and the number of (data) events observed
---
bin  RT2BTALLhtbin0_2017
observation 552.0
---

## expected events for signal and all backgrounds in the bins
---
bin        RT2BTALLhtbin0_2017    RT2BTALLhtbin0_2017    RT2BTALLhtbin0_2017    RT2BTALLhtbin0_2017
process    TTTT    TTX    other    DDBKG
process        0        1        2        3
rate       2.08149836399    13.9264444709    6.54215346277    582.581
---

## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin
---
## statistical uncertainties

RT2BTALLhtbin0_2017_TTTT_stat      lnN    1.01494955734    -    -    -
RT2BTALLhtbin0_2017_TTX_stat      lnN    -    1.07002867936    -    -
RT2BTALLhtbin0_2017_other_stat      lnN    -    -    1.30498608641    -
RT2BTALLhtbin0_2017_DDBKG_stat      lnN    -    -    -    1.09150487228

---
## systematic uncertainties

lumi_uncorr_2017   lnN    1.020    1.020    1.020    -    
lumi_corr16_17_18   lnN    1.009    1.009    1.009    -    
lumi_corr17_18   lnN    1.006    1.006    1.006    -    
elveto_2017   lnN    1.02324     1.01278    -    -    
muveto_2017   lnN    1.00272     1.00081    -    -    
prefire_2017   lnN    1.00462     1.00382    -    -    
cross_section   lnN   -   1.26    -    -    
data-pred_norm_disagreement_RT2BTALL_2017    lnN        -         -         -      1.179
data_pred_shape_disagreement_RT2BTALL_2017  shape?     -    -    -    1.0  

---
## shape uncertainties

jes_2017   shape     1.0   1.0   1.0    - 
jer_2017   shape     1.0   1.0   1.0    - 
pileup_2017   shape     1.0   1.0   1.0    - 
trigger_2017   shape     1.0   1.0   1.0    - 
btagLF  shape     1.0    1.0    1.0    - 
btagHF  shape     1.0    1.0    1.0    - 
btagLFstats1_2017   shape     1.0   1.0   1.0    - 
btagHFstats1_2017   shape     1.0   1.0   1.0    - 
btagLFstats2_2017   shape     1.0   1.0   1.0    - 
btagHFstats2_2017   shape     1.0   1.0   1.0    - 
btagCFerr1  shape     1.0    1.0    1.0    - 
btagCFerr2  shape     1.0    1.0    1.0    - 
DeepAK8TopSF_2017   shape     1.0   1.0   1.0    - 
DeepAK8WSF_2017   shape     1.0   1.0   1.0    - 
isr_TTTT  shape     1.0    -    -    -  
isr_TTX  shape     -    1.0    -    -  
fsr_TTTT  shape     1.0    -    -    -  
fsr_TTX  shape     -    1.0    -    -  
pdf  shape     1.0    1.0    -    - 
ME_TTTT  shape     1.0    -    -    -  
ME_TTX  shape     -    1.0    -    -  
ResTopEff_2017   shape     1.0   1.0   1.0    - 
ResTopMiss_2017   shape     1.0   1.0   1.0    - 

---

RT2BTALLhtbin0_2017 autoMCStats 0 1
---

