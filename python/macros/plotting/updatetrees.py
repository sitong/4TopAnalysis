import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import Defs as defs
import RootStuff as rs
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
gStyle.SetPalette(1)

parser = argparse.ArgumentParser(description='bla')
parser.add_argument("-p", "--proc", dest="proc", default='TTTT')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

pd = plotdefs(args.year, 'SR', modbins=False)
mcfile  = pd.mcfiles[args.proc] #TTTT, TTX or minor
mcrf, mct = rs.makeroot(mcfile, pd.tn)
    
outname = mcfile[mcfile.rfind('/')+1:]
outrf = r.TFile.Open(outname, "update")    
outrf.cd()
outtree=mct.CloneTree(0)

# PassTrig = np.full(1, 1.0, dtype=np.float32) #for data
# outtree.Branch('PassTrig',      PassTrig,     'PassTrig/F') #need for data
METFilters = np.full(1, 1.0, dtype=np.float32)
outtree.Branch('METFilters',    METFilters,   'METFilters/F')

TrigSF_nosys = np.full(1, 1.0, dtype=np.float32)
TrigSFUp = np.full(1, 1.0, dtype=np.float32);     TrigSFDown = np.full(1, 1.0, dtype=np.float32)
TrigSF_jerUp = np.full(1, 1.0, dtype=np.float32); TrigSF_jerDown = np.full(1, 1.0, dtype=np.float32)
TrigSF_jesUp = np.full(1, 1.0, dtype=np.float32); TrigSF_jesDown = np.full(1, 1.0, dtype=np.float32)
outtree.Branch('TrigSF_nosys',      TrigSF_nosys,     'TrigSF_nosys/F')
outtree.Branch('TrigSF_jerUp',      TrigSF_jerUp,     'TrigSF_jerUp/F')
outtree.Branch('TrigSF_jerDown',    TrigSF_jerDown,   'TrigSF_jerDown/F')
outtree.Branch('TrigSF_jesUp',      TrigSF_jesUp,     'TrigSF_jesUp/F')
outtree.Branch('TrigSF_jesDown',    TrigSF_jesDown,   'TrigSF_jesDown/F')
outtree.Branch('TrigSF_Up',   TrigSFUp,   'TrigSF_Up/F')
outtree.Branch('TrigSF_Down', TrigSFDown, 'TrigSF_Down/F')

for i, evt in enumerate(mct):
    #trig=False
    met=False

    if args.year=='2016':
        TrigSF_nosys[0] = evt.trigSF_nosys
        TrigSFUp[0]    = evt.trigSF_Up
        TrigSFDown[0]  = evt.trigSF_Down
        TrigSF_jesUp[0]   = evt.trig_jesUp
        TrigSF_jesDown[0] = evt.trig_jesDown
        TrigSF_jerUp[0]   = evt.trig_jerUp
        TrigSF_jerDown[0] = evt.trig_jerDown

        # t = [evt.trigHLT_PFHT400_SixJet30_DoubleBTagCSV_p056, 
        #      evt.trigHLT_PFHT450_SixJet40_BTagCSV_p056]
        # if any(t)==1:
        #     trig=True

        m = [evt.goodverticesflag,
             evt.haloflag,
             evt.HBHEflag,
             evt.HBHEisoflag,
             evt.ecaldeadcellflag,
             evt.badmuonflag,
             evt.eeBadScFilterflag]
        if all(m):
            met=True

    elif args.year=='2017':
        TrigSF_nosys[0] = evt.fixtrigSF_nosys
        TrigSFUp[0]    = evt.fixtrigSF_Up
        TrigSFDown[0]  = evt.fixtrigSF_Down
        TrigSF_jesUp[0]   = evt.fixtrig_jesUp
        TrigSF_jesDown[0] = evt.fixtrig_jesDown
        TrigSF_jerUp[0]   = evt.fixtrig_jerUp
        TrigSF_jerDown[0] = evt.fixtrig_jerDown
        
        # t1=evt.trigHLT_PFHT430_SixJet40_BTagCSV_p080
        # t2=evt.trigHLT_PFHT430_SixPFJet40_PFBTagCSV_1p5
        # t3=evt.trigHLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2
        # t4=evt.trigHLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2

        m = [evt.goodverticesflag,
             evt.haloflag,
             evt.HBHEflag,
             evt.HBHEisoflag,
             evt.ecaldeadcellflag,
             evt.badmuonflag]
        if all(m):
            met=True

    elif args.year=='2018':
        TrigSF_nosys[0] = evt.trigSF_nosys
        TrigSFUp[0]    = evt.trigSF_Up
        TrigSFDown[0]  = evt.trigSF_Down
        TrigSF_jesUp[0]   = evt.trig_jesUp
        TrigSF_jesDown[0] = evt.trig_jesDown
        TrigSF_jerUp[0]   = evt.trig_jerUp
        TrigSF_jerDown[0] = evt.trig_jerDown

        m = [evt.goodverticesflag,
             evt.haloflag,
             evt.HBHEflag,
             evt.HBHEisoflag,
             evt.ecaldeadcellflag,
             evt.badmuonflag]
        if all(m):
            met=True

    METFilters[0]=met

    outtree.Fill()

outtree.Write("", r.TFile.kOverwrite)
outtree.ResetBranchAddresses()

outrf.Close()
mcrf.Close()
print 'wrote', outrf

