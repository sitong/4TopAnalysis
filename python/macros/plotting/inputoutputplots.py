#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
# import sys
# from VRerrtempl import VRerrtempl
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-b", "--base", dest="base", default='VR') #SR, VR, or TT
parser.add_argument("-c", "--cr", dest="cr", default='D') #CR
parser.add_argument("-l", "--logy", dest="logy", default='True')
parser.add_argument("-m", "--modbins", dest="modbins", default='False')
parser.add_argument("-t", "--ttbb", dest="usettbb", default='False')
parser.add_argument("-s", "--shape", dest="shape", default='True')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

logy=True; modbins=True; useshape=True; usettbb=True
if args.logy=='False':
    logy=False
if args.usettbb=='False':
    usettbb=False
if args.modbins=='False' :#and args.base!='VR':
    modbins=False
if args.shape=='False':
    useshape=False

year = args.year
base = args.base
cr = args.cr
var = "catBDTdisc_nosys" 

def inputoutputplots(args):
    pd = plotdefs(year, base, cr, modbins, usettbb)
    nbins, selrange = pd.getHistSettings(var)

    for chanid, chansel in pd.binsels.iteritems():
        histname = year+'_'+base+'_'+chanid+'_'+var
        sels = pd.getsels(chanid, cr)

        predhist = pd.newhistfromtree(chanid)
        ttbbsel=sels['mcsel']
        # ttbbsel = sels['mcsel'].replace("nfunky_leptons==0 &&","nfunky_leptons==0 && isttbb==1 &&")
        print 'ttbbsel:', ttbbsel, '\n'
        mchist = pd.gethist(pd.mcfiles['ttbb'], var, ttbbsel)
        yld, unc = pd.getyield(mchist)
        ABCDyld = pd.extDDyields[chanid][0]
        ABCDunc = pd.extDDyields[chanid][1]
        ABCD_relunc = ABCDunc/ABCDyld
        mchist.Scale(ABCDyld/yld) #scale histogram
        for ibin in range(mchist.GetNbinsX()+2):
            stat_i = mchist.GetBinError(ibin) #statistics of that bin
            histval_i = mchist.GetBinContent(ibin) #value of that bin
            err = np.sqrt( (stat_i)**2 + (ABCD_relunc*histval_i)**2 )
            
        c = r.TCanvas("c","c",800,800)
        legend = r.TLegend(0.1, 0.75, 0.3, 0.9)

        outname = './plots/inout/'+histname+'.png'
        mchist.SetLineWidth(3)
        predhist.SetLineWidth(3)
        mchist.SetLineColor(r.kBlue)
        predhist.SetLineColor(r.kRed)
        predhist.Draw("histe")
        mchist.Draw("histesame")

        predhist.GetYaxis().SetTitleSize(20)
        predhist.GetYaxis().SetTitleFont(43)
        predhist.GetYaxis().SetTitleOffset(1.55)
        predhist.GetYaxis().SetTitle('NEntries')
        predhist.GetXaxis().SetTitle('BDT discriminant')
        predhist.GetYaxis().SetLabelFont(43)
        predhist.GetYaxis().SetLabelSize(15)
        predhist.SetTitle(" ")
        legend.AddEntry(mchist, "input", "L")
        legend.AddEntry(predhist, "output", "L")
        legend.Draw()

        gPad.Modified()
        gPad.Update()

        print 'saved histogram: ', outname
        c.SaveAs(outname)

inputoutputplots(args)
