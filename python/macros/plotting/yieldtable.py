#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
from collections import OrderedDict
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='yield table')
parser.add_argument("-y", "--year", dest="year", default='2016')
#parser.add_argument("-t", "--ttbb", dest="ttbb", default='False')
args = parser.parse_args()
args.ttbb ='True'
SRsels = [#('SR BASE','nrestops_nosys>=1 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
          ('SR RT1BT0 HTbin0','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700 && ht_nosys<800'),
          ('SR RT1BT0 HTbin1','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=800 && ht_nosys<900'),
          ('SR RT1BT0 HTbin2','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=900 && ht_nosys<1000'),
          ('SR RT1BT0 HTbin3','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=1000 && ht_nosys<1100'),
          ('SR RT1BT0 HTbin4','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=1100 && ht_nosys<1200'),
          ('SR RT1BT0 HTbin5','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=1200 && ht_nosys<1300'),
          ('SR RT1BT0 HTbin6','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=1300 && ht_nosys<1500'),
          ('SR RT1BT0 HTbin7','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=1500'),
          ('SR RT1BT1 HTbin0','nrestops_nosys==1 && nbstops_nosys>=1 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700 && ht_nosys<1400'),
          # ('SR RT1BT1 HTbin1','nrestops_nosys==1 && nbstops_nosys>=1 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=1400'),
          # ('SR RT2BTALL HTbin0','nrestops_nosys>=2 && nbstops_nosys>=0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700 && ht_nosys<1100'),
          # ('SR RT2BTALL HTbin1','nrestops_nosys>=2 && nbstops_nosys>=0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=1100')
]

CRsels = [('D (SR)','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
          ('B','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys==2 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
          ('A','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys==2 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'),
          ('C','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'),
          ('X','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys==2 && njets_nosys==7 && nfunky_leptons==0 && ht_nosys>=700'),
          ('Y','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==7 && nfunky_leptons==0 && ht_nosys>=700'),
]

VRsels = [#('SR BASE','nrestops_nosys>=1 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
          ('SR RT1BT0 HTbin0','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700 && ht_nosys<800'),
          ('SR RT1BT0 HTbin1','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=800 && ht_nosys<900'),
          ('SR RT1BT0 HTbin2','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=900 && ht_nosys<1000'),
          ('SR RT1BT0 HTbin3','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=1000 && ht_nosys<1100'),
          ('SR RT1BT0 HTbin4','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=1100 && ht_nosys<1200'),
          ('SR RT1BT0 HTbin5','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=1200 && ht_nosys<1300'),
          ('SR RT1BT0 HTbin6','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=1300 && ht_nosys<1500'),
          ('SR RT1BT0 HTbin7','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=1500'),
          ('SR RT1BT1 HTbin0','nrestops_nosys==1 && nbstops_nosys>=1 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700 && ht_nosys<1400'),
          ('SR RT1BT1 HTbin1','nrestops_nosys==1 && nbstops_nosys>=1 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=1400'),
          ('SR RT2BTALL HTbin0','nrestops_nosys>=2 && nbstops_nosys>=0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700 && ht_nosys<1100'),
          ('SR RT2BTALL HTbin1','nrestops_nosys>=2 && nbstops_nosys>=0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=1100')]

mHTsels = [('SR RT1BT0','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
           ('SR RT1BT1','nrestops_nosys==1 && nbstops_nosys>=1 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
           ('SR RT2BTALL','nrestops_nosys>=2 && nbstops_nosys>=0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
           ('VR RT1BT0','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'),
           ('VR RT1BT1','nrestops_nosys==1 && nbstops_nosys>=1 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'),
           ('VR RT2BTALL','nrestops_nosys>=2 && nbstops_nosys>=0 && nbjets_nosys>=3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700')]

SLsels = [
    ('$N_{tops}=0,N_{bJets}=2,N_{Jets}=8 (6)$', 'nrestops_nosys==0 && nbjets_nosys==2 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'), #
    ('$N_{tops}=0,N_{bJets}=2,N_{Jets}=9 (7)$', 'nrestops_nosys==0 && nbjets_nosys==2 && njets_nosys==9 && nfunky_leptons==0 && ht_nosys>=700'), #
    ('$N_{tops}=0,N_{bJets}=2,N_{Jets}=10 (8)$', 'nrestops_nosys==0 && nbjets_nosys==2 && njets_nosys==10 && nfunky_leptons==0 && ht_nosys>=700'), #
    ('$N_{tops}=0,N_{bJets}=2,N_{Jets}=11 (9)$', 'nrestops_nosys==0 && nbjets_nosys==2 && njets_nosys==11 && nfunky_leptons==0 && ht_nosys>=700'), #
    ('$N_{tops}=0,N_{bJets}=2,N_{Jets}\\geq12 (10)$', 'nrestops_nosys==0 && nbjets_nosys==2 && njets_nosys>=12 && nfunky_leptons==0 && ht_nosys>=700'), #

    ('$N_{tops}=0,N_{bJets}=3,N_{Jets}=8 (6)$', 'nrestops_nosys==0 && nbjets_nosys==3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}=0,N_{bJets}=3,N_{Jets}=9 (7)$', 'nrestops_nosys==0 && nbjets_nosys==3 && njets_nosys==9 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}=0,N_{bJets}=3,N_{Jets}=10 (8)$', 'nrestops_nosys==0 && nbjets_nosys==3 && njets_nosys==10 && nfunky_leptons==0 && ht_nosys>=700'),
    ('$N_{tops}=0,N_{bJets}=3,N_{Jets}=11 (9)$', 'nrestops_nosys==0 && nbjets_nosys==3 && njets_nosys==11 && nfunky_leptons==0 && ht_nosys>=700'),
    ('$N_{tops}=0,N_{bJets}=3,N_{Jets}\\geq12 (10)$', 'nrestops_nosys==0 && nbjets_nosys==3 && njets_nosys>=12 && nfunky_leptons==0 && ht_nosys>=700'),

    ('$N_{tops}=0,N_{bJets}\\geq4,N_{Jets}=8 (6)$', 'nrestops_nosys==0 && nbjets_nosys>=4 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}=0,N_{bJets}\\geq4,N_{Jets}=9 (7)$', 'nrestops_nosys==0 && nbjets_nosys>=4 && njets_nosys==9 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}=0,N_{bJets}\\geq4,N_{Jets}=10 (8)$', 'nrestops_nosys==0 && nbjets_nosys>=4 && njets_nosys==10 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}=0,N_{bJets}\\geq4,N_{Jets}=11 (9)$', 'nrestops_nosys==0 && nbjets_nosys>=4 && njets_nosys==11 && nfunky_leptons==0 && ht_nosys>=700'),
    ('$N_{tops}=0,N_{bJets}\\geq4,N_{Jets}\\geq12 (10)$', 'nrestops_nosys==0 && nbjets_nosys>=4 && njets_nosys>=12 && nfunky_leptons==0 && ht_nosys>=700'),

    ('$N_{tops}\geq1,N_{bJets}=2,N_{Jets}=8 (6)$', 'nrestops_nosys>=1 && nbjets_nosys==2 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'), # 
    ('$N_{tops}\geq1,N_{bJets}=2,N_{Jets}=9 (7)$', 'nrestops_nosys>=1 && nbjets_nosys==2 && njets_nosys==9 && nfunky_leptons==0 && ht_nosys>=700'), #
    ('$N_{tops}\geq1,N_{bJets}=2,N_{Jets}=10 (8)$', 'nrestops_nosys>=1 && nbjets_nosys==2 && njets_nosys==10 && nfunky_leptons==0 && ht_nosys>=700'), # 
    ('$N_{tops}\geq1,N_{bJets}=2,N_{Jets}=11 (9)$', 'nrestops_nosys>=1 && nbjets_nosys==2 && njets_nosys==11 && nfunky_leptons==0 && ht_nosys>=700'), #
    ('$N_{tops}\geq1,N_{bJets}=2,N_{Jets}\\geq12 (10)$', 'nrestops_nosys>=1 && nbjets_nosys==2 && njets_nosys>=12 && nfunky_leptons==0 && ht_nosys>=700'), #

    ('$N_{tops}\geq1,N_{bJets}=3,N_{Jets}=8 (6)$', 'nrestops_nosys>=1 && nbjets_nosys==3 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}\geq1,N_{bJets}=3,N_{Jets}=9 (7)$', 'nrestops_nosys>=1 && nbjets_nosys==3 && njets_nosys==9 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}\geq1,N_{bJets}=3,N_{Jets}=10 (8)$', 'nrestops_nosys>=1 && nbjets_nosys==3 && njets_nosys==10 && nfunky_leptons==0 && ht_nosys>=700'),
    ('$N_{tops}\geq1,N_{bJets}=3,N_{Jets}=11 (9)$', 'nrestops_nosys>=1 && nbjets_nosys==3 && njets_nosys==11 && nfunky_leptons==0 && ht_nosys>=700'),
    ('$N_{tops}\geq1,N_{bJets}=3,N_{Jets}\\geq12 (10)$', 'nrestops_nosys>=1 && nbjets_nosys==3 && njets_nosys>=12 && nfunky_leptons==0 && ht_nosys>=700'),

    ('$N_{tops}\geq1,N_{bJets}\\geq4,N_{Jets}=8 (6)$', 'nrestops_nosys>=1 && nbjets_nosys>=4 && njets_nosys==8 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}\geq1,N_{bJets}\\geq4,N_{Jets}=9 (7)$', 'nrestops_nosys>=1 && nbjets_nosys>=4 && njets_nosys==9 && nfunky_leptons==0 && ht_nosys>=700'), 
    ('$N_{tops}\geq1,N_{bJets}\\geq4,N_{Jets}=10 (8)$', 'nrestops_nosys>=1 && nbjets_nosys>=4 && njets_nosys==10 && nfunky_leptons==0 && ht_nosys>=700'),
    ('$N_{tops}\geq1,N_{bJets}\\geq4,N_{Jets}=11 (9)$', 'nrestops_nosys>=1 && nbjets_nosys>=4 && njets_nosys==11 && nfunky_leptons==0 && ht_nosys>=700'),
    ('$N_{tops}\geq1,N_{bJets}\\geq4,N_{Jets}\\geq12 (10)$', 'nrestops_nosys>=1 && nbjets_nosys>=4 && njets_nosys>=12 && nfunky_leptons==0 && ht_nosys>=700')]


class yieldtable:
    def __init__(self, args, seldict):
        self.year = args.year
        self.pd = plotdefs(self.year, 'SR', False)
        self.seldict = OrderedDict(seldict)
        self.var='catBDTdisc_nosys'
        self.treename = 'Events'
        self.procs = ['data','tttt' , 'ttX', 'other', "DD QCD+ttbar"]
        if args.ttbb=='True':
            self.procs = ['data','tttt' , 'ttbb', 'ttX', 'other', "DD QCD+ttbar"]

        self.goodsels = ['SR RT1BT0', 'SR RT1BT1', 'SR RT2BTALL', 'VR RT1BT0', 'VR RT1BT1', 'VR RT2BTALL']

    def table_header(self):
        header='Channel & '
        for proc in self.procs[:-1]:
            header += (proc+' & ')
        header += (self.procs[-1]+' \\\ ')
        print header 
        print '\\hline' 

    def start_table(self):
        print '\\begin{table}[!h]'
        print '\centering'
        nls = '|'+('l|'*(len(self.procs)+1))
        print "\\begin{tabular}{"+nls+"}"

    def end_table(self):
        print '\\hline' 
        print '\end{tabular}'
        print '\caption{ Yields for $N(leptons)=0$, $HT\geq700$ & given selections for year '+self.year+'}'
        print '\end{table}'

    def print_table(self):
        print 'making table...\n'
        self.start_table()
        self.table_header()
        for label, binsel in self.seldict.iteritems():
            calcDD=True
            # calcDD=False
            # if label in self.goodsels:
            #     calcDD=True
            #get the yields
            cache = self.find_yields(binsel, calcDD)
            self.get_line(label, binsel, cache)
        self.end_table()
        print '\n'
            
    def get_line(self, label, binsel, yields):
        line = label+' & '
        nprocs = len(self.procs)-1
        for i, proc in enumerate(self.procs):
            line += ('$'+str(round(yields[proc], 2)) +'\pm'+str(round(yields[proc+'_unc'], 2))+'$')
            if i!=nprocs:
                line+=' & '
            else:
                line+=' \\\ '
        print line

    def find_yields(self, binsel, calcDD=True):
        cutstr=''
        yields={}
        for proc in self.procs:
            if proc=='data': #get yield from hist
                cutstr = '('+binsel+self.pd.dstr[self.year]+')'
                yields[proc], yields[proc+'_unc'] = self.pd.getyield( self.pd.gethist(self.pd.datafilename, self.var, cutstr))
                # print cutstr
            elif proc=="DD QCD+ttbar": #get yield from ABCD formula
                # if calcDD: #if it can be calculated...
                #     cutstr = '('+binsel+self.pd.dstr[self.year]+')'
                #     CRs = self.getCRs(cutstr) #dictionary of selections
                #     ABCD = self.abcdyields(CRs)
                #     yields[proc]= ABCD['D']; yields[proc+'_unc']= ABCD['D_unc']
                yields[proc] = yields['data']-(yields['tttt']+yields['other']+yields['ttX'])
                yields[proc+'_unc'] = self.addunc(["data", 'ttX', 'other', 'tttt'], yields)
                if args.ttbb:
                    yields[proc] = yields['data']-(yields['tttt']+yields['other']+yields['ttX']+yields['ttbb'])
                    yields[proc+'_unc'] = self.addunc(["data", 'ttX', 'other', 'tttt', 'ttbb'], yields)

                # else:
                #     yields[proc]= 0.0; yields[proc+'_unc']= 0.0

            elif proc=='tttt' or proc =='ttbb' or proc=='ttX'or proc=='ttH'or proc=='ttW'or proc=='ttZ'or proc=='ttOther' or proc=='other': #get yield from hist
                # cutstr = '('+binsel+')'
                cutstr = '('+binsel+self.pd.mcstr[self.year]
                if proc=='ttbb':
                    cutstr = cutstr.replace('nfunky_leptons==0 &&', 'nfunky_leptons==0 && isttbb==1 &&') #just ttbb

                # print cutstr
                yields[proc], yields[proc+'_unc'] = self.pd.getyield( self.pd.gethist(self.pd.mcfiles[proc], self.var, cutstr))

            elif proc == 'Total B': #calculate
                if calcDD:
                    yields[proc] = yields["DD QCD+ttbar"]+yields['ttX']+yields['other']
                    yields[proc+'_unc'] = self.addunc(["DD QCD+ttbar", 'ttX', 'other'], yields)
                    if args.ttbb=='True':
                        yields[proc] = yields["DD QCD+ttbar"]+yields['ttX']+yields['other']+yields['ttbb']
                        yields[proc+'_unc'] = self.addunc(["DD QCD+ttbar", 'ttX', 'other', 'ttbb'], yields)
                        
                    # yields[proc] = yields["QCD"]+yields["TT"]+yields['ttX']+yields['other']
                    # yields[proc+'_unc'] = self.addunc(["QCD", "TT", 'ttX', 'other'], yields)
                    # yields[proc] = yields["QCD+ttbar"]+yields['ttH']+yields['ttW']+yields['ttZ']+yields['ttOther']+yields['other']
                    # yields[proc+'_unc'] = self.addunc(["QCD+ttbar", 'ttH','ttW','ttZ','ttOther', 'other'], yields)
                else:
                    yields[proc]= 0.0; yields[proc+'_unc']= 0.0

            elif proc == 'data/pred':
                if calcDD:
                    yields['pred'] = yields['tttt']+yields['Total B']
                    yields['pred_unc'] = self.addunc(['Total B', 'tttt'], yields)
                    yields[proc] = yields['data']/yields['pred']
                    yields[proc+'_unc'] = self.multunc(proc, ['data', 'pred'], yields)
                else:
                    yields[proc]= 0.0; yields[proc+'_unc']= 0.0

        # print yields, '\n'
        return yields
            
    def multunc(self, target, calclist, cache):
        #if X=A*B, Xunc = X*sqrt((Aunc/A)**2 + (Bunc/B)**2)
        unc = 0.0
        unclist = [(cache[p+'_unc']/cache[p])**2 for p in calclist]
        unc = cache[target]*np.sqrt(sum(unclist))
        return unc
        
    def addunc(self, calclist, cache):
        #if X=A+B, Xunc = sqrt(Aunc**2 + Bunc**2)
        unc=0.0
        unclist = [(cache[p+'_unc'])**2 for p in calclist]
        unc = np.sqrt(sum(unclist))
        return unc

    def abcdyields(self, CRs, extABCD=True, verbose=False):

        CRylds = {}
        if verbose:
            print 'finding ABCD yields. extABCD?', extABCD

        for CR, sel in CRs.iteritems(): #yields for A,B,C,D,X,Y
            CRylds[CR], CRylds[CR+'_unc'] = self.pd.getyield( self.pd.gethist(self.pd.datafilename, self.var, sel))
            
        if extABCD:
            if CRylds['A']<=0:
                CRylds['D'] = 0.0; CRylds['D_unc']=0.0
            else:
                CRylds['D'] = (CRylds['X']/(CRylds['B']*CRylds['Y']))*(((CRylds['B']*CRylds['C'])/CRylds['A'])**2)
                calcunc=(2.0*((CRylds['B_unc']/CRylds['B'])**2+ (CRylds['C_unc']/CRylds['C'])**2+ (CRylds['A_unc']/CRylds['A'])**2) + (CRylds['B_unc']/CRylds['B'])**2+ (CRylds['Y_unc']/CRylds['Y'])**2+ (CRylds['X_unc']/CRylds['X'])**2)
                CRylds['D_unc'] = CRylds['D']*np.sqrt(calcunc)

        elif not extABCD:
            CRylds['D'] = ((CRylds['B']*CRylds['C'])/CRylds['A'])
            CRylds['D_unc'] = CRylds['D']*np.sqrt( (CRylds['B_unc']/CRylds['B'])**2 + (CRylds['C_unc']/CRylds['C'])**2 + (CRylds['A_unc']/CRylds['A'])**2)
        
        if verbose:
            print CRylds

        return CRylds


    def getCRs(self, binsel, verbose=False):
        initsel=binsel
        
        if verbose:
            print 'Getting ABCD selections for the following cutstring\n'
            print binsel, '\n'
        
        nb_idx = binsel.find('nbjets_nosys')+12 #the index of the character AFTER the njets/nbjets string
        nj_idx = binsel.find('njets_nosys')+11

        bspc = binsel[nb_idx:].find(' ')+nb_idx
        jspc = binsel[nj_idx:].find(' ')+nj_idx
        #print 'num',binsel[nj_idx+2:jspc], 
        Nb = int(binsel[nb_idx+2:bspc])#the number
        Nj = int(binsel[nj_idx+2:jspc]) 

        #handling double digits:
        if (jspc-nj_idx-2)>1:
            initsel = self.replacer(binsel, jspc-1)
        if (bspc-nb_idx-2)>1:
            initsel = self.replacer(binsel, bspc-1)
        
        #if True need to change > to = in some cases, if False it's already =
        nb_sn = True if binsel[nb_idx]=='>' else False
        nj_sn = True if binsel[nj_idx]=='>' else False

        CRs = {'A':initsel, 'B':initsel, 'C':initsel, 'X':initsel, 'Y':initsel} #initialize with the original cutstring 

        #ok, now go through the options
        for CR, CRsel in CRs.iteritems():

            njchange=False; nbchange=False 
            nb=Nb; nj=Nj #reinitialize

            if verbose:
                print 'CR', CR, 'old:', CRsel[nb_idx-13:nb_idx+4], CRsel[nj_idx-13:nj_idx+4]

            if CR=='A':
                nj-=1; nb-=1
                njchange=True; nbchange=True

            elif CR=='B':
                nb-=1
                nbchange=True
                
            elif CR=='C':
                nj-=1
                njchange=True

            elif CR=='X':
                nj-=2; nb-=1
                njchange=True; nbchange=True

            elif CR=='Y':
                nj-=2
                njchange=True

            #replace the number
            # CRsel[nb_idx+3] = str(nb)
            CRsel = self.replacer(CRsel, nb_idx+2, str(nb))
            # CRsel[nj_idx+3] = str(nj)
            CRsel = self.replacer(CRsel, nj_idx+2, str(nj))

            #replace >= with == if the number changed
            if njchange and nj_sn:
                # CRsel[nj_idx+1] = '='
                CRsel = self.replacer(CRsel, nj_idx, '=')
            if nbchange and nb_sn:
                # CRsel[nb_idx+1] = '='
                CRsel = self.replacer(CRsel, nb_idx, '=')

            if verbose:
                print 'CR', CR, 'new:', CRsel[nb_idx-13:nb_idx+4], CRsel[nj_idx-13:nj_idx+4]
            CRs[CR]=CRsel

        return CRs

    #for replacing character at index
    #https://stackoverflow.com/questions/41752946/replacing-a-character-from-a-certain-index/41753038
    def replacer(self, s, index, newchar=None, nofail=False):
        # raise an error if index is outside of the string
        if not nofail and index not in range(len(s)):
            raise ValueError("index outside given string")

        if newchar is None:
            return s[:index] + s[index + 1:]

        # if not erroring, but the index is still not in the correct range..
        if index < 0:  # add it to the beginning
            return newchar + s
        if index > len(s):  # add it to the end
            return s + newchar

        # insert the new string between "slices" of the origina
        return s[:index] + newchar + s[index + 1:]

#run the code
# yt = yieldtable(args, CRsels)
# yt.print_table()
