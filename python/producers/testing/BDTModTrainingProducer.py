# produces flat ntuples for resolved top tagger bdt training
#returns info BY TOP CANDIDATE
import os
import numpy as np
import ROOT
import math
from pprint import pprint
import itertools
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
# import PhysicsTools.NanoTrees.helpers.LeptonHelper as lepsel
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
# import PhysicsTools.NanoTrees.helpers.GenPartHelper as GenHelp

from collections import OrderedDict
from PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

from PhysicsTools.NanoTrees.helpers.tmvahelper import TMVAHelper
from PhysicsTools.NanoTrees.helpers.ResMVAHelper import ResMVAHelper
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper

import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')

#class definition
class BDTModTrainingProducer(Module):
    def __init__(self):

#working points and constants:
        self.deepbs          = True
        self.cleanJvLs       = False
        self.verbose         = False
        self.cleanAK4overlap = 0.8 #how much res, bst and ws can overlap
        self.etaMax          = 2.4
        self.bJetEtaMax      = 2.4
        self.minJet_pt       = 35.0
        self.maxBJet_eta      = 2.4
        self.minBJet_pt       = 35.0
        self.defaultCSV = 'M' #M, L, or T
        self.maxJet_eta      = 2.4
        self.TopCand_minpt   = 100.0
        self.minjets         = 3 #minimum number of jets for a topcand
        self.Bimax           = 4 #number of top scoring csv jets to consider
        self.topmassrange    = 80
        self.wmassrange      = 40
        self.topmassparam    = 175.0 #really 172
        self.wmassparam      = 80.0

    def beginJob(self):
        # self.xgb = XGBHelper(self.bdt_file, self.bdt_vars)
        self.tchelp = ResMVAHelper()
        self.genhelp = genparthelper()
        self.jh = JetHelper('deepflav', 0.3039)
        pass

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.isMC = bool(inputTree .GetBranch('genWeight'))
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        self.out.branch("flag_signal", "O")
        self.out.branch("NoCandFlag", "I")
        self.out.branch("NgoodtagRes", "I")
        self.out.branch("NmistagRes", "I")
        self.out.branch("nrestops",   "I")
        self.out.branch("evt",     "I")
        self.out.branch("npv",       "I")
        self.out.branch('njets',     "I")
        self.out.branch('nbjets',    "I")
        self.out.branch('ndeepbjets',    "I")
        self.out.branch('nlbjets',    "I")
        self.out.branch('nldeepbjets',    "I")
        self.out.branch('met',    "F")
        self.out.branch('topcand_pt',    "F")
        self.out.branch('weight',        "F")
        self.out.branch("genweight",     "F")
        self.out.branch("gen_njets",     "I")
        self.out.branch("genjet5flag",   "I")
        self.out.branch("var_b_mass",     "F")
        self.out.branch("var_b_csv",      "F")
        self.out.branch("var_j2_csv",     "F")
        self.out.branch("var_j2_cvsl",    "F")
        self.out.branch("var_j2_ptD",     "F")
        self.out.branch("var_j2_axis1",    "F")
        self.out.branch("var_j3_csv",      "F")
        self.out.branch("var_j3_cvsl",     "F")
        self.out.branch("var_j3_ptD",      "F")
        self.out.branch("var_j3_axis1",    "F")
        self.out.branch("var_topcand_mass",    "F")
        self.out.branch("var_topcand_ptDR",    "F")
        self.out.branch("var_wcand_mass",      "F")
        self.out.branch("var_wcand_ptDR",      "F")
        self.out.branch("var_b_j2_mass",       "F")
        self.out.branch("var_b_j3_mass",       "F")
        self.out.branch("var_sd_n2",           "F")
        self.out.branch("var_j2_mult",         "I")
        self.out.branch("var_j3_mult",         "I")
        self.out.branch("var_b_btagDeepFlavB",  "F")
        self.out.branch("var_j2_btagDeepFlavB", "F")
        self.out.branch("var_j3_btagDeepFlavB", "F")
        self.out.branch("var_b_deepFlavourb",  "F")
        self.out.branch("var_j2_deepFlavourb", "F")
        self.out.branch("var_j3_deepFlavourb", "F")
        self.out.branch("var_b_deepFlavourbb",  "F")
        self.out.branch("var_j2_deepFlavourbb", "F")
        self.out.branch("var_j3_deepFlavourbb", "F")
        self.out.branch("var_b_deepFlavourc",  "F")
        self.out.branch("var_j2_deepFlavourc", "F")
        self.out.branch("var_j3_deepFlavourc", "F")
        self.out.branch("var_b_deepFlavourg",  "F")
        self.out.branch("var_j2_deepFlavourg", "F")
        self.out.branch("var_j3_deepFlavourg", "F")
        self.out.branch("var_b_deepFlavourlepb",  "F")
        self.out.branch("var_j2_deepFlavourlepb", "F")
        self.out.branch("var_j3_deepFlavourlepb", "F")
        self.out.branch("var_b_deepFlavouruds",  "F")
        self.out.branch("var_j2_deepFlavouruds", "F")
        self.out.branch("var_j3_deepFlavouruds", "F")
        self.out.branch("var_j2_nConstituents", "I")
        self.out.branch("var_j3_nConstituents", "I")
        # self.out.branch("var_j2_btagDeepFlavC", "F")
        # self.out.branch("var_j3_btagDeepFlavC", "F")
        # self.out.branch("var_j2_btagDeepC", "F")
        # self.out.branch("var_j3_btagDeepC", "F")
        self.out.branch("var_j2_qgl", "F")
        self.out.branch("var_j3_qgl", "F")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
##selections
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    # check that jets within resolved top cands don't overlap with each other
    def checkResJetOverlap(self, topcand, DeepRes=False):
        if not DeepRes:
            bj=topcand[1]; j2=topcand[2]; j3=topcand[3]
        elif DeepRes:
            bj=topcand.j1Idx; j2=topcand.j2Idx; j3=topcand.j3Idx
        if (bj==j2 or bj==j3 or j2==bj or j2==j3 or j3==bj or j3==j2):
            #print 'overlapping!!'
            return False #IS overlapping
        else:
            return True #not overlapping

    def checkResCandOverlap(self, Selcands, candpts):
        cleancands = []; cleanjets = []
        #order selcands from best (highest) to worst disc values:
        candptis  = np.argsort(candpts)[::-1]
        selcands = [Selcands[i] for i in candptis]
        #ordercandpts = np.sort(candpts)[::-1]

        for i1, c1 in enumerate(selcands):
         #use topcands [tvec, j1, j2, j3] if mva top and jet indices if deepres:
            c1j1=c1[1]; c1j2=c1[2]; c1j3=c1[3]
            C1 = c1
            if i1==0:
                cleancands.append(c1) #keep first/best candidate
                cleanjets.extend((c1j1,c1j2,c1j3))

            for i2, c2  in enumerate(selcands):
                c2j1=c2[1]; c2j2=c2[2]; c2j3=c2[3]

                if i2>i1:
                    #check for full overlap:
                    if (c2j1 in C1 and c2j2 in C1 and c2j3 in C1):
                        if self.verbose: print 'same jets!'
                    else:
                        if ((c2j1 not in C1) and (c2j2 not in C1) and (c2j3 not in C1)):
                            if ((c2j1 not in cleanjets) and (c2j2 not in cleanjets) and (c2j3 not in cleanjets)):
                                cleancands.append(c2) #c2 not overlapping!
                                cleanjets.extend((c2j1,c2j2,c2j3))
                            else: 
                                if self.verbose: print 'overlaps with cleanjets!'
                        else:
                            if self.verbose: print 'overlapping jets!'
        return np.array(cleancands)
        
    # discriminant threshold for resolved mva
    def SelMVAResTops(self, top4vec):
        if(top4vec.Eta()>self.etaMax):
            return False
        if(top4vec.Pt()<self.TopCand_minpt):
            return False
        return True #pass

##getters & fillers
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    #fills into branch
    def _get_filler(self, obj, fillval=0):
        def filler(branch, value, default=fillval):
            self.out.fillBranch(branch, value if obj else default)
        return filler

    # takes collection of ak4jets and return a collection of resolved top candidates
    def _getrescands(self, event, lepcleanjets):
        havecands = False; topcands = []; bestcsv = []
        if len(lepcleanjets)<self.minjets: #require 3 jets for topcand
            return havecands, None
        else:
            for jet in lepcleanjets:
                if not self.deepbs: csv = jet.btagCSVV2
                elif   self.deepbs: csv = jet.btagDeepFlavB
                bestcsv.append(csv)
        bestcsv = np.array(bestcsv)
        csvinds = np.argsort(bestcsv)[::-1]
        orderjets = [lepcleanjets[i] for i in csvinds] #order jets in descending csv score order

        nbcanjets=self.Bimax
        if len(orderjets)<self.Bimax: #make sure not considering more bcands than there are jets
            nbcanjets=len(orderjets)

        if self.verbose: print 'numjets', len(orderjets), 'n bjet cands', len(orderjets[:nbcanjets])
        #get the cands. NO CSV CUT
        for Bi, bcand in enumerate(orderjets[:nbcanjets]): #iterate over first 3 (or whatever) elements in list
            #print bcand.btagCSVV2
            for j2i, j2 in enumerate(orderjets[1:]):
                if j2i!=Bi:
                    for j3i, j3 in enumerate(orderjets[2:]):
                        if (j3i!=Bi and j3i!=j2i): #no overlapping jets
                            tcand = self.tchelp.getTopCand(bcand, j2, j3)
                            wcand = self.tchelp.getWCand(j2, j3)
                            passmass = self.tchelp.PassMass(tcand[0], wcand[0], self.topmassrange, self.wmassrange, self.topmassparam, self.wmassparam)
                            if passmass:
                                havecands = True  #true if there are ANY tops in the event
                                # print 'topcand----->', Bi, bcand, j2i, j2, j3i, j3
                                topcands.append(tcand)
        if havecands:
            return havecands, np.array(topcands)
        elif not havecands:
            return havecands, None #empty

    # fills variables needed to calculate bdt discriminant for restop mva
    def _checkCand(self, havecands, topcand): #should only calculate bdt if >1 topcand
        candPT = topcand[0].Pt()

        #Find good top candidates        
        goodcand = False
        if havecands:
            iscand = self.SelMVAResTops(topcand[0])
            isoverlap = self.checkResJetOverlap(topcand)
            if (iscand and isoverlap): #if passes selection and no overlap
                goodcand = True
        #be sure will fill vars if goodcand, else fill zero:
        return goodcand, topcand, candPT
            
    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # put it all together!
    def analyze(self, event):
        e = int(event.event)
        # print 'EVENT #', e, '\n'

        XWeight = xw.GetKnownWeight(self.infile, '2016', self.isMC) #xsec weight from filename
        if event.genWeight < 0: # get the right sign
            XWeight = -XWeight

        #variables for genwjet5:
        genjets       = Collection(event,"GenJet")
        event.met     = Object(event,"MET")
        gj5bool = False
        ngoodgjs = 0
        if event.nGenJet >=5:
            for gj in genjets:
                if (gj.pt>20 and abs(gj.eta<2.4)):
                    ngoodgjs+=1
        if ngoodgjs>=5:
            gj5bool = True

        #define+clean jets:
        # leptons    = lepsel.SelectLeptons(event, conc=True) #define leptons
        ak4jets    = Collection(event,"Jet")
 
        #add indices to ak4jets:
        ak4jets = self.genhelp.AddJetIdx(ak4jets)
        cleanak4 = [j for j in ak4jets if self.jh.cleanJets(j, minJet_pt=35.0, maxJet_eta=2.4, usejetid=True)]
        # if self.cleanJvLs: #clean jets v leptons
        #     cleanak4 = self.jh.cleanJetsvLeps(cleanak4, leptons, jetrad=0.4)

        #bjets
        BJets = [j for j in cleanak4 if self.jh.customBJets(j, 'csvv2', 0.8484, self.minBJet_pt, self.maxBJet_eta)]
        LBJets = [j for j in cleanak4 if self.jh.customBJets(j, 'csvv2', 0.5426, self.minBJet_pt, self.maxBJet_eta)]
        DeepBJets = [j for j in cleanak4 if self.jh.customBJets(j, 'deepflav', 0.3039, self.minBJet_pt, self.maxBJet_eta)]
        LDeepBJets = [j for j in cleanak4 if self.jh.customBJets(j, 'deepflav', 0.0614, self.minBJet_pt, self.maxBJet_eta)]
        nbjets = len(BJets) 
        nlbjets = len(LBJets)
        ndeepbjets = len(DeepBJets) 
        nldeepbjets = len(LDeepBJets)
        njets = len(cleanak4)

        #get MVA resolved top candidates 
        ResTops = []; candPTs = []#; rtdicts = []
        HaveCands, TopCands  = self._getrescands(event, cleanak4)
        # if not HaveCands:
        #     self._fillBDTvars(HaveCands, None)
        if HaveCands:
            for tc in TopCands: #looping over topcands
                isgood, GoodCand, candPT = self._checkCand(HaveCands,tc)
                #print 'disc:', disc
                if isgood:
                    ResTops.append(GoodCand); candPTs.append(candPT)

        nocand = 0
        #check that resolved tops are distinct!:
        ResTops = np.array(ResTops); candPTs = np.array(candPTs)
        if len(ResTops)>0:
            ResTops = self.checkResCandOverlap(ResTops, candPTs)
        else: nocand = 1

        # check gen matching of tops
        NgoodtagRes = 0; NmistagRes = 0
        matcharr = np.zeros(len(ResTops), dtype=int)
        for i, (tc, m) in enumerate(zip(ResTops, matcharr)):
            matched = self.genhelp.GenMatchTops(event, tc, True)
            if matched:
                NgoodtagRes+=1
                matcharr[i]=1 #reassign 0 to 1: candidate is a gen-matched top
            else: NmistagRes+=1

        #fill variables
        bcsv = 0.; j2csv = 0.; j3csv = 0.; wcand_deltaR=0; b_wcand_deltatR = 0; sd_0 = 0; var_sd_n2 =0

        for tc, m in zip(ResTops, matcharr):
            if HaveCands:
                wcand = self.tchelp.getWCand(tc[2], tc[3])
                bcsv = tc[1].btagDeepFlavB
                j2csv = tc[2].btagDeepFlavB
                j3csv = tc[3].btagDeepFlavB

                wcand_deltaR = phys.deltaR(wcand[1].eta, wcand[1].phi, wcand[2].eta, wcand[2].phi) #deltaR(j2, j3)
                b_wcand_deltaR = phys.deltaR(tc[1].eta, tc[1].phi, wcand[0].Eta(), wcand[0].Phi()) #deltaR(b, wcand)
                sd_0 = tc[3].pt/(tc[2].pt+tc[3].pt) #j3pt/j2pt+j3pt
                if wcand_deltaR != 0:
                    var_sd_n2 = sd_0/((wcand_deltaR)**-2)

            # self.out.fillBranch('flag_signal',  m)
            # self.out.fillBranch("evt",          int(event.event))
            # self.out.fillBranch("npv",          event.PV_npvs)
            # self.out.fillBranch("met",          event.met.pt) 
            # self.out.fillBranch('njets',        njets)
            # self.out.fillBranch('nbjets',       nbjets)
            # self.out.fillBranch('nlbjets',      nlbjets)
            # self.out.fillBranch('ndeepbjets',   ndeepbjets)
            # self.out.fillBranch('nldeepbjets',  nldeepbjets)
            # self.out.fillBranch('topcand_pt',   tc[0].Pt() )
            # self.out.fillBranch('weight',       XWeight) 
            # self.out.fillBranch('genweight',    event.genWeight)
            # self.out.fillBranch("gen_njets",    event.nGenJet)
            # self.out.fillBranch("genjet5flag",  int(gj5bool))
            # self.out.fillBranch("var_b_mass",  tc[1].mass)
            # self.out.fillBranch("var_b_btagDeepFlavB",  tc[1].btagDeepFlavB)
            # self.out.fillBranch("var_j2_btagDeepFlavB",  tc[2].btagDeepFlavB)
            # self.out.fillBranch("var_j3_btagDeepFlavB",  tc[3].btagDeepFlavB)
            # self.out.fillBranch("var_j2_btagDeepFlavC",  tc[2].btagDeepFlavC)
            # self.out.fillBranch("var_j3_btagDeepFlavC",  tc[3].btagDeepFlavC)
            # self.out.fillBranch("var_topcand_mass", tc[0].M())
            # self.out.fillBranch("var_topcand_ptDR", tc[0].Pt()*b_wcand_deltaR )#fixed, *b_wcand_deltaR
            # self.out.fillBranch("var_wcand_mass",   wcand[0].M())
            # self.out.fillBranch("var_wcand_ptDR",   wcand[0].Pt()*wcand_deltaR)#fixed, *wcandpt
            # self.out.fillBranch("var_b_j2_mass",    (self.tchelp.p4(tc[1]) + self.tchelp.p4(tc[2]) ).M() )
            # self.out.fillBranch("var_b_j3_mass",    (self.tchelp.p4(tc[1]) + self.tchelp.p4(tc[3]) ).M() )
            # self.out.fillBranch("var_sd_n2",        var_sd_n2)
            # self.out.fillBranch("var_j2_nConstituents",      tc[2].nConstituents)#corrected
            # self.out.fillBranch("var_j3_nConstituents",      tc[3].nConstituents)
            # self.out.fillBranch("var_j2_qgl", tc[2].qgl) #
            # self.out.fillBranch("var_j3_qgl", tc[3].qgl) #
            # self.out.fillBranch("nrestops",        len(ResTops))
            # self.out.fillBranch("NgoodtagRes",     NgoodtagRes)
            # self.out.fillBranch("NmistagRes",      NmistagRes)
            # self.out.fillBranch("NoCandFlag",      nocand) #no tops found
            self.out.fillBranch('flag_signal',  m)
            self.out.fillBranch("evt",          int(event.event))
            self.out.fillBranch("npv",          event.PV_npvs)
            self.out.fillBranch("met",          event.met.pt) 
            self.out.fillBranch('njets',        njets)
            self.out.fillBranch('nbjets',       nbjets)
            self.out.fillBranch('nlbjets',      nlbjets)
            self.out.fillBranch('ndeepbjets',   ndeepbjets)
            self.out.fillBranch('nldeepbjets',  nldeepbjets)
            self.out.fillBranch('topcand_pt',   tc[0].Pt() )
            self.out.fillBranch('weight',       XWeight) 
            self.out.fillBranch('genweight',    event.genWeight)
            self.out.fillBranch("gen_njets",    event.nGenJet)
            self.out.fillBranch("genjet5flag",  int(gj5bool))
            self.out.fillBranch("var_b_mass",  tc[1].mass)
            self.out.fillBranch("var_b_csv",   tc[1].btagCSVV2)
            self.out.fillBranch("var_j2_csv",  tc[2].btagCSVV2)
            self.out.fillBranch("var_j2_cvsl", tc[2].CvsL)
            self.out.fillBranch("var_j2_ptD",  tc[2].qgptD)
            self.out.fillBranch("var_j2_axis1",tc[2].qgAxis1)
            self.out.fillBranch("var_j3_csv",  tc[3].btagCSVV2)
            self.out.fillBranch("var_j3_cvsl", tc[3].CvsL)
            self.out.fillBranch("var_j3_ptD",  tc[3].qgptD)
            self.out.fillBranch("var_j3_axis1",tc[3].qgAxis1)
            self.out.fillBranch("var_topcand_mass", tc[0].M())
            self.out.fillBranch("var_topcand_ptDR", tc[0].Pt()*b_wcand_deltaR )#fixed, *b_wcand_deltaR
            self.out.fillBranch("var_wcand_mass",   wcand[0].M())
            self.out.fillBranch("var_wcand_ptDR",   wcand[0].Pt()*wcand_deltaR)#fixed, *wcandpt
            self.out.fillBranch("var_b_j2_mass",    (self.tchelp.p4(tc[1]) + self.tchelp.p4(tc[2]) ).M() )
            self.out.fillBranch("var_b_j3_mass",    (self.tchelp.p4(tc[1]) + self.tchelp.p4(tc[3]) ).M() )
            self.out.fillBranch("var_sd_n2",        var_sd_n2)
            self.out.fillBranch("var_j2_mult",      tc[2].qgMult)#corrected
            self.out.fillBranch("var_j3_mult",      tc[3].qgMult)
            self.out.fillBranch("var_b_deepFlavourb",  tc[1].deepFlavourb)#btagDeepFlavB) #deepFlavourb or btagDeepFlavB                                     
            self.out.fillBranch("var_j2_deepFlavourb", tc[2].deepFlavourb)#btagDeepFlavB) 
            self.out.fillBranch("var_j3_deepFlavourb", tc[3].deepFlavourb)#btagDeepFlavB) 
            self.out.fillBranch("var_b_deepFlavourbb",  tc[1].deepFlavourbb)#btagDeepFlavB) #deepFlavourb or btagDeepFlavB                                     
            self.out.fillBranch("var_j2_deepFlavourbb", tc[2].deepFlavourbb)#btagDeepFlavB) 
            self.out.fillBranch("var_j3_deepFlavourbb", tc[3].deepFlavourbb)#btagDeepFlavB) 
            self.out.fillBranch("var_b_deepFlavourc",  tc[1].deepFlavourc)#btagDeepFlavB) #deepFlavourb or btagDeepFlavB                                     
            self.out.fillBranch("var_j2_deepFlavourc", tc[2].deepFlavourc)#btagDeepFlavB) 
            self.out.fillBranch("var_j3_deepFlavourc", tc[3].deepFlavourc)#btagDeepFlavB) 
            self.out.fillBranch("var_b_deepFlavourg",  tc[1].deepFlavourg)#btagDeepFlavB) #deepFlavourb or btagDeepFlavB                                     
            self.out.fillBranch("var_j2_deepFlavourg", tc[2].deepFlavourg)#btagDeepFlavB) 
            self.out.fillBranch("var_j3_deepFlavourg", tc[3].deepFlavourg)#btagDeepFlavB) 
            self.out.fillBranch("var_b_deepFlavourlepb",  tc[1].deepFlavourlepb)#btagDeepFlavB) #deepFlavourb or btagDeepFlavB                                     
            self.out.fillBranch("var_j2_deepFlavourlepb", tc[2].deepFlavourlepb)#btagDeepFlavB) 
            self.out.fillBranch("var_j3_deepFlavourlepb", tc[3].deepFlavourlepb)#btagDeepFlavB) 
            self.out.fillBranch("var_b_deepFlavouruds",  tc[1].deepFlavouruds)#btagDeepFlavB) #deepFlavourb or btagDeepFlavB                                     
            self.out.fillBranch("var_j2_deepFlavouruds", tc[2].deepFlavouruds)#btagDeepFlavB) 
            self.out.fillBranch("var_j3_deepFlavouruds", tc[3].deepFlavouruds)#btagDeepFlavB) 
            self.out.fillBranch("var_b_btagDeepFlavB",  tc[1].btagDeepFlavB)#btagDeepFlavB) #deepFlavourb or btagDeepFlavB                                     
            self.out.fillBranch("var_j2_btagDeepFlavB", tc[2].btagDeepFlavB)#btagDeepFlavB) 
            self.out.fillBranch("var_j3_btagDeepFlavB", tc[3].btagDeepFlavB)#btagDeepFlavB) 
            self.out.fillBranch("var_j2_nConstituents", tc[2].nConstituents) #Nano deepflavour
            self.out.fillBranch("var_j3_nConstituents", tc[3].nConstituents) #Nano deepflavour
            # self.out.fillBranch("var_j2_btagDeepFlavC", tc[2].deepFlavourc) #in 102X: btagDeepFlavB and btagDeepFlavC
            # self.out.fillBranch("var_j3_btagDeepFlavC", tc[3].deepFlavourc) #
            # self.out.fillBranch("var_j2_btagDeepC", tc[2].btagDeepC) #in 102X: btagDeepFlavB and btagDeepFlavC
            # self.out.fillBranch("var_j3_btagDeepC", tc[3].btagDeepC) #
            self.out.fillBranch("var_j2_qgl", tc[2].qgl) #
            self.out.fillBranch("var_j3_qgl", tc[3].qgl) #
            self.out.fillBranch("nrestops",        len(ResTops))
            self.out.fillBranch("NgoodtagRes",     NgoodtagRes)
            self.out.fillBranch("NmistagRes",      NmistagRes)
            self.out.fillBranch("NoCandFlag",      nocand) #no tops found
            self.out.fill()
        return True #true for non-friend! False for friend. I think ##CORRECTION: i think it doesnt matter....

TopTrainMod = lambda : BDTModTrainingProducer()
