import subprocess
import os.path
# import catboost as cat
from catboost import CatBoostClassifier
import varlists as vl
import os.path
from rootpy.tree import Tree, TreeModel, FloatCol
from rootpy.io import root_open
from rootpy.tree import Tree
import argparse
from redofilelist import redofiles
from array import array
import numpy as np
import pandas as pd
import ROOT as r
from updatedthysystnormrecord import pdfnorms, MEnorms
from PhysicsTools.NanoTrees.helpers.bSFcorrHelper import bSFcorrHelper


# #adds theoretical uncertainties
parser = argparse.ArgumentParser(description='rootfile skimmer and more!')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()
# possible_procs = ['TTTT', 'ttHTobb','ttHToNonbb', 'TTG', 'TTWJetsToLNu','TTWJetsToQQ', 'TTZToLLNuNu', 'TTZToQQ']
possible_procs = ['/TT_Tune', '/TT_SPLIT','TTToHadronic', 'TTToSemiLeptonic', 'TTTo2L2Nu', 'TTTT', 'ttHTobb','ttHToNonbb', 'TTG', 'TTWJetsToLNu','TTWJetsToQQ', 'TTZToLLNuNu', 'TTZToQQ']
# if args.year=='2017':
#     possible_procs = ['TTTT', 'ttHTobb', 'ttHToNonbb', 'TTG', 'TTW', 'TTZ']
    
# addME   = True
# addPDF  = True
# addPS   = True
# addtrig = True
# addbSFc = True

# reweightTTG = False
# TTGweights = {'2016':0.0007743, '2017':0.0012953, '2018':0.0020066}

def get_sf(hist, var1, var2, syst=None):
    # `eta` refers to the first binning var, `pt` refers to the second binning var
    x, y = var1, var2
    SF = np.ones(3)
    SF = getBinContent2D(hist, x, y)
    if syst is not None:
        stat = SF[1] - SF[0]
        unc  = np.sqrt(syst*syst+stat*stat)
        SF[1] = SF[0] + unc
        SF[2] = SF[0] - unc
    return SF

def getBinContent2D(hist, x, y):
    bin_x0 = hist.GetXaxis().FindFixBin(x)
    bin_y0 = hist.GetYaxis().FindFixBin(y)
    binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
    binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
    if binX>hist.GetNbinsX(): ##? set to last bin
        binX=hist.GetNbinsX()
    if binY>hist.GetNbinsY():
        binY=hist.GetNbinsY()
    val = hist.GetBinContent(binX, binY)
    err = hist.GetBinError(binX, binY)
    return np.array([val, val + err, val - err])

# if addtrig:
#load trigger histogram
trigfile      = r.TFile(os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/TriggerSFFiles/'+args.year+'v6_2D.root'))
if args.year=='2017':
    trigfile      = r.TFile(os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/TriggerSFFiles/effSFs_2017fix.root'))
trighist      = trigfile.Get("h2_eff")
#
trighist.SetDirectory(0)
trigfile.Close()


#load files:
bSFch = bSFcorrHelper(args.year)

for f in args.filenames: #get proper outname:

    #initialize in file loop
    addME   = True
    addPDF  = True
    addPS   = True
    addtrig = True
    addbSFc = True

    exitloop=False
    proc = ''
    outf=f
    outname = outf[outf.rfind('/')+1:]
    MErenorm = []; pdfrenorm = []
    found=False
    for pn in possible_procs:
        # if 'minorMC' or  'merged' in f:
        #     addME   = False
        #     addPDF  = False
        #     addPS   = False
        #     addtrig = True
        #     addbSFc = True
        #     print 'filling trigger info...'
        #     found=True
        #     break
            
        if (pn in f):# or (outname in flist[pn]): 
            found=True
            proc=pn
            print 'process:', pn
            # if proc not in ['TT_', 'TTTo','QCD','ttinc']:
            MErenorm = MEnorms[pn][args.year]
            pdfrenorm = pdfnorms[pn][args.year]
            # if args.year=='2018' and pn=='TTTT' and 'split' in f: #use alternative for split samples, i think this was when didnt consider full set
            #     print '2018split'
            #     MErenorm = MEnorms[pn]['2018split']
            #     pdfrenorm = pdfnorms[pn]['2018split']
            # if pn in ['/TT_Tune', '/TT_SPLIT', 'TTToHadronic', 'TTToSemiLeptonic', 'TTTo2L2Nu']:
            #     print 'ttbar: not calculating bSFc'
            #     addbSFc=False
            break #exit loop

    if not found:
        # print 'file', outname, 'not found in list and process not known'
        # continue
        print 'process not found: filling trigger/bSFcorr info as if minorMC. file:', outname
        addME   = False
        addPDF  = False
        addPS   = False
        addtrig = True
        addbSFc = True
        
    f=f.replace("/eos/uscms/","root://cmseos.fnal.gov/")
    print 'prepping input file', f, '...'
    outdir = './rootfiles/'+args.year+'/'


## load files:
# for f in args.filenames: #get proper outname:
#     outf=f
#     f=f.replace("/eos/uscms/","root://cmseos.fnal.gov/")
#     print 'prepping input file', f, '...'
    rf = r.TFile.Open(f)
    tree = rf.Get('Events')
    nevts = tree.GetEntries()
    print 'nentries: ', nevts
    print outdir+proc+'_'+outname
    if nevts < 1:
        print 'NO ENTRIES IN FILE... continuing...'
        continue
    # tree.SetBranchStatus("ISR_Up",0)
    # tree.SetBranchStatus("FSR_Up",0)
    # tree.SetBranchStatus("ISR_Down",0)
    # tree.SetBranchStatus("FSR_Down",0)

    if addbSFc:
        #get histograms
        bcproc, corrhists = bSFch.getbSFcorrhists(f)
        print bcproc

    outrf = r.TFile.Open(outdir+proc+'_'+outname, "update")    
    outrf.cd()
    outtree=tree.CloneTree(0)#0 #clone events    

    #set up arrays
    #initialize branches
    if addPS:
        print 'filling ISR/FSR'
        isrUp = np.full(1, 1.0, dtype=np.float32); isrDown = np.full(1, 1.0, dtype=np.float32)
        fsrUp = np.full(1, 1.0, dtype=np.float32); fsrDown = np.full(1, 1.0, dtype=np.float32)
        outtree.Branch('isr_Up',   isrUp,   'isr_Up/F')
        outtree.Branch('isr_Down', isrDown, 'isr_Down/F')
        outtree.Branch('fsr_Up',   fsrUp,   'fsr_Up/F')
        outtree.Branch('fsr_Down', fsrDown, 'fsr_Down/F')
    if addME:                  
        print 'filling muR/muF'
        meUp= np.full(1, 1.0, dtype=np.float32);  meDown = np.full(1, 1.0, dtype=np.float32)
        mernUp= np.full(1, 1.0, dtype=np.float32);  mernDown = np.full(1, 1.0, dtype=np.float32)
        # outtree.Branch('ME_Up',    meUp,    'ME_Up/F')
        # outtree.Branch('ME_Down',  meDown,  'ME_Down/F')
        outtree.Branch('MErn_Up',    mernUp,    'MErn_Up/F')
        outtree.Branch('MErn_Down',  mernDown,  'MErn_Down/F')
    if addPDF:
        print 'filling pdf'
        pdfUp = np.full(1, 1.0, dtype=np.float32); pdfDown = np.full(1, 1.0, dtype=np.float32)
        pdfrnUp = np.full(1, 1.0, dtype=np.float32); pdfrnDown = np.full(1, 1.0, dtype=np.float32)
        # outtree.Branch('pdf_Up',   pdfUp,   'pdf_Up/F')
        # outtree.Branch('pdf_Down', pdfDown, 'pdf_Down/F')
        outtree.Branch('pdfrn_Up',   pdfrnUp,   'pdfrn_Up/F')
        outtree.Branch('pdfrn_Down', pdfrnDown, 'pdfrn_Down/F')
    if addtrig:
        print 'filling trigSF'
        trig_nosys = np.full(1, 1.0, dtype=np.float32)
        trigUp = np.full(1, 1.0, dtype=np.float32);     trigDown = np.full(1, 1.0, dtype=np.float32)
        trig_jerUp = np.full(1, 1.0, dtype=np.float32); trig_jerDown = np.full(1, 1.0, dtype=np.float32)
        trig_jesUp = np.full(1, 1.0, dtype=np.float32); trig_jesDown = np.full(1, 1.0, dtype=np.float32)
        outtree.Branch('trigSF_nosys',      trig_nosys,     'trigSF_nosys/F')
        outtree.Branch('trigSF_Up',   trigUp,   'trigSF_Up/F')
        outtree.Branch('trigSF_Down', trigDown, 'trigSF_Down/F')
        # if proc not in ['QCD', 'TT_', 'TTTo', 'ttinc']:
        outtree.Branch('trigSF_jerUp',      trig_jerUp,     'trigSF_jerUp/F')
        outtree.Branch('trigSF_jerDown',    trig_jerDown,   'trigSF_jerDown/F')
        outtree.Branch('trigSF_jesUp',      trig_jesUp,     'trigSF_jesUp/F')
        outtree.Branch('trigSF_jesDown',    trig_jesDown,   'trigSF_jesDown/F')
    if addbSFc:
        print 'filling bSFcorr'
        bSFcorr_nosys = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_jesUp = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_jesDown = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_jerUp = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_jerDown = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagLFUp = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagLFDown = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagHFUp = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagHFDown = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagLFstats1Up = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagLFstats1Down = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagHFstats1Up = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagHFstats1Down = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagLFstats2Up = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagLFstats2Down = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagHFstats2Up = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagHFstats2Down = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagCFerr1Up = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagCFerr1Down = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagCFerr2Up = np.full(1, 1.0, dtype=np.float32)
        bSFcorr_btagCFerr2Down = np.full(1, 1.0, dtype=np.float32)
        outtree.Branch('bSFcorr_nosys',   bSFcorr_nosys,   'bSFcorr_nosys/F')
        outtree.Branch('bSFcorr_jesUp',   bSFcorr_jesUp,   'bSFcorr_jesUp/F')
        outtree.Branch('bSFcorr_jesDown', bSFcorr_jesDown, 'bSFcorr_jesDown/F')
        outtree.Branch('bSFcorr_jerUp',   bSFcorr_jerUp,   'bSFcorr_jerUp/F')
        outtree.Branch('bSFcorr_jerDown', bSFcorr_jerDown, 'bSFcorr_jerDown/F')
        outtree.Branch('bSFcorr_btagLFUp',   bSFcorr_btagLFUp,   'bSFcorr_btagLFUp/F')
        outtree.Branch('bSFcorr_btagLFDown',   bSFcorr_btagLFDown,   'bSFcorr_btagLFDown/F')
        outtree.Branch('bSFcorr_btagHFUp',   bSFcorr_btagHFUp,   'bSFcorr_btagHFUp/F')
        outtree.Branch('bSFcorr_btagHFDown',   bSFcorr_btagHFDown,   'bSFcorr_btagHFDown/F')
        outtree.Branch('bSFcorr_btagLFstats1Up',   bSFcorr_btagLFstats1Up,   'bSFcorr_btagLFstats1Up/F')
        outtree.Branch('bSFcorr_btagLFstats1Down',   bSFcorr_btagLFstats1Down,   'bSFcorr_btagLFstats1Down/F')
        outtree.Branch('bSFcorr_btagHFstats1Up',   bSFcorr_btagHFstats1Up,   'bSFcorr_btagHFstats1Up/F')
        outtree.Branch('bSFcorr_btagHFstats1Down',   bSFcorr_btagHFstats1Down,   'bSFcorr_btagHFstats1Down/F')
        outtree.Branch('bSFcorr_btagLFstats2Up',   bSFcorr_btagLFstats2Up,   'bSFcorr_btagLFstats2Up/F')
        outtree.Branch('bSFcorr_btagLFstats2Down',   bSFcorr_btagLFstats2Down,   'bSFcorr_btagLFstats2Down/F')
        outtree.Branch('bSFcorr_btagHFstats2Up',   bSFcorr_btagHFstats2Up,   'bSFcorr_btagHFstats2Up/F')
        outtree.Branch('bSFcorr_btagHFstats2Down',   bSFcorr_btagHFstats2Down,   'bSFcorr_btagHFstats2Down/F')
        outtree.Branch('bSFcorr_btagCFerr1Up',   bSFcorr_btagCFerr1Up,   'bSFcorr_btagCFerr1Up/F')
        outtree.Branch('bSFcorr_btagCFerr1Down',   bSFcorr_btagCFerr1Down,   'bSFcorr_btagCFerr1Down/F')
        outtree.Branch('bSFcorr_btagCFerr2Up',   bSFcorr_btagCFerr2Up,   'bSFcorr_btagCFerr2Up/F')
        outtree.Branch('bSFcorr_btagCFerr2Down',   bSFcorr_btagCFerr2Down,   'bSFcorr_btagCFerr2Down/F')

    #fill branches
    for i, evt in enumerate(tree):
        # if reweightTTG:
        #     evt.weight = newweight[0]
        # if i==0:
        #     print 'weight:',evt.weight

        #isr and fsr systs
        #[0] is ISR=0.5 FSR=1; [1] is ISR=1 FSR=0.5; [2] is ISR=2 FSR=1; [3] is ISR=1 FSR=2 
        # print evt.PSWeight[0], evt.PSWeight[1], evt.PSWeight[2], evt.PSWeight[3]
        if addPS:
            if len(evt.PSWeight)==4:
                isrUp[0] = evt.PSWeight[2]
                isrDown[0] = evt.PSWeight[0]
                fsrUp[0] = evt.PSWeight[3]
                fsrDown[0] = evt.PSWeight[1]
            else:
                if i==0:
                    print 'no psweight included, setting to 1'
        # #ME systs
        # [0] is muR=0.50000E+00 muF=0.50000E+00 Rdown Fdown 
        # [1] is muR=0.50000E+00 muF=0.10000E+01 Rdown Fnone
        # [2] is muR=0.50000E+00 muF=0.20000E+01 Rdown Fup
        # [3] is muR=0.10000E+01 muF=0.50000E+00 Rnone Fdown
        # [4] is muR=0.10000E+01 muF=0.10000E+01 Rnone Fnone
        # [5] is muR=0.10000E+01 muF=0.20000E+01 Rnone Fup
        # [6] is muR=0.20000E+01 muF=0.50000E+00 Rup   Fdown
        # [7] is muR=0.20000E+01 muF=0.10000E+01 Rup   Fnone
        # [8] is muR=0.20000E+01 muF=0.20000E+01 Rup   Fup
        
        #MErenorm[4] should be the unvaried normalization (weight is always 1)
        if addME :
            #just for ttHToNonbb_SPLIT_25_tree.root and ttHToNonbb_SPLIT_26_tree.root 2017, which has some strange LHEScaleWeight arrays  
            if (args.year =='2017') and ('ttHToNonbb_SPLIT' in f) and len(evt.LHEScaleWeight)!=9:
                print 'ME ones for event', evt.event, 'file',f
                # muRF_nom = np.ones(9); muR_down = np.ones(9); muF_down = np.ones(9); muRF_down = np.ones(9); muR_up = np.ones(9); muF_up = np.ones(9); muRF_up = np.ones(9)
                mernDown[0]=1.0
                mernUp[0]=1.0
            else:
                muRF_nom  = evt.LHEScaleWeight[4]*MErenorm[4]
                muR_down  = evt.LHEScaleWeight[1]*MErenorm[1]
                muF_down  = evt.LHEScaleWeight[3]*MErenorm[3]
                muRF_down = evt.LHEScaleWeight[0]*MErenorm[0]
                muR_up    = evt.LHEScaleWeight[7]*MErenorm[7]
                muF_up    = evt.LHEScaleWeight[5]*MErenorm[5]
                muRF_up   = evt.LHEScaleWeight[8]*MErenorm[8]
            
                mernDown[0] = min([muR_down, muF_down, muRF_down, muR_up, muF_up, muRF_up])
                mernUp[0] = max([muR_down, muF_down, muRF_down, muR_up, muF_up, muRF_up])
            # meDown[0] = min([evt.LHEScaleWeight[1], evt.LHEScaleWeight[3], evt.LHEScaleWeight[0], evt.LHEScaleWeight[7], evt.LHEScaleWeight[5], evt.LHEScaleWeight[8]])
            # meUp[0] = max([evt.LHEScaleWeight[1], evt.LHEScaleWeight[3], evt.LHEScaleWeight[0], evt.LHEScaleWeight[7], evt.LHEScaleWeight[5], evt.LHEScaleWeight[8]])
            # print evt.LHEScaleWeight[0],evt.LHEScaleWeight[8],evt.LHEScaleWeight[4] 
            # print 'me',mernUp[0], mernDown[0]
        # #pdf systs
        if addPDF :
            if proc in ['TTZToLLNuNu', 'TTWJetsToLNu']: #no pdf for ttg
                pdfrnUp[0]=1.0
                pdfrnDown[0]=1.0
            else:
                pdf_nom = 1.0  #nominal weight is 1

                #just for ttHToNonbb_SPLIT_0_tree.root 2017, which has some strange LHEPdfWeight arrays
                if (args.year =='2017') and ('ttHToNonbb_SPLIT' in f) and len(evt.LHEPdfWeight)!=100:
                    print 'pdf ones for event', evt.event, 'file',f
                    pdfarr = np.ones(100)
                else: 
                    pdfarr = np.multiply(np.asarray(evt.LHEPdfWeight), np.asarray(pdfrenorm))

                # pdfarr = np.multiply(np.asarray(evt.LHEPdfWeight), np.asarray(pdfrenorm))
                pdf_std = np.std(pdfarr)
                pdfrnUp[0]   = (pdf_nom + pdf_std)#*evt.LHEWeight_originalXWGTUP
                pdfrnDown[0] = (pdf_nom - pdf_std)#*evt.LHEWeight_originalXWGTUP
            # pdfUp[0]   = (pdf_nom + np.std(np.asarray(evt.LHEPdfWeight)))#*evt.LHEWeight_originalXWGTUP
            # pdfDown[0] = (pdf_nom - np.std(np.asarray(evt.LHEPdfWeight)))#*evt.LHEWeight_originalXWGTUP
            # print 'pdf',pdfrnUp[0], pdfrnDown[0]

        #trigger efficiency and systs
        if addtrig: #fill only if within histogram bounds, else 1
            trigSF = get_sf(trighist, evt.nbjets_nosys, evt.njets_nosys)
            trig_nosys[0]     = trigSF[0]
            trigUp[0]         = trigSF[1]
            trigDown[0]       = trigSF[2]
            # if proc not in ['TT_', 'TTTo','QCD', 'ttinc']:
            trigSF_jerUp   = get_sf(trighist, evt.nbjets_jerUp, evt.njets_jerUp)
            trig_jerUp[0]     = trigSF_jerUp[0]
            trigSF_jerDown = get_sf(trighist, evt.nbjets_jerDown, evt.njets_jerDown)
            trig_jerDown[0]   = trigSF_jerDown[0]
            trigSF_jesUp   = get_sf(trighist, evt.nbjets_jesUp, evt.njets_jesUp)
            trig_jesUp[0]     = trigSF_jesUp[0]
            trigSF_jesDown = get_sf(trighist, evt.nbjets_jesDown, evt.njets_jesDown)
            trig_jesDown[0]   = trigSF_jesDown[0]
        
        if addbSFc:
            SFs = bSFch.getbSFcorrs(evt, bcproc, corrhists) #get SFs
            bSFcorr_nosys[0] = SFs["corr"+bcproc+args.year+'_nosys']
            bSFcorr_jesUp[0] = SFs["corr"+bcproc+args.year+'_jesUp']
            bSFcorr_jesDown[0] = SFs["corr"+bcproc+args.year+'_jesDown']
            bSFcorr_jerUp[0] = SFs["corr"+bcproc+args.year+'_jerUp']
            bSFcorr_jerDown[0] = SFs["corr"+bcproc+args.year+'_jerDown']
            bSFcorr_btagLFUp[0] = SFs["corr"+bcproc+args.year+'_btagLFUp']
            bSFcorr_btagLFDown[0] = SFs["corr"+bcproc+args.year+'_btagLFDown']
            bSFcorr_btagHFUp[0] = SFs["corr"+bcproc+args.year+'_btagHFUp']
            bSFcorr_btagHFDown[0] = SFs["corr"+bcproc+args.year+'_btagHFDown']
            bSFcorr_btagLFstats1Up[0] = SFs["corr"+bcproc+args.year+'_btagLFstats1Up']
            bSFcorr_btagLFstats1Down[0] = SFs["corr"+bcproc+args.year+'_btagLFstats1Down']
            bSFcorr_btagHFstats1Up[0] = SFs["corr"+bcproc+args.year+'_btagHFstats1Up']
            bSFcorr_btagHFstats1Down[0] = SFs["corr"+bcproc+args.year+'_btagHFstats1Down']
            bSFcorr_btagLFstats2Up[0] = SFs["corr"+bcproc+args.year+'_btagLFstats2Up']
            bSFcorr_btagLFstats2Down[0] = SFs["corr"+bcproc+args.year+'_btagLFstats2Down']
            bSFcorr_btagHFstats2Up[0] = SFs["corr"+bcproc+args.year+'_btagHFstats2Up']
            bSFcorr_btagHFstats2Down[0] = SFs["corr"+bcproc+args.year+'_btagHFstats2Down']
            bSFcorr_btagCFerr1Up[0] = SFs["corr"+bcproc+args.year+'_btagCFerr1Up']
            bSFcorr_btagCFerr1Down[0] = SFs["corr"+bcproc+args.year+'_btagCFerr1Down']
            bSFcorr_btagCFerr2Up[0] = SFs["corr"+bcproc+args.year+'_btagCFerr2Up']
            bSFcorr_btagCFerr2Down[0] = SFs["corr"+bcproc+args.year+'_btagCFerr2Down']

        outtree.Fill()
        # print 'evt#', i        
        # print 'isr', isrUp[i], isrDown[i]
        # print 'fsr', fsrUp[i], fsrDown[i]
        # print 'ME', meUp[i], meDown[i]
        # print 'pdf', pdfUp[i], pdfDown[i]
    
    # outtree.Scan('ME_Up')
    outtree.Write("", r.TFile.kOverwrite)
    outtree.ResetBranchAddresses()

    outrf.Close()
    rf.Close()
    print 'wrote', outrf
    reweightTTG=False


#     #ISR/FSR
#     isrfsr = {'isr_Up'  : array('f', [0.0]*nevts),
#               'isr_Down': array('f', [0.0]*nevts),
#               'fsr_Up'  : array('f', [0.0]*nevts),
#               'fsr_Down': array('f', [0.0]*nevts)}
#     #muf/mur
#     mufmur= {'muR_muF_Down' : array('f', [0.0]*nevts),
#              'muR_Down'     : array('f', [0.0]*nevts),
#              'muF_Down'     : array('f', [0.0]*nevts),
#              'muR_muF_Up'   : array('f', [0.0]*nevts),
#              'muR_Up'       : array('f', [0.0]*nevts),
#              'muF_Up'       : array('f', [0.0]*nevts)}

#     outtree=tree.Clone(treename)
#     for key, arr in isrfsr.iteritems():
#         outtree.Branch(key, arr, key+'/F')
#     for key, arr in mufmur.iteritems():
#         outtree.Branch(key, arr, key+'/F')
#     # for key, arr in isrfsr.iteritems():
#     #     outtree.Branch(key, arr, key+'/F')

#     for ein, eout in zip(tree, outtree):
        
    

#     histfile = r.TFile(self.outroot, "UPDATE")
#     # hist.Write()
#     histfile.Close()
#     del histfile
# >>>>>>> 9beebfe1885515810be6b070f09d45e763596505
