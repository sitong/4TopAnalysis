import numpy as np
import ROOT as r

#example implementation
# subMC = addMCsubweight()
# RT1BT0hist_2016 = subMC.setweighthist('RT1BT0', 2016)
# RT1BT1hist_2016 = subMC.setweighthist('RT1BT1', 2016)
# RT2hist_2016 = subMC.setweighthist('RT2BTALL', 2016)
# #loop over events in data file
#     weight = subMC.getweight(ht, bdt, RT1BT0hist_2016)

class addMCsubweight:
    def __init__(self, includettbb=False):
        self.includettbb = includettbb

        if not includettbb:
            filename = "MCsubweight_withttbb.root"
        else :
            filename = "MCsubweight_NOttbb.root"
        self.histfilename = filename

    def getweighthist(self, topsel, year, verbose=True): 
        #topsel must be RT1BT0, RT1BT1 or RT2BTALL
        histname = 'corr'+year+'_'+topsel
        if verbose:
            print 'Getting hist', histname, 'from file', self.histfilename
        weighthistfile = r.TFile(self.histfilename,"READ")
        weighthist = weighthistfile.Get(histname)
        weighthist.SetDirectory(0)
        weighthistfile.Close()
        return weighthist

    def getweight(self, HTval, BDTval, weighthist, verbose=False):
        useHT = HTval
        useBDT =  BDTval #in case I want the same check later
        #if HT is greater than 2000.0 input a value less than that to put it in that bin
        if HTval>2000.0:
            useHT = 1999.0
        wgt = self.get_sf(weighthist, useHT, useBDT)
        #wgt = [weight, weight up, weight down] (with statistical errors)
        if verbose:
            print 'HT:', HTval, 'BDT:', BDTval, 'weight', wgt[0]
        return wgt[0]

    def getBinContent2D(self, hist, x, y):
        bin_x0 = hist.GetXaxis().FindFixBin(x)
        bin_y0 = hist.GetYaxis().FindFixBin(y)
        binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
        binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
        if binX>hist.GetNbinsX(): ##? set to last bin
            binX=hist.GetNbinsX()
        if binY>hist.GetNbinsY():
            binY=hist.GetNbinsY()
        val = hist.GetBinContent(binX, binY)
        # print 'val', val, 'binX', binX, 'binY', binY, 'last bin:', hist.GetNbinsX(), hist.GetNbinsY()
        err = hist.GetBinError(binX, binY)
        return np.array([val, val + err, val - err])

    def get_sf(self, hist, var1, var2, syst=None):
        # `eta` refers to the first binning var, `pt` refers to the second binning var
        x, y = var1, var2
        SF = np.ones(3)
        SF = self.getBinContent2D(hist, x, y)
        if syst is not None:
            stat = SF[1] - SF[0]
            unc  = np.sqrt(syst*syst+stat*stat)
            SF[1] = SF[0] + unc
            SF[2] = SF[0] - unc
        return SF

    def makeroot(self, infile, treename, options="read"):
        rfile = r.TFile(infile, options)
        tree = rfile.Get(treename)
        return rfile, tree
