from root_numpy import root2array, array2root
# import varlists as vl
import numpy as np
import pandas as pd
# import xgboost as xgb
import subprocess
import os
import sys
import ROOT as r
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score
import re
# import xml.etree.cElementTree as ET
import sys
sys.path.append("../plotting/")
import RootStuff as rs
import argparse

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
args = parser.parse_args()

infiles = args.filenames#'/eos/uscms//store/user/mequinna/NanoAODv5/restop_training/modtrain/TTJets_DiLept_trainsamp_Friend.root'
treename = 'Events'
wgtvar = "weight"

lists ={'other':[]}

dic = { 0.000774330514089 : ['2016','TTG'],
        7.52140175661e-05 : ['2016','TTWJetsToLNu'],
        0.00094397062582  : ['2016','TTWJetsToQQ'],
        3.93874618916e-05 : ['2016','TTZToLLNuNu'],
        0.00150841202401  : ['2016','TTZToQQ'],
        3.00705982624e-05 : ['2016','ttHToNonbb'],
        3.0046759886e-05  : ['2016','ttHTobb'],
        1.18629356416e-05 : ['2016','TTTT'],
        0.0341880774778   : ['2016','WJetsToQQ_HT400to600'],
        0.00699175611619  : ['2016','WJetsToQQ_HT600to800'],
        0.00364778735385  : ['2016','WJetsToQQ_HT800toInf'],
        0.00719116884445  : ['2016','ZJetsToQQ_HT400to600'],
        0.00367597914717  : ['2016','ZJetsToQQ_HT600to800'],
        0.00107883609331  : ['2016','ZJetsToQQ_HT800toInf'],
        0.00515617381699  : ['2016','ST_tW_top'],
        0.00517085157074  : ['2016','ST_tW_antitop'],
        0.0020269461947   : ['2016','ST_t-channel_top'],
        0.00208574797203  : ['2016','ST_t-channel_antitop'],
#
        0.00129531046458  : ['2017','TTG'],
        3.86160302078e-05 : ['2017','TTWJetsToLNu'],
        0.00102900569981  : ['2017','TTWJetsToQQ'],
        3.45679849339e-05 : ['2017','TTZToLLNuNu'],
        0.00011509952839  : ['2017','TTZToQQ'],
        2.75250102019e-05 : ['2017','ttHToNonbb'],
        3.74534034472e-05 : ['2017','ttHTobb'],
        0.00452843630631  : ['2017','ST_tW_antitop'],
        3.21755929358e-06 : ['2017','TTTT'],
        0.0335720097831   : ['2017','WJetsToQQ_HT400to600'],
        0.00784122512123  : ['2017','WJetsToQQ_HT600to800'],
        0.00432226025064  : ['2017','WJetsToQQ_HT800toInf'],
        0.0140979476791   : ['2017','ZJetsToQQ_HT400to600'],
        0.00388918007492  : ['2017','ZJetsToQQ_HT600to800'],
        0.00238254565086  : ['2017','ZJetsToQQ_HT800toInf'],
        0.00452843630631  : ['2017','ST_tW_top'],
        0.0227379713758   : ['2017','ST_t-channel_top'],
        0.0220217578776   : ['2017','ST_t-channel_antitop'],
        0.000288992668185 : ['2017','ZJetsToNuNu_HT-600To800'],
#
        0.00200663595651  : ['2018','TTG'],
        7.6058367258e-05  : ['2018','TTWJetsToLNu'],
        0.00088646213877  : ['2018','TTWJetsToQQ'],
        4.03089170848e-05 : ['2018','TTZToLLNuNu'],
        0.000115736896844 : ['2018','TTZToQQ'],
        2.91924917074e-05 : ['2018','ttHToNonbb'],
        1.39908645472e-05 : ['2018','ttHTobb'],
        0.000943962009672 : ['2018','ST_t-channel_top'],
        0.00375238959706  : ['2018','ST_tW_top'],
        2.68399813731e-06 : ['2018','TTTT'],
        0.0314426703106   : ['2018','WJetsToQQ_HT400to600'],
        0.00451037576544  : ['2018','WJetsToQQ_HT600to800'],
        0.00236243649063  : ['2018','WJetsToQQ_HT800toInf'],
        0.00869694406779  : ['2018','ZJetsToQQ_HT400to600'],
        0.00231444818933  : ['2018','ZJetsToQQ_HT600to800'],
        0.00177824470701  : ['2018','ZJetsToQQ_HT800toInf'],
        0.0047244530309   : ['2018','ST_tW_antitop'],
        0.00109057160098  : ['2018','ST_t-channel_antitop'],

    }

for key in dic:
    key=np.float32(key)

print 'n', 'file', 'year', 'process', 'weight'
for n,infile in enumerate(infiles):
    # print "prepping input file", infile
    f=infile.replace("/eos/uscms/","root://cmseos.fnal.gov/")
    rf = r.TFile.Open(f)
    tree = rf.Get('Events')
    outname = f[f.rfind('/')+1:]
    idy = 'other'
    for i,evt in enumerate(tree):
        info = ['?','?']
        weight = abs(evt.weight)
        if i>0:
            break
        smallest = 1.0; sk=0.0
        if weight != 1.0:
            for key in dic:
                if abs(weight-key)<smallest:
                    smallest=abs(weight-key)
                    sk=key
            # print smallest, weight, sk
            info = dic[sk]
            idy = info[0]+'-'+info[1]
            if idy not in lists:
                lists[idy] = []
        lists[idy].append(outname)
        print n, outname, info[0], info[1], weight, sk
    
    rf.Close()

print '\n'
for key, val in lists.iteritems():
    print key
    print val
    print '\n'
    

