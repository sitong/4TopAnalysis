#adds event bdt disc to existing tree, NOT ntuple
#also applies baseline selection!!
from root_numpy import root2array, array2root
import varlists as vl
import argparse
import numpy as np
import pandas as pd
from root_pandas import to_root
import xgboost as xgb
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score
import re
import xml.etree.cElementTree as ET
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
# import PhysicsTools.NanoTrees.python.macros.plotting.RootStuff as rs
import RootStuff as rs

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
# parser.add_argument("-i", "--indir", dest="indir", default='/eos/uscms/store/user/mequinna/NanoAODv5/2016MC/fullset/')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
# parser.add_argument("-o", "--outdir", dest="outdir", default='./updatedrootfiles/')
parser.add_argument("-t", "--treename", dest="treename", default='Events')
# parser.add_argument("-b", "--baseline", dest="base", default='nleptons>=1 && njets>=2')
parser.add_argument("-b", "--baseline", dest="base", default='nleptons==0 && njets>=6')
# parser.add_argument("-b", "--baseline", dest="base", default='nleptons==0 && ht>700 & njets>=7 && nbjets>=2')
parser.add_argument("-v", "--version", dest="vers", default=['bdt4'])
parser.add_argument("-q", "--qcdtrain", dest="qcdtrain", default=True)
# parser.add_argument("-w", "--reweight", dest="reweight", default=False)
parser.add_argument("-k", "--knownweight", dest="knownweight", default=False)

args = parser.parse_args()
note = '_0lep'

def addBDT(args):
    print 'qcdtrain?', args.qcdtrain
    qcdtrain = True
    if args.qcdtrain=='False':
        print 'TT training'
        outdir = './updatedrootfiles_TT/'
        qcdtrain = False
    else:
        print 'QCD+TT training'
        outdir = './updatedrootfiles/'
        qcdtrain = True
    # else:
    #     print 'ERROR must be True or False!'
    #     raise ValueError

    filenames = args.filenames #['TTTT_TuneCUETP8M2T4_13TeV-amcatnlo-pythia8_1_tree.root']
    treename = args.treename #'Events'
    baseline = ''#args.base #'nleptons==0 & ht>700 & njets>=9 & nbjets>=3' 
    bdtdir = '/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/eventMVA/outputfiles/'
    
    print "Will fill bdt disc for", len(filenames), "files..."
    for f in filenames:
    #get proper filename:
        filename = f[f.rfind('/')+1:]
        filename = filename.strip('.root')

        infile = f
        if '1lep' in infile:
            note='_1lep'
            baseline = 'nleptons>=1 && njets>=3'
        elif '0lep' in infile:
            note='_0lep'
            baseline = 'nleptons==0 && njets>=6'

        print 'baseline', baseline
        if qcdtrain:
            filename += '_'+'withBDT'+note+'.root'
        elif not qcdtrain:
            filename += '_'+'withTTBDT'+note+'.root'

        new_vars = []
    # convert to xgb readable format
    # for i in range(nentries):
        for i,bdt in enumerate(args.vers):
            print 'adding bdt', bdt
            #uproot:
            # events.arrays("p[xyz]1", outputtype=pandas.DataFrame, entrystop=10)

            if i==0:
                print "prepping input file", infile
                data = pd.DataFrame(root2array(infile, treename, selection=baseline))
            else: #to make sure updating the file
                print "prepping input file", outdir+filename
                data = pd.DataFrame(root2array(outdir+filename, treename, selection=baseline))
            print '# input events:', len(data)
            if len(data)==0:
                continue
            if bdt=="bdt1":
                vars = vl.bdt1 #which bdt inputs you are using 
            elif bdt=="bdt2":
                vars = vl.bdt2
            elif bdt=="bdt3":
                vars = vl.bdt3
            elif bdt=="bdt4":
                vars = vl.bdt4
            elif bdt=="new1":
                vars = vl.new1
            elif bdt=="new2":
                vars = vl.new2
            elif bdt=="new3":
                vars = vl.new3
            elif bdt=="new4":
                vars = vl.new4
            elif bdt=="best12":
                vars = vl.best12
            elif bdt=="best20":
                vars = vl.best20 
            else:
                print 'Error: unknown bdt algorithm'

            bdtname  = vars['name']

            #triggers
            # for name, column in data.items:
            #     # if column.dtype == object:
            #     print column.dtype#data[column]

            #deal with discs
            # discs=np.array(data['discs'])
            # print discs.flatten()
            # print data['discs'], type(data['discs'])
            # cutdiscs = []
            alldiscs = []
            for i, d in enumerate(data['discs']):
                ntops = data['nrestops'][i]
                # print 'i',i, 'nrestops', ntops, 'discs', d[:ntops]
                # cutdiscs+=(d[:ntops].tolist())
                alldiscs+=(d[:].tolist())
            Discs = np.asarray(alldiscs, dtype=np.float32)
            # cutDiscs = np.asarray(cutdiscs, dtype=np.float32)
            #out with the old:
            data.drop(columns='discs')
            #in with the new:
            # discdata['passtopdiscs']=cutDiscs
            # discdata = pd.DataFrame(Discs, columns=['topdiscs'])
            discdata = pd.DataFrame()
            discdata['topdiscs']=Discs
            print discdata

    # infile = indir+filename
    #get right model file based on bdtname
            if not qcdtrain:
                modelfile = bdtdir+bdtname+'_ttonly_eventBDT-xgb.model'
            elif qcdtrain:
                modelfile = bdtdir+bdtname+'_qcdandtt_eventBDT-xgb.model'
            print modelfile
            branchname = bdtname+'disc'
            new_vars.append(branchname)
            # outputhd5 = False
            # if outputhd5:
            #     testset = bdtname+'_eventBDT-test-sample.h5'

    #load model
            model = xgb.Booster() #init mode. not sure about booster
            model.load_model(modelfile)
        # model = xgb.load(modelfile) #doesnt work
            obs_vars = vars['obs'] 
            varsF = vars['floats']
            varsI = vars['ints']
            input_vars = varsF+varsI
            # data = data.loc[:,  input_vars + obs_vars + new_vars].reset_index(drop=True) #new vars so it keeps other bdts
            testset = data[input_vars] #input vars to bdt
            preds = model.predict(xgb.DMatrix(testset))
            print 'writing discriminant to tree in branch:', branchname, 'output file', outdir+filename
            data[branchname] = preds
            print list(discdata.columns) 
            to_root(data, outdir+filename, key='Events', mode='a')  
            to_root(discdata, outdir+filename, key='Events', mode='a')  
            # array2root(data.to_records(index=False), filename=outdir+filename, treename=treename, mode='RECREATE')
        if len(data)==0:
            print "Empty file! Skipping..."
            continue
            
addBDT(args)

    #     print "Will fill bdt disc for", len(filenames), "files..."
    #     for f in filenames:
    # #get proper filename:
    #         filename = f[f.rfind('/')+1:]
    #         filename = filename.strip('.root')
    #         filename += '_'+bdtname+'.root'
    #         infile = f
    # # convert to xgb readable format
    # # for i in range(nentries):
    #         print "prepping input file", f
    #         data = pd.DataFrame(root2array(infile, treename, selection=baseline))
    #label by vars. need this?
            # data = data.loc[:,  input_vars + obs_vars].reset_index(drop=True)
            # print '# input events:', len(data)
    # print 'input data:', data
            # if outputhd5:
            #     data.to_hdf(testset, treename, format='table') #hd5 test sample
    # sum_wgt = data["weight"].sum() #weight?
    # data['test_weight']= data["weight"]*(1000.0/sum_wgt)
    # print 'sum weight:', sum_wgt, "test weight:", data["test_weight"]

    #calculate predicions
            # testset = data[input_vars] #input vars to bdt
            # preds = model.predict(xgb.DMatrix(testset))
        # print 'predicted discriminant:', preds
        
    #fill predictions to tree
            # print 'writing discriminant to tree in branch:', branchname, 'output file', outdir+filename
            # data[branchname] = preds
            # print branchname, preds
        # newnevts=len(branchname)
            # print 'old weight:', data["weight"][0]
            # if args.reweight:
            #     array2root(data.to_records(index=False), filename=outdir+filename, treename=treename, mode='RECREATE')
            #     warr=reweight(args, data["weight"][0], infile, outdir+filename)
            #     data["new_weight"] = warr

            #writes to file for every bdt
            # array2root(data.to_records(index=False), filename=outdir+filename, treename=treename, mode='RECREATE')
        # files.append(f)

    # return files



# #add new weight if not using full sample
# def reweight(args, weight, infile, outfile):
#     #load tree, create new branch
#     knownweight={
#         '/TT_Tune': 0.109108,
#         'TTTT_': 0.0010746,
#         'QCD_HT500to700': 9.2267838,
#         'QCD_HT700to1000': 2.9353984,
#         'QCD_HT1000to1500': 0.6348056,
#         'QCD_HT1500to2000': 0.168843,
#         'QCD_HT2000toInf': 0.012579}

#     print 'reweighting file...', infile, outfile
#     inrf, intree = rs.makeroot(infile, args.treename)
#     outrf, outtree = rs.makeroot(outfile, args.treename, options='read')
#     new_nevts = intree.GetEntries()
#     print 'input NEvents:', new_nevts
#     lenarr = outtree.GetEntries()
#     old_nevts = 0
#     mykey=''
#     for key in rs.nevt_dict:
#         if key in infile:
#             print "sample type:",key
#             old_nevts = rs.nevt_dict[key]
#             mykey=key
#     reweight = weight*(old_nevts/new_nevts)
#     if args.knownweight:
#         print 'using known weight:', mykey, knownweight[mykey]
#         reweight = knownweight[mykey] #corrected if needed
#     warr = np.full(lenarr, reweight, dtype=np.float32) 
#     branchname = 'new_weight'
#     #declare branch
#     # wbranch = outtree.Branch(branchname, warr, branchname+"/F")
#     # #fill branch
#     # for w in warr:
#     #     wbranch.Fill()
#     # outrf.Write()
#     # outrf.Close()
#     # delete outrf
#     print 'new weight:', reweight
#     return warr

