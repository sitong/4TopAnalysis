#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
# import sys
# from VRerrtempl import VRerrtempl
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-b", "--base", dest="base", default='VR') #SR, VR, or TT
parser.add_argument("-c", "--cr", dest="cr", default='D') #CR
parser.add_argument("-l", "--logy", dest="logy", default='True')
parser.add_argument("-m", "--modbins", dest="modbins", default='False')
parser.add_argument("-t", "--ttbb", dest="usettbb", default='False')
parser.add_argument("-s", "--shape", dest="shape", default='False')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

randseed=False
norm2data=False

logy=True; modbins=True; useshape=True; usettbb=True
if args.logy=='False':
    logy=False
if args.usettbb=='False':
    usettbb=False
if args.modbins=='False' :#and args.base!='VR':
    modbins=False
if args.shape=='False':
    useshape=False

if randseed:
    usettbb=True

year = args.year
base = args.base
cr = args.cr
var = "catBDTdisc_nosys" 

def predplots(args):
    # vt = VRerrtempl(year, region=args.base, usettbb=usettbb)
    if year=='ALL':
        pd = plotdefs("2016", base, cr, modbins)
        pd16 = plotdefs("2016", base, cr, modbins)
        pd17 = plotdefs("2017", base, cr, modbins)
        pd18 = plotdefs("2018", base, cr, modbins)
    else:
        pd = plotdefs(year, base, cr, modbins, usettbb)

    nbins, selrange = pd.getHistSettings(var)
    modhist = None
    for chanid, chansel in pd.binsels.iteritems():
        histname = year+'_'+base+'_'+chanid+'_'+var

        NAFname = chanid+'_'+year+'_BKG'
        # predhist = pd.NAFfile.Get(NAFname)
        sels = pd.getsels(chanid, cr)

        if year!='ALL':
            datahist = pd.gethist(pd.datafilename, 'catBDTdisc_nosys', sels['datasel'])
            if cr=='D':
                # predhist = pd.histfromtree(chanid)            
                predhist = pd.newhistfromtree(chanid)
            else:
                predhist = datahist.Clone("predhist")
  

        elif year=='ALL':
            pred16 = pd16.newhistfromtree(chanid)
            pred17 = pd17.newhistfromtree(chanid)
            pred18 = pd18.newhistfromtree(chanid)
            predhist = pred16.Clone("predhist")
            predhist.Add(pred17); predhist.Add(pred18)

            sels16 = pd16.getsels(chanid)
            data16 = pd16.gethist(pd16.datafilename, 'catBDTdisc_nosys', sels16['datasel'])
            sels17 = pd17.getsels(chanid)
            data17 = pd17.gethist(pd17.datafilename, 'catBDTdisc_nosys', sels17['datasel'])
            sels18 = pd18.getsels(chanid)
            data18 = pd18.gethist(pd18.datafilename, 'catBDTdisc_nosys', sels18['datasel'])
            print sels16
            datahist = data16.Clone("datahist")
            datahist.Sumw2()
            datahist.Add(data17); datahist.Add(data18)
            print 'SEP YLDS', pd.getyield(data16), pd.getyield(data17), pd.getyield(data18) , 'TOT', pd.getyield(datahist)

        dataname = chanid+'_'+year+'_data'

        print 'datasel', sels['datasel']
        print 'mcsel', sels['mcsel']

        if useshape: ########changed for spursig
            # uphist, downhist = pd.predshape(chanid, uncval=0.02, newtyp=True)
            uphist, downhist = pd.predshape(chanid, uncval=pd.shapeshift[year][chanid], newtyp=True)
            ####statcomm variation
            # uphist, downhist = pd.predshape(chanid, uncarr=pd.shapeshift[year][chanid])
        ###############################
            #for spurious signal
            # uphist = predhist.Clone("uphist")
            # downhist = predhist.Clone("downhist")
            # bin9 = predhist.GetBinContent(9)
            # N = (bin9*(1.0+pd.shapeshift[year][chanid])) - bin9
            # # print uphist, downhist
            # print '%', pd.shapeshift[year][chanid], 'binval', bin9, 'new binval', (bin9*(1.0+pd.shapeshift[year][chanid])), 'N',N
            # scale = (N+bin9)/bin9
            # print 'scale----------', scale 
            # uphist.Scale(scale)                                                                                                                                
            # downhist.Scale((1.0/scale))    

        if randseed:
            print 'RANDSEED TEST \n'
            predhist = pd.newhistfromtree(chanid, 'nom')
            # predhist = pd.newhistfromtree(chanid, 'med')
            uphist = pd.newhistfromtree(chanid, 'max')
            downhist = pd.newhistfromtree(chanid, 'min')
            # uphist = pd.newhistfromtree(chanid, 'hi')
            # downhist = pd.newhistfromtree(chanid, 'lo')
            uphist.Sumw2(); downhist.Sumw2()
            uphistwithmc = uphist.Clone('uphistwithmc')
            downhistwithmc = downhist.Clone('downhistwithmc')
            
        if pd.usevarbin:
            stacktot = r.TH1F('stacktot', 'stacktot', nbins, pd.edges)
        else:
            stacktot = r.TH1F('stacktot', 'stacktot', 10, 0.0, 1.0)
        stacktot.Sumw2() #stat errors

        stackhists = []; hnames = []
        mcyld=0.0
        sighist=None; sigyld=0.0        

        for proc in pd.mcprocs:
            if year!='ALL':
                if proc == 'ttbb':
                    ttbbsel=sels['mcsel']
                    ttbbsel = sels['mcsel'].replace("nfunky_leptons==0 &&","nfunky_leptons==0 && isttbb==1 &&")
                    # ttbbsel = ttbbsel.replace("*weight","*weight*1.37") #1.37 SF
                    print 'ttbbsel:', ttbbsel, '\n'
                    mchist = pd.gethist(pd.mcfiles[proc], var, ttbbsel)
                else:
                    mchist = pd.gethist(pd.mcfiles[proc], var, sels['mcsel'])
            else:
                mchist16 = pd16.gethist(pd16.mcfiles[proc], var, sels16['mcsel'])
                mchist17 = pd17.gethist(pd17.mcfiles[proc], var, sels17['mcsel'])
                mchist18 = pd18.gethist(pd18.mcfiles[proc], var, sels18['mcsel'])
                mchist = mchist16.Clone(proc)
                mchist.Sumw2()
                mchist.Add(mchist17); mchist.Add(mchist18) 
            yld, unc = pd.getyield(mchist)
            mcyld+=yld
            stackhists.append(mchist)
            stacktot.Add(mchist)
            hnames.append(proc)
            print 'MC yld:',proc, yld
            #spursig on signal
            # if proc=='tttt':
            #     S =  mchist.GetBinContent(9)
            #     scale = (N+S)/S
            #     print 'N', N, 'S', S, 'scale', scale
            #     tttt_uphist = mchist.Clone("tttt_uphist")
            #     tttt_downhist = mchist.Clone("tttt_downhist")
            #     tttt_uphist.Scale(scale)
            #     tttt_downhist.Scale((1.0/scale))
            #     if useshape or randseed:
            #         uphist.Add(tttt_uphist)
            #         downhist.Add(tttt_downhist)
            # if proc!='tttt' and (useshape or randseed):
            #     uphist.Add(mchist)
            #     downhist.Add(mchist)
            
        if cr!='D':
            #subtract MC backgrounds from data hist if CR is not D
            predhist.Add(stacktot, -1)
        stackhists.append(predhist); hnames.append('QCD+tt (DD)')
        stacktot.Add(predhist)
        # stackyld = pd.extDDyields[chanid][0]
        # stackunc = pd.extDDyields[chanid][1]#(pd.extDDyields[chanid][1]/pd.extDDyields[chanid][0])
        #add extra uncertainty if needed:
        # if addunc:
        #     uncval = pd.VRerr[year][chanid]
        #     print '\n'
        #     print 'original unc:', round(stackunc/stackyld,3), 'VRunc:', round(uncval,3)
        #     stackunc = np.sqrt((stackunc)**2 + (uncval*stackyld)**2)
        #     print 'combined:', round(stackunc/stackyld,3),'\n'
        # print 'saved yld', stackyld, 'unc', stackunc, 'relativeunc', stackunc/stackyld 
        # print 'data yld', datayld, 'unc', dataunc, 'dataunc', dataunc/datayld, '\n'
        # print 'yld ratio:d/b', datayld/stackyld

        dy=0.0; py=0.0
        if norm2data and base=='VR':
            print 'normalizing to data'
            py, pu = pd.getyield(stacktot)
            dy, du = pd.getyield(datahist)
            stacktot.Scale(dy/py) #normalize to data

        #top plot
        c = r.TCanvas("c","c",800,800)
        p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        p1.SetBottomMargin(0.1)
        p1.Draw()
        p1.cd()
        stack = r.THStack("stack", "stack")
        legend = r.TLegend(0.1, 0.6, 0.3, 0.9)

        datahist.SetLineWidth(2)
        datahist.SetMarkerStyle(20)
        datahist.SetLineColor(r.kBlack)
        if args.base !='SR':
            legend.AddEntry(datahist, "data", "L")           

        if sighist is not None:
            # sighist.Scale((mcyld+stackyld)/sigyld)
            scale =''
            # if not showsovb :
            #     if year!='ALL':
            #         sighist.Scale(10.0*sigyld)
            #         scale = " x10"
                # else:
                    # sighist.Scale(2.0*sigyld)
            sighist.SetLineWidth(3)
            sighist.SetFillColor(r.TColor.GetColor("#163d4e"))
            # datahist.SetMarkerStyle(20)
            sighist.SetLineColor(r.TColor.GetColor("#163d4e"))
            signame='tttt'+scale
            legend.AddEntry(sighist, signame, "L")           

        if useshape or randseed:
            uphist.SetLineWidth(3); downhist.SetLineWidth(3)
            uphist.SetLineColor(r.kBlue); downhist.SetLineColor(r.kRed) 
            legend.AddEntry(uphist, "up shape", "L")
            legend.AddEntry(downhist, "down shape", "L")

        hcount = 0
        for hist in stackhists:
            print hnames[hcount],pd.colors[hcount+1]
            hist.SetLineWidth(1)
            hist.SetFillColor(pd.colors[hcount])
            hist.SetLineColor(pd.colors[hcount])
            legend.AddEntry(hist, hnames[hcount], "F")           
            stack.Add(hist)
            hcount+=1
        if norm2data:
            stack = stacktot.Clone("stack")

        stack.SetTitle("")
        stack.Draw("hist")
        gPad.Modified()
        gPad.Update()
        ymax = p1.GetUymax()*1.4 #max+30%
        if year == 'ALL':
            ymax = p1.GetUymax()*1.6 #max+30%
        # print 'ymax', ymax
        stack.SetMaximum(ymax)

        stack.GetXaxis().SetNdivisions(505)
        stack.GetYaxis().SetTitleSize(20)
        stack.GetYaxis().SetTitleFont(43)
        stack.GetYaxis().SetTitleOffset(1.55)
        stack.GetYaxis().SetTitle('NEntries')
        stack.GetXaxis().SetTitle('')
        stack.GetXaxis().SetLabelSize(0)
        stack.GetYaxis().SetLabelFont(43)
        stack.GetYaxis().SetLabelSize(20)

        gPad.Modified()
        gPad.Update()

        #errors
        datahist.Sumw2() #stat errors

        stacktot.SetLineWidth(0) #dont show line
        stacktot.SetFillColor(r.kGray+2)
        stacktot.SetFillStyle(3002)
        stacktot.Draw("e2same")
        legend.AddEntry(stacktot, "uncertainty band", "F")
        if useshape or randseed:
            uphist.Draw("histesame")
            downhist.Draw("histesame")
        # if base != 'SR':
        datahist.Draw("psame")
        if sighist is not None:
            sighist.Draw("histesame")

        raterr = datahist.Clone("raterr")
        if useshape or randseed:
            ratup = datahist.Clone("ratup")
            ratdown = datahist.Clone("ratdown")

        #stacktot hist err = sqrt(stat*yield^2+yielderr^2)
        stackvals = []; sovbvals=[]
        for ibin in range(0, stacktot.GetNbinsX()+2):
            # print 'bin#', ibin 
            stackvals.append(stacktot.GetBinContent(ibin))
            # if showsovb:
            #     sovbvals.append(1.0+(sighist.GetBinContent(ibin)/np.sqrt(stacktot.GetBinContent(ibin))))
            # stat = stacktot.GetBinError(ibin) #stat error bars already scaled with scale
            # p = predhist.GetBinContent(ibin)
            # relyld=stackunc/stackyld
            # err = np.sqrt((stat)**2 + (relyld*p)**2)
            # stacktot.SetBinError(ibin, err)
            err = stacktot.GetBinError(ibin)
            s = stacktot.GetBinContent(ibin)
            relerr=0.0
            if s>0:
                relerr = err/s
            raterr.SetBinError(ibin, relerr)
            if useshape or randseed:
                uperr = uphist.GetBinError(ibin); downerr = downhist.GetBinError(ibin)
                u = uphist.GetBinContent(ibin); d = downhist.GetBinContent(ibin)
                # uperr = np.sqrt((upstat)**2 + (relyld*u)**2)
                # downerr = np.sqrt((downstat)**2 + (relyld*d)**2)
                uprelerr=0.0; downrelerr=0.0
                if u>0: uprelerr = uperr/u
                if d>0: downrelerr = downerr/d
                uphist.SetBinError(ibin, uperr)
                downhist.SetBinError(ibin, downerr)
                ratup.SetBinError(ibin, uprelerr)
                ratdown.SetBinError(ibin, downrelerr)
        
            # print 'stack:', 'stat',stat, 'ylderr',stackunc, 'toterr',err
            # print 'ratio:', 'relstat err', relstat, 'relylderr', relyld, 'reltoterr', relerr

        legend.Draw()
        gPad.Modified()
        gPad.Update()

        #bottom plot
        c.cd()
        p2 = r.TPad("p2", "p2", 0, 0.05, 1, 0.3)
        # p2.SetGridx()
        p2.SetGridy()
        p2.Draw()
        p2.cd()

        ratio = datahist.Clone("ratio")
        ratio.SetLineColor(r.kBlack)
        raterr.SetLineWidth(0)
        raterr.SetFillColor(r.kGray+2)
        raterr.SetFillStyle(3002)
        raterr.SetMarkerStyle(1)
        raterr.SetMinimum(0.4)
        raterr.SetMaximum(1.6)
        if useshape or randseed: 
            ratup.SetLineColor(r.kBlue)
            ratdown.SetLineColor(r.kRed)
            # ratup.SetFillStyle(0)
            # ratdown.SetFillStyle(0)
            ratup.SetMarkerStyle(20); ratdown.SetMarkerStyle(20)
        # ratio.Sumw2()

        ratio.Divide(stacktot) 
        ratio.SetMarkerStyle(20)
        if useshape or randseed:
            ratup.Divide(uphist)
            ratdown.Divide(downhist)

        #errors: stat err for data and stack error from above
        ratiovals = []; ratoffset=[]  #; ratioerr = []
        for ibin in range(0, ratio.GetNbinsX()+2):
            ratiovals.append(ratio.GetBinContent(ibin))
            ratoffset.append(abs(1.0-ratio.GetBinContent(ibin)))
            # ratioerr.append(ratio.GetBinError(ibin))
            derr = datahist.GetBinError(ibin)
            d = datahist.GetBinContent(ibin)
            relderr=0.0
            if d>0.0: relderr=derr/d
            #print 'derr/d', relderr, 'ratio', ratio.GetBinContent(ibin), 'data', datahist.GetBinContent(ibin), "pred",stacktot.GetBinContent(ibin)
            ratio.SetBinError(ibin, relderr)
            raterr.SetBinContent(ibin, 1)
            # if randseed:
            #     ratup.SetBinContent(ibin,1)
            #     ratdown.SetBinContent(ibin,1)
            if useshape or randseed:
                ###ratup = 1+ratio, ratdown = 1-ratio
                ru = ratup.GetBinContent(ibin); rd = ratdown.GetBinContent(ibin)
                rat = ratio.GetBinContent(ibin)
                ratup.SetBinContent(ibin, 1.0+(rat-ru))
                ratdown.SetBinContent(ibin, 1.0-(rd-rat))

            print 'bin', ibin, 'dataval', d, 'predval', stacktot.GetBinContent(ibin), 'd/p', ratio.GetBinContent(ibin)
        print ratiovals
        print ratoffset

        ratiovals = ratiovals[2:]
        stackvals = stackvals[2:]
        ratiovals = [0.0, 1.0]+ratiovals
        stackvals = [0.0, 0.0]+stackvals
        # print ratiovals
        tot = sum(stackvals)
        weights = np.divide(np.array(stackvals), tot)
        meani = np.divide(np.multiply( np.array(ratiovals), np.array(stackvals)),tot)
        meanisq = np.divide(np.multiply( np.multiply(np.array(ratiovals),np.array(ratiovals)), np.array(stackvals)),tot)
        mean = sum(meani)
        meansq = sum(meanisq)
        # print 'mean', mean, meansq
        rms = np.sqrt(meansq - mean**2)
        meanvals = np.full(len(weights), mean)
        rmsvals = np.full(len(weights), rms)
        meanup = meanvals
        rmsup = np.add(meanvals, rmsvals)
        rmsdown = np.subtract(meanvals, rmsvals)
        print '------------------'
        print chanid
        print 'mean', round(abs(1.0-mean),3), 'rms', round(rms,3), 'tot', round(np.sqrt((1.0-mean)**2+rms**2),3)
        # print 'tot',tot
        # print 'stackvals', stackvals
        # print weights
        # print mean
        # # print 'RMS',rms, RMS
        # print rms
        # print 'meanvals', meanvals
        # print 'rmsvals', rmsvals
        # # print 'meanup',meanup
        # # print 'meandown',meandown
        # print 'rmsup',rmsup
        # print 'rmsdown',rmsdown
        # print ratiovals
        # print 'mean +/- rms:', round(mean,3), '+/-', round(rms,3)
        # print 'Chi2 test:...'
        # datahist.Chi2Test(stacktot, "UWP")
        # print 'chi2', chi2, 'p_value', p_value
        print '------------------'

        # if showsovb:
        #     if pd.usevarbin:
        #         sovbhist = r.TH1F('sovbhist', 'sovbhist', nbins, pd.edges)
        #     else:
        #         sovbhist = r.TH1F('sovbhist', 'sovbhist', 10, 0.0, 1.0)

        meanuphist = r.TH1F('muhist', 'muhist', 10, 0.0, 1.0)
        # meandownhist = r.TH1F('mdhist', 'mdhist', 10, 0.0, 1.0)
        rmsuphist = r.TH1F('ruhist', 'ruhist', 10, 0.0, 1.0)
        rmsdownhist = r.TH1F('rdhist', 'rdhist', 10, 0.0, 1.0)
        for ibin in range(0, ratio.GetNbinsX()+2):
            # if showsovb:
            #     sovbhist.SetBinContent(ibin, sovbvals[ibin])
            meanuphist.SetBinContent(ibin, meanup[ibin])
            # meandownhist.SetBinContent(ibin, meandown[ibin])
            rmsuphist.SetBinContent(ibin, rmsup[ibin])
            rmsdownhist.SetBinContent(ibin, rmsdown[ibin])
        
        raterr.Draw("e2")
        # if args.base!='SR':
        ratio.Draw("pe0same")
        if randseed or useshape:
            ratup.Draw("histsame")
            ratdown.Draw("histsame")            
        l = r.TLine(p2.GetUxmin(), 1.0, p2.GetUxmax(), 1.0);
        l.SetLineColor(r.kBlack)
        l.SetLineWidth(1)
        l.Draw("same")
        
        # if showsovb:
        #     print 'sovbvals:',sovbvals
        #     sovbhist.SetLineColor(r.kRed)
        #     sovbhist.SetLineWidth(2)
        #     sovbhist.Draw("same")

        # meanuphist.SetLineColor(r.kRed)
        # meanuphist.SetLineWidth(2)
        # # meandownhist.SetLineColor(r.kRed)
        # rmsuphist.SetLineColor(r.kBlue)
        # rmsdownhist.SetLineColor(r.kBlue)
        # rmsuphist.SetLineWidth(2)
        # rmsdownhist.SetLineWidth(2)
        # meanuphist.Draw("same")
        # # meandownhist.Draw("same")
        # rmsuphist.Draw("same")
        # rmsdownhist.Draw("same")

        # lmean = r.TLine(p2.GetUxmin(), mean, p2.GetUxmax(), mean);
        # lmean.SetLineColor(r.kRed)
        # lmean.SetLineWidth(2)
        # lmean.Draw("same")
        # lup = r.TLine(p2.GetUxmin(), rmsup, p2.GetUxmax(), rmsup);
        # lup.SetLineColor(r.kRed)
        # lup.SetLineStyle(9)
        # lup.SetLineWidth(2)
        # lup.Draw("same")
        # ldown = r.TLine(p2.GetUxmin(), rmsdown, p2.GetUxmax(), rmsdown);
        # ldown.SetLineColor(r.kRed)
        # ldown.SetLineStyle(9)
        # ldown.SetLineWidth(2)
        # ldown.Draw("same")

        raterr.SetTitle("")
        raterr.GetYaxis().SetTitle("ratio data/prediction")
        raterr.GetYaxis().SetNdivisions(505)
        raterr.GetXaxis().SetNdivisions(505)
        raterr.GetYaxis().SetTitleSize(20)
        raterr.GetYaxis().SetTitleFont(43)
        raterr.GetYaxis().SetTitleOffset(1.55)
        raterr.GetYaxis().SetLabelFont(43)
        raterr.GetYaxis().SetLabelSize(20)

        raterr.GetXaxis().SetTitle("BDT discriminant")
        raterr.GetXaxis().SetTitleSize(20)
        raterr.GetXaxis().SetTitleFont(43)
        raterr.GetXaxis().SetTitleOffset(4.0)
        raterr.GetXaxis().SetLabelFont(43)
        raterr.GetXaxis().SetLabelSize(20)
        raterr.GetXaxis().SetTickLength(0.08)

        p2.SetTopMargin(0.1)
        p2.SetBottomMargin(0.2)
        gPad.Modified()
        gPad.Update()
        
        print 'saved histogram: ', pd.outdir+histname+'.png'
        c.SaveAs(pd.outdir+histname+'.png')
        c.SaveAs(pd.outdir+histname+'.C')

predplots(args)
