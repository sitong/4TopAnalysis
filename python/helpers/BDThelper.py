#adds event level bdt to trees
# import subprocess
from catboost import CatBoostClassifier
import os.path
import numpy as np
import pandas as pd
import ROOT as r

class BDThelper:

    def __init__(self, base='SR'):
        self.base=base
        self.bdtfile = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/eventBDTweights/newBDTwith16MC')
        modi = ['njets','nbjets','met','nbsws','metovsqrtht','ht','sfjm','restop1pt','sphericity','aplanarity','leadbpt','bjht','dphij1j2','dphib1b2','detaj1j2','detab1b2','jet7pt','meanbdisc','htratio','centrality']
        if base=='VR':
            modi = ['njets','nbjets','met','nbsws','metovsqrtht','ht','sfjm','restop1pt','sphericity','aplanarity','leadbpt','bjht','dphij1j2','dphib1b2','detaj1j2','detab1b2','jet6pt','meanbdisc','htratio','centrality']
        self.modi = modi

    def getCatBDT(self, data):
        #loads a catboost bdt file
        #data is a pandas dataframe for that event
        from_file = CatBoostClassifier()
        model = from_file.load_model(self.bdtfile)
        inputs = data[self.modi] #keep inputs needed for the bdt
        # print inputs
        #have to rename variables appropriately
        if self.base=='VR':
            inputs = inputs.rename(columns={"nbsws": "nsdw", 
                                            "restop1pt": "leadTpt1", 
                                            "njets": "nJet", 
                                            "nbjets": "nbJet", 
                                            "jet6pt":"jpt7th",
                                            "sfjm": "sumfatjetmass"})
        else:
            inputs = inputs.rename(columns={"nbsws": "nsdw", 
                                            "restop1pt": "leadTpt1", 
                                            "njets": "nJet", 
                                            "nbjets": "nbJet", 
                                            "jet7pt":"jpt7th",
                                            "sfjm": "sumfatjetmass"})
        preds = model.predict_proba(inputs)[:,1]
        return preds

    def getBDTdisc(self, evdict):
        #inputs dictionary of variable values for one event, converts it into a dataframe, and outputs a bdt discriminant 
        # data = pd.DataFrame.from_dict(evdict)
        data = pd.DataFrame([evdict])
        # print data
        BDTdisc = self.getCatBDT(data)
        # print BDTdisc
        return BDTdisc
