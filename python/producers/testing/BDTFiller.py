#fills event level bdt variables
import os
import numpy as np
import ROOT
import math
from pprint import pprint
import itertools
from collections import OrderedDict
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
import PhysicsTools.NanoTrees.helpers.LeptonHelper as lepsel
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
import PhysicsTools.NanoTrees.helpers.BDTVarHelper as bdth

from PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

from PhysicsTools.NanoTrees.producers.BaseProducer import BaseProducer, ResTopFiller
from PhysicsTools.NanoTrees.helpers.ConfigHelper import ResSetter, LepSetter, JetSetter, BoostSetter, bdtSetter
from PhysicsTools.NanoTrees.helpers.tmvahelper import TMVAHelper

class BDTFiller(BaseProducer):
    def __init__(self, channel, year, algo):
        super(BaseProducer, self).__init__()

        self.channel = channel
        self.year = year
        self.algo = algo
        self.getgentops  = False
        self.fillallvars = False
        self.verbose     = False
        self.btype = BaseProducer.BTYPE
        self.bname = BaseProducer.BWPNAME

        #general settings
        self.mu_pt, self.mu_eta, self.mu_ID, self.mu_isotype, self.mu_isoval, self.el_pt, self.el_eta, self.el_ID = LepSetter(self.channel)
        self.jpt, self.jeta, self.usejid, self.min_njets, self.bpt, self.beta, self.bWP, self.clean_jetsvleps     = JetSetter(self.channel, self.btype, self.bname, self.year)
        self.DAK8TOP_WP, self.DAK8TOP_PT, self.DAK8W_WP, self.DAK8W_PT = BoostSetter()
        self.topWP, self.minjets, self.Bimax, self.topmassrange, self.wmassrange, self.topmassparam, self.wmassparam, self.cleanAK4overlap, self.etaMax, self.minpt, self.useTopWP, self.cleanWs, self.resthenboost = ResSetter()
        self.bdt_file, self.bdt_vars = bdtSetter('nano')
        
        self.evbdt_file = bdth.eventBDT[algo]['xmlfile']
        self.evbdt_dict = bdth.eventBDT[algo]['vars']
        self.evbdt_vars = list(evbdt_dict.keys())
        
    def beginJob(self):
        super(BaseProducer, self).beginJob()
        self.tmva = TMVAHelper(self.bdt_file, self.bdt_vars)
        self.emva = TMVAHelper(self.evbdt_file, self.evbdt_vars)
        self.jh = JetHelper(self.btype, self.bWP)
        self.th = ResTopHelper(self.tmva, self.jh, self.btype)
        self.tops = ResTopFiller(self.channel, self.year)
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        if self.fillallvars:
            self.baseTrees(inputFile, outputFile, inputTree, wrappedOutputTree)

        #restop stuff
        self.tops.beginFile(inputFile, outputFile, inputTree, wrappedOutputTree)

        #variables for tree
        self.out.branch('bdtdisc', 'F')
        self.out.branch('nbstops', 'I')
        #fill each bdt variable into tree
        for key, value in evbdt_dict.items():
            typ = 'F'
            if isinstance(value, int):
                typ = 'I'
            self.out.branch(key, typ)

    def endJob(self):
        pass

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass
    
    def VarFiller(self, event):
        #get jet collections
        event.cleanjets, bjets = self.getJets(event)
        ht=0.0; metovsqht=0.0; qglsum30=0.0
        for i,j in enumerate(event.cleanjets):
            if i==0: j1=j
            if i==1: j2=j
            j2pt=j.pt; j2eta=j.eta
            ht+=j.pt
            if j.pt>=30.0:
                qglsum30+=j.qgl
        if ht>0.0:
            metovsqht=(event.met.pt)/math.sqrt(ht)

        # get boosted tops
        event.boostedTops, event.boostedWs = self.getBoosts(event)
        nbstops = len(event.boostedTops)
        #get restops, fill some restop info into trees
        nrestops, toppts = self.tops.TopFiller(event, returntopinfo=True)

        evtdict = self.evbdt_dict
        for key, value in evtdict.iteritems():
            if   key=='njets':
                value=len(event.cleanjets)
            elif key=='met':
                value=event.met.pt
            elif key=='nbjets':
                value = len(bjets)
            elif key=='ht':
                value = ht
            elif key=='qglsum30':
                value = qglsum30
            elif key=='metovsqrtht':
                value = metovsqht
            elif key=='nbsw':
                value = len(event.boostedWs)
            elif key=='j1pt':
                if len(event.cleanjets)>=1:
                    value = j1.pt
            elif key=='j2pt':
                if len(event.cleanjets)>=2:
                    value = j2.pt
            elif key=='j1eta':
                if len(event.cleanjets)>=1:
                    value = j1.eta
            elif key=='j2eta':
                if len(event.cleanjets)>=2:
                    value = j2.eta
            elif key=='t1pt':
                value = toppts[0]
            elif key=='t2pt':
                value = toppts[1]
            #fill into tree
            self.out.fillBranch(key, value)     

        self.emva.setValue(evtdict)
        evdisc =self.emva.eval()

        self.out.fillBranch("bdtdisc", evdisc)     
        self.out.fillBranch("nbstops", nbstops)
        
    def analyze(self, event):
        if self.fillallvars:
            self.EventFiller(event) #note: will be some overlap
        self.VarFiller(event)
        return True

alphaBDT = lambda : BDTFiller('0lep', '2016', 'alpha')
betaBDT  = lambda : BDTFiller('0lep', '2016', 'beta')
