#takes two rootfiles, plots ratio
import root_numpy as rn
import argparse
import os
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import ROOT as r
import random

title1 = 'new'
title2 = 'old'
file1 = './output/old/SingleLeptFromT_samplev5_Friend.root'
file2 = './output/new/SingleLeptFromT_samplev5_Skim.root'
treename1 = 'Events'
treename2 = 'Friends'
var1 = 'nrestops'
var2 = 'nrestops'
sel = ''

nbins = 5
selrange = (0,5)

def makeroot(infile, treename):
    rfile = r.TFile(infile)
    tree = rfile.Get(treename)
    return rfile, tree

f1, t1 = makeroot(file1, treename1)
f2, t2 = makeroot(file2, treename2)

#fill histograms
c = r.TCanvas("c", "canvas", 800, 800)
h1 = r.TH1D('h1', 'h1', nbins, selrange[0], selrange[1])
sel1 = "nrestops>>h1"
t1.Draw(sel1, sel)
h2 = r.TH1D('h2', 'h2', nbins, selrange[0], selrange[1])
sel2 = "nrestops>>h2"
t2.Draw(sel2, sel)#, sel)

pad1 = r.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
pad1.SetBottomMargin(0)
pad1.SetGridx()
pad1.Draw()
pad1.cd()
h1.SetStats(0)
h1.Draw()
h2.Draw("same")

h1.GetYaxis().SetLabelSize(0.0)
axis = r.TGaxis(-5, 20, -5, 220, 20,220,510,"")
axis.SetLabelFont(43)
axis.SetLabelSize(15)
axis.Draw()

c.cd()
pad2 = r.TPad("pad2", "pad2", 0, 0.05, 1, 1.0)
pad2.SetTopMargin(0)
pad2.SetBottomMargin(0.2)
pad2.SetGridx()
pad2.Draw()
pad2.cd()

#ratio plot
h3 = h1.Clone("h1")
h3.SetLineColor(kBlack)
h3.SetMinimum(0.8)
h3.SetMaximum(1.2)
h3.Sumw2()
h3.SetStats(0)
h3.Divide(h2)
h3.SetMarkerStype(21)
h3.Draw()

#asthetics
h1.SetLineColor(kBlue+1)
h1.SetLineWidth(2)
h2.SetLineColor(kRed)
h2.SetLineWidth(2)

h1.GetYaxis().SetTiltleSize(20)
h1.GetYaxis().SetTiltleFont(43)
h1.GetYaxis().SetTiltleOffset(1.55)

h3.SetTitle("")
h3.GetYaxis().SetTitle("ratio "+title1+"/"+title2)
h3.GetYaxis().SetNdivisions(505)
h3.GetYaxis().SetTitleSize(20)
h3.GetYaxis().SetTitleFont(43)
h3.GetYaxis().SetTitleOffset(1.55)
h3.GetYaxis().SetLabelFont(43)
h3.GetYaxis().SetLabelSize(15)

h3.GetXaxis().SetTitleSize(20)
h3.GetXaxis().SetTitleFont(43)
h3.GetXaxis().SetTitleOffset(4.0)
h3.GetXaxis().SetLabelFont(43)
h3.GetXaxis().SetLabelSize(15)
c.SaveAs("RATIO.jpg")
