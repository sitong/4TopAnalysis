import os
import numpy as np
import math
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
import ROOT as r

class bSFcorrHelper:

    def __init__(self, year, ismc = True):
        self.year = year
        self.systlist = ['nosys', 'jesUp', 'jesDown', 'jerUp', 'jerDown', 'btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down']

        # self.corrhistfile = histfile
        self.ismc = ismc
        #identify proc based on infile
        self.procs = {"TTTT":                  'TTTT',
                      "TTbb_":                 'TT',
                      "TTTo":                  'TT',
                      "TT_":                   'TT',
                      "ST_":                   'minor',
                      "TTG":                   'TTX',
                      "TTWJets":               'TTX',
                      "TTZ":                   'TTX',
                      "tZq":                   'minor',
                      "ttH":                   'TTX',
                      "WJets":                 'minor',
                      "DY":                    'minor',
                      "WW_":                   'minor',
                      "WZ_":                   'minor',
                      "ZZ_":                   'minor',
                      "ZJets":                 'minor'}

    def getbSFcorrhists(self, infile):
        bSFcorr = 1.0
        filestr=str(infile)
        corrhistfile = r.TFile(os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/bSFcorrFiles/bSFcorrections.root'))
        #get process
        proc = None
        if self.ismc:
            for key in self.procs:
                if key in filestr:
                    proc = self.procs[key]
        # print 'bcproc',proc 
        if not self.ismc:
            return bSFcorr # don't calculate for data
        if proc is None:
            return bSFcorr #proc not calculated, QCD or TT for example
        corrhists = {} 
        for syst in self.systlist:
            # print "corr"+proc+self.year+'_'+syst
            hist = corrhistfile.Get("corr"+proc+self.year+'_'+syst)
            corrhists["corr"+proc+self.year+'_'+syst] = hist
            hist.SetDirectory(0)
        corrhistfile.Close()
        return proc, corrhists

    def getbSFcorrs(self, evt, proc, corrhists):
        SFs = {}
        htnj = {}
        htnj = {"corr"+proc+self.year+'_nosys':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_jesUp':[evt.njets_jesUp, evt.ht_jesUp],
                "corr"+proc+self.year+'_jesDown':[evt.njets_jesDown, evt.ht_jesDown],
                "corr"+proc+self.year+'_jerUp':[evt.njets_jerUp, evt.ht_jerUp],
                "corr"+proc+self.year+'_jerDown':[evt.njets_jerDown, evt.ht_jerDown],
                "corr"+proc+self.year+'_btagLFUp':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagLFDown':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagHFUp':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagHFDown':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagLFstats1Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagLFstats1Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagHFstats1Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagHFstats1Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagLFstats2Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagLFstats2Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagHFstats2Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagHFstats2Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagCFerr1Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagCFerr1Down':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagCFerr2Up':[evt.njets_nosys, evt.ht_nosys],
                "corr"+proc+self.year+'_btagCFerr2Down':[evt.njets_nosys, evt.ht_nosys]}

        for name, hist in corrhists.iteritems():
            nj = htnj[name][0]
            ht = htnj[name][1]
            if nj>=11:
                nj=12 #put in that bin
            if ht>=1450.0:
                ht=1500.0 #put in that bin
            sf = self.get_sf(hist, nj, ht) # x is NJ y is HT
            SFs[name] = sf[0]
        return SFs

    def get_sf(self, hist, var1, var2, syst=None):
        # `eta` refers to the first binning var, `pt` refers to the second binning var
        x, y = var1, var2
        SF = np.ones(3)
        SF = self.getBinContent2D(hist, x, y)
        if syst is not None:
            stat = SF[1] - SF[0]
            unc  = np.sqrt(syst*syst+stat*stat)
            SF[1] = SF[0] + unc
            SF[2] = SF[0] - unc
        return SF

    def getBinContent2D(self,  hist, x, y):
        bin_x0 = hist.GetXaxis().FindFixBin(x)
        bin_y0 = hist.GetYaxis().FindFixBin(y)
        binX = np.clip(bin_x0, 1, hist.GetNbinsX()+1) #+1 is valentina's correction
        binY = np.clip(bin_y0, 1, hist.GetNbinsY()+1) #requires that bin values are greater than 1
        if binX>hist.GetNbinsX(): ##? set to last bin
            binX=hist.GetNbinsX()
        if binY>hist.GetNbinsY():
            binY=hist.GetNbinsY()
        val = hist.GetBinContent(binX, binY)
        # print 'val', val, 'binX', binX, 'binY', binY, 'last bin:', hist.GetNbinsX(), hist.GetNbinsY()
        err = hist.GetBinError(binX, binY)
        return np.array([val, val + err, val - err])


