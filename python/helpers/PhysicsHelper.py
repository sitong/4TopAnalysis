import ROOT
import numpy as np
import logging
from ROOT import  TMatrixD, TArrayD, TMatrixDEigen, TVector3#, Math
from scipy.special import eval_legendre


#helpful formulas

def deltaR(eta1, phi1, eta2, phi2):
    deta = eta1 - eta2
    dphi = ROOT.TVector2.Phi_mpi_pi(phi1 - phi2)#check
    deltaR2 = deta*deta + dphi*dphi
    deltaR = np.sqrt(abs(deltaR2))
    return deltaR

#calculates deltaR^2 from first to second argument
def deltaR2(eta1, phi1, eta2, phi2):
    deta = eta1 - eta2
    dphi = ROOT.TVector2.Phi_mpi_pi(phi1 - phi2)#check
    deltaR2 = deta*deta + dphi*dphi
    return deltaR2

def HT( Jets, minPT=0, maxEta=9999):#selected?
    ht = 0.0
    for jet in Jets:
        if (jet.pt>minPT and abs(jet.eta)<maxEta):
            ht += jet.pt
    return ht
    
#compares child to parents in a vector. finds index of child that is closest (within not vetoed and with minpt)
def FindNearestMom(child, parents, bestDist, maxDR2):   
    nKids = len(parents)#or num of daughters
    bestInd = -1
    bestDist = maxDR2
    for ind, mom in enumerate(parents):
        # if vetoed and vetoed[ind]: continue
        # if mom.pt < minpt: continue
        dist = deltaR2(child, mom)#or daughter
        if (dist < bestDist):
            bestInd = ind
            bestDist = dist
    return bestInd

def PadArray(arr, L, padnum=0):
    paddy = L-len(arr)
    if paddy>0:
        print 'WARNING: padding array len:', len(arr)
        arr = np.pad(arr, (0,paddy), 'constant', constant_values=padnum)
        return arr
    elif paddy==0:
        return arr
    elif paddy<0:
        print 'ERROR: array longer than expected! Expected:',L, 'len:', len(arr) 
        return False

#finds object in collection that is the closest neighbor to an object
#returns the index and dist of that nearest neighbor and if it falls within min radius
def FindNearestNeighbor(obj, collection,  minDist=0.4):
    inminDist = False
    bestInd = -1; bestDist = 100000#big num
    for ind,cob in enumerate(collection):
        dist = deltaR(obj.eta, obj.phi, cob.eta, cob.phi)
        if dist < bestDist:
            bestDist = dist
            bestInd = ind
        if bestDist<=minDist:
            inminDist = True
    return bestInd, bestDist, inminDist

#finds neighbors in collection that are within a too-close min deltaR. default:0.4
def ExcludeCloseNeighbors(obj, collection, minDR=0.4, returnbools=False):
    #returnbools tells it to return array of bools(pass or not) vs the cleaned jet collection
    goodJet=True
    closeNbrs=[]
    for ind,cob in enumerate(collection):
        dist = deltaR(obj.eta, obj.phi, cob.eta, cob.phi)
        if dist<minDR:
            goodJet=False
        elif dist>=minDR:
            goodJet=True
            if not returnbools:
                closeNbrs.append(cob) #array with index, object, distance
        if returnbools:
            closeNbrs.append(goodJet)
    return np.array(closeNbrs)

#finds neighbors in collection that are within a too-close min deltaR. default:0.4
def InvExcludeCloseNeighbors(obj, collection, minDR=0.4):
    #returnbools tells it to return array of bools(pass or not) vs the cleaned jet collection
    goodJet=True
    for ind,cob in enumerate(collection):
        dist = deltaR(obj.eta, obj.phi, cob.eta, cob.phi)
        if dist<minDR:
            goodJet=False
        elif dist>=minDR:
            goodJet=True
    return goodJet

# def Calc_FoxWolframMoment(l, minj, event, jetlist):
def Calc_FoxWolframMoment(l, minj, jetlist):
    wfm = 0.
    ptsum = 0.
    for i in range(minj, len(jetlist)):
        j_idx1 = jetlist[i]#ith jet
        # ptsum += event.Jet_pt[j_idx1]
        ptsum += j_idx1.pt
        
        for j in range(minj, len(jetlist)):
            j_idx2 = jetlist[j]
            j_vec1 = TVector3()
            j_vec2 = TVector3()
            # j_vec1.SetXYZ(event.Jet_pt[j_idx1], event.Jet_eta[j_idx1], event.Jet_phi[j_idx1])
            # j_vec2.SetXYZ(event.Jet_pt[j_idx2], event.Jet_eta[j_idx2], event.Jet_phi[j_idx2])
            j_vec1.SetXYZ(j_idx1.pt, j_idx1.eta, j_idx1.phi)
            j_vec2.SetXYZ(j_idx2.pt, j_idx2.eta, j_idx2.phi)
            cosOmega = j_vec1.Dot(j_vec2)/(j_vec1.Mag()*j_vec2.Mag())
            if cosOmega > 1.0: cosOmega = 1.0
            if cosOmega <-1.0: cosOmega = -1.0
            # Wij =  event.Jet_pt[j_idx1] * event.Jet_pt[j_idx2]
            Wij =  j_idx1.pt * j_idx2.pt
            # wfm += Wij * Math.legendre(l, cosOmega) #library issues, using scipy version
            wfm += Wij*eval_legendre(l, cosOmega)
    WFM = wfm/(ptsum**2)

    return WFM

def EventShape(cleanjetlist):
    xx = 0 ; yy = 0 ; zz = 0 ;xy = 0 ; xz = 0 ; yz = 0
    nJ = len(cleanjetlist)
    if nJ<1:
        return 0.0, 0.0
    for j in cleanjetlist:
        px = np.cos(j.phi)
        py = np.sin(j.phi)
        pz = np.sinh(j.eta)
        mag2 = px**2 + py**2 + pz**2
        xx += px*px/mag2 ; yy += py*py/mag2 ; zz += pz*pz/mag2
        xy += px*py/mag2 ; xz += px*pz/mag2 ; yz += py*pz/mag2

    matrix = TMatrixD(3,3)
    data = TArrayD(9)
    elements = [xx,xy,xz,xy,yy,yz,xz,yz,zz]
    
    for i in range(9):
        data[i] = elements[i] 
    matrix.SetMatrixArray(data.GetArray())
    matD=TMatrixDEigen(matrix)
    ei_mat=matD.GetEigenValues()

    lam2 = ei_mat(1,1)/float(nJ) 
    lam3 = ei_mat(2,2)/float(nJ)
    Sph = 1.5*(lam2 + lam3)
    Apl = 1.5*lam3

    return Sph, Apl
