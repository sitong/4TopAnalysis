import numpy as np
from collections import OrderedDict


def getNAFerrors(NAFfile):
    
    #dictionary of total error values calculated from prop uncertainties in extABCD method 
    NAFdict = { './NAF_VR_SR_before_combine/NAF_SR_2016_before_combine.root':  {'cut0bin0':[1615.131, 67.799],
                                                                               'cut0bin1':[1562.481, 74.815],
                                                                               'cut0bin2':[1181.7, 67.007],
                                                                               'cut0bin3':[1051.773, 71.268],
                                                                               'cut0bin4':[700.215, 57.475],
                                                                               'cut0bin5':[495.65, 51.552],
                                                                               'cut0bin6':[686.641, 67.088],
                                                                               'cut0bin7':[753.188, 83.088],
                                                                               'cut1bin0':[249.856, 30.501],
                                                                               'cut1bin1':[224.2, 51.392],
                                                                               'cut2bin0':[605.567, 53.035],
                                                                               'cut2bin1':[242.962, 68.323]
                                                                           },
               './NAF_VR_SR_before_combine/NAF_SR_2017_before_combine.root':  {'cut0bin0':[2113.445, 84.614],
                                                                               'cut0bin1':[1976.126, 89.029],
                                                                               'cut0bin2':[1391.696, 73.165],
                                                                               'cut0bin3':[1239.76, 76.521],
                                                                               'cut0bin4':[895.121, 66.868],
                                                                               'cut0bin5':[605.95, 55.211],
                                                                               'cut0bin6':[818.924, 72.591],
                                                                               'cut0bin7':[758.176, 78.274],
                                                                               'cut1bin0':[430.671, 45.841],
                                                                               'cut1bin1':[427.129, 82.361],
                                                                               'cut2bin0':[745.827, 62.514],
                                                                               'cut2bin1':[216.806, 52.457]
                                                                           },
               './NAF_VR_SR_before_combine/NAF_SR_2018_before_combine.root':  {'cut0bin0':[2844.721, 98.315],
                                                                               'cut0bin1':[2562.637, 99.223],
                                                                               'cut0bin2':[2155.301, 96.404],
                                                                               'cut0bin3':[1701.409, 91.311],
                                                                               'cut0bin4':[1351.45, 88.385],
                                                                               'cut0bin5':[1063.247, 85.036],
                                                                               'cut0bin6':[1245.621, 96.656],
                                                                               'cut0bin7':[1036.99, 94.63],
                                                                               'cut1bin0':[566.176, 51.083],
                                                                               'cut1bin1':[432.965, 75.162],
                                                                               'cut2bin0':[828.831, 62.232],
                                                                               'cut2bin1':[317.946, 69.081]
                                                                           }}

    if NAFfile in NAFdict.keys():
        # print NAFdict[NAFfile]
        return NAFdict[NAFfile] #returns dict of errors
    else: 
        print NAFfile, 'errors not found. '
        return None
