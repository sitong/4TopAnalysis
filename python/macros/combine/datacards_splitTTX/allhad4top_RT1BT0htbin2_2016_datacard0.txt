## card: datacards_splitTTX/allhad4top_RT1BT0htbin2_2016_datacard0.txt number in set: 0
##selection: (nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=900 && ht_nosys<1000 &&  nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag))*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*36.33*prefirewgt*

#----------------------------------------------#
#generating datacards. name: allhad4top, year2016

#number of cards in set: 12

## number of signals (i), backgrounds (j), and uncertainties (k)
---
imax: 1
jmax: 5
kmax: *
---

## input the bdt shape histograms
---
shapes * * allhad4top_2016_shapehists.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
---

## list the bin label and the number of (data) events observed
---
bin  RT1BT0htbin2_2016
observation 1239.0
---

## expected events for signal and all backgrounds in the bins
---
bin        RT1BT0htbin2_2016    RT1BT0htbin2_2016    RT1BT0htbin2_2016    RT1BT0htbin2_2016    RT1BT0htbin2_2016    RT1BT0htbin2_2016
process    TTTT    TTH    TTW    TTOther    other    DDBKG
process        0        1        2        3        4        5
rate       2.50778510794    13.7362927794    1.83124899119    8.65530223399    32.6243145466    1076.445
---

## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin
---
## statistical uncertainties

RT1BT0htbin2_2016_TTTT_stat      lnN    1.02295982508    -    -    -    -    -
RT1BT0htbin2_2016_TTH_stat      lnN    -    1.00990313029    -    -    -    -
RT1BT0htbin2_2016_TTW_stat      lnN    -    -    1.26341980385    -    -    -
RT1BT0htbin2_2016_TTOther_stat      lnN    -    -    -    1.20570425234    -    -
RT1BT0htbin2_2016_other_stat      lnN    -    -    -    -    1.07276389041    -
RT1BT0htbin2_2016_DDBKG_stat      lnN    -    -    -    -    -    1.05909544844

---
## systematic uncertainties

lumi_uncorr_2016   lnN    1.010    1.010    1.010    1.010    1.010    -    
lumi_corr16_17_18   lnN    1.006    1.006    1.006    1.006    1.006    -    
elveto_2016   lnN    1.04548     1.01689     1.01689     1.01689    -    -    
muveto_2016   lnN    1.00924     1.00019     1.00019     1.00019    -    -    
prefire_2016   lnN    1.00452     1.00364     1.00364     1.00364    -    -    
cross_section   lnU   -   1.50   1.23   1.23     -    -    
data_pred_norm_disagreement_2016    lnN        -      -         -         -         -      1.120
data_pred_shape_disagreement_2016  shape   -    -    -    -    -    1.0  

---
## shape uncertainties

jes_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
jer_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
pileup_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
trigger_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
btagLF  shape     1.0    1.0    1.0    1.0    1.0    - 
btagHF  shape     1.0    1.0    1.0    1.0    1.0    - 
btagLFstats1_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
btagHFstats1_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
btagLFstats2_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
btagHFstats2_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
btagCFerr1  shape     1.0    1.0    1.0    1.0    1.0    - 
btagCFerr2  shape     1.0    1.0    1.0    1.0    1.0    - 
DeepAK8TopSF_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
DeepAK8WSF_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
isr_TTTT  shape     1.0    -    -   -    -    -  
isr_TTH  shape     -    1.0    -    -    -    -  
isr_TTW  shape     -    -    1.0     -    -    -  
fsr_TTTT  shape     1.0    -    -   -    -    -  
fsr_TTH  shape     -    1.0    -    -    -    -  
fsr_TTW  shape     -    -    1.0     -    -    -  
pdf  shape     1.0    1.0    1.0      -     -     - 
ME_TTTT  shape     1.0    -    -   -    -    -  
ME_TTH  shape     -    1.0    -    -    -    -  
ME_TTW  shape     -    -    1.0     -    -    -  
ResTopEff_2016   shape     1.0    1.0   1.0   1.0   1.0    - 
ResTopMiss_2016   shape     1.0    1.0   1.0   1.0   1.0    - 

---

RT1BT0htbin2_2016 autoMCStats 0 1
---

