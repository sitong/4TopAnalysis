void ALL_VR_cut0bin7_catBDTdisc_nosys()
{
//=========Macro generated from canvas: c/c
//=========  (Wed Sep  7 04:02:00 2022) by ROOT version 6.12/07
   TCanvas *c = new TCanvas("c", "c",0,0,800,800);
   gStyle->SetOptStat(0);
   c->SetHighLightColor(2);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "p1",0,0.3,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-0.125,-70.66145,1.125,635.953);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("");
   stack->SetMaximum(538.3729);
   Double_t xAxis91[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *stack_stack_11 = new TH1F("stack_stack_11","",9, xAxis91);
   stack_stack_11->SetMinimum(0);
   stack_stack_11->SetMaximum(565.2916);
   stack_stack_11->SetDirectory(0);
   stack_stack_11->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_11->SetLineColor(ci);
   stack_stack_11->GetXaxis()->SetNdivisions(505);
   stack_stack_11->GetXaxis()->SetLabelFont(42);
   stack_stack_11->GetXaxis()->SetLabelSize(0);
   stack_stack_11->GetXaxis()->SetTitleSize(0.035);
   stack_stack_11->GetXaxis()->SetTitleFont(42);
   stack_stack_11->GetYaxis()->SetTitle("NEntries");
   stack_stack_11->GetYaxis()->SetLabelFont(43);
   stack_stack_11->GetYaxis()->SetLabelSize(20);
   stack_stack_11->GetYaxis()->SetTitleSize(20);
   stack_stack_11->GetYaxis()->SetTitleOffset(1.55);
   stack_stack_11->GetYaxis()->SetTitleFont(43);
   stack_stack_11->GetZaxis()->SetLabelFont(42);
   stack_stack_11->GetZaxis()->SetLabelSize(0.035);
   stack_stack_11->GetZaxis()->SetTitleSize(0.035);
   stack_stack_11->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_11);
   
   Double_t xAxis92[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *tttt_stack_1 = new TH1F("tttt_stack_1","hist",9, xAxis92);
   tttt_stack_1->SetBinContent(1,0.0004647718);
   tttt_stack_1->SetBinContent(2,0.004531504);
   tttt_stack_1->SetBinContent(3,0.004454357);
   tttt_stack_1->SetBinContent(4,0.0162117);
   tttt_stack_1->SetBinContent(5,0.02091091);
   tttt_stack_1->SetBinContent(6,0.04218448);
   tttt_stack_1->SetBinContent(7,0.07899698);
   tttt_stack_1->SetBinContent(8,0.1534908);
   tttt_stack_1->SetBinContent(9,0.1777542);
   tttt_stack_1->SetBinError(1,0.0005895855);
   tttt_stack_1->SetBinError(2,0.001628395);
   tttt_stack_1->SetBinError(3,0.001900404);
   tttt_stack_1->SetBinError(4,0.003192872);
   tttt_stack_1->SetBinError(5,0.004139259);
   tttt_stack_1->SetBinError(6,0.005552392);
   tttt_stack_1->SetBinError(7,0.006912927);
   tttt_stack_1->SetBinError(8,0.009709377);
   tttt_stack_1->SetBinError(9,0.01066355);
   tttt_stack_1->SetEntries(8898);
   tttt_stack_1->SetStats(0);

   ci = TColor::GetColor("#a1794a");
   tttt_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#a1794a");
   tttt_stack_1->SetLineColor(ci);
   tttt_stack_1->GetXaxis()->SetLabelFont(42);
   tttt_stack_1->GetXaxis()->SetLabelSize(0.035);
   tttt_stack_1->GetXaxis()->SetTitleSize(0.035);
   tttt_stack_1->GetXaxis()->SetTitleFont(42);
   tttt_stack_1->GetYaxis()->SetLabelFont(42);
   tttt_stack_1->GetYaxis()->SetLabelSize(0.035);
   tttt_stack_1->GetYaxis()->SetTitleSize(0.035);
   tttt_stack_1->GetYaxis()->SetTitleOffset(0);
   tttt_stack_1->GetYaxis()->SetTitleFont(42);
   tttt_stack_1->GetZaxis()->SetLabelFont(42);
   tttt_stack_1->GetZaxis()->SetLabelSize(0.035);
   tttt_stack_1->GetZaxis()->SetTitleSize(0.035);
   tttt_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(tttt_stack_1,"");
   Double_t xAxis93[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *ttHpLttV_stack_2 = new TH1F("ttHpLttV_stack_2","hist",9, xAxis93);
   ttHpLttV_stack_2->SetBinContent(1,0.3314691);
   ttHpLttV_stack_2->SetBinContent(2,0.3481791);
   ttHpLttV_stack_2->SetBinContent(3,0.5149326);
   ttHpLttV_stack_2->SetBinContent(4,1.904979);
   ttHpLttV_stack_2->SetBinContent(5,1.710756);
   ttHpLttV_stack_2->SetBinContent(6,2.752543);
   ttHpLttV_stack_2->SetBinContent(7,4.031752);
   ttHpLttV_stack_2->SetBinContent(8,3.667469);
   ttHpLttV_stack_2->SetBinContent(9,2.776246);
   ttHpLttV_stack_2->SetBinError(1,0.1621359);
   ttHpLttV_stack_2->SetBinError(2,0.2718435);
   ttHpLttV_stack_2->SetBinError(3,0.4310299);
   ttHpLttV_stack_2->SetBinError(4,0.5174842);
   ttHpLttV_stack_2->SetBinError(5,0.4632072);
   ttHpLttV_stack_2->SetBinError(6,0.6197966);
   ttHpLttV_stack_2->SetBinError(7,0.9696828);
   ttHpLttV_stack_2->SetBinError(8,0.7489935);
   ttHpLttV_stack_2->SetBinError(9,0.4913055);
   ttHpLttV_stack_2->SetEntries(8487);
   ttHpLttV_stack_2->SetStats(0);

   ci = TColor::GetColor("#163d4e");
   ttHpLttV_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#163d4e");
   ttHpLttV_stack_2->SetLineColor(ci);
   ttHpLttV_stack_2->GetXaxis()->SetLabelFont(42);
   ttHpLttV_stack_2->GetXaxis()->SetLabelSize(0.035);
   ttHpLttV_stack_2->GetXaxis()->SetTitleSize(0.035);
   ttHpLttV_stack_2->GetXaxis()->SetTitleFont(42);
   ttHpLttV_stack_2->GetYaxis()->SetLabelFont(42);
   ttHpLttV_stack_2->GetYaxis()->SetLabelSize(0.035);
   ttHpLttV_stack_2->GetYaxis()->SetTitleSize(0.035);
   ttHpLttV_stack_2->GetYaxis()->SetTitleOffset(0);
   ttHpLttV_stack_2->GetYaxis()->SetTitleFont(42);
   ttHpLttV_stack_2->GetZaxis()->SetLabelFont(42);
   ttHpLttV_stack_2->GetZaxis()->SetLabelSize(0.035);
   ttHpLttV_stack_2->GetZaxis()->SetTitleSize(0.035);
   ttHpLttV_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(ttH+ttV_stack_2,"");
   Double_t xAxis94[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *minor_stack_3 = new TH1F("minor_stack_3","hist",9, xAxis94);
   minor_stack_3->SetBinContent(1,0.8701121);
   minor_stack_3->SetBinContent(2,2.120218);
   minor_stack_3->SetBinContent(3,6.381641);
   minor_stack_3->SetBinContent(4,6.952455);
   minor_stack_3->SetBinContent(5,9.162741);
   minor_stack_3->SetBinContent(6,11.81121);
   minor_stack_3->SetBinContent(7,14.84983);
   minor_stack_3->SetBinContent(8,14.95168);
   minor_stack_3->SetBinContent(9,8.267107);
   minor_stack_3->SetBinError(1,0.3967508);
   minor_stack_3->SetBinError(2,0.493793);
   minor_stack_3->SetBinError(3,1.255983);
   minor_stack_3->SetBinError(4,1.051715);
   minor_stack_3->SetBinError(5,1.241805);
   minor_stack_3->SetBinError(6,1.509786);
   minor_stack_3->SetBinError(7,1.701534);
   minor_stack_3->SetBinError(8,1.853761);
   minor_stack_3->SetBinError(9,1.66428);
   minor_stack_3->SetEntries(1173);
   minor_stack_3->SetStats(0);

   ci = TColor::GetColor("#d07e93");
   minor_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#d07e93");
   minor_stack_3->SetLineColor(ci);
   minor_stack_3->GetXaxis()->SetLabelFont(42);
   minor_stack_3->GetXaxis()->SetLabelSize(0.035);
   minor_stack_3->GetXaxis()->SetTitleSize(0.035);
   minor_stack_3->GetXaxis()->SetTitleFont(42);
   minor_stack_3->GetYaxis()->SetLabelFont(42);
   minor_stack_3->GetYaxis()->SetLabelSize(0.035);
   minor_stack_3->GetYaxis()->SetTitleSize(0.035);
   minor_stack_3->GetYaxis()->SetTitleOffset(0);
   minor_stack_3->GetYaxis()->SetTitleFont(42);
   minor_stack_3->GetZaxis()->SetLabelFont(42);
   minor_stack_3->GetZaxis()->SetLabelSize(0.035);
   minor_stack_3->GetZaxis()->SetTitleSize(0.035);
   minor_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(minor_stack_3,"");
   Double_t xAxis95[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *predhist_stack_4 = new TH1F("predhist_stack_4","hist",9, xAxis95);
   predhist_stack_4->SetBinContent(1,24.81555);
   predhist_stack_4->SetBinContent(2,65.34306);
   predhist_stack_4->SetBinContent(3,153.0708);
   predhist_stack_4->SetBinContent(4,202.0794);
   predhist_stack_4->SetBinContent(5,249.788);
   predhist_stack_4->SetBinContent(6,286.3271);
   predhist_stack_4->SetBinContent(7,290.2266);
   predhist_stack_4->SetBinContent(8,301.6874);
   predhist_stack_4->SetBinContent(9,136.224);
   predhist_stack_4->SetBinContent(10,0.1112014);
   predhist_stack_4->SetBinError(1,3.865824);
   predhist_stack_4->SetBinError(2,9.371796);
   predhist_stack_4->SetBinError(3,21.06627);
   predhist_stack_4->SetBinError(4,27.93413);
   predhist_stack_4->SetBinError(5,34.48935);
   predhist_stack_4->SetBinError(6,39.1183);
   predhist_stack_4->SetBinError(7,39.46143);
   predhist_stack_4->SetBinError(8,41.27111);
   predhist_stack_4->SetBinError(9,20.03395);
   predhist_stack_4->SetBinError(10,0.1156599);
   predhist_stack_4->SetEntries(14758);
   predhist_stack_4->SetStats(0);

   ci = TColor::GetColor("#c1caf3");
   predhist_stack_4->SetFillColor(ci);

   ci = TColor::GetColor("#c1caf3");
   predhist_stack_4->SetLineColor(ci);
   predhist_stack_4->GetXaxis()->SetLabelFont(42);
   predhist_stack_4->GetXaxis()->SetLabelSize(0.035);
   predhist_stack_4->GetXaxis()->SetTitleSize(0.035);
   predhist_stack_4->GetXaxis()->SetTitleFont(42);
   predhist_stack_4->GetYaxis()->SetLabelFont(42);
   predhist_stack_4->GetYaxis()->SetLabelSize(0.035);
   predhist_stack_4->GetYaxis()->SetTitleSize(0.035);
   predhist_stack_4->GetYaxis()->SetTitleOffset(0);
   predhist_stack_4->GetYaxis()->SetTitleFont(42);
   predhist_stack_4->GetZaxis()->SetLabelFont(42);
   predhist_stack_4->GetZaxis()->SetLabelSize(0.035);
   predhist_stack_4->GetZaxis()->SetTitleSize(0.035);
   predhist_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(predhist_stack_4,"");
   stack->Draw("hist");
   Double_t xAxis96[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *stacktot__41 = new TH1F("stacktot__41","stacktot",9, xAxis96);
   stacktot__41->SetBinContent(1,26.0176);
   stacktot__41->SetBinContent(2,67.81599);
   stacktot__41->SetBinContent(3,159.9718);
   stacktot__41->SetBinContent(4,210.953);
   stacktot__41->SetBinContent(5,260.6824);
   stacktot__41->SetBinContent(6,300.933);
   stacktot__41->SetBinContent(7,309.1871);
   stacktot__41->SetBinContent(8,320.4601);
   stacktot__41->SetBinContent(9,147.4451);
   stacktot__41->SetBinContent(10,0.1112014);
   stacktot__41->SetBinError(1,3.889511);
   stacktot__41->SetBinError(2,9.388732);
   stacktot__41->SetBinError(3,21.10807);
   stacktot__41->SetBinError(4,27.95871);
   stacktot__41->SetBinError(5,34.51481);
   stacktot__41->SetBinError(6,39.15233);
   stacktot__41->SetBinError(7,39.51);
   stacktot__41->SetBinError(8,41.31951);
   stacktot__41->SetBinError(9,20.10896);
   stacktot__41->SetBinError(10,0.1156599);
   stacktot__41->SetEntries(33316);
   stacktot__41->SetStats(0);

   ci = TColor::GetColor("#666666");
   stacktot__41->SetFillColor(ci);
   stacktot__41->SetFillStyle(3002);

   ci = TColor::GetColor("#000099");
   stacktot__41->SetLineColor(ci);
   stacktot__41->SetLineWidth(0);
   stacktot__41->GetXaxis()->SetLabelFont(42);
   stacktot__41->GetXaxis()->SetLabelSize(0.035);
   stacktot__41->GetXaxis()->SetTitleSize(0.035);
   stacktot__41->GetXaxis()->SetTitleFont(42);
   stacktot__41->GetYaxis()->SetLabelFont(42);
   stacktot__41->GetYaxis()->SetLabelSize(0.035);
   stacktot__41->GetYaxis()->SetTitleSize(0.035);
   stacktot__41->GetYaxis()->SetTitleOffset(0);
   stacktot__41->GetYaxis()->SetTitleFont(42);
   stacktot__41->GetZaxis()->SetLabelFont(42);
   stacktot__41->GetZaxis()->SetLabelSize(0.035);
   stacktot__41->GetZaxis()->SetTitleSize(0.035);
   stacktot__41->GetZaxis()->SetTitleFont(42);
   stacktot__41->Draw("e2same");
   Double_t xAxis97[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *datahist__42 = new TH1F("datahist__42","hist",9, xAxis97);
   datahist__42->SetBinContent(1,18);
   datahist__42->SetBinContent(2,75);
   datahist__42->SetBinContent(3,129);
   datahist__42->SetBinContent(4,156);
   datahist__42->SetBinContent(5,217);
   datahist__42->SetBinContent(6,258);
   datahist__42->SetBinContent(7,276);
   datahist__42->SetBinContent(8,262);
   datahist__42->SetBinContent(9,121);
   datahist__42->SetBinError(1,4.242641);
   datahist__42->SetBinError(2,8.660254);
   datahist__42->SetBinError(3,11.35782);
   datahist__42->SetBinError(4,12.49);
   datahist__42->SetBinError(5,14.73092);
   datahist__42->SetBinError(6,16.06238);
   datahist__42->SetBinError(7,16.61325);
   datahist__42->SetBinError(8,16.18641);
   datahist__42->SetBinError(9,11);
   datahist__42->SetEntries(1512);
   datahist__42->SetStats(0);
   datahist__42->SetLineWidth(2);
   datahist__42->SetMarkerStyle(20);
   datahist__42->GetXaxis()->SetLabelFont(42);
   datahist__42->GetXaxis()->SetLabelSize(0.035);
   datahist__42->GetXaxis()->SetTitleSize(0.035);
   datahist__42->GetXaxis()->SetTitleFont(42);
   datahist__42->GetYaxis()->SetLabelFont(42);
   datahist__42->GetYaxis()->SetLabelSize(0.035);
   datahist__42->GetYaxis()->SetTitleSize(0.035);
   datahist__42->GetYaxis()->SetTitleOffset(0);
   datahist__42->GetYaxis()->SetTitleFont(42);
   datahist__42->GetZaxis()->SetLabelFont(42);
   datahist__42->GetZaxis()->SetLabelSize(0.035);
   datahist__42->GetZaxis()->SetTitleSize(0.035);
   datahist__42->GetZaxis()->SetTitleFont(42);
   datahist__42->Draw("psame");
   
   TLegend *leg = new TLegend(0.1,0.6,0.3,0.9,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("datahist","data","L");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("tttt_stack_1","tttt","F");

   ci = TColor::GetColor("#a1794a");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#a1794a");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("ttH+ttV_stack_2","ttH+ttV","F");

   ci = TColor::GetColor("#163d4e");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#163d4e");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("minor_stack_3","minor","F");

   ci = TColor::GetColor("#d07e93");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#d07e93");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("predhist_stack_4","QCD+tt (DD)","F");

   ci = TColor::GetColor("#c1caf3");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#c1caf3");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("stacktot","uncertainty band","F");

   ci = TColor::GetColor("#666666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(3002);

   ci = TColor::GetColor("#000099");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "p2",0,0.05,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-0.125,0.05714285,1.125,1.771429);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetGridy();
   p2->SetBottomMargin(0.2);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   Double_t xAxis98[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *raterr__43 = new TH1F("raterr__43","",9, xAxis98);
   raterr__43->SetBinContent(0,1);
   raterr__43->SetBinContent(1,1);
   raterr__43->SetBinContent(2,1);
   raterr__43->SetBinContent(3,1);
   raterr__43->SetBinContent(4,1);
   raterr__43->SetBinContent(5,1);
   raterr__43->SetBinContent(6,1);
   raterr__43->SetBinContent(7,1);
   raterr__43->SetBinContent(8,1);
   raterr__43->SetBinContent(9,1);
   raterr__43->SetBinContent(10,1);
   raterr__43->SetBinError(1,0.1494954);
   raterr__43->SetBinError(2,0.1384442);
   raterr__43->SetBinError(3,0.1319487);
   raterr__43->SetBinError(4,0.1325353);
   raterr__43->SetBinError(5,0.1324018);
   raterr__43->SetBinError(6,0.1301032);
   raterr__43->SetBinError(7,0.1277867);
   raterr__43->SetBinError(8,0.1289381);
   raterr__43->SetBinError(9,0.1363827);
   raterr__43->SetBinError(10,1.040094);
   raterr__43->SetMinimum(0.4);
   raterr__43->SetMaximum(1.6);
   raterr__43->SetEntries(1523);
   raterr__43->SetStats(0);

   ci = TColor::GetColor("#666666");
   raterr__43->SetFillColor(ci);
   raterr__43->SetFillStyle(3002);
   raterr__43->SetLineWidth(0);
   raterr__43->GetXaxis()->SetTitle("BDT discriminant");
   raterr__43->GetXaxis()->SetNdivisions(505);
   raterr__43->GetXaxis()->SetLabelFont(43);
   raterr__43->GetXaxis()->SetLabelSize(20);
   raterr__43->GetXaxis()->SetTitleSize(20);
   raterr__43->GetXaxis()->SetTickLength(0.08);
   raterr__43->GetXaxis()->SetTitleOffset(4);
   raterr__43->GetXaxis()->SetTitleFont(43);
   raterr__43->GetYaxis()->SetTitle("ratio data/prediction");
   raterr__43->GetYaxis()->SetNdivisions(505);
   raterr__43->GetYaxis()->SetLabelFont(43);
   raterr__43->GetYaxis()->SetLabelSize(20);
   raterr__43->GetYaxis()->SetTitleSize(20);
   raterr__43->GetYaxis()->SetTitleOffset(1.55);
   raterr__43->GetYaxis()->SetTitleFont(43);
   raterr__43->GetZaxis()->SetLabelFont(42);
   raterr__43->GetZaxis()->SetLabelSize(0.035);
   raterr__43->GetZaxis()->SetTitleSize(0.035);
   raterr__43->GetZaxis()->SetTitleFont(42);
   raterr__43->Draw("e2");
   Double_t xAxis99[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *ratio__44 = new TH1F("ratio__44","hist",9, xAxis99);
   ratio__44->SetBinContent(1,0.6918394);
   ratio__44->SetBinContent(2,1.105934);
   ratio__44->SetBinContent(3,0.8063922);
   ratio__44->SetBinContent(4,0.7395012);
   ratio__44->SetBinContent(5,0.8324305);
   ratio__44->SetBinContent(6,0.8573337);
   ratio__44->SetBinContent(7,0.8926633);
   ratio__44->SetBinContent(8,0.8175745);
   ratio__44->SetBinContent(9,0.8206443);
   ratio__44->SetBinError(1,0.2357023);
   ratio__44->SetBinError(2,0.1154701);
   ratio__44->SetBinError(3,0.08804509);
   ratio__44->SetBinError(4,0.08006408);
   ratio__44->SetBinError(5,0.06788442);
   ratio__44->SetBinError(6,0.06225728);
   ratio__44->SetBinError(7,0.06019293);
   ratio__44->SetBinError(8,0.06178021);
   ratio__44->SetBinError(9,0.09090909);
   ratio__44->SetEntries(309.6271);
   ratio__44->SetStats(0);
   ratio__44->SetLineWidth(2);
   ratio__44->SetMarkerStyle(20);
   ratio__44->GetXaxis()->SetLabelFont(42);
   ratio__44->GetXaxis()->SetLabelSize(0.035);
   ratio__44->GetXaxis()->SetTitleSize(0.035);
   ratio__44->GetXaxis()->SetTitleFont(42);
   ratio__44->GetYaxis()->SetLabelFont(42);
   ratio__44->GetYaxis()->SetLabelSize(0.035);
   ratio__44->GetYaxis()->SetTitleSize(0.035);
   ratio__44->GetYaxis()->SetTitleOffset(0);
   ratio__44->GetYaxis()->SetTitleFont(42);
   ratio__44->GetZaxis()->SetLabelFont(42);
   ratio__44->GetZaxis()->SetLabelSize(0.035);
   ratio__44->GetZaxis()->SetTitleSize(0.035);
   ratio__44->GetZaxis()->SetTitleFont(42);
   ratio__44->Draw("pe0same");
   TLine *line = new TLine(0,1,1,1);
   line->Draw();
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
