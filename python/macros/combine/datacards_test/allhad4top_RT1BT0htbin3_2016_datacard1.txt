## card: datacards_test/allhad4top_RT1BT0htbin3_2016_datacard1.txt number in set: 1
##selection: (nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1000 && ht_nosys<1100 &&  nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag))*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*36.33*prefirewgt*

#----------------------------------------------#
#generating datacards. name: allhad4top, year2016

#number of cards in set: 12

## number of signals (i), backgrounds (j), and uncertainties (k)
---
imax: 1
jmax: 3
kmax: *
---

## input the bdt shape histograms
---
shapes * * allhad4top_2016_shapehists.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
---

## list the bin label and the number of (data) events observed
---
bin  RT1BT0htbin3_2016
observation 547.0
---

## expected events for signal and all backgrounds in the bins
---
bin        RT1BT0htbin3_2016    RT1BT0htbin3_2016    RT1BT0htbin3_2016    RT1BT0htbin3_2016
process    TTTT    TTX    other    DDBKG
process        0        1        2        3
rate       0.313416264951    9.85810804367    17.0192387104    989.389
---

## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin
---
## statistical uncertainties

RT1BT0htbin3_2016_TTTT_stat      lnN    1.05986465721    -    -    -
RT1BT0htbin3_2016_TTX_stat      lnN    -    1.09535345247    -    -
RT1BT0htbin3_2016_other_stat      lnN    -    -    1.10078812352    -
RT1BT0htbin3_2016_DDBKG_stat      lnN    -    -    -    1.06995933854

---
## systematic uncertainties

lumi_uncorr_2016   lnN    1.010    1.010    1.010    -    
lumi_corr16_17_18   lnN    1.006    1.006    1.006    -    
elveto_2016   lnN    1.02698     1.02723    -    -    
muveto_2016   lnN    1.00232     1.00165    -    -    
prefire_2016   lnN    1.00453     1.0036    -    -    
cross_section   lnN   -   1.25    -    -    
data_pred_shape_disagreement_RT1BT0htbin3_2016  shape     -    -    -    1.0  

---
## shape uncertainties

jes_2016   shape     1.0   1.0   1.0    - 
jer_2016   shape     1.0   1.0   1.0    - 
pileup_2016   shape     1.0   1.0   1.0    - 
trigger_2016   shape     1.0   1.0   1.0    - 
btagLF  shape     1.0    1.0    1.0    - 
btagHF  shape     1.0    1.0    1.0    - 
btagLFstats1_2016   shape     1.0   1.0   1.0    - 
btagHFstats1_2016   shape     1.0   1.0   1.0    - 
btagLFstats2_2016   shape     1.0   1.0   1.0    - 
btagHFstats2_2016   shape     1.0   1.0   1.0    - 
btagCFerr1  shape     1.0    1.0    1.0    - 
btagCFerr2  shape     1.0    1.0    1.0    - 
DeepAK8TopSF_2016   shape     1.0   1.0   1.0    - 
DeepAK8WSF_2016   shape     1.0   1.0   1.0    - 
isr_TTTT  shape     1.0    -    -    -  
isr_TTX  shape     -    1.0    -    -  
fsr_TTTT  shape     1.0    -    -    -  
fsr_TTX  shape     -    1.0    -    -  
pdf  shape     1.0    1.0    -    - 
ME_TTTT  shape     1.0    -    -    -  
ME_TTX  shape     -    1.0    -    -  
ResTopEff_2016   shape     1.0   1.0   1.0    - 
ResTopMiss_2016   shape     1.0   1.0   1.0    - 

---

RT1BT0htbin3_2016 autoMCStats 0 1
---

