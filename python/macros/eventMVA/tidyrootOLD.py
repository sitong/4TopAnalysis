#skim tree pieces, flatten arrays, merge if desired. add bdts if desired. proper triggers
import subprocess
# import sys
# sys.path.insert(0, '$CMSSW_BASE/src/PhysicsTools/NanoTrees/python/catboost')
# from catboost import CatBoostClassifier, Pool, cv
# from catboost import catboost
import catboost as ctb
from catboost import CatBoostClassifier, Pool, cv
import inspect
import varlists as vl
import os.path
from root_numpy import root2array, array2root, fill_hist, array2tree
from rootpy.tree import Tree, TreeModel, FloatCol
from rootpy.io import root_open
from rootpy.tree import Tree
import argparse
from array import array
import numpy as np
import pandas as pd
from root_pandas import to_root
import xgboost as xgb
import ROOT as r
from blacklist import blacklist

print inspect.getmodule(xgb)
print inspect.getmodule(ctb)

parser = argparse.ArgumentParser(description='rootfile skimmer and more!')
#general:
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-t", "--treename", dest="treename", default='Friends')
parser.add_argument("-o", "--outdir", dest="outdir", default='./SFupdatedrootfiles/')
parser.add_argument("-v", "--verbose", dest="verbose", default='False')
#change weights
parser.add_argument("-w", "--changeweights", dest="changeweights", default='False')
#skimming:
parser.add_argument("-s", "--skim", dest="skim", default='True')
parser.add_argument("-c", "--cut", dest="cut", default='default') #if not specified will be default for 0 or 1 lep
#bdt:
parser.add_argument("-a", "--addbdt", dest="addbdt", default='False')
parser.add_argument("-C", "--fillcat", dest="fillcat", default='False')
parser.add_argument("-b", "--bdts", dest="bdts", default=['evtbdt']) #list of bdt discs to be added
#keep array branches:
parser.add_argument("-k", "--keeparr", dest="keeparr", default='False')
#merge:
parser.add_argument("-m", "--merge", dest="merge", default='False')
parser.add_argument("-r", "--removepieces", dest="rempieces", default='False') #remove smaller files aftter merge

args = parser.parse_args()

def Skim(args):

    sel0lep = 'nfunky_leptons==0 && njets>=2 && ht>=1000' #no skimming
    # sel0lep = 'nfunky_leptons==0 && njets>=4 && nbjets>=1 && ht>=700'

    # sel1lep = 'nleptons>=1 && njets>=3 && nbjets==0'
    sel1lep = 'nfunky_leptons>=1 && njets>=4 && nbjets>=1'
    sel = args.cut
    if sel=='default':
        if '1lep' in f:
            sel = sel1lep
        elif '0lep' in f or 'train' in f or 'output/test' in f:
            sel = sel0lep
        else: 
            sel=''
    return sel

def getBDT(args, data, bdtdir = '/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/eventMVA/updated_training/'):
    bdtdict = { "evtbdt": vl.evtbdt,
                "bdt1": vl.bdt1,
                "bdt2": vl.bdt2,
                "bdt3": vl.bdt3,
                "bdt4": vl.bdt4,
                "new1": vl.new1,
                "new2": vl.new2,
                "new3": vl.new3,
                "new4": vl.new4,
                "best12":vl.best12 }
    for i,bdt in enumerate(args.bdts):
        print 'adding bdt', bdt
        vars = bdtdict[bdt]
        bdtname = vars['name']
        modelfile = bdtdir+bdtname+'_qcdandtt_eventBDT-xgb.model'
        print modelfile
        branchname = bdtname+'disc'
        #load training
        model = xgb.Booster() 
        model.load_model(modelfile)
        varsF = vars['floats']
        varsI = vars['ints']
        input_vars = varsF+varsI
        testset = data[input_vars] #input vars to bdt
        preds = model.predict(xgb.DMatrix(testset))
        print 'writing discriminant to tree in branch:', branchname
        #add bdt to tree
        data[branchname] = preds
    return data

def getCatBDT(args, data, bdtfile='newBDTwith16MC'): #./new_BDT #./BDT_TT+QCD
    #loads a catboost, not xgboost, bdt file
    #data is a pandas dataframe
    from_file = CatBoostClassifier()
    model = from_file.load_model(bdtfile)
    # yout_q7   = model.predict_proba(xq7_val)[yq7_val==0,1]
    #inputs
# "nJet", "nbJet", "met", "nsdw","metovsqrtht", "ht", "sumfatjetmass", "leadTpt1","sphericity", "aplanarity", "leadbpt", "bjht", "dphij1j2", "dphib1b2", "detaj1j2", "detab1b2", "jpt7th", "meanbdisc","htratio", "centrality"
    modi = ['njets','nbjets','met','nbsws','metovsqrtht','ht','sfjm','restop1pt','sphericity','aplanarity','leadbpt','bjht','dphij1j2','dphib1b2','detaj1j2','detab1b2','jet7pt','meanbdisc','htratio','centrality']
    # modi = ['njets','nbjets','sphericity','met','aplanarity','nbsws','metovsqrtht','ht','restop1pt','leadbpt', 'bjht','dphij1j2','dphib1b2','detaj1j2','detab1b2','sfjm', 'centrality', 'htratio','meanbdisc', 'jet7pt']
    inputs = data[modi] #keep inputs needed for the bdt
    #have to rename variables appropriately
    inputs = inputs.rename(columns={"nbsws": "nsdw", 
                           "restop1pt": "leadTpt1", 
                           # "restop2pt": "leadTpt2", 
                           "jet7pt": "jpt7th", 
                           "njets": "nJet", 
                           "nbjets": "nbJet", 
                           "sfjm": "sumfatjetmass"})
    #get probablility the discriminant for those inputs is 1
    # print inputs["aplanarity"]
    # print '# inputs',len(modi)
    preds = model.predict_proba(inputs)[:,1]
    data['catBDTdisc']=preds
    print 'filling catboost bdt...'
    return data


def changeweights(args, data):

    oldweights = data["weight"]
    newweights = data["weight"]
    
    weightmap = { '2.72619795799'       : 5.43394042215, #17 QCD
                  '0.0676350668073'     : 0.136409486613, #17 QCD
                  '0.0330165401101'     : 0.0654710859292, #17 QCD
                  '0.00291155674495'    : 0.00938088725383, #17 TT 0L right
                  '0.00287062698044'    : 0.00844042680272, #17 TT 1L right
                  '0.076968818903'      : 0.00989022954205, #17 TT 2L right
                  '0.00209559220821'    : 0.00115274344767, #18 TT 0L right
                  '0.0022998675704'     : 0.00122286944605, #18 TT 1L right
                  '0.0108029311523'     : 0.00138813966801, #18 TT 2L right
                  #'': ,
    }

    for i,w in enumerate(oldweights):
        # print str(abs(w))
        if str(abs(w)) in weightmap:
            neww = weightmap[str(abs(w))]
            if w < 0.0:
                neww *= -1.0
            # print 'changing', w, 'to ', neww
            newweights[i] = neww
    data["weight"] = newweights
    return data
    

# def flatbranch(branch, data):
# # alternative: use just root, draw to new tree
# # basically did this before
#     allvals = []
#     for b in data[branch]:
#         # print len(b)
#         vec = r.std.vector("float")(len(b))
#         for i, val in enumerate(b):
#             vec[i]=val
#         # print 'vec', vec
#         # allvals.append(np.asarray(b, dtype=np.float32))
#         allvals.append(vec)
#         # allvals+=(b[:].tolist())
#     fillvals = allvals
#     # fillvals = np.asarray(allvals, dtype=np.float32)
#     flatdata = pd.DataFrame()
#     flatdata[branch]=fillvals #create new dataframe for the branch
#     # flatdata[branch]=data[branch] #create new dataframe for the branch
#     data.drop(columns=branch) #delete old branch
#     # print fillvals
#     print flatdata.columns
#     return flatdata

# def Merge(args, filedir, files, channel, killbits):
#     #NOTE: this may cause problems if there are other files in the filedir not in the list
#     samples = {"TTTT": ,
#                "QCD":  ,
              
#                    }
#     cmd = "hadd "+samp+
#     # subprocess.call(cmd, shell=True)

#set everything up
outfiles = []

fillbdts=False; mergey=False; killbits=False; skimmy=False; changeweight=False
print "Will process", len(args.filenames), "files..."
if args.skim=='True':
    print 'Will skim files.'
    skimmy=True
if args.addbdt=='True':
    print 'Filling bdts:', args.bdts
    fillbdts=True
# if args.changeweights=='True':
#     print "will update weights in file if needed"
if args.merge=='True':
    print 'Will merge files. Deleting pieces?', args.rempieces
    mergey=True
    if args.rempieces=='True':
        killbits=True

#load files:
for f in args.filenames: #get proper outname:
    outf=f
    f=f.replace("/eos/uscms/","root://cmseos.fnal.gov/")
    print 'prepping input file', f, '...'
    if f in blacklist:
        print 'file in blacklist. IGNORING.'
        continue
    outname = outf[outf.rfind('/')+1:]
    outname = outname.strip('.root')

    if os.path.exists(args.outdir+outname):
        print 'File ',args.outdir+outname,'exists, skipping...'
        continue
    rf = r.TFile.Open(f)
    t = rf.Get(args.treename)
    print t.GetEntries()
    if t.GetEntries()==0:
        print 'NO ENTRIES. skipping'
        continue
    rf.Close()
    if args.keeparr=='True':
        rf = r.TFile.Open(f)
        t = rf.Get(args.treename)
        passdiscs = []
        for entry in t:
            # print t.discs
            # should save it in same format as before.  
            # nleptons==0 && njets>=6 && nbjets>=1' nleptons>=1 && njets>=3 && nbjets>=1'
            discs = []
            if ('1lep' in f and t.njets>=4 and t.nleptons==1 and t.nbjets>=1) or ('0lep' in f and t.njets>=6 and t.nleptons==0 and t.nbjets>=1):
                # passdiscs.append(t.discs)
                # print t.discs
                for d in t.discs:
                    # print d, t.njets, t.nleptons
                    discs.append(d)
            passdiscs.append(discs)

    sel = ''; note = ''
    if '1lep' in f:
        note='1lep'
    elif '0lep' in f:
        note='0lep'
    if '2016' in f:
        note+='_2016'
    elif '2017' in f:
        note+='_2017'
    elif '2018' in f:
        note+='_2018'
    if args.keeparr=='True':
        outname += '_'+note+'_withBDTplus.root' 
    elif args.keeparr=='False':
        outname += '_'+note+'_withBDT.root' 
    
    #get skim selection if needed:
    if skimmy:
        skim=Skim(args)
        print 'selection:', skim
    data = pd.DataFrame(root2array(f, args.treename, selection=skim))
    print '# input events:', len(data)
    if len(data)==0:
        continue
    
    #add BDT(s) if needed:
    if fillbdts:
        data = getBDT(args, data)
    if args.fillcat=='True':
        data = getCatBDT(args, data)
        
    #delete unwanted columns
    
        
    # if changeweight:
    #     data = changeweights(args, data)

    # flatdfs = []
    # for c in data.columns:
    #     if data[c].dtype=='object':
    #         print 'flattening array in rootfile for branch:', c
    #         flatdfs.append(flatbranch(c, data))


    array2root(data.to_records(index=False), filename=args.outdir+outname, treename=args.treename, mode='RECREATE')

    if args.keeparr=='True':
        #add the disc array to the tree
        print 'filling discs'

        outrf = r.TFile.Open(args.outdir+outname, "update")
        tree = outrf.Get(args.treename)
        
        #create branch
        nmax = 10
        nevts = tree.GetEntries()

        discs = np.full( nmax, -1.0, dtype=np.float32)
        tree.Branch('topdiscs', discs, 'topdiscs['+str(nmax)+']/F')
        # disclist = []
        for i,d in enumerate(passdiscs):
            ## for j,val in enumerate(d):
            # discs[i]=d
            # print len(d)
            # dval = array( 'f', [ len(d) ] )
            # evdiscs = np.full( nmax, -1.0, dtype=np.float32) #reinitialize
            count=0
            for disc in d:
                # print disc
                if disc>-1.0:
                    discs[count]=disc
                    # print disc, discs
                    count+=1
            tree.Fill()
            discs = np.full( nmax, -1.0, dtype=np.float32) #reinitialize
        tree.Write()
        outrf.Close()
                    
    print 'Wrote', args.outdir+outname
    # outfiles.append(args.outdir+outname)

# if mergey:
#     print 'Merging files...'
#     Merge(args, args.outdir, outfiles, channel='0lep', killbits)
#     Merge(args, args.outdir, outfiles, channel='1lep', killbits)
