#easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
from collections import OrderedDict
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
import yieldtable as yt

parser = argparse.ArgumentParser(description='yield table')
parser.add_argument("-y", "--year", dest="year", default='2016')
parser.add_argument("-r", "--region", dest="region", default='SR')
args = parser.parse_args()

class datadists:
    def __init__(self, args):
        if args.region =='SR':
            self.CRs = yt.yieldtable(args, yt.SRsels)
        elif args.region =='VR':
            self.CRs = yt.yieldtable(args, yt.VRsels)
        self.year = args.year
        self.pd = plotdefs(self.year, args.region, False)
        self.treename = 'Events'
        self.myvar='catBDTdisc_nosys'


        self.varlist = ['njets_nosys',
                        'catBDTdisc_nosys',            
                        'nbjets_nosys',
                        'met_nosys',
                        'nbsws_nosys',
                        'metovsqrtht_nosys',
                        'ht_nosys',
                        'sfjm_nosys',
                        'restop1pt_nosys',
                        'sphericity_nosys',
                        'aplanarity_nosys',
                        'leadbpt_nosys',
                        'bjht_nosys',
                        'dphij1j2_nosys',
                        'dphib1b2_nosys',
                        'detaj1j2_nosys',
                        'detab1b2_nosys',
                        'jet7pt_nosys',
                        'meanbdisc_nosys',
                        'htratio_nosys',
                        'centrality_nosys']
        
        self.xmax = {'catBDTdisc_nosys':1,
                     'njets_nosys':20,
                     'nbjets_nosys':10,
                     'met_nosys':1000,
                     'nbsws_nosys':10,
                     'metovsqrtht_nosys':200,
                     'ht_nosys':3000,
                     'sfjm_nosys':100,
                     'restop1pt_nosys':1000,
                     'sphericity_nosys':1,
                     'aplanarity_nosys':1,
                     'leadbpt_nosys':3000,
                     'bjht_nosys':1500,
                     'dphij1j2_nosys':4,
                     'dphib1b2_nosys':4,
                     'detaj1j2_nosys':4,
                     'detab1b2_nosys':4,
                     'jet7pt_nosys':200,
                     'meanbdisc_nosys':1,
                     'htratio_nosys':100,
                     'centrality_nosys':1}


        sels = [('BASE','nrestops_nosys>=1 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
                ('RT1BT0','nrestops_nosys==1 && nbstops_nosys==0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
                ('RT1BT1','nrestops_nosys==1 && nbstops_nosys>=1 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700'),
                ('RT2','nrestops_nosys>=2 && nbstops_nosys>=0 && nbjets_nosys>=3 && njets_nosys>=9 && nfunky_leptons==0 && ht_nosys>=700')]

        self.sels = OrderedDict(sels)


    def print_plots(self):

        for var in self.varlist:
            print '-----------------------'
            for label, binsel in self.sels.iteritems():
                print 'making plots for', var, label, binsel
                self.twopads(var, label, binsel)


    def twopads(self, var, label, binsel):

        print 'making plots for', label, var, 'xmax', self.xmax[var]
        plots = self.getplots(binsel, var)

        TwoPads = r.TCanvas("FourPads","FourPads", 2000, 1000)
        TwoPads.Divide(2,1)
        # TwoPads.DrawFrame(0,0,self.xmax[var],30)
        
        # c1 = r.TPad("c1","c1",0,0,1,1);
        # c1 = r.TCanvas(); c1.DrawFrame(0,0,self.xmax[var],30)
        TwoPads.cd(1)
        gPad.DrawFrame(0,0,self.xmax[var],30)
        gPad.Modified()
        legend1 = r.TLegend(0.1, 0.6, 0.3, 0.9)
        plots['X'].SetAxisRange(0, self.xmax[var], 'X')
        plots['X'].Draw('hist');            legend1.AddEntry(plots['X'], "X", "L")
        plots['Y'].Draw('histsame');        legend1.AddEntry(plots['Y'], "Y", "L")           
        plots['A'].Draw('histsame');        legend1.AddEntry(plots['A'], "A", "L")           
        plots['B'].Draw('histsame');        legend1.AddEntry(plots['B'], "B", "L")           
        plots['C'].Draw('histsame');        legend1.AddEntry(plots['C'], "C", "L")           
        plots['X'].SetTitle(label) ; plots['X'].SetMaximum(30.0)
        legend1.Draw()
        # c1.Draw()
        gPad.Modified()
        gPad.Update()

        # TwoPads.cd()

        # c2 = r.TPad("c1","c1",0,0,1,1);        
        # # c2 = r.TCanvas(); c2.DrawFrame(0,0,self.xmax[var],30)
        # c2.cd()
        TwoPads.cd(2)
        gPad.DrawFrame(0,0,self.xmax[var],30)
        legend2 = r.TLegend(0.1, 0.6, 0.3, 0.9)
        plots['D'].Draw('histe');             legend2.AddEntry(plots['D9'], "data (9j)", "L")           
        plots['D9'].Draw('histesame');        legend2.AddEntry(plots['D'], "data", "L")           
        plots['D'].SetTitle(label) ; plots['D'].SetMaximum(30.0)
        legend2.Draw()
        # c2.Draw()
        gPad.Modified()
        gPad.Update()

        # TwoPads.Range(0,0,self.xmax[var],30)
        # TwoPads.cd(1) ; c1.DrawClonePad() 
        # TwoPads.cd(2) ; c2.DrawClonePad() #c2.Range(0,0,self.xmax[var],30)

        TwoPads.cd()
        gPad.Modified()
        gPad.Update()
                    
        print 'saved histogram: ', self.pd.outdir+label+'_'+var+'.png'
        TwoPads.SaveAs(self.pd.outdir+label+'_'+var+'.png')


    def getplots(self, binsel, var):

        colors = {'D':r.kGray+2,
                  'A':r.kBlue-7,
                  'B':r.kGreen-7,
                  'C':r.kRed-7,
                  'X':r.kMagenta-7,
                  'Y':r.kOrange-3}
        
        crs = self.CRs.getCRs(binsel)
        crs['D']=binsel #add D
        plots={}
        
        for CR, sel in crs.iteritems(): #yields for A,B,C,D,X,Y
            print CR
            print sel

            datahist = self.pd.gethist(self.pd.datafilename, var, sel)
            datahist.SetLineWidth(3)
            datahist.SetMarkerStyle(20)
            datahist.SetLineColor(colors[CR])
            dyld, dunc = self.pd.getyield(datahist)
            datahist.Scale(100.0/dyld)
            datahist.SetAxisRange(0, self.xmax[var], 'X')
                
            if CR=='D':
                sel = sel.replace('njets_nosys>=9', 'njets_nosys==9')
                data9hist = self.pd.gethist(self.pd.datafilename, var, sel)
                data9hist.SetLineWidth(3)
                data9hist.SetLineStyle(9)
                data9hist.SetLineColor(r.kRed+1)
                d9yld, d9unc = self.pd.getyield(data9hist)
                data9hist.Scale(100.0/dyld)
                data9hist.SetAxisRange(0, self.xmax[var], 'X')
                plots['D9']=data9hist

            plots[CR]= datahist
     
        return plots

runplots = datadists(args)
runplots.print_plots()
