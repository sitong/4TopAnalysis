from root_numpy import root2array, array2root
import json
import numpy as np
import argparse
import pandas as pd
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
import RootStuff as rs
import ROOT as r

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=[])
parser.add_argument("-c", "--compfile", dest="compfile", default='')
parser.add_argument("-j", "--jsonfile", dest="jsonfile", default='')
#if filenames is not specified, will not recreate json file
#if compfile is not specified, will not compare files

parser.add_argument("-v", "--vars", dest="vars", default=['event', 'run', 'lumi'])
parser.add_argument("-s", "--samp", dest="samp", default='TTTT')
parser.add_argument("-y", "--year", dest="year", default='2017')
parser.add_argument("-t", "--treename", dest="treename", default='Events')
parser.add_argument("-e", "--eventname", dest="eventname", default='event')
parser.add_argument("-l", "--luminame", dest="luminame", default='lumi')
parser.add_argument("-b", "--baseline", dest="base", default='nleptons==0 && ht>=700 && njets>=9 && nbjets>=3') #0.00417175533311
parser.add_argument("-o", "--outfile", dest="outfile", default='output_allhad_nort.json')
args = parser.parse_args()


#function to wrtie json file
def write_json(args, jsonFile):
#clear file if not empty
    full_json = {}

#loop over rootfiles and apply baseline
    totevts = 0
    for f in args.filenames: #get proper outname:
        outf=f
        if "/eos/uscms/" in f:
            f=f.replace("/eos/uscms/","root://cmseos.fnal.gov/")
        print 'prepping input file', f, '...'

        base = args.base
        # if args.year =='2016':
        #     base += '&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'
        # if args.year =='2017':
        #     base += '&& (HLT_PFHT380_SixJet32_DoubleBTagCSV_p075 ||  HLT_PFHT430_SixJet40_BTagCSV_p080 ||  HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 ||  HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 ||  HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0)'
        # if args.year =='2018':
        #     base += '&& (HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2 ||  HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 ||  HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||  HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59)'
        print 'selection', base

        data = pd.DataFrame(root2array(f, args.treename, selection=base))
        print '# input events in file:', len(data)
        totevts+=len(data)
        if len(data)==0:
            continue

        #extract information and fill json file    
        for i,d in enumerate(data[args.eventname]):
            json_data = {}
            for obj in args.vars:
                json_data[obj]=int(data[obj][i])
            # print d
            dictname = str(d)+'_'+str(data[args.luminame][i])
            # print dictname
            full_json[dictname] = json_data

        #print # keys in full dictionary for each file 
        print 'total events in json file:', len(full_json.keys())

    jsonFile.write(json.dumps(full_json, sort_keys=True))
    print '# files scanned:', len(args.filenames)
    print 'Total # input events:', totevts

#function to compare two dictionaries/json files
def compare_dict(a,b):   
    ngood = 0; nbad_a = 0; nbad_b = 0; nbad_val = 0; nvgood = 0
    for k,v in a.iteritems():         
        # print k, v
        # event check
        if not k in b:            
            nbad_a+=1
            # print 'event',k, 'not in', args.compfile
        #value check:
        else:
            goodval=True
            ngood+=1
            print 'match for event #', k, 'a:', a[k], 'b:', b[k]
            for subk, subv in v.iteritems():
                if subv != b[k][subk]:               
                    goodval=False
                    # print 'for event', k, 'variable', v,':', subv, 'does not match', b[k][subk], 'in', args.compfile
            if goodval:
                nvgood+=1

    for k,v in b.iteritems():         
        # event check
        if not k in a:
            nbad_b+=1
            # print 'event',k, 'not in', outjson

    print '---------------------------------------------------------------'
    print 
    print '# matching events:', ngood
    print '# events in a:', float(ngood+nbad_a)
    print '# events in b:', float(ngood+nbad_b)
    print '% overlapping events in a:', float(ngood)/float(ngood+nbad_a)
    print '% overlapping events in b:', float(ngood)/float(ngood+nbad_b)
    print '# events in', args.compfile, 'not in', outjson,':', nbad_a
    print '# events in',outjson, 'not in', args.compfile,':', nbad_b
    print '# events with mismatched values:', nbad_val 
    print '% matching events:', (float(ngood)/float(ngood+nbad_a+nbad_b))
    print '% events with matched values:', (float(nvgood)/float(ngood))




########################################################################

outjson = args.samp + '_' + args.year + '_'+ args.outfile

if args.jsonfile=='':
    if len(args.filenames)>0:
        print 'will create and write to ', outjson
        with open(outjson, 'w') as jsonFile:
            write_json(args, jsonFile)
        jsonFile.close()

    elif len(args.filenames)==0:
        print 'no files provided. will read json file', outjson
else:
    print 'opening', args.jsonfile
    outjson = args.jsonfile

if args.compfile != '':
#compare two text files, outputting % overlap
    with open(args.compfile, 'r') as compFile:
        with open(outjson, 'r') as myFile:
            print 'loading compfile:'
            compfile = json.load(compFile)    
            print 'loading myfile:'
            myfile   = json.load(myFile)
            compare_dict(myfile, compfile)

else:
    print 'no compfile provided'
