#easier plotting
# import root_numpy as rn
# import argparse
import os
import numpy as np
# import pandas as pd
# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# import re
import ROOT as r
# import random

#number of events in each full sample
nevt_dict= {
    '/TT_Tune': 139776724,
    'TTTT_': 2376039,
    'QCD_HT500to700': 71747175,
    'QCD_HT700to1000': 37233786,
    'QCD_HT1000to1500': 15210939,
    'QCD_HT1500to2000': 18942031,
    'QCD_HT2000toInf': 1801075,
    }

lumis= {'2016':{'1lep': 35.917482079, #1lep from json
                'mergey': 35.917482079,
                '1el' : 35.848767137,
                '1mu' : 35.754215597,
                '0lep': 35.91},
                # '0lep': 35.90609416 },
        '2017':{'0lep': 41.53,
                '1lep': 41.53},
        '2018':{'0lep': 59.74,
                '1lep': 59.74},
        'ALL':{'0lep': 137.19,
                '1lep': 137.19}}


#makes a rootfile in root format
def makeroot(infile, treename, options="read"):
    rfile = r.TFile(infile, options)
    tree = rfile.Get(treename)
    return rfile, tree

def getyield(hist, verbose=False):
    errorVal = r.Double(0)
    minbin=0
    maxbin=hist.GetNbinsX()+1
    hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
    if verbose:
        print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
    return hyield,  errorVal


# #unweighted yield: NEnt. weighted yield: rate
# def getyield(tree, cutstring, projvar='ht', lumi='36.0', wgtvar='weight', addweight=False):
#     htmp = r.TH1F('htmp','htmp',1,0,10000)
#     # cutstr = '('+str(lumi)+'*'+weight+')*('+cutstr+')'
#     if addweight:
#         print 'CUTSTRING', "("+cutstring+")*("+wgtvar+"*"+lumi+")"
#         NEnt = tree.Draw(projvar+">>htmp", "("+cutstring+")*("+wgtvar+"*"+lumi+")")
#         rate = htmp.Integral(0,2)
#     else:
#         print 'CUTSTRING', "("+cutstring+")"
#         NEnt = tree.Draw(projvar+">>htmp", cutstring)
#         rate = htmp.Integral(0,2)
#     # print 'file:', infile, 'cutstring:', cutstring, 'lumi', lumi
#     # print "event yield:", NEnt
#     return NEnt, rate

#delete the file, keep reference to histogram
def fixref(rootfile, hist):
    hist.SetDirectory(0)
    rootfile.Close()

#makes a 1D histogram
def getMultiFileHist(filedir, tags, treename, var, sel, nbins=100, selrange=(0,1)):
    chain = r.TChain(treename) #create tchain for all files
    # r.TH1.AddDirectory(kFALSE) #don't associate histogram to tfile
    for f in os.listdir(filedir):
        if any(tag in f for tag in tags):
            print 'adding', filedir+f
            chain.Add(filedir+f)            
    hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
    print 'drawing hist with', var, sel
    chain.Draw(var+">>hist", sel)
    return hist

#makes a 1D histogram
def gethist(tree, var, sel, nbins=100, selrange=(0,1)):
    # print sel
    hist = r.TH1F('hist', 'hist', nbins, selrange[0], selrange[1])
    tree.Draw(var+">>hist", sel)
    return hist

def getvarbinhist(tree, var, sel, edges):
    # print sel
    edges=np.array(edges)
    nbins = len(edges)-1
    hist = r.TH1F('hist', 'hist', nbins, edges)
    tree.Draw(var+">>hist", sel)
    return hist

#normalizations
def normhist(hist, N=1.0):
    scale = 1.0
    if hist.Integral()> 0:
        scale = N/hist.Integral()
    hist.Scale(scale)
    return hist

#takes list of histograms, basic sets aesthetics of them 
#make sure this works, maybe have to return things or make a class
def multiplot(outfile, plots, names, xtitle, ytitle="N Entries", plottitle="", legend=True):
    canvas = r.TCanvas("canvas", "canvas", 800, 800)
    colors = [r.kCyan+1, r.kRed-7, r.kOrange-4, r.kBlue-7, r.kPink-6, r.kGray]
    # colors = [r.kBlue+1, r.kAzure+8, r.kTeal+1, r.kGreen-6, r.kGreen+3, r.kSpring-4, r.kGray, r.kViolet+6, r.kViolet+4, r.kViolet-6, r.kViolet-1, r.kMagenta+2, r.kMagenta-7, r.kPink+7, r.kRed+1, r.kRed+3, r.kOrange+8, r.kOrange+1]
    if legend:
        leg = r.TLegend(0.1,0.8,0.5,0.9)

    for i, p in enumerate(plots):
        drawopt=''
        if i==0:
            p.SetStats(0) #only for histograms    
            print type(p)
            if (type(p)==r.TH1F or type==r.TH1D):
                drawopt="HISTL"
                # p.SetTile(plottitle) #not for plots
            elif type(p)==r.TGraph:
                print p.Eval(0)
                print p.Eval(0.9)
                drawopt="ALP*"
            print 'drawopt', drawopt
            
            p.Draw('HIST')

            p.GetYaxis().SetTitle(ytitle)
            p.GetYaxis().SetTitleSize(20)
            p.GetYaxis().SetTitleFont(43)
            p.GetYaxis().SetTitleOffset(1.55)
            
            p.GetXaxis().SetTitle(xtitle)
            p.GetXaxis().SetTitleSize(20)
            p.GetXaxis().SetTitleFont(43)
            p.GetXaxis().SetTitleOffset(1.55)
        else:
            p.Draw('HISTSAME')

        p.SetLineColor(colors[i])
        p.SetLineWidth(2)
        if legend:
            leg.AddEntry(p, names[i], "l")    

    leg.Draw('same')
    # canvas.Modified()
    canvas.Update()
    print 'saving as', outfile
    canvas.SaveAs(outfile)
    canvas.Draw()
    # return canvas, leg

