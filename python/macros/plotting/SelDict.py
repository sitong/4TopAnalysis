import numpy as np
import ROOT as r

metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'
mcSFlist     = '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'

lumi16 = 35.90609416
lumi17 = 41.53
lumi18 = 59.74

seldict = { 'BASE' :{ 'def'  : 'nleptons==0 && ht>=700 &&',
                      'r1'   : 'nrestops>=1 && nleptons==0 &&',
                      'r1b0' : 'nrestops==1 && nbstops==0 && nleptons==0 &&',
                      'r1b1' : 'nrestops==1 && nbstops>=1 && nleptons==0 &&',
                      'r2b0' : 'nrestops>=2 && nbstops>=0 && nleptons==0 &&',
                      'SRbin'   : '',
                    },
            'SR' :{ 'A' : 'nbjets==2 && njets==8',
                    'B' : 'nbjets==2 && njets>=9',
                    'C' : 'nbjets>=3 && njets==8',
                    'X' : 'nbjets>=3 && njets==7',
                    'Y' : 'nbjets==2 && njets==7',
                    'D' : 'nbjets>=3 && njets>=9',
                },
            'VR' :{ 'A' : 'nbjets==2 && njets==7',
                    'B' : 'nbjets==2 && njets==8',
                    'C' : 'nbjets>=3 && njets==7',
                    'X' : 'nbjets>=3 && njets==6',
                    'Y' : 'nbjets==2 && njets==6',
                    'D' : 'nbjets>=3 && njets==8',
                },
            'BASIC' :{ '2016' : '',
                       '2017' : '',
                       '2018' : '',
                },
            'MC' :{ '2016' : mcSFlist+lumi16,
                    '2017' : mcSFlist+lumi17,
                    '2018' : mcSFlist+lumi18,
                },
            'DATA' :{ '2016': '&& (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
                      '2017': '&& (HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || HLT_PFHT430_SixJet40_BTagCSV_p080 || HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || HLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,
                      '2018': '&& (HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,
                  },
        }


# def getsels(CR, base='VR', form='basic', year='2016'):
#     sel = '('
#     return sel
    

def getHistSettings(var):
    vardict = {
        #[nbins, xmin, xmax]
        'ht'      : [20, 0.0, 3000.0],
        'bjht'    : [50, 0, 1500],
        'met'     : [50, 0, 1000],
        'metovsqrtht' : [50, 0.0, 200.0],
        'sfjm'    : [100, 0, 1000],
        'njets'   : [20, 0, 20],
        'nbjets'  : [10, 0, 10],
        'nbsws'   : [10, 0, 10],
        'nrestops': [6, 0, 6],
        'nbstops' : [6, 0, 6],
        'nleptons': [20, 0, 20],
        'fwm0'  : [50, 0, 1],
        'fwm1'  : [50, 0, 1],
        'fwm2'  : [50, 0, 1],
        'fwm3'  : [50, 0, 1],
        'fwm4'  : [50, 0, 1],
        'fwm5'  : [50, 0, 1],
        'fwm6'  : [50, 0, 1],
        'fwm7'  : [50, 0, 1],
        'fwm8'  : [50, 0, 1],
        'fwm9'  : [50, 0, 1],
        'fwm10'  : [50, 0, 1],
        'fwm20'  : [50, 0, 1], 
        'fwm30'  : [50, 0, 1],
        # 'topdiscs'  : [50, -1, 1],
        'rescand_disc'  : [50, 0.95, 1],
        'rescand_pt'  : [20, 0.0, 1000],
        'jet1pt'  : [50, 0, 1000],
        'jet2pt'  : [50, 0, 1000],
        'bstop1_pt'  : [50, 100, 1000],
        'bstop2_pt'  : [50, 100, 1000],
        'restop1pt'  : [50, 100, 1000],
        'restop2pt'  : [50, 100, 1000],
        'qglsum'  : [50, 0, 30],
        'qglprod'  : [50, 0, 30],
        'jet8pt'  : [50, 0, 200],
        'aplanarity'  : [20, 0, 1],
        'sphericity'  : [20, 0, 1],
        'dphij1j2'  : [10, -4, 4],
        'detaj1j2'  : [10, -4, 4],
        'dphib1b2'  : [10, -4, 4],
        'detab1b2'  : [10, -4, 4],
        'leadbpt' : [50, 0, 500],
        'npv' : [50, 0, 100],
        'lep1pt'  : [50, 0, 250],
        'lep2pt'  : [50, 0, 250],
        'ModBDT2disc': [100, 0.0, 1.0],
        'evtbdtdisc': [100, 0.0, 1.0],
        'catBDTdisc': [50, 0.0, 1.0],
        'NewBDT3disc': [100, 0.0, 1.0],
        'NewBDT4disc': [100, 0.0, 1.0],
        'Best12disc': [100, 0.0, 1.0],
        'passtrigel': [2, 0,1], 
        'passtrigmu': [2, 0,1],
        }
    params = vardict[var]
    nbins = params[0]
    selrange = (params[1], params[2])
    return nbins, selrange
