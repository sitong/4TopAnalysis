#helper for lepton trigger selections
import numpy as np
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Object

def getleptriggers(event, infile):
    filestr = str(infile)
    typ=''
    if 'SINGLEEL' in filestr:
        typ='el'
    elif 'SINGLEMU' in filestr:
        typ='mu'

    trigel = False; trigmu = False; triglep = False
    passtrigmuht   = False; passtrigelht   = False; passtrigmetmht = False; passtriglepor  = False
    # normal triggers
    event.eTrigger   = Object(event, 'HLT_Ele27_WPTight_Gsf')
    event.muTrigger1 = Object(event, 'HLT_IsoMu24')
    event.muTrigger2 = Object(event, 'HLT_IsoTkMu24')
    # or trigger stuff
    #electrons (passTrigElHT)
    event.eltrigor1 = Object(event, 'HLT_Ele27_WPTight_Gsf')
    event.eltrigor2 = Object(event, 'HLT_Ele15_IsoVVVL_PFHT350_PFMET50')
    event.eltrigor3 = Object(event, 'HLT_Ele15_IsoVVVL_PFHT400_PFMET50')
    event.eltrigor4 = Object(event, 'HLT_Ele15_IsoVVVL_PFHT350')
    event.eltrigor5 = Object(event, 'HLT_Ele15_IsoVVVL_PFHT400')
    event.eltrigor6 = Object(event, 'HLT_Ele15_IsoVVVL_PFHT600')
    #muons (passTrigMuHT)
    event.mutrigor1 = Object(event, 'HLT_IsoMu24')
    event.mutrigor2 = Object(event, 'HLT_IsoTkMu24')
    event.mutrigor3 = Object(event, 'HLT_Mu15_IsoVVVL_PFHT350_PFMET50')
    event.mutrigor4 = Object(event, 'HLT_Mu15_IsoVVVL_PFHT400_PFMET50')
    event.mutrigor5 = Object(event, 'HLT_Mu15_IsoVVVL_PFHT350')
    event.mutrigor6 = Object(event, 'HLT_Mu15_IsoVVVL_PFHT400')
    event.mutrigor7 = Object(event, 'HLT_Mu15_IsoVVVL_PFHT600')
    #MET (passTrigMETMHT)
    event.mettrigor1 = Object(event, 'HLT_PFMET110_PFMHT110_IDTight')
    event.mettrigor2 = Object(event, 'HLT_PFMETNoMu110_PFMHTNoMu110_IDTight')
    event.mettrigor3 = Object(event, 'HLT_PFMET120_PFMHT120_IDTight')
    event.mettrigor4 = Object(event, 'HLT_PFMETNoMu120_PFMHTNoMu120_IDTight')

    if (event.eTrigger and typ=='el' and not (event.muTrigger1 or event.muTrigger2)): #or self.isMC
        trigel=True
    if ((event.muTrigger1 or event.muTrigger2) and typ=='mu' and not event.eTrigger): #or self.isMC:
        trigmu=True
    if trigel or trigmu:
        triglep=True
    if (event.mutrigor1 or event.mutrigor2 or event.mutrigor3 or event.mutrigor4 or event.mutrigor5 or event.mutrigor6 or event.mutrigor7): #or self.isMC
        passtrigmuht   = True
    if (event.eltrigor1 or event.eltrigor2 or event.eltrigor3 or event.eltrigor4 or event.eltrigor5 or event.eltrigor6): #or self.isMC
        passtrigelht   = True 
    if (event.mettrigor1 or event.mettrigor2 or event.mettrigor3 or event.mettrigor4): #or self.isMC
        passtrigmetmht = True
    if (passtrigmuht or passtrigelht or passtrigmetmht):
        passtriglepor=True
    return int(trigel), int(trigmu), int(triglep), int(passtrigmuht), int(passtrigelht), int(passtrigmetmht), int(passtriglepor)
