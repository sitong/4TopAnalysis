#takes rootfile and outputs rootfile with N number of random unique events
import root_numpy as rn
import argparse
import os
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import ROOT as r
import random

parser = argparse.ArgumentParser(description='shuffle split rootfile')
parser.add_argument("-i", "--infile", dest="infile", default="sync.root")
parser.add_argument("-t", "--treename", dest="treename", default="Friends")
parser.add_argument("-n", "--numevts", dest="numevts", default=100000)
parser.add_argument("-e", "--endevt", dest="endevt", default=1000000 )
# parser.add_argument("-o", "--outdir", dest="outdir", default="")
args = parser.parse_args()


inroot = r.TFile(args.infile)
intree = inroot.Get(args.treename)

#get as array
inarr = rn.tree2array(intree)
#choose random event numbers
totevts = len(inarr)
samps = random.sample(range(0,totevts), int(args.numevts))
outarr = []
for s in samps:
    # print inarr[s]
    print 'selecting event',  s, '...'
    outarr.append(inarr[s])
outarr = np.array(outarr)
# outarr=inarr
#write to new root file
outfile = args.infile.strip(".root")+"_"+str(args.numevts)+"evts.root"
print 'creating output file', outfile
rn.array2root(outarr, outfile, treename=args.treename, mode='recreate')
