# for testing 1lep channels lepton selection
#for restop mva, deepresolved testing
import os
import numpy as np
import ROOT
import math
import itertools
from collections import OrderedDict
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
import PhysicsTools.NanoTrees.helpers.weighthelper as xw

from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')

class FunkyLeptons(Module):
    def __init__(self, channel, year):        
        self.channel = channel
        self.year = year

        #muon veto: pt >= 15, eta <= 2.4, cutBaseIdLoose, MiniIsoLoose
        #el veto: pt >= 15, superCluster eta <= 2.5, not isEBEEGap, electronID("mvaEleID-Fall17-noIso-V2-wpLoose") = 1, miniIso <= 0.4
        
        # define all the lepton settings
        self.mu_pt      = 15.0
        self.mu_eta     = 2.4
        self.mu_ID      = 1  #Muon_looseId
        self.mu_isoval  = 1 #(1 means Muon_miniIsoId=MiniIsoLoose) #miniisoloose

        self.el_pt      = 15.0
        self.el_eta     = 2.5 #supereta = lep.eta + lep.deltaEtaSC
        self.el_ID      = 1 #Electron_mvaFall17V2noIso_WPL (mvaEleID-Fall17-noIso-V2-wpLoose) 
        self.el_isoval  = 0.4 #miniPFRelIso_all
        # self.isEBEEGap  = False #??

    def beginJob(self):
        self.genhelp = genparthelper()
    
    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        # self.baseTrees(inputFile, outputFile, inputTree, wrappedOutputTree)
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        self.isMC = bool(inputTree.GetBranch('genWeight'))
        print 'initializing trees...'

        #leptons
        self.out.branch("nfunky_leptons",   "I")
        self.out.branch("nfunky_muons",     "I")
        self.out.branch("nfunky_electrons", "I")
        self.out.branch("funkylep1_pt",     "F")
        self.out.branch("funkylep1_id",     "I")
        self.out.branch("funkylep2_pt",     "F")
        self.out.branch("funkylep2_id",     "I")

        self.out.branch("ngenmus",    "I")
        self.out.branch("ngenels",    "I")
        self.out.branch("ngenleps",   "I")
        
        # self.out.branch("nloosemus",    "I")
        # self.out.branch("nvetoels",     "I")
        # self.out.branch("nVRleps",      "I")

    def getleps(self, event):
        muons     = Collection(event, "Muon")
        electrons = Collection(event, "Electron")
        
        SelMus = []; SelEls = []; SelLeps=[]
        if self.channel == '0lep':
            for mu in muons:
                if (mu.pt>=self.mu_pt and abs(mu.eta)<=self.mu_eta): 
                    if (mu.looseId >= self.mu_ID):
                        if (mu.miniIsoId >= self.mu_isoval):
                            SelMus.append(mu); SelLeps.append(mu)
            for el in electrons:
                if (el.pt>=self.el_pt and abs(el.eta + el.deltaEtaSC)<=self.el_eta):
                    if (abs(el.eta + el.deltaEtaSC)<1.442 or abs(el.eta + el.deltaEtaSC)>1.566): #isEBEEGap. why on veto?
                        if (el.mvaFall17V2noIso_WPL >= self.el_ID):
                            if (el.miniPFRelIso_all <= self.el_isoval):
                                SelEls.append(el); SelLeps.append(el)
        elif self.channel=='1lep':
            for mu in muons:
                if (mu.pt>=20.0 and abs(mu.eta)<=2.4): 
                    if (mu.tightId >= 1):
                        if (mu.miniIsoId >= 3):
                            SelMus.append(mu); SelLeps.append(mu)
            for el in electrons:
                if (el.pt>=20.0 and abs(el.eta + el.deltaEtaSC)<=2.5):
                    if (abs(el.eta + el.deltaEtaSC)<1.442 or abs(el.eta + el.deltaEtaSC)>1.566): #isEBEEGap. why on veto?
                        if (el.mvaFall17V2noIso_WP90 >= 1,.0):
                            if (el.miniPFRelIso_all <= 0.1):
                                SelEls.append(el); SelLeps.append(el)

        return SelEls, SelMus, SelLeps

    def getFunkyLeptons(self, event, getVRleps=True):
        # muons     = Collection(event, "Muon")
        # electrons = Collection(event, "Electron")
        
        genEls=[]; genMus=[]; Ngenleps=0
        if self.isMC:
            GenParts = Collection(event, "GenPart")
            genEls, genMus, Ngenleps = self.genhelp.FindGenLeps(GenParts)
            
        # SelMus = []; SelEls = []; SelLeps=[]
        SelEls, SelMus, SelLeps = self.getleps(event)

        # if getVRleps:
        #     nvetoels = 0; nloosemus = 0
        #     for mu in muons:
        #         if (mu.pt>=10.0 and abs(mu.eta)<=2.5): 
        #             if (mu.pfRelIso04_all<0.25):
        #                 if (mu.looseId and not mu.tightId ):
        #                     if self.year=='2016':
        #                         if not mu.mediumId:
        #                             nloosemus+=1
        #                     else:
        #                         nloosemus+=1
        #     for el in electrons:
        #         if (el.pt>=15.0 and abs(el.eta)<2.5):
        #             if (el.cutBased==1):
        #                 nvetoels+=1
        #     nVRleps=nvetoels+nloosemus

        #     self.out.fillBranch("nloosemus", nloosemus )
        #     self.out.fillBranch("nvetoels", nvetoels )
        #     self.out.fillBranch("nVRleps", nVRleps )


        SelLeps.sort( key=lambda x : x.pt, reverse=True)        #sort highest to lowest pt
        # SelMus = np.array(SelMus)
        # SelEls = np.array(SelEls)
        # SelLeps = np.array(SelLeps)

        lep1pt=0.0;  lep1id=0
        lep2pt=0.0;  lep2id=0
        if len(SelLeps)>0:
            lep1pt = SelLeps[0].pt
            lep1id = SelLeps[0].pdgId
        if len(SelLeps)>1:
            lep2pt = SelLeps[1].pt
            lep2id = SelLeps[1].pdgId

        #fill trees
        self.out.fillBranch("nfunky_leptons",   len(SelLeps))
        self.out.fillBranch("nfunky_muons",     len(SelMus))
        self.out.fillBranch("nfunky_electrons", len(SelEls))
        self.out.fillBranch("funkylep1_pt",     lep1pt)
        self.out.fillBranch("funkylep1_id",     lep1id)
        self.out.fillBranch("funkylep2_pt",     lep2pt)
        self.out.fillBranch("funkylep2_id",     lep2id)

        #gen info (applies to all mc)
        self.out.fillBranch("ngenleps", Ngenleps )
        self.out.fillBranch("ngenmus", len(genMus) )
        self.out.fillBranch("ngenels", len(genEls) )
        
        return len(SelLeps)
        
    def analyze(self, event):
        nleps = self.getFunkyLeptons(event, getVRleps=True)
        if nleps==0 and self.channel =='0lep':
            return True
        elif nleps>0 and self.channel =='1lep':
            return True
        else:
            return False

FunkyLeps2016_0lep = lambda : FunkyLeptons('0lep','2016')
FunkyLeps2017_0lep = lambda : FunkyLeptons('0lep','2017')
FunkyLeps2018_0lep = lambda : FunkyLeptons('0lep','2018')

FunkyLeps2016_1lep = lambda : FunkyLeptons('1lep','2016')
FunkyLeps2017_1lep = lambda : FunkyLeptons('1lep','2017')
FunkyLeps2018_1lep = lambda : FunkyLeptons('1lep','2018')



