# xgboost event level mva 
from root_numpy import root2array, array2root
import varlists as vl
import os
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import xgboost as xgb
import re
import xml.etree.cElementTree as ET

#from before (BaseTMVAProducer.cc)
# const TString TMVAFactoryConfiguration::defaultOptions = "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.10:UseBaggedGrad:GradBaggingFraction=0.5:nCuts=200:NNodesMax=5";
#grad bagging fraction -> Defines the fraction of events to be used in each iteration, this is subsample in xgboost
#nCuts -> Number of grid points in variable range used in finding optimal cut in node splitting, maybe max_bin (hists only) in xgboost
#NodePurityLimit -> In boosting/pruning, nodes with purity > NodePurityLimit are signal; background otherwise

#INPUTS:
sig_file = 'TTTT_0lepv6_train_40k.root'
bkg_file = 'QCD_TT_0lepv6_train.root' 
# sig_file = '/eos/uscms//store/user/mquinnan/NanoAODv5/eventBDTtrain/TTTT_80k_train.root'
# bkg_file = '/eos/uscms//store/user/mquinnan/NanoAODv5/eventBDTtrain/TT_forTTonly_train.root' #TT_forTTonly_train.root
# bkg_file = '/eos/uscms//store/user/mquinnan/NanoAODv5/eventBDTtrain/QCD_TT_forboth_train_skimmed.root' #TT_forTTonly_train.root
# sig_file = '/eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/training/parts/TTTT_TuneCUETP8M2T4_13TeV-amcatnlo-pythia8_80k_tree.root'
# bkg_file = '/eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/training/parts/QCD_TT_tree.root'#trainingTT/parts/TT_TuneCUETP8M2T4_13TeV-powheg-pythia8_tree.root'#
# bkg_file = '/eos/uscms/store/user/mquinnan/NanoAODv5/2016MC/eventBDT/trainingTT/parts/TT_TuneCUETP8M2T4_13TeV-powheg-pythia8_tree.root'

vars     = vl.evtbdt #which bdt inputs you are using 
bdtname  = vars['name']

#OUTPUTS:
note='_qcdandtt'
# note='_ttonly'
trainoutput = './updated_training/'+bdtname+note+'_eventBDT-training-sample.h5' #training sample input
rootoutput  = './rootfiles/'+bdtname+note+'_eventBDT-result.root'        #training results as rootfile
modeloutput = './updated_training/'+bdtname+note+'_eventBDT-xgb.model'          #xgb training model output (disc weights)
xmloutput   = './updated_training/'+bdtname+note+'_eventBDT-xgb.xml'            #tmva xml-file training output (disc weights)

#BASELINE
basesel = 'nleptons==0 & ht>700 & njets>=9 & nbjets>=3' 
sigsel  = basesel
bgdsel  = basesel

#info for BDT
makeXML  =  True #convert model to tmva xml format
wgtvar   = 'weight'
treename = 'Events'

#BDT parameters
# frac_testvtrain = 0.3  #Size of the validation sample
frac_testvtrain = 0.1  #Size of the validation sample
BDT_objective   = 'binary:logistic' #learning task: logistic regression
BDT_eta         = 0.10 #Step size shrinkage used in update to prevent overfitting 0.05->0.10
BDT_maxdepth    = 5    #Maximum depth of a tree. Increasing this value will make the model more complex and more likely to overfit. 6->5
BDT_minchildwgt = 0    #Minimum sum of instance weight (hessian) needed in a child.
BDT_num_round   = 1000 #Max allowed # of trees 2000->1000
BDT_subsample   = 0.5  #fraction of events to be used in each iteration

#observer variables: (not used in training, just saved)
obs_vars = vars['obs'] 

#training vars: floats and ints
varsF = vars['floats']
varsI = vars['ints']
input_vars = varsF+varsI

#prepare input samples: variables+whether sig or bgd. Returns as dataframe
def prepInput(filepath, issig):
    if issig:
        sel, target = sigsel, 1
    else:
        sel, target = bgdsel, 0
    print 'prepping input file', filepath
    # dataframe = pd.DataFrame(root2array(filepath, treename))
    dataframe = pd.DataFrame(root2array(filepath, treename, selection=basesel))
    print 'selection:', sel
    print 'dataframe:', dataframe
    #apply selection:
    dataframe.query(sel, inplace=True)
    dataframe = dataframe.loc[:, input_vars + obs_vars].reset_index(drop=True)
    dataframe['target'] = target 
    #no reweighting yet...
    return dataframe

#set up training input for BDT. Takes sig/bgd dataframes, normalizes them by weight, and converts them to hd5 training inputs
def TrainSampSetup(output): #converttoHD5
    sig_df = prepInput(sig_file, True) 
    bkg_df = prepInput(bkg_file, False)
    sig_sum_wgt = sig_df[wgtvar].sum()
    bkg_sum_wgt = bkg_df[wgtvar].sum()
    print 'Signal events: %d, Background events: %d' % (len(sig_df), len(bkg_df))
    print 'Signal weight: %f, Background weight: %f' % (sig_sum_wgt, bkg_sum_wgt)
    sig_df['train_weight'] = sig_df[wgtvar] * (1000. / sig_sum_wgt)
    bkg_df['train_weight'] = bkg_df[wgtvar] * (1000. / bkg_sum_wgt)
    print 'After normalization:'
    print 'Signal weight: %f, Background weight: %f' % (sig_df['train_weight'].sum(), bkg_df['train_weight'].sum())
    full_df = pd.concat([sig_df, bkg_df], ignore_index=True)
    full_df.to_hdf(output, 'Events', format='table')
    return full_df

#loads training sample, creating it from sig and bgd files if needed.
def loadSample():
    try:
        return pd.read_hdf(trainoutput)
    except IOError:
        print 'Training sample %s does not exist! Creating HD5 training sample from input root files...' % trainoutput
        return TrainSampSetup(trainoutput)

#run xgboost, output training weights
def runXGBoost(trainfile):

    #shuffle dataset
    train_samp = trainfile.sample(frac=1).reset_index(drop=True)

    #Load variables
    X = train_samp[input_vars]    #what BDT is given
    Y = train_samp['target']      #what BDT finds out
    bdtW = train_samp['train_weight']#BDT weight
    inW  = train_samp['weight']   #input weight

    # split samples for training and testing
    test_size = frac_testvtrain  # size of the validation sample
    isTrain = np.random.uniform(size=len(train_samp)) > test_size
    isTest  = np.logical_not(isTrain)
    train_samp['isTraining'] = isTrain
    print "train size:", len(train_samp)
    X_train, X_test = X[isTrain], X[isTest]
    Y_train, Y_test = Y[isTrain], Y[isTest]
    W_train, W_test = bdtW[isTrain], bdtW[isTest]
    inW_train, inW_test = inW[isTrain], inW[isTest]

    # set up input matrices for xgboost
    d_train = xgb.DMatrix(X_train, label=Y_train, weight=W_train)
    d_test  = xgb.DMatrix(X_test,  label=Y_test,  weight=W_test)

    # setup parameters for xgboost
    param = {}
    param['objective']        = BDT_objective
    param['eval_metric']      = ['error', 'auc', 'logloss']
    param['min_child_weight'] = BDT_minchildwgt
    param['eta']              = BDT_eta
    param['max_depth']        = BDT_maxdepth
    param['subsample']        = BDT_subsample
    # param['gamma']=0.01
    # param['colsample_bytree'] = 0.8

    print 'Starting training...'
    print 'Using %d vars:' % len(input_vars)
    print input_vars

    watchlist = [ (d_train, 'train'), (d_test, 'eval') ]

    # start training
    algo = xgb.train(param, d_train, BDT_num_round, watchlist, early_stopping_rounds=20)
    
    # save the model
    algo.save_model(modeloutput)

    # print out variable ranking
    scores = algo.get_score()
    ivar = 1
    for k in sorted(scores, key=scores.get, reverse=True):
        print "%2d. %24s: %s" % (ivar, k, str(scores[k]))
        ivar = ivar + 1

    # fill MVA scores and make root file
    dmat = xgb.DMatrix(X, label=Y)
    preds = algo.predict(dmat)
    train_samp['eventBDTdisc'] = preds

    print('Writing prediction file to %s' % rootoutput)
    array2root(train_samp.to_records(index=False), filename=rootoutput, treename=treename, mode='RECREATE')
    model = algo.get_dump()
    return model

#makes tmva decision trees
def build_tree(xgtree, base_xml_element, var_indices):
    regex_float_pattern = r'[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?'
    parent_element_dict = {'0':base_xml_element}
    pos_dict = {'0':'s'}
    for line in xgtree.split('\n'):
        if not line: continue
        if ':leaf=' in line:
            # leaf node
            result = re.match(r'(\t*)(\d+):leaf=({0})$'.format(regex_float_pattern), line)
            if not result:
                print line
            depth = result.group(1).count('\t')
            inode = result.group(2)
            res = result.group(3)
            node_elementTree = ET.SubElement(parent_element_dict[inode], "Node", pos=str(pos_dict[inode]),
                                             depth=str(depth), NCoef="0", IVar="-1", Cut="0.0e+00", cType="1", res=str(res), rms="0.0e+00", purity="0.0e+00", nType="-99")
        else:
            # \t\t3:[var_topcand_mass<138.19] yes=7,no=8,missing=7
            result = re.match(r'(\t*)([0-9]+):\[(?P<var>.+)<(?P<cut>{0})\]\syes=(?P<yes>\d+),no=(?P<no>\d+)'.format(regex_float_pattern), line)
            if not result:
                print line
            depth = result.group(1).count('\t')
            inode = result.group(2)
            var = result.group('var')
            cut = result.group('cut')
            lnode = result.group('yes')
            rnode = result.group('no')
            pos_dict[lnode] = 'l'
            pos_dict[rnode] = 'r'
            node_elementTree = ET.SubElement(parent_element_dict[inode], "Node", pos=str(pos_dict[inode]),
                                             depth=str(depth), NCoef="0", IVar=str(var_indices[var]), Cut=str(cut),
                                             cType="1", res="0.0e+00", rms="0.0e+00", purity="0.0e+00", nType="0")
            parent_element_dict[lnode] = node_elementTree
            parent_element_dict[rnode] = node_elementTree

#convert to TMVA compatible XML file
def convert_model(model, input_variables, output_xml):
    NTrees = len(model)
    var_list = input_variables
    var_indices = {}

    # <MethodSetup>
    MethodSetup = ET.Element("MethodSetup", Method="BDT::BDT")

    # <Variables>
    Variables = ET.SubElement(MethodSetup, "Variables", NVar=str(len(var_list)))
    for ind, val in enumerate(var_list):
        name = val[0]
        var_type = val[1]
        var_indices[name] = ind
        Variable = ET.SubElement(Variables, "Variable", VarIndex=str(ind), Type=val[1],
            Expression=name, Label=name, Title=name, Unit="", Internal=name,
            Min="0.0e+00", Max="0.0e+00")

    # <GeneralInfo>
    GeneralInfo = ET.SubElement(MethodSetup, "GeneralInfo")
    Info_Creator = ET.SubElement(GeneralInfo, "Info", name="Creator", value="xgboost2TMVA")
    Info_AnalysisType = ET.SubElement(GeneralInfo, "Info", name="AnalysisType", value="Classification")

    # <Options>
    Options = ET.SubElement(MethodSetup, "Options")
    Option_NodePurityLimit = ET.SubElement(Options, "Option", name="NodePurityLimit", modified="No").text = "5.00e-01"
    Option_BoostType = ET.SubElement(Options, "Option", name="BoostType", modified="Yes").text = "Grad"

    # <Weights>
    Weights = ET.SubElement(MethodSetup, "Weights", NTrees=str(NTrees), AnalysisType="1")

    for itree in range(NTrees):
        BinaryTree = ET.SubElement(Weights, "BinaryTree", type="DecisionTree", boostWeight="1.0e+00", itree=str(itree))
        build_tree(model[itree], BinaryTree, var_indices)

    tree = ET.ElementTree(MethodSetup)
    tree.write(output_xml)

def plotROC(y_score, X_input, y_true, sample_weight=None):
    from sklearn.metrics import auc, roc_curve
    fpr, tpr, _ = roc_curve(y_true, y_score, sample_weight=sample_weight)
    roc_auc = auc(fpr, tpr, reorder=True)

    plt.figure()
    plt.plot(tpr, 1 - fpr, label='MVA (area = %0.3f)' % roc_auc)
#     plt.plot(tpr_tau21, fpr_tau21, label=r'$\tau_{21}$ (area = %0.3f)' % tau21_auc)
    plt.plot([0, 1], [1, 0], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Tagging eff.')
    plt.ylabel('Mistag rate')
#     plt.title('Receiver operating characteristic example')
    plt.legend(loc='best')
    plt.grid()



#put it all together! ###############################################################################3
trainfile = loadSample()
model     = runXGBoost(trainfile)

if makeXML:
    use_vars = [(k, 'F') for k in varsF] + [(k, 'I') for k in varsI]
    convert_model(model, input_variables=use_vars, output_xml=xmloutput)

# plotROC(bst.predict(d_test), X_test, y_test, wgt0_test)  # here we use the natural weight as in the sample
# plt.ylim(0.8, 1)
# plt.savefig('roc_xgb_nano_nvar%d.pdf' % len(input_vars))

