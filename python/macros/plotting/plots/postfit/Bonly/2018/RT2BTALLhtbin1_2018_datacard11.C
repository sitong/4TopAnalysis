void RT2BTALLhtbin1_2018_datacard11()
{
//=========Macro generated from canvas: c/c
//=========  (Wed Aug 24 23:03:27 2022) by ROOT version 6.12/07
   TCanvas *c = new TCanvas("c", "c",0,0,1000,1000);
   gStyle->SetOptStat(0);
   c->SetHighLightColor(2);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "p1",0,0.3,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-1.125,-16.11288,10.125,145.016);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("");
   stack->SetMaximum(122.7648);
   
   TH1F *stack_stack_12 = new TH1F("stack_stack_12","",9,0,9);
   stack_stack_12->SetMinimum(0);
   stack_stack_12->SetMaximum(128.9031);
   stack_stack_12->SetDirectory(0);
   stack_stack_12->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_12->SetLineColor(ci);
   stack_stack_12->GetXaxis()->SetTitle("BDT discriminant");
   stack_stack_12->GetXaxis()->SetLabelFont(42);
   stack_stack_12->GetXaxis()->SetLabelSize(0.035);
   stack_stack_12->GetXaxis()->SetTitleSize(0.035);
   stack_stack_12->GetXaxis()->SetTitleFont(42);
   stack_stack_12->GetYaxis()->SetTitle("NEntries");
   stack_stack_12->GetYaxis()->SetLabelFont(43);
   stack_stack_12->GetYaxis()->SetLabelSize(20);
   stack_stack_12->GetYaxis()->SetTitleSize(20);
   stack_stack_12->GetYaxis()->SetTitleOffset(1.55);
   stack_stack_12->GetYaxis()->SetTitleFont(43);
   stack_stack_12->GetZaxis()->SetLabelFont(42);
   stack_stack_12->GetZaxis()->SetLabelSize(0.035);
   stack_stack_12->GetZaxis()->SetTitleSize(0.035);
   stack_stack_12->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_12);
   
   
   TH1F *other_stack_1 = new TH1F("other_stack_1","other in RT2BTALLhtbin1_2018_datacard11",9,0,9);
   other_stack_1->SetBinContent(1,0.2432359);
   other_stack_1->SetBinContent(2,0.009765927);
   other_stack_1->SetBinContent(3,0.5098938);
   other_stack_1->SetBinContent(4,0.06185668);
   other_stack_1->SetBinContent(5,0.5681275);
   other_stack_1->SetBinContent(6,0.8995854);
   other_stack_1->SetBinContent(7,0.6248812);
   other_stack_1->SetBinContent(8,1.674898);
   other_stack_1->SetBinContent(9,1.345984);
   other_stack_1->SetEntries(9);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetLineColor(ci);
   other_stack_1->GetXaxis()->SetTitle("CMS_th1x");
   other_stack_1->GetXaxis()->SetLabelFont(42);
   other_stack_1->GetXaxis()->SetLabelSize(0.035);
   other_stack_1->GetXaxis()->SetTitleSize(0.035);
   other_stack_1->GetXaxis()->SetTitleFont(42);
   other_stack_1->GetYaxis()->SetTitle("Events / ( 1 )");
   other_stack_1->GetYaxis()->SetLabelFont(42);
   other_stack_1->GetYaxis()->SetLabelSize(0.035);
   other_stack_1->GetYaxis()->SetTitleSize(0.035);
   other_stack_1->GetYaxis()->SetTitleOffset(0);
   other_stack_1->GetYaxis()->SetTitleFont(42);
   other_stack_1->GetZaxis()->SetLabelFont(42);
   other_stack_1->GetZaxis()->SetLabelSize(0.035);
   other_stack_1->GetZaxis()->SetTitleSize(0.035);
   other_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(other_stack_1,"");
   
   TH1F *TTX_stack_2 = new TH1F("TTX_stack_2","TTX in RT2BTALLhtbin1_2018_datacard11",9,0,9);
   TTX_stack_2->SetBinContent(1,0.3805372);
   TTX_stack_2->SetBinContent(2,1.329999);
   TTX_stack_2->SetBinContent(3,2.085384);
   TTX_stack_2->SetBinContent(4,2.277744);
   TTX_stack_2->SetBinContent(5,1.790307);
   TTX_stack_2->SetBinContent(6,1.316732);
   TTX_stack_2->SetBinContent(7,2.278973);
   TTX_stack_2->SetBinContent(8,2.848853);
   TTX_stack_2->SetBinContent(9,2.174573);
   TTX_stack_2->SetEntries(9);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_2->SetLineColor(ci);
   TTX_stack_2->GetXaxis()->SetTitle("CMS_th1x");
   TTX_stack_2->GetXaxis()->SetLabelFont(42);
   TTX_stack_2->GetXaxis()->SetLabelSize(0.035);
   TTX_stack_2->GetXaxis()->SetTitleSize(0.035);
   TTX_stack_2->GetXaxis()->SetTitleFont(42);
   TTX_stack_2->GetYaxis()->SetTitle("Events / ( 1 )");
   TTX_stack_2->GetYaxis()->SetLabelFont(42);
   TTX_stack_2->GetYaxis()->SetLabelSize(0.035);
   TTX_stack_2->GetYaxis()->SetTitleSize(0.035);
   TTX_stack_2->GetYaxis()->SetTitleOffset(0);
   TTX_stack_2->GetYaxis()->SetTitleFont(42);
   TTX_stack_2->GetZaxis()->SetLabelFont(42);
   TTX_stack_2->GetZaxis()->SetLabelSize(0.035);
   TTX_stack_2->GetZaxis()->SetTitleSize(0.035);
   TTX_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(TTX_stack_2,"");
   
   TH1F *DDBKG_stack_3 = new TH1F("DDBKG_stack_3","DDBKG in RT2BTALLhtbin1_2018_datacard11",9,0,9);
   DDBKG_stack_3->SetBinContent(1,16.72869);
   DDBKG_stack_3->SetBinContent(2,27.89923);
   DDBKG_stack_3->SetBinContent(3,30.3198);
   DDBKG_stack_3->SetBinContent(4,42.0204);
   DDBKG_stack_3->SetBinContent(5,50.44384);
   DDBKG_stack_3->SetBinContent(6,52.90946);
   DDBKG_stack_3->SetBinContent(7,50.44745);
   DDBKG_stack_3->SetBinContent(8,57.68406);
   DDBKG_stack_3->SetBinContent(9,69.55374);
   DDBKG_stack_3->SetEntries(9);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_3->SetLineColor(ci);
   DDBKG_stack_3->GetXaxis()->SetTitle("CMS_th1x");
   DDBKG_stack_3->GetXaxis()->SetLabelFont(42);
   DDBKG_stack_3->GetXaxis()->SetLabelSize(0.035);
   DDBKG_stack_3->GetXaxis()->SetTitleSize(0.035);
   DDBKG_stack_3->GetXaxis()->SetTitleFont(42);
   DDBKG_stack_3->GetYaxis()->SetTitle("Events / ( 1 )");
   DDBKG_stack_3->GetYaxis()->SetLabelFont(42);
   DDBKG_stack_3->GetYaxis()->SetLabelSize(0.035);
   DDBKG_stack_3->GetYaxis()->SetTitleSize(0.035);
   DDBKG_stack_3->GetYaxis()->SetTitleOffset(0);
   DDBKG_stack_3->GetYaxis()->SetTitleFont(42);
   DDBKG_stack_3->GetZaxis()->SetLabelFont(42);
   DDBKG_stack_3->GetZaxis()->SetLabelSize(0.035);
   DDBKG_stack_3->GetZaxis()->SetTitleSize(0.035);
   DDBKG_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(DDBKG_stack_3,"");
   stack->Draw("hist");
   
   TH1F *datahist__45 = new TH1F("datahist__45","datahist",9,0,9);
   datahist__45->SetBinContent(1,18);
   datahist__45->SetBinContent(2,26);
   datahist__45->SetBinContent(3,32);
   datahist__45->SetBinContent(4,42);
   datahist__45->SetBinContent(5,48);
   datahist__45->SetBinContent(6,53);
   datahist__45->SetBinContent(7,62);
   datahist__45->SetBinContent(8,60);
   datahist__45->SetBinContent(9,78);
   datahist__45->SetBinError(1,4.794652);
   datahist__45->SetBinError(2,5.641959);
   datahist__45->SetBinError(3,6.195401);
   datahist__45->SetBinError(4,7.014221);
   datahist__45->SetBinError(5,7.459449);
   datahist__45->SetBinError(6,7.809797);
   datahist__45->SetBinError(7,8.401389);
   datahist__45->SetBinError(8,8.273814);
   datahist__45->SetBinError(9,9.356091);
   datahist__45->SetEntries(9);
   datahist__45->SetStats(0);

   ci = TColor::GetColor("#666666");
   datahist__45->SetLineColor(ci);
   datahist__45->SetLineWidth(2);
   datahist__45->SetMarkerStyle(20);
   datahist__45->GetXaxis()->SetLabelFont(42);
   datahist__45->GetXaxis()->SetLabelSize(0.035);
   datahist__45->GetXaxis()->SetTitleSize(0.035);
   datahist__45->GetXaxis()->SetTitleFont(42);
   datahist__45->GetYaxis()->SetLabelFont(42);
   datahist__45->GetYaxis()->SetLabelSize(0.035);
   datahist__45->GetYaxis()->SetTitleSize(0.035);
   datahist__45->GetYaxis()->SetTitleOffset(0);
   datahist__45->GetYaxis()->SetTitleFont(42);
   datahist__45->GetZaxis()->SetLabelFont(42);
   datahist__45->GetZaxis()->SetLabelSize(0.035);
   datahist__45->GetZaxis()->SetTitleSize(0.035);
   datahist__45->GetZaxis()->SetTitleFont(42);
   datahist__45->Draw("psame");
   
   TH1F *stacktot__46 = new TH1F("stacktot__46","stacktot",9,0,9);
   stacktot__46->SetBinContent(1,17.35247);
   stacktot__46->SetBinContent(2,29.23899);
   stacktot__46->SetBinContent(3,32.91508);
   stacktot__46->SetBinContent(4,44.36);
   stacktot__46->SetBinContent(5,52.80228);
   stacktot__46->SetBinContent(6,55.12578);
   stacktot__46->SetBinContent(7,53.35131);
   stacktot__46->SetBinContent(8,62.20781);
   stacktot__46->SetBinContent(9,73.0743);
   stacktot__46->SetEntries(27);
   stacktot__46->SetStats(0);

   ci = TColor::GetColor("#666666");
   stacktot__46->SetFillColor(ci);
   stacktot__46->SetFillStyle(3002);

   ci = TColor::GetColor("#000099");
   stacktot__46->SetLineColor(ci);
   stacktot__46->SetLineWidth(0);
   stacktot__46->GetXaxis()->SetLabelFont(42);
   stacktot__46->GetXaxis()->SetLabelSize(0.035);
   stacktot__46->GetXaxis()->SetTitleSize(0.035);
   stacktot__46->GetXaxis()->SetTitleFont(42);
   stacktot__46->GetYaxis()->SetLabelFont(42);
   stacktot__46->GetYaxis()->SetLabelSize(0.035);
   stacktot__46->GetYaxis()->SetTitleSize(0.035);
   stacktot__46->GetYaxis()->SetTitleOffset(0);
   stacktot__46->GetYaxis()->SetTitleFont(42);
   stacktot__46->GetZaxis()->SetLabelFont(42);
   stacktot__46->GetZaxis()->SetLabelSize(0.035);
   stacktot__46->GetZaxis()->SetTitleSize(0.035);
   stacktot__46->GetZaxis()->SetTitleFont(42);
   stacktot__46->Draw("e2same");
   
   TLegend *leg = new TLegend(0.1,0.6,0.3,0.9,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("other_stack_1","other","F");

   ci = TColor::GetColor("#d07e93");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#d07e93");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTTT","TTTT","F");

   ci = TColor::GetColor("#a1794a");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#a1794a");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTX_stack_2","TTX","F");

   ci = TColor::GetColor("#163d4e");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#163d4e");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("datahist","data","L");

   ci = TColor::GetColor("#666666");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("DDBKG_stack_3","DDBKG","F");

   ci = TColor::GetColor("#c1caf3");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#c1caf3");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "p2",0,0.05,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-1.125,0.05714285,10.125,1.771429);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetGridy();
   p2->SetBottomMargin(0.2);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   
   TH1F *raterr__47 = new TH1F("raterr__47","",9,0,9);
   raterr__47->SetBinContent(0,1);
   raterr__47->SetBinContent(1,1);
   raterr__47->SetBinContent(2,1);
   raterr__47->SetBinContent(3,1);
   raterr__47->SetBinContent(4,1);
   raterr__47->SetBinContent(5,1);
   raterr__47->SetBinContent(6,1);
   raterr__47->SetBinContent(7,1);
   raterr__47->SetBinContent(8,1);
   raterr__47->SetBinContent(9,1);
   raterr__47->SetBinContent(10,1);
   raterr__47->SetMinimum(0.4);
   raterr__47->SetMaximum(1.6);
   raterr__47->SetEntries(20);
   raterr__47->SetStats(0);

   ci = TColor::GetColor("#666666");
   raterr__47->SetFillColor(ci);
   raterr__47->SetFillStyle(3002);

   ci = TColor::GetColor("#666666");
   raterr__47->SetLineColor(ci);
   raterr__47->SetLineWidth(0);
   raterr__47->GetXaxis()->SetTitle("BDT discriminant");
   raterr__47->GetXaxis()->SetLabelFont(43);
   raterr__47->GetXaxis()->SetLabelSize(20);
   raterr__47->GetXaxis()->SetTitleSize(20);
   raterr__47->GetXaxis()->SetTitleOffset(4);
   raterr__47->GetXaxis()->SetTitleFont(43);
   raterr__47->GetYaxis()->SetTitle("ratio data/prediction");
   raterr__47->GetYaxis()->SetNdivisions(505);
   raterr__47->GetYaxis()->SetLabelFont(43);
   raterr__47->GetYaxis()->SetLabelSize(20);
   raterr__47->GetYaxis()->SetTitleSize(20);
   raterr__47->GetYaxis()->SetTitleOffset(1.55);
   raterr__47->GetYaxis()->SetTitleFont(43);
   raterr__47->GetZaxis()->SetLabelFont(42);
   raterr__47->GetZaxis()->SetLabelSize(0.035);
   raterr__47->GetZaxis()->SetTitleSize(0.035);
   raterr__47->GetZaxis()->SetTitleFont(42);
   raterr__47->Draw("e2");
   
   TH1F *ratio__48 = new TH1F("ratio__48","datahist",9,0,9);
   ratio__48->SetBinContent(1,1.037316);
   ratio__48->SetBinContent(2,0.8892235);
   ratio__48->SetBinContent(3,0.9721987);
   ratio__48->SetBinContent(4,0.946799);
   ratio__48->SetBinContent(5,0.9090517);
   ratio__48->SetBinContent(6,0.9614377);
   ratio__48->SetBinContent(7,1.162108);
   ratio__48->SetBinContent(8,0.9645091);
   ratio__48->SetBinContent(9,1.067407);
   ratio__48->SetBinError(1,0.2663696);
   ratio__48->SetBinError(2,0.2169984);
   ratio__48->SetBinError(3,0.1936063);
   ratio__48->SetBinError(4,0.1670053);
   ratio__48->SetBinError(5,0.1554052);
   ratio__48->SetBinError(6,0.1473547);
   ratio__48->SetBinError(7,0.1355063);
   ratio__48->SetBinError(8,0.1378969);
   ratio__48->SetBinError(9,0.1199499);
   ratio__48->SetEntries(290.8875);
   ratio__48->SetStats(0);
   ratio__48->SetLineWidth(2);
   ratio__48->SetMarkerStyle(20);
   ratio__48->GetXaxis()->SetLabelFont(42);
   ratio__48->GetXaxis()->SetLabelSize(0.035);
   ratio__48->GetXaxis()->SetTitleSize(0.035);
   ratio__48->GetXaxis()->SetTitleFont(42);
   ratio__48->GetYaxis()->SetLabelFont(42);
   ratio__48->GetYaxis()->SetLabelSize(0.035);
   ratio__48->GetYaxis()->SetTitleSize(0.035);
   ratio__48->GetYaxis()->SetTitleOffset(0);
   ratio__48->GetYaxis()->SetTitleFont(42);
   ratio__48->GetZaxis()->SetLabelFont(42);
   ratio__48->GetZaxis()->SetLabelSize(0.035);
   ratio__48->GetZaxis()->SetTitleSize(0.035);
   ratio__48->GetZaxis()->SetTitleFont(42);
   ratio__48->Draw("pe0same");
   TLine *line = new TLine(0,1,1,1);
   line->Draw();
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
