## card: ./datacards//allhad4top_RT2BTALLhtbin0_2016_datacard10.txt number in set: 10
##selection: (nrestops_nosys>=2 && nbstops_nosys>=0 && ht_nosys<1100 &&  nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag))*weight*btagSF_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*35.92*prefirewgt*

#----------------------------------------------#
#generating datacards. name: allhad4top, year2016

#number of cards in set: 12

## number of signals (i), backgrounds (j), and uncertainties (k)
---
imax: 1
jmax: 3
kmax: *
---

## input the bdt shape histograms
---
shapes * * allhad4top_2016_shapehists.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
---

## list the bin label and the number of (data) events observed
---
bin  RT2BTALLhtbin0_2016
observation 501
---

## expected events for signal and all backgrounds in the bins
---
bin        RT2BTALLhtbin0_2016    RT2BTALLhtbin0_2016    RT2BTALLhtbin0_2016    RT2BTALLhtbin0_2016
process    TTTT    TTX    other    DDBKG
process        0        1        2        3
rate       2.16471598367    24.7420977205    10.7331316474    463.831
---

## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin
---
## statistical uncertainties

RT2BTALLhtbin0_2016_TTTT_stat      lnN    1.02577876818    -    -    -
RT2BTALLhtbin0_2016_TTX_stat      lnN    -    1.05448094093    -    -
RT2BTALLhtbin0_2016_other_stat      lnN    -    -    1.14954665568    -
RT2BTALLhtbin0_2016_DDBKG_stat      lnN    -    -    -    1.09685855409

---
## systematic uncertainties

lumi_2016   lnN    1.025    1.025    1.025    -    
elveto_2016   lnN    1.03797     1.01367    -    -    
muveto_2016   lnN    1.00788     1.00178    -    -    
prefire_2016   lnN    1.00397     1.00301    -    -    
cross_section   lnN   -   1.25    -    -    
data-pred_norm_disagreement_2016    lnN        -         -         -      1.274

---
## shape uncertainties

jes_2016   shape     1.0   1.0   1.0    - 
jer_2016   shape     1.0   1.0   1.0    - 
pileup_2016   shape     1.0   1.0   1.0    - 
trigger_2016   shape     1.0   1.0   1.0    - 
btagLF  shape     1.0    1.0    1.0    - 
btagHF  shape     1.0    1.0    1.0    - 
btagLFstats1_2016   shape     1.0   1.0   1.0    - 
btagHFstats1_2016   shape     1.0   1.0   1.0    - 
btagLFstats2_2016   shape     1.0   1.0   1.0    - 
btagHFstats2_2016   shape     1.0   1.0   1.0    - 
btagCFerr1  shape     1.0    1.0    1.0    - 
btagCFerr2  shape     1.0    1.0    1.0    - 
DeepAK8TopSF_2016   shape     1.0   1.0   1.0    - 
DeepAK8WSF_2016   shape     1.0   1.0   1.0    - 
isr_TTTT  shape     1.0    -    -    -  
isr_TTX  shape     -    1.0    -    -  
fsr_TTTT  shape     1.0    -    -    -  
fsr_TTX  shape     -    1.0    -    -  
pdf  shape     1.0    1.0    -    - 
ME_TTTT  shape     1.0    -    -    -  
ME_TTX  shape     -    1.0    -    -  
ResTopEff_2016   shape     1.0   1.0   1.0    - 
ResTopMiss_2016   shape     1.0   1.0   1.0    - 

---

RT2BTALLhtbin0_2016 autoMCStats 0 0
---

