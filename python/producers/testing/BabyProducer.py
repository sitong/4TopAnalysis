#module for VERY BASIC event level postprocessing
import os
import numpy as np
import ROOT
import sys
import math
import itertools
from operator import mul
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
# import PhysicsTools.NanoTrees.helpers.LeptonHelper as lepsel
from  PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper

# from PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer import btagSFProducer
# from PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer import puWeightProducer

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')

class _NullObject:
    '''An null object which does not store anything, and does not raise exception.'''
    def __bool__(self):
        return False
    def __nonzero__(self):
        return False
    def __getattr__(self, name):
        pass
    def __setattr__(self, name, value):
        pass

#class definition
class BabyProducer(Module):
    def __init__(self):
        self.deepbs     = True
        self.tightlep   = False

        self.minJet_pt  = 35.0
        self.maxJet_eta = 2.4
        self.deepCSV_M  = 0.6324
        self.minBJet_pt = 35.0
        self.DeepAK8TopWP  = 0.8511
        self.DeepAK8TopPt  = 400.0
        self.DeepAK8WWP    = 0.8767
        self.DeepAK8WPt    = 200.0
        self.mu_miniiso = 0.2
        self.el_miniiso = 0.1
        if self.tightlep:
            self.mu_pt = 26.0
            self.mu_eta = 2.1
            self.mu_ID = 1 #(1 as flag)
            self.mu_pfiso = 0.15 #(1lep tight: 0.15 1lep loose: 0.25) #0.2
            self.el_pt = 35.0 #(15 or 35)
            self.el_eta = 2.1 #(2.1 or 2.5)
            self.el_ID = 4 #(1 is veto 4 is tight)
        elif not self.tightlep:
            self.mu_pt = 10.0 #(26 or 10)
            self.mu_eta = 2.5 #(2.1 or 2.5)
            self.mu_ID = 1 #(1 as flag)
            self.mu_pfiso = 0.25 #(1lep tight: 0.15 1lep loose: 0.25) #0.2
            self.el_pt = 15.0 #(15 or 35)
            self.el_eta = 2.5 #(2.1 or 2.5)
            self.el_ID = 1 #(1 is veto 4 is tight)
        
        # self.mu_dxy = 0.2
        # self.mu_dz = 0.5
        # self.el_pfiso =  0.2
        # self.el_dxy = 0.2
        # self.el_dz = 0.5

    def beginJob(self):
        # self.jh = JetHelper(self.deepbs)
        pass
    def endJob(self):
        pass

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        self.out.branch("evt", "I")
        self.out.branch("run", "I")
        self.out.branch("lumi", "I")
        # self.out.branch("genWgt", "I")
        # self.out.branch("goodverticesflag", "I")
        # self.out.branch("haloflag", "I")
        # self.out.branch("HBHEflag", "I")
        # self.out.branch("HBHEisoflag", "I")
        # self.out.branch("ecaldeadcellflag", "I")
        # self.out.branch("badmuonflag", "I")
        # self.out.branch("ht", "F")
        # self.out.branch("leadjpt", "F")
        # self.out.branch("njets", "I")
        # self.out.branch("nbjets", "I")
        # self.out.branch("nbstops", "I")
        # self.out.branch("nbsws", "I")
        # self.out.branch("bstop1_pt", "F")
        # self.out.branch("bstop2_pt", "F")
        # # self.out.branch("bstop1_pt", "F")
        # # self.out.branch("bstop1_pt", "F")
        # self.out.branch("HLT_PFHT450_SixJet40_BTagCSV_p056", "I")
        # self.out.branch("HLT_PFHT400_SixJet30_DoubleBTagCSV_p056", "I")
        # self.out.branch("nleptons_pf", "I")
        # self.out.branch("nMu_pf", "I")
        # self.out.branch("nEl_pf", "I")
        # self.out.branch("nleptons_mini", "I")
        # self.out.branch("nMu_mini", "I")
        # self.out.branch("nEl_mini", "I")
        # self.out.branch("pass0a", "F", lenVar="nJet")
        # self.out.branch("jetpt", "F", lenVar="nJet")
        # self.out.branch("jeteta", "F", lenVar="nJet")
        # self.out.branch("jetID", "I", lenVar="nJet")

    def SelBoostedWs(self, fatj):
        if fatj.deepTag_WvsQCD > self.DeepAK8WWP and fatj.pt > self.DeepAK8WPt:
            return 1
        return 0

    def SelBoostedTops(self, fatj):
        if fatj.deepTag_TvsQCD > self.DeepAK8TopWP and fatj.pt > self.DeepAK8TopPt:
            return 1
        return 0
        
    def _fillBoostVars(self, event): #can probs vectorize the selections for speed
        nbstops = 0; nbsws = 0; bstop1_pt = 0.0; bstop2_pt = 0.0
        bstcands = []; bswcands = []

        fatjets = Collection(event,"FatJet")
        for fj in fatjets:
            if (fj.pt>=self.minJet_pt and abs(fj.eta)<=self.maxJet_eta):
                #selection 
                seltop = self.SelBoostedTops(fj)
                selw = self.SelBoostedWs(fj)
                if seltop: #and not selw:
                    bstcands.append(fj)
                if selw: #and not seltop:
                    bswcands.append(fj)
                # elif seltop and selw: #fills tops first
                #     bstcands.append(fj)
        nbstops = len(bstcands)
        nbsws = len(bswcands)
        if nbstops>=1:
            bstop1_pt = bstcands[0].pt
        if nbstops>=2:
            bstop2_pt = bstcands[1].pt

        self.out.fillBranch("nbstops",   nbstops)
        self.out.fillBranch("nbsws",     nbsws)
        self.out.fillBranch("bstop1_pt", bstop1_pt)
        self.out.fillBranch("bstop2_pt", bstop2_pt)

    def _fillEventVars(self, event):
        event.flags   = Object(event,"Flag")

        genWeight = 1
        if event.genWeight<0:
            genWeight = -1

        #fill branches
        self.out.fillBranch("genWgt",           genWeight )
        self.out.fillBranch("goodverticesflag", event.flags.goodVertices)
        self.out.fillBranch("haloflag",         event.flags.globalTightHalo2016Filter)
        self.out.fillBranch("HBHEflag",         event.flags.HBHENoiseFilter)
        self.out.fillBranch("HBHEisoflag",      event.flags.HBHENoiseIsoFilter)
        self.out.fillBranch("ecaldeadcellflag", event.flags.EcalDeadCellTriggerPrimitiveFilter)
        self.out.fillBranch("badmuonflag",      event.flags.BadPFMuonFilter)
        self.out.fillBranch("evt",              int(event.event))
        self.out.fillBranch("run",              int(event.run))
        self.out.fillBranch("lumi",             event.luminosityBlock)
        
    def _fillJetVars(self, event):
        event.ak4jets = Collection(event,"Jet") 
        njets=0
        nbjets=0
        ht = 0.0
        jetpts = []; jetids = []; jetetas = []; pass0a = []

        filledtjpt = False
        leadjpt = 0.0
        for j in event.ak4jets:
            jetpts.append(j.pt); jetetas.append(j.eta); jetids.append(j.jetId)
            step0abool = 0
            if (j.pt>=self.minJet_pt and abs(j.eta)<=self.maxJet_eta and j.jetId>=1):
                step0abool = 1
                if not filledtjpt:
                    leadjpt = j.pt
                    filledtjpt=True
                njets+=1
                ht+=j.pt
                if (j.btagDeepB>=self.deepCSV_M and j.pt>=self.minBJet_pt and abs(j.eta)<=self.maxJet_eta):
                    nbjets+=1
            pass0a.append(step0abool)
        
        # print pass0a
        self.out.fillBranch("ht",       ht )
        self.out.fillBranch("leadjpt",  leadjpt )
        self.out.fillBranch("njets",    njets)
        self.out.fillBranch("nbjets",   nbjets)
        self.out.fillBranch("HLT_PFHT400_SixJet30_DoubleBTagCSV_p056",   event.HLT_PFHT400_SixJet30_DoubleBTagCSV_p056)
        self.out.fillBranch("HLT_PFHT450_SixJet40_BTagCSV_p056",   event.HLT_PFHT450_SixJet40_BTagCSV_p056)
        self.out.fillBranch("jetpt",       jetpts )
        self.out.fillBranch("jetID",       jetids )
        self.out.fillBranch("jeteta",      jetetas )
        self.out.fillBranch("pass0a",      pass0a )

    def _fillLepVars(self, event):
        muons     = Collection(event, "Muon")
        electrons = Collection(event, "Electron")
        nmus_pf = 0;   nels_pf = 0;   nleps_pf = 0
        nmus_mini = 0; nels_mini = 0; nleps_mini = 0
        for mu in muons:
            # if (    mu.pt               >= self.mu_pt 
            #         and abs(mu.eta)         < self.mu_eta 
            #     # and abs(mu.dxy)         < 0.2 
            #     # and abs(mu.dz)          < 0.5
            #     # and mu.mediumId         >= self.mu_ID): #medium
            #     # if (mu.miniPFRelIso_all < self.mu_miniiso): #old loose: 0.2 #new loose: 0.4 #new med: 0.2
            #     #     nmus_mini += 1
            #     # if (mu.pfRelIso04_all < self.mu_pfiso:  #loose
            #     #     nmus_pf += 1

            #    #test 1lep lepsel (loose)
            #         and ((self.tightlep and mu.tightId >= self.mu_ID) or (not self.tightlep and mu.mediumId >= self.mu_ID))):
                if (mu.miniPFRelIso_all <= self.mu_miniiso): #old loose: 0.2 #new loose: 0.4 #new med: 0.2
                    nmus_mini += 1
                if (mu.pt>=26 and abs(mu.eta)<2.1 and mu.pfRelIso04_all<=0.15 and mu.tightId==1):  #loose
                    nmus_pf += 1
                
        for el in electrons:
            pfWP = 0.0
            # if (    el.pt               >= self.el_pt
            #         and abs(el.eta)         < self.el_eta 
            #     # and abs(el.dxy)         < _emaxD0 
            #     # and abs(el.dz)          < _emaxDZ 

            #     # and el.cutBased        >= self.el_ID): #veto
            #     # if (el.miniPFRelIso_all < self.el_miniiso): #medium
            #     #     nels_mini += 1
            #     # #working pt defs
            #     # if abs(el.eta)<=1.479:
            #     #     if el.pfRelIso03_all < 0.0478+(0.506/el.pt):
            #     #         nels_pf+=1
            #     # if abs(el.eta)>1.479:
            #     #     if el.pfRelIso03_all < 0.0658+(0.963/el.pt):
            #     #         nels_pf += 1

            #     #test 1lep el lepsel (vetoid)
            #         and el.cutBased        >= self.el_ID): #veto
            if (el.pt>=35 and abs(el.eta)<2.1 and el.cutBased>=4):
                nels_pf += 1

            if (el.miniPFRelIso_all < self.el_miniiso): #medium
                nels_mini += 1
                    
        nleps_pf   = nels_pf   + nmus_pf
        nleps_mini = nels_mini + nmus_mini
        #print 'nmus_pf', nmus_pf, 'nmus_mini', nmus_mini

        self.out.fillBranch("nleptons_pf",   nleps_pf)
        self.out.fillBranch("nEl_pf",        nels_pf)
        self.out.fillBranch("nMu_pf",        nmus_pf)
        self.out.fillBranch("nleptons_mini", nleps_mini)
        self.out.fillBranch("nEl_mini",      nels_mini)
        self.out.fillBranch("nMu_mini",      nmus_mini)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    def analyze(self, event):
        self.out.fillBranch("evt",              int(event.event))
        self.out.fillBranch("run",              int(event.run))
        self.out.fillBranch("lumi",             event.luminosityBlock)

        # self._fillEventVars(event)
        # self._fillJetVars(event)
        # self._fillLepVars(event)
        # self._fillBoostVars(event)
        return True

print "BABY PRODUCER"
BabyTrees = lambda : BabyProducer()
# def BasicTrees():
#     return BasicVarProducer()
