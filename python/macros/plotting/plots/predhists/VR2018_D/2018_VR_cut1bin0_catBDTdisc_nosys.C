void 2018_VR_cut1bin0_catBDTdisc_nosys()
{
//=========Macro generated from canvas: c/c
//=========  (Wed Aug 24 21:34:44 2022) by ROOT version 6.12/07
   TCanvas *c = new TCanvas("c", "c",0,0,800,800);
   gStyle->SetOptStat(0);
   c->SetHighLightColor(2);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "p1",0,0.3,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-0.125,-34.02148,1.125,306.1933);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("");
   stack->SetMaximum(259.2113);
   Double_t xAxis27[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *stack_stack_3 = new TH1F("stack_stack_3","",9, xAxis27);
   stack_stack_3->SetMinimum(0);
   stack_stack_3->SetMaximum(272.1719);
   stack_stack_3->SetDirectory(0);
   stack_stack_3->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_3->SetLineColor(ci);
   stack_stack_3->GetXaxis()->SetTitle("BDT discriminant");
   stack_stack_3->GetXaxis()->SetLabelFont(42);
   stack_stack_3->GetXaxis()->SetLabelSize(0.035);
   stack_stack_3->GetXaxis()->SetTitleSize(0.035);
   stack_stack_3->GetXaxis()->SetTitleFont(42);
   stack_stack_3->GetYaxis()->SetTitle("NEntries");
   stack_stack_3->GetYaxis()->SetLabelFont(43);
   stack_stack_3->GetYaxis()->SetLabelSize(15);
   stack_stack_3->GetYaxis()->SetTitleSize(20);
   stack_stack_3->GetYaxis()->SetTitleOffset(1.55);
   stack_stack_3->GetYaxis()->SetTitleFont(43);
   stack_stack_3->GetZaxis()->SetLabelFont(42);
   stack_stack_3->GetZaxis()->SetLabelSize(0.035);
   stack_stack_3->GetZaxis()->SetTitleSize(0.035);
   stack_stack_3->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_3);
   
   Double_t xAxis28[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *hist_stack_1 = new TH1F("hist_stack_1","hist",9, xAxis28);
   hist_stack_1->SetBinContent(1,0.001246175);
   hist_stack_1->SetBinContent(2,0.004899678);
   hist_stack_1->SetBinContent(3,0.01100122);
   hist_stack_1->SetBinContent(4,0.02162141);
   hist_stack_1->SetBinContent(5,0.04428863);
   hist_stack_1->SetBinContent(6,0.0869857);
   hist_stack_1->SetBinContent(7,0.1563425);
   hist_stack_1->SetBinContent(8,0.2490054);
   hist_stack_1->SetBinContent(9,0.2722974);
   hist_stack_1->SetBinError(1,0.0004871043);
   hist_stack_1->SetBinError(2,0.001492247);
   hist_stack_1->SetBinError(3,0.002476545);
   hist_stack_1->SetBinError(4,0.003782703);
   hist_stack_1->SetBinError(5,0.004891489);
   hist_stack_1->SetBinError(6,0.006493307);
   hist_stack_1->SetBinError(7,0.008230841);
   hist_stack_1->SetBinError(8,0.01044879);
   hist_stack_1->SetBinError(9,0.01078814);
   hist_stack_1->SetEntries(13671);
   hist_stack_1->SetDirectory(0);
   hist_stack_1->SetStats(0);

   ci = TColor::GetColor("#a1794a");
   hist_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#a1794a");
   hist_stack_1->SetLineColor(ci);
   hist_stack_1->GetXaxis()->SetLabelFont(42);
   hist_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_stack_1->GetXaxis()->SetTitleFont(42);
   hist_stack_1->GetYaxis()->SetLabelFont(42);
   hist_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_stack_1->GetYaxis()->SetTitleOffset(0);
   hist_stack_1->GetYaxis()->SetTitleFont(42);
   hist_stack_1->GetZaxis()->SetLabelFont(42);
   hist_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_stack_1,"");
   Double_t xAxis29[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *hist_stack_2 = new TH1F("hist_stack_2","hist",9, xAxis29);
   hist_stack_2->SetBinContent(1,0.06786993);
   hist_stack_2->SetBinContent(2,0.1838822);
   hist_stack_2->SetBinContent(3,0.1734989);
   hist_stack_2->SetBinContent(4,2.066835);
   hist_stack_2->SetBinContent(5,2.070033);
   hist_stack_2->SetBinContent(6,1.271041);
   hist_stack_2->SetBinContent(7,2.756721);
   hist_stack_2->SetBinContent(8,1.744334);
   hist_stack_2->SetBinContent(9,1.760557);
   hist_stack_2->SetBinError(1,0.05565924);
   hist_stack_2->SetBinError(2,0.3146549);
   hist_stack_2->SetBinError(3,0.4713699);
   hist_stack_2->SetBinError(4,0.7127103);
   hist_stack_2->SetBinError(5,0.7764362);
   hist_stack_2->SetBinError(6,0.6766119);
   hist_stack_2->SetBinError(7,0.6897078);
   hist_stack_2->SetBinError(8,0.8034357);
   hist_stack_2->SetBinError(9,0.5704143);
   hist_stack_2->SetEntries(6232);
   hist_stack_2->SetDirectory(0);
   hist_stack_2->SetStats(0);

   ci = TColor::GetColor("#163d4e");
   hist_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#163d4e");
   hist_stack_2->SetLineColor(ci);
   hist_stack_2->GetXaxis()->SetLabelFont(42);
   hist_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_stack_2->GetXaxis()->SetTitleFont(42);
   hist_stack_2->GetYaxis()->SetLabelFont(42);
   hist_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_stack_2->GetYaxis()->SetTitleOffset(0);
   hist_stack_2->GetYaxis()->SetTitleFont(42);
   hist_stack_2->GetZaxis()->SetLabelFont(42);
   hist_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_stack_2,"");
   Double_t xAxis30[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *hist_stack_3 = new TH1F("hist_stack_3","hist",9, xAxis30);
   hist_stack_3->SetBinContent(1,0.02533206);
   hist_stack_3->SetBinContent(2,0.3053554);
   hist_stack_3->SetBinContent(3,0.7579564);
   hist_stack_3->SetBinContent(4,1.737101);
   hist_stack_3->SetBinContent(5,1.270705);
   hist_stack_3->SetBinContent(6,2.501719);
   hist_stack_3->SetBinContent(7,2.84939);
   hist_stack_3->SetBinContent(8,1.927753);
   hist_stack_3->SetBinContent(9,0.4999112);
   hist_stack_3->SetBinError(1,0.02533206);
   hist_stack_3->SetBinError(2,0.1678264);
   hist_stack_3->SetBinError(3,0.3250257);
   hist_stack_3->SetBinError(4,0.6265625);
   hist_stack_3->SetBinError(5,0.4467484);
   hist_stack_3->SetBinError(6,0.6035988);
   hist_stack_3->SetBinError(7,0.8422685);
   hist_stack_3->SetBinError(8,0.5380658);
   hist_stack_3->SetBinError(9,0.2104031);
   hist_stack_3->SetEntries(135);
   hist_stack_3->SetDirectory(0);
   hist_stack_3->SetStats(0);

   ci = TColor::GetColor("#d07e93");
   hist_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#d07e93");
   hist_stack_3->SetLineColor(ci);
   hist_stack_3->GetXaxis()->SetLabelFont(42);
   hist_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_stack_3->GetXaxis()->SetTitleFont(42);
   hist_stack_3->GetYaxis()->SetLabelFont(42);
   hist_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_stack_3->GetYaxis()->SetTitleOffset(0);
   hist_stack_3->GetYaxis()->SetTitleFont(42);
   hist_stack_3->GetZaxis()->SetLabelFont(42);
   hist_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_stack_3,"");
   Double_t xAxis31[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *hist_stack_4 = new TH1F("hist_stack_4","hist",9, xAxis31);
   hist_stack_4->SetBinContent(1,14.6173);
   hist_stack_4->SetBinContent(2,28.79497);
   hist_stack_4->SetBinContent(3,51.4353);
   hist_stack_4->SetBinContent(4,75.28457);
   hist_stack_4->SetBinContent(5,99.68336);
   hist_stack_4->SetBinContent(6,131.226);
   hist_stack_4->SetBinContent(7,170.5718);
   hist_stack_4->SetBinContent(8,139.4688);
   hist_stack_4->SetBinContent(9,73.636);
   hist_stack_4->SetBinError(1,3.438242);
   hist_stack_4->SetBinError(2,6.542559);
   hist_stack_4->SetBinError(3,11.49498);
   hist_stack_4->SetBinError(4,16.71051);
   hist_stack_4->SetBinError(5,22.0458);
   hist_stack_4->SetBinError(6,28.94297);
   hist_stack_4->SetBinError(7,37.54623);
   hist_stack_4->SetBinError(8,30.74534);
   hist_stack_4->SetBinError(9,16.35);
   hist_stack_4->SetEntries(7140);
   hist_stack_4->SetDirectory(0);
   hist_stack_4->SetStats(0);

   ci = TColor::GetColor("#c1caf3");
   hist_stack_4->SetFillColor(ci);

   ci = TColor::GetColor("#c1caf3");
   hist_stack_4->SetLineColor(ci);
   hist_stack_4->GetXaxis()->SetLabelFont(42);
   hist_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_stack_4->GetXaxis()->SetTitleFont(42);
   hist_stack_4->GetYaxis()->SetLabelFont(42);
   hist_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_stack_4->GetYaxis()->SetTitleOffset(0);
   hist_stack_4->GetYaxis()->SetTitleFont(42);
   hist_stack_4->GetZaxis()->SetLabelFont(42);
   hist_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_stack_4,"");
   stack->Draw("hist");
   Double_t xAxis32[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *stacktot__17 = new TH1F("stacktot__17","stacktot",9, xAxis32);
   stacktot__17->SetBinContent(1,14.71174);
   stacktot__17->SetBinContent(2,29.28911);
   stacktot__17->SetBinContent(3,52.37775);
   stacktot__17->SetBinContent(4,79.11013);
   stacktot__17->SetBinContent(5,103.0684);
   stacktot__17->SetBinContent(6,135.0857);
   stacktot__17->SetBinContent(7,176.3342);
   stacktot__17->SetBinContent(8,143.3899);
   stacktot__17->SetBinContent(9,76.16877);
   stacktot__17->SetBinError(1,3.438786);
   stacktot__17->SetBinError(2,6.552271);
   stacktot__17->SetBinError(3,11.50923);
   stacktot__17->SetBinError(4,16.73743);
   stacktot__17->SetBinError(5,22.06399);
   stacktot__17->SetBinError(6,28.95717);
   stacktot__17->SetBinError(7,37.56201);
   stacktot__17->SetBinError(8,30.76054);
   stacktot__17->SetBinError(9,16.36131);
   stacktot__17->SetEntries(27178);
   stacktot__17->SetStats(0);

   ci = TColor::GetColor("#666666");
   stacktot__17->SetFillColor(ci);
   stacktot__17->SetFillStyle(3002);

   ci = TColor::GetColor("#000099");
   stacktot__17->SetLineColor(ci);
   stacktot__17->SetLineWidth(0);
   stacktot__17->GetXaxis()->SetLabelFont(42);
   stacktot__17->GetXaxis()->SetLabelSize(0.035);
   stacktot__17->GetXaxis()->SetTitleSize(0.035);
   stacktot__17->GetXaxis()->SetTitleFont(42);
   stacktot__17->GetYaxis()->SetLabelFont(42);
   stacktot__17->GetYaxis()->SetLabelSize(0.035);
   stacktot__17->GetYaxis()->SetTitleSize(0.035);
   stacktot__17->GetYaxis()->SetTitleOffset(0);
   stacktot__17->GetYaxis()->SetTitleFont(42);
   stacktot__17->GetZaxis()->SetLabelFont(42);
   stacktot__17->GetZaxis()->SetLabelSize(0.035);
   stacktot__17->GetZaxis()->SetTitleSize(0.035);
   stacktot__17->GetZaxis()->SetTitleFont(42);
   stacktot__17->Draw("e2same");
   Double_t xAxis33[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *histup__18 = new TH1F("histup__18","histup",9, xAxis33);
   histup__18->SetBinContent(1,13.89858);
   histup__18->SetBinContent(2,27.24562);
   histup__18->SetBinContent(3,49.63777);
   histup__18->SetBinContent(4,72.69176);
   histup__18->SetBinContent(5,95.41483);
   histup__18->SetBinContent(6,120.7852);
   histup__18->SetBinContent(7,170.0921);
   histup__18->SetBinContent(8,140.1991);
   histup__18->SetBinContent(9,94.75299);
   histup__18->SetBinError(1,1.238184);
   histup__18->SetBinError(2,1.733597);
   histup__18->SetBinError(3,2.339947);
   histup__18->SetBinError(4,2.831669);
   histup__18->SetBinError(5,3.244201);
   histup__18->SetBinError(6,3.650117);
   histup__18->SetBinError(7,4.331536);
   histup__18->SetBinError(8,3.932534);
   histup__18->SetBinError(9,3.23293);
   histup__18->SetEntries(14280);
   histup__18->SetDirectory(0);
   histup__18->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   histup__18->SetLineColor(ci);
   histup__18->SetLineWidth(3);
   histup__18->GetXaxis()->SetLabelFont(42);
   histup__18->GetXaxis()->SetLabelSize(0.035);
   histup__18->GetXaxis()->SetTitleSize(0.035);
   histup__18->GetXaxis()->SetTitleFont(42);
   histup__18->GetYaxis()->SetLabelFont(42);
   histup__18->GetYaxis()->SetLabelSize(0.035);
   histup__18->GetYaxis()->SetTitleSize(0.035);
   histup__18->GetYaxis()->SetTitleOffset(0);
   histup__18->GetYaxis()->SetTitleFont(42);
   histup__18->GetZaxis()->SetLabelFont(42);
   histup__18->GetZaxis()->SetLabelSize(0.035);
   histup__18->GetZaxis()->SetTitleSize(0.035);
   histup__18->GetZaxis()->SetTitleFont(42);
   histup__18->Draw("histesame");
   Double_t xAxis34[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *histdown__19 = new TH1F("histdown__19","histdown",9, xAxis34);
   histdown__19->SetBinContent(1,15.60644);
   histdown__19->SetBinContent(2,30.99306);
   histdown__19->SetBinContent(3,52.64425);
   histdown__19->SetBinContent(4,79.68075);
   histdown__19->SetBinContent(5,103.53);
   histdown__19->SetBinContent(6,143.3154);
   histdown__19->SetBinContent(7,170.0222);
   histdown__19->SetBinContent(8,137.4905);
   histdown__19->SetBinContent(9,51.4353);
   histdown__19->SetBinError(1,1.309663);
   histdown__19->SetBinError(2,1.84561);
   histdown__19->SetBinError(3,2.405377);
   histdown__19->SetBinError(4,2.959269);
   histdown__19->SetBinError(5,3.37319);
   histdown__19->SetBinError(6,3.968754);
   histdown__19->SetBinError(7,4.322754);
   histdown__19->SetBinError(8,3.887264);
   histdown__19->SetBinError(9,2.377597);
   histdown__19->SetEntries(14280);
   histdown__19->SetDirectory(0);
   histdown__19->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   histdown__19->SetLineColor(ci);
   histdown__19->SetLineWidth(3);
   histdown__19->GetXaxis()->SetLabelFont(42);
   histdown__19->GetXaxis()->SetLabelSize(0.035);
   histdown__19->GetXaxis()->SetTitleSize(0.035);
   histdown__19->GetXaxis()->SetTitleFont(42);
   histdown__19->GetYaxis()->SetLabelFont(42);
   histdown__19->GetYaxis()->SetLabelSize(0.035);
   histdown__19->GetYaxis()->SetTitleSize(0.035);
   histdown__19->GetYaxis()->SetTitleOffset(0);
   histdown__19->GetYaxis()->SetTitleFont(42);
   histdown__19->GetZaxis()->SetLabelFont(42);
   histdown__19->GetZaxis()->SetLabelSize(0.035);
   histdown__19->GetZaxis()->SetTitleSize(0.035);
   histdown__19->GetZaxis()->SetTitleFont(42);
   histdown__19->Draw("histesame");
   Double_t xAxis35[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *hist__20 = new TH1F("hist__20","hist",9, xAxis35);
   hist__20->SetBinContent(1,8);
   hist__20->SetBinContent(2,25);
   hist__20->SetBinContent(3,49);
   hist__20->SetBinContent(4,56);
   hist__20->SetBinContent(5,104);
   hist__20->SetBinContent(6,111);
   hist__20->SetBinContent(7,133);
   hist__20->SetBinContent(8,127);
   hist__20->SetBinContent(9,51);
   hist__20->SetBinError(1,2.828427);
   hist__20->SetBinError(2,5);
   hist__20->SetBinError(3,7);
   hist__20->SetBinError(4,7.483315);
   hist__20->SetBinError(5,10.19804);
   hist__20->SetBinError(6,10.53565);
   hist__20->SetBinError(7,11.53256);
   hist__20->SetBinError(8,11.26943);
   hist__20->SetBinError(9,7.141428);
   hist__20->SetEntries(664);
   hist__20->SetDirectory(0);
   hist__20->SetStats(0);

   ci = TColor::GetColor("#666666");
   hist__20->SetLineColor(ci);
   hist__20->SetLineWidth(2);
   hist__20->SetMarkerStyle(20);
   hist__20->GetXaxis()->SetLabelFont(42);
   hist__20->GetXaxis()->SetLabelSize(0.035);
   hist__20->GetXaxis()->SetTitleSize(0.035);
   hist__20->GetXaxis()->SetTitleFont(42);
   hist__20->GetYaxis()->SetLabelFont(42);
   hist__20->GetYaxis()->SetLabelSize(0.035);
   hist__20->GetYaxis()->SetTitleSize(0.035);
   hist__20->GetYaxis()->SetTitleOffset(0);
   hist__20->GetYaxis()->SetTitleFont(42);
   hist__20->GetZaxis()->SetLabelFont(42);
   hist__20->GetZaxis()->SetLabelSize(0.035);
   hist__20->GetZaxis()->SetTitleSize(0.035);
   hist__20->GetZaxis()->SetTitleFont(42);
   hist__20->Draw("psame");
   
   TLegend *leg = new TLegend(0.1,0.6,0.3,0.9,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("hist","data","L");

   ci = TColor::GetColor("#666666");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("histup","up shape","L");

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("histdown","down shape","L");

   ci = TColor::GetColor("#ff0000");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(3);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_stack_1","tttt","F");

   ci = TColor::GetColor("#a1794a");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#a1794a");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_stack_2","ttH+ttV","F");

   ci = TColor::GetColor("#163d4e");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#163d4e");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_stack_3","minor","F");

   ci = TColor::GetColor("#d07e93");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#d07e93");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_stack_4","QCD+tt (DD)","F");

   ci = TColor::GetColor("#c1caf3");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#c1caf3");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "p2",0,0.05,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-0.125,0.05714285,1.125,1.771429);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetGridy();
   p2->SetBottomMargin(0.2);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   Double_t xAxis36[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *raterr__21 = new TH1F("raterr__21","",9, xAxis36);
   raterr__21->SetBinContent(0,1);
   raterr__21->SetBinContent(1,1);
   raterr__21->SetBinContent(2,1);
   raterr__21->SetBinContent(3,1);
   raterr__21->SetBinContent(4,1);
   raterr__21->SetBinContent(5,1);
   raterr__21->SetBinContent(6,1);
   raterr__21->SetBinContent(7,1);
   raterr__21->SetBinContent(8,1);
   raterr__21->SetBinContent(9,1);
   raterr__21->SetBinContent(10,1);
   raterr__21->SetBinError(1,0.2337443);
   raterr__21->SetBinError(2,0.2237101);
   raterr__21->SetBinError(3,0.219735);
   raterr__21->SetBinError(4,0.2115713);
   raterr__21->SetBinError(5,0.2140713);
   raterr__21->SetBinError(6,0.2143615);
   raterr__21->SetBinError(7,0.213016);
   raterr__21->SetBinError(8,0.2145238);
   raterr__21->SetBinError(9,0.2148034);
   raterr__21->SetMinimum(0.4);
   raterr__21->SetMaximum(1.6);
   raterr__21->SetEntries(675);
   raterr__21->SetStats(0);

   ci = TColor::GetColor("#666666");
   raterr__21->SetFillColor(ci);
   raterr__21->SetFillStyle(3002);

   ci = TColor::GetColor("#666666");
   raterr__21->SetLineColor(ci);
   raterr__21->SetLineWidth(0);
   raterr__21->GetXaxis()->SetTitle("BDT discriminant");
   raterr__21->GetXaxis()->SetLabelFont(43);
   raterr__21->GetXaxis()->SetLabelSize(15);
   raterr__21->GetXaxis()->SetTitleSize(20);
   raterr__21->GetXaxis()->SetTitleOffset(4);
   raterr__21->GetXaxis()->SetTitleFont(43);
   raterr__21->GetYaxis()->SetTitle("ratio data/prediction");
   raterr__21->GetYaxis()->SetNdivisions(505);
   raterr__21->GetYaxis()->SetLabelFont(43);
   raterr__21->GetYaxis()->SetLabelSize(15);
   raterr__21->GetYaxis()->SetTitleSize(20);
   raterr__21->GetYaxis()->SetTitleOffset(1.55);
   raterr__21->GetYaxis()->SetTitleFont(43);
   raterr__21->GetZaxis()->SetLabelFont(42);
   raterr__21->GetZaxis()->SetLabelSize(0.035);
   raterr__21->GetZaxis()->SetTitleSize(0.035);
   raterr__21->GetZaxis()->SetTitleFont(42);
   raterr__21->Draw("e2");
   Double_t xAxis37[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *ratio__22 = new TH1F("ratio__22","hist",9, xAxis37);
   ratio__22->SetBinContent(1,0.5437832);
   ratio__22->SetBinContent(2,0.8535596);
   ratio__22->SetBinContent(3,0.9355116);
   ratio__22->SetBinContent(4,0.7078739);
   ratio__22->SetBinContent(5,1.009039);
   ratio__22->SetBinContent(6,0.8217006);
   ratio__22->SetBinContent(7,0.7542496);
   ratio__22->SetBinContent(8,0.8856971);
   ratio__22->SetBinContent(9,0.6695657);
   ratio__22->SetBinError(1,0.3535534);
   ratio__22->SetBinError(2,0.2);
   ratio__22->SetBinError(3,0.1428571);
   ratio__22->SetBinError(4,0.1336306);
   ratio__22->SetBinError(5,0.09805807);
   ratio__22->SetBinError(6,0.0949158);
   ratio__22->SetBinError(7,0.086711);
   ratio__22->SetBinError(8,0.08873565);
   ratio__22->SetBinError(9,0.140028);
   ratio__22->SetEntries(127.1297);
   ratio__22->SetStats(0);
   ratio__22->SetLineWidth(2);
   ratio__22->SetMarkerStyle(20);
   ratio__22->GetXaxis()->SetLabelFont(42);
   ratio__22->GetXaxis()->SetLabelSize(0.035);
   ratio__22->GetXaxis()->SetTitleSize(0.035);
   ratio__22->GetXaxis()->SetTitleFont(42);
   ratio__22->GetYaxis()->SetLabelFont(42);
   ratio__22->GetYaxis()->SetLabelSize(0.035);
   ratio__22->GetYaxis()->SetTitleSize(0.035);
   ratio__22->GetYaxis()->SetTitleOffset(0);
   ratio__22->GetYaxis()->SetTitleFont(42);
   ratio__22->GetZaxis()->SetLabelFont(42);
   ratio__22->GetZaxis()->SetLabelSize(0.035);
   ratio__22->GetZaxis()->SetTitleSize(0.035);
   ratio__22->GetZaxis()->SetTitleFont(42);
   ratio__22->Draw("pe0same");
   Double_t xAxis38[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *ratup__23 = new TH1F("ratup__23","hist",9, xAxis38);
   ratup__23->SetBinContent(0,1);
   ratup__23->SetBinContent(1,0.9681847);
   ratup__23->SetBinContent(2,0.9359809);
   ratup__23->SetBinContent(3,0.9483601);
   ratup__23->SetBinContent(4,0.9374977);
   ratup__23->SetBinContent(5,0.9190614);
   ratup__23->SetBinContent(6,0.9027142);
   ratup__23->SetBinContent(7,0.9723202);
   ratup__23->SetBinContent(8,0.9798427);
   ratup__23->SetBinContent(9,1.131324);
   ratup__23->SetBinContent(10,1);
   ratup__23->SetBinError(1,0.05167745);
   ratup__23->SetBinError(2,0.05843082);
   ratup__23->SetBinError(3,0.04654446);
   ratup__23->SetBinError(4,0.03001438);
   ratup__23->SetBinError(5,0.03706206);
   ratup__23->SetBinError(6,0.0277728);
   ratup__23->SetBinError(7,0.01991304);
   ratup__23->SetBinError(8,0.02540967);
   ratup__23->SetBinError(9,0.0183681);
   ratup__23->SetEntries(4431.446);
   ratup__23->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   ratup__23->SetLineColor(ci);
   ratup__23->SetLineWidth(2);
   ratup__23->SetMarkerStyle(20);
   ratup__23->GetXaxis()->SetLabelFont(42);
   ratup__23->GetXaxis()->SetLabelSize(0.035);
   ratup__23->GetXaxis()->SetTitleSize(0.035);
   ratup__23->GetXaxis()->SetTitleFont(42);
   ratup__23->GetYaxis()->SetLabelFont(42);
   ratup__23->GetYaxis()->SetLabelSize(0.035);
   ratup__23->GetYaxis()->SetTitleSize(0.035);
   ratup__23->GetYaxis()->SetTitleOffset(0);
   ratup__23->GetYaxis()->SetTitleFont(42);
   ratup__23->GetZaxis()->SetLabelFont(42);
   ratup__23->GetZaxis()->SetLabelSize(0.035);
   ratup__23->GetZaxis()->SetTitleSize(0.035);
   ratup__23->GetZaxis()->SetTitleFont(42);
   ratup__23->Draw("histsame");
   Double_t xAxis39[10] = {0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1}; 
   
   TH1F *ratdown__24 = new TH1F("ratdown__24","hist",9, xAxis39);
   ratdown__24->SetBinContent(0,1);
   ratdown__24->SetBinContent(1,1.031174);
   ratdown__24->SetBinContent(2,1.046927);
   ratdown__24->SetBinContent(3,1.004736);
   ratdown__24->SetBinContent(4,1.005069);
   ratdown__24->SetBinContent(5,1.004499);
   ratdown__24->SetBinContent(6,1.047185);
   ratdown__24->SetBinContent(7,0.9719989);
   ratdown__24->SetBinContent(8,0.961997);
   ratdown__24->SetBinContent(9,0.6780288);
   ratdown__24->SetBinContent(10,1);
   ratdown__24->SetBinError(1,0.04335196);
   ratdown__24->SetBinError(2,0.04807266);
   ratdown__24->SetBinError(3,0.0425371);
   ratdown__24->SetBinError(4,0.02610567);
   ratdown__24->SetBinError(5,0.03273118);
   ratdown__24->SetBinError(6,0.02144909);
   ratdown__24->SetBinError(7,0.019889);
   ratdown__24->SetBinError(8,0.02611655);
   ratdown__24->SetBinError(9,0.04584262);
   ratdown__24->SetEntries(4856.162);
   ratdown__24->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   ratdown__24->SetLineColor(ci);
   ratdown__24->SetLineWidth(2);
   ratdown__24->SetMarkerStyle(20);
   ratdown__24->GetXaxis()->SetLabelFont(42);
   ratdown__24->GetXaxis()->SetLabelSize(0.035);
   ratdown__24->GetXaxis()->SetTitleSize(0.035);
   ratdown__24->GetXaxis()->SetTitleFont(42);
   ratdown__24->GetYaxis()->SetLabelFont(42);
   ratdown__24->GetYaxis()->SetLabelSize(0.035);
   ratdown__24->GetYaxis()->SetTitleSize(0.035);
   ratdown__24->GetYaxis()->SetTitleOffset(0);
   ratdown__24->GetYaxis()->SetTitleFont(42);
   ratdown__24->GetZaxis()->SetLabelFont(42);
   ratdown__24->GetZaxis()->SetLabelSize(0.035);
   ratdown__24->GetZaxis()->SetTitleSize(0.035);
   ratdown__24->GetZaxis()->SetTitleFont(42);
   ratdown__24->Draw("histsame");
   TLine *line = new TLine(0,1,1,1);
   line->Draw();
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
