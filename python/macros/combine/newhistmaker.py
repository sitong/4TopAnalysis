#a python script for proyperly generating datacards according to cut category
# https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part2/settinguptheanalysis/#binned-shape-analysis
# https://github.com/scikit-hep/uproot#writing-histograms
#modify cuts and baselines in datacard_dictionary.py
import os
import sys
import math
import numpy as np
import argparse
from ROOT import gROOT,TFile,TTree,TH1D
import ROOT as r
from datacardrefs import datacardrefs
gROOT.SetBatch(True)
# import uproot
# import types
# import uproot_methods.classes.TH1
import numpy as np
from array import array
from NAFdict import getNAFerrors
 
class histmaker:
# class that creates shape histograms for combine
    def __init__(self, year, outfilename):
        self.df = datacardrefs(year)
        self.systs = self.df.systs #dictionary of cut+bin ids and selections
        self.outroot = outfilename #name of shape rootfile
        self.year       = year
        self.yields= {}
        self.NAFfile = 'naf_March_09/naf_SR_'+year+'_Tree2.root' 
        print self.NAFfile
        self.VRerrname = 'data_pred_shape_disagreement'
        if self.df.randseed:
            self.VRerrname = 'NNseed_shape'
        self.VRerrfile = './VRerrtempl_shapeN.root'
        # self.VRerrfile = './VRerrtempl_slopeshape.root'
        self.blind = False
        if '.root' not in self.df.datafile:
            print 'blind run. will add empty data_obs histograms'
            self.blind = True
        else:
            print 'NOT BLINDED! Running with data', self.df.datafile
        self.halfbdt=self.df.halfbdt #bdt blinded from 0.5 and up
        if self.halfbdt:
            print 'NOTE: running half blinded datacards'
        self.qcdmc= self.df.qcdmc
            
    def makehalfbdt(self, hist):
        #sets bdt values and errors for bdt > 0.5 to zero
        for ibin in range(0, hist.GetNbinsX()+2):
            # print hist.GetBinLowEdge(ibin)
            if hist.GetBinLowEdge(ibin)>=0.5: 
                hist.SetBinError(ibin, 0.0)
                hist.SetBinContent(ibin, 0.0)
        return hist

    def datahist(self, channel):
        datafile = self.df.datafile
        histname = channel+'_'+self.year+'_data_obs'
        if not self.df.usevarbin:
            datahist = r.TH1D(histname, histname, self.df.nbins, self.df.selrange[0], self.df.selrange[1])
        else:
            datahist = r.TH1D(histname, histname, self.df.nbins, self.df.edges)


        #actually fill the histogram if it isn't a blind analysis
        if not self.blind:
            shapevar = 'catBDTdisc_nosys'
            # print self.df.binsels[channel]
            binsel = '('+self.df.binsels[channel]+' && '+self.df.datasel
            dfile = r.TFile(datafile)
            dtree = dfile.Get(self.df.tn)
            gROOT.cd()
            dtree.Draw(shapevar+'>>'+histname, str(binsel))#apply bin selection
            print shapevar+'>>'+histname, binsel
            print binsel, '\n'

            if self.halfbdt:
                datahist = self.makehalfbdt(datahist)

            # print 'netries',datahist.GetEntries()
            yld, unc = self.df.getyield(datahist)
            print 'DATA',yld,unc
            self.yields[histname] = [yld, unc]
  
            print 'filled data hist ', histname, 'with selection', binsel, 'for process data_obs and bin', channel
        else:
            print 'created empty data_obs hist', histname

        #open rootfile for writing histograms
        histfile = r.TFile(self.outroot, "UPDATE")
        datahist.Write()
        histfile.Close()
        del histfile

    #makes a histogram for each process and bin. channel is the binname
    def getmchists(self, histname, processname, selection, shapevar='catBDTdisc_nosys', qcdttrenorm=None):

        # get, modify and return histogram:
        pfile = r.TFile(self.df.procfiles[processname])
        ptree = pfile.Get(self.df.tn)
        if not self.df.usevarbin:
            dischist = r.TH1F(histname, histname, self.df.nbins, self.df.selrange[0], self.df.selrange[1])
        else:
            dischist = r.TH1F(histname, histname, self.df.nbins, self.df.edges)

        ptree.Draw(shapevar+'>>'+histname, selection)#apply bin selection
        print 'filled hist ', histname, 'with selection', selection, 'for process', processname

        if self.halfbdt:
            dischist = self.makehalfbdt(dischist)

        dischist.Sumw2()
        
        yld, unc = self.df.getyield(dischist)

        if qcdttrenorm is not None:
            dischist.Scale(self.df.DDyields[qcdttrenorm][0]/yld)
            yld = self.df.DDyields[qcdttrenorm][0]
            unc = self.df.DDyields[qcdttrenorm][1]

        self.yields[histname] = [yld, unc]

        histfile = r.TFile(self.outroot, "UPDATE")
        dischist.Write()
        histfile.Close()
        del histfile


    def getnewpredhists(self, histname, chanid, scalar=1.0, typ='nom'): #NEW
        #typ = nom, med, lo, hi
        #NEW
        path = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/data/NNretrain/'
        bdtvar = 'BDT_disc'
        if typ=='nom':
            if self.df.ttbb:
                nafdir = self.year+'_ttpure_minorbkgttbb/' #USEttbb
            elif not self.df.ttbb:
                nafdir = self.year+'_ttinclusive_minorbkg-20220305/'
            binalias = {'RT1BT0htbin0':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<800'],
                        'RT1BT0htbin1':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=800 && ht<900'],
                        'RT1BT0htbin2':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=900 && ht<1000'],
                        'RT1BT0htbin3':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1000 && ht<1100'],
                        'RT1BT0htbin4':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1100 && ht<1200'],
                        'RT1BT0htbin5':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1200 && ht<1300'],
                        'RT1BT0htbin6':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1300 && ht<1500'],
                        'RT1BT0htbin7':['240k_SR1.root', 'nJet>=9 && nbJet>=3 && ht>=1500'],
                        'RT1BT1htbin0':['240k_SR2.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1400'],
                        'RT1BT1htbin1':['240k_SR2.root', 'nJet>=9 && nbJet>=3 && ht>=1400'],
                        'RT2BTALLhtbin0':['240k_SR3.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1100'],
                        'RT2BTALLhtbin1':['240k_SR3.root', 'nJet>=9 && nbJet>=3 && ht>=1100']}# for reading trees
        elif typ=='med' or typ=='lo' or typ=='hi' or typ=='min' or typ=='max':
            if self.df.ttbb:
            #     print 'WARNING INCOMPATIBLE TEST - USETTBB=TRUE OR VR IN RANDSEED TEST'
            # else:
                nafdir = self.year+'SRs_minlomedhimax/'
                binalias = {'RT1BT0htbin0':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<800'],
                            'RT1BT0htbin1':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=800 && ht<900'],
                            'RT1BT0htbin2':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=900 && ht<1000'],
                            'RT1BT0htbin3':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1000 && ht<1100'],
                            'RT1BT0htbin4':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1100 && ht<1200'],
                            'RT1BT0htbin5':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1200 && ht<1300'],
                            'RT1BT0htbin6':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1300 && ht<1500'],
                            'RT1BT0htbin7':[self.year+'_240k_SR1_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1500'],
                            'RT1BT1htbin0':[self.year+'_240k_SR2_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1400'],
                            'RT1BT1htbin1':[self.year+'_240k_SR2_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1400'],
                            'RT2BTALLhtbin0':[self.year+'_240k_SR3_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=700 && ht<1100'],
                            'RT2BTALLhtbin1':[self.year+'_240k_SR3_'+typ+'.root', 'nJet>=9 && nbJet>=3 && ht>=1100']}# for reading trees  

        path = path+nafdir
        nafname = path+binalias[chanid][0]
        rf = r.TFile(nafname)
        t = rf.Get('mytree')
        print t.GetEntries()
        if not self.df.usevarbin:
            hist = r.TH1F(histname, histname, 10, 0.0, 1.0)
        else:
            hist = r.TH1F(histname, histname, self.df.nbins, self.df.edges)

        t.Draw(bdtvar+">>"+histname, binalias[chanid][1])
        print hist.GetEntries()
        hist.Sumw2()

        # self.df.fixref(rf, hist)
        yld, unc = self.df.getyield(hist)
        # if yld==0.0:
        #     yld=1.0
        print 'yld',yld
        print 'norm', self.df.DDyields[chanid][0]*scalar/yld
        hist.Scale((self.df.DDyields[chanid][0])/yld)
        self.yields[histname] = [self.df.DDyields[chanid][0], self.df.DDyields[chanid][1]]

        if self.halfbdt:
            hist = self.makehalfbdt(hist)
            #reset yields to recalculated ones
            yld, unc = self.df.getyield(hist)
            self.yields[histname] = [yld, unc]

        histfile = r.TFile(self.outroot, "UPDATE")
        hist.Write()
        histfile.Close()
        del histfile
        print 'filled hist ', histname, 'for channel', chanid, 'from NAF'
                    
    def getVRerrhists(self, chanid, uncval, upname, downname, newtyp=True):
        path = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/data/NNretrain/'
        nafdir = self.year+'_ttinclusive_minorbkg-20220305/'
        bdtvar = 'BDT_disc'
        tn = 'mytree'
        binalias = {'RT1BT0htbin0':['240k_SR1.root',700.0,800.0],
                    'RT1BT0htbin1':['240k_SR1.root',800.0,900.0],
                    'RT1BT0htbin2':['240k_SR1.root',900.0,1000.0],
                    'RT1BT0htbin3':['240k_SR1.root',1000.0,1100.0],
                    'RT1BT0htbin4':['240k_SR1.root',1100.0,1200.0],
                    'RT1BT0htbin5':['240k_SR1.root',1200.0,1300.0],
                    'RT1BT0htbin6':['240k_SR1.root',1300.0,1500.0],
                    'RT1BT0htbin7':['240k_SR1.root',1500.0,1000000.0],
                    'RT1BT1htbin0':['240k_SR2.root',700.0,1400.0],
                    'RT1BT1htbin1':['240k_SR2.root',1400.0,1000000.0],
                    'RT2BTALLhtbin0':['240k_SR3.root',700.0,1100.0],
                    'RT2BTALLhtbin1':['240k_SR3.root',1100.0,1000000.0]}# for reading trees
        treefile = path+nafdir+binalias[chanid][0]

        if not newtyp:
            tn='Events'
            bdtvar = 'BDT_dsic'
            treefile = self.NAFfile
            binalias = {'RT1BT0htbin0':'0',
                        'RT1BT0htbin1':'1',
                        'RT1BT0htbin2':'2',
                        'RT1BT0htbin3':'3',
                        'RT1BT0htbin4':'4',
                        'RT1BT0htbin5':'5',
                        'RT1BT0htbin6':'6',
                        'RT1BT0htbin7':'7',
                        'RT1BT1htbin0':'10',
                        'RT1BT1htbin1':'11',
                        'RT2BTALLhtbin0':'20',
                        'RT2BTALLhtbin1':'21'}# for reading trees
            mybin = binalias[chanid]
            
        rf, tree = self.df.makeroot(treefile, tn)

        systup = 1.0+uncval  #for set %                                                            
        systdown = 1.0-uncval

        histup = r.TH1F(upname, upname, len(self.df.edges)-1, self.df.edges)
        histdown = r.TH1F(downname, downname, len(self.df.edges)-1, self.df.edges)
        

        for i,ent in enumerate(tree):
            #selection:
            if newtyp: 
                if ( ent.nJet>=9  and ent.nbJet>=3 and ent.ht>=binalias[chanid][1] and ent.ht<binalias[chanid][2] and ent.BDT_disc>=0.0 and ent.BDT_disc<=1.0 ):
                    #OLD VERSION#############
                    print ent.BDT_disc
                    # if ent.BDT_disc>=0.6: #put in 0.8 cutoff
                    discup = systup*ent.BDT_disc  
                    discdown = systdown*ent.BDT_disc
                    # else:
                    #     discup = ent.BDT_disc
                    #     discdown = ent.BDT_disc
                    #############################
                    # # #statcomm version##########
                    # discup   = ent.BDT_disc
                    # discdown = ent.BDT_disc 
                    ###########################
                    if discup>=1.0:
                        discup=1.0
                    elif discup<0.0:
                        discup=0.0
                    if discdown>=1.0:
                        discdown=1.0
                    elif discdown<0.0:
                        discdown=0.0
                    histup.Fill(discup)
                    histdown.Fill(discdown)
                    histup.SetBinContent(10, 0.0) #overflow 
                    histdown.SetBinContent(10, 0.0) #overflow

            elif not newtyp:
                if (str(ent.bin)==mybin and ent.BDT_dsic>=0.0 and ent.BDT_dsic<=1.0) :
                    discup = systup*ent.BDT_dsic  
                    discdown = systdown*ent.BDT_dsic
                    if discup>=1.0:
                        discup=1.0
                    elif discup<0.0:
                        discup=0.0
                    if discdown>=1.0:
                        discdown=1.0
                    elif discdown<0.0:
                        discdown=0.0
                    histup.Fill(discup)
                    histdown.Fill(discdown)
                    histup.SetBinContent(10, 0.0) #overflow 
                    histdown.SetBinContent(10, 0.0) #overflow

        # hists={'up':histup, 'down':histdown}
        # for typ, hist in hists.iteritems():
        
        uyld, uunc = self.df.getyield(histup)
        dyld, dunc = self.df.getyield(histdown)
        print uyld, dyld
        histup.Sumw2(); histdown.Sumw2()
        ABCDyld = self.df.DDyields[chanid][0]
        ABCDunc = self.df.DDyields[chanid][1]
        ABCD_relunc = ABCDunc/ABCDyld
        histup.Scale(ABCDyld/uyld) #scale histogram
        histdown.Scale(ABCDyld/dyld) #scale histogram

        #statcomm version##########
        # if self.df.perbin:
        #     uncarr = self.df.VRshapearr[self.year][chanid]
        #     for b in range(9):
        #         histup.SetBinContent(b+1, histup.GetBinContent(1+b)*(1.0+uncarr[b]))
        #         histdown.SetBinContent(b+1, histdown.GetBinContent(1+b)*(1.00-uncarr[b]))
        #         print b, 1.0+uncarr[b], 1.00-uncarr[b]                
        # else:
        #     histup.SetBinContent(9, histup.GetBinContent(9)*systup)
        #     histdown.SetBinContent(9, histdown.GetBinContent(9)*systdown)
        #     histup.SetBinContent(8, histup.GetBinContent(8)*systup)
        #     histdown.SetBinContent(8, histdown.GetBinContent(8)*systdown)
        #############################

        if self.df.modspursig:
            bin9 = histup.GetBinContent(9)
            N = (bin9*(1.0+uncval)) - bin9
            scale = (N+bin9)/bin9
            histup.Scale(scale)
            histdown.Scale((1.0/scale))
        
        histup.SetDirectory(0); histdown.SetDirectory(0)
        tree.ResetBranchAddresses()
        rf.Close()
        print histup, histdown

        # histup.SetDirectory(0)
        # histdown.SetDirectory(0)
        # return histup, histdown
        histfile = r.TFile(self.outroot, "UPDATE")
        histup.Write()
        histdown.Write()
        histfile.Close()
        del histfile
        print 'Wrote histograms', upname, downname

  
    def makehists(self):        
        topcats = {'RT1BT0htbin0':'RT1BT0',  #for semi-correlation
                   'RT1BT0htbin1':'RT1BT0',
                   'RT1BT0htbin2':'RT1BT0',
                   'RT1BT0htbin3':'RT1BT0',
                   'RT1BT0htbin4':'RT1BT0',
                   'RT1BT0htbin5':'RT1BT0',
                   'RT1BT0htbin6':'RT1BT0',
                   'RT1BT0htbin7':'RT1BT0',
                   'RT1BT1htbin0':'RT1BT1',
                   'RT1BT1htbin1':'RT1BT1',
                   'RT2BTALLhtbin0':'RT2BTALL',
                   'RT2BTALLhtbin1':'RT2BTALL'}

        #first get the "normal" histograms
        for chanid, chansel in self.df.binsels.iteritems():
            #data hist
            self.datahist(chanid)
            #mc hists
            for processname in self.df.mcprocs:
                histname = chanid+'_'+self.year+'_'+processname
                binsel = '('+chansel+' && '+self.df.mcsel
                if processname=='ttbb':
                    ttbbsel = self.df.mcsel_ttbb
                    # ttbbsel = self.df.mcsel_ttbb.replace('*weight', '*weight*1.37') #SF test
                    binsel = '('+chansel+' && '+ttbbsel#self.df.mcsel_ttbb
                if ('TTTT' in processname) and self.year!='2018' and not self.df.mergehtbins:
                    #add prefire weight:
                    binsel=binsel+'*'+self.df.prefire[chanid+'_'+self.year]['TTTT'][0]
                elif processname in ['TTH', 'TTW', 'TTZ', 'TTOther', 'TTX'] and self.year!='2018' and not self.df.mergehtbins:
                    #add prefire weight:
                    binsel=binsel+'*'+self.df.prefire[chanid+'_'+self.year]['TTX'][0]
                self.getmchists(histname, processname, binsel)

            #naf hists
            if not self.qcdmc:
                nafhistname = chanid+'_'+self.year+'_'+self.df.DDprocname
                if self.df.randseed:
                    self.getnewpredhists(nafhistname, chanid, typ='nom')
                else:
                    self.getnewpredhists(nafhistname, chanid)

                if self.df.addshapeunc or self.df.randseed:
                #add rms uncertainties
                    #uncorr
                    if not self.df.corrnafunc:
                        nafhistname_up = nafhistname+'_'+self.VRerrname+'_'+topcats[chanid]+'_'+self.year+'Up' #partcorr
                        nafhistname_down = nafhistname+'_'+self.VRerrname+'_'+topcats[chanid]+'_'+self.year+'Down' #partcorr
                        nafhistname_shapeup = nafhistname+'_'+self.VRerrname+'_'+topcats[chanid]+'_'+self.year+'Up' #partcorr
                        nafhistname_shapedown = nafhistname+'_'+self.VRerrname+'_'+topcats[chanid]+'_'+self.year+'Down' #partcorr

                        # nafhistname_up = nafhistname+'_'+self.VRerrname+'_'+chanid+'_'+self.year+'Up' #uncorr
                        # nafhistname_down = nafhistname+'_'+self.VRerrname+'_'+chanid+'_'+self.year+'Down' #uncorr
                        # nafhistname_shapeup = nafhistname+'_'+self.VRerrname+'_'+chanid+'_'+self.year+'Up' #uncorr
                        # nafhistname_shapedown = nafhistname+'_'+self.VRerrname+'_'+chanid+'_'+self.year+'Down' #uncorr

                    #corr
                    elif self.df.corrnafunc:
                        nafhistname_up = nafhistname+'_'+self.VRerrname+'_'+self.year+'Up' #corr
                        nafhistname_down = nafhistname+'_'+self.VRerrname+'_'+self.year+'Down' #corr
                    if self.df.addshapeunc:
                        self.getVRerrhists(chanid, self.df.VRshapevals[self.year][chanid], nafhistname_shapeup, nafhistname_shapedown)
                        # self.getVRerrhists(chanid, self.df.VRshapevals[self.year][chanid], nafhistname_up, nafhistname_down)
                        # self.getVRerrhists(chanid, 0.01, nafhistname_up, nafhistname_down)
                    if self.df.randseed:
                        self.getnewpredhists(nafhistname_up, chanid, typ='max')
                        self.getnewpredhists(nafhistname_down, chanid, typ='min')

            elif self.qcdmc:
                qcdttname = self.df.DDprocname
                histname = chanid+'_'+self.year+'_'+qcdttname
                binsel = '('+chansel+' && '+self.df.mcsel
                self.getmchists(histname, qcdttname, binsel, qcdttrenorm=chanid)
                # self.getmchists(histname, qcdttname, binsel) #no renorm

            # naf normalization uncertainties:
            # if self.df.usenafunc:
            #     unc = np.sqrt((self.df.DDyields[1])**2+(self.df.nafunc*self.df.DDyields[0])**2) #extra norm uncertainty
            # else:
            #     unc = self.df.DDyields[1]
            # upunc=1.0+unc; downunc=1.0-unc
            # nafhistname_up = nafhistname+'_'+nafhistname+'_ABCDnormUp'
            # nafhistname_down = nafhistname+'_'+nafhistname+'_ABCDnormDown'

        #then get the systematics histograms:
        for processname, systlist in self.df.systs.iteritems():
            for syst in systlist:
                if syst in['jer','jes']:
                    discvarup = 'catBDTdisc_'+syst+'Up'
                    discvardown = 'catBDTdisc_'+syst+'Down'
                else: 
                    discvarup = 'catBDTdisc_nosys'
                    discvardown = 'catBDTdisc_nosys'

                    
                sysmcsel, sysbinsels = self.df.getsels(syst)
                if processname =='ttbb':
                    sysmcsel, sysbinsels = self.df.getsels(syst, isttbb=True)
                    # sysmcsel[0] = sysmcsel[0].replace('*weight', '*weight*1.37')#SF test
                    # sysmcsel[1] = sysmcsel[1].replace('*weight', '*weight*1.37')#SF test
                for chanid in self.df.binsels.keys():
                    syshistname = chanid+'_'+self.year+'_'+processname
                    # syshistname = self.year+'_'+processname
                    binselup = '('+sysbinsels[0][chanid]+' && '+sysmcsel[0]
                    binseldown = '('+sysbinsels[1][chanid]+' && '+sysmcsel[1]
                    # if processname != 'other' and processname!='DDBKG' and self.year!='2018' and not self.df.mergehtbins:
                    #     #add prefire weight:
                    #     binselup=binselup+'*'+self.df.prefire[chanid+'_'+self.year][processname][0]
                    #     binseldown=binseldown+'*'+self.df.prefire[chanid+'_'+self.year][processname][0]
                    if ('TTTT' in processname) and processname!='DDBKG' and self.year!='2018' and not self.df.mergehtbins:
                        #add prefire weight:
                        binselup=binselup+'*'+self.df.prefire[chanid+'_'+self.year]['TTTT'][0]
                        binseldown=binseldown+'*'+self.df.prefire[chanid+'_'+self.year]['TTTT'][0]
                    elif processname in ['TTH', 'TTW', 'TTZ', 'TTOther', 'TTX'] and processname!='DDBKG' and self.year!='2018' and not self.df.mergehtbins:
                        #add prefire weight:
                        binselup=binselup+'*'+self.df.prefire[chanid+'_'+self.year]['TTX'][0]
                        binseldown=binseldown+'*'+self.df.prefire[chanid+'_'+self.year]['TTX'][0]
                    if syst in ['pdf', 'btagLF','btagHF','btagCFerr1', 'btagCFerr2']: #year correlated systs, process correlated systs
                        syshistname_up = syshistname+'_'+syst+'Up'
                        syshistname_down = syshistname+'_'+syst+'Down'
                    elif syst in ['fsr', 'isr', 'ME'] and processname!='TTTT_spurious': #year correlated systs, not process correlated systs
                        syshistname_up = syshistname+'_'+syst+'_'+processname+'Up'
                        syshistname_down = syshistname+'_'+syst+'_'+processname+'Down'
                    elif syst in ['fsr', 'isr', 'ME'] and processname=='TTTT_spurious': #year correlated systs, not process correlated systs
                        syshistname_up = syshistname+'_'+syst+'_'+'TTTTUp'
                        syshistname_down = syshistname+'_'+syst+'_'+'TTTTDown'
                    else: # not year correlated systs, process correlated systs
                        syshistname_up = syshistname+'_'+syst+'_'+self.year+'Up'
                        syshistname_down = syshistname+'_'+syst+'_'+self.year+'Down'
                    if processname !='QCD-TT':
                        self.getmchists(syshistname_up, processname, binselup, discvarup)
                        self.getmchists(syshistname_down, processname, binseldown, discvardown)
                    #qcdttrenorm
                    elif processname =='QCD-TT':
                        self.getmchists(syshistname_up, processname, binselup, discvarup, qcdttrenorm=chanid)
                        self.getmchists(syshistname_down, processname, binseldown, discvardown, qcdttrenorm=chanid)
                        # self.getmchists(syshistname_up, processname, binselup, discvarup)  #no renorm
                        # self.getmchists(syshistname_down, processname, binseldown, discvardown)

        return self.yields


    def getpredhists(self, histname, chanid, scalar=1.0): #OLD
        binalias = {'RT1BT0htbin0':'0',
                    'RT1BT0htbin1':'1',
                    'RT1BT0htbin2':'2',
                    'RT1BT0htbin3':'3',
                    'RT1BT0htbin4':'4',
                    'RT1BT0htbin5':'5',
                    'RT1BT0htbin6':'6',
                    'RT1BT0htbin7':'7',
                    'RT1BT1htbin0':'10',
                    'RT1BT1htbin1':'11',
                    'RT2BTALLhtbin0':'20',
                    'RT2BTALLhtbin1':'21'}# for reading trees

        if self.df.mergehtbins:
            binalias = {'RT1BT0':'(bin==0 || bin==1 || bin==2 || bin==3 || bin==4 || bin==5 || bin==6 || bin==7)',
                        'RT1BT1':'(bin==10 || bin==11)',
                        'RT2BTALL':'(bin==20 || bin==21)'}
        var = 'BDT_dsic'
        treefile = self.NAFfile
        mybin = binalias[chanid]
        print mybin
        cut = '(bin=='+mybin+' && '+var+'>=0.0 && '+var+'<=1.0)'
        if self.df.mergehtbins:
            cut = '('+mybin+' && '+var+'>=0.0 && '+var+'<=1.0)'

        rf = r.TFile(treefile)
        t = rf.Get('Events')
        print t.GetEntries()
        if not self.df.usevarbin:
            hist = r.TH1F(histname, histname, 10, 0.0, 1.0)
        else:
            hist = r.TH1F(histname, histname, self.df.nbins, self.df.edges)

        t.Draw(var+">>"+histname, cut)
        print hist.GetEntries()

        hist.Sumw2()

        # self.df.fixref(rf, hist)
        yld, unc = self.df.getyield(hist)
        # if yld==0.0:
        #     yld=1.0
        print 'yld',yld
        print 'norm', self.df.DDyields[chanid][0]*scalar/yld
        hist.Scale((self.df.DDyields[chanid][0])/yld)
        self.yields[histname] = [self.df.DDyields[chanid][0], self.df.DDyields[chanid][1]]

        if self.halfbdt:
            hist = self.makehalfbdt(hist)
            #reset yields to recalculated ones
            yld, unc = self.df.getyield(hist)
            self.yields[histname] = [yld, unc]

        histfile = r.TFile(self.outroot, "UPDATE")
        hist.Write()
        histfile.Close()
        del histfile
        print 'filled hist ', histname, 'for channel', chanid, 'from NAF'
  


    #OLD
    # def getVRerrhists(self, chanid, upname, downname): #need to update
    #     binalias = {'RT1BT0htbin0':'cut0bin0',
    #                 'RT1BT0htbin1':'cut0bin1',
    #                 'RT1BT0htbin2':'cut0bin2',
    #                 'RT1BT0htbin3':'cut0bin3',
    #                 'RT1BT0htbin4':'cut0bin4',
    #                 'RT1BT0htbin5':'cut0bin5',
    #                 'RT1BT0htbin6':'cut0bin6',
    #                 'RT1BT0htbin7':'cut0bin7',
    #                 'RT1BT1htbin0':'cut1bin0',
    #                 'RT1BT1htbin1':'cut1bin1',
    #                 'RT2BTALLhtbin0':'cut2bin0',
    #                 'RT2BTALLhtbin1':'cut2bin1'}# for reading trees
    #     if self.df.mergehtbins:
    #         binalias = {'RT1BT0':'cut0',
    #                     'RT1BT1':'cut1',
    #                     'RT2BTALL':'cut2'}
    #     upalias   = binalias[chanid]+'_'+self.year+'_up'
    #     downalias = binalias[chanid]+'_'+self.year+'_down'

    #     rf = r.TFile(self.VRerrfile)
    #     getup = rf.Get(upalias)
    #     getdown = rf.Get(downalias)
    #     print rf, getup, getdown, upalias, downalias
    #     getup.SetDirectory(0); getdown.SetDirectory(0)
    #     rf.Close()
    #     # gROOT.cd()
    #     uphist = getup.Clone(upname)
    #     downhist = getdown.Clone(downname)

    #     if self.halfbdt:
    #         uphist = self.makehalfbdt(uphist)
    #         downhist = self.makehalfbdt(downhist)

    #     histfile = r.TFile(self.outroot, "UPDATE")
    #     uphist.Write()
    #     downhist.Write()
    #     histfile.Close()
    #     del histfile
    #     print 'Wrote histograms', upname, downname
