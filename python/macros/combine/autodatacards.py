#new and improved datacard generator, with shape included!
#a python script for properly generating datacards according to cut category
#modify cuts and baselines in datacard_dictionary.py
import os
import sys
import math
import numpy as np
import argparse
import uproot
import types
from collections import OrderedDict
import uproot_methods.classes.TH1
from ROOT import gROOT,TFile,TTree,TH1D
import ROOT as r
from datacard_dictionary import datacard_dict, getprocesses, datafile, getNAFfile
from histmaker import histmaker
gROOT.SetBatch(True)

parser = argparse.ArgumentParser(description='Datacard information')
parser.add_argument("-b", "--baseline", dest="base",     default="cutSR",                                 help="baseline")
parser.add_argument("-m", "--makehists",dest="makehists",default="False",                              help="regenerate histograms or no")
parser.add_argument("-c", "--cutsets",  dest="cutsets",  default=['BOTH'],                           help="array of cutsets to consider")
parser.add_argument("-o", "--outdir",   dest="outdir",   default='./datacards/',                       help="directory to put datacards in")
parser.add_argument("-n", "--treename", dest="treename", default='Events',                             help="treename")
parser.add_argument("-p", "--projvar",  dest="projvar",  default='catBDTdisc',                         help="projection variable for getting event yields")
parser.add_argument("-y", "--year",     dest="year",     default='2016',                               help="year")
parser.add_argument("-u", "--systval",  dest="systval",  default=20,                                   help="uncertainty value as % for systematics")
parser.add_argument("-s", "--shapesystname",dest="shapesystname",default='shapesyst',                  help="uncertainty name for shape histograms")
parser.add_argument("-v", "--shapesystval", dest="shapesystval", default=2,                            help="uncertainty value for shape histograms")
parser.add_argument("-d", "--shapedir",     dest="shapedir",     default='./datacards/',               help="directory where shape histograms are located")
parser.add_argument("-f", "--shapefile",    dest="shapefile",    default='shapehists.root',            help="directory where shape histograms are located")
parser.add_argument("-x", "--useNAF",    dest="useNAF",    default=True,            help="use NAF rootfile")

args = parser.parse_args()

fullR2=False

#16:36 17:42 18:60
lumi=36
if args.year=='2016':
    lumi=35.92
elif args.year=='2017':
    lumi=41.53
elif args.year=='2018':
    lumi=59.74
elif args.year=='RunII':
    lumi=137.19
    fullR2=True

#bins to run
# binset = ['htnj', 'nj', 'ht']#, 'nj', 'htnj']
binset = ['ht']#, 'nj', 'ht']#, 'nj', 'htnj']

useshape = True
usemcbinuncs = True
nounc = False

# if nounc:
# args.shapesystval = 0
# makehists=args.makehists
regen = True
# if not args.useNAF :
    #need to use NAFfile instead of MC processes
if not fullR2:
    processes={args.year : getprocesses(args.year)}
elif fullR2:
    processes = {'2016':getprocesses('2016'), 
                 '2017':getprocesses('2017'),
                 '2018':getprocesses('2018')}
# elif args.useNAF:
#     regen=True
#     if not fullR2:
#         procs = getprocesses(args.year)
#         processes={args.year : {"TTTT": procs["TTTT"]} }  #TTTT only
#     elif fullR2:
#         procs16 = getprocesses('2016')
#         procs17 = getprocesses('2017')
#         procs18 = getprocesses('2018')
#         processes = {'2016':{"TTTT": procs16["TTTT"]}, 
#                      '2017':{"TTTT": procs17["TTTT"]},
#                      '2018':{"TTTT": procs18["TTTT"]}}
        # print processes
#organizes set of bins for datacards and sets up card names and output directory
def cardsetup(args, cutname, binname, year, startbinnum):
    print processes[year]["TTTT"]

    #setup directories
    outdir=args.outdir; shapedir=args.shapedir
    if not os.path.exists(outdir):
        os.makedirs(outdir); print 'Making output directory ', outdir
    if not os.path.exists(shapedir):
        os.makedirs(shapedir); print 'Making shape directory ', shapedir
        
    #get bins for datacard sets from datacard dictionary
    cutlist = datacard_dict(args.base, cutname, binname, year, 'weight')

    # for NAF file
    if args.useNAF:
        NAFfile = getNAFfile(year, cutname, binname)

    # get name of output shape rootfile
    shaperoot = args.shapedir+'/'+cutname+binname+'_'+year+'_'+args.shapefile
    cardname = args.outdir+'/'+cutname+binname

    #print info on the datacards you are making
    #this is also text to be put in each datacard in the set
    header='#----------------------------------------------#\n'+'#generating datacards for cut: '+cutname+', bin: '+binname+'year'+year+'\n'+'#processes: '
    for key in processes[year].keys(): header+=(key+' ') 
    header+='\n'+'#number of cards in set: '+str(len(cutlist.keys()))+'\n'+'#cutids in set: '
    for key in cutlist.keys(): header+=(str(key)+' ')
    header+='\n'+'#baseline: '+args.base+'\n'+'#shape hist location: '+shaperoot+'\n'+'#shape uncertainty value: '+str(args.shapesystval)+', shape uncertainty name: '+args.shapesystname+'\n'+'#systval: '+str(args.systval)+'%\n'+'#lumi: '+str(lumi)+'\n'+'#output location: '+outdir+'\n'+'#----------------------------------------------#\n\n'
    print header

    #generate shape histograms:
    #reinitialize/create shaperoot if it exists
    NAFRates = {}; TTTTRates={}
    hm = histmaker(processes[year], cutlist, year, shaperoot, args.shapesystval, args.shapesystname, args.treename, datafile)
    # if os.path.exists(shaperoot) and not regen:   # if shaperoot exists and you dont want to regenerate the histograms
    #     print 'shaperoot exists:', shaperoot
    if not useshape and not args.useNAF:
        print 'cut only! no histograms.'
    elif useshape or args.useNAF: #if shaperoot doesnt exist and you want to regenerate the histograms
        histfile = r.TFile(shaperoot, "RECREATE")
        histfile.Close()
        del histfile
        if not args.useNAF:
            hm.makehists()
        elif args.useNAF:
            # NAFRates = hm.hybridhists(NAFfile, procname='BKG')
            NAFRates, TTTTRates = hm.hybridhists(NAFfile, procname='BKG')

    shapeline = 'shapes * * '+cutname+binname+'_'+year+'_'+args.shapefile+' $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC'
    print shapeline

    #make cards:
    binnum = startbinnum
    for chanid, chansel in cutlist.iteritems():
        binlabel=chanid+'_'+year
        BKDRates = [0,0]
        TTTTRate = [0,0]
        if args.useNAF:
            BKDRates = NAFRates[chanid]
            TTTTRate = TTTTRates[chanid]
            print TTTTRate
        makecard(args, cardname, binlabel, processes[year], chansel, binnum, header, shapeline, year, BKDRates, TTTTRate)
        binnum+=1
    return binnum

#calculates rates and statistical uncertainties
def getRate(args, processname, cutstr, year):
    # weight = 'weight'
    # cutstr = '('+str(lumi)+'*'+weight+')*('+cutstr+')'
    pfile = r.TFile(processes[year][processname])
    tree = pfile.Get(args.treename)
    htmp = r.TH1F('htmp','htmp',1,0,10000)
    tree.Project('htmp',args.projvar,cutstr)
    errorVal = r.Double(0)
    rate = htmp.IntegralAndError(0,3,errorVal)
    # the error is computed using error propagation from the bin errors assuming bins uncorrelated
    if rate < 0:#treatment of negative yields 
        print 'WARNING: NEGATIVE YIELD:', rate, processname, cutstr #will throw error
        rate = 0.0
    rateunc = 1.0 
    if rate>0.0: #nonzero
        rateunc += (errorVal/rate)
    return round(rate,3), round(rateunc,3)

def unctable(processes, unclist, uncname, unctype, binlabel, year):
    #note for shapes it has to match up with the template
    #uncname is the identifier (shapesyst, syst, stats...)
    #unctype is lnN or shape...
    #unclist is the list of uncertainties for each process

    rownames = []
    for proc in processes:
        # name is ie c1b4_TTTT_2016_shapesyst
        rowname = binlabel+'_'+proc+'_'+uncname
        rownames.append(rowname)

    # loop and create the table
    table = ''
    for i in range(len(processes)):
        row = ['-']*len(processes)
        row[i] = str(unclist[i])
        line = rownames[i]+'      '+unctype
        for u in row:
            line+=('    '+u)
        line += '\n'
        table+=line
    # print table
    return table

#creates a single datacard
def makecard(args, cardname, binlabel, processes, cutstr, nbin, header, shapeline, year, BKDRates, TTTTRates=None):
    #binlabel is cut#_bin#_year
    #cardfile is for example datacards/RTht5_cut1bin5_datacard.txt
    # cardfile = cardname+str(nbin)+'_'+binlabel+'_datacard.txt'
    cardfile = cardname+'_'+binlabel+'_datacard'+str(nbin)+'.txt'
    nprocs = len(processes.keys()) #number of processes
    if args.useNAF:
        nprocs+=1
    imax = 1#number of final states analyzed (signals)
    jmax = nprocs-1 #number of processes(incl signal)-1
    kmax = '*' #* makes the code figure it out. number of independent systematical uncertainties 
    shapetyp = 'shape'
    unctype  = 'lnN'

    print '\n'+binlabel
    print 'writing', cardfile,'selection:',cutstr

    # get yields and statistical uncertainties for each process in this bin:
    rates=[]; stats=[]
    for proc, procfile in processes.iteritems():
        r=0.0;u=0.0
        if args.useNAF and proc=='TTTT' and TTTTRates!=None:
            r = TTTTRates[0]
            u = TTTTRates[1]
            # print 'TTTT (hist)', r,u
        elif proc!='TTTT':
            r,u = getRate(args, proc, cutstr, year)
        rates.append(r); stats.append(u)
        print proc, r, u
    if args.useNAF:
        rates.append(BKDRates[0]); stats.append(BKDRates[1])
        print 'BKG', BKDRates[0], BKDRates[1]
    #number of observed data points or just the integer sum of MC rates
    # obs  = 0 
    obs  = int(round(sum(rates)))
    print 'TOTAL B: ', round(sum(rates[1:]))

    # put together uncertanties for each process
    sysval      = round(1.0+(float(args.systval)     /100.0),3) 
    shapesysval = 1.0 #round(1.0+(float(args.shapesystval)/100.0),3)
    systs     = [str(sysval)]*nprocs#systematics for each process
    shapeuncs = [str(shapesysval)]*nprocs
    
    #get tables for uncertainties
    procnames = processes.keys()
    if args.useNAF:
        procnames.append('BKG')
        systs[-1]=1.0
        print procnames
        print 'systs', systs
    # print nprocs, len(rates), len(stats), len(systs), len(shapeuncs)
    # print procnames
    stat_table  = unctable(procnames, stats, 'stat', 'lnN', binlabel, year)
    syst_table  = unctable(procnames, systs, 'syst', 'lnN', binlabel, year)
    shape_table = unctable(procnames, shapeuncs, args.shapesystname, 'shape', binlabel, year)

    rateline = ''
    procline = ''
    numline = ''

    #enable the automatic bin-wise uncertainties http://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part2/bin-wise-stats/
    #format: [channel(as in hist)] autoMCStats [threshold] [include-signal = 0] [hist-mode = 1]
    #include-signal 0 or 1, includes signal processes in uncertainty analysis
    #threshold should be set to a value greater than or equal to zero to enable the creation of automatic bin-wise uncertainties, or -1 to use the histogram classes without them
    #positive value sets the threshold on the effective number of unweighted events above which the uncertainty will be modeled with the Barlow-Beeston-lite approach
    #threshold setting on the number of events below which the the Barlow-Beeston-lite approach is not used https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part5/longexercise/
    #hist-mode changes the way the normalisation effect of shape-altering uncertainties is handled. usually dont mess with
    binuncline = ''
    if usemcbinuncs:
        thr = 0
        includesig = 0
        binuncline = binlabel+' autoMCStats '+str(thr)+' '+str(includesig)
        print binuncline

    #write datacard to file

    f = open(cardfile, "w")
    print>>f, '## card: '+cardfile+' number in set: '+str(nbin)
    print>>f, '##selection: '+cutstr+'\n'
    print>>f, header
    print>>f, '## number of signals (i), backgrounds (j), and uncertainties (k)'
    print>>f, "---"
    print>>f, "imax: "+str(imax)
    print>>f, "jmax: "+str(jmax)
    print>>f, "kmax: "+str(kmax)
    print>>f, "---\n"
    if useshape:
        print>>f, '## input the bdt shape histograms'
        print>>f, "---"
        print>>f, shapeline
    print>>f, "---\n"
    print>>f, '## list the bin label and the number of (data) events observed'
    print>>f, "---"
    print>>f, "bin  "+binlabel
    print>>f, "observation "+str(obs)
    print>>f, "---\n"
    print>>f, '## expected events for signal and all backgrounds in the bins'
    print>>f, "---"
    print>>f, "bin    "+(("    "+binlabel)*nprocs)
    for i,p in enumerate(procnames):
        procline+=("    "+p)
        numline+=("        "+str(i))
    print>>f, "process"+procline
    print>>f, "process"+numline
    for r in rates:
        rateline+=("    "+str(r))
    print>>f, "rate   "+rateline        
    print>>f, "---\n"
    if not nounc:
        print>>f, '## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin'
        print>>f, "---"
        print>>f, '## statistical uncertainties\n'
        print>>f, stat_table
    # if not nounc:
        print>>f, "---"
        print>>f, '## systematic uncertainties\n'
        print>>f, syst_table
    if useshape:
        print>>f, "---"
        print>>f, '## shape uncertainties\n'
        print>>f, shape_table
        print>>f, "---\n"
        print>>f, binuncline
        print>>f, "---\n"
    
    f.close()

####################################################################
# reinitialize roothists

startbinnum=0
print 'how many years?', len(processes.keys())
for year in processes.keys():
    for cut in args.cutsets:
        for bin in  binset:
            print 'year', year, 'startbinnum', startbinnum
            print 'making cards for cut: ', cut, 'with baseline:', args.base, 'and bin: ', bin, '...'
            lastbinnum = cardsetup(args, cut, bin, year, startbinnum)
            print 'lastbinnum', lastbinnum
            print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
            print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n'
    startbinnum=lastbinnum

