import os
import sys
import math
import numpy as np
import argparse
from ROOT import gROOT,TFile,TTree,TH1D
import ROOT as r
from datacardrefs import datacardrefs
gROOT.SetBatch(True)
# import uproot
# import types
# import uproot_methods.classes.TH1
import numpy as np
from array import array

year = '2016'
outroot = 'allhad4top_2016_shapehists.root'
df = datacardrefs(year)
yields = {}
blind = False

def datahist(channel):
    histname = channel+'_'+year+'_data_obs'
    datahist = r.TH1D("hist", "hist", df.nbins, df.edges)
    # datahist = r.TH1F(histname, histname, df.nbins, df.edges)
        
    #actually fill the histogram if it isn't a blind analysis
    shapevar = 'catBDTdisc_nosys'
    # print df.binsels[channel]
    #print df.datafile
    binsel = '('+df.binsels[channel]+' && '+df.datasel
    dfile = r.TFile('/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/allsysts/JETHT_'+year+'_merged_0lep_tree.root')
    print dfile
    dtree = dfile.Get('Events')
    print dtree
    # print ptree.Scan(shapevar,binsel)
    # datahist.SetDirectory(0)
    # pfile.Close()
    print 'nentries',dtree.GetEntries(binsel)
    gROOT.cd()
    #this is where it is just not working
    # for entry in ptree:
    #     print entry.catBDTdisc_nosys
    dtree.Project("hist", "catBDTdisc_nosys")
    # dtree.Draw("catBDTdisc_nosys>>hist")#apply bin selection
    # ptree.Draw(shapevar+">>"+histname, binsel)#apply bin selection
    # print shapevar+'>>'+histname
    print binsel, '\n'
    
    print 'filled data hist ', histname, 'with selection', binsel, 'for process data_obs and bin', channel
    yld, unc = df.getyield(datahist)
    print yld,unc
    yields[histname] = [yld, unc]

    #open rootfile for writing histograms
    histfile = r.TFile(outroot, "UPDATE")
    datahist.Write()
    histfile.Close()
    del histfile

datahist('RT1BT0htbin2')
