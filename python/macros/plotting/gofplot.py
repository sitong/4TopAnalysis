import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
# import sys
# from VRerrtempl import VRerrtempl
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

names = {'1':[374.995, 'higgsCombineTest.GoodnessOfFit.mH120.1.root'],
         '2':[372.099, 'higgsCombineTest.GoodnessOfFit.mH120.2.root'],
         '3':[372.799, 'higgsCombineTest.GoodnessOfFit.mH120.3.root'],
         '4':[371.996, 'higgsCombineTest.GoodnessOfFit.mH120.4.root'],
         'partcorr':[356.422,'higgsCombineTest.GoodnessOfFit.mH120.123456.root']}

p = 'partcorr'
val = names[p][0]
infile = '/uscms_data/d3/mquinnan/combine/CMSSW_10_2_13/src/HiggsAnalysis/CombinedLimit/'+names[p][1]
treename = 'limit'
outname = './plots/gofhist_'

rf, t = rs.makeroot(infile, treename)
h = rs.gethist(t, 'limit', '', 50, (200, 450))
c = r.TCanvas("c","c",800,800)
h.Draw("hist")
l = r.TLine(val, c.GetUymin(), val, 5);
l.SetLineColor(r.kRed)
l.SetLineWidth(2)
l.Draw("same")
c.SaveAs(outname+p+'.png')
