import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
# parser.add_argument("-f", "--fitfile", dest="fitfile", default='output_shapes_sb.root')
parser.add_argument("-b", "--bin", dest="inbin", default='RT1BT1htbin1')
parser.add_argument("-t", "--typ", dest="typ", default='SB')
args = parser.parse_args()

class postfit:

    def __init__(self, args):
        self.sbfitfile = './fitDiagnostics/output_shapes_sb.root'
        self.bfitfile = './fitDiagnostics/output_shapes_bonly.root'
        self.inbin = args.inbin
        print 'fitfile', self.sbfitfile, 'bin', args.inbin
        self.procs = ['TTX', 'other', 'DDBKG', 'TTTT','data_obs', 'TotalProcs']
        self.bkgs = ['DDBKG', 'TTX', 'other']
        self.colors = {'TTTT':r.TColor.GetColor("#a1794a"),
                       'DDBKG':r.TColor.GetColor("#c1caf3"),
                       'TTX':r.TColor.GetColor("#163d4e"),
                       'other':r.TColor.GetColor("#d07e93")}
        # self.sbrf = r.TFile(self.sbfitfile, 'read')
        # self.brf = r.TFile(self.bfitfile, 'read')

        outdir_Bonly = './plots/postfit/Bonly/RunII/'
        outdir_SB = './plots/postfit/SB/RunII/'
        if not os.path.exists(outdir_Bonly):
            os.makedirs(outdir_Bonly)
        self.outdir_Bonly = outdir_Bonly
        if not os.path.exists(outdir_SB):
            os.makedirs(outdir_SB)
        self.outdir_SB = outdir_SB

        self.hnames = {'TTTT':'tttt', 'DDBKG':'QCD+tt (DD)', 'TTX':'ttH+ttV', 'other':'minor', 'data_obs':'data'}

    def getdirnames(self, year):
        dirnames = {'RT1BT0htbin0':'RT1BT0htbin0_'+year+'_datacard2_postfit',
                    'RT1BT0htbin1':'RT1BT0htbin1_'+year+'_datacard3_postfit',
                    'RT1BT0htbin2':'RT1BT0htbin2_'+year+'_datacard0_postfit',
                    'RT1BT0htbin3':'RT1BT0htbin3_'+year+'_datacard1_postfit',
                    'RT1BT0htbin4':'RT1BT0htbin4_'+year+'_datacard6_postfit',
                    'RT1BT0htbin5':'RT1BT0htbin5_'+year+'_datacard7_postfit',
                    'RT1BT0htbin6':'RT1BT0htbin6_'+year+'_datacard4_postfit',
                    'RT1BT0htbin7':'RT1BT0htbin7_'+year+'_datacard5_postfit',
                    'RT1BT1htbin0':'RT1BT1htbin0_'+year+'_datacard9_postfit',
                    'RT1BT1htbin1':'RT1BT1htbin1_'+year+'_datacard8_postfit',
                    'RT2BTALLhtbin0':'RT2BTALLhtbin0_'+year+'_datacard10_postfit',
                    'RT2BTALLhtbin1':'RT2BTALLhtbin1_'+year+'_datacard11_postfit'}
        return dirnames[self.inbin]
                        
    def addhists(self,hist1,hist2,hist3):
        hist1.Sumw2(); hist2.Sumw2(); hist3.Sumw2()
        hist = hist1.Clone('hist')
        hist.Add(hist2)
        hist.Add(hist3)
        hist.SetDirectory(0)
        return hist
        
    def gethists(self, rf):        
        rf = r.TFile(rf, 'read') 
        r.gROOT.cd()
        hists16 = {}
        hists17 = {}
        hists18 = {}
        hists = {}

        TD16 = self.getdirnames('2016')
        TD17 = self.getdirnames('2017')
        TD18 = self.getdirnames('2018')

        # print year
        for proc in self.procs:
            print proc
            # rf.cd(TD16)
            hist16 = rf.Get(TD16+'/'+proc)
            # rf.cd('../'); rf.cd(TD17)
            hist17 = rf.Get(TD17+'/'+proc)
            # rf.cd('../'); rf.cd(TD18)
            hist18 = rf.Get(TD18+'/'+proc)
            print TD16+'/'+proc
            print hist16
            hist16.SetDirectory(0); hist17.SetDirectory(0); hist18.SetDirectory(0) 
            hists16[proc]=hist16; hists17[proc]=hist17; hists18[proc]=hist18
            hist = self.addhists(hist16, hist17, hist18)
            hist.SetDirectory(0)
            hists[proc]=hist
        rf.Close()
        print hists
        return hists


    def plot_settings(self, hists, useSB=True):
        c = r.TCanvas("c","c",800,800)
        p1 = r.TPad("p1","p1", 0, 0.3, 1, 1.0)
        p1.SetBottomMargin(0.1)
        p1.Draw()
        p1.cd()

        stack = r.THStack("stack", "stack")
        legend = r.TLegend(0.1, 0.6, 0.3, 0.9)
        edges = np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0], dtype=np.double)
        stacktot = r.TH1F('stacktot', 'stacktot', len(edges)-1, edges)
        # stacktot = r.TH1F('stacktot', 'stacktot', 9, 0.000000, 1.000000)
        hcount = 0; hnames = []

        for proc in self.procs:
            plot = hists[proc]
            print proc
            if proc == 'data_obs':
                datahist = plot
                datahist.SetLineWidth(2)
                datahist.SetMarkerStyle(20)
                datahist.SetLineColor(r.kBlack)
                legend.AddEntry(datahist, "data", "L")           
                
            elif proc in self.bkgs:
                plot.SetLineWidth(1)
                plot.SetFillColor(self.colors[proc])
                plot.SetLineColor(self.colors[proc])
                stack.Add(plot)
                stacktot.Add(plot)
                legend.AddEntry(plot, self.hnames[proc], "F")           
                hcount+=1

            elif proc == 'TTTT' and useSB:
                plot.SetLineWidth(1)
                plot.SetFillColor(self.colors[proc])
                plot.SetLineColor(self.colors[proc])
                stack.Add(plot)
                stacktot.Add(plot)
                legend.AddEntry(plot, self.hnames[proc], "F")           
                hcount+=1
                
        #draw stack
        stack.SetTitle("")
        stack.Draw("hist")
        gPad.Modified()
        gPad.Update()
        ymax = p1.GetUymax()*1.6 #max+30%
        stack.SetMaximum(ymax)

        #set errors to total error 
        for ibin in range(0, stacktot.GetNbinsX()+2):
            err = hists['TotalProcs'].GetBinError(ibin)
            stacktot.SetBinError(ibin, err)

        datahist.Draw("psame")

        # stacktot = r.TGraphAsymmErrors(stacktot)
        stacktot.SetLineWidth(0) #dont show line
        stacktot.SetFillColor(r.kGray+2)
        stacktot.SetFillStyle(3002)
        stacktot.Draw("e2same")
        legend.AddEntry(stacktot, "uncertainty band", "F")

        stack.GetXaxis().SetNdivisions(505)
        stack.GetYaxis().SetTitleSize(20)
        stack.GetYaxis().SetTitleFont(43)
        stack.GetYaxis().SetTitleOffset(1.55)
        stack.GetYaxis().SetTitle('NEntries')
        stack.GetXaxis().SetTitle('')
        stack.GetXaxis().SetTitleSize(0)
        stack.GetXaxis().SetLabelSize(0)
        stack.GetYaxis().SetLabelFont(43)
        stack.GetYaxis().SetLabelSize(20)

        gPad.Modified()
        gPad.Update()

        legend.Draw()
        gPad.Modified()
        gPad.Update()

        #bottom plot
        c.cd()
        p2 = r.TPad("p2", "p2", 0, 0.05, 1, 0.3)
        p2.SetGridy()
        p2.Draw()
        p2.cd()

        ratio = datahist.Clone("ratio")
        ratio.SetLineColor(r.kBlack)
        ratio.Divide(stacktot) 
        ratio.SetMarkerStyle(20)

        raterr = datahist.Clone("raterr")
        raterr.SetLineWidth(0)
        raterr.SetFillColor(r.kGray+2)
        raterr.SetFillStyle(3002)
        raterr.SetMarkerStyle(1)
        raterr.SetMinimum(0.4)
        raterr.SetMaximum(1.6)

        #error bars
        for ibin in range(0, ratio.GetNbinsX()+2):
            derr = datahist.GetBinError(ibin)
            d = datahist.GetBinContent(ibin)
            relderr=0.0
            if d>0.0: relderr=derr/d

            err = stacktot.GetBinError(ibin)
            s = stacktot.GetBinContent(ibin)
            relerr=0.0
            if s>0:
                relerr = err/s

            raterr.SetBinError(ibin, relerr)
            ratio.SetBinError(ibin, relderr)
            raterr.SetBinContent(ibin, 1)
        
        #draw
        raterr.Draw("e2")
        ratio.Draw("pe0same")
        l = r.TLine(p2.GetUxmin(), 1.0, p2.GetUxmax(), 1.0);
        l.SetLineColor(r.kBlack)
        l.SetLineWidth(1)
        l.Draw("same")

        raterr.SetTitle("")
        raterr.GetYaxis().SetTitle("ratio data/prediction")
        raterr.GetYaxis().SetNdivisions(505)
        raterr.GetXaxis().SetNdivisions(505)
        raterr.GetYaxis().SetTitleSize(20)
        raterr.GetYaxis().SetTitleFont(43)
        raterr.GetYaxis().SetTitleOffset(1.55)
        raterr.GetYaxis().SetLabelFont(43)
        raterr.GetYaxis().SetLabelSize(20)
        raterr.GetXaxis().SetTitle("BDT discriminant")
        raterr.GetXaxis().SetTitleSize(20)
        raterr.GetXaxis().SetTitleFont(43)
        raterr.GetXaxis().SetTitleOffset(4.0)
        raterr.GetXaxis().SetLabelFont(43)
        raterr.GetXaxis().SetLabelSize(20)
        raterr.GetXaxis().SetTickLength(0.08)
        p2.SetTopMargin(0.1)
        p2.SetBottomMargin(0.2)
        gPad.Modified()
        gPad.Update()
        
        outdir = self.outdir_Bonly
        if useSB:
            outdir = self.outdir_SB
        
        histname=self.inbin
        print 'saved histogram: ', outdir+histname+'.png'
        c.SaveAs(outdir+histname+'.png')        
        c.SaveAs(outdir+histname+'.C')        

    def runhists(self):
        hists = self.gethists(self.sbfitfile)
        self.plot_settings(hists,True)
        hists = self.gethists(self.bfitfile)
        self.plot_settings(hists,False)
        r.gROOT.Reset()
        #save


pf = postfit(args)
pf.runhists()
