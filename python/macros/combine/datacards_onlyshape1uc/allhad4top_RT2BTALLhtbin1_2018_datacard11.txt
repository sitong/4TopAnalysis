## card: datacards_onlyshape1uc/allhad4top_RT2BTALLhtbin1_2018_datacard11.txt number in set: 11
##selection: (nrestops_nosys>=2 && nbstops_nosys>=0 && ht_nosys>=1100 &&  nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*59.74*prefirewgt*

#----------------------------------------------#
#generating datacards. name: allhad4top, year2018

#number of cards in set: 12

## number of signals (i), backgrounds (j), and uncertainties (k)
---
imax: 1
jmax: 3
kmax: *
---

## input the bdt shape histograms
---
shapes * * allhad4top_2018_shapehists.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
---

## list the bin label and the number of (data) events observed
---
bin  RT2BTALLhtbin1_2018
observation 419.0
---

## expected events for signal and all backgrounds in the bins
---
bin        RT2BTALLhtbin1_2018    RT2BTALLhtbin1_2018    RT2BTALLhtbin1_2018    RT2BTALLhtbin1_2018
process    TTTT    TTX    other    DDBKG
process        0        1        2        3
rate       3.46621530037    15.8628344536    4.57654732466    350.532
---

## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin
---
## statistical uncertainties

RT2BTALLhtbin1_2018_TTTT_stat      lnN    1.01386043158    -    -    -
RT2BTALLhtbin1_2018_TTX_stat      lnN    -    1.08793036618    -    -
RT2BTALLhtbin1_2018_other_stat      lnN    -    -    1.23783147196    -
RT2BTALLhtbin1_2018_DDBKG_stat      lnN    -    -    -    1.16939965538

---
## systematic uncertainties

lumi_uncorr_2018   lnN    1.015    1.015    1.015    -    
lumi_corr16_17_18   lnN    1.020    1.020    1.020    -    
lumi_corr17_18   lnN    1.002    1.002    1.002    -    
elveto_2018   lnN    1.02767     1.01011    -    -    
muveto_2018   lnN    1.00131     1.00009    -    -    
cross_section   lnN   -   1.25    -    -    
data_pred_shape_disagreement_RT2BTALLhtbin1_2018  shape     -    -    -    1.0  

---
## shape uncertainties

jes_2018   shape     1.0   1.0   1.0    - 
jer_2018   shape     1.0   1.0   1.0    - 
pileup_2018   shape     1.0   1.0   1.0    - 
trigger_2018   shape     1.0   1.0   1.0    - 
btagLF  shape     1.0    1.0    1.0    - 
btagHF  shape     1.0    1.0    1.0    - 
btagLFstats1_2018   shape     1.0   1.0   1.0    - 
btagHFstats1_2018   shape     1.0   1.0   1.0    - 
btagLFstats2_2018   shape     1.0   1.0   1.0    - 
btagHFstats2_2018   shape     1.0   1.0   1.0    - 
btagCFerr1  shape     1.0    1.0    1.0    - 
btagCFerr2  shape     1.0    1.0    1.0    - 
DeepAK8TopSF_2018   shape     1.0   1.0   1.0    - 
DeepAK8WSF_2018   shape     1.0   1.0   1.0    - 
isr_TTTT  shape     1.0    -    -    -  
isr_TTX  shape     -    1.0    -    -  
fsr_TTTT  shape     1.0    -    -    -  
fsr_TTX  shape     -    1.0    -    -  
pdf  shape     1.0    1.0    -    - 
ME_TTTT  shape     1.0    -    -    -  
ME_TTX  shape     -    1.0    -    -  
ResTopEff_2018   shape     1.0   1.0   1.0    - 
ResTopMiss_2018   shape     1.0   1.0   1.0    - 

---

RT2BTALLhtbin1_2018 autoMCStats 0 1
---

