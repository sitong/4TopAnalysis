# easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
sys.path.append('../combine/')
from datacard_dictionary import datacard_dict

#creates stacked histograms with ratio plot below
#references controlhists.py and uses plottery

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
# parser.add_argument("-i", "--inhistfile", dest="inhistfile", default='../combine/NAF_ht_2016_shapehists_12bins_oct22.root')
# parser.add_argument("-n", "--cr", dest="cr", default='dataextclosure')
parser.add_argument("-v", "--base", dest="base", default='VR')
parser.add_argument("-c", "--cut", dest="cutname", default='BOTH')
parser.add_argument("-b", "--bin", dest="binname", default='ht')
parser.add_argument("-l", "--logy", dest="logy", default='True')
parser.add_argument("-y", "--year", dest="year", default='2016')
# parser.add_argument("-x", "--histbinnum", dest="histbinnum", default=10)
# parser.add_argument("-r", "--selrange", dest="selrange", default=[0,2500])
parser.add_argument("-s", "--samp", dest="samp", default='BOTH') #QCD or TT
args = parser.parse_args()

verbose=True

metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
metfilters   = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag)'

logy=True
if args.logy=='True':
    logy=True
elif args.logy=='False':
    logy=False

subtractMC = False
channel = '0lep' #0lep, 1lep, 1el, 1mu 
# cr = args.cr
samp = args.samp

channame = channel
if channel=='1el' or channel=='1mu' or channel=='mergey':
    channame='1lep'

# varstoplot = ['ht', 'nrestops', 'nbstops']

subhist = False
varstoplot = ['ht_nosys']
# varstoplot = ['catBDTdisc']
samplist = ["TTTT", "TTX", "minor"]
year = args.year
lumi = str(rs.lumis[year][channel])
tn = 'Events'
cutname=args.cutname
binname=args.binname

NAFfile = ''
if binname =='ht_nosys' and args.base=='SR':
    NAFfile = '../combine/TTMC_SR/TTMC_SR_'+year+'.root'
    # NAFfile = '../combine/NAF_with_mixed_yield/NAF_SR_'+year+'_new_yield.root'
    # NAFfile = '../combine/NAFfiles/NAF_SR_'+year+'_shapehists.root'
elif binname =='ht_nosys' and args.base=='VR':
    NAFfile = '../combine/NAF_with_mixed_yield/NAF_8jet_'+year+'_new_yield.root'
    # NAFfile = '../combine/NAFfiles/NAF_8jet_'+year+'_shapehists_with_datadriven.root'
NAFfile = r.TFile(NAFfile)

datafiles = {
    # '0lep':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/JETHT_'+year+'_merged_'+channame+'_tree.root' ,}  
    '0lep':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintreeswithsysts/JETHT_'+year+'_merged_'+channame+'_tree.root' ,}

mcfiles = {'TTTT': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/TTTT_'+year+'_merged_'+channame+'_tree.root',
           'TTX': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/TTX_'+year+'_merged_'+channame+'_tree.root',
           'minor': '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/minorMC_'+year+'_merged_'+channame+'_tree.root',
       }

QCD_TT_file =  '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/QCD_TT_'+year+'_merged_'+channame+'_tree.root'
MC_file =  '/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/maintrees_newWP2/MC_'+year+'_merged_'+channame+'_tree.root'

mcstr = {'2016' : '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi,
         '2017' : '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi,
         '2018' : '*weight*evtbtagSFshape_preselJets*puWeight*trigSF_0lep*'+lumi,}

dstr = {'2016' : '*trigCorr_0lep'+metfilters16,
        '2017' : '*trigCorr_0lep'+metfilters,
        '2018' : '*trigCorr_0lep'+metfilters,}
# dstr = {'2016' : '*trigCorr_0lep && (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056==1 || HLT_PFHT450_SixJet40_BTagCSV_p056==1)'+metfilters16,
#         '2017' : '*trigCorr_0lep && (HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0 || HLT_PFHT430_SixJet40_BTagCSV_p080 || HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2 || HLT_PFHT380_SixJet32_DoubleBTagCSV_p075)'+metfilters,
#         '2018' : '*trigCorr_0lep && (HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 || HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 ||HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)'+metfilters,}

def makeHists(cutid, plotvar, data, otherhist, hists, labels):
    
    colors = [r.kRed-7, r.kOrange+1, r.kYellow-7, r.kGreen-9, r.kCyan-7, r.kAzure+1, r.kBlue-9, r.kViolet-4, r.kMagenta-9]
    outname = 'plots/srbinhists'+args.base+'/'+cutid+'_'+year+'_'+plotvar+'.png'
         
    print outname
    print 'log?',logy
    print 'data',data
    print 'hists',hists
    print 'num bgs', len(hists)
    print 'colors',colors
    print 'labels',labels
    ply.plot_hist( 
        data = data,
        bgs  = hists,
        sigs = [otherhist],
        # syst = hsyst,
        sig_labels = ["scaled TT+QCD MC"],#["el", "mu"],
        colors = colors, 
        legend_labels = labels,
        options = {
            "clopper_pearson":[],
            "do_stack": True,
            "xaxis_label": plotvar,
            "yaxis_label": "Events",
            "legend_scalex": 0.7,
            "legend_scaley": 1.2,
            "bkg_sort_method":'ascending',
            # "extra_text": [plotvar],
            "yaxis_log": logy,
            "ratio_range":[0.0,2.0],
            # "ratio_pull": True,
            "hist_disable_xerrors": False,
            "show_bkg_errors": True,
            "bkg_err_fill_style":3003,
            "bkg_err_fill_color":r.kGray,
            # "palette_name": "pastel",
            "yaxis_moreloglabels": logy, 
            "legend_percentageinbox": False,
            "legend_alignment": "top right",
            "cms_label": "Preliminary",
            # "lumi_value": 36,
            "legend_smart": False,
            # "yaxis_range":[0,10],
            "output_name": outname,
            }
       )

def getyield(args, chanid, hist):
    errorVal = r.Double(0)
    minbin=0
    maxbin=hist.GetNbinsX()+1
    hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
    print 'channel', chanid, 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
    return hyield,  errorVal

def gethist(name, filename, projvar, cut, nbins, selrange, treename='Events'):
    rf, t = rs.makeroot(filename, treename)
    hist = rs.gethist(t, projvar, cut, nbins, selrange)
    rs.fixref(rf, hist)
    return hist

# def getMChist(CR, CRhist, mcprocs, chanid, chansel, histbinnum, selrange):
#     #returns MC subtracted CR hist
#     crdict = {        'CR1'       : '(nleptons==0 && ht>=700 && nbjets==2 && njets==7', #for VR
#                       'CR2'       : '(nleptons==0 && ht>=700 && nbjets>=3 && njets==7',
#                       'CR3'       : '(nleptons==0 && ht>=700 && nbjets==2 && njets==8',
#                       'CR1p'      : '(nleptons==0 && ht>=700 && nbjets==2 && njets==6',
#                       'CR2p'      : '(nleptons==0 && ht>=700 && nbjets>=3 && njets==6',
#                       }

#     crsel = chansel + crdict[CR] + mcstr[year]
#     if verbose: 
#         print CR, crsel
#     mcyield = 0; mcunc = 0
#     for proc in mcprocs:
#         mchist = gethist(proc, mcfiles[proc], varname, crsel, histbinnum, selrange)
#         yld, unc = getyield(args, chanid, mchist)
#         if proc!='TTTT':
#             mcyield+=yld
#             mcunc+=(unc*unc)
#         mcunc = np.sqrt(mcunc)

        

def SRbinhists(args, varname, predfile, cutname, binname, year, mcprocs):
    histbinnum = 1
    selrange = [0,1]
    if varname =='ht_nosys':
        selrange = [0,2500]

    # datafile=False
    # if args.base=='VR':
    datafile = datafiles[channel]

    binsels = datacard_dict(args.base, cutname, binname, year, 'weight', topsplit=False, basic=True)
    print 'making extABCD histograms for ', len(binsels.keys()), 'bins'
        
    rtbool = False
    if (varname=='restop1pt'): rtbool=True
    dlumi='1.0'

    if subhist :
        binsels = {'cut2bin1':'nrestops>=2 && nbstops>=0'}
        # binsels = {'cut2bin0':binsels['cut2bin0']}
    for chanid, chansel in binsels.iteritems():

        histlist=[]
        labels = []
        print chanid, chansel
        mysel = '('+chansel+' && ' #matches dataextclosure style of selection
        if args.base=='SR':
            basesel = 'nfunkyleptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9)'
        elif args.base == 'VR':
            basesel = 'nfunkyleptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys==8)'
        mcsel = mysel + basesel + mcstr[year]
        datasel = mysel + basesel + dstr[year]

        #data
        cr = 'dataextclosure'
        # if chanid=='cut2bin1' or chanid=='cut1bin1':
        #     cr = 'dataclosure'
        #mc:
        mccr = 'extclosure'
        if subhist:
            cr = 'dataextclosure'
            mccr = 'extclosure'
        mccuts = cp.getclosurecuts(mccr, 'mc', channel, year, lumi, rtbool, mysel)
        # mccr = 'extclosure'
        # if chanid=='cut2bin1' or chanid=='cut1bin1':
        #     mccr ='closure'
        print channel, lumi, year
        mcCRylds = {}
        mcyield=0.0; mcunc=0.0
        for proc in mcprocs:
            labels.append(proc)
            #need PREDICTED mc yields 
            mchist, mcCRhists = cp.getClosure(varname, mccr, '0lep', mcfiles[proc], 'Events', lumi, year, mccuts, histbinnum, selrange, returnhists=True)
            print proc, 'REAL MC yields:'
            realmchist = gethist(proc, mcfiles[proc], varname, mcsel, histbinnum, selrange)
            yld, unc = getyield(args, chanid, realmchist)
            mcyield+=yld
            mcunc+=(unc*unc)
            histlist.append(realmchist)
            # if proc!='TTTT':
            for CR, CRhist in mcCRhists.iteritems():
                print CR, 'mc yield:'
                cry,cryu = getyield(args, chanid, CRhist)
                mcCRylds[CR] = cry
                mcCRylds[CR+'unc']= cryu
        mcunc = np.sqrt(mcunc)
        print 'mcsel',mcsel

        if chanid=='cut2bin1' and subhist:
            # replace predhist with full mc prediction
            predhist, CRhists = cp.getClosure(varname, cr, '0lep', MC_file, 'Events', lumi, year, mccuts, histbinnum, selrange, returnhists=True)
            cuts = cp.getclosurecuts(cr, 'data', channel, year, dlumi, rtbool, mysel)
            predhist, CRhists = cp.getClosure(varname, cr, '0lep', predfile, 'Events', dlumi, year, cuts, histbinnum, selrange, returnhists=True)
            if varname =='catBDTdisc':
                NAFname = chanid+'_'+year+'_BKG'
                predhist = NAFfile.Get(NAFname)            
        else:
            cuts = cp.getclosurecuts(cr, 'data', channel, year, dlumi, rtbool, mysel)
            predhist, CRhists = cp.getClosure(varname, cr, '0lep', predfile, 'Events', dlumi, year, cuts, histbinnum, selrange, returnhists=True)
            if varname =='catBDTdisc':
                NAFname = chanid+'_'+year+'_BKG'
                predhist = NAFfile.Get(NAFname)            

        print 'CR yields:'
        CRylds = {}
        for CR, CRhist in CRhists.iteritems():
            # need to get the mc yields for each CR
            # CRhist = getMChist(CR, CRhist, mcprocs, chanid, mcsel, histbinnum, selrange)
            print CR, 'yield:'
            cry,cryu = getyield(args, chanid, CRhist)
            if subtractMC:
                CRylds[CR] = cry-mcCRylds[CR]
                CRylds[CR+'unc']= np.sqrt((cryu*cryu)+(mcCRylds[CR+'unc']*mcCRylds[CR+'unc']))
            else :
                CRylds[CR] = cry
                CRylds[CR+'unc']=cryu

        calcyld=0.0; calcunc=0.0
        if cr=='dataextclosure':
            calcyld = (CRylds['Ap']/(CRylds['B']*CRylds['Cp']))*(((CRylds['B']*CRylds['C'])/CRylds['A'])**2)
            calcunc=(2.0*((CRylds['Bunc']/CRylds['B'])**2+ (CRylds['Cunc']/CRylds['C'])**2+ (CRylds['Aunc']/CRylds['A'])**2) + (CRylds['Bunc']/CRylds['B'])**2+ (CRylds['Cpunc']/CRylds['Cp'])**2+ (CRylds['Apunc']/CRylds['Ap'])**2)
            calcunc = calcyld*np.sqrt(calcunc)
            if verbose: print chanid,'DD yield from CR integrals (extABCD):'

        elif cr=='dataclosure':
            calcyld = ((CRylds['B']*CRylds['C'])/CRylds['A'])
            calcunc = calcyld*np.sqrt( (CRylds['Bunc']/CRylds['B'])**2 + (CRylds['Cunc']/CRylds['C'])**2 + (CRylds['Aunc']/CRylds['A'])**2)
            if verbose: print chanid,'DD yield from CR integrals (ABCD):'
            
        # if verbose: print calcyld, calcunc,'\n'
        #get yield of histograms
        print '===================================='
        # print 'predicted integrated DD yield:'
        py, pyu = getyield(args, chanid, predhist)

        # print 'total mc yield:'
        # print round(mcyield, 3), '+/-', round(mcunc, 3), '\n' 

        if subtractMC:
            print 'DD BKGD - MC yields for channel',chanid, ':', round(calcyld,3), '+/-', round(calcunc,3)
        else:
            print 'yields',chanid, ':', round(calcyld,3), '+/-', round(calcunc,3)
            # print '% background MC driven:', calcyld/py, '\n'
            #then you need to scale  the DD BKGD histogram 
        predhist.Scale(calcyld/py)
        histlist.append(predhist); labels.append('DD_BKGD')

        if not datafile:
            print 'data blinded...'
            datahist=predhist
        else:
            #print datafile
            datahist = gethist('data', datafile, varname, datasel, histbinnum, selrange)
            print '\n true data yield:'
            dy, dyu = getyield(args, chanid, datahist)
            print 'data/pred', round((dy/(calcyld+mcyield)),2)

        print '%MC', round((mcyield/(calcyld+mcyield))*100.0, 2)
        print '===================================='

        qcdtthist = gethist('QCD_TT', QCD_TT_file, varname, mcsel, histbinnum, selrange)
        qcdttyield, qcdttunc = getyield(args, chanid, qcdtthist)
        qcdtthist.Scale(calcyld/qcdttyield)
            
        #make histograms
        makeHists(chanid, varname, datahist, qcdtthist, histlist, labels)

for v in varstoplot:
    if channel=='0lep':
        # print v, cr
        sampfile = datafiles[channel]
        #now split predhist into the SR bins and get the yields with error
        SRbinhists(args, v, sampfile, cutname, binname, year, samplist)
        # basehist(v, sampfile, cutname, binname, year)
    else:
        print 'invalid channel or cr!'


        # c1 = r.TCanvas("c1","multipads",700,700)
        # c1.cd()

        #now draw the histogram to an image and a rootfile if desired
        # outdir = './plots/control_hists/SRbinhists'+year+'/'
        # if not os.path.exists(outdir):
        #     os.makedirs(outdir); print 'Making output directory ', outdir

        # mycolor = r.kViolet-9
        # predhist.SetLineColor(r.TColor.GetColorDark(mycolor))
        # predhist.SetFillColor(mycolor)
        # predhist.SetLineWidth(2)
        # predhist.GetXaxis().SetTitle(varname)
        # predhist.GetYaxis().SetTitle('NEntries')
        # predhist.SetTitle(chanid+'_'+year+'_datapred')
        # predhist.Draw('histe1')

        # r.gPad.Modified()
        # c1.Update()
        # c1.SaveAs(outdir+outname)

        # print 'created histogram', outdir+outname

# def basehist(varname, sampfile, cutname, binname, year, outroot=False):
#     outdir = './plots/control_hists/SRbinhists'+year+'/'
#     if not os.path.exists(outdir):
#         os.makedirs(outdir); print 'Making output directory ', outdir

#     rtbool = False
#     if (varname=='restop1pt'): rtbool=True
#     lumi='1.0'

#     mysel = '(nrestops>=1 &&'
#               #matches dataextclosure style of selection

#     c1 = r.TCanvas("c1","multipads",700,700)
#     c1.cd()

#     cuts = cp.getclosurecuts('dataextclosure', 'data', channel, year, lumi, rtbool, mysel)

#     outname = varname+'_baseline_'+year+'_datapredhist.png'
#     predhist = cp.getClosure(varname, 'dataextclosure', '0lep', sampfile, 'Events', lumi, year, cuts)

#     pred_errorVal = r.Double(0)
#     minbin=0
#     maxbin=predhist.GetNbinsX()+2
#     pred_yield = predhist.IntegralAndError(minbin, maxbin, pred_errorVal)
#     print  'baseline yield:', round(pred_yield, 5), 'stat unc:', round(pred_errorVal, 5), '\n'


#     #now draw the histogram to an image and a rootfile if desired
#     mycolor = r.kViolet-9
#     predhist.SetLineColor(r.TColor.GetColorDark(mycolor))
#     predhist.SetFillColor(mycolor)
#     predhist.SetLineWidth(2)
#     predhist.GetXaxis().SetTitle(varname)
#     predhist.GetYaxis().SetTitle('NEntries')
#     predhist.SetTitle('baseline_'+year+'_datapred')
#     predhist.Draw('histe1')

#     r.gPad.Modified()
#     c1.Update()
#     c1.SaveAs(outdir+outname)
    # print 'created histogram', outdir+outname
