import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import Defs as defs
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
# import sys
# from VRerrtempl import VRerrtempl
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

infile = '/uscms_data/d3/mquinnan/nano4top/CMSSW_10_2_20_UL/src/PhysicsTools/NanoTrees/python/macros/eventMVA/ttttgentestrootfiles/tttt_gentest_2016.root'
treename = 'Events'
outname = './plots/ttttgentest'
var = 'catBDTdisc_nosys'

colors = [r.kCyan-7, r.kBlue-9, r.kGreen-9, r.kViolet-4]

metfilters16 = '&& (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag)'
mcstr = metfilters16+')*weight*btagSF_nosys*puWeight*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*'+str(rs.lumis['2016']['0lep'])
base = '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && nrestops_nosys>=1  '
# base = '( ngen_muons>=0 '
# base = '(nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3  '
lepsels = ['&& (ngen_muons==0 && ngen_electrons==0 && ngen_taus==0)', '&& (ngen_muons==0 && ngen_electrons>=1 && ngen_taus==0)', '&& (ngen_muons>=1 && ngen_electrons==0 && ngen_taus==0)', '&& (ngen_muons==0 && ngen_electrons==0 && ngen_taus>=1)']

hnames = ['hadronic', 'contains electrons', 'contains muons', 'contains taus']

sels = []
for ls in lepsels:
    sel = base+ls+mcstr
    print sel
    sels.append(sel)

c = r.TCanvas("c","c",800,800)
rf, t = rs.makeroot(infile, treename)
stack = r.THStack("stack", "stack")
legend = r.TLegend(0.1, 0.6, 0.3, 0.9)

print sels[0]
had  = rs.gethist(t, var, sels[0], 10, (0, 1))
els  = rs.gethist(t, var, sels[1], 10, (0, 1))
mus  = rs.gethist(t, var, sels[2], 10, (0, 1))
taus = rs.gethist(t, var, sels[3], 10, (0, 1))
stackhists = [had, els, mus, taus]

hcount=0
for hist in stackhists:
    print hnames[hcount]
    hist.SetLineWidth(2)
    hist.SetFillColor(colors[hcount])
    hist.SetLineColor(colors[hcount])
    legend.AddEntry(hist, hnames[hcount], "F")           
    rs.getyield(hist, verbose=True)
    stack.Add(hist)
    hcount+=1

stack.SetTitle("")
stack.Draw("hist")
c.SaveAs(outname+'.png')

