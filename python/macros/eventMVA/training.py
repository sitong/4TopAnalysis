# Load files, mk Trees
import uproot
import pandas as pd
import numpy as np
import sklearn
import matplotlib.pyplot as plt
from catboost.utils import get_roc_curve
from sklearn.metrics import accuracy_score
from sklearn import metrics

# Setting
model_path = '/content/drive/My Drive/model_v7/'
train_rate = 0.75              #In my case, I use 75% number of TT as BKG and same number of TTTT as signal to my training.

# Set the input of BDT
modi = ['nJet','nbJet','sphericity','aplanarity','met','nsdw','metovsqrtht','ht','1leadTpt','2leadTpt','leadbpt',
'bjht','dphij1j2','dphib1b2','detaj1j2','detab1b2','sumfatjetmass']
additional =  ["evWeight",'Idx']    # The evWeight is PUweight*genWeight*btagweight, I use them for Fill the dist of disc value. 
                                    # For Idx, I save evt with the nJ>=9, nb>=3 as Idx 0, And CR1 as Idx 1, CR2 as Idx 2..
                                    # I select event with Inx = 0 in the function "condi"
inputs = modi+additional

# Definition some functions
def mk_test_sample(pd_df,sig=False): 
  # Signal has label = 1 , BGK has lable = 0
  if sig :
    labels = pd.DataFrame({'label': np.array([1]*pd_df.shape[0])})
  else : 
    labels = pd.DataFrame({'label': np.array([0]*pd_df.shape[0])})
  pddf_w_lable = pd.concat((pd_df, labels), axis=1)
  sample  = pddf_w_lable
  return sample

def shuffle_split(bg_pd,sig_pd,single_mode=False,we=False):
  if we:
    weights = pd.DataFrame({'weight': np.array([bg_pd.shape[0]/sig_pd.shape[0]]*sig_pd.shape[0])})
    sig_pd = pd.concat((sig_pd, weights), axis=1)
    weights = pd.DataFrame({'weight': np.array([1.]*bg_pd.shape[0])})
    bg_pd  = pd.concat((bg_pd, weights), axis=1)
  if single_mode:
    fulldata = bg_pd
  if not single_mode:
    fulldata = pd.concat([bg_pd,sig_pd]).reset_index(drop=True)
    fulldata = fulldata.sample(frac=1).reset_index(drop=True)
  y = fulldata["label"]
  z = fulldata["evWeight"]
  x = fulldata.drop(['label','evWeight'], axis=1)
  if we:
    z = fulldata["weight"]
    x = fulldata.drop(['label','evWeight','weight'], axis=1)
  return x,y,z

def Draw_disc(sig_out,bg_out,Weights=None,NN=False,save=None):
  fig,ax = plt.subplots()
  ax.hist(sig_out, bins=40, range=(0.0,1.0),density=True, alpha=0.6, label='Sig')
  ax.hist(bg_out , bins=40,weights=Weights, range=(0.0,1.0),density=True, alpha=0.6, label='BG')
  ax.xaxis.set_tick_params(labelsize=14)
  ax.yaxis.set_tick_params(labelsize=14)
  ax.set_xlabel('discriminants value', fontsize=14)
  ax.xaxis.set_label_coords(0.75, -0.10)
  ax.set_ylabel('normalized', fontsize=14)
  ax.legend(bbox_to_anchor=(0.84, 1), loc='upper left', borderaxespad=0.)
  if save != None:
    fig.savefig(model_path+'%s.png'%(save), dpi=fig.dpi)

def Draw_ROC(y_val,y_out,weight,NN=False,save=None):
  fpr, tpr, thresholds = sklearn.metrics.roc_curve(y_val,y_out,sample_weight=weight)  #acutally y_val,yout_nn
  roc_auc = sklearn.metrics.auc(fpr, tpr)
  plt.figure(figsize=(6, 6))
  lw = 2
  plt.plot(fpr, tpr, color='darkorange', lw=lw, label='ROC curve (area = %0.3f)' % roc_auc, alpha=0.5)
  plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--', alpha=0.5)
  plt.xlim([0.0, 1.0])
  plt.ylim([0.0, 1.05])
  plt.xticks(fontsize=16)
  plt.yticks(fontsize=16)
  plt.grid(True)
  plt.xlabel('False Positive Rate', fontsize=16)
  plt.ylabel('True Positive Rate', fontsize=16)
  plt.legend(loc="lower right", fontsize=16)
  fig = plt.gcf()
  plt.show()
  if save != None:
    fig.savefig(model_path+'%s_%d.png'%(save), dpi=fig.dpi)

def load_qcd_weight():
  q_t4 = [xq2_val,xq3_val,xq4_val,xq5_val,xq6_val,xq7_val]
  Xsec = np.array([347700000.0,32100000.0,6831000.0,1207000.0,119900.0,25240.0])
  Entr = np.array([54337325.0,62622029.0,37233786.0,15067818.0,11839357.0,6019541.0])
  w_ratio = Xsec/Entr
  lis = []
  num_q = 0
  num_t4 = xt4_val.shape[0]
  for i in range(len(q_t4)) :
    lis.append(np.array([w_ratio[i]]*q_t4[i].shape[0]))
    num_q += q_t4[i].shape[0]*w_ratio[i]
  weight_for_hist = np.hstack(lis)
  weight_for_roc = (weight_for_hist/num_q*num_t4).reshape(weight_for_hist.shape[0],1)
  weight_for_roc = np.vstack((weight_for_roc,np.array([1]*num_t4).reshape(num_t4,1)))
  return weight_for_hist, weight_for_roc 

def load_weight():
  tt_qcd = [xt2_val, xq2_val,xq3_val,xq4_val,xq5_val,xq6_val,xq7_val]
  Xsec = np.array([831760.0,347700000.0,32100000.0,6831000.0,1207000.0,119900.0,25240.0])
  Entr = np.array([153790053.0,54337325.0, 62622029.0,37233786.0, 15067818.0, 11839357.0, 6019541.0])
  w_ratio = Xsec/Entr
  lis = []
  num_tt_qcd = 0
  num_t4 = xt4_val.shape[0]
  for i in range(len(tt_qcd)) :
    lis.append(np.array([w_ratio[i]]*tt_qcd[i].shape[0]))
    num_tt_qcd += tt_qcd[i].shape[0]*w_ratio[i]
  weight_for_hist = np.hstack(lis)
  weight_for_roc = (weight_for_hist/num_tt_qcd*num_t4).reshape(weight_for_hist.shape[0],1)
  weight_for_roc = np.vstack((weight_for_roc,np.array([1]*num_t4).reshape(num_t4,1)))
  return weight_for_hist, weight_for_roc 

def condi(df) :
  df0 = df[ (df['Idx'] == 0) ]
  df = df0.drop(['Idx'], axis=1)
  df.reset_index(inplace=True, drop=True)
  return df

#read ROOT files
ttttroot1 = uproot.open('/content/drive/My Drive/data_v6/Skim_tttt_0.root')
ttroot0 = uproot.open('/content/drive/My Drive/data_v6/Skim_tt_0.root')
qcdroot2 = uproot.open('/content/drive/My Drive/data_v6/Skim_qcd_300to500.root')
qcdroot3 = uproot.open('/content/drive/My Drive/data_v6/Skim_qcd_500to700.root')
qcdroot4 = uproot.open('/content/drive/My Drive/data_v6/Skim_qcd_700to1000.root')
qcdroot5 = uproot.open('/content/drive/My Drive/data_v6/Skim_qcd_1000to1500.root')
qcdroot6 = uproot.open('/content/drive/My Drive/data_v6/Skim_qcd_1500to2000.root')
qcdroot7 = uproot.open('/content/drive/My Drive/data_v6/Skim_qcd_2000toInf.root')

# Get TTree
tttt_tree = ttttroot1['Events']
tt_tree = ttroot0['Events']
qcd_tree2 = qcdroot2['Events']
qcd_tree3 = qcdroot3['Events']
qcd_tree4 = qcdroot4['Events']
qcd_tree5 = qcdroot5['Events']
qcd_tree6 = qcdroot6['Events']
qcd_tree7 = qcdroot7['Events']

# read ROOT files
tttt_pd_ = tttt_tree.pandas.df(inputs)
tt_pd_   = tt_tree.pandas.df(inputs)
qcd_pd2_ = qcd_tree2.pandas.df(inputs)
qcd_pd3_ = qcd_tree3.pandas.df(inputs)
qcd_pd4_ = qcd_tree4.pandas.df(inputs)
qcd_pd5_ = qcd_tree5.pandas.df(inputs)
qcd_pd6_ = qcd_tree6.pandas.df(inputs)
qcd_pd7_ = qcd_tree7.pandas.df(inputs)

tt_pd   = condi(tt_pd_) 
tttt_pd = condi(tttt_pd_) 
qcd_pd2 = condi(qcd_pd2_) 
qcd_pd3 = condi(qcd_pd3_) 
qcd_pd4 = condi(qcd_pd4_) 
qcd_pd5 = condi(qcd_pd5_) 
qcd_pd6 = condi(qcd_pd6_) 
qcd_pd7 = condi(qcd_pd7_) 

# Divide the tt and tttt sample for training and validataion

tt_pd_for_tr = tt_pd[0:int(train_rate*tt_pd.shape[0])]
tt_pd_for_te = tt_pd[int(train_rate*tt_pd.shape[0]):].reset_index(drop=True)
# tt_pd_for_tr = tt_pd[0:10000]                         # you can commend this for using certain number of traing data set.
# tt_pd_for_te = tt_pd[10000:].reset_index(drop=True)
tttt_pd_for_tr = tttt_pd[0:tt_pd_for_tr.shape[0]]
tttt_pd_for_te = tttt_pd[tt_pd_for_tr.shape[0]:tt_pd_for_tr.shape[0]+tt_pd_for_te.shape[0]].reset_index(drop=True)

tr_tttt = mk_test_sample(tttt_pd_for_tr,sig=True)
te_tttt = mk_test_sample(tttt_pd_for_te,sig=True)
tr_tt   = mk_test_sample(tt_pd_for_tr)
te_tt   = mk_test_sample(tt_pd_for_te)
te_qcd2 = mk_test_sample(qcd_pd2)
te_qcd3 = mk_test_sample(qcd_pd3)
te_qcd4 = mk_test_sample(qcd_pd4)
te_qcd5 = mk_test_sample(qcd_pd5)
te_qcd6 = mk_test_sample(qcd_pd6)
te_qcd7 = mk_test_sample(qcd_pd7)

X_train,y_train,z_tr = shuffle_split(tr_tt,tr_tttt,we=True)
X_validation,y_validation,z_te = shuffle_split(te_tt,te_tttt,we=True)
xt2_val,yt2_val,zt2_val = shuffle_split(te_tt,None,single_mode=True)
xt4_val,yt4_val,zt4_val = shuffle_split(te_tttt,None,single_mode=True)
xq2_val,yq2_val,zq2_val = shuffle_split(te_qcd2,None,single_mode=True)
xq3_val,yq3_val,zq3_val = shuffle_split(te_qcd3,None,single_mode=True)
xq4_val,yq4_val,zq4_val = shuffle_split(te_qcd4,None,single_mode=True)
xq5_val,yq5_val,zq5_val = shuffle_split(te_qcd5,None,single_mode=True)
xq6_val,yq6_val,zq6_val = shuffle_split(te_qcd6,None,single_mode=True)
xq7_val,yq7_val,zq7_val = shuffle_split(te_qcd7,None,single_mode=True)

#For QCD
x_val_qcd = pd.concat([xq2_val,xq3_val,xq4_val,xq5_val,xq6_val,xq7_val,xt4_val]).reset_index(drop=True)
y_val_qcd = pd.concat([yq2_val,yq3_val,yq4_val,yq5_val,yq6_val,yq7_val,yt4_val]).reset_index(drop=True)

#For QCD + TT
x_val_t2_qcd = pd.concat([xt2_val,xq2_val,xq3_val,xq4_val,xq5_val,xq6_val,xq7_val,xt4_val]).reset_index(drop=True)
y_val_t2_qcd = pd.concat([yt2_val,yq2_val,yq3_val,yq4_val,yq5_val,yq6_val,yq7_val,yt4_val]).reset_index(drop=True)


from catboost import CatBoostClassifier, Pool, cv
from sklearn.metrics import accuracy_score

# From X_train extract the categorical features
categorical_features_indices = np.where(X_train.dtypes != np.float32)[0]
itera = 1000000
learning_r = 0.005
model = CatBoostClassifier(
    # cat_features=categorical_features_indices,        
    iterations=itera,
    learning_rate=learning_r,
    random_seed=42,
    loss_function='CrossEntropy',
    task_type='GPU' # uncomment if you want to use GPU
)

model.fit(
    X_train, y_train,
    cat_features=categorical_features_indices,
    eval_set = (X_validation, y_validation),
    logging_level='Verbose',  # you can uncomment this for text output
    metric_period = 100,
    early_stopping_rounds = 300,
    plot = True
    # save_snapshot = True,
    # snapshot_file = "snapshot_BDT",
)

# Save the model
model.save_model(model_path+"BDT")

# Save the model details, include the importance of inputs
txtf = open(model_path+"BDT_txt.txt", 'w')
data = ["iputs=",str(inputs),"traing size for tt,tttt :%s,%s \n number of tt,tttt:%d,%d"%(str(train_rate),str(0.5),tr_tt.shape[0],tr_tttt.shape[0]),
        "iteration:%d"%itera,"learning rate:%f"%learning_r,"Best loss & iteration:",str(model.get_best_score()),str(model.get_best_iteration())]
import_pd = model.get_feature_importance(prettified=True)

for i in range(len(data)):
  txtf.write(data[i])

for i in range(import_pd.shape[0]):
   txtf.write("\n" + import_pd.loc[i][0]+","+str(import_pd.loc[i][1]))
txtf.close()

# Draw and Save the Disc histogram and the disc dist and ROC for 4top vs TT, 4top vs QCD
yout = model.predict_proba(X_validation)
Draw_disc(yout[y_validation==1,1],yout[y_validation==0,1],save="BDT")
Draw_ROC(y_validation,yout[:,1:2],z_te,save="BDT_ROC_TT")

weight_for_hist,weight_for_roc = load_qcd_weight()
yout2 = model.predict_proba(x_val_qcd)
Draw_disc(yout2[y_val_qcd==1,1],yout2[y_val_qcd==0,1],Weights=weight_for_hist,save="BDT_QCD")
Draw_ROC(y_val_qcd,yout2[:,1:2],weight_for_roc,save="BDT_ROC_QCD")

# Draw ROC for QCD+TT
weight_for_hist,weight_for_roc = load_weight()
yout2 = model.predict_proba(x_val_t2_qcd)
Draw_ROC(y_val_t2_qcd,yout2[:,1:2],weight_for_roc,save="ROC_QCD+TT")
