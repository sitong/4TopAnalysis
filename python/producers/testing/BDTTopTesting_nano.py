#for restop mva, deepresolved testing
#returns info BY TOP CANDIDATE
import os
import numpy as np
import ROOT
import math
from pprint import pprint
import itertools
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
# import PhysicsTools.NanoTrees.helpers.LeptonHelper as lepsel
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
# import PhysicsTools.NanoTrees.helpers.GenPartHelper as GenHelp

from collections import OrderedDict
from PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

from PhysicsTools.NanoTrees.helpers.tmvahelper import TMVAHelper
from PhysicsTools.NanoTrees.helpers.ResMVAHelper import ResMVAHelper
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper

import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')

#class definition
class BDTTopTesting_nano(Module):
    def __init__(self):

#working points and constants:
        # self.cleanJvLs       = False
        self.verbose         = False
        # self.isMC = bool(inputTree .GetBranch('genWeight'))
        self.deepbs          = True
        self.bwp = 0.3039
        self.cleanAK4overlap = 0.8 #how much res, bst and ws can overlap
        self.etaMax          = 2.4
        self.bJetEtaMax      = 2.4
        self.minJet_pt       = 35.0
        self.maxJet_eta      = 2.4
        # self.csvcut          = 0.8484 #0.5426 #From TreeReader/Defaults.h
        self.TopCand_minpt   = 100.0
        self.minjets         = 3 #minimum number of jets for a topcand
        self.Bimax           = 4 #number of top scoring csv jets to consider
        self.topmassrange    = 80
        self.wmassrange      = 40
        self.topmassparam    = 175.0 #really 172
        self.wmassparam      = 80.0
        self.bdt_file = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/python/macros/ResTopRetrain/xgboost-NANO_deepbase.xml')
        # self.bdt_file = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/ResMVAweights/xgboostNANO.xml')
        self.bdt_vars = [
            'var_b_mass', #static                       
            'var_topcand_mass',
            'var_topcand_ptDR',
            'var_wcand_mass',
            'var_wcand_ptDR',
            'var_b_j2_mass',
            'var_b_j3_mass',
            'var_sd_n2', 
            'var_b_btagDeepFlavB',#nanomod version               
            'var_j2_btagDeepFlavB',                              
            'var_j3_btagDeepFlavB',                              
            'var_j2_qgl',                                        
            'var_j2_btagDeepFlavC',                              
            'var_j3_qgl',                                        
            'var_j3_btagDeepFlavC',                              
            'var_j2_nConstituents', #nanomod version             
            'var_j3_nConstituents'                               
            ]

    def beginJob(self):
        # self.xgb = XGBHelper(self.bdt_file, self.bdt_vars)
        self.tmva = TMVAHelper(self.bdt_file, self.bdt_vars)
        self.tmva.addVariables()
        self.tchelp = ResMVAHelper()
        self.genhelp = genparthelper()
        # self.jh = JetHelper(self.deepbs)
        pass

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        #gen info
        self.out.branch("newevent", "I")
        self.out.branch("restop_genmatch", "I")
        self.out.branch("resMVAtop_disc", "F") #these discs are for viable candidates
        self.out.branch("NoCandFlag", "I")
        self.out.branch("NgoodtagRes", "I")
        self.out.branch("NmistagRes", "I")
        self.out.branch("nrestops", "I")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
##selections
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    # check that jets within resolved top cands don't overlap with each other
    def checkResJetOverlap(self, topcand, DeepRes=False):
        if not DeepRes:
            bj=topcand[1]; j2=topcand[2]; j3=topcand[3]
        elif DeepRes:
            bj=topcand.j1Idx; j2=topcand.j2Idx; j3=topcand.j3Idx
        if (bj==j2 or bj==j3 or j2==bj or j2==j3 or j3==bj or j3==j2):
            #print 'overlapping!!'
            return False #IS overlapping
        else:
            return True #not overlapping

    def checkResCandOverlap(self, Selcands, discs, DeepRes=False):
        cleancands = []; cleanjets = []
        #order selcands from best (highest) to worst disc values:
        discis  = np.argsort(discs)[::-1]
        selcands = [Selcands[i] for i in discis]
        orderdiscs = np.sort(discs)[::-1]

        for i1, c1 in enumerate(selcands):
            if not DeepRes: #use topcands [tvec, j1, j2, j3] if mva top and jet indices if deepres:
                c1j1=c1[1]; c1j2=c1[2]; c1j3=c1[3]
                C1 = c1
            elif DeepRes:
                c1j1=c1.j1Idx; c1j2=c1.j2Idx; c1j3=c1.j3Idx
                C1 = [c1.j1Idx, c1.j2Idx, c1.j3Idx]
            if i1==0:
                if not DeepRes: c1[4]=orderdiscs[i1] #add disc to array if considered a candidate
                cleancands.append(c1) #keep first/best candidate
                cleanjets.extend((c1j1,c1j2,c1j3))

            for i2, c2  in enumerate(selcands):
                if not DeepRes: #use topcands [tvec, j1, j2, j3] if mva top and jet indices if deepres:
                    c2j1=c2[1]; c2j2=c2[2]; c2j3=c2[3]
                elif DeepRes:
                    c2j1=c2.j1Idx; c2j2=c2.j2Idx; c2j3=c2.j3Idx

                if i2>i1:
                    #check for full overlap:
                    if (c2j1 in C1 and c2j2 in C1 and c2j3 in C1):
                        if self.verbose: print 'same jets!'
                    else:
                        if ((c2j1 not in C1) and (c2j2 not in C1) and (c2j3 not in C1)):
                            if ((c2j1 not in cleanjets) and (c2j2 not in cleanjets) and (c2j3 not in cleanjets)):
                                if not DeepRes: c2[4]=orderdiscs[i2]
                                cleancands.append(c2) #c2 not overlapping!
                                cleanjets.extend((c2j1,c2j2,c2j3))
                            else: 
                                if self.verbose: print 'overlaps with cleanjets!'
                        else:
                            if self.verbose: print 'overlapping jets!'
        return np.array(cleancands)
        
    # discriminant threshold for resolved mva
    def SelMVAResTops(self, top4vec, disc):
        if(top4vec.Eta()>self.etaMax):
            return False
        if(top4vec.Pt()<self.TopCand_minpt):
            return False
        return True #pass

##getters & fillers
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    # takes collection of ak4jets and return a collection of resolved top candidates
    def _getrescands(self, event, lepcleanjets):
        havecands = False; topcands = []; bestcsv = []
        if len(lepcleanjets)<self.minjets: #require 3 jets for topcand
            return havecands, None
        else:
            for jet in lepcleanjets:
                if not self.deepbs: csv = jet.btagCSVV2
                elif   self.deepbs: csv = jet.btagDeepFlavB
                bestcsv.append(csv)
        bestcsv = np.array(bestcsv)
        csvinds = np.argsort(bestcsv)[::-1]
        orderjets = [lepcleanjets[i] for i in csvinds] #order jets in descending csv score order

        nbcanjets=self.Bimax
        if len(orderjets)<self.Bimax: #make sure not considering more bcands than there are jets
            #print 'numjets', len(orderjets), 'n bjet cands', len(orderjets[:nbcanjets])
            nbcanjets=len(orderjets)

        if self.verbose: print 'numjets', len(orderjets), 'n bjet cands', len(orderjets[:nbcanjets])
        #get the cands. NO CSV CUT
        for Bi, bcand in enumerate(orderjets[:nbcanjets]): #iterate over first 3 (or whatever) elements in list
            #print bcand.btagCSVV2
            for j2i, j2 in enumerate(orderjets[1:]):
                if j2i!=Bi:
                    for j3i, j3 in enumerate(orderjets[2:]):
                        if (j3i!=Bi and j3i!=j2i): #no overlapping jets
                            tcand = self.tchelp.getTopCand(bcand, j2, j3)
                            wcand = self.tchelp.getWCand(j2, j3)
                            passmass = self.tchelp.PassMass(tcand[0], wcand[0], self.topmassrange, self.wmassrange, self.topmassparam, self.wmassparam)
                            if passmass:
                                havecands = True  #true if there are ANY tops in the event
                                # print 'topcand----->', Bi, bcand, j2i, j2, j3i, j3
                                topcands.append(tcand)
        if havecands:
            return havecands, np.array(topcands)
        elif not havecands:
            return havecands, None #empty


    def _cleanJets(self, jet, minJet_pt=20.0, maxJet_eta=2.4, usejetid=False):
        if (jet.pt<minJet_pt or abs(jet.eta)>=maxJet_eta):
            return False
        if usejetid:
            if (jet.jetId==1): # loose
                return False
        return True

    # fills variables needed to calculate bdt discriminant for restop mva
    def _fillBDTvars(self, havecands, topcand): #should only calculate bdt if >1 topcand
        if havecands: wcand = self.tchelp.getWCand(topcand[2], topcand[3])
        bcsv = 0.; j2csv = 0.; j3csv = 0.
        if not self.deepbs and havecands:
            bcsv = topcand[1].btagCSVV2
            j2csv = topcand[2].btagCSVV2
            j3csv = topcand[3].btagCSVV2
        elif self.deepbs and havecands:
            bcsv = topcand[1].btagDeepFlavB
            j2csv = topcand[2].btagDeepFlavB
            j3csv = topcand[3].btagDeepFlavB

        #calc BDT vars
        wcand_deltaR=0; b_wcand_deltatR = 0; sd_0 = 0; var_sd_n2 =0
        if havecands:
            wcand_deltaR = phys.deltaR(wcand[1].eta, wcand[1].phi, wcand[2].eta, wcand[2].phi) #deltaR(j2, j3)
            b_wcand_deltaR = phys.deltaR(topcand[1].eta, topcand[1].phi, wcand[0].Eta(), wcand[0].Phi()) #deltaR(b, wcand)
            sd_0 = topcand[3].pt/(topcand[2].pt+topcand[3].pt) #j3pt/j2pt+j3pt
            if wcand_deltaR != 0:
                var_sd_n2 = sd_0/((wcand_deltaR)**-2)

        # create dictionary
        empty_tcdict = OrderedDict( [("var_b_mass",  0),
                                     ("var_topcand_mass",0),
                                     ("var_topcand_ptDR",0),
                                     ("var_wcand_mass",  0), 
                                     ("var_wcand_ptDR",  0),
                                     ("var_b_j2_mass",   0),
                                     ("var_b_j3_mass",   0),
                                     ("var_sd_n2",       0),
                                     ("var_b_btagDeepFlavB",  -1), 
                                     ("var_j2_btagDeepFlavB", -1), 
                                     ("var_j3_btagDeepFlavB",  -1),
                                     ("var_j2_qgl",-1), 
                                     ("var_j2_btagDeepFlavC",-1),
                                     ("var_j3_qgl", -1),
                                     ("var_j3_btagDeepFlavC", -1),
                                     ("var_j2_nConstituents",     0),
                                     ("var_j3_nConstituents",     0) ])

        if not havecands: full_tcdict = empty_tcdict
        elif havecands:
                #try to avoid NaNs
            bbtag=-1.0; j2btag=-1.0; j3btag=-1.0
            j2ctag=-1.0; j3ctag=-1.0
            j2qgl=-1.0; j3qgl=-1.0
            if not math.isnan( topcand[1].btagDeepFlavB):
                bbtag = topcand[1].btagDeepFlavB
            if not math.isnan( topcand[2].btagDeepFlavB):
                j2btag = topcand[2].btagDeepFlavB
            if not math.isnan( topcand[3].btagDeepFlavB):
                j3btag = topcand[3].btagDeepFlavB
            if not math.isnan( topcand[2].btagDeepFlavC):
                j2ctag = topcand[2].btagDeepFlavC
            if not math.isnan( topcand[3].btagDeepFlavC):
                j3ctag = topcand[3].btagDeepFlavC
            if not math.isnan( topcand[2].qgl):
                j2qgl = topcand[2].qgl
            if not math.isnan( topcand[3].qgl):
                j3qgl = topcand[3].qgl
            full_tcdict = OrderedDict([
                    ("var_b_mass",  topcand[1].mass),
                    ("var_topcand_mass", topcand[0].M()),
                    ("var_topcand_ptDR", topcand[0].Pt()*b_wcand_deltaR ),#fixed, *b_wcand_deltaR
                    ("var_wcand_mass",   wcand[0].M()),
                    ("var_wcand_ptDR",   wcand[0].Pt()*wcand_deltaR),#fixed, *wcandpt
                    ("var_b_j2_mass",    (self.tchelp.p4(topcand[1]) + self.tchelp.p4(topcand[2]) ).M() ),
                    ("var_b_j3_mass",    (self.tchelp.p4(topcand[1]) + self.tchelp.p4(topcand[3]) ).M() ),
                    ("var_sd_n2",        var_sd_n2),
                    ("var_b_btagDeepFlavB",  bbtag),
                    ("var_j2_btagDeepFlavB", j2btag),
                    ("var_j3_btagDeepFlavB", j3btag),
                    ("var_j2_qgl"          , j2qgl),
                    ("var_j2_btagDeepFlavC", j2ctag),
                    ("var_j3_qgl"          , j3qgl),
                    ("var_j3_btagDeepFlavC", j3ctag),
                    ("var_j2_nConstituents",   int(topcand[2].nConstituents)),#corrected
                    ("var_j3_nConstituents",   int(topcand[3].nConstituents)) ])

        # for key,val in full_tcdict.iteritems():
        #     if math.isnan(val) :
        #         print key, val
        #print full_tcdict
        #fill and evalutate mva:
        if havecands:
            self.tmva.setValue(full_tcdict)
            disc =self.tmva.eval()
        elif not havecands: disc =-1

        #Find good top candidates        
        goodcand = False
        if havecands:
            iscand = self.SelMVAResTops(topcand[0], disc)
            isoverlap = self.checkResJetOverlap(topcand)
            if (iscand and isoverlap): #if passes selection and no overlap
                goodcand = True
        #be sure will fill vars if goodcand, else fill zero:
        if not goodcand:
            full_tcdict = empty_tcdict 
        return goodcand, topcand, disc, full_tcdict
            
    # def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
    #     pass

    # put it all together!
    def analyze(self, event):
        # e = int(event.event)
        # print 'EVENT #', e, '\n'

        #define+clean jets:
        # leptons    = lepsel.SelectLeptons(event, conc=True) #define leptons
        ak4jets    = Collection(event,"Jet")
 
        #add indices to ak4jets:
        ak4jets = self.genhelp.AddJetIdx(ak4jets)
        cleanak4 = []
        for j in ak4jets: 
           if self._cleanJets(j, self.minJet_pt, self.etaMax, True):
            # if j.pt>=self.minJet_pt and abs(j.eta<self.etaMax) and j.jetId>1:  
                cleanak4.append(j)

        # print self.minJet_pt, self.etaMax, True
         # print cleanak4
        # cleanak4 = [j for j in ak4jets if self.jh._cleanJets(j)]
        # if self.cleanJvLs: #clean jets v leptons
        #     cleanak4 = self.jh.cleanJetsvLeps(cleanak4, leptons, jetrad=0.4)
        # cleanak4 = np.array(cleanak4)
                
        #get MVA resolved top candidates 
        ResTops = []; discs = []#; rtdicts = []
        HaveCands, TopCands  = self._getrescands(event, cleanak4)
        # print HaveCands, TopCands
        if not HaveCands:
            self._fillBDTvars(HaveCands, None)
        elif HaveCands:
            for tc in TopCands: #looping over topcands
                isgood, GoodCand, disc, candDict = self._fillBDTvars(HaveCands,tc)
                # print 'disc:', disc
                if isgood:
                    ResTops.append(GoodCand); discs.append(disc)#; rtdicts.append(candDict)

        nocand = 0
        #check that resolved tops are distinct!:
        ResTops = np.array(ResTops); discs = np.array(discs)
        if len(ResTops)>0:
            ResTops = self.checkResCandOverlap(ResTops, discs)
        else: nocand = 1

        # check gen matching of tops
        NgoodtagRes = 0; NmistagRes = 0
        matcharr = np.zeros(len(ResTops), dtype=int)
        for i, (tc, m) in enumerate(zip(ResTops, matcharr)):
            matched = self.genhelp.GenMatchTops(event, tc, True)
            if matched:
                NgoodtagRes+=1
                matcharr[i]=1 #reassign 0 to 1
            else: NmistagRes+=1

        #fill variables
        for i, (tc, m) in enumerate(zip(ResTops, matcharr)):
            firstcand=0
            if i==0:
                firstcand=1
            # print 'nrestops', len(ResTops), 'restop_genmatch', m
            # print 'discs', tc[4]
            self.out.fillBranch('newevent',        firstcand)
            self.out.fillBranch('restop_genmatch', m)
            self.out.fillBranch('resMVAtop_disc',  tc[4])
            self.out.fillBranch("nrestops",        len(ResTops))
            self.out.fillBranch("NgoodtagRes",     NgoodtagRes)
            self.out.fillBranch("NmistagRes",      NmistagRes)
            self.out.fillBranch("NoCandFlag",      nocand)
            self.out.fill()
        return False

TopTestNano = lambda : BDTTopTesting_nano()
