#module for general event level variables
#NOTE: need to add corrections still
import os
import numpy as np
import ROOT
import sys
import math
import itertools
from operator import mul
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
from  PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoTrees.helpers.ConfigHelper import LepSetter, JetSetter

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')


#class definition
class BasicVarProducer(Module):
    def __init__(self, channel, year):
        self.channel = channel
        self.year    = year

    def beginJob(self):
        self.LS = LepSetter(self.channel)
        self.JS = JetSetter(self.channel, self.year)
        self.jh = JetHelper(self.JS.btype, self.JS.bWP)
        pass

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        print 'infile', inputFile
        self.isMC = bool(inputTree.GetBranch('genWeight'))
        self.infile = inputFile
        self.intree = inputTree
        #basic vars:
        self.out = wrappedOutputTree
        self.out.branch("evt", "I")
        self.out.branch("run", "I")
        self.out.branch("ismc", "I")
        self.out.branch("lumi", "I")
        self.out.branch("metfilterflag", "I")
        self.out.branch("npv", "I")
        self.out.branch("met", "F")
        #basic lepton vars
        self.out.branch("nleptons", "I") #
        self.out.branch("lepton1pt", "F") #
        # weights, sfs, and gen stuff
        self.out.branch("weight", "F") #
        self.out.branch("genweight", "F") #
        self.out.branch("gen_njets",   "I") #
        self.out.branch("genjet5flag", "I") #
        #jet vars
        self.out.branch("njets", "I")
        self.out.branch("metovsqrtht", "F")
        self.out.branch("ht", "F")
        self.out.branch("nbjets", "I")
        self.out.branch("bjht", "F")
        self.out.branch("sfjm", "F")
        self.out.branch("j1pt", "F")
        self.out.branch("j2pt", "F")
        self.out.branch("j1eta", "F")
        self.out.branch("j2eta", "F")
        self.out.branch("dphij1j2", "F")
        self.out.branch("dphib1b2", "F")
        self.out.branch("detaj1j2", "F")
        self.out.branch("detab1b2", "F")
        self.out.branch("dphij1met", "F")
        self.out.branch("dphij2met", "F")
        self.out.branch("dphij3met", "F")
        self.out.branch("dphij4met", "F")
        # #qgl vars
        self.out.branch("qglsum", "F")
        self.out.branch("qglprod", "F")
        self.out.branch("qglsum30", "F")

        #by-jet variables not included:
        self.out.branch("jetpt", "F")
        self.out.branch("jetCvsL", "F")
        self.out.branch("jeteta", "F")
        self.out.branch("jetphi", "F")
        self.out.branch("jetID", "I")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
## getters & fillers
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    def _get_filler(self, obj):
        def filler(branch, value, default=0):
            self.out.fillBranch(branch, value if obj else default)
        return filler

    def _fillEventVars(self, event):

        #data or mc
        ismc = int(self.isMC)
        genweight = 0.0; gen_njets = 0; gj5bool = False

        if ismc:
            genjets   = Collection(event,"GenJet")
            genweight = event.genWeight
            gen_njets = event.nGenJet
            #variables for genjet5:
            ngoodgjs = 0
            if event.nGenJet >=5:
                for gj in genjets:
                    if (gj.pt>20 and abs(gj.eta)<2.4):
                        ngoodgjs+=1
            if ngoodgjs>=5:
                gj5bool = True

        #weight:
        #NOTE: only works if running on single, complete file! Otherwise have to add the weights later or use known weight
        XWeight = xw.GetKnownWeight(self.infile, self.year, self.isMC) #xsec weight from filename

        #collections needed
        event.met     = Object(event,"MET")#Object not collection?
        event.flags   = Object(event,"Flag")

        #fill branches
        self.out.fillBranch("evt",           long(event.event))
        self.out.fillBranch("metfilterflag", event.flags.METFilters)
        self.out.fillBranch("run",           long(event.run))
        self.out.fillBranch("ismc",          ismc)
        self.out.fillBranch("lumi",          event.luminosityBlock)
        self.out.fillBranch("npv",           event.PV_npvs)
        self.out.fillBranch("met",           event.met.pt) 
        self.out.fillBranch("weight",        XWeight)
        self.out.fillBranch("genweight",     genweight)
        self.out.fillBranch("gen_njets",     gen_njets)
        self.out.fillBranch("genjet5flag",   int(gj5bool))
        
    def _fillJetVars(self, event):
        #collections
        ak4jets = Collection(event,"Jet") 
        fatjets = Collection(event,"FatJet")
        if self.channel=='0lep':
            els, mus, leptons = self.LS.getLeps(event)
        elif self.channel=='1lep':
            ak4jets, els, mus, leptons = self.LS.getLeps(event)

        #a few lepton things
        nleptons = len(leptons)
        leppt = 0.0
        if nleptons > 0:
            leppt = leptons[0].pt

        # clean jets:
        cleanjets    = [j for j in ak4jets if self.jh.cleanJets(j, self.JS.jpt, self.JS.jeta, self.JS.usejid)]
        cleanfatjets = [j for j in fatjets if self.jh.cleanJets(j, self.JS.jpt, self.JS.jeta, self.JS.usejid)]

        # bjets
        BJets = [j for j in cleanjets if self.jh.cleanBJets(j, self.JS.bpt, self.JS.beta)]

        #HT stuff
        calcht = phys.HT(cleanjets, self.JS.jpt, self.JS.jeta) #can also do lep cleaned jets
        mosht = 0.0
        if calcht!=0.0:
            mosht =  (event.met.pt/math.sqrt(calcht))
        bjht = phys.HT(BJets,  self.JS.jpt, self.JS.jeta)

        #some more jet defs
        sumfatjetmass = 0.0
        for fj in cleanfatjets:
            sumfatjetmass += fj.mass
        nbjets = len(BJets)  
        njets = len(cleanjets)

        #initializations:
        leadJets  = cleanjets[:4]    #empty collection of 4 lead jets
        leadBJets = BJets[:2]        #up to 2 lead bjets
        are2bjets=True; are4jets=True; are3jets=True; are2jets=True; are1jets=True
        if nbjets<2: are2bjets=False
        if njets<4:  are4jets=False
        if njets<3:  are3jets=False
        if njets<2:  are2jets=False
        if njets<1:  are1jets=False 

        qglsum=0.0; qglprod=0.0; qglsum30=0.0 
        j1pt = 0.0; j1eta = 0.0; j2pt = 0.0; j2eta = 0.0
        dphij1j2 = 0.0; dphib1b2 = 0.0; detaj1j2 = 0.0; detab1b2 = 0.0
        dphij1met = 0.0; dphij2met = 0.0; dphij3met = 0.0; dphij4met = 0.0

        #calculate
        for jet in cleanjets:
            qglsum  += jet.qgl
            qglprod *= jet.qgl
            if jet.pt >= 30.0:
                qglsum30 += jet.qgl
        if are1jets:
            j1pt =  leadJets[0].pt
            j1eta =  leadJets[0].eta
        if are2jets:
            j2pt =  leadJets[1].pt
            j2eta =  leadJets[1].eta
            dphij1j2 =  abs( ROOT.TVector2.Phi_mpi_pi(leadJets[0].phi-leadJets[1].phi))
            detaj1j2 =  abs( leadJets[0].eta-leadJets[1].eta)
            dphij1met = abs( ROOT.TVector2.Phi_mpi_pi(leadJets[0].phi-event.met.phi))
            dphij2met = abs( ROOT.TVector2.Phi_mpi_pi(leadJets[1].phi-event.met.phi))
        if are3jets: 
            dphij3met = abs( ROOT.TVector2.Phi_mpi_pi(leadJets[2].phi-event.met.phi))
        if are4jets:
            dphij4met = abs( ROOT.TVector2.Phi_mpi_pi(leadJets[3].phi-event.met.phi))
        if are2bjets:
            dphib1b2 = abs( ROOT.TVector2.Phi_mpi_pi(leadBJets[0].phi-leadBJets[1].phi))
            detab1b2 = abs(leadBJets[0].eta-leadBJets[1].eta) 

        # fill branches
        self.out.fillBranch("j1pt",        j1eta)
        self.out.fillBranch("j1eta",       j1pt)
        self.out.fillBranch("j2pt",        j2pt)
        self.out.fillBranch("j2eta",       j2eta)
        self.out.fillBranch("dphij1j2",    dphij1j2)
        self.out.fillBranch("detaj1j2",    detaj1j2)
        self.out.fillBranch("dphij1met",   dphij1met)
        self.out.fillBranch("dphij2met",   dphij2met)
        self.out.fillBranch("dphij3met",   dphij3met)
        self.out.fillBranch("dphij4met",   dphij4met)
        self.out.fillBranch("dphib1b2",    dphib1b2)
        self.out.fillBranch("detab1b2",    detab1b2)
        self.out.fillBranch("njets",       njets)
        self.out.fillBranch("sfjm",        sumfatjetmass)
        self.out.fillBranch("nbjets",      nbjets)
        self.out.fillBranch("bjht",        bjht )
        self.out.fillBranch("qglsum",      qglsum)
        self.out.fillBranch("qglsum30",    qglsum30)
        self.out.fillBranch("qglprod",     qglprod)
        self.out.fillBranch("ht",          calcht )
        self.out.fillBranch("metovsqrtht", mosht)
        self.out.fillBranch("lepton1pt",     leppt)
        self.out.fillBranch("nleptons",      nleptons)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
    #     pass

    def analyze(self, event):
        self._fillEventVars(event)
        self._fillJetVars(event)
        return True

print "BASIC VAR PRODUCER"
# BasicTrees = lambda : BasicVarProducer()
Basic2016_0lep = lambda : BasicVarProducer('0lep','2016')
Basic2016_1lep = lambda : BasicVarProducer('1lep','2016')
