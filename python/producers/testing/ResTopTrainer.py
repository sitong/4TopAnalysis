# produces flat ntuples for resolved top tagger bdt training
#returns info BY TOP CANDIDATE
import numpy as np
from PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys

from PhysicsTools.NanoTrees.helpers.ConfigHelper import ResSetter, LepSetter, JetSetter, BoostSetter, bdtSetter
from PhysicsTools.NanoTrees.helpers.tmvahelper import TMVAHelper
from PhysicsTools.NanoTrees.helpers.ResTopHelper import ResTopHelper
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper

# import logging
# logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')

#class definition
class ResTopTrainer(Module):
    def __init__(self, year):        
        self.channel = '0lep'
        self.year = year
        self.verbose   = False
        # self.btype = 'deepflav'
        # self.bwpname = 'M'
        # self.tmva = None #placeholder for helper initialization
        # print 'Training module for NanoAODv5 resolved tops. Note: should be run as --friend module'

        #make sure no disc used:
        self.useTopWP=False

    def beginJob(self):
        self.genhelp = genparthelper()
        self.LS = LepSetter(self.channel)
        self.JS = JetSetter(self.channel, self.year)
        self.jh = JetHelper(self.JS.btype, self.JS.bWP)
        self.th = ResTopHelper(self.JS.btype)
        pass

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.isMC = bool(inputTree .GetBranch('genWeight'))
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        #gen info
        self.out.branch("flag_signal", "O")
        self.out.branch("NoCandFlag", "I")
        self.out.branch("NgoodtagRes", "I")
        self.out.branch("NmistagRes", "I")
        self.out.branch("nrestops",   "I")
        self.out.branch("evt",     "I")
        self.out.branch("npv",       "I")
        self.out.branch('njets',     "I")
        self.out.branch('ndeepbjets',    "I")
        self.out.branch('nldeepbjets',    "I")
        self.out.branch('met',    "F")
        self.out.branch('topcand_pt',    "F")
        self.out.branch('weight',        "F")
        self.out.branch("genweight",     "F")
        self.out.branch("gen_njets",     "I")
        self.out.branch("genjet5flag",   "I")
        self.out.branch("var_b_mass",     "F")
        self.out.branch("var_topcand_mass",    "F")
        self.out.branch("var_topcand_ptDR",    "F")
        self.out.branch("var_wcand_mass",      "F")
        self.out.branch("var_wcand_ptDR",      "F")
        self.out.branch("var_b_j2_mass",       "F")
        self.out.branch("var_b_j3_mass",       "F")
        self.out.branch("var_sd_n2",           "F")
        self.out.branch("var_b_btagDeepFlavB",     "F")
        self.out.branch("var_j2_btagDeepFlavB",     "F")
        self.out.branch("var_j3_btagDeepFlavB",     "F")
        self.out.branch("var_j2_btagDeepFlavC",     "F")
        self.out.branch("var_j2_qgl", "F")
        self.out.branch("var_j3_btagDeepFlavC",     "F")
        self.out.branch("var_j3_qgl", "F")
        self.out.branch("var_j2_nConstituents", "I")
        self.out.branch("var_j3_nConstituents", "I")

    # fills variables needed to calculate bdt discriminant for restop mva
    def _checkCand(self, havecands, topcand): #should only calculate bdt if >1 topcand
        candPT = topcand[0].Pt()

        #Find good top candidates        
        goodcand = False
        if havecands:
            iscand = self.th.SelMVAResTops(topcand[0])
            isoverlap = self.th.checkResJetOverlap(topcand)
            if (iscand and isoverlap): #if passes selection and no overlap
                goodcand = True
        #be sure will fill vars if goodcand, else fill zero:
        return goodcand, topcand, candPT
            
    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # put it all together!
    def analyze(self, event):
        #collections
        genjets       = Collection(event,"GenJet")
        event.met     = Object(event,"MET")
        ak4jets    = Collection(event,"Jet")

        e = int(event.event)
        if self.verbose: print 'EVENT #', e, '\n'

        #cross sectional weights
        XWeight = xw.GetKnownWeight(self.infile, self.intree, self.isMC) #xsec weight from filename
        if event.genWeight < 0: # get the right sign
            XWeight = -XWeight

        #variables for genjet5:
        gj5bool = False
        ngoodgjs = 0
        if event.nGenJet >=5:
            for gj in genjets:
                if (gj.pt>20 and abs(gj.eta<2.4)):
                    ngoodgjs+=1
        if ngoodgjs>=5:
            gj5bool = True

        #define+clean jets, add indices to ak4jets:
        ak4jets = self.genhelp.AddJetIdx(ak4jets)
        cleanak4 = [j for j in ak4jets if self.jh._cleanJets(j, self.JS.jpt, self.JS.jeta, self.JS.usejid)]

        #bjets
        DeepBJets = [j for j in cleanak4 if self.jh._cleanBJets(j, self.JS.bpt, self.JS.beta)]
        LDeepBJets = [j for j in cleanak4 if self.jh._cleanBJets(j, self.JS.bpt, self.JS.beta)]
        nbjets = len(DeepBJets) 
        nlbjets = len(LDeepBJets)
        njets = len(cleanak4)

        #get MVA resolved top candidates 
        ResTops = []; candPTs = []#; rtdicts = []
        HaveCands, TopCands  = self.th.getrescands(cleanak4)
        if HaveCands:
            for tc in TopCands: #looping over topcands
                isgood, GoodCand, candPT = self._checkCand(HaveCands,tc)
                if isgood:
                    ResTops.append(GoodCand); candPTs.append(candPT)

        nocand = 0
        #check that resolved tops are distinct!:
        ResTops = np.array(ResTops); candPTs = np.array(candPTs)
        if len(ResTops)>0:
            ResTops = self.th.checkResCandOverlap(ResTops, candPTs)
        else: nocand = 1

        # check gen matching of tops
        NgoodtagRes = 0; NmistagRes = 0
        matcharr = np.zeros(len(ResTops), dtype=int)
        for i, (tc, m) in enumerate(zip(ResTops, matcharr)):
            matched = self.genhelp.GenMatchTops(event, tc, True)
            if matched:
                NgoodtagRes+=1
                matcharr[i]=1 #reassign 0 to 1: candidate is a gen-matched top
            else: NmistagRes+=1

        #fill variables
        bcsv = 0.; j2csv = 0.; j3csv = 0.; wcand_deltaR=0; b_wcand_deltatR = 0; sd_0 = 0; var_sd_n2 =0
        for tc, m in zip(ResTops, matcharr):
            if HaveCands:
                wcand = self.th.getWCand(tc[2], tc[3])
                bcsv = tc[1].btagDeepFlavB
                j2csv = tc[2].btagDeepFlavB
                j3csv = tc[3].btagDeepFlavB

                wcand_deltaR = phys.deltaR(wcand[1].eta, wcand[1].phi, wcand[2].eta, wcand[2].phi) #deltaR(j2, j3)
                b_wcand_deltaR = phys.deltaR(tc[1].eta, tc[1].phi, wcand[0].Eta(), wcand[0].Phi()) #deltaR(b, wcand)
                sd_0 = tc[3].pt/(tc[2].pt+tc[3].pt) #j3pt/j2pt+j3pt
                if wcand_deltaR != 0:
                    var_sd_n2 = sd_0/((wcand_deltaR)**-2)

            self.out.fillBranch('flag_signal',           m)
            self.out.fillBranch("evt",                   int(event.event))
            self.out.fillBranch("npv",                   event.PV_npvs)
            self.out.fillBranch("met",                   event.met.pt) 
            self.out.fillBranch('njets',                 njets)
            self.out.fillBranch('ndeepbjets',            nbjets)
            self.out.fillBranch('nldeepbjets',           nlbjets)
            self.out.fillBranch('topcand_pt',            tc[0].Pt() )
            self.out.fillBranch('weight',                XWeight) 
            self.out.fillBranch('genweight',             event.genWeight)
            self.out.fillBranch("gen_njets",             event.nGenJet)
            self.out.fillBranch("genjet5flag",           int(gj5bool))
            self.out.fillBranch("var_b_mass",            tc[1].mass)
            self.out.fillBranch("var_b_btagDeepFlavB",   tc[1].btagDeepFlavB)
            self.out.fillBranch("var_j2_btagDeepFlavB",  tc[2].btagDeepFlavB)
            self.out.fillBranch("var_j3_btagDeepFlavB",  tc[3].btagDeepFlavB)
            self.out.fillBranch("var_j2_btagDeepFlavC",  tc[2].btagDeepFlavC)
            self.out.fillBranch("var_j3_btagDeepFlavC",  tc[3].btagDeepFlavC)
            self.out.fillBranch("var_topcand_mass",      tc[0].M())
            self.out.fillBranch("var_topcand_ptDR",      tc[0].Pt()*b_wcand_deltaR )
            self.out.fillBranch("var_wcand_mass",        wcand[0].M())
            self.out.fillBranch("var_wcand_ptDR",        wcand[0].Pt()*wcand_deltaR)
            self.out.fillBranch("var_b_j2_mass",         (self.th.p4(tc[1]) + self.th.p4(tc[2]) ).M() )
            self.out.fillBranch("var_b_j3_mass",         (self.th.p4(tc[1]) + self.th.p4(tc[3]) ).M() )
            self.out.fillBranch("var_sd_n2",             var_sd_n2)
            self.out.fillBranch("var_j2_nConstituents",  tc[2].nConstituents)
            self.out.fillBranch("var_j3_nConstituents",  tc[3].nConstituents)
            self.out.fillBranch("var_j2_qgl",            tc[2].qgl) 
            self.out.fillBranch("var_j3_qgl",            tc[3].qgl) 
            self.out.fillBranch("nrestops",              len(ResTops))
            self.out.fillBranch("NgoodtagRes",           NgoodtagRes)
            self.out.fillBranch("NmistagRes",            NmistagRes)
            self.out.fillBranch("NoCandFlag",            nocand) #no tops found
            self.out.fill()
        return True 

TopTrain2016 = lambda : ResTopTrainer('2016')
