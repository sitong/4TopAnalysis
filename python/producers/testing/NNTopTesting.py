#for restop mva, deepresolved testing
#returns info BY TOP CANDIDATE
import os
import numpy as np
import ROOT
import math
from pprint import pprint
import itertools
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
# import PhysicsTools.NanoTrees.helpers.GenPartHelper as GenHelp

from collections import OrderedDict
from PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

from PhysicsTools.NanoTrees.helpers.ResMVAHelper import ResMVAHelper
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper

import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')

#class definition
class NNTopTesting(Module):
    def __init__(self):

#working points and constants:
        self.verbose         = False
        self.deepbs          = True
        self.cleanAK4overlap = 0.8 #how much res, bst and ws can overlap
        self.etaMax          = 2.4
        self.bJetEtaMax      = 2.4
        self.minJet_pt       = 35.0
        self.maxJet_eta      = 2.4
        # self.csvcut          = 0.8484 #0.5426 #From TreeReader/Defaults.h
        self.TopCand_minpt   = 100.0
        self.minjets         = 4 #minimum number of jets for a topcand
        self.Bimax           = 4 #number of top scoring csv jets to consider
        self.topmassrange    = 80
        self.wmassrange      = 40
        self.topmassparam    = 175.0 #really 172
        self.wmassparam      = 80.0

    def beginJob(self):
        self.tchelp = ResMVAHelper()
        self.genhelp = genparthelper()
        # self.jh = JetHelper(self.deepbs)
        pass

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        #gen info
        self.out.branch("newevent", "I")
        self.out.branch("deeptop_genmatch", "I")
        self.out.branch("resMVAtop_disc", "F")
        self.out.branch("DeepNoCandFlag", "I")
        self.out.branch("NgoodtagDeeps", "I")
        self.out.branch("NmistagDeeps", "I")
        self.out.branch("nrestops", "I")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
##selections
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    # check that jets within resolved top cands don't overlap with each other
    def checkResJetOverlap(self, topcand, DeepRes=False):
        if not DeepRes:
            bj=topcand[1]; j2=topcand[2]; j3=topcand[3]
        elif DeepRes:
            bj=topcand.j1Idx; j2=topcand.j2Idx; j3=topcand.j3Idx
        if (bj==j2 or bj==j3 or j2==bj or j2==j3 or j3==bj or j3==j2):
            #print 'overlapping!!'
            return False #IS overlapping
        else:
            return True #not overlapping

    def checkResCandOverlap(self, Selcands, discs, DeepRes=False):
        cleancands = []; cleanjets = []
        #order selcands from best (highest) to worst disc values:
        discis  = np.argsort(discs)[::-1]
        selcands = [Selcands[i] for i in discis]
        orderdiscs = np.sort(discs)[::-1]

        for i1, c1 in enumerate(selcands):
            if not DeepRes: #use topcands [tvec, j1, j2, j3] if mva top and jet indices if deepres:
                c1j1=c1[1]; c1j2=c1[2]; c1j3=c1[3]
                C1 = c1
            elif DeepRes:
                c1j1=c1.j1Idx; c1j2=c1.j2Idx; c1j3=c1.j3Idx
                C1 = [c1.j1Idx, c1.j2Idx, c1.j3Idx]
            if i1==0:
                if not DeepRes: c1[4]=orderdiscs[i1] #add disc to array if considered a candidate
                cleancands.append(c1) #keep first/best candidate
                cleanjets.extend((c1j1,c1j2,c1j3))

            for i2, c2  in enumerate(selcands):
                if not DeepRes: #use topcands [tvec, j1, j2, j3] if mva top and jet indices if deepres:
                    c2j1=c2[1]; c2j2=c2[2]; c2j3=c2[3]
                elif DeepRes:
                    c2j1=c2.j1Idx; c2j2=c2.j2Idx; c2j3=c2.j3Idx

                if i2>i1:
                    #check for full overlap:
                    if (c2j1 in C1 and c2j2 in C1 and c2j3 in C1):
                        if self.verbose: print 'same jets!'
                    else:
                        if ((c2j1 not in C1) and (c2j2 not in C1) and (c2j3 not in C1)):
                            if ((c2j1 not in cleanjets) and (c2j2 not in cleanjets) and (c2j3 not in cleanjets)):
                                if not DeepRes: c2[4]=orderdiscs[i2]
                                cleancands.append(c2) #c2 not overlapping!
                                cleanjets.extend((c2j1,c2j2,c2j3))
                            else: 
                                if self.verbose: print 'overlaps with cleanjets!'
                        else:
                            if self.verbose: print 'overlapping jets!'
        return np.array(cleancands)
        
    # deepresolved HOT tagger selection
    def SelDeepResolved(self, rescand, njets):
        #have to clean against boosted objs
        if math.fabs(rescand.eta) > self.etaMax:
            return False
        if rescand.pt < self.TopCand_minpt:
            return False
        #analogous requirement: require at least 3 jets
        if njets<self.minjets:
            return False
        return True

##getters & fillers
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    # gets the deep resolved top candidates
    def _getdeeprescands(self, event, lepcleanjets, lepcleanHOTs):
        rescands = []
        for cand in lepcleanHOTs:
            if len(lepcleanjets)>0:
                if self.SelDeepResolved(cand, len(lepcleanjets)):
                    if self.checkResJetOverlap(cand, DeepRes=True):
                        rescands.append(cand)
        return rescands

    def _cleanJets(self, jet, minJet_pt=20.0, maxJet_eta=2.4, usejetid=False):
        if (jet.pt<minJet_pt or abs(jet.eta)>=maxJet_eta):
            return False
        if usejetid:
            if (jet.jetId==1): # loose
                return False
        return True

    # put it all together!
    def analyze(self, event):
        e = int(event.event)
        #print 'EVENT #', e, '\n'

        #define+clean jets:
        # leptons    = lepsel.SelectLeptons(event, conc=True) #define leptons
        HOTcands   = Collection(event,"ResolvedTopCandidate") 
        ak4jets    = Collection(event,"Jet")
 
        #add indices to ak4jets:
        ak4jets = self.genhelp.AddJetIdx(ak4jets)
        cleanak4 = []; cleanHOT = []
        for j in ak4jets: 
           if self._cleanJets(j, self.minJet_pt, self.etaMax, True):
            # if j.pt>=self.minJet_pt and abs(j.eta<self.etaMax) and j.jetId>1:  
                cleanak4.append(j)
        for H in HOTcands: 
           if self._cleanJets(H, self.minJet_pt, self.etaMax, False):
            # if j.pt>=self.minJet_pt and abs(j.eta<self.etaMax) and j.jetId>1:  
                cleanHOT.append(H)

        NNnocand = 0
        #get deep resolved candidates:
        DeepResTops = self._getdeeprescands(event, cleanak4, cleanHOT)
        DeepResTops = np.array(DeepResTops) 
        if len(DeepResTops)>0:
            deepdiscs = [c.discriminator for c in DeepResTops]
            deepdiscs = np.array(deepdiscs)
            DeepResTops = self.checkResCandOverlap(DeepResTops, deepdiscs, DeepRes=True)
        else: NNnocand = 1
        if any(DeepResTops==None): DeepResTops = [] #if np array

        # check gen matching of tops
        NgoodtagDeeps = 0; NmistagDeeps = 0
        dmatcharr = np.zeros(len(DeepResTops), dtype=int)
        for j, (tc, dm) in enumerate(zip(DeepResTops, dmatcharr)):
            matched = self.genhelp.GenMatchTops(event, tc, False)
            if matched:
                NgoodtagDeeps+=1
                dmatcharr[j] = 1
            else: NmistagDeeps+=1

        for i, (tc,dm) in enumerate(zip(DeepResTops, dmatcharr)):
            firstcand=0
            if i==0:
                firstcand=1 
            # print tc.discriminator
            self.out.fillBranch('newevent',         firstcand)
            self.out.fillBranch('deeptop_genmatch', dm)
            self.out.fillBranch('resMVAtop_disc',    tc.discriminator)
            self.out.fillBranch("nrestops",       len(DeepResTops))
            self.out.fillBranch("NgoodtagDeeps",    NgoodtagDeeps)
            self.out.fillBranch("NmistagDeeps",     NmistagDeeps)
            self.out.fillBranch("DeepNoCandFlag",   NNnocand)
            self.out.fill()
        return True #false if friend!

TopTestNNTrees = lambda : NNTopTesting()
