#copies central nanoaodv5 ntuples to eos, making the directories as well
import subprocess
import os
import sys
# import argparse
import math
import numpy as np

infile = './datasets/forBDTtrain.txt'#'nanoaodv5_data2016.txt'
# infile = './datasets/nanoaodv5_MC2016.txt'#'nanoaodv5_data2016.txt'
indir = ''
outdir   = 'root://cmseos.fnal.gov//store/user/lpcstop/noreplica/mequinna/NanoAODv6/'
if 'MC' in infile:
    if '2016' in infile or 'BDT' in infile:
        indir    = 'root://cmsxrootd.fnal.gov//store/mc/RunIISummer16NanoAODv6/'
    elif '2017' in infile:
        indir    = 'root://cmsxrootd.fnal.gov//store/mc/RunIIFall17NanoAODv6/'
    elif '2018' in infile:
        indir    = 'root://cmsxrootd.fnal.gov//store/mc/RunIIAutumn18NanoAODv6/'
elif '_data' in infile:
    indir    = 'root://cmsxrootd.fnal.gov//store/data/'
    # outdir+='/DATA/'

#loop over files in infile
command = ''
with open(infile) as searchfile:
    for line in searchfile:
        newoutdir=outdir
        #cleaning: make sure doesnt end in \n, ignore lines that start with #
        line = line.strip()
        if not line.startswith('#'):
            print 'line', line
            #copy files to that directory
            if not indir.endswith('/'): indir+='/'
            # if not outdir.endswith('/'): outdir+='/'
            if '2016' in infile:
                newoutdir+='2016'
            elif '2017' in infile:
                newoutdir+='2017'
            elif '2018' in infile:
                newoutdir+='2018'

            if '_data' in infile:
                newoutdir+='/DATA/'
                if 'JetHT' in line: newoutdir+='JETHT'
                elif 'SingleElectron' in line: newoutdir+='SINGLEEL'
                elif 'EGamma' in line: newoutdir+='SINGLEEL'
                elif 'SingleMuon' in line: newoutdir+='SINGLEMU'
                elif 'SinglePhoton' in line: newoutdir+='SINGLEPHO'
                else:
                    raise ValueError('ERROR data not recognized')
            elif '_MC' in infile:
                newoutdir+='/MC/'

            command += 'xrdcp -r ' +indir+line+' '+newoutdir+'; '
command = command[:-2]
print command
# subprocess.call(command, shell=True)
