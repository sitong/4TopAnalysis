import numpy as np
import math
import logging
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

# from MCM workflow:
# #Production notes (SL filter -> nGenJet 9)
# # Combination of filters is applied:
# # exactly 1 lepton (electron,muon or tau) in LHE record
# # HT calculated from jets with pT>30 and |eta|<2.4 > 500 GeV
# # Jet multiplicity (jet pT>30) >= 9 (SL) or 7 (DL)

# auto nFTAGenLep = static_cast<Int_t>(LHEPart_pdgId[abs(LHEPart_pdgId)==11 || abs(LHEPart_pdgId)==13 || abs(LHEPart_pdgId)==15].size());
# auto nFTAGenJet = static_cast<Int_t>(GenJet_pt[GenJet_pt > 30].size());
# auto FTAGenHT = VecOps::RVec::Sum(GenJet_pt[GenJet_pt > 30 && abs(GenJet_eta) < 2.4]);
# auto passDL = (nFTAGenLep == 2) && (nFTAGenJet >= 7) && (FTAGenHT > 500);
# auto passSL = (nFTAGenLep == 1) && (nFTAGenJet >= 9) && (FTAGenHT > 500);

class ttbbFilterTest(Module):
    
    def __init__(self, channel, year):
        self.channel = channel
        self.year    = year
        
    def beginJob(self):
        pass 

    def endJob(self):
        pass

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        print "inputFile", inputFile
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        print 'initializing trees...'
        self.out.branch("nFTAGenLep", "I")
        self.out.branch("nFTAGenJet", "I")
        self.out.branch("FTAGenHT", "F")
        self.out.branch("passAH", "I")
        self.out.branch("passSL", "I")
        self.out.branch("passDL", "I")

    def EventFiller(self, event):
        
        nFTAGenLep = 0
        nFTAGenJet = 0
        FTAGenHT = 0.0

        LHEParts = Collection(event, "LHEPart")
        GenJets = Collection(event, "GenJet")

        for p in LHEParts:
            if (abs(p.pdgId)==11 or abs(p.pdgId)==13 or abs(p.pdgId)==15):
                nFTAGenLep+=1

        for j in GenJets:
            if j.pt>30.0:
                nFTAGenJet+=1
                if abs(j.eta)<2.4:
                    FTAGenHT+=j.pt

        # nFTAGenLep = np.count_nonzero(abs(event.LHEPart_pdgId)==11 | abs(event.LHEPart_pdgId)==13 | abs(event.LHEPart_pdgId)==15 )
        # nFTAGenJet = np.count_nonzero((event.GenJet_pt)>30.0)
        # forHT = event.GenJet_pt[(event.GenJet_pt >30.0 & abs(event.GenJet_eta)<2.4)]
        # FTAGenHT = np.sum(forHT)
       
        passAH = False; passSL = False; passDL = False
        if (nFTAGenLep==2 and nFTAGenJet>=7 and FTAGenHT >500.0):
            passDL = True
        if (nFTAGenLep==1 and nFTAGenJet>=9 and FTAGenHT >500.0):
            passSL = True
        if (nFTAGenLep==0 and nFTAGenJet>=9 and FTAGenHT >500.0):
            passAH = True

        self.out.fillBranch("nFTAGenLep",       int(nFTAGenLep))
        self.out.fillBranch("nFTAGenJet",       int(nFTAGenJet))
        self.out.fillBranch("FTAGenHT",         FTAGenHT)
        self.out.fillBranch("passDL",           int(passDL))  
        self.out.fillBranch("passSL",           int(passSL))  
        self.out.fillBranch("passAH",           int(passAH))  

    def analyze(self, event):
        self.EventFiller(event)
        return True

filter2016_0lep = lambda : ttbbFilterTest('0lep','2016')
filter2017_0lep = lambda : ttbbFilterTest('0lep','2017')
filter2018_0lep = lambda : ttbbFilterTest('0lep','2018')
