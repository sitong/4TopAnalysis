## card: datacards_statcomm_PC/allhad4top_RT1BT0htbin7_2017_datacard5.txt number in set: 5
##selection: (nrestops_nosys==1 && nbstops_nosys==0 && ht_nosys>=1500 &&  nfunky_leptons==0 && ht_nosys>=700 && nbjets_nosys>=3 && njets_nosys>=9 && (goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag))*weight*btagSF_nosys*bSFcorr_nosys*puWeight*trigSF_nosys*bsWSF_nosys*bsTopSF_nosys*RTeffSF_nosys*RTmissSF_nosys*41.53*prefirewgt*

#----------------------------------------------#
#generating datacards. name: allhad4top, year2017

#number of cards in set: 12

## number of signals (i), backgrounds (j), and uncertainties (k)
---
imax: 1
jmax: 3
kmax: *
---

## input the bdt shape histograms
---
shapes * * allhad4top_2017_shapehists.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC
---

## list the bin label and the number of (data) events observed
---
bin  RT1BT0htbin7_2017
observation 861.0
---

## expected events for signal and all backgrounds in the bins
---
bin        RT1BT0htbin7_2017    RT1BT0htbin7_2017    RT1BT0htbin7_2017    RT1BT0htbin7_2017
process    TTTT    TTX    other    DDBKG
process        0        1        2        3
rate       1.45581247658    12.8619151115    26.7551546097    628.353
---

## list the independent sources of uncertainties, and give their effect (syst. error) on each process and bin
---
## statistical uncertainties

RT1BT0htbin7_2017_TTTT_stat      lnN    1.02356484839    -    -    -
RT1BT0htbin7_2017_TTX_stat      lnN    -    1.08516246492    -    -
RT1BT0htbin7_2017_other_stat      lnN    -    -    1.13085766806    -
RT1BT0htbin7_2017_DDBKG_stat      lnN    -    -    -    1.10923796019

---
## systematic uncertainties

lumi_uncorr_2017   lnN    1.020    1.020    1.020    -    
lumi_corr16_17_18   lnN    1.009    1.009    1.009    -    
lumi_corr17_18   lnN    1.006    1.006    1.006    -    
elveto_2017   lnN    1.03761     1.04195    -    -    
muveto_2017   lnN    1.00555     1.00014    -    -    
prefire_2017   lnN    1.00557     1.00445    -    -    
cross_section   lnN   -   1.26    -    -    
data-pred_norm_disagreement_RT1BT0_2017    lnN        -         -         -      1.25
data_pred_shape_disagreement_RT1BT0_2017  shape?     -    -    -    1.0  

---
## shape uncertainties

jes_2017   shape     1.0   1.0   1.0    - 
jer_2017   shape     1.0   1.0   1.0    - 
pileup_2017   shape     1.0   1.0   1.0    - 
trigger_2017   shape     1.0   1.0   1.0    - 
btagLF  shape     1.0    1.0    1.0    - 
btagHF  shape     1.0    1.0    1.0    - 
btagLFstats1_2017   shape     1.0   1.0   1.0    - 
btagHFstats1_2017   shape     1.0   1.0   1.0    - 
btagLFstats2_2017   shape     1.0   1.0   1.0    - 
btagHFstats2_2017   shape     1.0   1.0   1.0    - 
btagCFerr1  shape     1.0    1.0    1.0    - 
btagCFerr2  shape     1.0    1.0    1.0    - 
DeepAK8TopSF_2017   shape     1.0   1.0   1.0    - 
DeepAK8WSF_2017   shape     1.0   1.0   1.0    - 
isr_TTTT  shape     1.0    -    -    -  
isr_TTX  shape     -    1.0    -    -  
fsr_TTTT  shape     1.0    -    -    -  
fsr_TTX  shape     -    1.0    -    -  
pdf  shape     1.0    1.0    -    - 
ME_TTTT  shape     1.0    -    -    -  
ME_TTX  shape     -    1.0    -    -  
ResTopEff_2017   shape     1.0   1.0   1.0    - 
ResTopMiss_2017   shape     1.0   1.0   1.0    - 

---

RT1BT0htbin7_2017 autoMCStats 0 1
---

