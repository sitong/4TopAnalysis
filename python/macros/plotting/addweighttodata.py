import os
import numpy as np
import argparse
import ROOT as r
import argparse
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)
gStyle.SetPalette(1)

from Defs import plotdefs
from addMCsubweight import addMCsubweight
import RootStuff as rs

#have RootStuff.py, addMCsubweight.py and the weight histograms in the same directory
#run command as (for example) python addweighttodata.py -y 2018 
#change path to datafilename
#will output modified datafile with new branches to current directory 

parser = argparse.ArgumentParser(description='input year with -y')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

subMC_withttbb = addMCsubweight(True)
subMC_NOttbb = addMCsubweight(False)

RT1BT0hist_withttbb    = subMC_withttbb.getweighthist('RT1BT0', args.year)
RT1BT1hist_withttbb    = subMC_withttbb.getweighthist('RT1BT1', args.year)
RT2BTALLhist_withttbb  = subMC_withttbb.getweighthist('RT2BTALL', args.year)

RT1BT0hist_NOttbb    = subMC_NOttbb.getweighthist('RT1BT0', args.year)
RT1BT1hist_NOttbb    = subMC_NOttbb.getweighthist('RT1BT1', args.year)
RT2BTALLhist_NOttbb  = subMC_NOttbb.getweighthist('RT2BTALL', args.year)

pd = plotdefs(args.year, 'SR', modbins=False)
datafilename = pd.datafilename

treename = 'Events'
drf, dt =  rs.makeroot(datafilename, treename)

outname = datafilename[datafilename.rfind('/')+1:]
#will output updated file to current directory. can change outputname if you want to put it elsewhere
outrf = r.TFile.Open(outname, "update")    
outrf.cd()
outtree=dt.CloneTree(0)

print 'adding variables to tree in outputfile', outname 
#name variables to add to tree
MCsubweight_RT1BT0_withttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweight_RT1BT1_withttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweight_RT2_withttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweight_RT1BT0_NOttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweight_RT1BT1_NOttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweight_RT2_NOttbb = np.full(1, 1.0, dtype=np.float32)
outtree.Branch('MCsubweight_RT1BT0_withttbb',   MCsubweight_RT1BT0_withttbb,   'MCsubweight_RT1BT0_withttbb/F')
outtree.Branch('MCsubweight_RT1BT1_withttbb',   MCsubweight_RT1BT1_withttbb,   'MCsubweight_RT1BT1_withttbb/F')
outtree.Branch('MCsubweight_RT2_withttbb',      MCsubweight_RT2_withttbb,      'MCsubweight_RT2_withttbb/F')
outtree.Branch('MCsubweight_RT1BT0_NOttbb',     MCsubweight_RT1BT0_NOttbb,     'MCsubweight_RT1BT0_NOttbb/F')
outtree.Branch('MCsubweight_RT1BT1_NOttbb',     MCsubweight_RT1BT1_NOttbb,     'MCsubweight_RT1BT1_NOttbb/F')
outtree.Branch('MCsubweight_RT2_NOttbb',        MCsubweight_RT2_NOttbb,        'MCsubweight_RT2_NOttbb/F')
MCsubweightBDTdisc_RT1BT0_withttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweightBDTdisc_RT1BT1_withttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweightBDTdisc_RT2_withttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweightBDTdisc_RT1BT0_NOttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweightBDTdisc_RT1BT1_NOttbb = np.full(1, 1.0, dtype=np.float32)
MCsubweightBDTdisc_RT2_NOttbb = np.full(1, 1.0, dtype=np.float32)
outtree.Branch('MCsubweightBDTdisc_RT1BT0_withttbb',   MCsubweightBDTdisc_RT1BT0_withttbb,   'MCsubweightBDTdisc_RT1BT0_withttbb/F')
outtree.Branch('MCsubweightBDTdisc_RT1BT1_withttbb',   MCsubweightBDTdisc_RT1BT1_withttbb,   'MCsubweightBDTdisc_RT1BT1_withttbb/F')
outtree.Branch('MCsubweightBDTdisc_RT2_withttbb',      MCsubweightBDTdisc_RT2_withttbb,      'MCsubweightBDTdisc_RT2_withttbb/F')
outtree.Branch('MCsubweightBDTdisc_RT1BT0_NOttbb',     MCsubweightBDTdisc_RT1BT0_NOttbb,     'MCsubweightBDTdisc_RT1BT0_NOttbb/F')
outtree.Branch('MCsubweightBDTdisc_RT1BT1_NOttbb',     MCsubweightBDTdisc_RT1BT1_NOttbb,     'MCsubweightBDTdisc_RT1BT1_NOttbb/F')
outtree.Branch('MCsubweightBDTdisc_RT2_NOttbb',        MCsubweightBDTdisc_RT2_NOttbb,        'MCsubweightBDTdisc_RT2_NOttbb/F')

#calculate variables to add to tree
#NOTE: need to use correct HT and BDT disc names
for i, evt in enumerate(dt):
    MCsubweight_RT1BT0_withttbb[0] = subMC_withttbb.getweight(evt.ht_nosys, evt.catBDTdisc_nosys, RT1BT0hist_withttbb)
    MCsubweight_RT1BT1_withttbb[0] = subMC_withttbb.getweight(evt.ht_nosys, evt.catBDTdisc_nosys, RT1BT1hist_withttbb)
    MCsubweight_RT2_withttbb[0] = subMC_withttbb.getweight(evt.ht_nosys, evt.catBDTdisc_nosys, RT2BTALLhist_withttbb)
    MCsubweight_RT1BT0_NOttbb[0] = subMC_NOttbb.getweight(evt.ht_nosys, evt.catBDTdisc_nosys, RT1BT0hist_NOttbb)
    MCsubweight_RT1BT1_NOttbb[0] = subMC_NOttbb.getweight(evt.ht_nosys, evt.catBDTdisc_nosys, RT1BT1hist_NOttbb)
    MCsubweight_RT2_NOttbb[0] = subMC_NOttbb.getweight(evt.ht_nosys, evt.catBDTdisc_nosys, RT2BTALLhist_NOttbb)

    MCsubweightBDTdisc_RT1BT0_withttbb[0] = evt.catBDTdisc_nosys*MCsubweight_RT1BT0_withttbb[0]
    MCsubweightBDTdisc_RT1BT1_withttbb[0] = evt.catBDTdisc_nosys*MCsubweight_RT1BT1_withttbb[0]
    MCsubweightBDTdisc_RT2_withttbb[0] = evt.catBDTdisc_nosys*MCsubweight_RT2_withttbb[0]
    MCsubweightBDTdisc_RT1BT0_NOttbb[0] = evt.catBDTdisc_nosys*MCsubweight_RT1BT0_NOttbb[0]
    MCsubweightBDTdisc_RT1BT1_NOttbb[0] = evt.catBDTdisc_nosys*MCsubweight_RT1BT1_NOttbb[0]
    MCsubweightBDTdisc_RT2_NOttbb[0] = evt.catBDTdisc_nosys*MCsubweight_RT2_NOttbb[0]

    outtree.Fill()

outtree.Write("", r.TFile.kOverwrite)
outtree.ResetBranchAddresses()

outrf.Close()
drf.Close()
print 'wrote', outrf
