#just make sure that split files are finished
import subprocess
import numpy as np
import argparse
import ROOT as r
import pandas as pd
import sys
sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
# import RootStuff as rs
import os
import re

parser = argparse.ArgumentParser(description='')
parser.add_argument("-f", "--filenames", dest="txtfile", default='./2018split.txt')
parser.add_argument("-d", "--dir", dest="checkdir", default='/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6/')
args = parser.parse_args()

procs = ['TTTT', 'TTGJets', 'TTZToLLNuNu', 'TTZToQQ','TTWJetsToLNu', 'TTWJetsToQQ', 'ttHTobb', 'ttHToNonbb']
years = {'/2016/':'2016', '/2017/':'2017', '/2018/':'2018'}

with open(args.txtfile, "r") as filelist:
    for f in filelist:
        if '#' not in f:
            fname = f[f.rfind('/')+1:]
            fname = fname.strip()
            fname = fname.replace('.root', '')
            proc = ''; year = ''
            for p in procs:
                if p in f:
                    proc = p
            for yname, y in years.iteritems():
                if yname in f:
                    year = y

            #_TTTT2016_0.root
            checkfile0 = args.checkdir+year+'/MC/'+proc+'_SPLIT/'+fname+'_'+proc+year+'_0.root'
            checkfile1 = args.checkdir+year+'/MC/'+proc+'_SPLIT/'+fname+'_'+proc+year+'_1.root'
            checkfile2 = args.checkdir+year+'/MC/'+proc+'_SPLIT/'+fname+'_'+proc+year+'_2.root'
            checkfile3 = args.checkdir+year+'/MC/'+proc+'_SPLIT/'+fname+'_'+proc+year+'_3.root'
            # print checkfile0
            # print checkfile1
            # print checkfile2
            # print checkfile3
            cmd0= 'ls '+checkfile0
            cmd1= 'ls '+checkfile1
            cmd2= 'ls '+checkfile2
            cmd3= 'ls '+checkfile3
            # print cmd0
            subprocess.call(cmd0, shell=True)
            subprocess.call(cmd1, shell=True)
            subprocess.call(cmd2, shell=True)
            subprocess.call(cmd3, shell=True)

