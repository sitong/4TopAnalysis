#sf plotsevt, udated from ControlPlots
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Defs import plotdefs
import root_numpy as rn
# import re
import ROOT as r
import selections as sels
import Defs as defs
import RootStuff as rs
import plottery as ply
import getABCDbackgrounds as getABCD
import argparse
import sys
from ROOT import gStyle, gPad
gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='adds eventBDT discriminants to premade trees.')
# parser.add_argument("-v", "--var", dest="var", default='catBDTdisc')
parser.add_argument("-t", "--typ", dest="typ", default='val')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

typ=args.typ #miss, eff
v = 'rescand_pt'

year = args.year
lumi = str(rs.lumis[year]['0lep'])
treename = 'Friends'
if v=='nrestops':
    treename='Events'
byevt = False
gensub = True

varbins=False
# if v=='rescand_pt':
#     varbins=True
edges = [0.0, 100.0, 200.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 1000.0]
# edges = [0.0, 100.0, 200.0, 300.0, 400.0, 500.0, 600.0, 700.0, 800.0, 1000.0]
# edges = [0.0, 100.0, 150.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 1000.0]

if typ =='eff':
    edges = [0.0, 100.0, 200.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 800.0]
    samplist = ["MC"]
    allmc=["MC"]
    ### # if not gensub:
    allmc =  ["TT_1L", "TTTT", "singletop", "TT_2L", "ttX", "TT_0L", "other", "QCD"]
    if year =='2016':
        allmc =  ["TT", "TTTT", "singletop", "ttX", "other", "QCD"]
    # allmc =  ["TT_1L", "TTTT", "DY",  "dibosons", "singletop", "TT_2L", "Z+Jets", "W+Jets", "ttX", "TT_0L", "QCD"] # 
    # if year =='2016':
    #     allmc =  ["TT", "TTTT", "DY",  "dibosons", "singletop",  "Z+Jets", "W+Jets", "ttX", "QCD"]
    channame = '1lep'

elif typ =='val' or typ=='effval' or typ=='misval':
    samplist = ["MC"]
    # allmc=["MC"]
    #    if not gensub:
    allmc=['minormc']
    if v=='nrestops':
        allmc = ['minormc_evt']
    # allmc =  ["TT_1L", "TTTT", "singletop", "TT_2L", "ttX", "TT_0L", "other"]
    # if year =='2016':
    #     allmc =  ["TT", "TTTT", "singletop", "ttX", "other"]
    channame = '1lep'
    if typ=='misval' or typ=='effval' or typ=='val':
        channame = '0lep'
        
elif typ=='miss':
    samplist = ["MC"]
    allmc = ["MC"]
    ####if not gensub:
    # allmc =  ["TT_1L", "TTTT", "singletop", "TT_2L", "ttX", "TT_0L", "other", "QCD"]
    # if year =='2016':
    #     allmc =  ["TT", "TTTT", "singletop", "ttX", "other", "QCD"]
    # allmc =  ["TT_1L", "TTTT", "DY",  "dibosons", "singletop", "TT_2L", "Z+Jets", "W+Jets", "ttX", "TT_0L", "QCD"]
    # if year =='2016':
    #     allmc =  ["TT", "TTTT", "DY",  "dibosons", "singletop",  "Z+Jets", "W+Jets", "ttX", "QCD"]
    channame = '0lep'

elif typ=='genmiss':
    allmc =  ["MC", "MC_nogen"]
    samplist = ["MC"]
    channame = '0lep'

elif typ=='geneff':
    edges = [0.0, 100.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 550.0, 1000.0]
    samplist = ["MC"]
    allmc = ["MC", "MC_nogen"]
    channame = '1lep'

def getyield(hist):
    errorVal = r.Double(0)
    minbin=0
    maxbin=hist.GetNbinsX()+1
    hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
    print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
    return hyield,  errorVal


def getDatahists(projvar, cut, channel, sub=None):
    nbins, selrange = sels.getHistSettings(projvar)

    if channel == '0lep' or channel=='0lep_evt':
        f = datafiles[channel]
    elif channel=='1lep':
        f = datafiles['1mu']
    elif channel=='1el':
        f = datafiles['1el']

    print f, channel
    rf, t = rs.makeroot(f, treename)

    if byevt:
        cut = cut.replace('( ', '(candnum==0 && ')
        
    if varbins:
        # print edges
        datahist = rs.getvarbinhist(t, projvar, cut, edges)
    else:        
        datahist = rs.gethist(t, projvar, cut, nbins, selrange)

    getyield(datahist)        
    
    if sub is not None:
        subhist = sub[0].Clone("subhist")
        print 'subtracting MC histogram', subhist
        datahist.Add(subhist)

    print 'data cut',cut
    getyield(datahist)        
    rs.fixref(rf, datahist)
    datahist.SetDirectory(0)    
    return datahist

def getMChists(projvar, mycut, mcfiles, samplist):
    nbins, selrange = sels.getHistSettings(projvar)
    hists = []
    for i,f in enumerate(samplist):
        print i,f, mcfiles[f]
        cut=mycut
        if byevt:
            cut = cut.replace('( ', '(candnum==0 && ')
        # elif byevt and gensub:
        #     cut = cut.replace('(', '(candnum==0 && NgoodtagRes>=1 &&')
            
        if f=='MC_nogen' and (typ=='geneff'):
            cut = cut.replace("rescand_genmatch==1","rescand_genmatch==0")
        if f=='MC' and typ=='genmiss':
            cut = cut.replace("rescand_genmatch==0","rescand_genmatch==1")

        rf, t = rs.makeroot(mcfiles[f], treename)
        print 'MC cut:', cut

        if varbins:
            hist = rs.getvarbinhist(t, projvar, cut, edges)
        else:
            hist = rs.gethist(t, projvar, cut, nbins, selrange)

        rs.fixref(rf, hist)
        getyield(hist)
        hists.append(hist)            
    return hists

def makeControlHist(plotvar, data, hists, labels, name, cperr=None, logy=False):
    varnames = {
    'rescand_disc': 'resolved top BDT discriminant',
    'rescand_pt': 'resolved top candidate pT',
    'rescand_ptDR': 'resolved top candidate ptDR',
    'rescand_mass': 'resolved top candidate mass',
    'ht': 'HT',
    'bjht': 'HT(bJets)', 
    'met': 'MET',
    'njets': 'N(Jets)',
    'nbjets': 'N(bJets)', 
    'nbstops': 'N(boosted tops)',
    'nleptons': 'N(leptons)',
    'jet1pt': 'PT(Lead Jet)',
    'nrestops': 'N(resolved tops)',
    'restop1pt': 'PT(Lead resolved top)',
    'leadbpt': 'PT(Lead bJet)',
    'sphericity': 'sphericity', 
    'aplanarity': 'aplanarity', 
    'fwm30': 'Fox Wolfram Moment (30)',
    'catBDTdisc' : 'eventBDT discriminant',
    'evtbdtdisc' : 'eventBDT discriminant',
    'passtrigmu': 'passtrigmu',
    'passtrigel': 'passtrigel',
    'lep1pt':'lep1pt',
    'lep2pt':'lep2pt',
    }
    colors = [r.kRed-7, r.kOrange+1, r.kYellow-7, r.kGreen-9, r.kGreen, r.kCyan-7, r.kAzure, r.kBlue-9, r.kBlue, r.kViolet-4,r.kViolet, r.kMagenta-9, r.kMagenta]
    # colors = [r.kRed-2,r.kBlue,r.kViolet, r.kSpring, r.kTeal-1, r.kPink, r.kAzure-4, r.kCyan, r.kMagenta]# r.kYellow,r.kGreen,r.kOrange+2,
    if not os.path.exists('plots/sfdists/'):
        os.makedirs('plots/sfdists/')        
    outname = 'plots/sfdists/'+name+'_'+year+'_'+plotvar+'.png'

    print outname
    print 'log?',logy
    print 'data',data
    print 'hists',hists
    print 'num bgs', len(hists)
    print 'labels',labels
    cp = []
    if cperr is not None:
        cp = cperr
    c1, hist, err = ply.plot_hist( 
        data = data,
        bgs  = hists,
        colors = colors, 
        legend_labels = labels,
        options = {
            #special error bars?
            # if cperr_b and cperr_d:
            "clopper_pearson":cp,
            "do_stack": True,
            "xaxis_label": varnames[plotvar],
            "yaxis_label": "Events",
            "legend_scalex": 0.7,
            "legend_scaley": 1.2,
            "bkg_sort_method":'ascending',
            # "extra_text": [plotvar],
            "yaxis_log": logy,
            "ratio_range":[0.5,1.5],
            "returnhist": True,
            "hist_disable_xerrors": False,
            "show_bkg_errors": True,
            "bkg_err_fill_style":3003,
            "bkg_err_fill_color":r.kGray,
            # "palette_name": "pastel",
            "yaxis_moreloglabels": logy, 
            "legend_percentageinbox": False,
            "legend_alignment": "top right",
            "cms_label": "Preliminary",
            "legend_smart": False,
            "nofill": False,
            "output_name": outname,
            "yaxis_label":"Resolved Top Candidates",
            }
       )
    return hist, err


wp = {'2016':'0.9988',
      '2017':'0.9992',
      '2018':'0.9994'}
topwp=wp[year]

mcfiles   = { #'+channame+'
    'MC':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/MC_'+year+'_merged_'+channame+'SF_loosesel_tree.root',
    'other':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/other_'+year+'_merged_'+channame+'SF_tree.root',
    'minormc':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/minorMC_'+year+'_merged_'+channame+'SF_loosesel_tree.root',
    'minormc_evt':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/perevent/minorMC_'+year+'_merged_'+channame+'_loosesel_tree.root',
    'TT_evt':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/perevent/TT_'+year+'_merged_'+channame+'_loosesel_tree.root',
    'QCD_evt':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/perevent/QCD_'+year+'_merged_'+channame+'_loosesel_tree.root',
    'MC_nogen':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/MC_'+year+'_merged_'+channame+'SF_loosesel_tree.root',
    'TTTT':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/TTTT_'+year+'_merged_'+channame+'SF_tree.root',
    'Z+Jets':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/zjets_'+year+'_merged_'+channame+'SF_tree.root',
    'TT':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/TT_'+year+'_merged_'+channame+'SF_tree.root',
    'TT_2L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/TTTo2L2Nu_'+year+'_merged_'+channame+'SF_tree.root',
    'TT_1L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/TTToSemiLeptonic_'+year+'_merged_'+channame+'SF_tree.root',
    'TT_0L':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/TTToHadronic_'+year+'_merged_'+channame+'SF_tree.root',
    'QCD':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/QCD_'+year+'_merged_'+channame+'SF_tree.root',
    'ttX':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/TTX_'+year+'_merged_'+channame+'SF_tree.root',
    'singletop':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/singletop_'+year+'_merged_'+channame+'SF_tree.root',
    'dibosons':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/dibosons_'+year+'_merged_'+channame+'SF_tree.root',
    'W+Jets':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/wjets_'+year+'_merged_'+channame+'SF_tree.root',
    'DY':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/'+channame+'/SFtrees/withevtsfs/DY_'+year+'_merged_'+channame+'SF_tree.root'}

datafiles = {'1lep': None, 
             '1el':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/1lep/SFtrees/withtrig/SINGLEEL_'+year+'_merged_'+channame+'SF_tree.root',
             '1mu':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/1lep/SFtrees/SINGLEMU_'+year+'_merged_'+channame+'SF_tree.root',
             '0lep':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/SFtrees/withevtsfs/JETHT_'+year+'_merged_'+channame+'SF_loosesel_tree.root' ,
             # '0lep':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/SFtrees/JETHT_'+year+'_merged_'+channame+'SF_tree.root' 
             '0lep_evt':'/eos/uscms/store/user/lpcstop/noreplica/mequinna/NanoAODv6_trees/'+year+'/MC/0lep/SFtrees/perevent/JETHT_'+year+'_merged_'+channame+'_loosesel_tree.root' }
 
metfilters   = ' && goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag'
if year=='2016':
    metfilters = ' && goodverticesflag && haloflag && HBHEflag && HBHEisoflag && ecaldeadcellflag && badmuonflag && eeBadScFilterflag'

trigs ={
    '1lep':{'2016':'&& (trigHLT_IsoTkMu24 || trigHLT_IsoMu24)',
            '2017':'&& (trigHLT_IsoMu27 || trigHLT_IsoMu30)',
            '2018':'&& (trigHLT_IsoMu27 || trigHLT_IsoMu30)'},
    '1el':{'2016':'&& (trigHLT_Ele27_WPTight_Gsf)',
           '2017':'&& (trigHLT_Ele27_WPTight_Gsf || trigHLT_Ele32_WPTight_Gsf)',
           '2018':'&& (trigHLT_Ele27_WPTight_Gsf || trigHLT_Ele32_WPTight_Gsf || trigHLT_Ele35_WPTight_Gsf)'},
    '0lep':{'2016':'&& trigHLT_PFHT900',
            '2017':'&& trigHLT_PFHT1050',
            '2018':'&& trigHLT_PFHT1050'},
    '0lep_evt':{'2016':'&& trigHLT_PFHT900',
            '2017':'&& trigHLT_PFHT1050',
            '2018':'&& trigHLT_PFHT1050'}}

weights = '*weight*'+lumi+'*puWeight'
if v=='nrestops':
    weights='*weight*btagSF_nosys*puWeight*bsWSF_nosys*bsTopSF_nosys*'+lumi
    if gensub:
        weights+='*RTeffSF_nosys*RTmissSF_nosys'
norms = {
    'eff':{'2016':'1.035', #>2 b
           '2017':'1.110',
           '2018':'1.166'},
    # 'val':{'2016':'1.076',
    #        '2017':'1.196',
    #        '2018':'1.094'},
    'val':{'2016':4.08578280826,
           '2017':2.89192916233,
           '2018':3.59082641974},
    # 'eff':{'2016':'1.',
    #        '2017':'1.',
    #        '2018':'1.'},
    # 'miss':{'2016':'1.',
    #         '2017':'1.',
    #         '2018':'1.'}}
    # 'miss':{'2016':'0.899', #met cut 50
    #         '2017':'1.480',
    #         '2018':'1.441'}}
    'miss':{'2016':'0.938', #old
            '2017':'1.63',
            '2018':'1.62'}}

if typ=='eff' or typ=='geneff':
    channel = '1lep'
    norm = norms['eff'][year]
    weights+=norm
    # weights+='*mistagSF'

    mcut_num = '( nfunky_leptons==1 && nfunky_muons==1 && rescand_genmatch==1 && met>=75.0 && nbjets>=2 && njets>=4 && funkylep1_pt>50.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    mcut_den = '( nfunky_leptons==1 && nfunky_muons==1 && rescand_genmatch==1 && met>=75.0 && nbjets>=2 && njets>=4 && funkylep1_pt>50.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    dcut_num = '( nfunky_leptons==1 && nfunky_muons==1 && met>=75.0 && nbjets>=2 && njets>=4 && funkylep1_pt>50.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    dcut_den = '( nfunky_leptons==1 && nfunky_muons==1 && met>=75.0 && nbjets>=2 && njets>=4 && funkylep1_pt>50.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

    # mcut_num = '( nfunky_leptons==1 && nfunky_muons==1 && rescand_genmatch==1 && nbjets>=2 && (rescand_pt<300.0 ? (met>=0.0 && met<30.0):met>100.0) && njets>=4 && funkylep1_pt>50.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    # mcut_den = '( nfunky_leptons==1 && nfunky_muons==1 && rescand_genmatch==1 && nbjets>=2 && (rescand_pt<300.0 ? (met>=0.0 && met<30.0):met>100.0) && njets>=4 && funkylep1_pt>50.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    # dcut_num = '( nfunky_leptons==1 && nfunky_muons==1 && nbjets>=2 && (rescand_pt<300.0 ? (met>=0.0 && met<30.0):met>100.0) && njets>=4 && funkylep1_pt>50.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    # dcut_den = '( nfunky_leptons==1 && nfunky_muons==1 && nbjets>=2 && (rescand_pt<300.0 ? (met>=0.0 && met<30.0):met>100.0) && njets>=4 && funkylep1_pt>50.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

    # mcut_num = '( nfunky_leptons==1 && nfunky_muons==1 && rescand_genmatch==1 && (nbjets==1 || (nbjets>=2 && ht<510)) && njets>=4 && met>30 && funkylep1_pt>40.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    # mcut_den = '( nfunky_leptons==1 && nfunky_muons==1 && rescand_genmatch==1 && (nbjets==1 || (nbjets>=2 && ht<510)) && njets>4 && met>30 && funkylep1_pt>40.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    # dcut_num = '( nfunky_leptons==1 && nfunky_muons==1 && (nbjets==1 || (nbjets>=2 && ht<510)) && njets>=4 && met>30 && funkylep1_pt>40.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    # dcut_den = '( nfunky_leptons==1 && nfunky_muons==1 && (nbjets==1 || (nbjets>=2 && ht<510)) && njets>=4 && met>30 && funkylep1_pt>40.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

elif typ=='effval':
    channel = '0lep'
    norm = norms['val'][year]
    #weights+=norm
    if gensub and v!='nrestops':
            weights+='*(rescand_genmatch==1 ? effSF : mistagSF)'
    mcut_num = '( nfunky_leptons==0 && nbjets>=3 && njets==8 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    mcut_den = '( nfunky_leptons==0 && nbjets>=3 && njets==8 && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    dcut_num = '( nfunky_leptons==0 && nbjets>=3 && njets==8 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    dcut_den = '( nfunky_leptons==0 && nbjets>=3 && njets==8 && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

elif typ=='misval':
    channel = '0lep'
    norm = norms['val'][year]
    if gensub and v!='nrestops':
            weights+='*(rescand_genmatch==1 ? effSF : mistagSF)'
    #weights+=norm
    mcut_num = '( nfunky_leptons==0 && nbjets==1 && (njets==4 || njets==5) && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    mcut_den = '( nfunky_leptons==0 && nbjets==1 && (njets==4 || njets==5) && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    dcut_num = '( nfunky_leptons==0 && nbjets==1 && (njets==4 || njets==5) && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    dcut_den = '( nfunky_leptons==0 && nbjets==1 && (njets==4 || njets==5) && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

elif typ=='val':
    channel = '0lep'
    # norm = norms['val'][year]
    # weights+=norm
    if gensub and v!='nrestops':
            weights+='*(rescand_genmatch==1 ? effSF : mistagSF)'
    mcut_num = '( nfunky_leptons==0 && nbjets>=3 && njets==8 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    dcut_num = '( nfunky_leptons==0 && nbjets>=3 && njets==8 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    
elif typ=='miss' or typ=='genmiss':
    channel = '0lep'
    norm =  norms['miss'][year]
    # normnum =  norms['missnum'][year]
    # normden =  norms['missden'][year]
    # weightsnum=weights+normnum
    # weightsden=weights+normden
    weights+=norm
    # mcut_num = '( nfunky_leptons==0 && nbjets==1 && rescand_genmatch==0 && njets>=6 && met<50.0 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    # mcut_den = '( nfunky_leptons==0 && nbjets==1 && rescand_genmatch==0 && njets>=6 && met<50.0 && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    # dcut_num = '( nfunky_leptons==0 && nbjets==1 && njets>=6 && met<50.0 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    # dcut_den = '( nfunky_leptons==0 && nbjets==1 && njets>=6 && met<50.0 && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

    mcut_num = '( nfunky_leptons==0 && nbjets==1 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    mcut_den = '( nfunky_leptons==0 && nbjets==1 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    dcut_num = '( nfunky_leptons==0 && nbjets==1 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    dcut_den = '( nfunky_leptons==0 && nbjets==1 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'
    # mcut_num = '( nfunky_leptons==0 && nbjets==1 && rescand_genmatch==0 && njets>=6 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    # mcut_den = '( nfunky_leptons==0 && nbjets==1 && rescand_genmatch==0 && njets>=6 && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    # dcut_num = '( nfunky_leptons==0 && nbjets==1 && njets>=6 && ht>=1200.0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    # dcut_den = '( nfunky_leptons==0 && nbjets==1 && njets>=6 && ht>=1200.0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

    # mcut_num = '( nfunky_leptons==0 && nbjets==1 && ht>=1200.0 && (rescand_pt<300.0 ? met<20.0:met>=0.0) && njets>=6 && rescand_genmatch==0 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'+weights
    # mcut_den = '( nfunky_leptons==0 && nbjets==1 && ht>=1200.0 && (rescand_pt<300.0 ? met<20.0:met>=0.0) && njets>=6 && rescand_genmatch==0 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'+weights
    # dcut_num = '( nfunky_leptons==0 && nbjets==1 && ht>=1200.0 && (rescand_pt<300.0 ? met<20.0:met>=0.0) && njets>=6 && rescand_disc>'+topwp+metfilters+trigs[channel][year]+')'
    # dcut_den = '( nfunky_leptons==0 && nbjets==1 && ht>=1200.0 && (rescand_pt<300.0 ? met<20.0:met>=0.0) && njets>=6 && rescand_disc>-1.0'+metfilters+trigs[channel][year]+')'

if typ=='geneff' or typ=='genmiss' :
    datahist_num = getDatahists(v, dcut_num, channel)
    datahist_den = getDatahists(v, dcut_den, channel)
    mc_pass  = getMChists(v, mcut_num, mcfiles, allmc)
    mc_all   = getMChists(v, mcut_den, mcfiles, allmc)

elif typ=='effval' or typ=='misval':
    #mc - use everything except ttbar and QCD - create dedicated files.
    #else use data-othermc
    mcv=v
    if v!='nrestops':
        minormcfile = mcfiles['minormc']
        dfile = datafiles['0lep']
    elif v == 'nrestops': #other type of file
        minormcfile = mcfiles['minormc_evt']
        dfile = datafiles['0lep_evt']
        mcut_num = mcut_num.replace('&& rescand_disc>'+topwp, '')
        mcut_num = mcut_num.replace(trigs['0lep'][year],'')
        mcut_num = mcut_num.replace('nbjets','nbjets_nosys')
        mcut_num = mcut_num.replace('njets','njets_nosys')
        mcut_num = mcut_num.replace(' ht',' ht_nosys')
        mcv='nrestops_nosys'
    mc_pass  = getMChists(mcv, mcut_num, mcfiles, allmc) # minormc file
    dd=getABCD.ABCDbackgrounds(v, typ, year, minormcfile, dfile, gensub) # initialize ABCD calculation class
    ABCDhist, datahist_num = dd.getABCDhists()
    mc_pass.append(ABCDhist)

elif typ=='val':

    # print dcut_num
    # mc_pass  = getMChists(v, mcut_num, mcfiles, allmc)
    mcv=v
    if v!='nrestops':
        minormcfile = mcfiles['minormc']
        dfile = datafiles['0lep']
    elif v == 'nrestops': #other type of file
        minormcfile = mcfiles['minormc_evt']
        dfile = datafiles['0lep_evt']
        mcut_num = mcut_num.replace('&& rescand_disc>'+topwp, '')
        mcut_num = mcut_num.replace(trigs['0lep'][year],'')
        mcut_num = mcut_num.replace('nbjets','nbjets_nosys')
        mcut_num = mcut_num.replace('njets','njets_nosys')
        mcut_num = mcut_num.replace(' ht',' ht_nosys')
        mcv='nrestops_nosys'
    mc_pass  = getMChists(mcv, mcut_num, mcfiles, allmc) # minormc file
    qcdvar='QCD'; ttvar='TT'
    if v=='nrestops':
        qcdvar='QCD_evt'; ttvar='TT_evt'
    QCDhist = getMChists(mcv, mcut_num, mcfiles, [qcdvar])
    QCDhist=QCDhist[0]
    TThist = getMChists(mcv, mcut_num, mcfiles, [ttvar])
    TThist=TThist[0]
    dd=getABCD.ABCDbackgrounds(v, 'effval', year, minormcfile, dfile, gensub) # initialize ABCD calculation class
    ABCDhist, datahist_num, calcyld, calcunc = dd.getABCDhists(returnyld=True)
    QCDhist.Sumw2(); TThist.Sumw2()
    if v=='nrestops' or v=='rescand_pt':
        qcdyld, qcderr = getyield(QCDhist)
        ttyld, tterr = getyield(TThist)
        ttrat = ttyld/(qcdyld+ttyld)
        qcdrat = qcdyld/(qcdyld+ttyld)
        ttcalcyld = (ttrat*calcyld)/ttyld
        qcdcalcyld = (qcdrat*calcyld)/qcdyld
        QCDhist.Scale(qcdcalcyld)
        TThist.Scale(ttcalcyld)
        print 'qcd yld/calcyld', qcdcalcyld, ttcalcyld
        #and do the error bands
        for ibin in range(0, QCDhist.GetNbinsX()+2):
            qv=QCDhist.GetBinContent(ibin)
            qe=QCDhist.GetBinError(ibin)
            tv=TThist.GetBinContent(ibin)
            te=TThist.GetBinError(ibin)
            tnew = np.sqrt(te**2+((calcunc/calcyld)*tv)**2)
            qnew = np.sqrt(qe**2+((calcunc/calcyld)*qv)**2)
            # print 'TT',te, tnew
            # print 'QCD',qe, qnew
            TThist.SetBinError(ibin, tnew)
            QCDhist.SetBinError(ibin, qnew)
    # else:
    #     norm = norms['val'][year]
    #     qcdyld, qcderr = getyield(QCDhist)
    #     ttyld, tterr = getyield(TThist)
    #     ttrat = ttyld/(qcdyld+ttyld)
    #     qcdrat = qcdyld/(qcdyld+ttyld)
    #     ttcalcyld = ttrat*norm
    #     qcdcalcyld = qcdrat*norm
    #     print 'qcd yld/calcyld', qcdcalcyld, ttcalcyld
    #     QCDhist.Scale(qcdcalcyld)
    #     TThist.Scale(ttcalcyld)
    mc_pass.append(QCDhist)
    allmc.append(qcdvar)
    mc_pass.append(TThist)
    allmc.append(ttvar)
    print allmc
    print mc_pass
    dcut=dcut_num
    if v=='nrestops':
        dcut = dcut.replace('&& rescand_disc>'+topwp, '')
        dcut = dcut.replace(trigs['0lep'][year],'')
        dcut = dcut.replace('nbjets','nbjets_nosys')
        dcut = dcut.replace('njets','njets_nosys')
        dcut = dcut.replace(' ht',' ht_nosys')
        dv='nrestops_nosys'
        datahist_num = getDatahists(dv, dcut, '0lep_evt')
    elif v=='rescand_pt':
        datahist_num = getDatahists(v, dcut, '0lep')

    # datahist_den = getDatahists(v, dcut_den, channel)
    # mc_all   = getMChists(v, mcut_den, mcfiles, allmc)

    # datahist_num_mu = getDatahists(v, dcut_num_mu, channel)
    # datahist_den_mu = getDatahists(v, dcut_den_mu, channel)
    # datahist_num_el = getDatahists(v, dcut_num_el, '1el')
    # datahist_den_el = getDatahists(v, dcut_den_el, '1el')

    # datahist_num = datahist_num_mu.Clone("datahist_num")
    # datahist_num.Add(datahist_num_el)
    # datahist_den =datahist_den_mu.Clone("datahist_den")
    # datahist_den.Add(datahist_den_el)

    # mc_pass  = getMChists(v, mcut_num, mcfiles, allmc)
    # mc_all   = getMChists(v, mcut_den, mcfiles, allmc)

elif typ=='miss':
    if not gensub:
        mcut_num = mcut_num.replace("rescand_genmatch==0 &&","")
        mcut_den = mcut_den.replace("rescand_genmatch==0 &&","")        
    mchists_num  = getMChists(v, mcut_num, mcfiles, samplist)
    mchists_den  = getMChists(v, mcut_den, mcfiles, samplist)
    mc_pass  = getMChists(v, mcut_num, mcfiles, allmc)
    mc_all   = getMChists(v, mcut_den, mcfiles, allmc)

    if gensub:
        mcut_num_gen = mcut_num.replace("rescand_genmatch==0","rescand_genmatch==1")
        mcut_den_gen = mcut_den.replace("rescand_genmatch==0","rescand_genmatch==1")
        mcut_num_gen = mcut_num_gen.replace("*weight","*weight*-1.0")
        mcut_den_gen = mcut_den_gen.replace("*weight","*weight*-1.0")
        subhist_num = getMChists(v, mcut_num_gen, mcfiles, allmc)
        subhist_den = getMChists(v, mcut_den_gen, mcfiles, allmc)
        datahist_num = getDatahists(v, dcut_num, channel, sub=subhist_num)
        datahist_den = getDatahists(v, dcut_den, channel, sub=subhist_den)
    else:
        datahist_num = getDatahists(v, dcut_num, channel)
        datahist_den = getDatahists(v, dcut_den, channel)

elif typ=='eff':
    if not gensub:
        mcut_num = mcut_num.replace("rescand_genmatch==1 &&","")
        mcut_den = mcut_den.replace("rescand_genmatch==1 &&","")        
    elif gensub and byevt:
        mcut_num = mcut_num.replace("rescand_genmatch==1 &&","NgoodtagRes>=1 &&")
        mcut_den = mcut_den.replace("rescand_genmatch==1 &&","NgoodtagRes>=1 &&")        
    mchists_num  = getMChists(v, mcut_num, mcfiles, samplist)
    mchists_den  = getMChists(v, mcut_den, mcfiles, samplist)
    print allmc
    mc_pass  = getMChists(v, mcut_num, mcfiles, allmc)
    mc_all   = getMChists(v, mcut_den, mcfiles, allmc)

    if gensub :
        if not byevt:
            mcut_num_gen = mcut_num.replace("rescand_genmatch==1","rescand_genmatch==0")
            mcut_den_gen = mcut_den.replace("rescand_genmatch==1","rescand_genmatch==0")
        elif byevt:
            mcut_num_gen = mcut_num.replace("NgoodtagRes>=1","NgoodtagRes==0")
            mcut_den_gen = mcut_den.replace("NgoodtagRes>=1","NgoodtagRes==0")
        mcut_num_gen = mcut_num_gen.replace("*weight","*weight*-1.0")
        mcut_den_gen = mcut_den_gen.replace("*weight","*weight*-1.0")
        print 'SUBHIST:'
        subhist_num = getMChists(v, mcut_num_gen, mcfiles, ['MC'])
        subhist_den = getMChists(v, mcut_den_gen, mcfiles, ['MC'])
        datahist_num = getDatahists(v, dcut_num, channel, sub=subhist_num)
        datahist_den = getDatahists(v, dcut_den, channel, sub=subhist_den)
    elif not gensub:
        datahist_num = getDatahists(v, dcut_num, channel)
        datahist_den = getDatahists(v, dcut_den, channel)

if typ=='eff' or typ=='miss':
    #divide histograms:
    datahist = datahist_num.Clone("datahist")
    datahist.Divide(datahist_num, datahist_den)
    # mchists = []
    mchist = mchists_num[0].Clone("mchist")
    if len(mchists_num)>1:
        print 'ERROR should only have one MC histogram'
    mchist.Divide(mchists_num[0], mchists_den[0])
    #clopper pearson
    CPvals_b = []; CPvals_d = []
    print 'clopper pearson errors:'
    for ibin in range(0,mchist.GetNbinsX()+2):            
        # print 'ibin', ibin
        #background
        npass_b = mchists_num[0].GetBinContent(ibin) #should be int not float
        ntotal_b = mchists_den[0].GetBinContent(ibin)
        # print 'MC',ntotal_b, npass_b, mchist[ibin]
        CPval_b = r.TEfficiency.ClopperPearson(abs(ntotal_b), abs(npass_b), 0.683, True) - abs(mchist[ibin]) #sometimes has negative bin vals
        CPvals_b.append(CPval_b)
        #data
        npass_d = datahist_num.GetBinContent(ibin)
        ntotal_d = datahist_den.GetBinContent(ibin)
        # print 'data',ntotal_d, npass_d, datahist[ibin]
        CPval_d = r.TEfficiency.ClopperPearson(ntotal_d, npass_d, 0.683, True) - datahist[ibin]
        CPvals_d.append(CPval_d)
    
    # print mchist, len(mchist)
    sfhist, sferrs = makeControlHist(v, datahist,    [mchist], samplist, typ, cperr=[CPvals_b,CPvals_d])

    sfs=[1.0]; sfsup=[1.0]; sfsdown=[1.0]
    for ibin in range(2, sfhist.GetNbinsX()+2):
        sfs.append(sfhist.GetBinContent(ibin))
        sfsup.append(sfhist.GetBinContent(ibin)+sferrs[ibin])
        sfsdown.append(sfhist.GetBinContent(ibin)-sferrs[ibin])
    print edges, len(edges)
    print sfs, len(sfs), '\n'
    print sfsup, len(sfs), '\n'
    print sfsdown, len(sfs), '\n'
    
# h1=makeControlHist(v, datahist_num, mchists_num, samplist, 'mcpass')#, logy=True)
# h2=makeControlHist(v, datahist_den, mchists_num, samplist, 'mcall')#, logy=True)
if typ =='misval' or typ=='effval':
    h1=makeControlHist(v, datahist_num, mc_pass, ['minormc', 'DDBKG'], 'mcpass')#, logy=True)

else:
    h1=makeControlHist(v, datahist_num, mc_pass, allmc, 'mcpass')#, logy=True)
    h2=makeControlHist(v, datahist_den, mc_all, allmc, 'mcall')#, logy=True)
