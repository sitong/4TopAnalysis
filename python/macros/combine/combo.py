import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import root_numpy as rn
# import re
import ROOT as r
import scipy.integrate as integrate
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

infile = './TTTTMC_SR/TTTTMC_SR_2018.root'
outname = './TTTTMC_SR/TTTTMC_SR_2018_combined.root'

names = ['cut0bin0_2018_TTTT',
         'cut0bin1_2018_TTTT',
         'cut0bin2_2018_TTTT',
         'cut0bin3_2018_TTTT',
         'cut0bin4_2018_TTTT',
         'cut0bin5_2018_TTTT',
         'cut0bin6_2018_TTTT',
         'cut0bin7_2018_TTTT',
         'cut1bin0_2018_TTTT',
         'cut1bin1_2018_TTTT',
         'cut2bin0_2018_TTTT',
         'cut2bin1_2018_TTTT']

infile = r.TFile(infile)
outfile = r.TFile(outname, 'UPDATE')

keep={}
for histname in names:
    histname_up = histname+'_'+histname+'_shapesystUp'
    histname_down = histname+'_'+histname+'_shapesystDown'
    
    print 'adding hist:', histname 
    NAFhist      = infile.Get(histname)
    NAFhist_up   = infile.Get(histname_up)
    NAFhist_down = infile.Get(histname_down)
    
    if (histname == 'cut2bin0_2018_TTTT' or histname=='cut2bin1_2018_TTTT'):
        keep[histname] = NAFhist
        keep[histname_up] = NAFhist_up
        keep[histname_down] = NAFhist_down
    else:
        NAFhist.Write()
        NAFhist_up.Write()
        NAFhist_down.Write()

combohist = keep['cut2bin0_2018_TTTT']
combohist_up = keep['cut2bin0_2018_TTTT_cut2bin0_2018_TTTT_shapesystUp']
combohist_down = keep['cut2bin0_2018_TTTT_cut2bin0_2018_TTTT_shapesystDown']
combohist.Add(keep['cut2bin0_2018_TTTT'])
combohist_up.Add(keep['cut2bin0_2018_TTTT_cut2bin0_2018_TTTT_shapesystUp'])
combohist_down.Add(keep['cut2bin0_2018_TTTT_cut2bin0_2018_TTTT_shapesystDown'])

combohist.Write()
combohist_up.Write()
combohist_down.Write()

outfile.Close()
del outfile

        
