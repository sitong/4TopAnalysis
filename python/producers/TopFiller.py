#for restop mva, deepresolved testing
import os
# import resource
import numpy as np
import ROOT
import math
from pprint import pprint
import itertools
from collections import OrderedDict
ROOT.PyConfig.IgnoreCommandLineOptions = True
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys
# import PhysicsTools.NanoTrees.helpers.LeptonHelper as lepsel
import PhysicsTools.NanoTrees.helpers.weighthelper as xw
# import PhysicsTools.NanoTrees.helpers.GenPartHelper as GenHelp

from PhysicsTools.NanoTrees.helpers.JetHelper import JetHelper
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest

from PhysicsTools.NanoTrees.helpers.ConfigHelper import ResSetter, JetSetter, BoostSetter, LepSetter
from PhysicsTools.NanoTrees.helpers.ResTopHelper import ResTopHelper
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper

import logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')

#class definition
class TopFiller(Module):
    def __init__(self, channel, year, tagtype='nano'):        
        self.channel = channel
        self.tagtype = tagtype
        self.year = year
        self.verbose         = False
        self.getgentops      = True
        self.outputdiscs     = False
        self.LS = LepSetter(self.channel, self.year)
        self.JS = JetSetter(self.channel, self.year)
        self.BS = BoostSetter(self.year)
        self.genhelp = genparthelper()
        #mva stuff
        self.th = ResTopHelper(self.JS.btype, self.tagtype, self.year)

    def beginJob(self):
        #general settings
        pass

    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        # self.baseTrees(inputFile, outputFile, inputTree, wrappedOutputTree)
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree
        self.isMC = bool(inputTree.GetBranch('genWeight'))
        if not self.isMC:
            self.getgentops=False
        print 'initializing trees...'
        # #extra restop things
        self.out.branch("nrestops", "I")
        self.out.branch("restop1pt", "F")
        self.out.branch("restop2pt", "F")
        self.out.branch("restop3pt", "F")
        self.out.branch("restop4pt", "F")
        #boosted tops
        self.out.branch("nbstops", "I")
        self.out.branch("nbsws", "I")
        self.out.branch("bstop1_pt", "F")
        self.out.branch("bstop2_pt", "F")
        self.out.branch("bstop3_pt", "F")
        self.out.branch("sfjm", "F")

        # self.out.branch("NoCandFlag", "I")

        if self.outputdiscs:
            self.out.branch("discs", "F", lenVar="nJet") #will be unnecassarily long but there you go

        if self.getgentops:
        # #gen info
        #     self.out.branch("restop_genmatch", "I", lenVar="nJet")
            self.out.branch("NgoodtagRes", "I")
            self.out.branch("NmistagRes", "I")

    # def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
    #     pass

    def getsf(self, candpt, typ, prevSF):

        SFs={'miss':{'2016':[1.0, 1.3486641645431519, 1.148268222808838, 1.1212979555130005, 1.1519887447357178, 1.1012041568756104, 1.0909525156021118, 1.157222867012024, 1.0504330396652222, 0.9640796184539795, 1.0218476057052612, 1.0670303106307983],
                     '2017':[1.0, 1.1454821825027466, 1.1085493564605713, 1.0633724927902222, 1.1575764417648315, 1.0952008962631226, 1.0599652528762817, 1.0429214239120483, 0.9864946007728577, 1.0190281867980957, 1.0380607843399048, 0.9544106721878052],
                     '2018':[1.0, 1.1755003929138184, 1.1824177503585815, 1.0982625484466553, 1.041006088256836, 1.01741361618042, 0.9709364771842957, 0.9495097398757935, 0.9101343750953674, 0.9283883571624756, 0.8783420920372009, 0.9578810334205627]},
             'missup':{'2016':[1.0, 1.3791774626191824, 1.1768247718344196, 1.158585114334322, 1.185709153956627, 1.1304812752262605, 1.117889968640206, 1.1858634363653475, 1.0789487490559106, 0.996885855710745, 1.072218987995334, 1.1081695452320304],
                       '2017':[1.0, 1.1694756793685461, 1.1348150005214854, 1.0965332127832468, 1.189721536701208, 1.1235193959433603, 1.0856284257790452, 1.0681161377023558, 1.0128724939412128, 1.0550202280899128, 1.0907042937905038, 0.99106013040378],
                       '2018':[1.0, 1.2002815413265353, 1.210227410426209, 1.1326789409436975, 1.0695533798823385, 1.0425687424984358, 0.9929995956075807, 0.9709379258545188, 0.9328702493828099, 0.9569627565310543, 0.9173672289005572, 0.990311514623479]},
             'missdown':{'2016':[1.0, 1.3181508664671213, 1.1197116737832562, 1.084010796691679, 1.1182683355148086, 1.0719270385249602, 1.0640150625640177, 1.1285822976587003, 1.0219173302745337, 0.931273381197214, 0.9714762234151884, 1.0258910760295663],
                         '2017':[1.0, 1.121488685636947, 1.0822837123996571, 1.0302117727971976, 1.1254313468284551, 1.0668823965828849, 1.0343020799735183, 1.0177267101217409, 0.9601167076045025, 0.9830361455062787, 0.9854172748893057, 0.9177612139718303],
                         '2018':[1.0, 1.1507192445011014, 1.1546080902909541, 1.063846155949613, 1.0124587966313334, 0.9922584898624041, 0.9488733587610106, 0.9280815538970681, 0.8873985008079249, 0.8998139577938968, 0.8393169551738446, 0.9254505522176465]},
             'eff':{'2016':[1.044, 0.955, 0.978],
                    '2017':[0.916, 0.99, 0.866],
                    '2018':[0.845, 0.858, 0.954]},
             'effup':{'2016':[1.096, 0.991, 1.035],
                      '2017':[0.959, 1.05, 0.948],
                      '2018':[0.881, 0.889, 0.999]},
             'effdown':{'2016':[0.992, 0.919, 0.921],
                        '2017':[0.873, 0.93, 0.784],
                        '2018':[0.809, 0.827, 0.909]}
         }

        sf = [1.0,1.0,1.0]
        if typ=='eff':
            sflist = SFs['eff'][self.year]
            sflist_up = SFs['effup'][self.year]
            sflist_down = SFs['effdown'][self.year]
            if candpt >=100.0 and candpt<300.0:
                sf[0]=sflist[0]; sf[1]=sflist_up[0]; sf[2]=sflist_down[0]
            elif candpt >=300.0 and candpt<500.0:
                sf[0]=sflist[1]; sf[1]=sflist_up[1]; sf[2]=sflist_down[1]
            elif candpt >=500.0:
                sf[0]=sflist[2]; sf[1]=sflist_up[2]; sf[2]=sflist_down[2]

        elif typ=='miss':
            sflist = SFs['miss'][self.year]
            sflist_up = SFs['missup'][self.year]
            sflist_down = SFs['missdown'][self.year]
            if candpt >=100.0 and candpt<200.0:
                sf[0]=sflist[1]; sf[1]=sflist_up[1]; sf[2]=sflist_down[1]
            elif candpt >=200.0 and candpt<300.0:
                sf[0]=sflist[2]; sf[1]=sflist_up[2]; sf[2]=sflist_down[2]
            elif candpt >=300.0 and candpt<350.0:
                sf[0]=sflist[3]; sf[1]=sflist_up[3]; sf[2]=sflist_down[3]
            elif candpt >=350.0 and candpt<400.0:
                sf[0]=sflist[4]; sf[1]=sflist_up[4]; sf[2]=sflist_down[4]
            elif candpt >=400.0 and candpt<450.0:
                sf[0]=sflist[5]; sf[1]=sflist_up[5]; sf[2]=sflist_down[5]
            elif candpt >=450.0 and candpt<500.0:
                sf[0]=sflist[6]; sf[1]=sflist_up[6]; sf[2]=sflist_down[6]
            elif candpt >=500.0 and candpt<550.0:
                sf[0]=sflist[7]; sf[1]=sflist_up[7]; sf[2]=sflist_down[7]
            elif candpt >=550.0 and candpt<600.0:
                sf[0]=sflist[8]; sf[1]=sflist_up[8]; sf[2]=sflist_down[8]
            elif candpt >=600.0 and candpt<650.0:
                sf[0]=sflist[9]; sf[1]=sflist_up[9]; sf[2]=sflist_down[9]
            elif candpt >=650.0 and candpt<700.0:
                sf[0]=sflist[10]; sf[1]=sflist_up[10]; sf[2]=sflist_down[10]
            elif candpt >=700.0:
                sf[0]=sflist[11]; sf[1]=sflist_up[11]; sf[2]=sflist_down[11]

        outSF = np.multiply(np.array(sf), np.array(prevSF))
        # if 1.0 not in prevSF:
        #     print 'prevsf',prevSF
        # print 'sf',sf
        # print 'outsf',outSF
        return outSF

    # put it all together!
    def TopGetter(self, event, jets=None, BSTops=None, BSWs=None, isMC=True):
        #uses collections from BaseProducer: event.cleanjets, event.ak4jets,  #event.boostedTops, #event.BSWs
        ak4jets=jets

        if jets is None:
            ak4jets   = Collection(event,"Jet")
            ak4jets, bjets  = self.JS.getJets(self.year, event, ak4jets, getvars=False)

        if self.getgentops or isMC:
            ak4jets = self.genhelp.AddJetIdx(ak4jets)         #add indices to jets

        cleanjets=ak4jets
        # if self.LS.clean_jetsvleps:
        #     ak4jets, SelEls, SelMus, SelLeps = self.LS.getLeps(event)

        #get BDT resolved top candidates 
        ResTops = []; discs = []#; rtdicts = []
        HaveCands, TopCands  = self.th.getrescands(cleanjets)
        # print HaveCands, TopCands
        if not HaveCands:
            self.th._fillBDTvars(HaveCands, None)
        elif HaveCands:
            for tc in TopCands: #looping over topcands
                isgood, GoodCand, disc, candDict = self.th._fillBDTvars(HaveCands,tc)
                # if self.verbose: print 'disc:', disc
                if isgood:
                    ResTops.append(GoodCand); discs.append(disc)#; rtdicts.append(candDict)
        nocand = 0

        #check that resolved tops are distinct!:
        ResTops = np.array(ResTops); discs = np.array(discs)
        if len(ResTops)>0:
            ResTops = self.th.checkResCandOverlap(ResTops, discs)
        else: nocand = 1

        sfjm = 0.0
        if BSTops is None or BSWs is None:
            BSTops, BSWs, sfjm = self.BS.getBoosts(event, FatJets=None, getvars=True)

        if len(ResTops)>0:
            BSTops, BSWs, ResTops = self.th.CleanResvsBoost(BSTops, BSWs, ResTops)

        EffSF=[1.0,1.0,1.0]; MissSF=[1.0,1.0,1.0]
        NgoodtagRes = 0; NmistagRes = 0
        
        if not isMC:
            topdict = {'BSTops':BSTops,
                       'BSWs':BSWs,
                       'ResTops':ResTops,
                       'discs':discs,
                       'sfjm':sfjm,
                       'missSF':MissSF,
                       'effSF':EffSF,
                       'Ngood':NgoodtagRes,
                       'Nbad':NmistagRes}
            return topdict

        elif self.getgentops or isMC:
            # check gen matching of tops        
            matcharr = np.zeros(len(ResTops), dtype=int)
            for i, (tc, m) in enumerate(zip(ResTops, matcharr)):
                matched = self.genhelp.GenMatchTops(event, tc, True)
                if matched:
                    NgoodtagRes+=1
                    matcharr[i]=1 #reassign 0 to 1
                    EffSF=self.getsf(tc[0].Pt(), 'eff', EffSF)
                else: 
                    NmistagRes+=1
                    MissSF=self.getsf(tc[0].Pt(), 'miss', MissSF)
            
        topdict = {'BSTops':BSTops,
                   'BSWs':BSWs,
                   'ResTops':ResTops,
                   'discs':discs,
                   'sfjm':sfjm,
                   'missSF':MissSF,
                   'effSF':EffSF,
                   'Ngood':NgoodtagRes,
                   'Nbad':NmistagRes,
               }

        return topdict

    def TopFiller(self, event, td, returntopinfo=False):

        BSTops = td['BSTops']; ResTops = td['ResTops']; BSWs = td['BSWs']; sfjm = td['sfjm']; discs = td['discs']
        Ngood = td['Ngood']; Nbad = td['Nbad']
        #effSF = ['effSF']; missSF = ['missSF']

        #fatjet/ boosted stuff
        nbstops = len(BSTops)
        nbsws   = len(BSWs)
        bt1pt=0.0; bt2pt=0.0; bt3pt = 0.0
        if len(BSTops)>0:
            bt1pt = BSTops[0].pt
        elif len(BSTops)>1:
            bt2pt = BSTops[1].pt
        elif len(BSTops)>2:
            bt3pt = BSTops[2].pt

        tpts = np.zeros(4)
        if self.outputdiscs:
            tWP = self.th.restopWP()
            ntops = 0
            if len(ResTops)>0:
                discs = ResTops[:,4]
                discs = list(discs)
                toppts = []      
                for t in ResTops:
                    if t[4]> tWP: #disc passes thr
                        ntops+=1
                        toppts.append(t[0].Pt())
                # toppts = [t[0].Pt() for t in ResTops]
                #order by highest to lowest top pt
                if toppts:
                    toppts  = np.sort(np.array(toppts))[::-1] 
                # discs   = [discs[i] for i in topidx] #not ordering discs
                # if self.getgentops:
                #     topidx  = np.argsort(np.array(toppts))[::-1] 
                #     matcharr = [matcharr[i] for i in topidx] #same order

                for i, pt in enumerate(toppts):
                    tpts[i]=pt

        else:
            ntops = len(ResTops) #unfiltered
            if ntops>0:
                discs = ResTops[:,4]
                #toppts = ResTops[:,0]
                toppts = [t[0].Pt() for t in ResTops]
            #order by highest to lowest top pt
                toppts  = np.sort(np.array(toppts))[::-1] 
                topidx  = np.argsort(np.array(toppts))[::-1] 
                discs   = [discs[i] for i in topidx]
                # if self.getgentops:
                #     matcharr = [matcharr[i] for i in topidx] #same order

                for i, pt in enumerate(toppts):
                    tpts[i]=pt

        pad = [-1.0]*(event.nJet - len(discs))
        disclist=pad
        if len(discs)>0:
            disclist = discs + disclist
        if self.verbose: print 'nrestops', ntops, 'restop_genmatch', matcharr
        if self.verbose: print 'discs',discs
        # print 'ntops', ntops, 'tpts', tpts, 'discs', discs
        # print disclist

        self.out.fillBranch("nrestops",        ntops)
        if self.outputdiscs:
            self.out.fillBranch('discs',           list(disclist))
        self.out.fillBranch("restop1pt",       tpts[0])
        self.out.fillBranch("restop2pt",       tpts[1])
        self.out.fillBranch("restop3pt",       tpts[2])
        self.out.fillBranch("restop4pt",       tpts[3])

        self.out.fillBranch("nbstops",          nbstops)
        self.out.fillBranch("nbsws",            nbsws)
        self.out.fillBranch("bstop1_pt",        bt1pt)
        self.out.fillBranch("bstop2_pt",        bt1pt)
        self.out.fillBranch("bstop3_pt",        bt1pt)
        self.out.fillBranch("sfjm",             sfjm)

        if self.getgentops:
            # if len(matcharr)>0:
            #     matcharr = matcharr + ([0]*(event.nJet - len(matcharr))) #padding
            # self.out.fillBranch('restop_genmatch', list(matcharr))
            self.out.fillBranch("NgoodtagRes",     Ngood)
            self.out.fillBranch("NmistagRes",      Nmiss)
        
        #delete lists
        del ResTops; del BSTops; del BSWs; del disclist; del pad; del tpts
        # if self.getgentops: del matcharr
        # del ak4jets; del cleanjets;
        if returntopinfo:
            return ntops, tpts
        # self.out.fillBranch("NoCandFlag",      nocand)
            
    def analyze(self, event):
        if self.verbose:         print 'EVENT #', event.event, '\n'
        topdict = self.TopGetter(event)
        self.TopFiller(event, topdict)   #resolved tops
        return True

TopTrees2016_0lep = lambda : TopFiller('0lep','2016')
TopTrees2016_1lep = lambda : TopFiller('1lep','2016')

TopTrees2017_0lep = lambda : TopFiller('0lep','2017')
TopTrees2017_1lep = lambda : TopFiller('1lep','2017')

TopTrees2018_0lep = lambda : TopFiller('0lep','2018')
TopTrees2018_1lep = lambda : TopFiller('1lep','2018')

