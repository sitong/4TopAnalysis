# easier plotting
import os
import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import root_numpy as rn
# import re
import ROOT as r
import ClosurePlots as cp
import SelDict as sels
# import selections as sels
import RootStuff as rs
import scipy.integrate as integrate
import plottery as ply
import argparse
import sys
from ROOT import gStyle
gStyle.SetOptStat(0)
sys.path.append('../combine/')
from datacard_dictionary import datacard_dict

parser = argparse.ArgumentParser(description='plotting')
parser.add_argument("-v", "--verbose", dest="verbose", default='verbose')
parser.add_argument("-b", "--base", dest="base", default='VR')
parser.add_argument("-t", "--plottype", dest="plottype", default='BOTHht')
parser.add_argument("-l", "--logy", dest="logy", default='True')
parser.add_argument("-y", "--year", dest="year", default='2016')
args = parser.parse_args()

class plotmaster:
#class that plots things nicely with plottery
    def __init__(self, args):

        self.verbose = args.vebose
        self.logy = args.logy
        self.base = args.base
        self.year = args.year
        self.ptyp = args.plottype

        self.varstoplot = ['catBDTdisc']
        self.mcsamplist = ["TTTT", "TTX", "minor"]
        self.treename  = 'Events'
        self.colors    = colors = [r.kRed-7, r.kOrange+1, r.kYellow-7, r.kGreen-9, r.kCyan-7, r.kAzure+1, r.kBlue-9, r.kViolet-4, r.kMagenta-9]
        self.outdir    = './plots/hists'+args.base+'/'

    def getyield(self, identifier, hist, verbose=True):
        errorVal = r.Double(0)
        minbin=0
        maxbin=hist.GetNbinsX()+1
        hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
        if verbose:
            print identifier, 'yield:', round(hyield, 3), round(errorVal, 3), '\n'
        return hyield,  errorVal

    def gethist(self, filename, projvar, cut, nbins, selrange, treename='Events'):
        rf, t = rs.makeroot(filename, treename)
        hist = rs.gethist(t, projvar, cut, nbins, selrange)
        rs.fixref(rf, hist)
        return hist

    def makeHists(self, identifier, plotvar, data, hists, labels):
    
        outname = self.outdir+identifier+'_'+year+'_'+plotvar+'.png'
        print outname
        print 'log?',self.logy
        print 'data',data
        print 'hists',hists
        print 'num bgs', len(hists)
        print 'colors',self.colors
        print 'labels',labels
        ply.plot_hist( 
            data = data,
            bgs  = hists,
            # sigs = mydata,
            # syst = hsyst,
            # sig_labels = labels,#["el", "mu"],
            colors = self.colors, 
            legend_labels = labels,
            options = {
                "clopper_pearson":[],
                "do_stack": True,
                "xaxis_label": plotvar,
                "yaxis_label": "Events",
                "legend_scalex": 0.7,
                "legend_scaley": 1.2,
                "bkg_sort_method":'ascending',
                # "extra_text": [plotvar],
                "yaxis_log": logy,
                "ratio_range":[0.0,2.0],
                # "ratio_pull": True,
                "hist_disable_xerrors": False,
                "show_bkg_errors": True,
                "bkg_err_fill_style":3003,
                "bkg_err_fill_color":r.kGray,
                # "palette_name": "pastel",
                "yaxis_moreloglabels": self.logy, 
                "legend_percentageinbox": False,
                "legend_alignment": "top right",
                "cms_label": "Preliminary",
                # "lumi_value": 36,
                "legend_smart": False,
                # "yaxis_range":[0,10],
                "output_name": outname,
            }
        )

    def getclosurecuts(self, cr, form='DATA', extracuts=' ', verbose=False):
        cuts = {}
        sd = sels.seldict
        if cr=='closure': #D=BC/A
            B = '('+extracuts + sd['BASE']['def']+sd[self.base]['B']+')'+sd[form][self.year]
            C = '('+extracuts + sd['BASE']['def']+sd[self.base]['C']+')'+sd[form][self.year]
            A = '('+extracuts + sd['BASE']['def']+sd[self.base]['A']+')'+sd[form][self.year]
            cuts = {'B':B, 'C':C, 'A':A}

        elif cr=='extclosure': #D=(BC/A)^2(A'/BC')
            B = '('+extracuts + sd['BASE']['def']+sd[self.base]['B']+')'+sd[form][self.year]
            C = '('+extracuts + sd['BASE']['def']+sd[self.base]['C']+')'+sd[form][self.year]
            A = '('+extracuts + sd['BASE']['def']+sd[self.base]['A']+')'+sd[form][self.year]
            Cp = '('+extracuts + sd['BASE']['def']+sd[self.base]['Cp']+')'+sd[form][self.year]
            Ap = '('+extracuts + sd['BASE']['def']+sd[self.base]['Ap']+')'+sd[form][self.year]
            cuts = {'B':B, 'C':C, 'A':A, 'Ap':Ap, 'Cp':Cp}

        else: print 'ERROR: not a valid closure region'
        if verbose: print cuts 
        return cuts


    def getClosure(projvar, cr, channel, sampfile, treename='Events', lumi='35.9', year='2016', cutdict=None, setnbins=None, setselrange=None, returnhists=False):
        # print 'channel:', channel
        rtbool = False
        if (projvar=='restop1pt'): rtbool=True
        
        form = 'mc'
        if 'data' in cr:
            form = 'data'

        #if aren't given cut dict as input, get them yourself:
        if cutdict is None:
            print 'getting cuts...'
            cuts = getclosurecuts(cr, form, channel, year, lumi, rtbool, sels.seldict[cr])
        else:
            print 'given cut dictionary as input!'
            cuts = cutdict

        # if projvar=='restop1pt': #no underflow
        #     cut = sels.getcut(cr, format=form, chan=channel, year=year, lumi=lumi, rtcut=True) 

        nbins, selrange = sels.getHistSettings(projvar)
        if setnbins is not None:
            nbins = setnbins
        if setselrange is not None:
            selrange=setselrange
        Dhist = r.TH1F('Dhist', 'Dhist', nbins, selrange[0], selrange[1])
        # for i,f in enumerate(samplist): #loop through samplist
        print sampfile
        rf, t = rs.makeroot(sampfile, treename)
        closurehists = {}
        closurestats = {}
        closureroothists = {}
        for key, cut in cuts.iteritems(): #loop over cut dictionaries, get histograms
            statsarr=[]# for statistical uncertainties
            hist = rs.gethist(t, projvar, cut, nbins, selrange)
            histarr = rn.hist2array(hist, include_overflow=True)
            #get bin errors
            for ibin in range(0, hist.GetNbinsX()+2): 
                statsarr.append(hist.GetBinError(ibin)) 
            closurehists[key]=histarr
            closurestats[key]=statsarr#these are the statistical errors per bin in each CR hist
            if returnhists:
                closureroothists[key] = hist
            hist.SetDirectory(0)

        # divide bins. bin number should be the same
        if cr=='closure' or cr=='dataclosure': #D=BC/A
            print 'A', closurehists['A']
            print 'B', closurehists['B']
            print 'C', closurehists['C']
            D = np.divide(np.multiply(closurehists['B'], closurehists['C']), closurehists['A'])
            D[np.isnan(D)] = 0.0
            D[np.isinf(D)] = 0.0
            print D
            Dhist = rn.array2hist(D, Dhist)

        elif cr=='extclosure' or cr=='dataextclosure': #D=(BC/A)*(BC/A)*(A'/BC')
            D1 = np.divide(np.multiply(closurehists['B'], closurehists['C']), closurehists['A'])
            D2 = np.divide(closurehists['Ap'], np.multiply(closurehists['B'],closurehists['Cp']))
            D1sq = np.multiply(D1, D1)
            D = np.multiply(D1sq, D2)
            # D = np.multiply( np.exp2(np.divide(np.multiply(closurehists['B'],closurehists['C']), closurehists['A'])), np.divide(closurehists['Ap'], np.multiply(closurehists['B'],closurehists['Cp'])))
            D[np.isnan(D)] = 0.0
            D[np.isinf(D)] = 0.0
            print D
            
            #deal with statistical uncertainties
            print 'Dshape', D.shape, len(D)

            #staterrorprop by bin...
            #(dA/A)^2 = (dA)^2/A^2
            Asqerr = np.divide(np.square(closurestats['A']),np.square(closurehists['A'])) 
            Bsqerr = np.divide(np.square(closurestats['B']),np.square(closurehists['B']))
            Csqerr = np.divide(np.square(closurestats['C']),np.square(closurehists['C'])) 
            Apsqerr = np.divide(np.square(closurestats['Ap']),np.square(closurehists['Ap']))
            Cpsqerr = np.divide(np.square(closurestats['Cp']),np.square(closurehists['Cp']))
            #if X= BC/A and Y=Ap/BCp then dD = D* sqrt( 2(dX/X)^2 + (dY/Y)^2) )
            
            #(dX/X)^2 = Dstats1 = (dB/B)^2 + (dC/C)^2 + (dA/A)^2
            #(dY/Y)^2 = Dstats2 = (dAp/Ap)^2 + (dB/B)^2 + (dCp/Cp)^2
            Dstats1 = np.add(np.add(Bsqerr,Csqerr),Asqerr) #sqrt( (dB/B)^2 + (dC/C)^2 + (dA/A)^2)^2 
            Dstats2 = np.add(Apsqerr, np.add(Bsqerr,Cpsqerr)) #sqrt( (dAp/Ap)^2 + (dB/B)^2 + (dCp/Cp)^2)^2 
            Dstats = np.multiply( D, np.sqrt( np.add( (2.0*Dstats1) ,Dstats2) ) ) #D* sqrt( 2(dX/X)^2 + (dY/Y)^2) )
            Dstats[np.isnan(Dstats)] = 0.0
            Dstats[np.isinf(Dstats)] = 0.0
            print Dstats

            Dhist = rn.array2hist(D, Dhist, errors=Dstats)
        

        rf.Close()
        if not returnhists:
            return Dhist
        elif returnhists:
            return Dhist, closureroothists
