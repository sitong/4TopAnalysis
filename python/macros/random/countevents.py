import subprocess
import os
import argparse
import sys
import math
import numpy as np
import ROOT as r

parser = argparse.ArgumentParser(description='count nevts') 
parser.add_argument("-i", "--indir", dest="indir", default='/eos/uscms//store/user/lpcstop/noreplica/mequinna/NanoAODv6/2016/DATA/test/') 
args = parser.parse_args()


def makeroot(infile, treename='Events', options="read"):
    rfile = r.TFile(infile, options)
    tree = rfile.Get(treename)
    return rfile, tree

totnevts = 0

for subdir, dirs, files in os.walk(args.indir):
    for file in files:
        f=subdir+'/'+file
        rf, tree = makeroot(f)
        nents = tree.GetEntries()
        totnevts+=nents
        print 'file:',f, 'nents', nents
print 'total # events:',totnevts
