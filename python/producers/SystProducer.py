import os
import math
import json
import logging
import numpy as np
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoTrees.helpers.GenPartHelper import genparthelper
from PhysicsTools.NanoTrees.helpers.ConfigHelper import LepSetter
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi, deltaR, closest
from PhysicsTools.NanoTrees.helpers.ConfigHelper import BoostSetter, JetSetter
# from PhysicsTools.NanoTrees.helpers.TriggerHelper import TriggerHelper
# from PhysicsTools.NanoTrees.helpers.bSFcorrHelper import bSFcorrHelper
from PhysicsTools.NanoTrees.producers.TopFiller import TopFiller
from PhysicsTools.NanoTrees.helpers.DeepAK8SFhelper import GetBoostSFs
# from PhysicsTools.NanoTrees.helpers.BDThelper import BDThelper
import PhysicsTools.NanoTrees.helpers.PhysicsHelper as phys

#class definition
class SystProducer(Module):
    def __init__(self, channel, year, reducedset=False):
        #does nothing if data, assumes mc
        self.reducedset=reducedset
        self.verbose = False
        self.year = year
        self.channel = channel
        # #for trigger
        # if year != '2017':
        #     tf = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/TriggerSFFiles/'+year+'v6_2D.root')
        #     trigfile = ROOT.TFile(tf)
        #     trighist = trigfile.Get("h2_eff")
        # elif year == '2017':
        #     tf = os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/TriggerSFFiles/effSFs_2017fix.root')
        #     trigfile = ROOT.TFile(tf)
        #     trighist = trigfile.Get("ht_eff")
        # # trighist.SetDirectory(0)
        # self.trighist = trighist
        # trigfile.Close()
        # #for bSFcorr
        # self.corrhistfile = ROOT.TFile(os.path.expandvars('$CMSSW_BASE/src/PhysicsTools/NanoTrees/data/bSFcorrFiles/bSFcorrections.root'))

        metname = "MET"
        if self.year =='2017':
            metname = "METFixEE2017"
        metsysname = metname+"_T1"
        self.metname = metname; self.metsysname=metname+"_T1"

        self.outvarlist = {'btagSF':'F',
                           'bsWSF':'F',
                           'bsTopSF':'F',
                           'RTeffSF':'F',
                           'RTmissSF':'F'}#,
                           # 'trigSF': 'F',
                           # 'bSFcorr': 'F'}
        
                           # 'catBDTdisc':'F',
                           # 'nleptons':'I',
        
        self.bdtvars  = {'nrestops':'I',  #list of variables to make systematics of and info on their type
                         'ht':'F', 
                         'nbstops':'I',
                         'njets':'I',
                         'nbjets':'I',
                         'met'        :'F',
                         'nbsws'      :'I',
                         'metovsqrtht':'F',
                         'sfjm'       :'F',
                         'restop1pt'  :'F',
                         'sphericity' :'F',
                         'aplanarity' :'F',
                         'leadbpt'    :'F',
                         'bjht'       :'F',
                         'dphij1j2'   :'F',
                         'dphib1b2'   :'F',
                         'detaj1j2'   :'F',
                         'detab1b2'   :'F',
                         'jet6pt'     :'F',
                         'jet7pt'     :'F',
                         'meanbdisc'  :'F',
                         'htratio'    :'F',
                         'centrality' :'F'}
        #list of systematics to run
        # self.systlist = ['nosys']
        self.systlist = ['nosys', 'jerUp', 'jerDown', 'jesUp', 'jesDown', 'btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down', 'DeepAK8TopSF_Up', 'DeepAK8TopSF_Down', 'DeepAK8WSF_Up', 'DeepAK8WSF_Down', 'ResTopEff_Up', 'ResTopEff_Down', 'ResTopMiss_Up', 'ResTopMiss_Down']#'trigSFUp', 'trigSFDown'

    def beginJob(self):
        self.JS = JetSetter(self.channel, self.year) #jet settings
        self.BS = BoostSetter(self.year) #jet settings
        self.TF = TopFiller(self.channel, self.year) #tops and boosted tops
        # self.BDT = BDThelper("SR")
        
    def endJob(self):
        pass

    # declare tree branches
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.isMC = bool(inputTree.GetBranch('genWeight'))
        # self.th = TriggerHelper(self.trighist, self.isMC)# trigger SFs
        # self.bch = bSFcorrHelper(inputFile, self.year, self.corrhistfile, self.isMC) #bSFcorr factors
        self.infile = inputFile
        self.intree = inputTree
        self.out = wrappedOutputTree

        if not self.isMC:
            print 'no systematics: is data'
            self.systlist=['nosys'] #dont calculate systematic variations for data 
        # else: print 'is mc, will calc systematics'
        
        elif self.isMC and self.reducedset: #other MC
            self.systlist = ['nosys']#, 'jerUp', 'jerDown', 'jesUp', 'jesDown']

        print self.systlist            
        for outvar, outtype in self.outvarlist.iteritems():
            for syst in self.systlist:
                branchname = outvar+'_'+syst
                self.out.branch(branchname, outtype)

        for outvar, outtype in self.bdtvars.iteritems():
            for syst in self.systlist:
                if syst=='jerUp' or syst=='jesUp' or syst=='jerDown' or syst=='jesDown' or syst=='nosys':
                    branchname = outvar+'_'+syst
                    self.out.branch(branchname, outtype)


    #jet selection from ConfigHelpter and jethelper
    def cleanJets(self, jet, jetpt, minJet_pt, maxJet_eta, usejetid=True, year='2016'):
        if (jetpt<minJet_pt or abs(jet.eta)>=maxJet_eta):
            return False
        if usejetid:
            if (year=='2016' and jet.jetId<1): # loose
                return False
            elif ((year=='2017'or year=='2018') and jet.jetId<2): # tight
                return False
        return True

    def cleanBJets(self, jet, jetpt, bWP, minBJet_pt, maxBJet_eta):
        if jet.btagDeepFlavB <= bWP:
            return False
        if (jetpt<minBJet_pt or abs(jet.eta)>=maxBJet_eta):
            return False
        return True

    def getSystJets(self, year, jets, JETPTS, JETMS, JETBSFS=None):
        #returns jet variables and SYSTEMATIC SPECIFIC selected jets and bjets
        seljets  = []
        selbjets = []

        seljet_pts  = []
        seljet_SFs = []
        njets=0; nbjets=0; ht = 0.0; bjht=0.0; leadbpt=0.0
        sumE=0.0; centrality=0.0; meanbdisc=0.0; sumbdisc=0.0
        for i,j in enumerate(jets):
            if self.cleanJets(j, JETPTS[i], self.JS.jpt, self.JS.jeta, self.JS.usejid, year):
                #correct selected jets and their pts/masses. make sure this works
                sj = j
                sj.pt = JETPTS[i] #corresponds to syst
                sj.mass = JETMS[i] #corresponds to syst
                seljets.append(sj)
                seljet_pts.append(JETPTS[i])
                if JETBSFS is not None:
                    seljet_SFs.append(JETBSFS[i])
                njets+=1
                ht+=JETPTS[i]
                sumE += np.sqrt((JETPTS[i]**2)*np.cosh(j.eta)**2 + JETMS[i]**2)
                if self.cleanBJets(j, JETPTS[i], self.JS.bWP, self.JS.bpt, self.JS.beta):
                    selbjets.append(sj)
                    nbjets+=1
                    bjht+=JETPTS[i]
                    if JETPTS[i]>leadbpt:
                        leadbpt=JETPTS[i]
                    sumbdisc+=j.btagDeepFlavB
        if sumE>0.0:
            centrality = ht/sumE
        sph, apl = phys.EventShape(seljets)
        dphib1b2 = 0.0; detab1b2 = 0.0; dphij1j2 = 0.0; detaj1j2 = 0.0; htratio=0.0
        j6pt=0.0; j7pt=0.0 
        if nbjets>0:
            meanbdisc = sumbdisc/nbjets        
        if njets>1:
            dphij1j2 = deltaPhi(seljets[0].phi, seljets[1].phi)
            detaj1j2 = seljets[0].eta-seljets[1].eta
        if nbjets>1:
            dphib1b2 = deltaPhi(selbjets[0].phi, selbjets[1].phi)
            detab1b2 = selbjets[0].eta-selbjets[1].eta
        if njets>=6:
            if ht>0.0:
                htratio = (seljet_pts[0]+seljet_pts[1]+seljet_pts[2]+seljet_pts[3]+seljet_pts[4]+seljet_pts[5])/ht
            j6pt=seljet_pts[5]
        if njets>=7:
            j7pt=seljet_pts[6]
        #event-level bSF
        btagSF = 1.0
        if len(seljet_SFs)>0:
            # btagSF= reduce(lambda x, y: x*y, seljet_SFs)
            for sf in seljet_SFs:
                # print sf
                btagSF*=sf
        # print btagSF

        seljets  = np.array(seljets)
        selbjets = np.array(selbjets)

        jetdict = {'njets':njets,
                   'nbjets':nbjets,
                   'ht':ht,
                   'bjht':bjht,
                   'leadbpt':leadbpt,
                   'btagSF':btagSF,
                   'sphericity':sph, 
                   'aplanarity':apl,
                   'dphij1j2':dphij1j2,
                   'detaj1j2':detaj1j2,
                   'dphib1b2':dphib1b2,
                   'detab1b2':detab1b2,
                   'jet6pt':j6pt,
                   'jet7pt':j7pt,
                   'centrality':centrality,
                   'htratio':htratio, 
                   'meanbdisc':meanbdisc   }
        return seljets, selbjets, jetdict

    def GetSysts(self, syst, normdefs, event, jets, fatjets):
        # returns needed versions of jet, met, and fatjet collections for given systematic as dictionary
        if not self.isMC:
            return normdefs

        systdefs = {'btagLFUp'  : [jet.btagSF_deepjet_shape_up_lf for jet in jets],
                    'btagLFDown': [jet.btagSF_deepjet_shape_down_lf for jet in jets],
                    'btagHFUp'  : [jet.btagSF_deepjet_shape_up_hf for jet in jets],
                    'btagHFDown': [jet.btagSF_deepjet_shape_down_hf for jet in jets],
                    'btagLFstats1Up'  : [jet.btagSF_deepjet_shape_up_lfstats1 for jet in jets],
                    'btagLFstats1Down': [jet.btagSF_deepjet_shape_down_lfstats1 for jet in jets],
                    'btagHFstats1Up'  : [jet.btagSF_deepjet_shape_up_hfstats1 for jet in jets],
                    'btagHFstats1Down': [jet.btagSF_deepjet_shape_down_hfstats1 for jet in jets],
                    'btagLFstats2Up'  : [jet.btagSF_deepjet_shape_up_lfstats2 for jet in jets],
                    'btagLFstats2Down': [jet.btagSF_deepjet_shape_down_lfstats2 for jet in jets],
                    'btagHFstats2Up'  : [jet.btagSF_deepjet_shape_up_hfstats2 for jet in jets],
                    'btagHFstats2Down': [jet.btagSF_deepjet_shape_down_hfstats2 for jet in jets],
                    'btagCFerr1Up'  : [jet.btagSF_deepjet_shape_up_cferr1 for jet in jets],
                    'btagCFerr1Down': [jet.btagSF_deepjet_shape_down_cferr1 for jet in jets],
                    'btagCFerr2Up'  : [jet.btagSF_deepjet_shape_up_cferr2 for jet in jets],
                    'btagCFerr2Down': [jet.btagSF_deepjet_shape_down_cferr2 for jet in jets]}
        
        #don't change anything
        if (syst in ['nosys', 'DeepAK8WSF_Up', 'DeepAK8WSF_Down', 'DeepAK8TopSF_Up', 'DeepAK8TopSF_Down', 'ResTopEff_Up', 'ResTopEff_Down', 'ResTopMiss_Up', 'ResTopMiss_Down']):#, 'trigSFUp', 'trigSFDown']):
            return normdefs

            # only change btagsfs
        elif (syst in ['btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down']):
            newbtagSF = systdefs[syst]
            newdict = {'jpts':normdefs['jpts'],
                       'jmass':normdefs['jmass'],
                       'fjpts':normdefs['fjpts'],
                       'fjmass':normdefs['fjmass'],
                       'bSFs':newbtagSF, #changed
                       'met':normdefs['met']}
            return newdict
        elif syst =='jerUp':
            newdict = {'jpts' :[jet.pt_jerUp for jet in jets],
                       'jmass':[jet.mass_jerUp for jet in jets],
                       'fjpts':[fatjet.pt_jerUp for fatjet in fatjets],
                       'fjmass':[fatjet.mass_jerUp for fatjet in fatjets],
                       'bSFs' :normdefs['bSFs'],
                       'met'  :Object(event,self.metsysname).pt_jerUp}
            return newdict
        elif syst =='jerDown':
            newdict = {'jpts' :[jet.pt_jerDown for jet in jets],
                       'jmass':[jet.mass_jerDown for jet in jets],
                       'fjpts':[fatjet.pt_jerDown for fatjet in fatjets],
                       'fjmass':[fatjet.mass_jerDown for fatjet in fatjets],
                       'bSFs' :normdefs['bSFs'],
                       'met'  :Object(event,self.metsysname).pt_jerDown}
            return newdict
        elif syst=='jesUp':
            newdict = {'jpts' :[jet.pt_jesTotalUp for jet in jets],
                       'jmass':[jet.mass_jesTotalUp for jet in jets],
                       'fjpts':[fatjet.pt_jesTotalUp for fatjet in fatjets],
                       'fjmass':[fatjet.mass_jesTotalUp for fatjet in fatjets],
                       'bSFs' :[jet.btagSF_deepjet_shape_up_jes for jet in jets], #per jet SFs
                       'met'  :Object(event,self.metsysname).pt_jesTotalUp}
            return newdict
        elif syst=='jesDown':
            newdict = {'jpts' :[jet.pt_jesTotalDown for jet in jets],
                       'jmass':[jet.mass_jesTotalDown for jet in jets],
                       'fjpts':[fatjet.pt_jesTotalDown for fatjet in fatjets],
                       'fjmass':[fatjet.mass_jesTotalDown for fatjet in fatjets],
                       'bSFs' :[jet.btagSF_deepjet_shape_down_jes for jet in jets], #per jet SFs
                       'met'  :Object(event,self.metsysname).pt_jesTotalDown}
            return newdict
        else:
            print 'unknown syst'

    def fillsystvars(self, syst, normdefs, event, jets, fatjets):

        #get needed jet variations as disctonary
        jvs = self.GetSysts(syst, normdefs, event, jets, fatjets)
        met = jvs['met']
            
        #get selected jets and variables:
        if self.isMC:
            seljets, selbjets, jetvars = self.getSystJets(self.year, jets, jvs['jpts'], jvs['jmass'], jvs['bSFs'])
        else:
            seljets, selbjets, jetvars = self.getSystJets(self.year, jets, jvs['jpts'], jvs['jmass'])

        #get selected boosted objects and variables:
        for i, fj in enumerate(fatjets): #redefine mass and pt in fatjets according to systs
            fj.mass = jvs['fjmass'][i]
            fj.pt = jvs['fjpts'][i]
            
        selBTs, selBWs, sfjm = self.BS.getBoosts(event, fatjets, getvars=True)
        #boosted top and w sfs
        bsWSF =1.0 ; bsTopSF = 1.0
        if len(fatjets)>0:
            boostWSFs, boostTSFs = GetBoostSFs(self.year, fatjets)
            if syst == 'DeepAK8WSF_Up':
                bsWSF = boostWSFs[1]
                bsTopSF = boostTSFs[0]
            elif syst == 'DeepAK8WSF_Down':
                bsWSF = boostWSFs[2]
                bsTopSF = boostTSFs[0]
            elif syst == 'DeepAK8TopSF_Up':
                bsWSF = boostWSFs[0]
                bsTopSF = boostTSFs[1]
            elif syst == 'DeepAK8TopSF_Down':
                bsWSF = boostWSFs[0]
                bsTopSF = boostTSFs[2]
            else:
                bsWSF = boostWSFs[0]
                bsTopSF = boostTSFs[0]

        #get resolved tops using selected jets and boosted objects
        topdict = self.TF.TopGetter(event, seljets, selBTs, selBWs, self.isMC)   
        boostedtops = topdict['BSTops']; restops = topdict['ResTops']; Ws = topdict['BSWs']; topmissSF=topdict['missSF']; topeffSF=topdict['effSF']

        #resolved top scale factors
        effSF = 1.0; missSF = 1.0
        if syst == 'ResTopEff_Up':
            effSF = topeffSF[1]
            missSF = topmissSF[0]
        elif syst == 'ResTopEff_Down':
            effSF = topeffSF[2]
            missSF = topmissSF[0]
        elif syst == 'ResTopMiss_Up':
            effSF = topeffSF[0]
            missSF = topmissSF[1]
        elif syst == 'ResTopMiss_Down':
            effSF = topeffSF[0]
            missSF = topmissSF[2]
        else:
            effSF = topeffSF[0]
            missSF = topmissSF[0]

        leadtpt=0.0; nrestops=len(restops)
        if nrestops>0:
            for t in restops:
                if t[0].Pt()>leadtpt:
                    leadtpt=t[0].Pt()

        metovsqrtht=0.0
        if jetvars['ht']>0.0:
            metovsqrtht = met/np.sqrt(jetvars['ht'])

        #trigger SFs
        # trigSF=1.0
        # if self.isMC:
        #     triggerSFs =  self.th.get_trig_sf(jetvars['njets'], jetvars['nbjets'])
        #     trigSF = triggerSFs[0]        
        #     if syst == 'trigSFUp':
        #         trigSF = triggerSFs[1]
        #     elif syst == 'trigSFDown':
        #         trigSF = triggerSFs[2]

        # #bcorrSFs
        # bSFcorr=1.0
        # if (self.isMC and syst in ['nosys', 'jesUp', 'jesDown', 'jerUp', 'jerDown', 'btagLFUp', 'btagLFDown','btagHFUp', 'btagHFDown', 'btagLFstats1Up', 'btagLFstats1Down','btagHFstats1Up', 'btagHFstats1Down', 'btagLFstats2Up', 'btagLFstats2Down','btagHFstats2Up', 'btagHFstats2Down', 'btagCFerr1Up', 'btagCFerr1Down', 'btagCFerr2Up', 'btagCFerr2Down']):
        #     bSFcorr = self.bch.getbSFcorr(syst, jetvars['njets'], jetvars['ht'])

        #get bdt discriminants for sr and vr:
        bdtdict  = {'nrestops'    :int(nrestops),
                    'ht'          :float(jetvars['ht']),
                    'nbstops'     :len(boostedtops),
                    'njets'      :jetvars['njets'],
                    'nbjets'     :jetvars['nbjets'],
                    'met'        :float(met), ##
                    'nbsws'      :int(len(selBWs)), ##
                    'metovsqrtht':float(metovsqrtht), ##
                    'sfjm'       :float(sfjm), ##
                    'restop1pt'  :float(leadtpt), ##
                    'sphericity' :jetvars['sphericity'],
                    'aplanarity' :jetvars['aplanarity'],
                    'leadbpt'    :jetvars['leadbpt'],
                    'bjht'       :jetvars['bjht'],
                    'dphij1j2'   :jetvars['dphij1j2'],
                    'dphib1b2'   :jetvars['dphib1b2'],
                    'detaj1j2'   :jetvars['detaj1j2'],
                    'detab1b2'   :jetvars['detab1b2'],
                    'jet6pt'     :jetvars['jet6pt'],                        
                    'jet7pt'     :jetvars['jet7pt'],
                    'meanbdisc'  :jetvars['meanbdisc'],
                    'htratio'    :jetvars['htratio'],
                    'centrality' :jetvars['centrality']}
            
        # catBDTdisc = self.BDT.getBDTdisc(bdtdict)

        outvals = {'btagSF'      :float(jetvars['btagSF']),
                   'bsWSF'       :bsWSF,
                   'bsTopSF'     :bsTopSF,
                   'RTeffSF'     :effSF,
                   'RTmissSF'    :missSF}#,
                   # 'trigSF'      :trigSF,
                   # 'bSFcorr'     :bSFcorr}
                   #      'catBDTdisc'  :catBDTdisc,

        #write to the output trees
        for outvar in self.outvarlist.keys():
            branchname = outvar+'_'+syst
            self.out.fillBranch(branchname, outvals[outvar])
            
        for var in self.bdtvars.keys():
            if syst=='jerUp' or syst=='jesUp' or syst=='jerDown' or syst=='jesDown' or syst=='nosys':
                branchname = var+'_'+syst                
                self.out.fillBranch(branchname, bdtdict[var])
            
        if (jetvars['njets']>=6 and jetvars['nbjets']>=2):
            return True
        else: 
            return False
            
    def analyze(self, event):
        jets      = Collection(event, "Jet")
        fatjets   = Collection(event, "FatJet")

        normdefs = {'jpts' :[jet.pt for jet in jets],
                    'jmass':[jet.mass for jet in jets],
                    'fjpts':[fatjet.pt for fatjet in fatjets],
                    'fjmass':[fatjet.mass for fatjet in fatjets],
                    'met'  :Object(event,self.metname).pt}
        if self.isMC:
            normdefs['bSFs']=[jet.btagSF_deepjet_shape for jet in jets] #per jet SFs
        
        #apply preselection: if all counts of njets and nbjets dont pass selection nb>=2 nj>=6,throw out event
        passbase = []
        # print '\n NEW EVENT'
        for syst in self.systlist:
            torf = self.fillsystvars(syst, normdefs, event, jets, fatjets)
            passbase.append(torf)

        if any(passbase): #if in any case it passes skim return event, otherwise do not
            return True
        else:
            return False

SystProducer2016 = lambda : SystProducer('0lep','2016')
SystProducer2017 = lambda : SystProducer('0lep','2017')
SystProducer2018 = lambda : SystProducer('0lep','2018')

SystProducer2016red = lambda : SystProducer('0lep','2016',True)
SystProducer2017red = lambda : SystProducer('0lep','2017',True)
SystProducer2018red = lambda : SystProducer('0lep','2018',True)

