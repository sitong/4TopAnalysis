#skim tree pieces, flatten arrays, merge if desired. add bdts if desired. proper triggers
import uproot
import argparse
import numpy as np
import pandas as pd
import xgboost as xgb

parser = argparse.ArgumentParser(description='rootfile skimmer and more!')
#general:
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-t", "--treename", dest="treename", default='Events')
parser.add_argument("-o", "--outdir", dest="outdir", default='./updatedrootfiles/')
parser.add_argument("-v", "--verbose", dest="verbose", default='False')
#skimming:
parser.add_argument("-s", "--skim", dest="skim", default='True')
parser.add_argument("-c", "--cut", dest="cut", default='default') #if not specified will be default for 0 or 1 lep
#bdt:
parser.add_argument("-a", "--addbdt", dest="addbdt", default='True')
parser.add_argument("-b", "--bdts", dest="bdts", default=['bdt4']) #list of bdt discs to be added
#merge:
parser.add_argument("-m", "--merge", dest="merge", default='False')
parser.add_argument("-r", "--removepieces", dest="rempieces", default='False') #remove smaller files aftter merge

args = parser.parse_args()

def Skim(branches, f):
    mask = (branches['njets']>=3)
    if '1lep' in f:
        mask = (branches['njets']>=3 )#& branches['nleptons']>=1)
    elif '0lep' in f:
        mask = (branches['njets']>=6 )#& branches['nleptons']==0)
    branches[:] = branches[:][mask]
    # branches['njets'] = branches['njets'][mask]
    print branches['njets']    


#set everything up
fillbdts=False; mergey=False; killbits=False; skimmy=False
print "Will process", len(args.filenames), "files..."
if args.addbdt=='True':
    print 'filling bdts:', args.bdts
    fillbdts=True
if args.merge=='True':
    print 'Will merge files. Deleting pieces?', args.rempieces
    mergey=True
    if args.rempieces=='True':
        killbits=True
if args.skim=='True':
    print 'Will skim files.'
    skimmy=True

sel0lep = 'nleptons==0 && njets>=6'
sel1lep = 'nleptons>=1 && njets>=3'

#load files:
for f in args.filenames:
        #get proper outname:
    outname = f[f.rfind('/')+1:]
    outname = outname.strip('.root')

    sel = ''; note = ''
    if '1lep' in f:
        note='1lep_'
    elif '0lep' in f:
        note='0lep_'
        
    outname += '_'+note+'withBDT.root'
    print outname
        
    infile = uproot.open(f)
    tree = infile[args.treename]
        #convert tree to dataframe:
        # data = tree.arrays("b", outputtype=pd.DataFrame, )
    print tree.keys()
    branches = tree.arrays(branches=None, namedecode='utf-8', flatten=True)
    if skimmy:
        branches = Skim(branches, f)
        # data = tree.arrays(b'njets', outputtype=pd.DataFrame)#namedecode='utf-8')

    # print branches['discs']
    # print branches['HLT_IsoMu24']
    # print data
    
        
