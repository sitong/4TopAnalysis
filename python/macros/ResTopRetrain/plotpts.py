import numpy as np
import matplotlib
import argparse
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import root_numpy as rn
import ROOT as r
r.gStyle.SetOptStat(0)

def getyield(hist, verbose=False):
    errorVal = r.Double(0)
    minbin=0
    maxbin=hist.GetNbinsX()+1
    hyield = hist.IntegralAndError(minbin, maxbin, errorVal)
    if verbose:
        print 'yield:', round(hyield, 3), '+/-', round(errorVal, 3), '\n'
    return hyield,  errorVal


c1 = r.TCanvas("c","c",800,800)
legend = r.TLegend(0.1, 0.6, 0.3, 0.9)

infile='./outputfilesNANO/resTop-result-ucsb2016-flatpt-deepflavour-tt.root'
f = r.TFile(infile)
t = f.Get('Friends')

nbins=15; minrange=0.0; maxrange=1000.0
shistB = r.TH1F('shistB', 'shistB', nbins, minrange, maxrange)
shistA = r.TH1F('shistA', 'shistA', nbins, minrange, maxrange)
bhistB = r.TH1F('bhistB', 'bhistB', nbins, minrange, maxrange)
bhistA = r.TH1F('bhistA', 'bhistA', nbins, minrange, maxrange)

sel = 'njets>=5 & ndeepbjets>=1'
sig_sel = sel + ' & flag_signal>0'
bkg_sel = sel + ' & flag_signal<=0'
wgtvar = 'weight'
reweighted_var = 'reweight'

t.Draw("topcand_pt>>shistB", "("+sig_sel+")*"+wgtvar)
t.Draw("topcand_pt>>bhistB", "("+bkg_sel+")*"+wgtvar)
t.Draw("topcand_pt>>shistA", "("+sig_sel+")*"+reweighted_var)
t.Draw("topcand_pt>>bhistA", "("+bkg_sel+")*"+reweighted_var)

y_sB, e_sB = getyield(shistB)
y_sA, e_sA = getyield(shistA)
y_bB, e_bB = getyield(bhistB)
y_bA, e_bA = getyield(bhistA)
shistB.Scale(1.0/y_sB)
shistA.Scale(1.0/y_sA)
bhistB.Scale(1.0/y_bB)
bhistA.Scale(1.0/y_bA)

shistA.SetLineWidth(2)
shistB.SetLineWidth(2)
shistA.SetLineColor(r.kRed+2)
shistB.SetLineColor(r.kRed)
legend.AddEntry(shistB, "sig before", "L")           
legend.AddEntry(shistA, "sig after", "L")           


bhistA.SetLineWidth(2)
bhistB.SetLineWidth(2)
bhistA.SetLineColor(r.kBlue+2)
bhistB.SetLineColor(r.kBlue)
legend.AddEntry(bhistB, "bkg before", "L")           
legend.AddEntry(bhistA, "bkg after", "L")           

shistB.Draw("hist")
shistA.Draw("histsame")
bhistB.Draw("histsame")
bhistA.Draw("histsame")
legend.Draw()

c1.SaveAs("reweightinghists.png")
