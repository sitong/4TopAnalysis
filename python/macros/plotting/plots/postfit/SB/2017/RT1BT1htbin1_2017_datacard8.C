void RT1BT1htbin1_2017_datacard8()
{
//=========Macro generated from canvas: c/c
//=========  (Wed Aug 24 23:03:21 2022) by ROOT version 6.12/07
   TCanvas *c = new TCanvas("c", "c",0,0,1000,1000);
   gStyle->SetOptStat(0);
   c->SetHighLightColor(2);
   c->Range(0,0,1,1);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "p1",0,0.3,1,1);
   p1->Draw();
   p1->cd();
   p1->Range(-1.125,-15.78624,10.125,142.0762);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(2);
   p1->SetFrameBorderMode(0);
   p1->SetFrameBorderMode(0);
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("");
   stack->SetMaximum(120.2761);
   
   TH1F *stack_stack_22 = new TH1F("stack_stack_22","",9,0,9);
   stack_stack_22->SetMinimum(0);
   stack_stack_22->SetMaximum(126.2899);
   stack_stack_22->SetDirectory(0);
   stack_stack_22->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_22->SetLineColor(ci);
   stack_stack_22->GetXaxis()->SetTitle("BDT discriminant");
   stack_stack_22->GetXaxis()->SetLabelFont(42);
   stack_stack_22->GetXaxis()->SetLabelSize(0.035);
   stack_stack_22->GetXaxis()->SetTitleSize(0.035);
   stack_stack_22->GetXaxis()->SetTitleFont(42);
   stack_stack_22->GetYaxis()->SetTitle("NEntries");
   stack_stack_22->GetYaxis()->SetLabelFont(43);
   stack_stack_22->GetYaxis()->SetLabelSize(20);
   stack_stack_22->GetYaxis()->SetTitleSize(20);
   stack_stack_22->GetYaxis()->SetTitleOffset(1.55);
   stack_stack_22->GetYaxis()->SetTitleFont(43);
   stack_stack_22->GetZaxis()->SetLabelFont(42);
   stack_stack_22->GetZaxis()->SetLabelSize(0.035);
   stack_stack_22->GetZaxis()->SetTitleSize(0.035);
   stack_stack_22->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_22);
   
   
   TH1F *other_stack_1 = new TH1F("other_stack_1","other in RT1BT1htbin1_2017_datacard8",9,0,9);
   other_stack_1->SetBinContent(1,0.3515164);
   other_stack_1->SetBinContent(2,0.2808236);
   other_stack_1->SetBinContent(3,0.2004);
   other_stack_1->SetBinContent(4,0.8975306);
   other_stack_1->SetBinContent(5,0.5449196);
   other_stack_1->SetBinContent(6,0.7326589);
   other_stack_1->SetBinContent(7,0.6347814);
   other_stack_1->SetBinContent(8,2.648055);
   other_stack_1->SetBinContent(9,0.0617025);
   other_stack_1->SetBinError(1,0.7522994);
   other_stack_1->SetBinError(2,0.09527978);
   other_stack_1->SetBinError(3,0.2405972);
   other_stack_1->SetBinError(4,0.4452355);
   other_stack_1->SetBinError(5,0.3579673);
   other_stack_1->SetBinError(6,0.4477422);
   other_stack_1->SetBinError(7,0.2495014);
   other_stack_1->SetBinError(8,1.249793);
   other_stack_1->SetBinError(9,1.630787);
   other_stack_1->SetEntries(9);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#d07e93");
   other_stack_1->SetLineColor(ci);
   other_stack_1->GetXaxis()->SetTitle("CMS_th1x");
   other_stack_1->GetXaxis()->SetLabelFont(42);
   other_stack_1->GetXaxis()->SetLabelSize(0.035);
   other_stack_1->GetXaxis()->SetTitleSize(0.035);
   other_stack_1->GetXaxis()->SetTitleFont(42);
   other_stack_1->GetYaxis()->SetTitle("Events / ( 1 )");
   other_stack_1->GetYaxis()->SetLabelFont(42);
   other_stack_1->GetYaxis()->SetLabelSize(0.035);
   other_stack_1->GetYaxis()->SetTitleSize(0.035);
   other_stack_1->GetYaxis()->SetTitleOffset(0);
   other_stack_1->GetYaxis()->SetTitleFont(42);
   other_stack_1->GetZaxis()->SetLabelFont(42);
   other_stack_1->GetZaxis()->SetLabelSize(0.035);
   other_stack_1->GetZaxis()->SetTitleSize(0.035);
   other_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(other_stack_1,"");
   
   TH1F *TTTT_stack_2 = new TH1F("TTTT_stack_2","TTTT in RT1BT1htbin1_2017_datacard8",9,0,9);
   TTTT_stack_2->SetBinContent(1,0.06808501);
   TTTT_stack_2->SetBinContent(2,0.1462072);
   TTTT_stack_2->SetBinContent(3,0.2387636);
   TTTT_stack_2->SetBinContent(4,0.4430403);
   TTTT_stack_2->SetBinContent(5,0.6786266);
   TTTT_stack_2->SetBinContent(6,1.115468);
   TTTT_stack_2->SetBinContent(7,1.803819);
   TTTT_stack_2->SetBinContent(8,3.333211);
   TTTT_stack_2->SetBinContent(9,8.68838);
   TTTT_stack_2->SetBinError(1,0.03084218);
   TTTT_stack_2->SetBinError(2,0.0658623);
   TTTT_stack_2->SetBinError(3,0.09559036);
   TTTT_stack_2->SetBinError(4,0.1905096);
   TTTT_stack_2->SetBinError(5,0.2651251);
   TTTT_stack_2->SetBinError(6,0.4532977);
   TTTT_stack_2->SetBinError(7,0.6922073);
   TTTT_stack_2->SetBinError(8,1.321722);
   TTTT_stack_2->SetBinError(9,3.231979);
   TTTT_stack_2->SetEntries(9);

   ci = TColor::GetColor("#a1794a");
   TTTT_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#a1794a");
   TTTT_stack_2->SetLineColor(ci);
   TTTT_stack_2->GetXaxis()->SetTitle("CMS_th1x");
   TTTT_stack_2->GetXaxis()->SetLabelFont(42);
   TTTT_stack_2->GetXaxis()->SetLabelSize(0.035);
   TTTT_stack_2->GetXaxis()->SetTitleSize(0.035);
   TTTT_stack_2->GetXaxis()->SetTitleFont(42);
   TTTT_stack_2->GetYaxis()->SetTitle("Events / ( 1 )");
   TTTT_stack_2->GetYaxis()->SetLabelFont(42);
   TTTT_stack_2->GetYaxis()->SetLabelSize(0.035);
   TTTT_stack_2->GetYaxis()->SetTitleSize(0.035);
   TTTT_stack_2->GetYaxis()->SetTitleOffset(0);
   TTTT_stack_2->GetYaxis()->SetTitleFont(42);
   TTTT_stack_2->GetZaxis()->SetLabelFont(42);
   TTTT_stack_2->GetZaxis()->SetLabelSize(0.035);
   TTTT_stack_2->GetZaxis()->SetTitleSize(0.035);
   TTTT_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(TTTT_stack_2,"");
   
   TH1F *TTX_stack_3 = new TH1F("TTX_stack_3","TTX in RT1BT1htbin1_2017_datacard8",9,0,9);
   TTX_stack_3->SetBinContent(1,0.5609113);
   TTX_stack_3->SetBinContent(2,0.3530273);
   TTX_stack_3->SetBinContent(3,0.2175385);
   TTX_stack_3->SetBinContent(4,0.5463908);
   TTX_stack_3->SetBinContent(5,0.86589);
   TTX_stack_3->SetBinContent(6,1.5482);
   TTX_stack_3->SetBinContent(7,1.506258);
   TTX_stack_3->SetBinContent(8,1.453182);
   TTX_stack_3->SetBinContent(9,2.157969);
   TTX_stack_3->SetBinError(1,0.4405441);
   TTX_stack_3->SetBinError(2,0.2549518);
   TTX_stack_3->SetBinError(3,0.2281061);
   TTX_stack_3->SetBinError(4,0.267572);
   TTX_stack_3->SetBinError(5,0.4468468);
   TTX_stack_3->SetBinError(6,0.7996568);
   TTX_stack_3->SetBinError(7,0.8194917);
   TTX_stack_3->SetBinError(8,0.686121);
   TTX_stack_3->SetBinError(9,1.008958);
   TTX_stack_3->SetEntries(9);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#163d4e");
   TTX_stack_3->SetLineColor(ci);
   TTX_stack_3->GetXaxis()->SetTitle("CMS_th1x");
   TTX_stack_3->GetXaxis()->SetLabelFont(42);
   TTX_stack_3->GetXaxis()->SetLabelSize(0.035);
   TTX_stack_3->GetXaxis()->SetTitleSize(0.035);
   TTX_stack_3->GetXaxis()->SetTitleFont(42);
   TTX_stack_3->GetYaxis()->SetTitle("Events / ( 1 )");
   TTX_stack_3->GetYaxis()->SetLabelFont(42);
   TTX_stack_3->GetYaxis()->SetLabelSize(0.035);
   TTX_stack_3->GetYaxis()->SetTitleSize(0.035);
   TTX_stack_3->GetYaxis()->SetTitleOffset(0);
   TTX_stack_3->GetYaxis()->SetTitleFont(42);
   TTX_stack_3->GetZaxis()->SetLabelFont(42);
   TTX_stack_3->GetZaxis()->SetLabelSize(0.035);
   TTX_stack_3->GetZaxis()->SetTitleSize(0.035);
   TTX_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(TTX_stack_3,"");
   
   TH1F *DDBKG_stack_4 = new TH1F("DDBKG_stack_4","DDBKG in RT1BT1htbin1_2017_datacard8",9,0,9);
   DDBKG_stack_4->SetBinContent(1,12.08588);
   DDBKG_stack_4->SetBinContent(2,25.51935);
   DDBKG_stack_4->SetBinContent(3,28.09177);
   DDBKG_stack_4->SetBinContent(4,31.64595);
   DDBKG_stack_4->SetBinContent(5,39.13164);
   DDBKG_stack_4->SetBinContent(6,47.69249);
   DDBKG_stack_4->SetBinContent(7,45.91146);
   DDBKG_stack_4->SetBinContent(8,48.69571);
   DDBKG_stack_4->SetBinContent(9,60.68488);
   DDBKG_stack_4->SetBinError(1,1.170833);
   DDBKG_stack_4->SetBinError(2,2.01558);
   DDBKG_stack_4->SetBinError(3,2.020812);
   DDBKG_stack_4->SetBinError(4,2.474775);
   DDBKG_stack_4->SetBinError(5,2.857621);
   DDBKG_stack_4->SetBinError(6,3.444538);
   DDBKG_stack_4->SetBinError(7,3.160166);
   DDBKG_stack_4->SetBinError(8,3.591373);
   DDBKG_stack_4->SetBinError(9,6.107173);
   DDBKG_stack_4->SetEntries(9);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_4->SetFillColor(ci);

   ci = TColor::GetColor("#c1caf3");
   DDBKG_stack_4->SetLineColor(ci);
   DDBKG_stack_4->GetXaxis()->SetTitle("CMS_th1x");
   DDBKG_stack_4->GetXaxis()->SetLabelFont(42);
   DDBKG_stack_4->GetXaxis()->SetLabelSize(0.035);
   DDBKG_stack_4->GetXaxis()->SetTitleSize(0.035);
   DDBKG_stack_4->GetXaxis()->SetTitleFont(42);
   DDBKG_stack_4->GetYaxis()->SetTitle("Events / ( 1 )");
   DDBKG_stack_4->GetYaxis()->SetLabelFont(42);
   DDBKG_stack_4->GetYaxis()->SetLabelSize(0.035);
   DDBKG_stack_4->GetYaxis()->SetTitleSize(0.035);
   DDBKG_stack_4->GetYaxis()->SetTitleOffset(0);
   DDBKG_stack_4->GetYaxis()->SetTitleFont(42);
   DDBKG_stack_4->GetZaxis()->SetLabelFont(42);
   DDBKG_stack_4->GetZaxis()->SetLabelSize(0.035);
   DDBKG_stack_4->GetZaxis()->SetTitleSize(0.035);
   DDBKG_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(DDBKG_stack_4,"");
   stack->Draw("hist");
   
   TH1F *datahist__85 = new TH1F("datahist__85","datahist",9,0,9);
   datahist__85->SetBinContent(1,11);
   datahist__85->SetBinContent(2,19);
   datahist__85->SetBinContent(3,27);
   datahist__85->SetBinContent(4,36);
   datahist__85->SetBinContent(5,44);
   datahist__85->SetBinContent(6,54);
   datahist__85->SetBinContent(7,52);
   datahist__85->SetBinContent(8,50);
   datahist__85->SetBinContent(9,73);
   datahist__85->SetBinError(1,3.883919);
   datahist__85->SetBinError(2,4.909463);
   datahist__85->SetBinError(3,5.738257);
   datahist__85->SetBinError(4,6.536263);
   datahist__85->SetBinError(5,7.165933);
   datahist__85->SetBinError(6,7.877872);
   datahist__85->SetBinError(7,7.741083);
   datahist__85->SetBinError(8,7.601662);
   datahist__85->SetBinError(9,9.069177);
   datahist__85->SetEntries(9);
   datahist__85->SetStats(0);

   ci = TColor::GetColor("#666666");
   datahist__85->SetLineColor(ci);
   datahist__85->SetLineWidth(2);
   datahist__85->SetMarkerStyle(20);
   datahist__85->GetXaxis()->SetLabelFont(42);
   datahist__85->GetXaxis()->SetLabelSize(0.035);
   datahist__85->GetXaxis()->SetTitleSize(0.035);
   datahist__85->GetXaxis()->SetTitleFont(42);
   datahist__85->GetYaxis()->SetLabelFont(42);
   datahist__85->GetYaxis()->SetLabelSize(0.035);
   datahist__85->GetYaxis()->SetTitleSize(0.035);
   datahist__85->GetYaxis()->SetTitleOffset(0);
   datahist__85->GetYaxis()->SetTitleFont(42);
   datahist__85->GetZaxis()->SetLabelFont(42);
   datahist__85->GetZaxis()->SetLabelSize(0.035);
   datahist__85->GetZaxis()->SetTitleSize(0.035);
   datahist__85->GetZaxis()->SetTitleFont(42);
   datahist__85->Draw("psame");
   
   TH1F *stacktot__86 = new TH1F("stacktot__86","stacktot",9,0,9);
   stacktot__86->SetBinContent(1,13.06639);
   stacktot__86->SetBinContent(2,26.29941);
   stacktot__86->SetBinContent(3,28.74847);
   stacktot__86->SetBinContent(4,33.53291);
   stacktot__86->SetBinContent(5,41.22108);
   stacktot__86->SetBinContent(6,51.08882);
   stacktot__86->SetBinContent(7,49.85632);
   stacktot__86->SetBinContent(8,56.13016);
   stacktot__86->SetBinContent(9,71.59293);
   stacktot__86->SetBinError(1,1.46008);
   stacktot__86->SetBinError(2,2.03494);
   stacktot__86->SetBinError(3,2.050058);
   stacktot__86->SetBinError(4,2.53587);
   stacktot__86->SetBinError(5,2.926448);
   stacktot__86->SetBinError(6,3.593083);
   stacktot__86->SetBinError(7,3.346583);
   stacktot__86->SetBinError(8,4.083828);
   stacktot__86->SetBinError(9,7.170824);
   stacktot__86->SetEntries(36);
   stacktot__86->SetStats(0);

   ci = TColor::GetColor("#666666");
   stacktot__86->SetFillColor(ci);
   stacktot__86->SetFillStyle(3002);

   ci = TColor::GetColor("#000099");
   stacktot__86->SetLineColor(ci);
   stacktot__86->SetLineWidth(0);
   stacktot__86->GetXaxis()->SetLabelFont(42);
   stacktot__86->GetXaxis()->SetLabelSize(0.035);
   stacktot__86->GetXaxis()->SetTitleSize(0.035);
   stacktot__86->GetXaxis()->SetTitleFont(42);
   stacktot__86->GetYaxis()->SetLabelFont(42);
   stacktot__86->GetYaxis()->SetLabelSize(0.035);
   stacktot__86->GetYaxis()->SetTitleSize(0.035);
   stacktot__86->GetYaxis()->SetTitleOffset(0);
   stacktot__86->GetYaxis()->SetTitleFont(42);
   stacktot__86->GetZaxis()->SetLabelFont(42);
   stacktot__86->GetZaxis()->SetLabelSize(0.035);
   stacktot__86->GetZaxis()->SetTitleSize(0.035);
   stacktot__86->GetZaxis()->SetTitleFont(42);
   stacktot__86->Draw("e2same");
   
   TLegend *leg = new TLegend(0.1,0.6,0.3,0.9,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("other_stack_1","other","F");

   ci = TColor::GetColor("#d07e93");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#d07e93");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTTT_stack_2","TTTT","F");

   ci = TColor::GetColor("#a1794a");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#a1794a");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("TTX_stack_3","TTX","F");

   ci = TColor::GetColor("#163d4e");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#163d4e");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("datahist","data","L");

   ci = TColor::GetColor("#666666");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("DDBKG_stack_4","DDBKG","F");

   ci = TColor::GetColor("#c1caf3");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#c1caf3");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   p1->Modified();
   c->cd();
  
// ------------>Primitives in pad: p2
   TPad *p2 = new TPad("p2", "p2",0,0.05,1,0.3);
   p2->Draw();
   p2->cd();
   p2->Range(-1.125,0.05714285,10.125,1.771429);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(2);
   p2->SetGridy();
   p2->SetBottomMargin(0.2);
   p2->SetFrameBorderMode(0);
   p2->SetFrameBorderMode(0);
   
   TH1F *raterr__87 = new TH1F("raterr__87","",9,0,9);
   raterr__87->SetBinContent(0,1);
   raterr__87->SetBinContent(1,1);
   raterr__87->SetBinContent(2,1);
   raterr__87->SetBinContent(3,1);
   raterr__87->SetBinContent(4,1);
   raterr__87->SetBinContent(5,1);
   raterr__87->SetBinContent(6,1);
   raterr__87->SetBinContent(7,1);
   raterr__87->SetBinContent(8,1);
   raterr__87->SetBinContent(9,1);
   raterr__87->SetBinContent(10,1);
   raterr__87->SetBinError(1,0.1117432);
   raterr__87->SetBinError(2,0.07737587);
   raterr__87->SetBinError(3,0.07131017);
   raterr__87->SetBinError(4,0.0756233);
   raterr__87->SetBinError(5,0.07099398);
   raterr__87->SetBinError(6,0.07033012);
   raterr__87->SetBinError(7,0.06712455);
   raterr__87->SetBinError(8,0.07275639);
   raterr__87->SetBinError(9,0.1001611);
   raterr__87->SetMinimum(0.4);
   raterr__87->SetMaximum(1.6);
   raterr__87->SetEntries(20);
   raterr__87->SetStats(0);

   ci = TColor::GetColor("#666666");
   raterr__87->SetFillColor(ci);
   raterr__87->SetFillStyle(3002);

   ci = TColor::GetColor("#666666");
   raterr__87->SetLineColor(ci);
   raterr__87->SetLineWidth(0);
   raterr__87->GetXaxis()->SetTitle("BDT discriminant");
   raterr__87->GetXaxis()->SetLabelFont(43);
   raterr__87->GetXaxis()->SetLabelSize(20);
   raterr__87->GetXaxis()->SetTitleSize(20);
   raterr__87->GetXaxis()->SetTitleOffset(4);
   raterr__87->GetXaxis()->SetTitleFont(43);
   raterr__87->GetYaxis()->SetTitle("ratio data/prediction");
   raterr__87->GetYaxis()->SetNdivisions(505);
   raterr__87->GetYaxis()->SetLabelFont(43);
   raterr__87->GetYaxis()->SetLabelSize(20);
   raterr__87->GetYaxis()->SetTitleSize(20);
   raterr__87->GetYaxis()->SetTitleOffset(1.55);
   raterr__87->GetYaxis()->SetTitleFont(43);
   raterr__87->GetZaxis()->SetLabelFont(42);
   raterr__87->GetZaxis()->SetLabelSize(0.035);
   raterr__87->GetZaxis()->SetTitleSize(0.035);
   raterr__87->GetZaxis()->SetTitleFont(42);
   raterr__87->Draw("e2");
   
   TH1F *ratio__88 = new TH1F("ratio__88","datahist",9,0,9);
   ratio__88->SetBinContent(1,0.8418545);
   ratio__88->SetBinContent(2,0.7224497);
   ratio__88->SetBinContent(3,0.9391805);
   ratio__88->SetBinContent(4,1.073572);
   ratio__88->SetBinContent(5,1.067415);
   ratio__88->SetBinContent(6,1.056983);
   ratio__88->SetBinContent(7,1.042997);
   ratio__88->SetBinContent(8,0.8907867);
   ratio__88->SetBinContent(9,1.019654);
   ratio__88->SetBinError(1,0.3530836);
   ratio__88->SetBinError(2,0.2583928);
   ratio__88->SetBinError(3,0.212528);
   ratio__88->SetBinError(4,0.1815628);
   ratio__88->SetBinError(5,0.1628621);
   ratio__88->SetBinError(6,0.1458865);
   ratio__88->SetBinError(7,0.148867);
   ratio__88->SetBinError(8,0.1520332);
   ratio__88->SetBinError(9,0.1242353);
   ratio__88->SetEntries(203.8932);
   ratio__88->SetStats(0);
   ratio__88->SetLineWidth(2);
   ratio__88->SetMarkerStyle(20);
   ratio__88->GetXaxis()->SetLabelFont(42);
   ratio__88->GetXaxis()->SetLabelSize(0.035);
   ratio__88->GetXaxis()->SetTitleSize(0.035);
   ratio__88->GetXaxis()->SetTitleFont(42);
   ratio__88->GetYaxis()->SetLabelFont(42);
   ratio__88->GetYaxis()->SetLabelSize(0.035);
   ratio__88->GetYaxis()->SetTitleSize(0.035);
   ratio__88->GetYaxis()->SetTitleOffset(0);
   ratio__88->GetYaxis()->SetTitleFont(42);
   ratio__88->GetZaxis()->SetLabelFont(42);
   ratio__88->GetZaxis()->SetLabelSize(0.035);
   ratio__88->GetZaxis()->SetTitleSize(0.035);
   ratio__88->GetZaxis()->SetTitleFont(42);
   ratio__88->Draw("pe0same");
   TLine *line = new TLine(0,1,1,1);
   line->Draw();
   p2->Modified();
   c->cd();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
