#adds event bdt disc to existing tree, NOT ntuple
#also applies baseline selection!!
from root_numpy import root2array, array2root
# import varlists as vl
import argparse
import numpy as np
import pandas as pd
# import xgboost as xgb
import subprocess
import os
import sys
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score
import re
# import xml.etree.cElementTree as ET
import sys
# sys.path.append("/uscms_data/d3/mquinnan/Nano4top2/CMSSW_10_2_10/src/PhysicsTools/NanoTrees/python/macros/plotting/")
# import PhysicsTools.NanoTrees.python.macros.plotting.RootStuff as rs
# import RootStuff as rs

parser = argparse.ArgumentParser(description='removes non baseline events and merges rootfiles')
parser.add_argument("-f", "--filenames", dest="filenames", nargs="*", default=['./testfiles/bla.root'])
parser.add_argument("-o", "--outdir", dest="outdir", default='./')
parser.add_argument("-t", "--treename", dest="treename", default='Events')
parser.add_argument("-b", "--baseline", dest="base", default='nleptons==0 && ht>700 & njets>=8 && nbjets>=2')
parser.add_argument("-m", "--merge", dest="merge", default=True)
parser.add_argument("-n", "--mergename", dest="mergename", default='_2016_merged_tree.root')
args = parser.parse_args()

def SkimFiles(args):
    filenames = args.filenames 
    treename = args.treename 
    baseline = args.base 
    outfiles = []
    print "Will skim", len(filenames), "files..."
    for f in filenames:
    #get proper filename:
        filename = f[f.rfind('/')+1:]
        filename = filename.strip('.root')
        filename += '_'+'skimmed'+'.root'
        infile = f
        print "prepping input file", infile
        data = pd.DataFrame(root2array(infile, treename, selection=baseline))
        print '# input events:', len(data)
        if len(data)==0:
            break
        print 'writing file'
        array2root(data.to_records(index=False), filename=args.outdir+filename, treename=treename, mode='RECREATE')
        # outfiles.append(outdir+filename)
    # return outfiles

#merge files
# def merge(args, files):

SkimFiles(args)

